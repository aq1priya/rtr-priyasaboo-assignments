#include<stdlib.h>
#include"common.h"

extern FILE* tpfile;

//function to get node for vertex list
vertex_triangle_t *get_vertex_triangle_node(triangle_t tri)
{
	vertex_triangle_t *node = (vertex_triangle_t *)xcalloc(1, sizeof(vertex_triangle_t));

	node->triangle = tri;
	node->triangletype = VALID_TRIANGLE;
	node->next = NULL;
	node->prev = NULL;
	
	return(node);
}

//function to insert node in vlist
void g_insert_triangle_vlist(vertex_triangle_t *beg, vertex_triangle_t *mid, vertex_triangle_t *end)
{
	mid->prev = beg;
	mid->next = end;
	beg->next = mid;
	end->prev = mid;
}

//function to initialize variable of structure triangle_t
triangle_t set_triangle(point_t p1, point_t p2, point_t p3, circle_t cir)
{
	triangle_t t;
	t.v[0] = { p1.x,p1.y };
	t.v[1] = { p2.x,p2.y };
	t.v[2] = { p3.x,p3.y };
	t.e[0] = { p1,p2 };
	t.e[1] = { p2,p3 };
	t.e[2] = { p3,p1 };
	t.c = cir;
	
	return(t);
}

void delete_vertex_node(vertex_triangle_t *node)
{
	node->next->prev = node->prev;
	node->prev->next = node->next;
	free(node);
}
