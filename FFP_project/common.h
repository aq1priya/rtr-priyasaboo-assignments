#pragma once
//headers
#include<Windows.h>
#include<stdio.h>
#include<time.h>
#include<gl/GL.h>
#include<gl/GLU.h>

#define  _USE_MATH_DEFINES 1
#include<math.h>

#pragma comment (lib, "glu32.lib")
#pragma comment (lib, "opengl32.lib")

#pragma once
#define SUCCESS				1
#define FAILURE				0
#define VALID_TRIANGLE		1
#define INVALID_TRIANGLE	0
#define POINTS_COLINEAR		-4
#define RANDOM_POINTS		15
#define MAX					(RANDOM_POINTS+4)
#define TEMP				false
#define PERM				true
#define INFY				99
#define NILL				-1

typedef int result_t;
typedef int len_t;

//structures
typedef struct point
{
	float x;
	float y;
	//float z;
	int number = 0;
}point_t;

typedef struct linknode
{
	point_t point;
	struct linknode *next;
}linknode_t;

typedef struct edgenode
{
	point_t start;
	point_t end;
	struct edgenode *next;
}edgenode_t;

typedef struct circle
{
	float radius =0.0f;
	point_t center = { 0.0f,0.0f };
	int isvalid = 1;
}circle_t;

typedef struct edge
{
	point_t start, end;
}edge_t;

typedef struct triangle
{
	point_t v[3];
	edge_t e[3];
	circle_t c;
	
}triangle_t;

//typedef struct adj_triangle
//{
//	triangle_t adj_triangle;
//	struct adj_triangle *prev;
//	struct adj_triangle *next;
//	int  triangletype;
//}adj_triangle_t;
//variable
typedef struct vertex_triangle
{
	triangle_t triangle;
	triangle_t pred_triangle;
	//struct adj_triangle *head;
	struct vertex_triangle *prev;
	struct vertex_triangle *next;
	int  triangletype;
}vertex_triangle_t;

typedef struct graph
{
	struct vertex_triangle *head;
	int nr_vertex_triangle;
	int nr_edges;
}graph_t;

/* Graph interface routines */
graph_t				*create_graph				(void);
result_t			add_vertex_triangle			(graph_t *g, triangle_t t);
result_t			delete_graph				(graph_t *);
void				create_adjacency_matrix		(edgenode_t *, linknode_t *);
void				dijkstra					(int, int, int);

/* Graph auxillary routines */
vertex_triangle_t	*get_vertex_triangle_node	(triangle_t);
void				g_insert_triangle_vlist		(vertex_triangle_t*, vertex_triangle_t*, vertex_triangle_t*);
triangle_t			set_triangle				(point_t, point_t, point_t, circle_t);
void				delete_vertex_node			(vertex_triangle_t *);

/*  Point linkedlist routines */
linknode_t*			create_point_list			(void);
linknode_t*			create_new_point_node		(point_t);
void				insert_at_beginning			(linknode_t*, point_t);
void				insert_at_end				(linknode_t*, point_t);
void				insert_after_node			(linknode_t*, point_t);
void				delete_at_begining			(linknode_t*);
void				delete_at_end				(linknode_t*);
void				delete_point_node			(linknode_t *, float);
void				print_list					(linknode_t* head);
result_t			delete_point_list			(linknode_t *);


/* Edge list interface routines */
edgenode			*create_edge_list			(void);
edgenode_t			*create_new_edge_node		(point_t,point_t);
void				insert_edgenode_at_beginning(edgenode *, point_t, point_t);
void				delete_edge_list			(edgenode_t *);
void				store_edges_to_edgebuffer	(edgenode_t *, triangle_t);
void				keep_valid_edges			(edgenode_t *);
void				single_copy_of_edge			(edgenode_t *, edge_t);
bool				delete_duplicate_edges		(edgenode_t *, edgenode_t *);


/* delaunay interface routine */
graph_t			*initialize_delaunay			(void);
graph_t			*triangulate					(graph_t *, linknode_t *);
edgenode_t		*delaunay_valid_edges			(graph_t *);
void			draw_delaunay					(edgenode_t *);

/* delaunay auxillary routine */
circle_t		cc_properties					(triangle_t traiangle);
float			dist							(point_t p, point_t q);
bool			check_point						(point_t, circle_t);
void			check_duplicate_edges_and_delete(edgenode_t *);

/* aurillary routine */
void			*xcalloc(int, size_t);

//bool			point_in_triangle(triangle_t, point_t);
//float			calculate_area(point_t, point_t, point_t);
//void			check_vertex_triangle_list(graph *);
