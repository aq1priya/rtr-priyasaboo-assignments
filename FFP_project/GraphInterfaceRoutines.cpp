#include"common.h"
extern FILE *gpfile;
float adj[MAX][MAX];
int index = 0;
int path[MAX];

graph_t *create_graph(void)
{
	graph_t *g = (graph_t *)xcalloc(1, sizeof(graph_t));

	g->head = (vertex_triangle_t*)xcalloc(1, sizeof(vertex_triangle_t));
	g->nr_vertex_triangle = 0;
	g->nr_edges = 0;
	g->head->next = g->head;
	g->head->prev = g->head;
	return(g);
}

result_t add_vertex_triangle(graph_t *g, triangle_t t)
{
	vertex_triangle_t *node = get_vertex_triangle_node(t);
	g_insert_triangle_vlist(g->head->prev, node, g->head);
	++g->nr_vertex_triangle;

	return(SUCCESS);
}

result_t delete_graph(graph_t *g)
{
	vertex_triangle_t *run, *temp;

	for (run = g->head->next; run != g->head; run = temp)
	{
		temp = run->next;
		delete_vertex_node(temp);
	}
	if (run = g->head)
	{
		free(g);
	}
	return(SUCCESS);
}

void create_adjacency_matrix(edgenode_t * edgehead, linknode_t *pointhead)
{
	int id_s, id_e;
	edgenode_t *erun;
	point_t pt_s, pt_e;

	erun = edgehead->next;

	//initializing 2d array with some random maximum value;
	for (int i = 0; i < MAX; i++)
	{
		for (int j = 0; j < MAX; j++)
		{
			adj[i][j] = 99.0f;
		}
	}

	print_list(pointhead);

	for (erun; erun != NULL; erun = erun->next)
	{
		pt_s = erun->start;
		pt_e = erun->end;
		id_s = erun->start.number;
		id_e = erun->end.number;

		adj[id_s][id_e] = dist(pt_s, pt_e);
		adj[id_e][id_s] = dist(pt_s, pt_e);
	}

	/*fprintf(gpfile, "\n\n**DISPLAY ADJACENCY MATRIX**\n\n");
	fprintf(gpfile, "column\t");
	for (int i = 0; i < MAX; i++)
	{
		fprintf(gpfile, "%d\t\t", i);
	}
	fprintf(gpfile, "\n");*/

	/*for (int i = 0; i < MAX; i++)
	{
		fprintf(gpfile, "row %d", i);
		for (int j = 0; j < MAX; j++)
		{
			fprintf(gpfile, "\t%f,", adj[i][j]);
		}
		fprintf(gpfile,"\n");
	}*/
}

void dijkstra(int n, int startnode, int destination)
{
	float cost[MAX][MAX];
	float distance[MAX], mindistance;
	int pred[MAX];
	int visited[MAX], count, nextnode;
	int d = destination;
	int s = startnode;

	//n is total numver of vertices
	//pred[] stores the predecessor of each node
	//count gives the number of nodes seen so far
	//distance[] stores the distance of each node from source node
	//create the cost matrix
	for (int i = 0; i < n; i++)
	{
		for (int j = 0; j < n; j++)
		{
			if (adj[i][j] == 99)
				cost[i][j] = INFY;
			else
				cost[i][j] = adj[i][j];
		}
	}

	//initialize pred[],distance[] and visited[]
	for (int i = 0; i < n; i++)
	{
		distance[i] = cost[startnode][i];
		pred[i] = startnode;
		visited[i] = 0;
	}

	distance[startnode] = 0;			//distance from source to source is set to 0;
	visited[startnode] = 1;				//startnode i.e source is visited , set v[source] = 1
	count = 1;							//1 node have seen so far i.e. startnode

	while (count < n - 1)				//while we dont cover all other nodes
	{
		mindistance = INFY;				//set mindistance to maximum
		
		//nextnode gives the node at minimum distance
		for (int i = 0; i < n; i++)
		{
			if (distance[i] < mindistance && !visited[i])
			{
				mindistance = distance[i];
				nextnode = i;
			}
		}

		//check if a better path exists through nextnode			
		visited[nextnode] = 1;

		for (int i = 0; i < n; i++)
		{
			if (!visited[i])
			{
				if (mindistance + cost[nextnode][i] < distance[i])
				{
					distance[i] = mindistance + cost[nextnode][i];
					pred[i] = nextnode;
				}
			}
			else
			{
				//fprintf(gpfile, "already visited\n");
			}
		}
		count++;
		//fprintf(gpfile, "no of nodes seen so far %d\n\n", count);
	}

	//print the path and distance of each node
	for (int i = 0; i < n; i++)
	{
		if (i != startnode)
		{
			fprintf(gpfile, "\nDistance of node%d=%f", i, distance[i]);
			fprintf(gpfile, "\nPath=%d", i);

			int j = i;
			do
			{
				j = pred[j];
				fprintf(gpfile, "<-%d", j);
			} while (j != startnode);
		}
	}

	
	float shortdist = 0;
	
	//fprintf(gpfile, "\n\nfind_path: shortest path form %d to %d  is:\n", s, d);
	while (d != s)
	{
		index++;
		path[index] = d;
		int u = pred[d];
		shortdist += adj[u][d];
		d = u;
	}
	index = index + 1;
	path[index] = s;
	for (int i = index; i >= 1; i--)
	{
		fprintf(gpfile, "%d --", path[i]);
	}
	fprintf(gpfile, "\nshortest distance = %f\n", shortdist);
}
