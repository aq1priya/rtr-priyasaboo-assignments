#include "common.h"
extern FILE *tpfile;
extern FILE *gpfile;

edgenode_t *create_edge_list(void)
{
	edgenode_t *head;

	head = (edgenode_t *)malloc(sizeof(edgenode_t));
	head->next = NULL;
	
	return(head);
}

edgenode_t *create_new_edge_node(point_t p1, point_t p2)
{
	edgenode *newnode;
	
	newnode = (edgenode_t *)malloc(sizeof(edgenode_t));
	newnode->start = p1;
	newnode->end = p2;
	newnode->start.number = p1.number;
	newnode->end.number = p2.number;
	newnode->next = NULL;

	return(newnode);
}

void insert_edgenode_at_beginning(edgenode *head, point_t p1, point_t p2)
{
	edgenode_t *node = create_new_edge_node(p1, p2);
	node->next = head->next;
	head->next = node;
}

void store_edges_to_edgebuffer(edgenode_t *eb_head, triangle_t t)
{
	for (int i = 0; i < 3; i++)
	{
		insert_edgenode_at_beginning(eb_head, t.e[i].start, t.e[i].end);
	}
}

void keep_valid_edges(edgenode_t *ehead)
{
	edgenode_t *erun = ehead->next;
	edge_t ed;

	for (erun = ehead->next; erun != NULL; erun = erun->next)
	{
		ed.start = erun->start;
		ed.end = erun->end;
		single_copy_of_edge(ehead, ed);
	}
}

bool delete_duplicate_edges(edgenode_t *head, edgenode_t * current)
{
	edgenode_t *beforeCurrent = head;
	edgenode_t *erun = head->next;
	edgenode_t *temp = head;
	point_t e_start = current->start, e_end = current->end;
	point_t run_start, run_end;
	int count = 0;

	for (erun; erun != NULL; erun = temp->next)
	{
		run_start = erun->start;
		run_end = erun->end;
		if (((e_start.x == run_start.x && e_start.y == run_start.y) && (e_end.x == run_end.x && e_end.y == run_end.y))
			|| ((e_start.x == run_end.x && e_start.y == run_end.y) && (e_end.x == run_start.x && e_end.y == run_start.y)))
		{
			count = count + 1;
		}
		if (count > 1)
		{
			temp->next = erun->next;
			free(erun);
			erun = NULL;

			beforeCurrent->next = current->next;
			free(current);
			current = NULL;
			return true;
		}
		else {
			if (count < 1) {
				beforeCurrent = erun;
			}
			temp = erun;
		}
	}
	return(false);
}
void single_copy_of_edge(edgenode_t *head, edge_t ed)
{
	edgenode_t *erun = head->next;
	edgenode_t *temp = head;

	point_t e_start = ed.start, e_end = ed.end;
	point_t run_start, run_end;
	int count = 0;

	for (erun; erun != NULL; erun = temp->next)
	{
		run_start = erun->start;
		run_end = erun->end;
		if (((e_start.x == run_start.x && e_start.y == run_start.y) && (e_end.x == run_end.x && e_end.y == run_end.y))
			|| ((e_start.x == run_end.x && e_start.y == run_end.y) && (e_end.x == run_start.x && e_end.y == run_start.y)))
		{
			count = count + 1;
		}
		if (count > 1)
		{
			//delete_edgenode();
			temp->next = erun->next;
			free(erun);
			erun = NULL;
			count = 1;
		}
		else {
			temp = erun;
		}
	}

}

void delete_edge_list(edgenode_t *head)
{
	edgenode *temp, *current;
	
	current = head->next;
	while (current != NULL)
	{
		temp = current;
		current = current->next;
		free(temp);
	}
	free(head);
}

