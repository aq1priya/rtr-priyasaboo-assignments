#include<stdlib.h>
#include"common.h"

void *xcalloc(int nr_elements, size_t size_per_element)
{
	void *tmp;

	tmp = calloc(nr_elements, size_per_element);
	if (!tmp)
	{
		//fprintf(stderr, "calloc:fatal:out of memory\n");
		exit(EXIT_FAILURE);
	}

	return (tmp);
}
