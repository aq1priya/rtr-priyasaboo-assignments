#pragma once
#include"common.h"

extern FILE* gpfile;
extern int id_point;

linknode_t* create_point_list(void)
{
	linknode_t *head;

	head = (linknode_t*)malloc(sizeof(linknode_t));
	head->next = NULL;
	return(head);
}

linknode_t* create_new_point_node(point_t p)
{
	linknode_t *NewNode;

	NewNode = (linknode_t*)malloc(sizeof(linknode_t));
	NewNode->point.x = p.x;
	NewNode->point.y = p.y;
	NewNode->point.number = id_point++;
	return (NewNode);
}

void insert_at_beginning(linknode_t* head, point_t p)
{
	linknode_t *NewNode;

	NewNode = create_new_point_node(p);
	NewNode->next = head->next;
	head->next = NewNode;
}

void insert_at_end(linknode_t* head, point_t p)
{
	linknode_t *CurrentNode, *NewNode;
	
	NewNode = create_new_point_node(p);

	CurrentNode = head;
	while (CurrentNode->next != NULL)
	{
		CurrentNode = CurrentNode->next;
	}
	NewNode->next = NULL;
	CurrentNode->next = NewNode;
}

void insert_after_node(linknode_t* head, point_t p)
{
	linknode_t *currentNode;
	linknode_t *newNode = create_new_point_node(p);
	
	currentNode = head->next;
	while (currentNode != NULL)
	{
		if (newNode->point.x == currentNode->point.x)
		{
			delete_point_node(head, newNode->point.x);
			break;
		}
		currentNode = currentNode->next;
	}
	currentNode = head->next;
	
	while (newNode->point.x > currentNode->point.x)
	{

		if (newNode->point.x < currentNode->next->point.x)
		{
			break;
		}
		currentNode = currentNode->next;
	}
	newNode->next = currentNode->next;
	currentNode->next = newNode;
}

void delete_at_begining(linknode_t *head)
{
	linknode_t *Current;

	Current = head->next;
	head->next = head->next->next;
	free(Current);
	Current = NULL;
}

void delete_at_end(linknode_t *head)
{
	linknode_t *Current;
	linknode_t *Temp;

	Current = head;
	Temp = head->next;
	while (Temp->next != NULL)
	{
		Temp = Temp->next;
		Current = Current->next;
	}
	Current->next = NULL;
	free(Temp);
	Temp = NULL;
}

void delete_point_node(linknode_t *head, float X)
{
	linknode_t *currentNode;
	linknode_t *temp;
	currentNode = head;
	while (X != currentNode->next->point.x)
	{
		currentNode = currentNode->next;
	}
	temp = currentNode->next;
	currentNode->next = temp->next;
	free(temp);
	temp = NULL;
}

void print_list(linknode_t* head)
{
	linknode_t* Current;
	Current = head->next;

	fprintf(gpfile, "\n\n*****printing point list*****\n\n");
	while (Current != NULL)
	{
		fprintf(gpfile,"(%f, %f)(%d)-->\n;", Current->point.x, Current->point.y, Current->point.number);
		Current = Current->next;
	}
	fprintf(gpfile,"NULL\n\n");
}

result_t delete_point_list(linknode_t *l)
{
	linknode_t *run, *temp;

	for (run = l->next; run != NULL; run = temp)
	{
		temp = run->next;
		delete_at_begining(run);
	}
	if (run == NULL)
	{
		free(l);
		l = NULL;
	}
	return(SUCCESS);
}
