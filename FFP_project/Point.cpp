#include"common.h"
#include"resource.h"

#pragma comment(lib, "Winmm.lib")

//macros
#define WIN_WIDTH 800
#define WIN_HEIGHT 600

//global variables for windowing
HWND			ghwnd			= NULL;
HDC				ghdc			= NULL;
HGLRC			ghrc			= NULL;
HINSTANCE		hInst			= NULL;
FILE			*gpfile			= NULL;
FILE			*tpfile			= NULL;
WINDOWPLACEMENT wpPrev			= { sizeof(WINDOWPLACEMENT) };
bool			gbActiveWindow	= false;
bool			gbFullScreen	= false;
bool			bLight			= true;
DWORD			dwStyle;

//global variables 
bool			plotpoints			= false;
bool			plotedges			= false;
bool			plot_shortest_path	= false;
bool			AnimatePoints		= false;
bool			AnimateEdges		= false;
bool			AnimateSD			= false;
bool			AnimatePath			= false;
bool			showSource			= true;
bool			showDestination		= false;
bool			ShowPathWeights		= false;
int				id_point			= 0;
GLfloat			camera_y			= 0.0f;
GLfloat			fontAngleS			= 90.0f;
GLfloat			fontAngleD			= 90.0f;
unsigned int	listbase, listbase2;
long long int	counter				= 0;
GLYPHMETRICSFLOAT gmf[256];

linknode_t		*plisthead, *run, *temprun;
graph_t			*graphhead;
edgenode_t		*edgehead, *temp_edge_run, *erun;
point_t			p_source, p_destination;
point_t			shortest_path[MAX], ab[MAX];

GLUquadric      *quadric_sphere[MAX];
GLUquadric      *quadric_source_sphere;
GLUquadric      *quadric_destination_sphere;
GLUquadric		*newq				= NULL;

extern int		path[MAX];
extern int		index;

//lights properties
GLfloat			lightAmbient[4]		= { 0.0f,0.0f,0.0f,1.0f };
GLfloat			lightDiffuse[4]		= { 1.0f,1.0f,1.0f,1.0f };
GLfloat			lightSpecular1[4]	= { 1.0f,1.0f,1.0f,1.0f };
GLfloat			lightSpecular2[4]	= { 0.5f,0.5f,0.5f,1.0f };
GLfloat			lightPosition1[4]	= { -6.0f,10.0f,0.0f,1.0f };
GLfloat			lightPosition2[4]	= { 5.0f,10.0f,0.0f,1.0f };

//material properties
GLfloat			MaterialAmbient[4]	= { 0.5f,0.5f,0.5f,1.0f };
GLfloat			MaterialDiffuse[4]	= { 1.0f,1.0f,1.0f,1.0f };
GLfloat			MaterialSpecular[4]	= { 1.0f,1.0f,1.0f,1.0f };
GLfloat			MaterialShininess[] = { 50.0f };

//global function declarations
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

//WinMain()
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{
	//function declarations
	int initialize(void);
	void display(void);
	void update(void);
	void ToggleFullScreen(void);
	//void update(void);

	//variable declarations
	WNDCLASSEX	wndclass;
	HWND		hwnd;
	MSG			msg;
	TCHAR		szAppName[] = TEXT("OGLSkeleton");
	bool		bDone		= false;
	int			iRet		= 0;

	//code
	if (fopen_s(&gpfile, "log.txt", "w") != 0)
	{
		MessageBox(NULL, TEXT("Log file cannot be created"), TEXT("Erroe"), MB_OK);
		exit(0);
	}
	if (fopen_s(&tpfile, "triangleslog.txt", "w") != 0)
	{
		MessageBox(NULL, TEXT("Log file cannot be created"), TEXT("Erroe"), MB_OK);
		exit(0);
	}
	//initialization of wndclass structure
	wndclass.cbSize = sizeof(WNDCLASSEX);
	wndclass.cbWndExtra = 0;
	wndclass.cbClsExtra = 0;
	wndclass.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
	wndclass.lpfnWndProc = WndProc;
	wndclass.hInstance = hInstance;
	wndclass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	wndclass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndclass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndclass.lpszClassName = szAppName;
	wndclass.lpszMenuName = NULL;
	wndclass.hIconSm = LoadIcon(hInstance, IDI_APPLICATION);

	//register above class 
	RegisterClassEx(&wndclass);

	//create window
	hwnd = CreateWindowEx(WS_EX_APPWINDOW,
		szAppName,
		TEXT("Points - PriyaSaboo"),
		WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN | WS_CLIPSIBLINGS | WS_VISIBLE,
		100,
		100,
		WIN_WIDTH,
		WIN_HEIGHT,
		NULL,
		NULL,
		hInstance,
		NULL);

	ghwnd = hwnd;
	iRet = initialize();
	if (iRet == -1)
	{
		fprintf(gpfile, "ChoosePixelFormat() Failed\n");
		DestroyWindow(hwnd);
	}
	else if (iRet == -2)
	{
		fprintf(gpfile, "SetPixelFormat() Failed\n");
		DestroyWindow(hwnd);
	}
	else if (iRet == -3)
	{
		fprintf(gpfile, "wglCreateContext() Failed\n");
		DestroyWindow(hwnd);
	}
	else if (iRet == -4)
	{
		fprintf(gpfile, "wglMakeCurrent() Failed\n");
		DestroyWindow(hwnd);
	}
	else
		fprintf(gpfile, "Initialize() Succeded\n");

	//show window
	ToggleFullScreen();
	ShowWindow(hwnd, iCmdShow);
	SetForegroundWindow(hwnd);
	SetFocus(hwnd);

	//gameloop
	while (bDone == false)
	{
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			if (msg.message == WM_QUIT)
			{
				bDone = true;
			}
			else
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}
		else
		{
			if (gbActiveWindow == true)
			{
				//update should be called here
				update();
			}
			display();
		}
	}
	return((int)msg.wParam);
}

//callback function WndProc()
LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	//function declaration
	void resize(int, int);
	void ToggleFullScreen(void);
	void uninitialize(void);
	linknode_t * GenerateRandomPoints(void);
	void AddPoint(void);

	//variables
	static GLfloat width, height;

	//code
	switch (iMsg)
	{
	case WM_SETFOCUS:
		gbActiveWindow = true;
		break;

	case WM_KILLFOCUS:
		gbActiveWindow = false;
		break;

	case WM_ERASEBKGND:
		return(0);

	case WM_SIZE:
		resize(LOWORD(lParam), HIWORD(lParam));
		break;

	case WM_CLOSE:
		DestroyWindow(hwnd);
		break;

	case WM_KEYDOWN:
		switch (LOWORD(wParam))
		{
		case VK_ESCAPE:
			DestroyWindow(hwnd);
			break;
		}
		break;
	case WM_CHAR:
		switch (LOWORD(wParam))
		{
		case 'l':
		case 'L':
			if (!bLight)
			{
				bLight = true;
				glEnable(GL_LIGHTING);
			}
			else
			{
				bLight = false;
				glDisable(GL_LIGHTING);
			}
			break;

		case 'y':
			camera_y = camera_y + 0.2f;
			break;
	
		case 'Y':
			camera_y = camera_y - 0.2f;
			break;

		case 'p':
			plotpoints = true;
			AnimatePoints = true;
			break;
		}
		break;

	case WM_DESTROY:
		uninitialize();
		PostQuitMessage(0);
		break;
	}
	return(DefWindowProc(hwnd, iMsg, wParam, lParam));
}

//ToggleFullScreen() function
void ToggleFullScreen(void)
{
	MONITORINFO mi;
	if (gbFullScreen == false)
	{
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);

		if (dwStyle & WS_OVERLAPPEDWINDOW)
		{
			mi = { sizeof(MONITORINFO) };
			if (GetWindowPlacement(ghwnd, &wpPrev) && GetMonitorInfo(MonitorFromWindow(ghwnd, MONITORINFOF_PRIMARY), &mi))
			{
				SetWindowLong(ghwnd, GWL_STYLE, dwStyle & ~WS_OVERLAPPEDWINDOW);
				SetWindowPos(ghwnd,
					HWND_TOP,
					mi.rcMonitor.left,
					mi.rcMonitor.top,
					mi.rcMonitor.right - mi.rcMonitor.left,
					mi.rcMonitor.bottom - mi.rcMonitor.top,
					SWP_NOZORDER | SWP_FRAMECHANGED);
			}
		}
		ShowCursor(FALSE);
		gbFullScreen = true;
	}
}

//initialize() function
int initialize(void)
{
	//function declarations
	void resize(int, int);
	linknode_t *GenerateRandomPoints(void);
	unsigned int CreateOutlineFont(char *, int, float);

	//vaiable declaraions
	PIXELFORMATDESCRIPTOR pfd;
	int iPixelFormatIndex;
	char fontStyle[] = "Arial";
	char fontStyle2[] = "Sitka Banner";

	//code
	//initialize pfd structure
	ZeroMemory(&pfd, sizeof(PIXELFORMATDESCRIPTOR));
	pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
	pfd.nVersion = 1;
	pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
	pfd.iPixelType = PFD_TYPE_RGBA;
	pfd.cColorBits = 32;
	pfd.cRedBits = 8;
	pfd.cGreenBits = 8;
	pfd.cBlueBits = 8;
	pfd.cAlphaBits = 8;

	ghdc = GetDC(ghwnd);
	iPixelFormatIndex = ChoosePixelFormat(ghdc, &pfd);
	if (iPixelFormatIndex == 0)
	{
		return(-1);
	}
	if (SetPixelFormat(ghdc, iPixelFormatIndex, &pfd) == false)
	{
		return(-2);
	}
	ghrc = wglCreateContext(ghdc);
	if (ghrc == NULL)
	{
		return(-3);
	}
	if (wglMakeCurrent(ghdc, ghrc) == false)
	{
		return(-4);
	}

	glShadeModel(GL_SMOOTH);

	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);	//r,g,b,a
	
	//depth related initialization
	glClearDepth(1.0f);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);
	glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);

	//lights related initialization
	glEnable(GL_AUTO_NORMAL);
	glEnable(GL_NORMALIZE);

	glLightfv(GL_LIGHT0, GL_AMBIENT, lightAmbient);
	glLightfv(GL_LIGHT0, GL_DIFFUSE, lightDiffuse);
	glLightfv(GL_LIGHT0, GL_SPECULAR, lightSpecular1);
	glLightfv(GL_LIGHT0, GL_POSITION, lightPosition1);

	glLightfv(GL_LIGHT1, GL_AMBIENT, lightAmbient);
	glLightfv(GL_LIGHT1, GL_DIFFUSE, lightDiffuse);
	glLightfv(GL_LIGHT1, GL_SPECULAR, lightSpecular2);
	glLightfv(GL_LIGHT1, GL_POSITION, lightPosition2);

	glMaterialfv(GL_FRONT, GL_AMBIENT, MaterialAmbient);
	glMaterialfv(GL_FRONT, GL_DIFFUSE, MaterialDiffuse);
	glMaterialfv(GL_FRONT, GL_SHININESS, MaterialShininess);
	glMaterialfv(GL_FRONT, GL_SPECULAR, MaterialSpecular);
	glEnable(GL_LIGHTING);
	glEnable(GL_LIGHT0);

	//creating font
	listbase = CreateOutlineFont(fontStyle, 2, 0.25f);

	//delaunay functions
	plisthead = create_point_list();
	graphhead = initialize_delaunay();
	srand((unsigned int)time(0));
	plisthead = GenerateRandomPoints();
	triangulate(graphhead, plisthead);
	edgehead = delaunay_valid_edges(graphhead);
	create_adjacency_matrix(edgehead, plisthead);
	dijkstra(MAX, MAX - 1, 3);
	
	temprun = plisthead->next;
	temp_edge_run = edgehead->next;
	erun = edgehead->next;

	for (int i = 0; i < MAX; i++)
	{
		quadric_sphere[i] = gluNewQuadric();
	}

	//wramup call to resize
	resize(WIN_WIDTH, WIN_HEIGHT);
	return(0);
}

void update(void)
{
	if (AnimatePoints == true)
	{
		if (plotpoints && (counter % 500 == 0))
			if (temprun != NULL)
				temprun = temprun->next;
		if (!temprun)
		{
			AnimateEdges = true;
			plotedges = true;
		}
	}

	if (AnimateEdges == true)
	{
		if (plotedges && (counter % 100 == 0))
			if (temp_edge_run != NULL)
				temp_edge_run = temp_edge_run->next;
		if (!temp_edge_run)
		{
			ShowPathWeights = true;
			AnimateSD = true;
		}
	}

	if (AnimateSD)
	{
		if (showSource)
		{
			if (fontAngleS < 360.0f)
				fontAngleS = fontAngleS + 1.0f;
			if (fontAngleS >= 360.0f)
				fontAngleS = 361.0f;
		}
		if (showDestination)
		{
			if (fontAngleD < 360.0f)
				fontAngleD = fontAngleD + 1.0f;
			if (fontAngleD >= 360.0f)
				fontAngleD = 361.0f;
		}
	}
	
	counter++;
}

//display() function
void display(void)
{
	//function declaration
	void plotThePoints(linknode_t*);
	void draw_fill_triangles(vertex_triangle_t *);
	void plotlines(void);
	void mark_source_and_destination(linknode_t *);
	void PrintString(unsigned int, char *);
	void ShowWeights(edgenode_t *);


	//variable declarations
	char	string1[] = "Source";
	char	string2[] = "Destination";
	bool	bString1 = false;
	static	GLint j = 0;
	GLfloat stepx = 0.0f, stepy = 0.0f;

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	gluLookAt(0.0f, camera_y, 25, 0.0f, 0.0f, 0.0f, 0.0f, 1.0f, 0.0f);
	hInst = (HINSTANCE)GetWindowLongPtr(ghwnd, GWLP_HINSTANCE);
	//GLfloat color[] = { 1.0f,1.0f,0.0f ,1.0f};
	
	if (AnimatePoints)
	{
		if (plotpoints && counter % 500)
		{
			plotThePoints(temprun);
		}
		else
			plotThePoints(plisthead);
	}
	
	if (AnimateEdges)
	{
		if (plotedges && counter % 100)
		{
			draw_delaunay(temp_edge_run);
		}
		else
		{
			draw_delaunay(edgehead);
		}
	}

	if (AnimateSD)
	{
		GLfloat fdx = p_destination.x, fdy = p_destination.y, fdz = 1.0f;
		static GLfloat idx = 0.0f, idy = 0.0f, idz = 18.0f;
		static GLfloat isx = 0.0f, isy = 0.0f, isz = 18.0f;
		GLfloat stepx, stepy, stepz;

		mark_source_and_destination(plisthead);

		MaterialDiffuse[0] = 0.0f;
		MaterialDiffuse[1] = 0.780f;
		MaterialDiffuse[2] = 0.70f;
		MaterialDiffuse[3] = 1.0f;
		glMaterialfv(GL_FRONT, GL_DIFFUSE, MaterialDiffuse);
		//glEnable(GL_COLOR_MATERIAL);

		if (showSource && fontAngleS <360.0f)
		{
			glLoadIdentity();
			gluLookAt(0.0f, camera_y, 25.0f, 0.0f, 0.0f, 0.0f, 0.0f, 1.0f, 0.0f);
			glTranslatef(idx, idy, idz);
			glColor3f(0.0f, 1.0f, 1.0f);
			glRotatef(fontAngleS, 1.0f, 0.0f, 0.0f);
			PrintString(listbase, string1);
		}
		else if (showSource && fontAngleS >= 360.0f)
		{
			stepx = (12.0f - 0.0f) / 1000;	stepy = (-9.0f - 0.0f) / 1000;	stepz = (1.0f - 18.0f)/1000;

			if (isx < 13.0f)
			{
				fprintf(gpfile, "check 21\n");

				isx = isx + stepx;
				isy = isy + stepy;
				isz = isz + stepz;
				glLoadIdentity();
				gluLookAt(0.0f, camera_y, 25.0f, 0.0f, 0.0f, 0.0f, 0.0f, 1.0f, 0.0f);
				glTranslatef(isx, isy, isz);
				glColor3f(0.0f, 0.0f, 1.0f);
				glRotatef(fontAngleS, 1.0f, 0.0f, 0.0f);
				PrintString(listbase, string1);
			}
			else if (isx >= 13.0f)
			{
				showSource = false;
				showDestination = true;
			}
		}
		else if (!showSource && fontAngleS > 360.0f)
		{
			glLoadIdentity();
			gluLookAt(0.0f, camera_y, 25.0f, 0.0f, 0.0f, 0.0f, 0.0f, 1.0f, 0.0f);
			glTranslatef(12.0f, -9.0f, 1.0f);
			glColor3f(1.0f, 0.0f, 1.0f);
			glRotatef(fontAngleS, 1.0f, 0.0f, 0.0f);
			PrintString(listbase, string1);
		}

		//for destination
		MaterialDiffuse[0] = 1.0f;
		MaterialDiffuse[1] = 0.3f;
		MaterialDiffuse[2] = 1.0f;
		MaterialDiffuse[3] = 1.0f;
		glMaterialfv(GL_FRONT, GL_DIFFUSE, MaterialDiffuse);
		if (showDestination && fontAngleD < 360.0f)
		{
			idx = 0.0f, idy = 0.0f, idz = 18.0f;
			glLoadIdentity();
			gluLookAt(0.0f, camera_y, 25.0f, 0.0f, 0.0f, 0.0f, 0.0f, 1.0f, 0.0f);
			glTranslatef(idx, idy, idz);
			glColor3f(1.0f, 0.0f, 0.0f);
			glRotatef(fontAngleD, 1.0f, 0.0f, 0.0f);
			PrintString(listbase, string2);
		}
		else if (showDestination && fontAngleD >= 360.0f)
		{
			stepx = (fdx - 0.0f) / 1000;	stepy = (fdy - 0.0f) / 1000;	stepz = (2.0f - 18.0f) / 1000;

			if (idz > 2.0f)
			{
				idx = idx + stepx;
				idy = idy + stepy;
				idz = idz + stepz;
				glLoadIdentity();
				gluLookAt(0.0f, camera_y, 25.0f, 0.0f, 0.0f, 0.0f, 0.0f, 1.0f, 0.0f);
				glTranslatef(idx, idy, idz);
				glColor3f(1.0f, 0.0f, 0.0f);
				glRotatef(360.0f, 1.0f, 0.0f, 0.0f);
				PrintString(listbase, string2);
			}
			else if (!(idz>2.0f))
			{
				showDestination = false;
			}
		}
		else if (!showDestination && fontAngleD > 360.0f)
		{
			glLoadIdentity();
			gluLookAt(0.0f, camera_y, 25.0f, 0.0f, 0.0f, 0.0f, 0.0f, 1.0f, 0.0f);
			glTranslatef(fdx, fdy, fdz);
			glColor3f(1.0f, 0.0f, 0.0f);
			glRotatef(fontAngleS, 1.0f, 0.0f, 0.0f);
			PrintString(listbase, string2);
			plot_shortest_path = true;
		}
			
		if (plot_shortest_path)
		{
			plotlines();
			AnimatePath = true;
		}
	}
	
	if (AnimatePath)
	{
		static GLfloat trans_x = ab[0].x;
		static GLfloat trans_y = ab[0].y;
		static bool marknode_green = false;

		stepx = (ab[j + 1].x - ab[j].x) / 1000;
		stepy = (ab[j + 1].y - ab[j].y) / 1000;

		glEnable(GL_LIGHTING);

		newq = gluNewQuadric();
		GLfloat MaterialDiffuse[] = { 0.0f,1.0f,0.0f,1.0f };
		glMaterialfv(GL_FRONT, GL_DIFFUSE, MaterialDiffuse);
		glLoadIdentity();
		gluLookAt(0.0f, camera_y, 25, 0.0f, 0.0f, 0.0f, 0.0f, 1.0f, 0.0f);
		glTranslatef(trans_x, trans_y, 0.0f);
		glColor3f(1.0f, 1.0f, 1.0f);
		gluSphere(newq, 0.5f, 30, 30);

		if (!((trans_x > (ab[j + 1].x - 0.05f)) && (trans_x < (ab[j + 1].x + 0.05f))) 
			|| !((trans_y >= (ab[j + 1].y - 0.05f)) && (trans_y < (ab[j + 1].y + 0.05f))))
		{
			trans_x = trans_x + stepx;
			trans_y = trans_y + stepy;
		}
		else
		{
			trans_x = ab[j + 1].x;
			trans_y = ab[j + 1].y;
			if (j < index-2)
				j = j + 1;
			marknode_green = true;
		}
		if (marknode_green)
		{
			for (int i = 1; i <= j; i++)
			{
				glLoadIdentity();
				gluLookAt(0.0f, camera_y, 25, 0.0f, 0.0f, 0.0f, 0.0f, 1.0f, 0.0f);
				newq = gluNewQuadric();
				GLfloat MaterialDiffuse[] = { 0.0f,1.0f,1.0f,1.0f };
				glMaterialfv(GL_FRONT, GL_DIFFUSE, MaterialDiffuse);
				glTranslatef(ab[i].x, ab[i].y, 0.0f);
				glColor3f(1.0f, 1.0f, 1.0f);
				gluSphere(newq, 0.5f, 30, 30);
			}
		}
	}
	SwapBuffers(ghdc);
}

//reize() function
void resize(int win_width, int win_height)
{
	if (win_height == 0)
		win_height = 1;
	glViewport(0, 0, (GLsizei)win_width, (GLsizei)win_height);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluPerspective(45.0f, (GLfloat)win_width / (GLfloat)win_height, 0.1f, 100.0f);
}

void plotlines(void)
{
	linknode_t *run;

	glDisable(GL_LIGHTING);
	glLoadIdentity();
	gluLookAt(0.0f, camera_y, 25.0f, 0.0f, 0.0f, 0.0f, 0.0f, 1.0f, 0.0f);
	glPointSize(5.0f);
	for (int i = index, k = 0; i > 0, k < index; i--, k++)
	{
		run = plisthead->next;
		while (run->point.number != path[i])
			run = run->next;
		shortest_path[i] = run->point;
		ab[k] = shortest_path[i];
		fprintf(gpfile, "plotlines: ab[%d]=%f,%f\t", k, ab[k].x, ab[k].y);
	}
	shortest_path[0] = shortest_path[1];

	glLineWidth(10.0f);
	glBegin(GL_LINES);
	for (int i = index; i > 0; i--)
	{
		glColor3f(1.0f, 0.0f, 0.3f);
		glVertex3f(shortest_path[i].x, shortest_path[i].y, 0.0f);
		glVertex3f(shortest_path[i - 1].x, shortest_path[i - 1].y, 0.0f);
	}
	glEnd();
}

void mark_source_and_destination(linknode_t *head)
{
	glEnable(GL_LIGHTING);

	//mark source
	MaterialDiffuse[0] = 0.0f;
	MaterialDiffuse[1] = 0.0f;
	MaterialDiffuse[2] = 1.0f;
	MaterialDiffuse[3] = 1.0f;
	quadric_source_sphere = gluNewQuadric();
	glRotatef(90.0f, 1.0f, 0.0f, 0.0f);
	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	glMaterialfv(GL_FRONT, GL_DIFFUSE, MaterialDiffuse);
	glLoadIdentity();
	gluLookAt(0.0f, camera_y, 25.0f, 0.0f, 0.0f, 0.0f, 0.0f, 1.0f, 0.0f);
	glTranslatef(p_source.x,p_source.y, 0.0f);
	gluSphere(quadric_source_sphere, 0.5f, 30, 30);

	//mark destination
	MaterialDiffuse[0] = 1.0f;
	MaterialDiffuse[1] = 0.0f;
	MaterialDiffuse[2] = 0.0f;
	MaterialDiffuse[3] = 1.0f;
	quadric_destination_sphere = gluNewQuadric();
	glRotatef(90.0f, 1.0f, 0.0f, 0.0f);
	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	glMaterialfv(GL_FRONT, GL_DIFFUSE, MaterialDiffuse);
	glLoadIdentity();
	gluLookAt(0.0f, camera_y, 25.0f, 0.0f, 0.0f, 0.0f, 0.0f, 1.0f, 0.0f);
	glTranslatef(p_destination.x, p_destination.y, 0.0f);
	gluSphere(quadric_source_sphere, 0.5f, 30, 30);
}

void plotThePoints(linknode_t* h)
{
	//code
	run = plisthead->next;
	int i = 0;

	glEnable(GL_LIGHTING);
	while (run != temprun)
	{
		quadric_sphere[i] = gluNewQuadric();
		glRotatef(90.0f, 1.0f, 0.0f, 0.0f);
		glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
		GLfloat MaterialDiffuse[] = { 1.0f,0.5f,0.0f,1.0f };
		glMaterialfv(GL_FRONT, GL_DIFFUSE, MaterialDiffuse);
		glLoadIdentity();
		gluLookAt(0.0f, camera_y, 25.0f, 0.0f, 0.0f, 0.0f, 0.0f, 1.0f, 0.0f);
		
		glTranslatef(run->point.x, run->point.y, 0.0f);
		gluSphere(quadric_sphere[i], 0.5f, 30, 30);

		if (run->point.number == 3)
		{
			p_destination = run->point;
		}
		else if (run->point.number == MAX - 1)
		{
			p_source = run->point;
		}
		run = run->next;
		i++;
	}
}

void draw_fill_triangles(vertex_triangle_t * vhead)
{
	point_t v1, v2, v3;
	vertex_triangle_t *vrun = vhead->next;

	glLoadIdentity();
	glTranslatef(0.0f, 0.0f, -25.0f);
	glBegin(GL_TRIANGLES);
	for (vrun; vrun != vhead; vrun = vrun->next)
	{
		v1 = vrun->triangle.v[0];
		v2 = vrun->triangle.v[1];
		v3 = vrun->triangle.v[2];
		glColor3f(1.0f, 1.0f, 0.0f);
		glVertex3f(v1.x, v1.y, 0.0f);
		glColor3f(0.0f, 1.0f, 1.0f);
		glVertex3f(v2.x, v2.y, 0.0f);
		glColor3f(1.0f, 0.0f, 1.0f);
		glVertex3f(v3.x, v3.y, 0.0f);
	}
	glEnd();
}

linknode_t *GenerateRandomPoints(void)
{
	//	Variable declarations
	point_t P;
	float MinX, MaxX;
	//code
	for (int i = 0; i < RANDOM_POINTS; i++)
	{
		P.x = (GLfloat)(rand() % 1000) / 100 - (GLfloat)(rand() % 1000) / 100;
		P.y = (GLfloat)(rand() % 1000) / 100 - (GLfloat)(rand() % 1000) / 100;

		if (plisthead->next == NULL)
		{
			insert_at_beginning(plisthead, P);
			MinX = P.x;
			MaxX = P.x;
		}
		else
		{
			if ((P.x < MinX))
			{
				insert_at_beginning(plisthead, P);
				MinX = P.x;
			}
			else if ((P.x > MaxX))// && (current->next == NULL)
			{
				insert_at_end(plisthead, P);
				MaxX = P.x;
			}
			else if ((P.x < MaxX) && (P.x > MinX))
			{
				insert_after_node(plisthead, P);
			}
		}
	}
	insert_at_end(plisthead, { 10.0f,10.0f });		//top right corner
	insert_at_end(plisthead, { -10.0f,10.0f });		//top left corner
	insert_at_end(plisthead, { -10.0f,-10.0f });		//bottom left corner
	insert_at_end(plisthead, { 10.0f,-10.0f });		//bottom right corner
	return(plisthead);
}

unsigned int CreateOutlineFont(char *fontName, int fontSize, float depth)
{
	//function declaration
	void uninitialize(void);
	HFONT hFont;
	unsigned int base;

	base = glGenLists(256);
	if (strcmp(fontName, "symbol") == 0)
	{
		hFont = CreateFont(fontSize, 0, 0, 0, FW_BOLD, FALSE, FALSE, FALSE,
			SYMBOL_CHARSET, OUT_TT_PRECIS, CLIP_DEFAULT_PRECIS, ANTIALIASED_QUALITY,
			FF_DONTCARE | DEFAULT_PITCH, (LPCWSTR)fontName);

	}
	else
	{
		hFont = CreateFont(fontSize, 0, 0, 0, FW_BOLD, FALSE, FALSE, FALSE,
			ANSI_CHARSET, OUT_TT_PRECIS, CLIP_DEFAULT_PRECIS, ANTIALIASED_QUALITY,
			FF_DONTCARE | DEFAULT_PITCH, (LPCWSTR)fontName);
	}

	if (!hFont)
	{
		uninitialize();
		DestroyWindow(ghwnd);
		return(1);
	}
	SelectObject(ghdc, hFont);
	wglUseFontOutlines(ghdc, 0, 255, base, 0.0f, depth, WGL_FONT_POLYGONS, gmf);
}

void ClearFont(unsigned int base)
{
	glDeleteLists(base, 256);
}

void PrintString(unsigned int base, char *str)
{
	float length = 0;
	int idx;
	int len = (int)strlen(str);
	//center the text
	for (idx = 0; idx < len; idx++)
	{
		length += gmf[str[idx]].gmfCellIncX;
	}

	glTranslatef(-length / 2.0f, 0.0f, 0.0f);
	glColor3f(1.0f, 0.0f, 0.5f);
	//draw the text
	glPushAttrib(GL_LIST_BIT);
	glListBase(base);
	glCallLists((GLsizei)strlen(str), GL_UNSIGNED_BYTE, str);
	glPopAttrib();
}

//uninitialize() function
void uninitialize(void)
{
	if (newq)
	{
		gluDeleteQuadric(newq);
		newq = NULL;
	}

	if (quadric_source_sphere)
	{
		gluDeleteQuadric(quadric_source_sphere);
		quadric_source_sphere = NULL;
	}

	if (quadric_destination_sphere)
	{
		gluDeleteQuadric(quadric_destination_sphere);
		quadric_destination_sphere = NULL;
	}

	for (int i = 0; i < MAX; i++)
	{
		if (quadric_sphere)
		{
			gluDeleteQuadric(quadric_sphere[i]);
			quadric_sphere[i] = NULL;
		}
	}

	if (edgehead)
	{
		delete_edge_list(edgehead);
		edgehead = NULL;
	}

	if (graphhead)
	{
		delete_graph(graphhead);
		graphhead = NULL;
	}

	if (plisthead)
	{
		delete_point_list(plisthead);
		plisthead = NULL;
	}

	if (gbFullScreen == true)
	{
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPED);
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd,
			HWND_TOP,
			0,
			0,
			0,
			0,
			SWP_NOZORDER | SWP_FRAMECHANGED | SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER);
		ShowCursor(TRUE);
	}

	//break the current context
	if (wglGetCurrentContext() == ghrc)
	{
		wglMakeCurrent(NULL, NULL);
	}

	if (ghrc)
	{
		wglDeleteContext(ghrc);
		ghrc = NULL;
	}

	if (ghdc)
	{
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	if (gpfile)
	{
		fprintf(gpfile, "log file closed successfully\n");
		fclose(gpfile);
		gpfile = NULL;
	}
}
