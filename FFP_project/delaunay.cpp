#include"common.h"
#include<math.h>
#include<stdio.h>

extern GLfloat		camera_y;
extern FILE			*tpfile;
extern FILE			*gpfile;
extern linknode_t	*plisthead;
extern int			id_point;
extern edgenode_t	*erun;
extern edgenode_t	*edgehead, *temp_edge_run;
extern unsigned int listbase;

graph_t *initialize_delaunay(void)
{
	//variables
	graph_t *g_head = NULL;
	circle_t cir;
	triangle_t t;
	point_t top_right = { 10.0f,10.0f,RANDOM_POINTS };
	point_t top_left = { -10.0f,10.0f,RANDOM_POINTS + 1 };
	point_t bottom_left = { -10.0f,-10.0f,RANDOM_POINTS + 2 };
	point_t bottom_right = { 10.0f,-10.0f,RANDOM_POINTS + 3 };

	//create graph, edgebuffer and get respective head pointer
	g_head = create_graph();
	
	//set 1st triangle, calculate its cc properties and add it to vertex triangle list 
	t = set_triangle(top_right, top_left, bottom_left, cir);
	cir = cc_properties(t);
	if (cir.isvalid != POINTS_COLINEAR)
		t.c = cir;
	fprintf(tpfile, "***1st triangle***\n\n");
	fprintf(tpfile, "vertex a: {%f,%f}\n", t.v[0].x, t.v[0].y);
	fprintf(tpfile, "vertex b: {%f,%f}\n", t.v[1].x, t.v[1].y);
	fprintf(tpfile, "vertex c: {%f,%f}\n", t.v[2].x, t.v[2].y);
	fprintf(tpfile, "radius: %f\n", t.c.radius);
	fprintf(tpfile, "center: %f, %f\n", t.c.center.x,t.c.center.y);
	add_vertex_triangle(g_head, t);
	
	// set 2nd triangle, calculate its cc properties and add it to vertex triangle list
	t = set_triangle(top_right,bottom_left, bottom_right, cir);
	cir = cc_properties(t);
	if (cir.isvalid != POINTS_COLINEAR)
		t.c = cir;
	fprintf(tpfile, "\n\n***2nd triangle***\n\n");
	fprintf(tpfile, "vertex a: {%f,%f}\n", t.v[0].x, t.v[0].y);
	fprintf(tpfile, "vertex b: {%f,%f}\n", t.v[1].x, t.v[1].y);
	fprintf(tpfile, "vertex c: {%f,%f}\n", t.v[2].x, t.v[2].y);
	fprintf(tpfile, "radius: %f\n", t.c.radius);
	fprintf(tpfile, "center: %f, %f\n", t.c.center.x, t.c.center.y); add_vertex_triangle(g_head, t);
	return(g_head);

}

graph_t *triangulate(graph_t *g_head, linknode_t *pl_head)
{
	//variables
	point_t point;
	triangle_t t;
	circle_t cc;
	bool check = false;

	edgenode_t			*eb_head = NULL;		//edge buffer
	graph_t				*gh		 = g_head;
	vertex_triangle_t	*vhead	 = gh->head;

	edgenode_t			*erun;
	linknode_t			*lrun	 = pl_head->next;
	vertex_triangle_t	*vrun , *temp;

	
	for (lrun; lrun != NULL; lrun = lrun->next)
	{
		eb_head = create_edge_list();
		erun = eb_head->next;
		vrun = vhead->next;
		temp = vhead;
		point = lrun->point;
		//check if point lies inside circum circle of triangles in vertex list
		for (vrun; vrun != vhead; vrun = temp->next)
		{
			t = vrun->triangle;
			cc = cc_properties(t);
			check = check_point(point, cc);
			if (check)
			{
				vrun->triangletype = INVALID_TRIANGLE;
				//store edges of that triangle to edge buffer
				store_edges_to_edgebuffer(eb_head, t);
				//delete that vertex_triangle from vertex list
				delete_vertex_node(vrun);
			}
			else
			{
				vrun->triangletype = VALID_TRIANGLE;
				temp = vrun;
			}
		}
		check_duplicate_edges_and_delete(eb_head);
		//creat new triangle from point and that edge.
		for (erun = eb_head->next; erun != NULL; erun = erun->next)
		{
			t = set_triangle(point, erun->start, erun->end, cc);
			t.c = cc_properties(t);
			add_vertex_triangle(g_head, t);
		}
		delete_edge_list(eb_head);
	}
	return(g_head);
}

void check_duplicate_edges_and_delete(edgenode_t *edgehead)
{
	edgenode_t *erun = edgehead->next;
	edgenode_t *temp = edgehead;
	
	for (erun = edgehead->next; erun != NULL; erun = temp->next)
	{
		bool isDeleted = delete_duplicate_edges(edgehead, erun);
		if (!isDeleted) {
			temp = erun;
		}
	}
}


edgenode_t *delaunay_valid_edges(graph_t *g)
{
	edgenode_t *e_head;
	vertex_triangle_t *run_vnode, *vhead;
	int count_edges = 0;

	vhead = g->head;

	e_head = create_edge_list();
	
	for (run_vnode = vhead->next; run_vnode != vhead; run_vnode = run_vnode->next)
	{
		if (run_vnode->triangletype == VALID_TRIANGLE)
		{
			for (int i = 0; i < 3; i++)
			{
				insert_edgenode_at_beginning(e_head, run_vnode->triangle.e[i].start, run_vnode->triangle.e[i].end);
			}
		}
	}
	keep_valid_edges(e_head);

	/*for (erun = e_head->next; erun != NULL; erun = erun->next)
	{
		fprintf(gpfile, "edge: start(%f,%f,%d)-end(%f,%f,%d)\n", erun->start.x, erun->start.y, erun->start.number,
			erun->end.x, erun->end.y, erun->end.number);
	}*/
	return(e_head);
}


circle_t cc_properties(triangle_t t)
{
	circle_t cir;
	point_t a = t.v[0], b = t.v[1], c = t.v[2];
	float ab0, ab1, cb0, cb1, det, absq, cbsq, ctr0, ctr1, r;
	ab0 = a.x - b.x;	ab1 = a.y - b.y;
	cb0 = c.x - b.x;	cb1 = c.y - b.y;
	det = ab0 * cb1 - cb0 * ab1;
	if (det == 0)
	{
		cir.isvalid = POINTS_COLINEAR;
		return(cir);
	}
	det = 0.5f / det;
	absq = ab0 * ab0 + ab1 * ab1;
	cbsq = cb0 * cb0 + cb1 * cb1;
	ctr0 = det * (absq*cb1 - cbsq * ab1);
	ctr1 = det * (cbsq*ab0 - absq * cb0);
	r = ctr0 * ctr0 + ctr1 * ctr1;
	cir.center = { ctr0 + b.x, ctr1 + b.y };
	cir.radius = (float)sqrt(r);
	return(cir);
}

float dist(point_t p, point_t q)
{
	float d = 0.0f;
	float temp_p[2] = { p.x,p.y };
	float temp_q[2] = { q.x,q.y };
	for (int i = 0; i < 2; i++)
	{
		d += (float)pow(temp_p[i] - temp_q[i], 2);
	}
	return((float)sqrt(d));
}


bool check_point(point_t point, circle_t cc_circle)
{
	float p_distance;

	p_distance = dist(point, cc_circle.center);
	if (p_distance < cc_circle.radius)
		return(true);
	else
		return(false);
}

void draw_delaunay(edgenode_t *h)
{
	void PrintString(unsigned int, char *);
	erun = edgehead->next;
	char pathBuffer[10] = "hello";
	float pathWeight;

	for (erun; erun != temp_edge_run; erun = erun->next)
	{
		GLfloat x1 = erun->start.x, y1 = erun->start.y;
		GLfloat x2 = erun->end.x, y2 = erun->end.y;

		glLoadIdentity();
		gluLookAt(0.0f, camera_y, 25.0f, 0.0f, 0.0f, 0.0f, 0.0f, 1.0f, 0.0f);
		glLineWidth(3.0f);
		glColor3f(0.5f, 0.5f, 0.5f);
		glDisable(GL_LIGHTING);
		glLineWidth(3.0f);
		glBegin(GL_LINES);
		glNormal3f(1.0f, 1.0f, 0.0f);
		glVertex3f(x1, y1, 0.0f);
		glVertex3f(x2, y2, 0.0f);
		glEnd();

		glEnable(GL_LIGHTING);
		GLfloat fontColorDiffuse[] = { 1.0f,1.0f,1.0f,1.0f };
		glMaterialfv(GL_FRONT, GL_DIFFUSE, fontColorDiffuse);
		pathWeight = dist(erun->end, erun->start);
		_gcvt_s(pathBuffer, sizeof(pathBuffer), pathWeight, 3);
		glLoadIdentity();
		glTranslatef((erun->start.x + erun->end.x) / 2, (erun->start.y + erun->end.y) / 2, -25.0f);
		glScalef(0.5f, 0.5f, 0.5f);
		PrintString(listbase, pathBuffer);

	}
}

void ShowWeights(edgenode_t *ehead)
{
	void PrintString(unsigned int, char *);

	GLfloat pathWeight;
	char pathBuffer[10];

	erun = ehead->next;

	for (erun; erun != NULL; erun->next)
	{
		pathWeight = dist(erun->end, erun->start);
		_gcvt_s(pathBuffer, sizeof(pathBuffer), pathWeight, 6);
		glLoadIdentity();
		glTranslatef((erun->start.x + erun->end.x) / 2, (erun->start.y + erun->end.y) / 2, -35.0f);
		PrintString(listbase, pathBuffer);
	}
}

//bool point_in_triangle(triangle_t t, point_t p)
//{
//	float area_a, area_b, area_c, area_T;
//	area_a = calculate_area(p, t.v[0], t.v[1]);
//	area_b = calculate_area(p, t.v[1], t.v[2]);
//	area_c = calculate_area(p, t.v[2], t.v[3]);
//	area_T = calculate_area(t.v[0], t.v[1], t.v[2]);
//	fprintf(tpfile, "area_a = %f \n area_b = %f\n, area_c = %f\n", area_a, area_b, area_c);
//	fprintf(tpfile, "total area = %f\tarea_T = %f\n", area_a + area_b + area_c, area_T);
//	if (area_a + area_b + area_c == area_T)
//		return(true);
//	else
//		return(false);
//}

//float calculate_area(point_t a, point_t b, point_t c)
//{
//	float x1 = a.x, x2 = b.x, x3 = c.x;
//	float y1 = a.y, y2 = b.y, y3 = c.y;
//	
//	float A = (x1*(y2 - y3) + x2 * (y3 - y1) + x3 * (y1 - y2));
//
//	return(A);
//}

//void check_vertex_triangle_list(graph_t *g)
//{
//	bool check;
//	point_t current;
//	vertex_triangle_t *run;
//	vertex_triangle_t *vhead = g->head;
//	current = plisthead->next->point;
//
//	for (run = vhead->next; run != vhead; run = run->next)
//	{
//		check = point_in_triangle(run->triangle, current);
//		if (check)
//			run->triangletype = INVALID_TRIANGLE;
//		else
//			run->triangletype = VALID_TRIANGLE;
//	}
//}
