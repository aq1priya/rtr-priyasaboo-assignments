#include <windows.h>
#include <stdio.h>

#include <d3d11.h>					// D3D11 header file
#include <d3dcompiler.h>			// shader compilation

#pragma warning( disable : 4838)
#include "XNAMath\xnamath.h"
#include "Sphere.h"

#pragma comment (lib, "d3d11.lib")	// D3d11 library
#pragma comment (lib, "D3dcompiler.lib")	// header compilation library
#pragma comment (lib, "Sphere.lib")			// header for sphere

#define WIN_WIDTH 800
#define WIN_HEIGHT 600

// global function declarations
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

// global variables
FILE *gpfile_ps = NULL;
char gszLogFileName_ps[] = "log.txt";

HWND ghwnd_ps = NULL;
DWORD dwStyle_ps;
WINDOWPLACEMENT wpPrev_ps = { sizeof(WINDOWPLACEMENT) };

bool gbActiveWindow_ps = false;
bool gbEscapeKeyIsPressed_ps = false;
bool gbFullScreen_ps = false;
bool gbLight_ps = false;

float gClearColor[4];	// Array to hold color values as glClearColor in directX takes array as a parameter
float sphere_vertices[1146];
float sphere_normals[1146];
float sphere_textures[764];
unsigned short sphere_elements[2280];
unsigned int gNumElements;
unsigned int gNumVertices;

IDXGISwapChain *gpIDXGISwapChain_ps = NULL;
ID3D11Device *gpID3D11Device_ps = NULL;
ID3D11DeviceContext *gpID3D11DeviceContext_ps = NULL;
ID3D11RenderTargetView *gpID3D11RenderTargetView_ps = NULL;
ID3D11VertexShader *gpID3D11VertexShader = NULL;
ID3D11PixelShader *gpID3D11PixelShader = NULL;
ID3D11Buffer *gpID3D11Buffer_VertexBuffer_Position_Sphere = NULL;
ID3D11Buffer *gpID3D11Buffer_VertexBuffer_Texture_Sphere = NULL;
ID3D11Buffer *gpID3D11Buffer_VertexBuffer_Normal_Sphere = NULL;
ID3D11Buffer *gpID3D11Buffer_IndexBuffer = NULL;

ID3D11InputLayout *gpID3D11InputLayout = NULL;
ID3D11Buffer *gpID3D11Buffer_ConstantBuffer = NULL;

ID3D11RasterizerState *gpID3D11RasterizerState = NULL;
ID3D11DepthStencilView *gpID3D11DepthStencilView = NULL;

struct CBUFFER
{
	XMMATRIX WorldMatrix;
	XMMATRIX ViewMatrix;
	XMMATRIX ProjectionMatrix;
	XMVECTOR La;
	XMVECTOR Ld;
	XMVECTOR Ls;
	XMVECTOR Ka;
	XMVECTOR Kd;
	XMVECTOR Ks;
	XMVECTOR LightPosition;
	float MaterialShininess;
	unsigned int KeyPressed;
};

float LightAmbient[4] = { 0.0f, 0.0f, 0.0f, 1.0f };
float LightDiffuse[4] = { 1.0f, 1.0f, 1.0f, 1.0f };
float LightSpecular[4] = { 1.0f, 1.0f, 1.0f, 1.0f };
float LightPosition[4] = { 100.0f, 100.0f, -100.0f, 1.0f };
float MaterialAmbient[4] = { 0.0f, 0.0f, 0.0f, 1.0f };
float MaterialDiffuse[4] = { 1.0f, 1.0f, 1.0f, 1.0f };
float MaterialSpecular[4] = { 1.0f, 1.0f, 1.0f, 1.0f };
float Shininess = 50.0f;

XMMATRIX gPerspectiveProjectionMatrix;

// WinMain
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{
	// function declarations
	HRESULT initialize(void);
	void uninitialize(void);
	void display(void);
	void update(void);

	// variable declarations
	WNDCLASSEX wndClass_ps;
	HWND hwnd_ps;
	MSG msg_ps;
	TCHAR szClassName_ps[] = TEXT("Direct3D11");
	bool bDone_ps = false;

	// code
	// create log file
	if (fopen_s(&gpfile_ps, gszLogFileName_ps, "w") != 0)
	{
		MessageBox(NULL, TEXT("Log File Cannot Be Created\n, Exitting From Application\n"), TEXT("ERROR!!!"), MB_OK | MB_TOPMOST | MB_ICONSTOP);
		exit(0);
	}
	else
	{
		fprintf(gpfile_ps, "Log File is Created Successfully\n");
		fclose(gpfile_ps);
	}

	// initialize WNDCLASSEX structure
	wndClass_ps.cbSize = sizeof(WNDCLASSEX);
	wndClass_ps.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
	wndClass_ps.cbClsExtra = 0;
	wndClass_ps.cbWndExtra = 0;
	wndClass_ps.lpfnWndProc = WndProc;
	wndClass_ps.lpszClassName = szClassName_ps;
	wndClass_ps.lpszMenuName = NULL;
	wndClass_ps.hInstance = hInstance;
	wndClass_ps.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndClass_ps.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndClass_ps.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	wndClass_ps.hIconSm = LoadIcon(NULL, IDI_APPLICATION);

	// register WNDCLASSEX structure
	RegisterClassEx(&wndClass_ps);

	// create window
	hwnd_ps = CreateWindow(szClassName_ps,
		TEXT("Direct3D11 Window"),
		WS_OVERLAPPEDWINDOW,
		100,
		100,
		WIN_WIDTH,
		WIN_HEIGHT,
		NULL,			// parent Handle
		NULL,			// menu name
		hInstance,		// hInstance
		NULL);

	ghwnd_ps = hwnd_ps;

	ShowWindow(hwnd_ps, iCmdShow);
	SetForegroundWindow(hwnd_ps);
	SetFocus(hwnd_ps);

	// initialize D3D
	HRESULT hr;
	hr = initialize();
	if (FAILED(hr))
	{
		fopen_s(&gpfile_ps, gszLogFileName_ps, "a+");
		fprintf_s(gpfile_ps, "initialize() failed. Exitting Now...\n");
		fclose(gpfile_ps);
		DestroyWindow(hwnd_ps);
		hwnd_ps = NULL;
	}
	else
	{
		fopen_s(&gpfile_ps, gszLogFileName_ps, "a+");
		fprintf_s(gpfile_ps, "initialize() Successful.\n");
		fclose(gpfile_ps);
	}

	// message loop
	while (bDone_ps == false)
	{
		if (PeekMessage(&msg_ps, NULL, 0, 0, PM_REMOVE))
		{
			if (msg_ps.message == WM_QUIT)
				bDone_ps = true;
			else
			{
				TranslateMessage(&msg_ps);
				DispatchMessage(&msg_ps);
			}
		}
		else
		{
			// render
			if (gbActiveWindow_ps == true)
			{
				if (gbEscapeKeyIsPressed_ps == true)
					bDone_ps = true;
				// update should be called here
				update();

			}
			display();
		}
	}

	// clean up
	uninitialize();

	return((int)msg_ps.wParam);
}

LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	// function declarations
	HRESULT resize(int, int);
	void ToggleFullscreen(void);
	void uninitialize(void);

	// variable declarations
	HRESULT hr;
	// code
	switch (iMsg)
	{
	case WM_ACTIVATE:
		if (HIWORD(wParam) == 0)		//if 0, window is active
			gbActiveWindow_ps = true;
		else						// if non zero, window is inactive
			gbActiveWindow_ps = false;
		break;
	case WM_ERASEBKGND:
		return(0);
	case WM_SIZE:
		if (gpID3D11DeviceContext_ps)
		{
			hr = resize(LOWORD(lParam), HIWORD(lParam));
			if (FAILED(hr))
			{
				fopen_s(&gpfile_ps, gszLogFileName_ps, "a+");
				fprintf_s(gpfile_ps, "resize() failed.\n");
				fclose(gpfile_ps);
				return(hr);
			}
			else
			{
				fopen_s(&gpfile_ps, gszLogFileName_ps, "a+");
				fprintf_s(gpfile_ps, "resize() Successful.\n");
				fclose(gpfile_ps);
			}
		}
		break;
	case WM_KEYDOWN:
		switch (wParam)
		{
		case VK_ESCAPE:				// case 27
			if (gbEscapeKeyIsPressed_ps == false)
				gbEscapeKeyIsPressed_ps = true;
			break;

		case 0X46:					// f
			if (gbFullScreen_ps == false)
			{
				ToggleFullscreen();
				gbFullScreen_ps = true;
			}
			else
			{
				ToggleFullscreen();
				gbFullScreen_ps = false;
			}
			break;

		default:
			break;
		}
		break;
	case WM_CHAR:
		switch (LOWORD(wParam))
		{
		case 'l':
		case 'L':
			if(gbLight_ps == true)
				gbLight_ps = false;
			else
				gbLight_ps = true;
			break;
		}
		break;

	case WM_LBUTTONDOWN:
		break;

	case WM_CLOSE:
		uninitialize();
		break;

	case WM_DESTROY:
		PostQuitMessage(0);
		break;

	default:
		break;
	}
	return(DefWindowProc(hwnd, iMsg, wParam, lParam));
}

void ToggleFullscreen(void)
{
	// variable declarations
	MONITORINFO mi;
	if (gbFullScreen_ps == false)
	{
		dwStyle_ps = GetWindowLong(ghwnd_ps, GWL_STYLE);
		if (dwStyle_ps &WS_OVERLAPPEDWINDOW)
		{
			mi = { sizeof(MONITORINFO) };
			if (GetWindowPlacement(ghwnd_ps, &wpPrev_ps) && GetMonitorInfo(MonitorFromWindow(ghwnd_ps, MONITORINFOF_PRIMARY), &mi))
			{
				SetWindowLong(ghwnd_ps, GWL_STYLE, dwStyle_ps & ~WS_OVERLAPPEDWINDOW);
				SetWindowPos(ghwnd_ps, HWND_TOP, mi.rcMonitor.left,
					mi.rcMonitor.top,
					mi.rcMonitor.right - mi.rcMonitor.left,
					mi.rcMonitor.bottom - mi.rcMonitor.top,
					SWP_NOZORDER | SWP_FRAMECHANGED);
			}
		}
		ShowCursor(FALSE);
	}
	else
	{
		SetWindowLong(ghwnd_ps, GWL_STYLE, dwStyle_ps | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd_ps, &wpPrev_ps);
		SetWindowPos(ghwnd_ps, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_FRAMECHANGED);

		ShowCursor(TRUE);
	}
}

HRESULT initialize(void)
{
	// function declarations
	void uninitialize(void);
	HRESULT resize(int, int);

	// variable declarations
	HRESULT hr;
	D3D_DRIVER_TYPE d3dDriverType;
	D3D_DRIVER_TYPE d3dDriverTypes[] =
	{ D3D_DRIVER_TYPE_HARDWARE, D3D_DRIVER_TYPE_WARP, D3D_DRIVER_TYPE_REFERENCE };
	D3D_FEATURE_LEVEL d3dFeatureLevel_required = D3D_FEATURE_LEVEL_11_0;
	D3D_FEATURE_LEVEL d3dFeatureLevel_acquired = D3D_FEATURE_LEVEL_10_0;	// default lowest
	UINT createDeviceFlags = 0;
	UINT numDriverTypes = 0;
	UINT numFeatureLevels = 1;	//based upon d3dFeatureLevel_required

	// code
	// calculate sizeof array
	numDriverTypes = sizeof(d3dDriverTypes) / sizeof(d3dDriverTypes[0]);

	DXGI_SWAP_CHAIN_DESC dxgiSwapChainDesc;
	ZeroMemory((void *)&dxgiSwapChainDesc, sizeof(DXGI_SWAP_CHAIN_DESC));

	dxgiSwapChainDesc.BufferCount = 1;
	dxgiSwapChainDesc.BufferDesc.Width = WIN_WIDTH;
	dxgiSwapChainDesc.BufferDesc.Height = WIN_HEIGHT;
	dxgiSwapChainDesc.BufferDesc.Format = DXGI_FORMAT_B8G8R8A8_UNORM;
	dxgiSwapChainDesc.BufferDesc.RefreshRate.Numerator = 60;
	dxgiSwapChainDesc.BufferDesc.RefreshRate.Denominator = 1;
	dxgiSwapChainDesc.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT;
	dxgiSwapChainDesc.OutputWindow = ghwnd_ps;
	dxgiSwapChainDesc.SampleDesc.Count = 1;
	dxgiSwapChainDesc.SampleDesc.Quality = 0;
	dxgiSwapChainDesc.Windowed = TRUE;

	for (UINT driverTypeIndex = 0; driverTypeIndex < numDriverTypes; driverTypeIndex++)
	{
		d3dDriverType = d3dDriverTypes[driverTypeIndex];

		hr = D3D11CreateDeviceAndSwapChain(
			NULL,							// Adapter
			d3dDriverType,					// Driver Type
			NULL, 							// Software
			createDeviceFlags,				// Flags
			&d3dFeatureLevel_required,		// feature levels
			numFeatureLevels,				// Num feature Level
			D3D11_SDK_VERSION,				// SDK Version
			&dxgiSwapChainDesc,				// Swap chain descriptor
			&gpIDXGISwapChain_ps, 			//SwapChain
			&gpID3D11Device_ps,				// Device
			&d3dFeatureLevel_acquired,		// feature level
			&gpID3D11DeviceContext_ps		// Device context				
		);
		if (SUCCEEDED(hr))
			break;
	}
	if (FAILED(hr))
	{
		fopen_s(&gpfile_ps, gszLogFileName_ps, "a+");
		fprintf_s(gpfile_ps, "D3D11CreateDeviceAndSwapChain() failed.\n");
		fclose(gpfile_ps);
		return(hr);
	}
	else
	{
		fopen_s(&gpfile_ps, gszLogFileName_ps, "a+");
		fprintf_s(gpfile_ps, "D3D11CreateDeviceAndSwapChain() Successful.\n");

		fprintf_s(gpfile_ps, "Chosen Driver Is Of ");
		if (d3dDriverType == D3D_DRIVER_TYPE_HARDWARE)
		{
			fprintf_s(gpfile_ps, "Hardware Type.\n ");
		}
		else if (d3dDriverType == D3D_DRIVER_TYPE_WARP)
		{
			fprintf_s(gpfile_ps, "Warp Type.\n ");
		}
		else if (d3dDriverType == D3D_DRIVER_TYPE_REFERENCE)
		{
			fprintf_s(gpfile_ps, "Reference Type.\n ");
		}
		else
		{
			fprintf_s(gpfile_ps, "Unknown Type.\n ");
		}

		fprintf_s(gpfile_ps, "The Supported Highest Feature Level Is \n ");
		if (d3dFeatureLevel_acquired == D3D_FEATURE_LEVEL_11_0)
		{
			fprintf_s(gpfile_ps, "11.0 \n ");
		}
		else if (d3dFeatureLevel_acquired == D3D_FEATURE_LEVEL_10_1)
		{
			fprintf_s(gpfile_ps, "10.1 \n ");
		}
		else if (d3dFeatureLevel_acquired == D3D_FEATURE_LEVEL_10_0)
		{
			fprintf_s(gpfile_ps, "10.0 \n ");
		}
		else
		{
			fprintf_s(gpfile_ps, "Unknown Type.\n ");
		}
		fclose(gpfile_ps);
	}

	// ***** VERTEX SHADER *****
	// Vertex Shader Source Code
	const char *vertexShaderSourceCode =
		"cbuffer ConstantBuffer											" \
		"{																" \
		"	float4x4 worldMatrix;							" \
		"	float4x4 viewMatrix;							" \
		"	float4x4 projectionMatrix;" \
		"	float4 la;"	\
		"	float4 ld;"	\
		"	float4 ls;"	\
		"	float4 ka;"	\
		"	float4 kd;"	\
		"	float4 ks;"	\
		"	float4 lightPosition;"	\
		"	float materialShininess;"	\
		"	uint keyPressed;"	\
		"}" \
		"struct vertex_output	" \
		"{"\
		"	float4 position : SV_POSITION;" \
		"	float3 tn : NORMAL0;" \
		"	float3 ld : NORMAL1;" \
		"	float3 vv : NORMAL2;" \
		"};"
		"vertex_output main(float4 pos : POSITION, float4 normal : NORMAL)" \
		"{																" \
		"	vertex_output output;"	\
		"	if(keyPressed == 1)"	\
		"	{"	\
		"		float4 eyeCoordinates = mul(worldMatrix, pos);"	\
		"		eyeCoordinates = mul(viewMatrix, eyeCoordinates);"	\
		"		output.tn = (mul((float3x3)worldMatrix, (float3)normal));"	\
		"		output.ld = (float3)(lightPosition - eyeCoordinates);"	\
		"		output.vv = (-eyeCoordinates.xyz);"	\
		"	}"	\
		"	float4 Position = mul(worldMatrix, pos);"	\
		"	Position = mul(viewMatrix, Position);"	\
		"	Position = mul(projectionMatrix, Position);"	\
		"	output.position = Position;" \
		"	return(output);" \
		"}";

	// variables to hold errors and source code
	ID3DBlob *pID3DBlob_VertexShaderCode = NULL;
	ID3DBlob *pID3DBlob_Error = NULL;

	// Compile vertex shader
	hr = D3DCompile(vertexShaderSourceCode,
		lstrlenA(vertexShaderSourceCode) + 1,
		"VS",
		NULL,
		D3D_COMPILE_STANDARD_FILE_INCLUDE,
		"main",
		"vs_5_0",
		0,
		0,
		&pID3DBlob_VertexShaderCode,
		&pID3DBlob_Error);

	// Compilation error checking
	if (FAILED(hr))
	{
		if (pID3DBlob_Error != NULL)
		{
			fopen_s(&gpfile_ps, gszLogFileName_ps, "a+");
			fprintf_s(gpfile_ps, "D3DCompile() Failed For Vertex Shader: %s. \n", (char *)pID3DBlob_Error->GetBufferPointer());
			fclose(gpfile_ps);
			pID3DBlob_Error->Release();
			pID3DBlob_Error = NULL;
			return(hr);
		}
	}
	else
	{
		fopen_s(&gpfile_ps, gszLogFileName_ps, "a+");
		fprintf_s(gpfile_ps, "D3DCompile() Succeeded For Vertex Shader Compilation\n");
		fclose(gpfile_ps);
	}

	// Create vertex shader object
	hr = gpID3D11Device_ps->CreateVertexShader(pID3DBlob_VertexShaderCode->GetBufferPointer(),
		pID3DBlob_VertexShaderCode->GetBufferSize(),
		NULL,
		&gpID3D11VertexShader);

	// Create vertex shader error checking
	if (FAILED(hr))
	{
		fopen_s(&gpfile_ps, gszLogFileName_ps, "a+");
		fprintf_s(gpfile_ps, "ID3D11Device_ps::CreateVertexShader() Failed.\n");
		fclose(gpfile_ps);
		return(hr);
	}
	else
	{
		fopen_s(&gpfile_ps, gszLogFileName_ps, "a+");
		fprintf_s(gpfile_ps, "ID3D11Device_ps::CreateVertexShader() Succeeded.\n");
		fclose(gpfile_ps);
	}

	// Set vertex shader to the pipeline
	gpID3D11DeviceContext_ps->VSSetShader(gpID3D11VertexShader, NULL, NULL);
	//------------------------------vertex shader ends-------------------------------//


	// ***** PIXEL SHADER *****	
	// Pixel shader Source code
	const char *pixelShaderSourceCode =
		"cbuffer ConstantBuffer											" \
		"{																" \
		"	float4x4 worldMatrix;							" \
		"	float4x4 viewMatrix;							" \
		"	float4x4 projectionMatrix;" \
		"	float4 la;"	\
		"	float4 ld;"	\
		"	float4 ls;"	\
		"	float4 ka;"	\
		"	float4 kd;"	\
		"	float4 ks;"	\
		"	float4 lightPosition;"	\
		"	float materialShininess;"	\
		"	uint keyPressed;"	\
		"}" \
		"struct vertex_output	" \
		"{"\
		"	float4 position : SV_POSITION;" \
		"	float3 tn : NORMAL0;" \
		"	float3 ld : NORMAL1;" \
		"	float3 vv : NORMAL2;" \
		"};"
		"	float4 main(vertex_output input) : SV_TARGET" \
		"	{" \
		"		float4 phong_ads_light;" \
		"		if(keyPressed == 1)" \
		"		{" \
		"float3 norm_tn = normalize(input.tn);" \
		"float3 norm_ld = normalize(input.ld);" \
		"float3 norm_vv = normalize(input.vv);" \
		"float3 ref_vec = normalize(reflect(-norm_ld, norm_tn));" \
		"float tn_dot_ld = max(dot(norm_tn, norm_ld), 0.0);" \
		"float rv_dot_vv = max(dot(norm_vv, ref_vec), 0.0);" \
		"float4 ambient = la * ka;" \
		"float4 diffuse = ld * kd * tn_dot_ld;"
		"float4 specular = ls * ks * pow(rv_dot_vv, materialShininess);"
		"phong_ads_light = ambient+diffuse+specular;" \
		"}"
		"else" \
		"{" \
		"phong_ads_light = float4(1.0,0.0,0.0,1.0);" \
		"}" \
		"return(phong_ads_light);" \
		"}";

	// variables to hold errors and source code
	ID3DBlob *pID3DBlob_PixelShaderCode;
	pID3DBlob_Error = NULL;

	// Compile Pixel shader
	hr = D3DCompile(pixelShaderSourceCode,
		lstrlenA(pixelShaderSourceCode) + 1,
		"PS",
		NULL,
		D3D_COMPILE_STANDARD_FILE_INCLUDE,
		"main",
		"ps_5_0",
		0,
		0,
		&pID3DBlob_PixelShaderCode,
		&pID3DBlob_Error);

	// Pixel shader compilation error checking
	if (FAILED(hr))
	{
		fopen_s(&gpfile_ps, gszLogFileName_ps, "a+");
		fprintf_s(gpfile_ps, "D3DCompile() Failed For Pixel Shader: %s. \n", (char *)pID3DBlob_Error->GetBufferPointer());
		fclose(gpfile_ps);
		pID3DBlob_Error->Release();
		pID3DBlob_Error = NULL;
		return(hr);
	}
	else
	{
		fopen_s(&gpfile_ps, gszLogFileName_ps, "a+");
		fprintf_s(gpfile_ps, "D3DCompile() Succeeded For Pixel Shader Compilation\n");
		fclose(gpfile_ps);
	}

	// Create Pixel shader object
	hr = gpID3D11Device_ps->CreatePixelShader(pID3DBlob_PixelShaderCode->GetBufferPointer(),
		pID3DBlob_PixelShaderCode->GetBufferSize(),
		NULL,
		&gpID3D11PixelShader);

	// Create Pixel shader error checking
	if (FAILED(hr))
	{
		fopen_s(&gpfile_ps, gszLogFileName_ps, "a+");
		fprintf_s(gpfile_ps, "ID3D11Device_ps::CreatePixelShader() Failed.\n");
		fclose(gpfile_ps);
		return(hr);
	}
	else
	{
		fopen_s(&gpfile_ps, gszLogFileName_ps, "a+");
		fprintf_s(gpfile_ps, "ID3D11Device_ps::CreatePixelShader() Succeeded.\n");
		fclose(gpfile_ps);
	}

	// Set pixel shader to the pipeline
	gpID3D11DeviceContext_ps->PSSetShader(gpID3D11PixelShader, 0, 0);
	//----------------------pixel shader ends------------------------//

	// ******************** INPUT LAYOUT ***********************
	// create and set input layout
	D3D11_INPUT_ELEMENT_DESC inputElementDesc[2];

	// zero out the structure
	ZeroMemory(inputElementDesc, 2 * sizeof(D3D11_INPUT_ELEMENT_DESC));

	// fill structure members
	inputElementDesc[0].SemanticName = "POSITION";			// Whatever written in shader after pos :
	inputElementDesc[0].SemanticIndex = 0;					// Index of POSITION semantic you want to assign, legacy(0 to position)
	inputElementDesc[0].Format = DXGI_FORMAT_R32G32B32_FLOAT;	// Position data  3 for x, y, z
	inputElementDesc[0].InputSlot = 0;						// Layout qualifier
	inputElementDesc[0].AlignedByteOffset = 0;				// Tightly Packed Data therefore 0
	inputElementDesc[0].InstanceDataStepRate = 0;
	inputElementDesc[0].InputSlotClass = D3D11_INPUT_PER_VERTEX_DATA;

	// fill structure members
	inputElementDesc[1].SemanticName = "NORMAL";			// Whatever written in shader after pos :
	inputElementDesc[1].SemanticIndex = 0;					// Index of NORMAL semantic you want to assign, legacy(1 to normal)
	inputElementDesc[1].Format = DXGI_FORMAT_R32G32B32_FLOAT;	// Position data  3 for x, y, z
	inputElementDesc[1].InputSlot = 1;						// Layout qualifier
	inputElementDesc[1].AlignedByteOffset = 0;				// Tightly Packed Data therefore 0
	inputElementDesc[1].InstanceDataStepRate = 0;
	inputElementDesc[1].InputSlotClass = D3D11_INPUT_PER_VERTEX_DATA;

	// create input layout
	hr = gpID3D11Device_ps->CreateInputLayout(inputElementDesc, 	// 1st parameter can take array 
		_ARRAYSIZE(inputElementDesc),								// if array, no of element in array
		pID3DBlob_VertexShaderCode->GetBufferPointer(),				// specify shader code
		pID3DBlob_VertexShaderCode->GetBufferSize(),					// target shader code size
		&gpID3D11InputLayout);										// pointer to input layout

	// error checking of input layout creation
	if (FAILED(hr))
	{
		fopen_s(&gpfile_ps, gszLogFileName_ps, "a+");
		fprintf_s(gpfile_ps, "ID3D11DEVICE::CreateInputLayout() Failed.\n");
		fclose(gpfile_ps);
		return(hr);
	}
	else
	{
		fopen_s(&gpfile_ps, gszLogFileName_ps, "a+");
		fprintf_s(gpfile_ps, "ID3D11DEVICE::CreateInputLayout() Succeeded.\n");
		fclose(gpfile_ps);
	}

	// set input layout to the pipeline
	gpID3D11DeviceContext_ps->IASetInputLayout(gpID3D11InputLayout);

	// release the blob for shaders here
	pID3DBlob_VertexShaderCode->Release();
	pID3DBlob_VertexShaderCode = NULL;
	pID3DBlob_PixelShaderCode->Release();
	pID3DBlob_PixelShaderCode = NULL;
	// --------------------------------- input layout ends -------------------------------

	// ************************ VERTEX BUFFER ****************************

	// ************************ LOAD SPHERE DATA *************************
	getSphereVertexData(sphere_vertices, sphere_normals, sphere_textures, sphere_elements);
	gNumVertices = getNumberOfSphereVertices();
	gNumElements = getNumberOfSphereElements();

	// create vertex buffer
	D3D11_BUFFER_DESC bufferDesc_VertexBuffer_Sphere_Vertex;
	D3D11_BUFFER_DESC bufferDesc_VertexBuffer_Sphere_Normal;
	D3D11_BUFFER_DESC bufferDesc_VertexBuffer_Sphere_Indices;

	// zero out the memory
	ZeroMemory(&bufferDesc_VertexBuffer_Sphere_Vertex, sizeof(D3D11_BUFFER_DESC));
	ZeroMemory(&bufferDesc_VertexBuffer_Sphere_Normal, sizeof(D3D11_BUFFER_DESC));
	ZeroMemory(&bufferDesc_VertexBuffer_Sphere_Indices, sizeof(D3D11_BUFFER_DESC));

	// fill the structure members ( 4 out of 6) for rectangle
	bufferDesc_VertexBuffer_Sphere_Vertex.Usage = D3D11_USAGE_DYNAMIC;
	bufferDesc_VertexBuffer_Sphere_Vertex.ByteWidth = sizeof(float) * ARRAYSIZE(sphere_vertices);
	bufferDesc_VertexBuffer_Sphere_Vertex.BindFlags = D3D11_BIND_VERTEX_BUFFER;
	bufferDesc_VertexBuffer_Sphere_Vertex.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;

	// fill the structure members ( 4 out of 6) for rectangle
	bufferDesc_VertexBuffer_Sphere_Normal.Usage = D3D11_USAGE_DYNAMIC;
	bufferDesc_VertexBuffer_Sphere_Normal.ByteWidth = sizeof(float) * ARRAYSIZE(sphere_normals);
	bufferDesc_VertexBuffer_Sphere_Normal.BindFlags = D3D11_BIND_VERTEX_BUFFER;
	bufferDesc_VertexBuffer_Sphere_Normal.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;

	// fill the structure members ( 4 out of 6) for rectangle
	bufferDesc_VertexBuffer_Sphere_Indices.Usage = D3D11_USAGE_DYNAMIC;
	bufferDesc_VertexBuffer_Sphere_Indices.ByteWidth = sizeof(short) * gNumElements;
	bufferDesc_VertexBuffer_Sphere_Indices.BindFlags = D3D11_BIND_INDEX_BUFFER;
	bufferDesc_VertexBuffer_Sphere_Indices.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;

	// create buffer for sphere position
	hr = gpID3D11Device_ps->CreateBuffer(&bufferDesc_VertexBuffer_Sphere_Vertex, NULL, &gpID3D11Buffer_VertexBuffer_Position_Sphere);

	// buffer creation error checking
	if (FAILED(hr))
	{
		fopen_s(&gpfile_ps, gszLogFileName_ps, "a+");
		fprintf_s(gpfile_ps, "ID3D11Device::CreateBuffer() Failed For Sphere Position Vertex Buffer.\n");
		fclose(gpfile_ps);
		return(hr);
	}
	else
	{
		fopen_s(&gpfile_ps, gszLogFileName_ps, "a+");
		fprintf_s(gpfile_ps, "ID3D11Device::CreateBuffer() Succeeded For Sphere Position Vertex Buffer.\n");
		fclose(gpfile_ps);
	}

	// create buffer for sphere normal
	hr = gpID3D11Device_ps->CreateBuffer(&bufferDesc_VertexBuffer_Sphere_Normal, NULL, &gpID3D11Buffer_VertexBuffer_Normal_Sphere);

	// buffer creation error checking
	if (FAILED(hr))
	{
		fopen_s(&gpfile_ps, gszLogFileName_ps, "a+");
		fprintf_s(gpfile_ps, "ID3D11Device::CreateBuffer() Failed For  Sphere Normal Vertex Buffer.\n");
		fclose(gpfile_ps);
		return(hr);
	}
	else
	{
		fopen_s(&gpfile_ps, gszLogFileName_ps, "a+");
		fprintf_s(gpfile_ps, "ID3D11Device::CreateBuffer() Succeeded For Sphere Normal Vertex Buffer.\n");
		fclose(gpfile_ps);
	}

	// create buffer for sphere indices
	hr = gpID3D11Device_ps->CreateBuffer(&bufferDesc_VertexBuffer_Sphere_Indices, NULL, &gpID3D11Buffer_IndexBuffer);

	// buffer creation error checking
	if (FAILED(hr))
	{
		fopen_s(&gpfile_ps, gszLogFileName_ps, "a+");
		fprintf_s(gpfile_ps, "ID3D11Device::CreateBuffer() Failed For  Sphere Index Buffer.\n");
		fclose(gpfile_ps);
		return(hr);
	}
	else
	{
		fopen_s(&gpfile_ps, gszLogFileName_ps, "a+");
		fprintf_s(gpfile_ps, "ID3D11Device::CreateBuffer() Succeeded For Sphere Index Buffer.\n");
		fclose(gpfile_ps);
	}


	// copy sphere vertices into above buffer	
	D3D11_MAPPED_SUBRESOURCE mappedSubresource;

	// zero out the memory
	ZeroMemory(&mappedSubresource, sizeof(D3D11_MAPPED_SUBRESOURCE));

	// mapping
	gpID3D11DeviceContext_ps->Map(gpID3D11Buffer_VertexBuffer_Position_Sphere, 		// GPU memory buffer
		0,															// Byte Offset Starting
		D3D11_MAP_WRITE_DISCARD,									// permission
		0,															// decides what cpu will do while mapping takes place,
															// DirectX takes decision to make call synchronusly or asyncronously
		&mappedSubresource);
	memcpy(mappedSubresource.pData, sphere_vertices, sizeof(sphere_vertices));

	// ummap
	gpID3D11DeviceContext_ps->Unmap(gpID3D11Buffer_VertexBuffer_Position_Sphere, 0);

	// copy sphere normal
	// zero out the memory
	ZeroMemory(&mappedSubresource, sizeof(D3D11_MAPPED_SUBRESOURCE));

	// mapping
	gpID3D11DeviceContext_ps->Map(gpID3D11Buffer_VertexBuffer_Normal_Sphere, 		// GPU memory buffer
		0,															// Byte Offset Starting
		D3D11_MAP_WRITE_DISCARD,									// permission
		0,															// decides what cpu will do while mapping takes place,
																	// DirectX takes decision to make call synchronusly or asyncronously
		&mappedSubresource);
	memcpy(mappedSubresource.pData, sphere_normals, sizeof(sphere_normals));

	// ummap
	gpID3D11DeviceContext_ps->Unmap(gpID3D11Buffer_VertexBuffer_Normal_Sphere, 0);

	// copy sphere indices
	// zero out the memory
	ZeroMemory(&mappedSubresource, sizeof(D3D11_MAPPED_SUBRESOURCE));

	// mapping
	gpID3D11DeviceContext_ps->Map(gpID3D11Buffer_IndexBuffer, 		// GPU memory buffer
		0,															// Byte Offset Starting
		D3D11_MAP_WRITE_DISCARD,									// permission
		0,															// decides what cpu will do while mapping takes place,
																	// DirectX takes decision to make call synchronusly or asyncronously
		&mappedSubresource);
	memcpy(mappedSubresource.pData, sphere_elements, gNumElements * sizeof(short));

	// ummap
	gpID3D11DeviceContext_ps->Unmap(gpID3D11Buffer_IndexBuffer, 0);


	// *************************** CONSTANT BUFFER *******************************
	// constant buffer
	D3D11_BUFFER_DESC bufferDesc_ConstantBuffer;

	// zero out the memory
	ZeroMemory(&bufferDesc_ConstantBuffer, sizeof(D3D11_BUFFER_DESC));

	// fill the structure member
	bufferDesc_ConstantBuffer.Usage = D3D11_USAGE_DEFAULT;
	bufferDesc_ConstantBuffer.ByteWidth = sizeof(CBUFFER);
	bufferDesc_ConstantBuffer.BindFlags = D3D11_BIND_CONSTANT_BUFFER;

	//create buffer
	hr = gpID3D11Device_ps->CreateBuffer(&bufferDesc_ConstantBuffer, 0, &gpID3D11Buffer_ConstantBuffer);

	// constant buffer creation error checking
	if (FAILED(hr))
	{
		fopen_s(&gpfile_ps, gszLogFileName_ps, "a+");
		fprintf_s(gpfile_ps, "ID3D11Device::CreateBuffer() Failed For Constant Buffer.\n");
		fclose(gpfile_ps);
		return(hr);
	}
	else
	{
		fopen_s(&gpfile_ps, gszLogFileName_ps, "a+");
		fprintf_s(gpfile_ps, "ID3D11Device::CreateBuffer() Succeeded For Constant Buffer.\n");
		fclose(gpfile_ps);
	}

	gpID3D11DeviceContext_ps->VSSetConstantBuffers(0, 1, &gpID3D11Buffer_ConstantBuffer);
	gpID3D11DeviceContext_ps->PSSetConstantBuffers(0, 1, &gpID3D11Buffer_ConstantBuffer);

	// set rasterization state
	// in D3D, backface culling is by default 'ON'
	// means backface of geometry will not be visible.
	// this causes our 2d shape backface to vanish on rotation.
	// so set culling 'OFF'

	D3D11_RASTERIZER_DESC rasterizerDesc;
	ZeroMemory((void *)&rasterizerDesc, sizeof(D3D11_RASTERIZER_DESC));

	rasterizerDesc.AntialiasedLineEnable = FALSE;
	rasterizerDesc.CullMode = D3D11_CULL_NONE;
	rasterizerDesc.DepthBias = 0;
	rasterizerDesc.DepthBiasClamp = 0.0f;
	rasterizerDesc.DepthClipEnable = TRUE;
	rasterizerDesc.FillMode = D3D11_FILL_SOLID;
	rasterizerDesc.FrontCounterClockwise = FALSE;
	rasterizerDesc.MultisampleEnable = FALSE;
	rasterizerDesc.ScissorEnable = FALSE;
	rasterizerDesc.SlopeScaledDepthBias = 0.0f;

	hr = gpID3D11Device_ps->CreateRasterizerState(&rasterizerDesc, &gpID3D11RasterizerState);
	if (FAILED(hr))
	{
		fopen_s(&gpfile_ps, gszLogFileName_ps, "a+");
		fprintf_s(gpfile_ps, "ID3D11Device::CreateRasterizerState() Failed For Culling.\n");
		fclose(gpfile_ps);
		return(hr);
	}
	else
	{
		fopen_s(&gpfile_ps, gszLogFileName_ps, "a+");
		fprintf_s(gpfile_ps, "ID3D11Device::CreateRasterizerState() Succedeed For Culling.\n");
		fclose(gpfile_ps);
	}

	gpID3D11DeviceContext_ps->RSSetState(gpID3D11RasterizerState);

	/* clear color ( blue)*/
	gClearColor[0] = 0.0f;
	gClearColor[1] = 0.0f;
	gClearColor[2] = 0.0f;
	gClearColor[3] = 1.0f;

	//set projection matrix to identity matrix
	gPerspectiveProjectionMatrix = XMMatrixIdentity();

	// call resize for first tym
	hr = resize(WIN_WIDTH, WIN_HEIGHT);
	if (FAILED(hr))
	{
		fopen_s(&gpfile_ps, gszLogFileName_ps, "a+");
		fprintf_s(gpfile_ps, "resize() failed.\n ");
		fclose(gpfile_ps);
		return(hr);
	}
	else
	{
		fopen_s(&gpfile_ps, gszLogFileName_ps, "a+");
		fprintf_s(gpfile_ps, "resize() successful.\n ");
		fclose(gpfile_ps);
	}

	return(S_OK);
}

HRESULT resize(int width, int height)
{
	// code
	HRESULT hr = S_OK;

	// free any sizedependent resources
	if (gpID3D11RenderTargetView_ps)
	{
		gpID3D11RenderTargetView_ps->Release();
		gpID3D11RenderTargetView_ps = NULL;
	}

	if (gpID3D11DepthStencilView)
	{
		gpID3D11DepthStencilView->Release();
		gpID3D11DepthStencilView = NULL;
	}

	// resize swap chain buffers accordingly
	gpIDXGISwapChain_ps->ResizeBuffers(1, width, height, DXGI_FORMAT_B8G8R8A8_UNORM, 0);

	// again get back buffer from swap chain
	ID3D11Texture2D *pID3D11Texture2D_BackBuffer;
	gpIDXGISwapChain_ps->GetBuffer(0, __uuidof(ID3D11Texture2D), (LPVOID*)&pID3D11Texture2D_BackBuffer);

	// again get render target view from d3d11 device using above back buffer
	hr = gpID3D11Device_ps->CreateRenderTargetView(pID3D11Texture2D_BackBuffer, NULL, &gpID3D11RenderTargetView_ps);
	if (FAILED(hr))
	{
		fopen_s(&gpfile_ps, gszLogFileName_ps, "a+");
		fprintf_s(gpfile_ps, "CreateRenderTargetView() failed.\n ");
		fclose(gpfile_ps);
		return(hr);
	}
	else
	{
		fopen_s(&gpfile_ps, gszLogFileName_ps, "a+");
		fprintf_s(gpfile_ps, "CreateRenderTargetView() successful.\n ");
		fclose(gpfile_ps);
	}

	pID3D11Texture2D_BackBuffer->Release();
	pID3D11Texture2D_BackBuffer = NULL;

	// create depth stencil buffer ( or zbuffer)
	D3D11_TEXTURE2D_DESC textureDesc;

	ZeroMemory(&textureDesc, sizeof(D3D11_TEXTURE2D_DESC));

	textureDesc.Width = (UINT)width;
	textureDesc.Height = (UINT)height;
	textureDesc.ArraySize = 1;
	textureDesc.MipLevels = 1;
	textureDesc.SampleDesc.Count = 1;
	textureDesc.SampleDesc.Quality = 0;
	textureDesc.Format = DXGI_FORMAT_D32_FLOAT;
	textureDesc.Usage = D3D11_USAGE_DEFAULT;
	textureDesc.BindFlags = D3D11_BIND_DEPTH_STENCIL;
	textureDesc.CPUAccessFlags = 0;
	textureDesc.MiscFlags = 0;

	ID3D11Texture2D *pID3D11Texture2D_DepthBuffer;

	// create texture buffer as depth buffer
	gpID3D11Device_ps->CreateTexture2D(&textureDesc, NULL, &pID3D11Texture2D_DepthBuffer);

	// create depth stencil view from above depth stencil buffer
	D3D11_DEPTH_STENCIL_VIEW_DESC depthStencilViewDesc;

	// zero out the memory
	ZeroMemory(&depthStencilViewDesc, sizeof(D3D11_DEPTH_STENCIL_VIEW_DESC));

	// fill the structure members
	depthStencilViewDesc.Format = DXGI_FORMAT_D32_FLOAT;
	depthStencilViewDesc.ViewDimension = D3D11_DSV_DIMENSION_TEXTURE2DMS;

	// create depth stencil view
	hr = gpID3D11Device_ps->CreateDepthStencilView(pID3D11Texture2D_DepthBuffer, &depthStencilViewDesc, &gpID3D11DepthStencilView);

	// error checking
	if (FAILED(hr))
	{
		fopen_s(&gpfile_ps, gszLogFileName_ps, "a+");
		fprintf_s(gpfile_ps, "ID3D11Device::CreateDepthStencilView() failed.\n ");
		fclose(gpfile_ps);
		return(hr);
	}
	else
	{
		fopen_s(&gpfile_ps, gszLogFileName_ps, "a+");
		fprintf_s(gpfile_ps, "ID3D11Device::CreateDepthStencilView() successful.\n ");
		fclose(gpfile_ps);
	}
	pID3D11Texture2D_DepthBuffer->Release();
	pID3D11Texture2D_DepthBuffer = NULL;

	// set render target view as render target with depth stencil view
	gpID3D11DeviceContext_ps->OMSetRenderTargets(1, &gpID3D11RenderTargetView_ps, gpID3D11DepthStencilView);

	//set viewport
	D3D11_VIEWPORT d3dViewPort;
	d3dViewPort.TopLeftX = 0;
	d3dViewPort.TopLeftY = 0;
	d3dViewPort.Width = (float)width;
	d3dViewPort.Height = (float)height;
	d3dViewPort.MinDepth = 0.0f;
	d3dViewPort.MaxDepth = 1.0f;
	gpID3D11DeviceContext_ps->RSSetViewports(1, &d3dViewPort);

	// set perspective matrix
	gPerspectiveProjectionMatrix = XMMatrixPerspectiveFovLH(XMConvertToRadians(45.0f), (float)width / (float)height, 0.1f, 100.0f);

	return(hr);
}

void update(void)
{
	
}

void display(void)
{
	// code
	// function declarations

	// clear render target view to a chosen color
	gpID3D11DeviceContext_ps->ClearRenderTargetView(gpID3D11RenderTargetView_ps, gClearColor);

	// clear depth/stencil view to 1.0
	gpID3D11DeviceContext_ps->ClearDepthStencilView(gpID3D11DepthStencilView, D3D11_CLEAR_DEPTH, 1.0f, 0);
	//select which vertex buffer to display
	UINT stride = sizeof(float) * 3;
	UINT offset = 0;

	// translation is concerned with world matrix transformation
	XMMATRIX translationMatrix = XMMatrixIdentity();
	XMMATRIX rotationMatrix = XMMatrixIdentity();
	XMMATRIX worldMatrix = XMMatrixIdentity();
	XMMATRIX viewMatrix = XMMatrixIdentity();
	XMMATRIX wvMatrix = XMMatrixIdentity();

	CBUFFER constantBuffer;

	gpID3D11DeviceContext_ps->UpdateSubresource(gpID3D11Buffer_ConstantBuffer,
		0,
		NULL,
		&constantBuffer,
		0,
		0);

	// *************************** DRAW SPHERE *****************************
	gpID3D11DeviceContext_ps->IASetVertexBuffers(0, 1, &gpID3D11Buffer_VertexBuffer_Position_Sphere, &stride, &offset);
	gpID3D11DeviceContext_ps->IASetVertexBuffers(1, 1, &gpID3D11Buffer_VertexBuffer_Normal_Sphere, &stride, &offset);
	gpID3D11DeviceContext_ps->IASetIndexBuffer(gpID3D11Buffer_IndexBuffer, DXGI_FORMAT_R16_UINT, 0);	//R16 maps with short

	// select geometry primitive
	gpID3D11DeviceContext_ps->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLESTRIP);

	// final WorldViewProjection Matrix
	translationMatrix = XMMatrixTranslation(0.0f, 0.0f, 5.0f);

	worldMatrix = rotationMatrix * translationMatrix;
	wvMatrix = worldMatrix * viewMatrix;

	// load the data into constant buffer
	if (gbLight_ps == true)
	{
		constantBuffer.La = XMVectorSet(LightAmbient[0], LightAmbient[1], LightAmbient[2], LightAmbient[3]);
		constantBuffer.Ld = XMVectorSet(LightDiffuse[0], LightDiffuse[1], LightDiffuse[2], LightDiffuse[3]);
		constantBuffer.Ls = XMVectorSet(LightSpecular[0], LightSpecular[1], LightSpecular[2], LightSpecular[3]);
		

		constantBuffer.Ka = XMVectorSet(MaterialAmbient[0], MaterialAmbient[1], MaterialAmbient[2], MaterialAmbient[3]);
		constantBuffer.Kd = XMVectorSet(MaterialDiffuse[0], MaterialDiffuse[1], MaterialDiffuse[2], MaterialDiffuse[3]);
		constantBuffer.Ks = XMVectorSet(MaterialSpecular[0], MaterialSpecular[1], MaterialSpecular[2], MaterialSpecular[3]);
		constantBuffer.MaterialShininess = Shininess;
		//constantBuffer.LightPosition = XMVectorSet(LightPosition[0], LightPosition[1], LightPosition[2], LightPosition[3]);
		constantBuffer.LightPosition = XMVectorSet(100.0, 100.0, -100.0, 1.0);
		constantBuffer.KeyPressed = 1;

	}
	else
	{
		constantBuffer.KeyPressed = 0;
	}
	constantBuffer.WorldMatrix = worldMatrix;
	constantBuffer.ViewMatrix = viewMatrix;
	constantBuffer.ProjectionMatrix = gPerspectiveProjectionMatrix;

	gpID3D11DeviceContext_ps->UpdateSubresource(gpID3D11Buffer_ConstantBuffer,
		0,
		NULL,
		&constantBuffer,
		0,
		0);

	// draw vertex buffer to render target
	gpID3D11DeviceContext_ps->DrawIndexed(gNumElements, 0, 0);	// 6 vertices from 0 to 6

	// switch between front and back buffers
	gpIDXGISwapChain_ps->Present(0, 0);
}

void uninitialize(void)
{
	//code
	if (gpID3D11Buffer_ConstantBuffer)
	{
		gpID3D11Buffer_ConstantBuffer->Release();
		gpID3D11Buffer_ConstantBuffer = NULL;
	}

	if (gpID3D11InputLayout)
	{
		gpID3D11InputLayout->Release();
		gpID3D11InputLayout = NULL;
	}

	if (gpID3D11Buffer_VertexBuffer_Position_Sphere)
	{
		gpID3D11Buffer_VertexBuffer_Position_Sphere->Release();
		gpID3D11Buffer_VertexBuffer_Position_Sphere = NULL;
	}

	if (gpID3D11Buffer_VertexBuffer_Normal_Sphere)
	{
		gpID3D11Buffer_VertexBuffer_Normal_Sphere->Release();
		gpID3D11Buffer_VertexBuffer_Normal_Sphere = NULL;
	}

	if (gpID3D11PixelShader)
	{
		gpID3D11PixelShader->Release();
		gpID3D11PixelShader = NULL;
	}

	if (gpID3D11VertexShader)
	{
		gpID3D11VertexShader->Release();
		gpID3D11VertexShader = NULL;
	}

	if (gpID3D11RenderTargetView_ps)
	{
		gpID3D11RenderTargetView_ps->Release();
		gpID3D11RenderTargetView_ps = NULL;
	}

	if (gpIDXGISwapChain_ps)
	{
		gpIDXGISwapChain_ps->Release();
		gpIDXGISwapChain_ps = NULL;
	}

	if (gpID3D11DeviceContext_ps)
	{
		gpID3D11DeviceContext_ps->Release();
		gpID3D11DeviceContext_ps = NULL;
	}

	if (gpID3D11Device_ps)
	{
		gpID3D11Device_ps->Release();
		gpID3D11Device_ps = NULL;
	}

	if (gpfile_ps)
	{
		fopen_s(&gpfile_ps, gszLogFileName_ps, "a+");
		fprintf_s(gpfile_ps, "uninitiaize() succeded.\n ");
		fprintf_s(gpfile_ps, "log file is closed successfully.\n ");
		fclose(gpfile_ps);
	}
}