#include <windows.h>
#include <stdio.h>

#include <d3d11.h>					// D3D11 header file
#include <d3dcompiler.h>			// shader compilation

#pragma warning( disable : 4838)
#include "XNAMath\xnamath.h"
#include "Sphere.h"

#pragma comment (lib, "d3d11.lib")	// D3d11 library
#pragma comment (lib, "D3dcompiler.lib")	// header compilation library
#pragma comment (lib, "Sphere.lib")			// header for sphere

#define WIN_WIDTH 800
#define WIN_HEIGHT 600

// global function declarations
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

// global variables
FILE *gpfile_ps = NULL;
char gszLogFileName_ps[] = "log.txt";

HWND ghwnd_ps = NULL;
DWORD dwStyle_ps;
WINDOWPLACEMENT wpPrev_ps = { sizeof(WINDOWPLACEMENT) };

bool gbActiveWindow_ps = false;
bool gbEscapeKeyIsPressed_ps = false;
bool gbFullScreen_ps = false;
bool gbLight_ps = false;
bool gbToggle_ps = false;

float gClearColor[4];	// Array to hold color values as glClearColor in directX takes array as a parameter
float angle = 0.0f;
int axis = 0;
int gWidth = 0;
int gHeight = 0;

float sphere_vertices[1146];
float sphere_normals[1146];
float sphere_textures[764];
unsigned short sphere_elements[2280];
unsigned int gNumElements;
unsigned int gNumVertices;

IDXGISwapChain *gpIDXGISwapChain_ps = NULL;
ID3D11Device *gpID3D11Device_ps = NULL;
ID3D11DeviceContext *gpID3D11DeviceContext_ps = NULL;
ID3D11RenderTargetView *gpID3D11RenderTargetView_ps = NULL;

ID3D11VertexShader *gpID3D11VertexShader_PerPixel = NULL;
ID3D11PixelShader *gpID3D11PixelShader_PerPixel = NULL;
ID3D11VertexShader *gpID3D11VertexShader_PerVertex = NULL;
ID3D11PixelShader *gpID3D11PixelShader_PerVertex = NULL;

ID3D11Buffer *gpID3D11Buffer_VertexBuffer_Position_Sphere = NULL;
ID3D11Buffer *gpID3D11Buffer_VertexBuffer_Texture_Sphere = NULL;
ID3D11Buffer *gpID3D11Buffer_VertexBuffer_Normal_Sphere = NULL;
ID3D11Buffer *gpID3D11Buffer_IndexBuffer = NULL;

ID3D11InputLayout *gpID3D11InputLayout = NULL;
ID3D11Buffer *gpID3D11Buffer_ConstantBuffer = NULL;

ID3D11RasterizerState *gpID3D11RasterizerState = NULL;
ID3D11DepthStencilView *gpID3D11DepthStencilView = NULL;

struct CBUFFER
{
	XMMATRIX WorldMatrix;
	XMMATRIX ViewMatrix;
	XMMATRIX ProjectionMatrix;
	XMVECTOR La;
	XMVECTOR Ld;
	XMVECTOR Ls;
	XMVECTOR Ka;
	XMVECTOR Kd;
	XMVECTOR Ks;
	XMVECTOR LightPosition;
	float MaterialShininess;
	unsigned int KeyPressed;
};

float LightAmbient[] = { 0.0f, 0.0f, 0.0f, 1.0f };
float LightDiffuse[] = { 1.0f, 1.0f, 1.0f, 1.0f };
float LightSpecular[] = { 1.0f, 1.0f, 1.0f, 1.0f };
float LightPosition[] = { 0.0f, 0.0f, 0.0f, 1.0f };
float MaterialAmbient[] = { 0.0f, 0.0f, 0.0f, 1.0f };
float MaterialDiffuse[] = { 1.0f, 1.0f, 1.0f, 1.0f };
float MaterialSpecular[] = { 1.0f, 1.0f, 1.0f, 1.0f };
float Shininess = 128.0f;

// Sphere characteristics
struct SphereProperty
{
	float spherePosition[3];
	float MDiffuse[4];
	float MAmbient[4];
	float MSpecular[4];
	int SViewport[4];
	float sphereShininess;
};

struct SphereProperty Spheres[24];

XMMATRIX gPerspectiveProjectionMatrix;

// WinMain
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{
	// function declarations
	HRESULT initialize(void);
	void uninitialize(void);
	void display(void);
	void update(void);

	// variable declarations
	WNDCLASSEX wndClass_ps;
	HWND hwnd_ps;
	MSG msg_ps;
	TCHAR szClassName_ps[] = TEXT("Direct3D11");
	bool bDone_ps = false;

	// code
	// create log file
	if (fopen_s(&gpfile_ps, gszLogFileName_ps, "w") != 0)
	{
		MessageBox(NULL, TEXT("Log File Cannot Be Created\n, Exitting From Application\n"), TEXT("ERROR!!!"), MB_OK | MB_TOPMOST | MB_ICONSTOP);
		exit(0);
	}
	else
	{
		fprintf(gpfile_ps, "Log File is Created Successfully\n");
		fclose(gpfile_ps);
	}

	// initialize WNDCLASSEX structure
	wndClass_ps.cbSize = sizeof(WNDCLASSEX);
	wndClass_ps.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
	wndClass_ps.cbClsExtra = 0;
	wndClass_ps.cbWndExtra = 0;
	wndClass_ps.lpfnWndProc = WndProc;
	wndClass_ps.lpszClassName = szClassName_ps;
	wndClass_ps.lpszMenuName = NULL;
	wndClass_ps.hInstance = hInstance;
	wndClass_ps.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndClass_ps.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndClass_ps.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	wndClass_ps.hIconSm = LoadIcon(NULL, IDI_APPLICATION);

	// register WNDCLASSEX structure
	RegisterClassEx(&wndClass_ps);

	// create window
	hwnd_ps = CreateWindow(szClassName_ps,
		TEXT("Direct3D11 Window"),
		WS_OVERLAPPEDWINDOW,
		100,
		100,
		WIN_WIDTH,
		WIN_HEIGHT,
		NULL,			// parent Handle
		NULL,			// menu name
		hInstance,		// hInstance
		NULL);

	ghwnd_ps = hwnd_ps;

	ShowWindow(hwnd_ps, iCmdShow);
	SetForegroundWindow(hwnd_ps);
	SetFocus(hwnd_ps);

	// initialize D3D
	HRESULT hr;
	hr = initialize();
	if (FAILED(hr))
	{
		fopen_s(&gpfile_ps, gszLogFileName_ps, "a+");
		fprintf_s(gpfile_ps, "initialize() failed. Exitting Now...\n");
		fclose(gpfile_ps);
		DestroyWindow(hwnd_ps);
		hwnd_ps = NULL;
	}
	else
	{
		fopen_s(&gpfile_ps, gszLogFileName_ps, "a+");
		fprintf_s(gpfile_ps, "initialize() Successful.\n");
		fclose(gpfile_ps);
	}

	// message loop
	while (bDone_ps == false)
	{
		if (PeekMessage(&msg_ps, NULL, 0, 0, PM_REMOVE))
		{
			if (msg_ps.message == WM_QUIT)
				bDone_ps = true;
			else
			{
				TranslateMessage(&msg_ps);
				DispatchMessage(&msg_ps);
			}
		}
		else
		{
			// render
			if (gbActiveWindow_ps == true)
			{
				if (gbEscapeKeyIsPressed_ps == true)
					bDone_ps = true;
				// update should be called here
				update();

			}
			display();
		}
	}

	// clean up
	uninitialize();

	return((int)msg_ps.wParam);
}

LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	// function declarations
	HRESULT resize(int, int);
	void ToggleFullscreen(void);
	void uninitialize(void);

	// variable declarations
	HRESULT hr;
	// code
	switch (iMsg)
	{
	case WM_ACTIVATE:
		if (HIWORD(wParam) == 0)		//if 0, window is active
			gbActiveWindow_ps = true;
		else						// if non zero, window is inactive
			gbActiveWindow_ps = false;
		break;
	case WM_ERASEBKGND:
		return(0);
	case WM_SIZE:
		if (gpID3D11DeviceContext_ps)
		{
			hr = resize(LOWORD(lParam), HIWORD(lParam));
			if (FAILED(hr))
			{
				fopen_s(&gpfile_ps, gszLogFileName_ps, "a+");
				fprintf_s(gpfile_ps, "resize() failed.\n");
				fclose(gpfile_ps);
				return(hr);
			}
			else
			{
				fopen_s(&gpfile_ps, gszLogFileName_ps, "a+");
				fprintf_s(gpfile_ps, "resize() Successful.\n");
				fclose(gpfile_ps);
			}
		}
		break;
	case WM_KEYDOWN:
		switch (wParam)
		{
		case VK_ESCAPE:				// case 27
			if (gbEscapeKeyIsPressed_ps == false)
				gbEscapeKeyIsPressed_ps = true;
			break;

		case 0X46:					// f
			if (gbFullScreen_ps == false)
			{
				ToggleFullscreen();
				gbFullScreen_ps = true;
			}
			else
			{
				ToggleFullscreen();
				gbFullScreen_ps = false;
			}
			break;

		default:
			break;
		}
		break;
	case WM_CHAR:
		switch (LOWORD(wParam))
		{
		case 'l':
		case 'L':
			if(gbLight_ps == true)
				gbLight_ps = false;
			else
				gbLight_ps = true;
			break;

		case 't':
		case 'T':
			if (gbToggle_ps == true)
				gbToggle_ps = false;
			else
				gbToggle_ps = true;
			break;

		case 'x':
		case 'X':
			axis = 1;
			angle = 0.0f;
			LightPosition[0] = 0.0f;
			LightPosition[1] = 0.0f;
			LightPosition[2] = 0.0f;
			break;

		case 'y':
		case 'Y':
			axis = 2;
			angle = 0.0f;
			LightPosition[0] = 0.0f;
			LightPosition[1] = 0.0f;
			LightPosition[2] = 0.0f;
			break;

		case 'z':
		case 'Z':
			axis = 3;
			angle = 0.0f;
			LightPosition[0] = 0.0f;
			LightPosition[1] = 0.0f;
			LightPosition[2] = 0.0f;
			break;

		}
		break;

	case WM_LBUTTONDOWN:
		break;

	case WM_CLOSE:
		uninitialize();
		break;

	case WM_DESTROY:
		PostQuitMessage(0);
		break;

	default:
		break;
	}
	return(DefWindowProc(hwnd, iMsg, wParam, lParam));
}

void ToggleFullscreen(void)
{
	// variable declarations
	MONITORINFO mi;
	if (gbFullScreen_ps == false)
	{
		dwStyle_ps = GetWindowLong(ghwnd_ps, GWL_STYLE);
		if (dwStyle_ps &WS_OVERLAPPEDWINDOW)
		{
			mi = { sizeof(MONITORINFO) };
			if (GetWindowPlacement(ghwnd_ps, &wpPrev_ps) && GetMonitorInfo(MonitorFromWindow(ghwnd_ps, MONITORINFOF_PRIMARY), &mi))
			{
				SetWindowLong(ghwnd_ps, GWL_STYLE, dwStyle_ps & ~WS_OVERLAPPEDWINDOW);
				SetWindowPos(ghwnd_ps, HWND_TOP, mi.rcMonitor.left,
					mi.rcMonitor.top,
					mi.rcMonitor.right - mi.rcMonitor.left,
					mi.rcMonitor.bottom - mi.rcMonitor.top,
					SWP_NOZORDER | SWP_FRAMECHANGED);
			}
		}
		ShowCursor(FALSE);
	}
	else
	{
		SetWindowLong(ghwnd_ps, GWL_STYLE, dwStyle_ps | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd_ps, &wpPrev_ps);
		SetWindowPos(ghwnd_ps, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_FRAMECHANGED);

		ShowCursor(TRUE);
	}
}

HRESULT initialize(void)
{
	// function declarations
	void uninitialize(void);
	HRESULT resize(int, int);

	// variable declarations
	HRESULT hr;
	D3D_DRIVER_TYPE d3dDriverType;
	D3D_DRIVER_TYPE d3dDriverTypes[] =
	{ D3D_DRIVER_TYPE_HARDWARE, D3D_DRIVER_TYPE_WARP, D3D_DRIVER_TYPE_REFERENCE };
	D3D_FEATURE_LEVEL d3dFeatureLevel_required = D3D_FEATURE_LEVEL_11_0;
	D3D_FEATURE_LEVEL d3dFeatureLevel_acquired = D3D_FEATURE_LEVEL_10_0;	// default lowest
	UINT createDeviceFlags = 0;
	UINT numDriverTypes = 0;
	UINT numFeatureLevels = 1;	//based upon d3dFeatureLevel_required

	// code
	// calculate sizeof array
	numDriverTypes = sizeof(d3dDriverTypes) / sizeof(d3dDriverTypes[0]);

	DXGI_SWAP_CHAIN_DESC dxgiSwapChainDesc;
	ZeroMemory((void *)&dxgiSwapChainDesc, sizeof(DXGI_SWAP_CHAIN_DESC));

	dxgiSwapChainDesc.BufferCount = 1;
	dxgiSwapChainDesc.BufferDesc.Width = WIN_WIDTH;
	dxgiSwapChainDesc.BufferDesc.Height = WIN_HEIGHT;
	dxgiSwapChainDesc.BufferDesc.Format = DXGI_FORMAT_B8G8R8A8_UNORM;
	dxgiSwapChainDesc.BufferDesc.RefreshRate.Numerator = 60;
	dxgiSwapChainDesc.BufferDesc.RefreshRate.Denominator = 1;
	dxgiSwapChainDesc.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT;
	dxgiSwapChainDesc.OutputWindow = ghwnd_ps;
	dxgiSwapChainDesc.SampleDesc.Count = 1;
	dxgiSwapChainDesc.SampleDesc.Quality = 0;
	dxgiSwapChainDesc.Windowed = TRUE;

	for (UINT driverTypeIndex = 0; driverTypeIndex < numDriverTypes; driverTypeIndex++)
	{
		d3dDriverType = d3dDriverTypes[driverTypeIndex];

		hr = D3D11CreateDeviceAndSwapChain(
			NULL,							// Adapter
			d3dDriverType,					// Driver Type
			NULL, 							// Software
			createDeviceFlags,				// Flags
			&d3dFeatureLevel_required,		// feature levels
			numFeatureLevels,				// Num feature Level
			D3D11_SDK_VERSION,				// SDK Version
			&dxgiSwapChainDesc,				// Swap chain descriptor
			&gpIDXGISwapChain_ps, 			//SwapChain
			&gpID3D11Device_ps,				// Device
			&d3dFeatureLevel_acquired,		// feature level
			&gpID3D11DeviceContext_ps		// Device context				
		);
		if (SUCCEEDED(hr))
			break;
	}
	if (FAILED(hr))
	{
		fopen_s(&gpfile_ps, gszLogFileName_ps, "a+");
		fprintf_s(gpfile_ps, "D3D11CreateDeviceAndSwapChain() failed.\n");
		fclose(gpfile_ps);
		return(hr);
	}
	else
	{
		fopen_s(&gpfile_ps, gszLogFileName_ps, "a+");
		fprintf_s(gpfile_ps, "D3D11CreateDeviceAndSwapChain() Successful.\n");

		fprintf_s(gpfile_ps, "Chosen Driver Is Of ");
		if (d3dDriverType == D3D_DRIVER_TYPE_HARDWARE)
		{
			fprintf_s(gpfile_ps, "Hardware Type.\n ");
		}
		else if (d3dDriverType == D3D_DRIVER_TYPE_WARP)
		{
			fprintf_s(gpfile_ps, "Warp Type.\n ");
		}
		else if (d3dDriverType == D3D_DRIVER_TYPE_REFERENCE)
		{
			fprintf_s(gpfile_ps, "Reference Type.\n ");
		}
		else
		{
			fprintf_s(gpfile_ps, "Unknown Type.\n ");
		}

		fprintf_s(gpfile_ps, "The Supported Highest Feature Level Is \n ");
		if (d3dFeatureLevel_acquired == D3D_FEATURE_LEVEL_11_0)
		{
			fprintf_s(gpfile_ps, "11.0 \n ");
		}
		else if (d3dFeatureLevel_acquired == D3D_FEATURE_LEVEL_10_1)
		{
			fprintf_s(gpfile_ps, "10.1 \n ");
		}
		else if (d3dFeatureLevel_acquired == D3D_FEATURE_LEVEL_10_0)
		{
			fprintf_s(gpfile_ps, "10.0 \n ");
		}
		else
		{
			fprintf_s(gpfile_ps, "Unknown Type.\n ");
		}
		fclose(gpfile_ps);
	}

	//--------------------------------------------------- PER VERTEX ----------------------------------------
	// ***** VERTEX SHADER FOR PER VERTEX LIGHTING*****
	// Vertex Shader Source Code
	const char *vertexShaderSourceCode_pv =
		"cbuffer ConstantBuffer											" \
		"{																" \
		"	float4x4 worldMatrix;							" \
		"	float4x4 viewMatrix;							" \
		"	float4x4 projectionMatrix;" \
		"	float4 la;"	\
		"	float4 ld;"	\
		"	float4 ls;"	\
		"	float4 ka;"	\
		"	float4 kd;"	\
		"	float4 ks;"	\
		"	float4 lightPosition;"	\
		"	float materialShininess;"	\
		"	uint keyPressed;"	\
		"}" \
		"struct vertex_output	" \
		"{"\
		"	float4 position : SV_POSITION;" \
		"	float4 phong_ads_color : COLOR;" \
		"};"
		"vertex_output main(float4 pos : POSITION, float4 normal : NORMAL)" \
		"{																" \
		"	vertex_output output;"	\
		"	if(keyPressed == 1)"	\
		"	{"	\
		"		float4 eyeCoordinates = mul(worldMatrix, pos);"	\
		"		eyeCoordinates = mul(viewMatrix, eyeCoordinates);"	\
		"		float3 tNorm = normalize(mul((float3x3)worldMatrix, (float3)normal));"	\
		"		float3 lightDirection = (float3)normalize(lightPosition - eyeCoordinates);"	\
		"		float tn_dot_ld = max(dot(tNorm, lightDirection), 0.0);	"	\
		"		float4 ambient = la * ka;"	\
		"		float4 diffuse = ld * kd * tn_dot_ld;"	\
		"		float3 reflectionVector = reflect(-lightDirection, tNorm);"	\
		"		float3 viewerVector = normalize(-eyeCoordinates.xyz);"	\
		"		float4 specular = ls * ks * pow(max(dot(reflectionVector, viewerVector), 0.0), materialShininess);"
		"		output.phong_ads_color = diffuse + ambient + specular;"	\
		"	}"	\
		"	else"	\
		"	{"	\
		"		output.phong_ads_color = float4(1.0, 1.0, 1.0, 1.0);"	\
		"	}" \
		"	float4 Position = mul(worldMatrix, pos);"	\
		"	Position = mul(viewMatrix, Position);"	\
		"	Position = mul(projectionMatrix, Position);"	\
		"	output.position = Position;" \
		"	return(output);" \
		"}";

	// variables to hold errors and source code
	ID3DBlob *pID3DBlob_VertexShaderCode_pv = NULL;
	ID3DBlob *pID3DBlob_Error_pv = NULL;

	// Compile vertex shader
	hr = D3DCompile(vertexShaderSourceCode_pv,
		lstrlenA(vertexShaderSourceCode_pv) + 1,
		"VS",
		NULL,
		D3D_COMPILE_STANDARD_FILE_INCLUDE,
		"main",
		"vs_5_0",
		0,
		0,
		&pID3DBlob_VertexShaderCode_pv,
		&pID3DBlob_Error_pv);

	// Compilation error checking
	if (FAILED(hr))
	{
		if (pID3DBlob_Error_pv != NULL)
		{
			fopen_s(&gpfile_ps, gszLogFileName_ps, "a+");
			fprintf_s(gpfile_ps, "D3DCompile() Failed For Vertex Shader: %s. \n", (char *)pID3DBlob_Error_pv->GetBufferPointer());
			fclose(gpfile_ps);
			pID3DBlob_Error_pv->Release();
			pID3DBlob_Error_pv = NULL;
			return(hr);
		}
	}
	else
	{
		fopen_s(&gpfile_ps, gszLogFileName_ps, "a+");
		fprintf_s(gpfile_ps, "D3DCompile() Succeeded For Vertex Shader Compilation\n");
		fclose(gpfile_ps);
	}

	// Create vertex shader object
	hr = gpID3D11Device_ps->CreateVertexShader(pID3DBlob_VertexShaderCode_pv->GetBufferPointer(),
		pID3DBlob_VertexShaderCode_pv->GetBufferSize(),
		NULL,
		&gpID3D11VertexShader_PerVertex);

	// Create vertex shader error checking
	if (FAILED(hr))
	{
		fopen_s(&gpfile_ps, gszLogFileName_ps, "a+");
		fprintf_s(gpfile_ps, "ID3D11Device_ps::CreateVertexShader() Failed for per vertex lighting.\n");
		fclose(gpfile_ps);
		return(hr);
	}
	else
	{
		fopen_s(&gpfile_ps, gszLogFileName_ps, "a+");
		fprintf_s(gpfile_ps, "ID3D11Device_ps::CreateVertexShader() Succeeded for per vertex lighting.\n");
		fclose(gpfile_ps);
	}

	// Set vertex shader to the pipeline
	gpID3D11DeviceContext_ps->VSSetShader(gpID3D11VertexShader_PerVertex, NULL, NULL);
	//------------------------------vertex shader ends-------------------------------//

	// ***** PIXEL SHADER *****	
	// Pixel shader Source code
	const char *pixelShaderSourceCode_pv =
		"	float4 main(float4 pos : SV_POSITION, float4 diffuse_light_color : COLOR) : SV_TARGET" \
		"	{" \
		"		float4 color = diffuse_light_color;"	\
		"		return(color);" \
		"	}";

	// variables to hold errors and source code
	ID3DBlob *pID3DBlob_PixelShaderCode_pv;
	pID3DBlob_Error_pv = NULL;

	// Compile Pixel shader
	hr = D3DCompile(pixelShaderSourceCode_pv,
		lstrlenA(pixelShaderSourceCode_pv) + 1,
		"PS",
		NULL,
		D3D_COMPILE_STANDARD_FILE_INCLUDE,
		"main",
		"ps_5_0",
		0,
		0,
		&pID3DBlob_PixelShaderCode_pv,
		&pID3DBlob_Error_pv);

	// Pixel shader compilation error checking
	if (FAILED(hr))
	{
		fopen_s(&gpfile_ps, gszLogFileName_ps, "a+");
		fprintf_s(gpfile_ps, "D3DCompile() Failed For Pixel Shader: %s. \n", (char *)pID3DBlob_Error_pv->GetBufferPointer());
		fclose(gpfile_ps);
		pID3DBlob_Error_pv->Release();
		pID3DBlob_Error_pv = NULL;
		return(hr);
	}
	else
	{
		fopen_s(&gpfile_ps, gszLogFileName_ps, "a+");
		fprintf_s(gpfile_ps, "D3DCompile() Succeeded For Pixel Shader Compilation\n");
		fclose(gpfile_ps);
	}

	// Create Pixel shader object
	hr = gpID3D11Device_ps->CreatePixelShader(pID3DBlob_PixelShaderCode_pv->GetBufferPointer(),
		pID3DBlob_PixelShaderCode_pv->GetBufferSize(),
		NULL,
		&gpID3D11PixelShader_PerVertex);

	// Create Pixel shader error checking
	if (FAILED(hr))
	{
		fopen_s(&gpfile_ps, gszLogFileName_ps, "a+");
		fprintf_s(gpfile_ps, "ID3D11Device_ps::CreatePixelShader() Failed for per vertex lighting.\n");
		fclose(gpfile_ps);
		return(hr);
	}
	else
	{
		fopen_s(&gpfile_ps, gszLogFileName_ps, "a+");
		fprintf_s(gpfile_ps, "ID3D11Device_ps::CreatePixelShader() Succeeded for per vertex lighting.\n");
		fclose(gpfile_ps);
	}

	// Set pixel shader to the pipeline
	gpID3D11DeviceContext_ps->PSSetShader(gpID3D11PixelShader_PerVertex, 0, 0);
	//----------------------pixel shader ends------------------------//
	
	//--------------------------------------------------- PER PIXEL ----------------------------------------
	// ***** VERTEX SHADER FOR PER PIXEL LIGHTING *****
	// Vertex Shader Source Code
	const char *vertexShaderSourceCode_pp =
		"cbuffer ConstantBuffer											" \
		"{																" \
		"	float4x4 worldMatrix;							" \
		"	float4x4 viewMatrix;							" \
		"	float4x4 projectionMatrix;" \
		"	float4 la;"	\
		"	float4 ld;"	\
		"	float4 ls;"	\
		"	float4 ka;"	\
		"	float4 kd;"	\
		"	float4 ks;"	\
		"	float4 lightPosition;"	\
		"	float materialShininess;"	\
		"	uint keyPressed;"	\
		"}" \
		"struct vertex_output	" \
		"{"\
		"	float4 position : SV_POSITION;" \
		"	float3 tn : NORMAL0;" \
		"	float3 ld : NORMAL1;" \
		"	float3 vv : NORMAL2;" \
		"};"
		"vertex_output main(float4 pos : POSITION, float4 normal : NORMAL)" \
		"{																" \
		"	vertex_output output;"	\
		"	if(keyPressed == 1)"	\
		"	{"	\
		"		float4 eyeCoordinates = mul(worldMatrix, pos);"	\
		"		eyeCoordinates = mul(viewMatrix, eyeCoordinates);"	\
		"		output.tn = (mul((float3x3)worldMatrix, (float3)normal));"	\
		"		output.ld = (float3)(lightPosition - eyeCoordinates);"	\
		"		output.vv = (-eyeCoordinates.xyz);"	\
		"	}"	\
		"	float4 Position = mul(worldMatrix, pos);"	\
		"	Position = mul(viewMatrix, Position);"	\
		"	Position = mul(projectionMatrix, Position);"	\
		"	output.position = Position;" \
		"	return(output);" \
		"}";

	// variables to hold errors and source code
	ID3DBlob *pID3DBlob_VertexShaderCode_pp = NULL;
	ID3DBlob *pID3DBlob_Error_pp = NULL;

	// Compile vertex shader
	hr = D3DCompile(vertexShaderSourceCode_pp,
		lstrlenA(vertexShaderSourceCode_pp) + 1,
		"VS",
		NULL,
		D3D_COMPILE_STANDARD_FILE_INCLUDE,
		"main",
		"vs_5_0",
		0,
		0,
		&pID3DBlob_VertexShaderCode_pp,
		&pID3DBlob_Error_pp);

	// Compilation error checking
	if (FAILED(hr))
	{
		if (pID3DBlob_Error_pp != NULL)
		{
			fopen_s(&gpfile_ps, gszLogFileName_ps, "a+");
			fprintf_s(gpfile_ps, "D3DCompile() Failed For Vertex Shader: %s. \n", (char *)pID3DBlob_Error_pp->GetBufferPointer());
			fclose(gpfile_ps);
			pID3DBlob_Error_pp->Release();
			pID3DBlob_Error_pp = NULL;
			return(hr);
		}
	}
	else
	{
		fopen_s(&gpfile_ps, gszLogFileName_ps, "a+");
		fprintf_s(gpfile_ps, "D3DCompile() Succeeded For Vertex Shader Compilation\n");
		fclose(gpfile_ps);
	}

	// Create vertex shader object
	hr = gpID3D11Device_ps->CreateVertexShader(pID3DBlob_VertexShaderCode_pp->GetBufferPointer(),
		pID3DBlob_VertexShaderCode_pp->GetBufferSize(),
		NULL,
		&gpID3D11VertexShader_PerPixel);

	// Create vertex shader error checking
	if (FAILED(hr))
	{
		fopen_s(&gpfile_ps, gszLogFileName_ps, "a+");
		fprintf_s(gpfile_ps, "ID3D11Device_ps::CreateVertexShader() Failed.\n");
		fclose(gpfile_ps);
		return(hr);
	}
	else
	{
		fopen_s(&gpfile_ps, gszLogFileName_ps, "a+");
		fprintf_s(gpfile_ps, "ID3D11Device_ps::CreateVertexShader() Succeeded.\n");
		fclose(gpfile_ps);
	}

	// Set vertex shader to the pipeline
	gpID3D11DeviceContext_ps->VSSetShader(gpID3D11VertexShader_PerPixel, NULL, NULL);
	//------------------------------vertex shader ends-------------------------------//


	// ***** PIXEL SHADER FOR PER PIXEL LIGHTING *****	
	// Pixel shader Source code
	const char *pixelShaderSourceCode_pp =
		"cbuffer ConstantBuffer											" \
		"{																" \
		"	float4x4 worldMatrix;							" \
		"	float4x4 viewMatrix;							" \
		"	float4x4 projectionMatrix;" \
		"	float4 la;"	\
		"	float4 ld;"	\
		"	float4 ls;"	\
		"	float4 ka;"	\
		"	float4 kd;"	\
		"	float4 ks;"	\
		"	float4 lightPosition;"	\
		"	float materialShininess;"	\
		"	uint keyPressed;"	\
		"}" \
		"struct vertex_output	" \
		"{"\
		"	float4 position : SV_POSITION;" \
		"	float3 tn : NORMAL0;" \
		"	float3 ld : NORMAL1;" \
		"	float3 vv : NORMAL2;" \
		"};"
		"	float4 main(vertex_output input) : SV_TARGET" \
		"	{" \
		"		float4 phong_ads_light;" \
		"		if(keyPressed == 1)" \
		"		{" \
		"float3 norm_tn = normalize(input.tn);" \
		"float3 norm_ld = normalize(input.ld);" \
		"float3 norm_vv = normalize(input.vv);" \
		"float3 ref_vec = normalize(reflect(-norm_ld, norm_tn));" \
		"float tn_dot_ld = max(dot(norm_tn, norm_ld), 0.0);" \
		"float rv_dot_vv = max(dot(norm_vv, ref_vec), 0.0);" \
		"float4 ambient = la * ka;" \
		"float4 diffuse = ld * kd * tn_dot_ld;"
		"float4 specular = ls * ks * pow(rv_dot_vv, materialShininess);"
		"phong_ads_light = ambient+diffuse+specular;" \
		"}"
		"else" \
		"{" \
		"phong_ads_light = float4(1.0,1.0,1.0,1.0);" \
		"}" \
		"return(phong_ads_light);" \
		"}";

	// variables to hold errors and source code
	ID3DBlob *pID3DBlob_PixelShaderCode_pp;
	pID3DBlob_Error_pp = NULL;

	// Compile Pixel shader
	hr = D3DCompile(pixelShaderSourceCode_pp,
		lstrlenA(pixelShaderSourceCode_pp) + 1,
		"PS",
		NULL,
		D3D_COMPILE_STANDARD_FILE_INCLUDE,
		"main",
		"ps_5_0",
		0,
		0,
		&pID3DBlob_PixelShaderCode_pp,
		&pID3DBlob_Error_pp);

	// Pixel shader compilation error checking
	if (FAILED(hr))
	{
		fopen_s(&gpfile_ps, gszLogFileName_ps, "a+");
		fprintf_s(gpfile_ps, "D3DCompile() Failed For Pixel Shader: %s. \n", (char *)pID3DBlob_Error_pp->GetBufferPointer());
		fclose(gpfile_ps);
		pID3DBlob_Error_pp->Release();
		pID3DBlob_Error_pp = NULL;
		return(hr);
	}
	else
	{
		fopen_s(&gpfile_ps, gszLogFileName_ps, "a+");
		fprintf_s(gpfile_ps, "D3DCompile() Succeeded For Pixel Shader Compilation\n");
		fclose(gpfile_ps);
	}

	// Create Pixel shader object
	hr = gpID3D11Device_ps->CreatePixelShader(pID3DBlob_PixelShaderCode_pp->GetBufferPointer(),
		pID3DBlob_PixelShaderCode_pp->GetBufferSize(),
		NULL,
		&gpID3D11PixelShader_PerPixel);

	// Create Pixel shader error checking
	if (FAILED(hr))
	{
		fopen_s(&gpfile_ps, gszLogFileName_ps, "a+");
		fprintf_s(gpfile_ps, "ID3D11Device_ps::CreatePixelShader() Failed.\n");
		fclose(gpfile_ps);
		return(hr);
	}
	else
	{
		fopen_s(&gpfile_ps, gszLogFileName_ps, "a+");
		fprintf_s(gpfile_ps, "ID3D11Device_ps::CreatePixelShader() Succeeded.\n");
		fclose(gpfile_ps);
	}

	// Set pixel shader to the pipeline
	gpID3D11DeviceContext_ps->PSSetShader(gpID3D11PixelShader_PerPixel, 0, 0);
	//----------------------pixel shader ends------------------------//

	// ******************** INPUT LAYOUT ***********************
	// create and set input layout
	D3D11_INPUT_ELEMENT_DESC inputElementDesc[2];

	// zero out the structure
	ZeroMemory(inputElementDesc, 2 * sizeof(D3D11_INPUT_ELEMENT_DESC));

	// fill structure members
	inputElementDesc[0].SemanticName = "POSITION";			// Whatever written in shader after pos :
	inputElementDesc[0].SemanticIndex = 0;					// Index of POSITION semantic you want to assign, legacy(0 to position)
	inputElementDesc[0].Format = DXGI_FORMAT_R32G32B32_FLOAT;	// Position data  3 for x, y, z
	inputElementDesc[0].InputSlot = 0;						// Layout qualifier
	inputElementDesc[0].AlignedByteOffset = 0;				// Tightly Packed Data therefore 0
	inputElementDesc[0].InstanceDataStepRate = 0;
	inputElementDesc[0].InputSlotClass = D3D11_INPUT_PER_VERTEX_DATA;

	// fill structure members
	inputElementDesc[1].SemanticName = "NORMAL";			// Whatever written in shader after pos :
	inputElementDesc[1].SemanticIndex = 0;					// Index of NORMAL semantic you want to assign, legacy(1 to normal)
	inputElementDesc[1].Format = DXGI_FORMAT_R32G32B32_FLOAT;	// Position data  3 for x, y, z
	inputElementDesc[1].InputSlot = 1;						// Layout qualifier
	inputElementDesc[1].AlignedByteOffset = 0;				// Tightly Packed Data therefore 0
	inputElementDesc[1].InstanceDataStepRate = 0;
	inputElementDesc[1].InputSlotClass = D3D11_INPUT_PER_VERTEX_DATA;

	// create input layout for  PER PIXEL
	hr = gpID3D11Device_ps->CreateInputLayout(inputElementDesc, 	// 1st parameter can take array 
		_ARRAYSIZE(inputElementDesc),								// if array, no of element in array
		pID3DBlob_VertexShaderCode_pp->GetBufferPointer(),				// specify shader code
		pID3DBlob_VertexShaderCode_pp->GetBufferSize(),					// target shader code size
		&gpID3D11InputLayout);										// pointer to input layout

	// error checking of input layout creation
	if (FAILED(hr))
	{
		fopen_s(&gpfile_ps, gszLogFileName_ps, "a+");
		fprintf_s(gpfile_ps, "ID3D11DEVICE::CreateInputLayout() Failed.\n");
		fclose(gpfile_ps);
		return(hr);
	}
	else
	{
		fopen_s(&gpfile_ps, gszLogFileName_ps, "a+");
		fprintf_s(gpfile_ps, "ID3D11DEVICE::CreateInputLayout() Succeeded.\n");
		fclose(gpfile_ps);
	}

	// set input layout to the pipeline
	gpID3D11DeviceContext_ps->IASetInputLayout(gpID3D11InputLayout);

	// release the blob for shaders here
	pID3DBlob_VertexShaderCode_pp->Release();
	pID3DBlob_VertexShaderCode_pp = NULL;
	pID3DBlob_PixelShaderCode_pp->Release();
	pID3DBlob_PixelShaderCode_pp = NULL;

	// create input layout for  PER VERTEX
	hr = gpID3D11Device_ps->CreateInputLayout(inputElementDesc, 	// 1st parameter can take array 
		_ARRAYSIZE(inputElementDesc),								// if array, no of element in array
		pID3DBlob_VertexShaderCode_pv->GetBufferPointer(),				// specify shader code
		pID3DBlob_VertexShaderCode_pv->GetBufferSize(),					// target shader code size
		&gpID3D11InputLayout);										// pointer to input layout

	// error checking of input layout creation
	if (FAILED(hr))
	{
		fopen_s(&gpfile_ps, gszLogFileName_ps, "a+");
		fprintf_s(gpfile_ps, "ID3D11DEVICE::CreateInputLayout() Failed.\n");
		fclose(gpfile_ps);
		return(hr);
	}
	else
	{
		fopen_s(&gpfile_ps, gszLogFileName_ps, "a+");
		fprintf_s(gpfile_ps, "ID3D11DEVICE::CreateInputLayout() Succeeded.\n");
		fclose(gpfile_ps);
	}

	// set input layout to the pipeline
	gpID3D11DeviceContext_ps->IASetInputLayout(gpID3D11InputLayout);

	// release the blob for shaders here
	pID3DBlob_VertexShaderCode_pv->Release();
	pID3DBlob_VertexShaderCode_pv = NULL;
	pID3DBlob_PixelShaderCode_pv->Release();
	pID3DBlob_PixelShaderCode_pv = NULL;
	// --------------------------------- input layout ends -------------------------------

	// ************************ VERTEX BUFFER ****************************

	// ************************ LOAD SPHERE DATA *************************
	getSphereVertexData(sphere_vertices, sphere_normals, sphere_textures, sphere_elements);
	gNumVertices = getNumberOfSphereVertices();
	gNumElements = getNumberOfSphereElements();

	// create vertex buffer
	D3D11_BUFFER_DESC bufferDesc_VertexBuffer_Sphere_Vertex;
	D3D11_BUFFER_DESC bufferDesc_VertexBuffer_Sphere_Normal;
	D3D11_BUFFER_DESC bufferDesc_VertexBuffer_Sphere_Indices;

	// zero out the memory
	ZeroMemory(&bufferDesc_VertexBuffer_Sphere_Vertex, sizeof(D3D11_BUFFER_DESC));
	ZeroMemory(&bufferDesc_VertexBuffer_Sphere_Normal, sizeof(D3D11_BUFFER_DESC));
	ZeroMemory(&bufferDesc_VertexBuffer_Sphere_Indices, sizeof(D3D11_BUFFER_DESC));

	// fill the structure members ( 4 out of 6) for rectangle
	bufferDesc_VertexBuffer_Sphere_Vertex.Usage = D3D11_USAGE_DYNAMIC;
	bufferDesc_VertexBuffer_Sphere_Vertex.ByteWidth = sizeof(float) * ARRAYSIZE(sphere_vertices);
	bufferDesc_VertexBuffer_Sphere_Vertex.BindFlags = D3D11_BIND_VERTEX_BUFFER;
	bufferDesc_VertexBuffer_Sphere_Vertex.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;

	// fill the structure members ( 4 out of 6) for rectangle
	bufferDesc_VertexBuffer_Sphere_Normal.Usage = D3D11_USAGE_DYNAMIC;
	bufferDesc_VertexBuffer_Sphere_Normal.ByteWidth = sizeof(float) * ARRAYSIZE(sphere_normals);
	bufferDesc_VertexBuffer_Sphere_Normal.BindFlags = D3D11_BIND_VERTEX_BUFFER;
	bufferDesc_VertexBuffer_Sphere_Normal.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;

	// fill the structure members ( 4 out of 6) for rectangle
	bufferDesc_VertexBuffer_Sphere_Indices.Usage = D3D11_USAGE_DYNAMIC;
	bufferDesc_VertexBuffer_Sphere_Indices.ByteWidth = sizeof(short) * gNumElements;
	bufferDesc_VertexBuffer_Sphere_Indices.BindFlags = D3D11_BIND_INDEX_BUFFER;
	bufferDesc_VertexBuffer_Sphere_Indices.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;

	// create buffer for sphere position
	hr = gpID3D11Device_ps->CreateBuffer(&bufferDesc_VertexBuffer_Sphere_Vertex, NULL, &gpID3D11Buffer_VertexBuffer_Position_Sphere);

	// buffer creation error checking
	if (FAILED(hr))
	{
		fopen_s(&gpfile_ps, gszLogFileName_ps, "a+");
		fprintf_s(gpfile_ps, "ID3D11Device::CreateBuffer() Failed For Sphere Position Vertex Buffer.\n");
		fclose(gpfile_ps);
		return(hr);
	}
	else
	{
		fopen_s(&gpfile_ps, gszLogFileName_ps, "a+");
		fprintf_s(gpfile_ps, "ID3D11Device::CreateBuffer() Succeeded For Sphere Position Vertex Buffer.\n");
		fclose(gpfile_ps);
	}

	// create buffer for sphere normal
	hr = gpID3D11Device_ps->CreateBuffer(&bufferDesc_VertexBuffer_Sphere_Normal, NULL, &gpID3D11Buffer_VertexBuffer_Normal_Sphere);

	// buffer creation error checking
	if (FAILED(hr))
	{
		fopen_s(&gpfile_ps, gszLogFileName_ps, "a+");
		fprintf_s(gpfile_ps, "ID3D11Device::CreateBuffer() Failed For  Sphere Normal Vertex Buffer.\n");
		fclose(gpfile_ps);
		return(hr);
	}
	else
	{
		fopen_s(&gpfile_ps, gszLogFileName_ps, "a+");
		fprintf_s(gpfile_ps, "ID3D11Device::CreateBuffer() Succeeded For Sphere Normal Vertex Buffer.\n");
		fclose(gpfile_ps);
	}

	// create buffer for sphere indices
	hr = gpID3D11Device_ps->CreateBuffer(&bufferDesc_VertexBuffer_Sphere_Indices, NULL, &gpID3D11Buffer_IndexBuffer);

	// buffer creation error checking
	if (FAILED(hr))
	{
		fopen_s(&gpfile_ps, gszLogFileName_ps, "a+");
		fprintf_s(gpfile_ps, "ID3D11Device::CreateBuffer() Failed For  Sphere Index Buffer.\n");
		fclose(gpfile_ps);
		return(hr);
	}
	else
	{
		fopen_s(&gpfile_ps, gszLogFileName_ps, "a+");
		fprintf_s(gpfile_ps, "ID3D11Device::CreateBuffer() Succeeded For Sphere Index Buffer.\n");
		fclose(gpfile_ps);
	}


	// copy sphere vertices into above buffer	
	D3D11_MAPPED_SUBRESOURCE mappedSubresource;

	// zero out the memory
	ZeroMemory(&mappedSubresource, sizeof(D3D11_MAPPED_SUBRESOURCE));

	// mapping
	gpID3D11DeviceContext_ps->Map(gpID3D11Buffer_VertexBuffer_Position_Sphere, 		// GPU memory buffer
		0,															// Byte Offset Starting
		D3D11_MAP_WRITE_DISCARD,									// permission
		0,															// decides what cpu will do while mapping takes place,
															// DirectX takes decision to make call synchronusly or asyncronously
		&mappedSubresource);
	memcpy(mappedSubresource.pData, sphere_vertices, sizeof(sphere_vertices));

	// ummap
	gpID3D11DeviceContext_ps->Unmap(gpID3D11Buffer_VertexBuffer_Position_Sphere, 0);

	// copy sphere normal
	// zero out the memory
	ZeroMemory(&mappedSubresource, sizeof(D3D11_MAPPED_SUBRESOURCE));

	// mapping
	gpID3D11DeviceContext_ps->Map(gpID3D11Buffer_VertexBuffer_Normal_Sphere, 		// GPU memory buffer
		0,															// Byte Offset Starting
		D3D11_MAP_WRITE_DISCARD,									// permission
		0,															// decides what cpu will do while mapping takes place,
																	// DirectX takes decision to make call synchronusly or asyncronously
		&mappedSubresource);
	memcpy(mappedSubresource.pData, sphere_normals, sizeof(sphere_normals));

	// ummap
	gpID3D11DeviceContext_ps->Unmap(gpID3D11Buffer_VertexBuffer_Normal_Sphere, 0);

	// copy sphere indices
	// zero out the memory
	ZeroMemory(&mappedSubresource, sizeof(D3D11_MAPPED_SUBRESOURCE));

	// mapping
	gpID3D11DeviceContext_ps->Map(gpID3D11Buffer_IndexBuffer, 		// GPU memory buffer
		0,															// Byte Offset Starting
		D3D11_MAP_WRITE_DISCARD,									// permission
		0,															// decides what cpu will do while mapping takes place,
																	// DirectX takes decision to make call synchronusly or asyncronously
		&mappedSubresource);
	memcpy(mappedSubresource.pData, sphere_elements, gNumElements * sizeof(short));

	// ummap
	gpID3D11DeviceContext_ps->Unmap(gpID3D11Buffer_IndexBuffer, 0);


	// *************************** CONSTANT BUFFER *******************************
	// constant buffer
	D3D11_BUFFER_DESC bufferDesc_ConstantBuffer;

	// zero out the memory
	ZeroMemory(&bufferDesc_ConstantBuffer, sizeof(D3D11_BUFFER_DESC));

	// fill the structure member
	bufferDesc_ConstantBuffer.Usage = D3D11_USAGE_DEFAULT;
	bufferDesc_ConstantBuffer.ByteWidth = sizeof(CBUFFER);
	bufferDesc_ConstantBuffer.BindFlags = D3D11_BIND_CONSTANT_BUFFER;

	//create buffer
	hr = gpID3D11Device_ps->CreateBuffer(&bufferDesc_ConstantBuffer, 0, &gpID3D11Buffer_ConstantBuffer);

	// constant buffer creation error checking
	if (FAILED(hr))
	{
		fopen_s(&gpfile_ps, gszLogFileName_ps, "a+");
		fprintf_s(gpfile_ps, "ID3D11Device::CreateBuffer() Failed For Constant Buffer.\n");
		fclose(gpfile_ps);
		return(hr);
	}
	else
	{
		fopen_s(&gpfile_ps, gszLogFileName_ps, "a+");
		fprintf_s(gpfile_ps, "ID3D11Device::CreateBuffer() Succeeded For Constant Buffer.\n");
		fclose(gpfile_ps);
	}

	gpID3D11DeviceContext_ps->VSSetConstantBuffers(0, 1, &gpID3D11Buffer_ConstantBuffer);
	gpID3D11DeviceContext_ps->PSSetConstantBuffers(0, 1, &gpID3D11Buffer_ConstantBuffer);

	// set rasterization state
	// in D3D, backface culling is by default 'ON'
	// means backface of geometry will not be visible.
	// this causes our 2d shape backface to vanish on rotation.
	// so set culling 'OFF'

	D3D11_RASTERIZER_DESC rasterizerDesc;
	ZeroMemory((void *)&rasterizerDesc, sizeof(D3D11_RASTERIZER_DESC));

	rasterizerDesc.AntialiasedLineEnable = FALSE;
	rasterizerDesc.CullMode = D3D11_CULL_NONE;
	rasterizerDesc.DepthBias = 0;
	rasterizerDesc.DepthBiasClamp = 0.0f;
	rasterizerDesc.DepthClipEnable = TRUE;
	rasterizerDesc.FillMode = D3D11_FILL_SOLID;
	rasterizerDesc.FrontCounterClockwise = FALSE;
	rasterizerDesc.MultisampleEnable = FALSE;
	rasterizerDesc.ScissorEnable = FALSE;
	rasterizerDesc.SlopeScaledDepthBias = 0.0f;

	hr = gpID3D11Device_ps->CreateRasterizerState(&rasterizerDesc, &gpID3D11RasterizerState);
	if (FAILED(hr))
	{
		fopen_s(&gpfile_ps, gszLogFileName_ps, "a+");
		fprintf_s(gpfile_ps, "ID3D11Device::CreateRasterizerState() Failed For Culling.\n");
		fclose(gpfile_ps);
		return(hr);
	}
	else
	{
		fopen_s(&gpfile_ps, gszLogFileName_ps, "a+");
		fprintf_s(gpfile_ps, "ID3D11Device::CreateRasterizerState() Succedeed For Culling.\n");
		fclose(gpfile_ps);
	}

	gpID3D11DeviceContext_ps->RSSetState(gpID3D11RasterizerState);

	/* clear color ( blue)*/
	gClearColor[0] = 0.0f;
	gClearColor[1] = 0.0f;
	gClearColor[2] = 1.0f;
	gClearColor[3] = 1.0f;

	//set projection matrix to identity matrix
	gPerspectiveProjectionMatrix = XMMatrixIdentity();

	// call resize for first tym
	hr = resize(WIN_WIDTH, WIN_HEIGHT);
	if (FAILED(hr))
	{
		fopen_s(&gpfile_ps, gszLogFileName_ps, "a+");
		fprintf_s(gpfile_ps, "resize() failed.\n ");
		fclose(gpfile_ps);
		return(hr);
	}
	else
	{
		fopen_s(&gpfile_ps, gszLogFileName_ps, "a+");
		fprintf_s(gpfile_ps, "resize() successful.\n ");
		fclose(gpfile_ps);
	}

	return(S_OK);
}

HRESULT resize(int width, int height)
{
	// code
	HRESULT hr = S_OK;

	gWidth = width;
	gHeight = height;

	// free any sizedependent resources
	if (gpID3D11RenderTargetView_ps)
	{
		gpID3D11RenderTargetView_ps->Release();
		gpID3D11RenderTargetView_ps = NULL;
	}

	if (gpID3D11DepthStencilView)
	{
		gpID3D11DepthStencilView->Release();
		gpID3D11DepthStencilView = NULL;
	}

	// resize swap chain buffers accordingly
	gpIDXGISwapChain_ps->ResizeBuffers(1, width, height, DXGI_FORMAT_B8G8R8A8_UNORM, 0);

	// again get back buffer from swap chain
	ID3D11Texture2D *pID3D11Texture2D_BackBuffer;
	gpIDXGISwapChain_ps->GetBuffer(0, __uuidof(ID3D11Texture2D), (LPVOID*)&pID3D11Texture2D_BackBuffer);

	// again get render target view from d3d11 device using above back buffer
	hr = gpID3D11Device_ps->CreateRenderTargetView(pID3D11Texture2D_BackBuffer, NULL, &gpID3D11RenderTargetView_ps);
	if (FAILED(hr))
	{
		fopen_s(&gpfile_ps, gszLogFileName_ps, "a+");
		fprintf_s(gpfile_ps, "CreateRenderTargetView() failed.\n ");
		fclose(gpfile_ps);
		return(hr);
	}
	else
	{
		fopen_s(&gpfile_ps, gszLogFileName_ps, "a+");
		fprintf_s(gpfile_ps, "CreateRenderTargetView() successful.\n ");
		fclose(gpfile_ps);
	}

	pID3D11Texture2D_BackBuffer->Release();
	pID3D11Texture2D_BackBuffer = NULL;

	// create depth stencil buffer ( or zbuffer)
	D3D11_TEXTURE2D_DESC textureDesc;

	ZeroMemory(&textureDesc, sizeof(D3D11_TEXTURE2D_DESC));

	textureDesc.Width = (UINT)width;
	textureDesc.Height = (UINT)height;
	textureDesc.ArraySize = 1;
	textureDesc.MipLevels = 1;
	textureDesc.SampleDesc.Count = 1;
	textureDesc.SampleDesc.Quality = 0;
	textureDesc.Format = DXGI_FORMAT_D32_FLOAT;
	textureDesc.Usage = D3D11_USAGE_DEFAULT;
	textureDesc.BindFlags = D3D11_BIND_DEPTH_STENCIL;
	textureDesc.CPUAccessFlags = 0;
	textureDesc.MiscFlags = 0;

	ID3D11Texture2D *pID3D11Texture2D_DepthBuffer;

	// create texture buffer as depth buffer
	gpID3D11Device_ps->CreateTexture2D(&textureDesc, NULL, &pID3D11Texture2D_DepthBuffer);

	// create depth stencil view from above depth stencil buffer
	D3D11_DEPTH_STENCIL_VIEW_DESC depthStencilViewDesc;

	// zero out the memory
	ZeroMemory(&depthStencilViewDesc, sizeof(D3D11_DEPTH_STENCIL_VIEW_DESC));

	// fill the structure members
	depthStencilViewDesc.Format = DXGI_FORMAT_D32_FLOAT;
	depthStencilViewDesc.ViewDimension = D3D11_DSV_DIMENSION_TEXTURE2DMS;

	// create depth stencil view
	hr = gpID3D11Device_ps->CreateDepthStencilView(pID3D11Texture2D_DepthBuffer, &depthStencilViewDesc, &gpID3D11DepthStencilView);

	// error checking
	if (FAILED(hr))
	{
		fopen_s(&gpfile_ps, gszLogFileName_ps, "a+");
		fprintf_s(gpfile_ps, "ID3D11Device::CreateDepthStencilView() failed.\n ");
		fclose(gpfile_ps);
		return(hr);
	}
	else
	{
		fopen_s(&gpfile_ps, gszLogFileName_ps, "a+");
		fprintf_s(gpfile_ps, "ID3D11Device::CreateDepthStencilView() successful.\n ");
		fclose(gpfile_ps);
	}
	pID3D11Texture2D_DepthBuffer->Release();
	pID3D11Texture2D_DepthBuffer = NULL;

	// set render target view as render target with depth stencil view
	gpID3D11DeviceContext_ps->OMSetRenderTargets(1, &gpID3D11RenderTargetView_ps, gpID3D11DepthStencilView);

	

	// set perspective matrix
	gPerspectiveProjectionMatrix = XMMatrixPerspectiveFovLH(XMConvertToRadians(45.0f), (float)width / (float)height, 0.1f, 100.0f);

	return(hr);
}

void update(void)
{
	if (gbLight_ps == true)
		angle = angle + 0.002f;
	else
		angle = 0.0f;
}

void display(void)
{
	// code
	// function declarations
	void initSpheres(void);

	// clear render target view to a chosen color
	gpID3D11DeviceContext_ps->ClearRenderTargetView(gpID3D11RenderTargetView_ps, gClearColor);

	// clear depth/stencil view to 1.0
	gpID3D11DeviceContext_ps->ClearDepthStencilView(gpID3D11DepthStencilView, D3D11_CLEAR_DEPTH, 1.0f, 0);

	//set viewport
	D3D11_VIEWPORT d3dViewPort;
	d3dViewPort.TopLeftX = 0;
	d3dViewPort.TopLeftY = 0;
	d3dViewPort.Width = (float)gWidth / 2;
	d3dViewPort.Height = (float)gHeight / 2;
	d3dViewPort.MinDepth = 0.0f;
	d3dViewPort.MaxDepth = 1.0f;
	gpID3D11DeviceContext_ps->RSSetViewports(1, &d3dViewPort);

	initSpheres();

	//select which vertex buffer to display
	UINT stride = sizeof(float) * 3;
	UINT offset = 0;

	// translation is concerned with world matrix transformation
	XMMATRIX translationMatrix = XMMatrixIdentity();
	XMMATRIX rotationMatrix = XMMatrixIdentity();
	XMMATRIX worldMatrix = XMMatrixIdentity();
	XMMATRIX viewMatrix = XMMatrixIdentity();
	XMMATRIX wvMatrix = XMMatrixIdentity();

	CBUFFER constantBuffer;


	// *************************** DRAW SPHERE *****************************
	gpID3D11DeviceContext_ps->IASetVertexBuffers(0, 1, &gpID3D11Buffer_VertexBuffer_Position_Sphere, &stride, &offset);
	gpID3D11DeviceContext_ps->IASetVertexBuffers(1, 1, &gpID3D11Buffer_VertexBuffer_Normal_Sphere, &stride, &offset);
	gpID3D11DeviceContext_ps->IASetIndexBuffer(gpID3D11Buffer_IndexBuffer, DXGI_FORMAT_R16_UINT, 0);	//R16 maps with short

	// select geometry primitive
	gpID3D11DeviceContext_ps->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLESTRIP);

	// final WorldViewProjection Matrix
	translationMatrix = XMMatrixTranslation(0.0f, 0.0f, 5.0f);

	worldMatrix = rotationMatrix * translationMatrix;
	wvMatrix = worldMatrix * viewMatrix;

	for(int i = 0; i<24; i++)
	{

		float c = 10.0f * cosf(angle);
		float s = 10.0f * sinf(angle);

		if (gbToggle_ps == false)		//per vertex
		{
			// load the data into constant buffer
			if (gbLight_ps == true)
			{
				if (axis == 1)
				{
					LightPosition[0] = 0.0f;
					LightPosition[1] = c;
					LightPosition[2] = s;
				}
				if (axis == 2)
				{
					LightPosition[0] = c;
					LightPosition[1] = 0.0f;
					LightPosition[2] = s;
				}
				if (axis == 3)
				{
					LightPosition[0] = c;
					LightPosition[1] = s;
					LightPosition[2] = 0.0f;
				}

				constantBuffer.La = XMVectorSet(LightAmbient[0], LightAmbient[1], LightAmbient[2], LightAmbient[3]);
				constantBuffer.Ld = XMVectorSet(LightDiffuse[0], LightDiffuse[1], LightDiffuse[2], LightDiffuse[3]);
				constantBuffer.Ls = XMVectorSet(LightSpecular[0], LightSpecular[1], LightSpecular[2], LightSpecular[3]);
				constantBuffer.Ka = XMVectorSet(Spheres[i].MAmbient[0], Spheres[i].MAmbient[1], Spheres[i].MAmbient[2], Spheres[i].MAmbient[3]);
				constantBuffer.Kd = XMVectorSet(Spheres[i].MDiffuse[0], Spheres[i].MDiffuse[1], Spheres[i].MDiffuse[2], Spheres[i].MDiffuse[3]);
				constantBuffer.Ks = XMVectorSet(Spheres[i].MSpecular[0], Spheres[i].MSpecular[1], Spheres[i].MSpecular[2], Spheres[i].MSpecular[3]);
				constantBuffer.MaterialShininess = Spheres[i].sphereShininess;
				constantBuffer.LightPosition = XMVectorSet(LightPosition[0], LightPosition[1], LightPosition[2], LightPosition[3]);
				constantBuffer.KeyPressed = 1;

			}
			else
			{
				constantBuffer.KeyPressed = 0;
			}
			constantBuffer.WorldMatrix = worldMatrix;
			constantBuffer.ViewMatrix = viewMatrix;
			constantBuffer.ProjectionMatrix = gPerspectiveProjectionMatrix;

			gpID3D11DeviceContext_ps->UpdateSubresource(gpID3D11Buffer_ConstantBuffer,
				0,
				NULL,
				&constantBuffer,
				0,
				0);

			//set viewport 
			d3dViewPort.TopLeftX = Spheres[i].SViewport[0];
			d3dViewPort.TopLeftY = Spheres[i].SViewport[1];

			gpID3D11DeviceContext_ps->RSSetViewports(1, &d3dViewPort);

			// set shader
			gpID3D11DeviceContext_ps->VSSetShader(gpID3D11VertexShader_PerVertex, NULL, NULL);
			gpID3D11DeviceContext_ps->PSSetShader(gpID3D11PixelShader_PerVertex, NULL, NULL);
			gpID3D11DeviceContext_ps->IASetInputLayout(gpID3D11InputLayout);
			gpID3D11DeviceContext_ps->VSSetConstantBuffers(0, 1, &gpID3D11Buffer_ConstantBuffer);

			// draw vertex buffer to render target
			gpID3D11DeviceContext_ps->DrawIndexed(gNumElements, 0, 0);
		}
		else							// per pixel
		{
			// load the data into constant buffer
			if (gbLight_ps == true)
			{
				if (axis == 1)
				{
					LightPosition[0] = 0.0f;
					LightPosition[1] = c;
					LightPosition[2] = s;
				}
				if (axis == 2)
				{
					LightPosition[0] = c;
					LightPosition[1] = 0.0f;
					LightPosition[2] = s;
				}
				if (axis == 3)
				{
					LightPosition[0] = c;
					LightPosition[1] = s;
					LightPosition[2] = 0.0f;
				}

				constantBuffer.La = XMVectorSet(LightAmbient[0], LightAmbient[1], LightAmbient[2], LightAmbient[3]);
				constantBuffer.Ld = XMVectorSet(LightDiffuse[0], LightDiffuse[1], LightDiffuse[2], LightDiffuse[3]);
				constantBuffer.Ls = XMVectorSet(LightSpecular[0], LightSpecular[1], LightSpecular[2], LightSpecular[3]);


				constantBuffer.Ka = XMVectorSet(Spheres[i].MAmbient[0], Spheres[i].MAmbient[1], Spheres[i].MAmbient[2], Spheres[i].MAmbient[3]);
				constantBuffer.Kd = XMVectorSet(Spheres[i].MDiffuse[0], Spheres[i].MDiffuse[1], Spheres[i].MDiffuse[2], Spheres[i].MDiffuse[3]);
				constantBuffer.Ks = XMVectorSet(Spheres[i].MSpecular[0], Spheres[i].MSpecular[1], Spheres[i].MSpecular[2], Spheres[i].MSpecular[3]);
				constantBuffer.MaterialShininess = Spheres[i].sphereShininess;
				constantBuffer.LightPosition = XMVectorSet(LightPosition[0], LightPosition[1], LightPosition[2], LightPosition[3]);
				constantBuffer.KeyPressed = 1;

			}
			else
			{
				constantBuffer.KeyPressed = 0;
			}
			constantBuffer.WorldMatrix = worldMatrix;
			constantBuffer.ViewMatrix = viewMatrix;
			constantBuffer.ProjectionMatrix = gPerspectiveProjectionMatrix;

			gpID3D11DeviceContext_ps->UpdateSubresource(gpID3D11Buffer_ConstantBuffer,
				0,
				NULL,
				&constantBuffer,
				0,
				0);

			//set viewport 
			d3dViewPort.TopLeftX = Spheres[i].SViewport[0];
			d3dViewPort.TopLeftY = Spheres[i].SViewport[1];

			gpID3D11DeviceContext_ps->RSSetViewports(1, &d3dViewPort);

			// set shader
			gpID3D11DeviceContext_ps->VSSetShader(gpID3D11VertexShader_PerPixel, NULL, NULL);
			gpID3D11DeviceContext_ps->PSSetShader(gpID3D11PixelShader_PerPixel, NULL, NULL);
			gpID3D11DeviceContext_ps->IASetInputLayout(gpID3D11InputLayout);
			gpID3D11DeviceContext_ps->VSSetConstantBuffers(0, 1, &gpID3D11Buffer_ConstantBuffer);
			gpID3D11DeviceContext_ps->PSSetConstantBuffers(0, 1, &gpID3D11Buffer_ConstantBuffer);

			// draw vertex buffer to render target
			gpID3D11DeviceContext_ps->DrawIndexed(gNumElements, 0, 0);
		}
	}

	// switch between front and back buffers
	gpIDXGISwapChain_ps->Present(0, 0);
}

void initSpheres(void)
{
	//****************** 1st column GEMS***************
	// EMARALD
	Spheres[0].MAmbient[0] = 0.0215f;
	Spheres[0].MAmbient[1] = 0.1745f;
	Spheres[0].MAmbient[2] = 0.0215f;
	Spheres[0].MAmbient[3] = 1.0f;

	Spheres[0].MDiffuse[0] = 0.07568f;
	Spheres[0].MDiffuse[1] = 0.61424f;
	Spheres[0].MDiffuse[2] = 0.07568f;
	Spheres[0].MDiffuse[3] = 1.0f;

	Spheres[0].MSpecular[0] = 0.633f;
	Spheres[0].MSpecular[1] = 0.727811f;
	Spheres[0].MSpecular[2] = 0.633f;
	Spheres[0].MSpecular[3] = 1.0f;

	Spheres[0].SViewport[0] = 0;
	Spheres[0].SViewport[1] = -1 * gHeight / 6;
	Spheres[0].SViewport[2] = gWidth / 2;
	Spheres[0].SViewport[3] = gHeight / 2;
	
	Spheres[0].sphereShininess = 0.6f * 128.0f;

		// JADE
	Spheres[1].MAmbient[0] = 0.135f;
	Spheres[1].MAmbient[1] = 0.2225f;
	Spheres[1].MAmbient[2] = 0.1575f;
	Spheres[1].MAmbient[3] = 1.0f;

	Spheres[1].MDiffuse[0] = 0.54f;
	Spheres[1].MDiffuse[1] = 0.89f;
	Spheres[1].MDiffuse[2] = 0.63f;
	Spheres[1].MDiffuse[3] = 1.0f;

	Spheres[1].MSpecular[0] = 0.316228f;
	Spheres[1].MSpecular[1] = 0.316228f;
	Spheres[1].MSpecular[2] = 0.316228f;
	Spheres[1].MSpecular[3] = 1.0f;

	Spheres[1].SViewport[0] = 0;
	Spheres[1].SViewport[1] = 0;
	Spheres[1].SViewport[2] = gWidth / 2;
	Spheres[1].SViewport[3] = gHeight / 2;

	Spheres[1].sphereShininess = 0.1f * 128.0f;

		// OBSIDIAN
	Spheres[2].MAmbient[0] = 0.05375f;
	Spheres[2].MAmbient[1] = 0.05f;
	Spheres[2].MAmbient[2] = 0.06625f;
	Spheres[2].MAmbient[3] = 1.0f;

	Spheres[2].MDiffuse[0] = 0.18275f;
	Spheres[2].MDiffuse[1] = 0.17f;
	Spheres[2].MDiffuse[2] = 0.22525f;
	Spheres[2].MDiffuse[3] = 1.0f;

	Spheres[2].MSpecular[0] = 0.332741f;
	Spheres[2].MSpecular[1] = 0.328634f;
	Spheres[2].MSpecular[2] = 0.346435f;
	Spheres[2].MSpecular[3] = 1.0f;

		Spheres[2].SViewport[0] = 0;
		Spheres[2].SViewport[1] = 1 * gHeight / 6;
		Spheres[2].SViewport[2] = gWidth / 2;
		Spheres[2].SViewport[3] = gHeight / 2;

		Spheres[2].sphereShininess = 0.3f * 128.0f;

		// PEARL
		Spheres[3].MAmbient[0] = 0.25f;
		Spheres[3].MAmbient[1] = 0.20725f;
		Spheres[3].MAmbient[2] = 0.20725f;
		Spheres[3].MAmbient[3] = 1.0f;

		Spheres[3].MDiffuse[0] = 1.0f;
		Spheres[3].MDiffuse[1] = 0.829f;
		Spheres[3].MDiffuse[2] = 0.829f;
		Spheres[3].MDiffuse[3] = 1.0f;

		Spheres[3].MSpecular[0] = 0.296648f;
		Spheres[3].MSpecular[1] = 0.296648f;
		Spheres[3].MSpecular[2] = 0.296648f;
		Spheres[3].MSpecular[3] = 1.0f;

		Spheres[3].SViewport[0] = 0;
		Spheres[3].SViewport[1] = 2 *gHeight / 6;
		Spheres[3].SViewport[2] = gWidth / 2;
		Spheres[3].SViewport[3] = gHeight / 2;
		
		Spheres[3].sphereShininess = 0.88f * 128.0f;

		// RUBY
		Spheres[4].MAmbient[0] = 0.1745f;
		Spheres[4].MAmbient[1] = 0.01175f;
		Spheres[4].MAmbient[2] = 0.01175f;
		Spheres[4].MAmbient[3] = 1.0f;

		Spheres[4].MDiffuse[0] = 0.61424f;
		Spheres[4].MDiffuse[1] = 0.04136f;
		Spheres[4].MDiffuse[2] = 0.04136f;
		Spheres[4].MDiffuse[3] = 1.0f;

		Spheres[4].MSpecular[0] = 0.727811f;
		Spheres[4].MSpecular[1] = 0.626959f;
		Spheres[4].MSpecular[2] = 0.626959f;
		Spheres[4].MSpecular[3] = 1.0f;

		Spheres[4].SViewport[0] = 0;
		Spheres[4].SViewport[1] = 3 * gHeight /6;
		Spheres[4].SViewport[2] = 3 * gWidth / 2;
		Spheres[4].SViewport[3] = gHeight / 2;

		Spheres[4].sphereShininess = 0.6f * 128.0f;
		
		// TURQUOISE
		Spheres[5].MAmbient[0] = 0.1f;
		Spheres[5].MAmbient[1] = 0.18725f;
		Spheres[5].MAmbient[2] = 0.1745f;
		Spheres[5].MAmbient[3] = 1.0f;

		Spheres[5].MDiffuse[0] = 0.396f;
		Spheres[5].MDiffuse[1] = 0.74151f;
		Spheres[5].MDiffuse[2] = 0.69102f;
		Spheres[5].MDiffuse[3] = 1.0f;

		Spheres[5].MSpecular[0] = 0.297254f;
		Spheres[5].MSpecular[1] = 0.30829f;
		Spheres[5].MSpecular[2] = 0.306678f;
		Spheres[5].MSpecular[3] = 1.0f;

		Spheres[5].SViewport[0] = 0;
		Spheres[5].SViewport[1] = 4 * gHeight / 6;
		Spheres[5].SViewport[2] = gWidth / 2;
		Spheres[5].SViewport[3] = gHeight / 2;

		Spheres[5].sphereShininess = 0.1f * 128.0f;

		// **************** 2nd Column : Metal **************************
		// BRASS
		Spheres[6].MAmbient[0] = 0.329412f;
		Spheres[6].MAmbient[1] = 0.223529f;
		Spheres[6].MAmbient[2] = 0.027451f;
		Spheres[6].MAmbient[3] = 1.0f;

		Spheres[6].MDiffuse[0] = 0.780392f;
		Spheres[6].MDiffuse[1] = 0.568627f;
		Spheres[6].MDiffuse[2] = 0.113725f;
		Spheres[6].MDiffuse[3] = 1.0f;

		Spheres[6].MSpecular[0] = 0.992157f;
		Spheres[6].MSpecular[1] = 0.941176f;
		Spheres[6].MSpecular[2] = 0.807843f;
		Spheres[6].MSpecular[3] = 1.0f;

		Spheres[6].SViewport[0] = gWidth / 6;
		Spheres[6].SViewport[1] = -1 * gHeight / 6;
		Spheres[6].SViewport[2] = gWidth / 2;
		Spheres[6].SViewport[3] = gHeight / 2;
		Spheres[6].sphereShininess = 0.21794872f * 128.0f;
		// BRONZE
		Spheres[7].MAmbient[0] = 0.2125f;
		Spheres[7].MAmbient[1] = 0.1275f;
		Spheres[7].MAmbient[2] = 0.054f;
		Spheres[7].MAmbient[3] = 1.0f;

		Spheres[7].MDiffuse[0] = 0.714f;
		Spheres[7].MDiffuse[1] = 0.4284f;
		Spheres[7].MDiffuse[2] = 0.18144f;
		Spheres[7].MDiffuse[3] = 1.0f;

		Spheres[7].MSpecular[0] = 0.393548f;
		Spheres[7].MSpecular[1] = 0.271906f;
		Spheres[7].MSpecular[2] = 0.166721f;
		Spheres[7].MSpecular[3] = 1.0f;

		Spheres[7].SViewport[0] = gWidth / 6;
		Spheres[7].SViewport[1] = 0 * gHeight / 6;
		Spheres[7].SViewport[2] = gWidth / 2;
		Spheres[7].SViewport[3] = gHeight / 2;

		Spheres[7].sphereShininess = 0.2f * 128.0f;

		// CHROME
		Spheres[8].MAmbient[0] = 0.25f;
		Spheres[8].MAmbient[1] = 0.25f;
		Spheres[8].MAmbient[2] = 0.25f;
		Spheres[8].MAmbient[3] = 1.0f;

		Spheres[8].MDiffuse[0] = 0.4f;
		Spheres[8].MDiffuse[1] = 0.4f;
		Spheres[8].MDiffuse[2] = 0.4f;
		Spheres[8].MDiffuse[3] = 1.0f;

		Spheres[8].MSpecular[0] = 0.774597f;
		Spheres[8].MSpecular[1] = 0.774597f;
		Spheres[8].MSpecular[2] = 0.774597f;
		Spheres[8].MSpecular[3] = 1.0f;

		Spheres[8].SViewport[0] = gWidth / 6;
		Spheres[8].SViewport[1] = 1 * gHeight / 6;
		Spheres[8].SViewport[2] = gWidth / 2;
		Spheres[8].SViewport[3] = gHeight / 2;

		Spheres[8].sphereShininess = 0.6f * 128.0f;
		
		// COPPER
		Spheres[9].MAmbient[0] = 0.19125f;
		Spheres[9].MAmbient[1] = 0.0735f;
		Spheres[9].MAmbient[2] = 0.0225f;
		Spheres[9].MAmbient[3] = 1.0f;

		Spheres[9].MDiffuse[0] = 0.7038f;
		Spheres[9].MDiffuse[1] = 0.27048f;
		Spheres[9].MDiffuse[2] = 0.0828f;
		Spheres[9].MDiffuse[3] = 1.0f;

		Spheres[9].MSpecular[0] = 0.256777f;
		Spheres[9].MSpecular[1] = 0.137622f;
		Spheres[9].MSpecular[2] = 0.086014f;
		Spheres[9].MSpecular[3] = 1.0f;

		Spheres[9].SViewport[0] = gWidth / 6;
		Spheres[9].SViewport[1] = 2 * gHeight / 6;
		Spheres[9].SViewport[2] = gWidth / 2;
		Spheres[9].SViewport[3] = gHeight / 2;

		Spheres[9].sphereShininess = 0.1f * 128.0f;

		// GOLD
		Spheres[10].MAmbient[0] = 0.24725f;
		Spheres[10].MAmbient[1] = 0.1995f;
		Spheres[10].MAmbient[2] = 0.0745f;
		Spheres[10].MAmbient[3] = 1.0f;

		Spheres[10].MDiffuse[0] = 0.75164f;
		Spheres[10].MDiffuse[1] = 0.555802f;
		Spheres[10].MDiffuse[2] = 0.366065f;
		Spheres[10].MDiffuse[3] = 1.0f;

		Spheres[10].MSpecular[0] = 0.628281f;
		Spheres[10].MSpecular[1] = 0.555802f;
		Spheres[10].MSpecular[2] = 0.366065f;
		Spheres[10].MSpecular[3] = 1.0f;

		Spheres[10].SViewport[0] = gWidth / 6;
		Spheres[10].SViewport[1] = 3 * gHeight / 6;
		Spheres[10].SViewport[2] = gWidth / 2;
		Spheres[10].SViewport[3] = gHeight / 2;
		
		Spheres[10].sphereShininess = 0.4f * 128.0f;

		// SILVER
		Spheres[11].MAmbient[0] = 0.19225f;
		Spheres[11].MAmbient[1] = 0.19225f;
		Spheres[11].MAmbient[2] = 0.19225f;
		Spheres[11].MAmbient[3] = 1.0f;

		Spheres[11].MDiffuse[0] = 0.50754f;
		Spheres[11].MDiffuse[1] = 0.50754f;
		Spheres[11].MDiffuse[2] = 0.50754f;
		Spheres[11].MDiffuse[3] = 1.0f;

		Spheres[11].MSpecular[0] = 0.508273f;
		Spheres[11].MSpecular[1] = 0.508273f;
		Spheres[11].MSpecular[2] = 0.508273f;
		Spheres[11].MSpecular[3] = 1.0f; 
		
		Spheres[11].SViewport[0] = gWidth / 6;
		Spheres[11].SViewport[1] = 4 * gHeight / 6;
		Spheres[11].SViewport[2] = gWidth / 2;
		Spheres[11].SViewport[3] = gHeight / 2;

		Spheres[11].sphereShininess = 0.4f * 128.0f;

		// **************** 3rd Column : Plastic **************************
		// BLACK PLASTIC
		Spheres[12].MAmbient[0] = 0.0f;
		Spheres[12].MAmbient[1] = 0.0f;
		Spheres[12].MAmbient[2] = 0.0f;
		Spheres[12].MAmbient[3] = 1.0f;

		Spheres[12].MDiffuse[0] = 0.01f;
		Spheres[12].MDiffuse[1] = 0.01f;
		Spheres[12].MDiffuse[2] = 0.01f;
		Spheres[12].MDiffuse[3] = 1.0f;

		Spheres[12].MSpecular[0] = 0.50f;
		Spheres[12].MSpecular[1] = 0.50f;
		Spheres[12].MSpecular[2] = 0.50f;
		Spheres[12].MSpecular[3] = 1.0f; 
		
		Spheres[12].SViewport[0] = gWidth / 3;
		Spheres[12].SViewport[1] = -1 * gHeight / 6;
		Spheres[12].SViewport[2] = gWidth / 2;
		Spheres[12].SViewport[3] = gHeight / 2;

		Spheres[12].sphereShininess = 0.25f * 128.0f;

		// CYAN PLASTIC
		Spheres[13].MAmbient[0] = 0.0f;
		Spheres[13].MAmbient[1] = 0.1f;
		Spheres[13].MAmbient[2] = 0.06f;
		Spheres[13].MAmbient[3] = 1.0f;

		Spheres[13].MDiffuse[0] = 0.0f;
		Spheres[13].MDiffuse[1] = 0.50980392f;
		Spheres[13].MDiffuse[2] = 0.50980392f;
		Spheres[13].MDiffuse[3] = 1.0f;

		Spheres[13].MSpecular[0] = 0.50196078f;
		Spheres[13].MSpecular[1] = 0.50196078f;
		Spheres[13].MSpecular[2] = 0.50196078f;
		Spheres[13].MSpecular[3] = 1.0f;

		Spheres[13].SViewport[0] = gWidth / 3;
		Spheres[13].SViewport[1] = 0 * gHeight / 6;
		Spheres[13].SViewport[2] = gWidth / 2;
		Spheres[13].SViewport[3] = gHeight / 2;

		Spheres[13].sphereShininess = 0.25f * 128.0f;

		// GREEN PLASTIC
		Spheres[14].MAmbient[0] = 0.0f;
		Spheres[14].MAmbient[1] = 0.0f;
		Spheres[14].MAmbient[2] = 0.0f;
		Spheres[14].MAmbient[3] = 1.0f;

		Spheres[14].MDiffuse[0] = 0.1f;
		Spheres[14].MDiffuse[1] = 0.35f;
		Spheres[14].MDiffuse[2] = 0.1f;
		Spheres[14].MDiffuse[3] = 1.0f;

		Spheres[14].MSpecular[0] = 0.45f;
		Spheres[14].MSpecular[1] = 0.55f;
		Spheres[14].MSpecular[2] = 0.45f;
		Spheres[14].MSpecular[3] = 1.0f;

		Spheres[14].SViewport[0] = gWidth / 3;
		Spheres[14].SViewport[1] = 1 * gHeight / 6;
		Spheres[14].SViewport[2] = gWidth / 2;
		Spheres[14].SViewport[3] = gHeight / 2;

		Spheres[14].sphereShininess = 0.25f * 128.0f;

		// RED PLASTIC
		Spheres[15].MAmbient[0] = 0.0f;
		Spheres[15].MAmbient[1] = 0.0f;
		Spheres[15].MAmbient[2] = 0.0f;
		Spheres[15].MAmbient[3] = 1.0f;

		Spheres[15].MDiffuse[0] = 0.5f;
		Spheres[15].MDiffuse[1] = 0.0f;
		Spheres[15].MDiffuse[2] = 0.0f;
		Spheres[15].MDiffuse[3] = 1.0f;

		Spheres[15].MSpecular[0] = 0.7f;
		Spheres[15].MSpecular[1] = 0.6f;
		Spheres[15].MSpecular[2] = 0.6f;
		Spheres[15].MSpecular[3] = 1.0f;

		Spheres[15].SViewport[0] = gWidth / 3;
		Spheres[15].SViewport[1] = 2 * gHeight / 6;
		Spheres[15].SViewport[2] = gWidth / 2;
		Spheres[15].SViewport[3] = gHeight / 2;

		Spheres[15].sphereShininess = 0.25f * 128.0f;
		// WHITE PLASTIC
		Spheres[16].MAmbient[0] = 0.0f;
		Spheres[16].MAmbient[1] = 0.0f;
		Spheres[16].MAmbient[2] = 0.0f;
		Spheres[16].MAmbient[3] = 1.0f;

		Spheres[16].MDiffuse[0] = 0.55f;
		Spheres[16].MDiffuse[1] = 0.55f;
		Spheres[16].MDiffuse[2] = 0.55f;
		Spheres[16].MDiffuse[3] = 1.0f;

		Spheres[16].MSpecular[0] = 0.70f;
		Spheres[16].MSpecular[1] = 0.70f;
		Spheres[16].MSpecular[2] = 0.70f;
		Spheres[16].MSpecular[3] = 1.0f;

		Spheres[16].SViewport[0] = gWidth / 3;
		Spheres[16].SViewport[1] = 3 * gHeight / 6;;
		Spheres[16].SViewport[2] = gWidth / 2;
		Spheres[16].SViewport[3] = gHeight / 2;

		Spheres[16].sphereShininess = 0.25f * 128.0f;

		// YELLOW PLASTIC
		Spheres[17].MAmbient[0] = 0.0f;
		Spheres[17].MAmbient[1] = 0.0f;
		Spheres[17].MAmbient[2] = 0.0f;
		Spheres[17].MAmbient[3] = 1.0f;

		Spheres[17].MDiffuse[0] = 0.5f;
		Spheres[17].MDiffuse[1] = 0.5f;
		Spheres[17].MDiffuse[2] = 0.0f;
		Spheres[17].MDiffuse[3] = 1.0f;

		Spheres[17].MSpecular[0] = 0.60f;
		Spheres[17].MSpecular[1] = 0.60f;
		Spheres[17].MSpecular[2] = 0.50f;
		Spheres[17].MSpecular[3] = 1.0f;

		Spheres[17].SViewport[0] = gWidth / 3;
		Spheres[17].SViewport[1] = 4 * gHeight / 6;
		Spheres[17].SViewport[2] = gWidth / 2;
		Spheres[17].SViewport[3] = gHeight / 2;

		Spheres[17].sphereShininess = 0.25f * 128.0f;

		// **************** 4th Column : Rubber **************************
		// BLACK RUBBER
		Spheres[18].MAmbient[0] = 0.02f;
		Spheres[18].MAmbient[1] = 0.02f;
		Spheres[18].MAmbient[2] = 0.02f;
		Spheres[18].MAmbient[3] = 1.0f;

		Spheres[18].MDiffuse[0] = 0.01f;
		Spheres[18].MDiffuse[1] = 0.01f;
		Spheres[18].MDiffuse[2] = 0.01f;
		Spheres[18].MDiffuse[3] = 1.0f;

		Spheres[18].MSpecular[0] = 0.4f;
		Spheres[18].MSpecular[1] = 0.4f;
		Spheres[18].MSpecular[2] = 0.4f;
		Spheres[18].MSpecular[3] = 1.0f;

		Spheres[18].SViewport[0] = gWidth / 2;
		Spheres[18].SViewport[1] = -1 * gHeight / 6;
		Spheres[18].SViewport[2] = gWidth / 2;
		Spheres[18].SViewport[3] = gHeight / 2;

		Spheres[18].sphereShininess = 0.078125f * 128.0f;

		// CYAN RUBBER
		Spheres[19].MAmbient[0] = 0.0f;
		Spheres[19].MAmbient[1] = 0.05f;
		Spheres[19].MAmbient[2] = 0.05f;
		Spheres[19].MAmbient[3] = 1.0f;

		Spheres[19].MDiffuse[0] = 0.40f;
		Spheres[19].MDiffuse[1] = 0.50f;
		Spheres[19].MDiffuse[2] = 0.50f;
		Spheres[19].MDiffuse[3] = 1.0f;

		Spheres[19].MSpecular[0] = 0.04f;
		Spheres[19].MSpecular[1] = 0.7f;
		Spheres[19].MSpecular[2] = 0.7f;
		Spheres[19].MSpecular[3] = 1.0f;

		Spheres[19].SViewport[0] = gWidth / 2;
		Spheres[19].SViewport[1] = 0 * gHeight / 6;
		Spheres[19].SViewport[2] = gWidth / 2;
		Spheres[19].SViewport[3] = gHeight / 2;

		Spheres[19].sphereShininess = 0.078125f * 128.0f;
		
		// GREEN RUBBER
		Spheres[20].MAmbient[0] = 0.0f;
		Spheres[20].MAmbient[1] = 0.05f;
		Spheres[20].MAmbient[2] = 0.0f;
		Spheres[20].MAmbient[3] = 1.0f;

		Spheres[20].MDiffuse[0] = 0.4f;
		Spheres[20].MDiffuse[1] = 0.5f;
		Spheres[20].MDiffuse[2] = 0.4f;
		Spheres[20].MDiffuse[3] = 1.0f;

		Spheres[20].MSpecular[0] = 0.04f;
		Spheres[20].MSpecular[1] = 0.7f;
		Spheres[20].MSpecular[2] = 0.04f;
		Spheres[20].MSpecular[3] = 1.0f;

		Spheres[20].SViewport[0] = gWidth / 2;
		Spheres[20].SViewport[1] = 1 * gHeight / 6;
		Spheres[20].SViewport[2] = gWidth / 2;
		Spheres[20].SViewport[3] = gHeight / 2;

		Spheres[20].sphereShininess = 0.078125f * 128.0f;

		// RED RUBBER
		Spheres[21].MAmbient[0] = 0.05f;
		Spheres[21].MAmbient[1] = 0.0f;
		Spheres[21].MAmbient[2] = 0.0f;
		Spheres[21].MAmbient[3] = 1.0f;

		Spheres[21].MDiffuse[0] = 0.5f;
		Spheres[21].MDiffuse[1] = 0.4f;
		Spheres[21].MDiffuse[2] = 0.4f;
		Spheres[21].MDiffuse[3] = 1.0f;

		Spheres[21].MSpecular[0] = 0.7f;
		Spheres[21].MSpecular[1] = 0.04f;
		Spheres[21].MSpecular[2] = 0.04f;
		Spheres[21].MSpecular[3] = 1.0f;

		Spheres[21].SViewport[0] = gWidth / 2;
		Spheres[21].SViewport[1] = 2 * gHeight / 6;
		Spheres[21].SViewport[2] = gWidth / 2;
		Spheres[21].SViewport[3] = gHeight / 2;

		Spheres[21].sphereShininess = 0.078125f * 128.0f;

		// WHITE RUBBER
		Spheres[22].MAmbient[0] = 0.05f;
		Spheres[22].MAmbient[1] = 0.05f;
		Spheres[22].MAmbient[2] = 0.05f;
		Spheres[22].MAmbient[3] = 1.0f;

		Spheres[22].MDiffuse[0] = 0.5f;
		Spheres[22].MDiffuse[1] = 0.5f;
		Spheres[22].MDiffuse[2] = 0.5f;
		Spheres[22].MDiffuse[3] = 1.0f;

		Spheres[22].MSpecular[0] = 0.70f;
		Spheres[22].MSpecular[1] = 0.70f;
		Spheres[22].MSpecular[2] = 0.70f;
		Spheres[22].MSpecular[3] = 1.0f;

		Spheres[22].SViewport[0] = gWidth / 2;
		Spheres[22].SViewport[1] = 3 * gHeight / 6;;
		Spheres[22].SViewport[2] = gWidth / 2;
		Spheres[22].SViewport[3] = gHeight / 2;

		Spheres[22].sphereShininess = 0.078125f * 128.0f;

		// YELLOW RUBBER
		Spheres[23].MAmbient[0] = 0.05f;
		Spheres[23].MAmbient[1] = 0.05f;
		Spheres[23].MAmbient[2] = 0.0f;
		Spheres[23].MAmbient[3] = 1.0f;

		Spheres[23].MDiffuse[0] = 0.50f;
		Spheres[23].MDiffuse[1] = 0.50f;
		Spheres[23].MDiffuse[2] = 0.40f;
		Spheres[23].MDiffuse[3] = 1.0f;

		Spheres[23].MSpecular[0] = 0.70f;
		Spheres[23].MSpecular[1] = 0.70f;
		Spheres[23].MSpecular[2] = 0.04f;
		Spheres[23].MSpecular[3] = 1.0f;

		Spheres[23].SViewport[0] = gWidth / 2;
		Spheres[23].SViewport[1] = 4 * gHeight / 6;
		Spheres[23].SViewport[2] = gWidth / 2;
		Spheres[23].SViewport[3] = gHeight / 2;

		Spheres[23].sphereShininess = 0.078125f * 128.0f;
}

void uninitialize(void)
{
	//code
	if (gpID3D11Buffer_ConstantBuffer)
	{
		gpID3D11Buffer_ConstantBuffer->Release();
		gpID3D11Buffer_ConstantBuffer = NULL;
	}

	if (gpID3D11InputLayout)
	{
		gpID3D11InputLayout->Release();
		gpID3D11InputLayout = NULL;
	}

	if (gpID3D11Buffer_VertexBuffer_Position_Sphere)
	{
		gpID3D11Buffer_VertexBuffer_Position_Sphere->Release();
		gpID3D11Buffer_VertexBuffer_Position_Sphere = NULL;
	}

	if (gpID3D11Buffer_VertexBuffer_Normal_Sphere)
	{
		gpID3D11Buffer_VertexBuffer_Normal_Sphere->Release();
		gpID3D11Buffer_VertexBuffer_Normal_Sphere = NULL;
	}

	if (gpID3D11PixelShader_PerPixel)
	{
		gpID3D11PixelShader_PerPixel->Release();
		gpID3D11PixelShader_PerPixel = NULL;
	}

	if (gpID3D11VertexShader_PerPixel)
	{
		gpID3D11VertexShader_PerPixel->Release();
		gpID3D11VertexShader_PerPixel = NULL;
	}

	if (gpID3D11RenderTargetView_ps)
	{
		gpID3D11RenderTargetView_ps->Release();
		gpID3D11RenderTargetView_ps = NULL;
	}

	if (gpIDXGISwapChain_ps)
	{
		gpIDXGISwapChain_ps->Release();
		gpIDXGISwapChain_ps = NULL;
	}

	if (gpID3D11DeviceContext_ps)
	{
		gpID3D11DeviceContext_ps->Release();
		gpID3D11DeviceContext_ps = NULL;
	}

	if (gpID3D11Device_ps)
	{
		gpID3D11Device_ps->Release();
		gpID3D11Device_ps = NULL;
	}

	if (gpfile_ps)
	{
		fopen_s(&gpfile_ps, gszLogFileName_ps, "a+");
		fprintf_s(gpfile_ps, "uninitiaize() succeded.\n ");
		fprintf_s(gpfile_ps, "log file is closed successfully.\n ");
		fclose(gpfile_ps);
	}
}