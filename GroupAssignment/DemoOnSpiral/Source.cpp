//Headers
#include<Windows.h>
#include<stdio.h>
#include<math.h>
#include<gl/gl.h>
#include<gl/glu.h>

#pragma comment (lib, "glu32.lib")
#pragma comment (lib, "opengl32.lib")

//macros
#define WIN_WIDTH 800
#define WIN_HEIGHT 600

//global function declarations
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

// global variables
HWND ghwnd = NULL;
HDC ghdc = NULL;
HGLRC ghrc = NULL;				//rendering context for opengl
bool gbActiveWindow = false;	//flag to check whether window is active or not
bool gbFullScreen = false;		//flag to check whether sceen is fullscreen or not
DWORD dwStyle;
GLfloat angle = 0.0f;
WINDOWPLACEMENT wpPrev = { sizeof(WINDOWPLACEMENT) };
FILE *gpfile = NULL;
GLfloat X = 1.0f, Y = 1.0f;
int index =0;

//WinMain
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{
	//function declarations
	int initialize(void);
	void display(void);
	void update(void);

	//variable declarations
	WNDCLASSEX wndclass;
	HWND hwnd;
	MSG msg;
	TCHAR szAppName[] = TEXT("OGLSkeleton");
	bool bDone = false;
	int iRet = 0;

	//code
	if (fopen_s(&gpfile, "log.txt", "w") != 0)
	{
		MessageBox(NULL, TEXT("Log File cannot be created"), TEXT("Error"), MB_OK);
		exit(0);
	}
	//initialization of WNDCLASSEX structure
	wndclass.cbSize = sizeof(WNDCLASSEX);
	wndclass.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
	wndclass.cbClsExtra = 0;
	wndclass.cbWndExtra = 0;
	wndclass.lpfnWndProc = WndProc;
	wndclass.hInstance = hInstance;
	wndclass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	wndclass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndclass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndclass.lpszClassName = szAppName;
	wndclass.lpszMenuName = NULL;
	wndclass.hIconSm = LoadIcon(hInstance, IDI_APPLICATION);

	//register above class
	RegisterClassEx(&wndclass);

	//create window
	hwnd = CreateWindowEx(WS_EX_APPWINDOW,
		szAppName,
		TEXT("Colored Rectangle- Priya Saboo"),
		WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN | WS_CLIPSIBLINGS | WS_VISIBLE,
		100,
		100,
		WIN_WIDTH,
		WIN_HEIGHT,
		NULL,
		NULL,
		hInstance,
		NULL);

	ghwnd = hwnd;
	iRet = initialize();
	if (iRet == -1)
	{
		fprintf(gpfile, "ChoosePixelFormat() Failed\n");
		DestroyWindow(hwnd);
	}
	else if (iRet == -2)
	{
		fprintf(gpfile, "SetPixelFormat() Failed\n");
		DestroyWindow(hwnd);
	}
	else if (iRet == -3)
	{
		fprintf(gpfile, "wglCreateContext() Failed\n");
		DestroyWindow(hwnd);
	}
	else if (iRet == -4)
	{
		fprintf(gpfile, "wglMakeCurrent() Failed\n");
		DestroyWindow(hwnd);
	}
	else
		fprintf(gpfile, "Initialize() Succeded\n");
	ShowWindow(hwnd, iCmdShow);
	SetForegroundWindow(hwnd);
	SetFocus(hwnd);

	//gameloop
	while (bDone == false)
	{
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			if (msg.message == WM_QUIT)
			{
				bDone = true;
			}
			else
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}
		else
		{
			if (gbActiveWindow == true)
			{
				//here call update
				update();
			}
			//here u should call display, though in this application we call it in WM_PAINT
			display();
		}
	}
	return((int)msg.wParam);
}

LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	//function declarations
	void resize(int, int);
	void ToggleFullScreen(void);
	void uninitialize(void);

	//code
	switch (iMsg)
	{
	case WM_SETFOCUS:
		gbActiveWindow = true;
		break;

	case WM_KILLFOCUS:
		gbActiveWindow = false;
		break;

	case WM_SIZE:
		resize(LOWORD(lParam), HIWORD(lParam));
		break;

	case WM_ERASEBKGND:
		return(0);

	case WM_CLOSE:
		DestroyWindow(hwnd);
		break;

	case WM_KEYDOWN:
		switch (LOWORD(wParam))
		{
		case VK_ESCAPE:
			DestroyWindow(hwnd);
			break;

		case 0x46:
			ToggleFullScreen();
			break;
		}
		break;

	case WM_DESTROY:
		uninitialize();
		PostQuitMessage(0);
		break;
	}
	return(DefWindowProc(hwnd, iMsg, wParam, lParam));
}

//toggle fullscreen function
void ToggleFullScreen(void)
{
	MONITORINFO mi;
	if (gbFullScreen == false)
	{
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);
		if (dwStyle & WS_OVERLAPPEDWINDOW)
		{
			mi = { sizeof(MONITORINFO) };
			if (GetWindowPlacement(ghwnd, &wpPrev) && GetMonitorInfo(MonitorFromWindow(ghwnd, MONITORINFOF_PRIMARY), &mi))
			{
				SetWindowLong(ghwnd, GWL_STYLE, dwStyle & ~WS_OVERLAPPEDWINDOW);
				SetWindowPos(ghwnd,
					HWND_TOP,
					mi.rcMonitor.left,
					mi.rcMonitor.top,
					mi.rcMonitor.right - mi.rcMonitor.left,
					mi.rcMonitor.bottom - mi.rcMonitor.top,
					SWP_NOZORDER | SWP_FRAMECHANGED);
			}
		}
		ShowCursor(FALSE);
		gbFullScreen = true;
	}
	else
	{
		fprintf(gpfile, "Fullscreen to normal\n");
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPED);
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd,
			HWND_TOP,
			0,
			0,
			0,
			0,
			SWP_NOZORDER | SWP_FRAMECHANGED | SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER);
		ShowCursor(TRUE);
		gbFullScreen = false;
	}
}

//initialize() function
int initialize(void)
{
	//function declarations
	void resize(int, int);

	//variable declarations
	PIXELFORMATDESCRIPTOR pfd;
	int iPixelFormatIndex;

	//code
	//initializa pfd structure
	ZeroMemory(&pfd, sizeof(PIXELFORMATDESCRIPTOR));
	pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
	pfd.nVersion = 1;
	pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
	pfd.iPixelType = PFD_TYPE_RGBA;
	pfd.cColorBits = 32;
	pfd.cRedBits = 8;
	pfd.cGreenBits = 8;
	pfd.cBlueBits = 8;
	pfd.cAlphaBits = 8;

	ghdc = GetDC(ghwnd);
	iPixelFormatIndex = ChoosePixelFormat(ghdc, &pfd);
	if (iPixelFormatIndex == 0)
	{
		return(-1);
	}
	if (SetPixelFormat(ghdc, iPixelFormatIndex, &pfd) == false)
	{
		return(-2);
	}
	ghrc = wglCreateContext(ghdc);
	if (ghrc == NULL)
	{
		return(-3);
	}
	if (wglMakeCurrent(ghdc, ghrc) == false)
	{
		return(-4);
	}

	//clear the screen by opengl color
	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);	//r,g,b,a

	//warmup call to resize
	resize(WIN_WIDTH, WIN_HEIGHT);
	return(0);
}

//update() function
void update(void)
{
	if (index = 90)
	{
		index = 0;
	}
	index = index = 1;
}

//display() function
void display(void)
{
	//glClear(GL_COLOR_BUFFER_BIT);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(0.0f, 0.0f, -4.0f);
	GLfloat degree, radians;
	GLfloat x1[90], y1[90];
	GLfloat Tri_x1[90], Tri_y1[90], Tri_x2[90], Tri_y2[90], Tri_x3[90], Tri_y3[90];
	GLfloat Hex_x1[90], Hex_y1[90], Hex_x2[90], Hex_y2[90], Hex_x3[90], Hex_y3[90],
		Hex_x4[90], Hex_y4[90], Hex_x5[90], Hex_y5[90], Hex_x6[90], Hex_y6[90];

	int i = 0;
	degree = 0.0f;
	GLfloat xnew, ynew, x, y;
	//************************************TRIANGLE********************************************

	//for (i = 0; i < 90; i++)
	//{
	//	radians = (degree * 2 * 3.14159f) / 360;
	//	//1st quadrant
	//	Tri_x1[i] = 0.001f*(GLfloat)exp(radians + (3.14159f/4.0f))*(GLfloat)cos(radians+ (3.14159f / 4.0f));
	//	Tri_y1[i] = 0.001f*(GLfloat)exp(radians+ (3.14159f / 4.0f))*(GLfloat)sin(radians+ (3.14159f / 4.0f));
	//	degree = degree + 4.0f;
	//}

	//for (i = 0; i < 90; i++)
	//{
	//	Tri_x2[i] = cos((120 * 2 * 3.14159) / 360)*Tri_x1[i] - sin((120 * 2 * 3.14159f) / 360)*Tri_y1[i];//rotation by 25degree
	//	Tri_y2[i] = sin((120 * 2 * 3.14159f) / 360)*Tri_x1[i] + cos((120 * 2 * 3.14159) / 360)*Tri_y1[i];
	//}

	//for (i = 0; i < 90; i++)
	//{
	//	Tri_x3[i] = cos((240 * 2 * 3.14159) / 360)*Tri_x1[i] - sin((240 * 2 * 3.14159f) / 360)*Tri_y1[i];//rotation by 25degree
	//	Tri_y3[i] = sin((240 * 2 * 3.14159f) / 360)*Tri_x1[i] + cos((240 * 2 * 3.14159) / 360)*Tri_y1[i];
	//}
	//glBegin(GL_LINES);
	//for (i = 0; i < 90; i++)
	//{
	//	glColor3f(Tri_x1[i], Tri_y1[i], 1.0f);
	//	glVertex2f(Tri_x1[i], Tri_y1[i]);
	//	glVertex2f(Tri_x2[i], Tri_y2[i]);

	//	glVertex2f(Tri_x2[i], Tri_y2[i]);
	//	glVertex2f(Tri_x3[i], Tri_y3[i]);

	//	glVertex2f(Tri_x3[i], Tri_y3[i]);
	//	glVertex2f(Tri_x1[i], Tri_y1[i]);
	//}
	//glEnd();



	//************************SQUARE*********************************************

	for (i = 0; i < 90; i++)
	{
		radians = (degree * 2 * 3.14159f) / 360;
		//1st quadrant
		x1[i] = 0.001f*(GLfloat)exp(radians + (3.14159f/4.0f))*(GLfloat)cos(radians+ (3.14159f / 4.0f));
		//fprintf(gpfile, "x1[%d]=%f\t", i, x1[i]);
		y1[i] = 0.001f*(GLfloat)exp(radians+ (3.14159f / 4.0f))*(GLfloat)sin(radians+ (3.14159f / 4.0f));
		//fprintf(gpfile, "y1[%d]=%f\n", i, y1[i]);
		degree = degree + 4.0f;
	}

	glLoadIdentity();
	glTranslatef(0.0f, 0.0f, -2.0f);
	glRotatef(4.0f, 0.0f, 0.0f, 1.0f);
	glLineWidth(2.0f);
	
	/*glBegin(GL_LINES);
		fprintf(gpfile, "%d\n",index);
		glColor3f(x1[index], y1[index], 0.2f);
		glVertex2f(x1[index], y1[index]);
		glVertex2f(-y1[index], x1[index]);

		glVertex2f(-y1[index], x1[index]);
		glVertex2f(-x1[index], -y1[index]);

		glVertex2f(-x1[index], -y1[index]);
		glVertex2f(y1[index], -x1[index]);

		glVertex2f(y1[index], -x1[index]);
		glVertex2f(x1[index], y1[index]);
	glEnd();*/
	//index = index + 1;
	glBegin(GL_LINES);
	for (i = 0; i < 90; i++)
	{
		fprintf(gpfile, "*x1[%d]=%f\n", i, x1[i]);
		glColor3f(x1[i], y1[i], 0.2f);
		glVertex2f(x1[i], y1[i]);
		glVertex2f(-y1[i], x1[i]);

		glVertex2f(-y1[i], x1[i]);
		glVertex2f(-x1[i], -y1[i]);

		glVertex2f(-x1[i], -y1[i]);
		glVertex2f(y1[i], -x1[i]);

		glVertex2f(y1[i], -x1[i]);
		glVertex2f(x1[i], y1[i]);
	}
	glEnd();



	//**********************************************HEXAGON********************************************

	//for (i = 0; i < 90; i++)
	//{
	//	radians = (degree * 2 * 3.14159f) / 360;
	//	//1st quadrant
	//	Hex_x1[i] = 0.001f*(GLfloat)exp(radians + (3.14159f / 4.0f))*(GLfloat)cos(radians + (3.14159f / 4.0f));
	//	Hex_y1[i] = 0.001f*(GLfloat)exp(radians + (3.14159f / 4.0f))*(GLfloat)sin(radians + (3.14159f / 4.0f));
	//	degree = degree + 4.0f;
	//}

	//for (i = 0; i < 90; i++)
	//{
	//	Hex_x2[i] = cos((60 * 2 * 3.14159) / 360)*Hex_x1[i] - sin((60 * 2 * 3.14159f) / 360)*Hex_y1[i];//rotation by 25degree
	//	Hex_y2[i] = sin((60 * 2 * 3.14159f) / 360)*Hex_x1[i] + cos((60 * 2 * 3.14159) / 360)*Hex_y1[i];
	//}

	//for (i = 0; i < 90; i++)
	//{
	//	Hex_x3[i] = cos((120 * 2 * 3.14159) / 360)*Hex_x1[i] - sin((120 * 2 * 3.14159f) / 360)*Hex_y1[i];//rotation by 25degree
	//	Hex_y3[i] = sin((120 * 2 * 3.14159f) / 360)*Hex_x1[i] + cos((120 * 2 * 3.14159) / 360)*Hex_y1[i];
	//}

	//for (i = 0; i < 90; i++)
	//{
	//	Hex_x4[i] = cos((180 * 2 * 3.14159) / 360)*Hex_x1[i] - sin((180 * 2 * 3.14159f) / 360)*Hex_y1[i];//rotation by 25degree
	//	Hex_y4[i] = sin((180 * 2 * 3.14159f) / 360)*Hex_x1[i] + cos((180 * 2 * 3.14159) / 360)*Hex_y1[i];
	//}

	//for (i = 0; i < 90; i++)
	//{
	//	Hex_x5[i] = cos((240 * 2 * 3.14159) / 360)*Hex_x1[i] - sin((240 * 2 * 3.14159f) / 360)*Hex_y1[i];//rotation by 25degree
	//	Hex_y5[i] = sin((240 * 2 * 3.14159f) / 360)*Hex_x1[i] + cos((240 * 2 * 3.14159) / 360)*Hex_y1[i];
	//}

	//for (i = 0; i < 90; i++)
	//{
	//	Hex_x6[i] = cos((300 * 2 * 3.14159) / 360)*Hex_x1[i] - sin((300 * 2 * 3.14159f) / 360)*Hex_y1[i];//rotation by 25degree
	//	Hex_y6[i] = sin((300 * 2 * 3.14159f) / 360)*Hex_x1[i] + cos((300 * 2 * 3.14159) / 360)*Hex_y1[i];
	//}
	//glLoadIdentity();
	//glTranslatef(0.0f, 0.0f, -2.8f);
	//glRotatef(10.0f, 0.0f, 0.0f, 1.0f);
	//glBegin(GL_LINES);
	//for (i = 0; i < 90; i++)
	//{
	//	glColor3f(Hex_x2[i], Hex_y2[i], 0.3f);
	//	glVertex2f(Hex_x1[i], Hex_y1[i]);
	//	glVertex2f(Hex_x2[i], Hex_y2[i]);

	//	glVertex2f(Hex_x2[i], Hex_y2[i]);
	//	glVertex2f(Hex_x3[i], Hex_y3[i]);

	//	glVertex2f(Hex_x3[i], Hex_y3[i]);
	//	glVertex2f(Hex_x4[i], Hex_y4[i]);

	//	glVertex2f(Hex_x4[i], Hex_y4[i]);
	//	glVertex2f(Hex_x5[i], Hex_y5[i]);

	//	glVertex2f(Hex_x5[i], Hex_y5[i]);
	//	glVertex2f(Hex_x6[i], Hex_y6[i]);

	//	glVertex2f(Hex_x6[i], Hex_y6[i]);
	//	glVertex2f(Hex_x1[i], Hex_y1[i]);
	//}
	//glEnd();
	SwapBuffers(ghdc);
}

//resize() function
void resize(int width, int height)
{
	if (height == 0)
	{
		height = 1;
	}
	glViewport(0, 0, width, height);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluPerspective(45.0f, (GLfloat)width / (GLfloat)height, 0.1f, 100.0f);
}

//uninitialize() function
void uninitialize(void)
{
	if (gbFullScreen == true)
	{
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPED);
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd,
			HWND_TOP,
			0,
			0,
			0,
			0,
			SWP_NOZORDER | SWP_FRAMECHANGED | SWP_NOSIZE | SWP_NOOWNERZORDER);
		ShowCursor(TRUE);
	}

	//break the current context
	if (wglGetCurrentContext() == ghrc)
	{
		wglMakeCurrent(NULL, NULL);
	}

	if (ghrc)
	{
		wglDeleteContext(ghrc);
		ghrc = NULL;
	}

	if (ghdc)
	{
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	if (gpfile)
	{
		fprintf(gpfile, "log file is closed successfully\n");
		fclose(gpfile);
		gpfile = NULL;
	}
}
