#include<iostream>
#include<stdio.h>
#include<stdlib.h>
#include<memory.h>

#include<X11/Xlib.h>
#include<X11/Xutil.h>
#include<X11/XKBlib.h>
#include<X11/keysym.h>

#include<GL/glew.h>
#include<GL/gl.h>
#include<GL/glx.h>
#include<SOIL/SOIL.h>

#include"vmath.h"

//namespaces
using namespace std;
using namespace vmath;

//global variable declarations
bool bFullScreen = false;
Display *gpDisplay = NULL;
XVisualInfo *gpXVisualInfo = NULL;
Colormap gColormap;
Window gWindow;
int giWindowWidth = 800;
int giWindowHeight = 600;

typedef GLXContext(*glxCreateContextAttribsARBProc)(Display*, GLXFBConfig, GLXContext, Bool, const int*);
glxCreateContextAttribsARBProc glxCreateContextAttribsARB = NULL;
GLXFBConfig gGLXFBConfig;
GLXContext gGLXContext;


GLuint shaderProgramObject;

GLuint vao_cube;
GLuint vbo_cube_data;
GLuint textureMarble;

GLuint samplerUniform;
GLuint mUniform, vUniform, pUniform;    // ModelView Matrix Uniform and Projection Matrix Uniform
GLuint LdUniform, KdUniform;            // Diffuse property of Light and diffuse property of material
GLuint LaUniform, KaUniform;            // Ambient property of Light and ambient property of material
GLuint LsUniform, KsUniform;            // Specular property of Light and specular property of material
GLuint materialShininessUniform;        // material shininess
GLuint lightPositionUniform;            // light position
GLuint keyPressedUniform;               // is L key Pressed, to enable lighting effect

GLfloat angleCube = 0.0f;

bool bEnableLighting = false;
bool bEnableAnimation = false;

float lightAmbient[4]  = { 0.25f, 0.25f, 0.25f, 0.25f };
float lightDiffuse[4]  = { 1.0f, 1.0f, 1.0f, 1.0f };
float lightSpecular[4] = { 1.0f, 1.0f, 1.0f, 1.0f };
float lightPosition[4] = { 10.0f, 10.0f, 10.0f, 1.0f };

float materialAmbient[4] = { 0.25f, 0.25f, 0.25f, 0.0f };
float materialDiffuse[4] = { 1.0f, 1.0f, 1.0f, 1.0f };
float materialSpecular[4] = { 1.0f, 1.0f, 1.0f, 1.0f };
float materialShininess = 128.0f;

mat4 perspectiveProjectionMatrix;

enum 
{
    AMC_ATTRIBUTE_POSITION = 0,
    AMC_ATTRIBUTE_COLOR,
    AMC_ATTRIBUTE_NORMAL,
    AMC_ATTRIBUTE_TEXCOORD0
};

//entry point function
int main(void)
{
    //function prototype
    void CreateWindow(void);
    void ToggleFullScreen(void);
    void uninitialize();
    void initialize(void);
    void resize(int,int);
    void display(void);
    void update(void);

    //variable declarations
    int winWidth = giWindowWidth;
    int winHeight = giWindowHeight;
    bool bDone = false;

    //code
    CreateWindow();
    
    //initialize
    initialize();

    //Message Loop
    XEvent event;
    KeySym keysym;

    while(bDone == false)
    {
        while(XPending(gpDisplay))
        {
            XNextEvent(gpDisplay,&event);
            switch(event.type)
            {
                case MapNotify:
                    break;
                case KeyPress:
                    keysym = XkbKeycodeToKeysym(gpDisplay, event.xkey.keycode, 0, 0);
                    switch(keysym)
                    {
                        case XK_Escape:
                            bDone = true;
                            break;
                        case XK_F:
                        case XK_f:
                            if(bFullScreen == false)
                            {
                                ToggleFullScreen();
                                bFullScreen = true;
                            }
                            else
                            {
                                ToggleFullScreen();
                                bFullScreen = false;
                            }
                            break;
                        case XK_l:
                        case XK_L:
                            if(bEnableLighting == false)
                                bEnableLighting = true;
                            else
                                bEnableLighting = false;
                            break;
                        case XK_a:
                        case XK_A:
                            if(bEnableAnimation == false)
                                bEnableAnimation = true;
                            else
                                bEnableAnimation = false;
                            break;
                       default:
                            break;
                    }
                    break;
                case ButtonPress:
                   switch(event.xbutton.button)
                   {
                        case 1:                         //left button
                            break;
                        case 2:                         //middle button
                            break;
                        case 3:                         //right button
                            break;
                        default:
                            break;
                   }
                   break;
                case MotionNotify:
                   break;
                case ConfigureNotify:
                    winWidth = event.xconfigure.width;
                    winHeight =event.xconfigure.height;
                    resize(winWidth, winHeight);
                    break;
                case Expose:
                    break;
                case DestroyNotify:
                    break;
                case 33:                                //to handle close button and close menu
                    bDone = true;
                    break;
                default:
                    break;
            }
        }
        if(bEnableAnimation == true)
            update();
        display();
    }
    uninitialize();
    return(0);
}

void CreateWindow(void)
{
    //function prototype
    void uninitialize(void);

    //variable declarations
    XSetWindowAttributes winAttribs;
    int defaultScreen;
    int defaultDepth;
    int styleMask;
    int iNumberOfFBConfigs = 0;					// number of FBConfigs
    GLXFBConfig *pGLXFBConfigs = NULL;			//array of FBConfigs
    GLXFBConfig bestGLXFBConfig;				//best FBConfig among all
    XVisualInfo *pTempXVisualInfo = NULL;		//temporary visual info

    // Initialize Frame Buffer Attributes
    static int frameBufferAttributes[] = {
        GLX_X_RENDERABLE, True,
        GLX_DRAWABLE_TYPE,GLX_WINDOW_BIT,
        GLX_RENDER_TYPE, GLX_RGBA_BIT,
        GLX_X_VISUAL_TYPE, GLX_TRUE_COLOR,
        GLX_DOUBLEBUFFER, True,
        GLX_RED_SIZE, 8,
        GLX_GREEN_SIZE , 8,
        GLX_BLUE_SIZE, 8,
        GLX_ALPHA_SIZE, 8,
        GLX_DEPTH_SIZE, 24,
        GLX_STENCIL_SIZE, 8,
        None};

    //code
    gpDisplay = XOpenDisplay(NULL);
    if(!gpDisplay)
    {
        printf("ERROR : Unable To Open X Display.\nExitting Now ...\n");
        uninitialize();
        exit(1);
    }

    defaultScreen = XDefaultScreen(gpDisplay);

	// Retrive All FBConfigs Driver Has
	pGLXFBConfigs = glXChooseFBConfig(gpDisplay, defaultScreen,	frameBufferAttributes, &iNumberOfFBConfigs);

	printf("Number Of FBConfigs Matching To FBConfig = %d\n", iNumberOfFBConfigs);
	// Now We Have Multiple FBConfigs, We Need To Find Best Out Of Them

	int bestFrameBufferConfig = -1;
	int bestNumberOfSamples   = -1;
	int worstFrameBufferConfig = -1;
	int worstNumberOfSamples  = 999;

	//loop to check all fb configs
	for(int i = 0; i < iNumberOfFBConfigs; i++)
	{
		// Getting visual from each obtained FBConfig
		pTempXVisualInfo = glXGetVisualFromFBConfig(gpDisplay, pGLXFBConfigs[i]);

		if(pTempXVisualInfo)
		{
			//get Number of samplebuffer from respective fbConfig
			int sampleBuffers, samples;
			glXGetFBConfigAttrib(gpDisplay,
				pGLXFBConfigs[i],
				GLX_SAMPLE_BUFFERS,
				&sampleBuffers);

			//get number of samples from respective FBConfig
			glXGetFBConfigAttrib(gpDisplay, pGLXFBConfigs[i], GLX_SAMPLES, &samples);

			//More the number of samples and sample buffer more the eligible config buffer is
			if(bestFrameBufferConfig < 0 || sampleBuffers && samples >bestNumberOfSamples)
			{
				bestFrameBufferConfig=i;
				bestNumberOfSamples =samples;
			}

			if(worstFrameBufferConfig < 0 || !sampleBuffers || samples < worstNumberOfSamples)
			{
				worstFrameBufferConfig = i;
				worstNumberOfSamples = samples;
			}
		}

		XFree(pTempXVisualInfo);
	}

	// Now assign the found best one
	bestGLXFBConfig = pGLXFBConfigs[bestFrameBufferConfig];

	//Now assign the local best to global best
	gGLXFBConfig = bestGLXFBConfig;

	//Free the obtaind GLXFBConfig array;
	XFree(pGLXFBConfigs);

	//Accordingly get best visual
	gpXVisualInfo = glXGetVisualFromFBConfig(gpDisplay,  bestGLXFBConfig);

    winAttribs.border_pixel = 0;
    winAttribs.background_pixmap = 0;
    winAttribs.colormap = XCreateColormap(gpDisplay, 
                                          RootWindow(gpDisplay, gpXVisualInfo->screen),
                                          gpXVisualInfo->visual,
                                          AllocNone);
    gColormap = winAttribs.colormap;

    winAttribs.background_pixel = BlackPixel(gpDisplay,defaultScreen);

    winAttribs.event_mask = ExposureMask | VisibilityChangeMask | ButtonPressMask |
                           KeyPressMask | PointerMotionMask | StructureNotifyMask;
   
    styleMask = CWBorderPixel | CWBackPixel | CWEventMask | CWColormap;

    gWindow = XCreateWindow(gpDisplay,
                            RootWindow(gpDisplay,gpXVisualInfo->screen),
                            0,
                            0,
                            giWindowWidth,
                            giWindowHeight,
                            0,                                              // border width
                            gpXVisualInfo->depth,                           // color depth
                            InputOutput,
                            gpXVisualInfo->visual,
                            styleMask,
                            &winAttribs);
    if(!gWindow)
    {
        printf("ERROR : Failed To Create Main Window.\nExitting Now ...\n");
        uninitialize();
        exit(1);
    }

    XStoreName(gpDisplay, gWindow, "Animated 3d Shapes With Texture");

    Atom windowManagerDelete = XInternAtom(gpDisplay, "WM_DELETE_WINDOW", True);
    XSetWMProtocols(gpDisplay, gWindow, &windowManagerDelete, 1);

    XMapWindow(gpDisplay, gWindow);
}

void ToggleFullScreen(void)
{
    //variable declarations
    Atom  wm_state;
    Atom fullscreen;
    XEvent xev = {0};

    //code
    wm_state = XInternAtom(gpDisplay, "_NET_WM_STATE", False);
    memset(&xev,0,sizeof(xev));

    xev.type = ClientMessage;
    xev.xclient.window = gWindow;
    xev.xclient.message_type = wm_state;
    xev.xclient.format = 32;
    xev.xclient.data.l[0] = bFullScreen ? 0 : 1;

    fullscreen = XInternAtom(gpDisplay, "_NET_WM_STATE_FULLSCREEN", False);
    xev.xclient.data.l[1] = fullscreen;
    
    XSendEvent(gpDisplay,
               RootWindow(gpDisplay, gpXVisualInfo->screen),
               False,
               StructureNotifyMask,
               &xev);
}

void uninitialize(void)
{
    if(vao_cube)
    {
        glDeleteVertexArrays(1, &vao_cube);
        vao_cube = 0;
    }

    if (vbo_cube_data)
    {
        glDeleteBuffers(1, &vbo_cube_data);
        vbo_cube_data = 0;
    }

    if(shaderProgramObject)
    {
        GLsizei shaderCount;
        GLsizei shaderNumber;

        glUseProgram(shaderProgramObject);
        glGetProgramiv(shaderProgramObject, GL_ATTACHED_SHADERS, &shaderCount);
        GLuint *pShaders = (GLuint *)malloc(sizeof(GLuint) * shaderCount);

        if(pShaders)
        {
            glGetAttachedShaders(shaderProgramObject, shaderCount, &shaderCount, pShaders);
            for(shaderNumber = 0; shaderNumber < shaderCount; shaderNumber++)
            {
                // Detach all shaaders one by one
                glDetachShader(shaderProgramObject, pShaders[shaderNumber]);
                // Delete the detached shader
                glDeleteShader(pShaders[shaderNumber]);
                pShaders[shaderNumber] = 0;
            }
            free(pShaders);
        }
        glDeleteProgram(shaderProgramObject);
        shaderProgramObject = 0;
        glUseProgram(0);
    }

    if(gWindow)
    {
        XDestroyWindow(gpDisplay,gWindow);
    }
    if(gColormap)
    {
        XFreeColormap(gpDisplay, gColormap);
    }
    if(gpXVisualInfo)
    {
        free(gpXVisualInfo);
        gpXVisualInfo = NULL;
    }
    if(gpDisplay)
    {
        XCloseDisplay(gpDisplay);
        gpDisplay = NULL;
    }
}

//initialize() function
void initialize(void)
{
    //variable declaration
    GLenum result;
    GLint iShaderCompileStatus = 0;
    GLint iProgramLinkStatus   = 0;
    GLint iInfoLogLength       = 0;
    GLchar *szInfoLog          = NULL;
    
    GLuint vertexShaderObject;
    GLuint fragmentShaderObject;

    bool bCheck = false;

    //function declaration
    bool loadTexture(GLuint *, const char *);

    glxCreateContextAttribsARB = (glxCreateContextAttribsARBProc)glXGetProcAddressARB((GLubyte *)"glXCreateContextAttribsARB");
    if(glxCreateContextAttribsARB == NULL)
    {
    	printf("Address not found for glXCreateContextAttribsARB\n");
    	uninitialize();
    }

     const int attribs[] = {
    	GLX_CONTEXT_MAJOR_VERSION_ARB, 4,
    	GLX_CONTEXT_MINOR_VERSION_ARB, 4,
    	GLX_CONTEXT_PROFILE_MASK_ARB, GLX_CONTEXT_CORE_PROFILE_BIT_ARB,
    	None
    };

    gGLXContext = glxCreateContextAttribsARB(gpDisplay, gGLXFBConfig, 0, True, attribs);

   

    if(!gGLXContext)
    {
    	//if obtained one is not highest one, specify the lowest one it will give you highest possible for it
    	const int glattribs[]={
    		GLX_CONTEXT_MAJOR_VERSION_ARB, 1,
    		GLX_CONTEXT_MINOR_VERSION_ARB, 0,
    		None
    	};

    	gGLXContext = glxCreateContextAttribsARB(gpDisplay, gGLXFBConfig, 0, True, glattribs);
    }

    //Check whether the obtained context is really a hardware rendering context or not
    if(!glXIsDirect(gpDisplay, gGLXContext))
    {
    	printf("The Obtained Context Is Not Hardware Rendering Context\n");
    }
    else
    {
    	printf("The Obtained Context Is Hardware Rendering Context\n");
    }

    //Make the Obtained Context as Current Rendering Context
    glXMakeCurrent(gpDisplay, gWindow, gGLXContext);

    // Initialize GLEW
    result = glewInit();
    if(result != GLEW_OK)
    {
        printf("glewInit() failed");
        uninitialize();
    }

    // Define Vertex Shader Object
    vertexShaderObject = glCreateShader(GL_VERTEX_SHADER);
    
    // Write Vertex Shader Code
    const GLchar *vertexShaderSourceCode =
        "   #version 430 core                                                                   " \
        "   \n                                                                                  " \
        "   in vec4 vPosition;                                                                  " \
        "   in vec3 vNormal;                                                                    " \
        "   in vec4 vColor;                                                                     " \
        "   in vec2 vTexCoord;                                                                  " \
        "                                                                                     \n" \
        "   out vec4 out_color;                                                                 " \
        "   out vec2 out_tex;                                                                   " \
        "   out vec4 eyeCoordinates;                                                            " \
        "   out vec3 tNorm;                                                                     " \
        "   out vec3 lightDirection;                                                            " \
        "   out vec3 viewerVector;                                                              " \
        "                                                                                     \n" \
        "   uniform mat4 u_m_matrix;                                                            " \
        "   uniform mat4 u_v_matrix;                                                            " \
        "   uniform mat4 u_p_matrix;                                                            " \
        "   uniform int u_l_key_is_pressed;                                                     " \
        "   uniform vec4 u_light_position;                                                      " \
        "                                                                                     \n" \
        "   void main(void)                                                                     " \
        "   {                                                                                   " \
        "       if(u_l_key_is_pressed == 1)                                                     " \
        "       {                                                                               " \
        "           eyeCoordinates = u_v_matrix * u_m_matrix * vPosition;                       " \
        "           tNorm = mat3(u_v_matrix * u_m_matrix) * vNormal;                            " \
        "           lightDirection = vec3(u_light_position - eyeCoordinates);                   " \
        "           viewerVector = vec3(-eyeCoordinates);                                       " \
        "       }                                                                               " \
        "                                                                                     \n" \
        "   gl_Position = u_v_matrix * u_p_matrix * u_m_matrix * vPosition;                     " \
        "   out_tex = vTexCoord;                                                              \n" \
        "   out_color = vColor;                                                               \n" \
        "   }                                                                                   ";


    // Specify Above Source Code To The Verex Shader Object
    glShaderSource(vertexShaderObject, 1, (const GLchar **)&vertexShaderSourceCode, NULL);

    // Compile Vertex Shader
    glCompileShader(vertexShaderObject);
    
    // Check Compilation Status
    glGetShaderiv(vertexShaderObject, GL_COMPILE_STATUS, &iShaderCompileStatus);

    //Error Checking
    if(iShaderCompileStatus == GL_FALSE)
    {
        glGetShaderiv(vertexShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
        if(iInfoLogLength > 0)
        {
            szInfoLog = (GLchar *)malloc(iInfoLogLength);
            if(szInfoLog != NULL)
            {
                GLsizei written;
                glGetShaderInfoLog(vertexShaderObject, iInfoLogLength, &written, szInfoLog);
                printf("VERTEX SHADER COMPILATION LOG : %s\n", szInfoLog);
                free(szInfoLog);
                uninitialize();
            }
        }
    }

    // Define Fragment Shader Object
    fragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);

    // Write fragment shader source code
    const GLchar *fragmentShaderSourceCode =
        "   #version 430 core                                                                       " \
        "   \n                                                                                      " \
        "   in vec3 tNorm;                                                                          " \
        "   in vec3 lightDirection;                                                                 " \
        "   in vec3 viewerVector;                                                                   " \
        "   in vec2 out_tex;                                                                      \n" \
        "   in vec4 out_color;                                                                    \n" \
        "                                                                                         \n" \
        "   out vec4 fragColor;                                                                     " \
        "                                                                                         \n" \
        "   uniform vec3 u_ld;                                                                      " \
        "   uniform vec3 u_kd;                                                                      " \
        "   uniform vec3 u_la;                                                                      " \
        "   uniform vec3 u_ka;                                                                      " \
        "   uniform vec3 u_ls;                                                                      " \
        "   uniform vec3 u_ks;                                                                      " \
        "   uniform float u_material_shininess;                                                     " \
        "   uniform int u_l_key_is_pressed;                                                         " \
        "   uniform sampler2D u_sampler;                                                          \n" \
        "                                                                                         \n" \
        "   void main(void)                                                                         " \
        "   {                                                                                       " \
        "       vec3 phong_ads_light;                                                               " \
        "       vec4 texture_color;                                                               \n" \
        "                                                                                         \n" \
        "       if(u_l_key_is_pressed == 1)                                                         " \
        "       {                                                                                   " \
        "           vec3 normalizedtNorm = normalize(tNorm);                                        " \
        "           vec3 normalizedlightDirection = normalize(lightDirection);                      " \
        "           vec3 normalizedviewerVector = normalize(viewerVector);                          " \
        "                                                                                         \n" \
        "           vec3 reflectionVector = normalize(reflect(-normalizedlightDirection, normalizedtNorm));" \
        "           float tnDotld = max(dot(normalizedlightDirection, normalizedtNorm), 0.0);       " \
        "           float rvDotvv = max(dot(reflectionVector, normalizedviewerVector), 0.0);        " \
        "                                                                                         \n" \
        "           vec3 ambient = u_la * u_ka;                                                     " \
        "           vec3 diffuse = u_ld * u_kd * tnDotld;                                           " \
        "           vec3 specular = u_ls * u_ks * pow(rvDotvv, u_material_shininess);               " \
        "           phong_ads_light = ambient + diffuse + specular;                                 " \
        "                                                                                         \n" \
        "       }                                                                                   " \
        "       else                                                                                " \
        "       {                                                                                   " \
        "           phong_ads_light = vec3(1.0, 1.0, 1.0);                                          " \
        "       }                                                                                   " \
        "                                                                                         \n" \
        "   texture_color = texture(u_sampler, out_tex);                                          \n" \
        "                                                                                         \n" \
        "   fragColor = texture_color * vec4(( vec3(out_color) *phong_ads_light), 1.0);           \n" \
        "   }                                                                                       ";


    // Specify above source code to fragment shader object
    glShaderSource(fragmentShaderObject, 1, (const GLchar **)&fragmentShaderSourceCode, NULL);

    // Compile fragment shader
    iShaderCompileStatus = 0;
    iInfoLogLength =0;
    szInfoLog = NULL;

    glCompileShader(fragmentShaderObject);
    glGetShaderiv(fragmentShaderObject, GL_COMPILE_STATUS, &iShaderCompileStatus);

    if(iShaderCompileStatus == GL_FALSE)
    {
        glGetShaderiv(fragmentShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
        if(iInfoLogLength > 0)
        {
            szInfoLog = (GLchar *)malloc(iInfoLogLength);
            if (szInfoLog != NULL)
            {
                GLsizei written;
                glGetShaderInfoLog(fragmentShaderObject, iInfoLogLength, &written, szInfoLog);
                printf("FRAGMENT SHADER COMPILATION LOG : %s\n", szInfoLog);
                free(szInfoLog);
                uninitialize();
            }

        }
    }

    // Create Shader Program Object
    shaderProgramObject = glCreateProgram();

    // Attach Vertex Shader and Fragment Shader to the Shader Program
    glAttachShader(shaderProgramObject, vertexShaderObject);
    glAttachShader(shaderProgramObject, fragmentShaderObject);

    // Pre-Linking  binding to vertex attribute
    glBindAttribLocation(shaderProgramObject, AMC_ATTRIBUTE_POSITION, "vPosition");
    glBindAttribLocation(shaderProgramObject, AMC_ATTRIBUTE_NORMAL, "vNormal");
    glBindAttribLocation(shaderProgramObject, AMC_ATTRIBUTE_TEXCOORD0, "vTexCoord");
    glBindAttribLocation(shaderProgramObject, AMC_ATTRIBUTE_COLOR, "vColor");

    // Link Shader Program
    glLinkProgram(shaderProgramObject);

    // Error Checking
    iProgramLinkStatus = 0;
    iInfoLogLength = 0;
    szInfoLog = NULL;

    glGetProgramiv(shaderProgramObject, GL_LINK_STATUS, &iProgramLinkStatus);
    if(iProgramLinkStatus == GL_FALSE)
    {
        glGetProgramiv(shaderProgramObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
        if(iInfoLogLength > 0)
        {
            szInfoLog = (GLchar *)malloc(iInfoLogLength);
            if(szInfoLog != NULL)
            {
                GLint written;
                glGetProgramInfoLog(shaderProgramObject, iInfoLogLength, &written, szInfoLog);
                free(szInfoLog);
                uninitialize();
            }
        }
    }

    // Post-Linking retrieving uniform location
    vUniform = glGetUniformLocation(shaderProgramObject, "u_v_matrix");
    mUniform = glGetUniformLocation(shaderProgramObject, "u_m_matrix");
    pUniform = glGetUniformLocation(shaderProgramObject, "u_p_matrix");
    LdUniform = glGetUniformLocation(shaderProgramObject, "u_ld");
    KdUniform = glGetUniformLocation(shaderProgramObject, "u_kd");
    LaUniform = glGetUniformLocation(shaderProgramObject, "u_la");
    KaUniform = glGetUniformLocation(shaderProgramObject, "u_ka");
    LsUniform = glGetUniformLocation(shaderProgramObject, "u_ls");
    KsUniform = glGetUniformLocation(shaderProgramObject, "u_ks");
    materialShininessUniform = glGetUniformLocation(shaderProgramObject, "u_material_shininess");
    lightPositionUniform = glGetUniformLocation(shaderProgramObject, "u_light_position");
    keyPressedUniform = glGetUniformLocation(shaderProgramObject, "u_l_key_is_pressed");
    samplerUniform = glGetUniformLocation(shaderProgramObject, "u_sampler");

    const GLfloat cube_vcnt[] = {
         1.0f,  1.0f,  1.0f,    1.0f, 0.0f, 0.0f,    0.0f,  0.0f,  1.0f,    1.0f, 1.0f, 
        -1.0f,  1.0f,  1.0f,    1.0f, 0.0f, 0.0f,    0.0f,  0.0f,  1.0f,    0.0f, 1.0f,
        -1.0f, -1.0f,  1.0f,    1.0f, 0.0f, 0.0f,    0.0f,  0.0f,  1.0f,    0.0f, 0.0f,
         1.0f, -1.0f,  1.0f,    1.0f, 0.0f, 0.0f,    0.0f,  0.0f,  1.0f,    1.0f, 0.0f,
         1.0f,  1.0f, -1.0f,    0.0f, 1.0f, 0.0f,    1.0f,  0.0f,  0.0f,    1.0f, 0.0f, 
         1.0f,  1.0f,  1.0f,    0.0f, 1.0f, 0.0f,    1.0f,  0.0f,  0.0f,    1.0f, 1.0f,
         1.0f, -1.0f,  1.0f,    0.0f, 1.0f, 0.0f,    1.0f,  0.0f,  0.0f,    0.0f, 1.0f,
         1.0f, -1.0f, -1.0f,    0.0f, 1.0f, 0.0f,    1.0f,  0.0f,  0.0f,    0.0f, 0.0f,
         1.0f,  1.0f, -1.0f,    0.0f, 0.0f, 1.0f,    0.0f,  0.0f, -1.0f,    0.0f, 0.0f, 
        -1.0f,  1.0f, -1.0f,    0.0f, 0.0f, 1.0f,    0.0f,  0.0f, -1.0f,    0.0f, 1.0f,
        -1.0f, -1.0f, -1.0f,    0.0f, 0.0f, 1.0f,    0.0f,  0.0f, -1.0f,    1.0f, 1.0f,
         1.0f, -1.0f, -1.0f,    0.0f, 0.0f, 1.0f,    0.0f,  0.0f, -1.0f,    1.0f, 0.0f,
        -1.0f,  1.0f, -1.0f,    1.0f, 0.0f, 1.0f,   -1.0f,  0.0f,  0.0f,    1.0f, 0.0f, 
        -1.0f,  1.0f,  1.0f,    1.0f, 0.0f, 1.0f,   -1.0f,  0.0f,  0.0f,    0.0f, 0.0f,
        -1.0f, -1.0f,  1.0f,    1.0f, 0.0f, 1.0f,   -1.0f,  0.0f,  0.0f,    0.0f, 1.0f,
        -1.0f, -1.0f, -1.0f,    1.0f, 0.0f, 1.0f,   -1.0f,  0.0f,  0.0f,    1.0f, 1.0f,
         1.0f,  1.0f, -1.0f,    1.0f, 1.0f, 0.0f,    0.0f,  1.0f,  0.0f,    0.0f, 1.0f, 
        -1.0f,  1.0f, -1.0f,    1.0f, 1.0f, 0.0f,    0.0f,  1.0f,  0.0f,    0.0f, 0.0f,
        -1.0f,  1.0f,  1.0f,    1.0f, 1.0f, 0.0f,    0.0f,  1.0f,  0.0f,    1.0f, 0.0f,
         1.0f,  1.0f,  1.0f,    1.0f, 1.0f, 0.0f,    0.0f,  1.0f,  0.0f,    1.0f, 1.0f,
         1.0f, -1.0f, -1.0f,    0.0f, 1.0f, 1.0f,    0.0f, -1.0f,  0.0f,    1.0f, 0.0f, 
        -1.0f, -1.0f, -1.0f,    0.0f, 1.0f, 1.0f,    0.0f, -1.0f,  0.0f,    0.0f, 0.0f,
        -1.0f, -1.0f,  1.0f,    0.0f, 1.0f, 1.0f,    0.0f, -1.0f,  0.0f,    0.0f, 1.0f,
         1.0f, -1.0f,  1.0f,    0.0f, 1.0f, 1.0f,    0.0f, -1.0f,  0.0f,    1.0f, 1.0f
    };//front:4-right:4-back:4-left:4-top:4-bottom:4
    

    // ***** RECTANGLE *****

    glGenVertexArrays(1, &vao_cube);
    glBindVertexArray(vao_cube);

    // Buffer for position
    glGenBuffers(1, &vbo_cube_data);
    glBindBuffer(GL_ARRAY_BUFFER, vbo_cube_data);
    glBufferData(GL_ARRAY_BUFFER, 11 * 24 * sizeof(GLfloat), cube_vcnt, GL_STATIC_DRAW);
    
    glVertexAttribPointer(AMC_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 11 * sizeof(GLfloat), (void *)(0 * sizeof(GLfloat)));
    glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);
    
    glVertexAttribPointer(AMC_ATTRIBUTE_COLOR, 3, GL_FLOAT, GL_FALSE, 11 * sizeof(GLfloat), (void *)(3 * sizeof(GLfloat)));
    glEnableVertexAttribArray(AMC_ATTRIBUTE_COLOR);
    
    glVertexAttribPointer(AMC_ATTRIBUTE_NORMAL, 3, GL_FLOAT, GL_FALSE, 11 * sizeof(GLfloat), (void *)(6 * sizeof(GLfloat)));
    glEnableVertexAttribArray(AMC_ATTRIBUTE_NORMAL);
    
    glVertexAttribPointer(AMC_ATTRIBUTE_TEXCOORD0, 2, GL_FLOAT, GL_FALSE, 11 * sizeof(GLfloat), (void *)(9 * sizeof(GLfloat)));
    glEnableVertexAttribArray(AMC_ATTRIBUTE_TEXCOORD0);
    
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glBindVertexArray(0);

    // Load Textures
    bCheck = loadTexture(&textureMarble, "./Marble.bmp");
    if(bCheck == false)
    {
    	printf("Marble Texture Loading Failed\n");
    }

    //glDisable(GL_CULL_FACE);
    glClearDepth(1.0f);
    glEnable(GL_DEPTH_TEST);
    glDepthFunc(GL_LEQUAL);

    glClearColor(0.0f,0.0f,0.0f,1.0f);

    perspectiveProjectionMatrix = mat4::identity();
}

//resize() function
void resize(int width, int height)
{
    if(height == 0)
        height =1;
    glViewport(0,0,(GLsizei)width, (GLsizei)height);

    perspectiveProjectionMatrix = perspective(45.0f, (GLfloat)width/(GLfloat)height, 0.1f, 100.0f);
}

// update function
void update(void)
{
    if(angleCube > 360.0f)
    {
        angleCube = 0.0f;
    }

    angleCube = angleCube + 0.05f;
}

//display function
void display(void)
{
    // Declaration of matrices
    mat4 modelMatrix;
    mat4 viewMatrix;
    mat4 projectionMatrix;
    mat4 translationMatrix;
    mat4 rotationMatrix;
    mat4 scaleMatrix;

    // Initialize above matrices to identity
    modelMatrix = mat4::identity();
    viewMatrix = mat4::identity();
    projectionMatrix = mat4::identity();
    translationMatrix = mat4::identity();
    rotationMatrix = mat4::identity();
    scaleMatrix = mat4::identity();

    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glUseProgram(shaderProgramObject);

    // transformations
    translationMatrix = translate(0.0f, 0.0f, -6.0f);
    rotationMatrix = rotate(angleCube, angleCube, angleCube);
    modelMatrix = translationMatrix * scaleMatrix * rotationMatrix;
    projectionMatrix = perspectiveProjectionMatrix;

    if(bEnableLighting == true)
    {
        glUniform1i(keyPressedUniform, 1);
        glUniform3fv(LdUniform, 1, lightDiffuse);
        glUniform3fv(LaUniform, 1, lightAmbient);
        glUniform3fv(LsUniform, 1, lightSpecular);

        glUniform3fv(KdUniform, 1, materialDiffuse);
        glUniform3fv(KaUniform, 1, materialAmbient);
        glUniform3fv(KsUniform, 1, materialSpecular);

        glUniform1f(materialShininessUniform, materialShininess);

        glUniform4fv(lightPositionUniform, 1, lightPosition);
    }
    else
    {
        glUniform1i(keyPressedUniform, 0);
    }

    //send necessary matrices to shader in respective uniforms
    glUniform1i(samplerUniform, 0);
    glUniformMatrix4fv(mUniform, 1, GL_FALSE, modelMatrix);
    glUniformMatrix4fv(vUniform, 1, GL_FALSE, viewMatrix);
    glUniformMatrix4fv(pUniform, 1, GL_FALSE, projectionMatrix);

    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, textureMarble);

    // ***** CUBE *****
    //-----------------------------------------------
    // Bind to vao for cube
    glBindVertexArray(vao_cube);
    glDrawArrays(GL_TRIANGLE_FAN, 0, 4);
    glDrawArrays(GL_TRIANGLE_FAN, 4, 4);
    glDrawArrays(GL_TRIANGLE_FAN, 8, 4);
    glDrawArrays(GL_TRIANGLE_FAN, 12, 4);
    glDrawArrays(GL_TRIANGLE_FAN, 16, 4);
    glDrawArrays(GL_TRIANGLE_FAN, 20, 4);

    // Unbind to vao
    glBindVertexArray(0);
    glBindTexture(GL_TEXTURE_2D, 0);
    //-----------------------------------------------

    // Unuse program
    glUseProgram(0);
    glXSwapBuffers(gpDisplay, gWindow);
}

// function to load texture
bool loadTexture(GLuint *texture, const char *path)
{
    int imageWidth, imageHeight;
    bool bStatus = false;
    unsigned char *imageData = NULL;

    imageData = SOIL_load_image(path, &imageWidth, &imageHeight, 0, SOIL_LOAD_RGB);
    if(imageData == NULL)
    {
        bStatus = false;
        printf("Image Data Is NULL\n");
        return(bStatus);
    }
    else
        bStatus = true;

    glEnable(GL_TEXTURE_2D);
    glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
    glGenTextures(1, texture);
    glBindTexture(GL_TEXTURE_2D, *texture);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
    glTexImage2D(GL_TEXTURE_2D,
        0,
        GL_RGB,
        imageWidth,
        imageHeight,
        0,
        GL_RGB,
        GL_UNSIGNED_BYTE,
        imageData);
    glGenerateMipmap(GL_TEXTURE_2D);
    SOIL_free_image_data(imageData);
    return(bStatus);
}



