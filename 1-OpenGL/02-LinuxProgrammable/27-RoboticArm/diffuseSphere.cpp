#include<iostream>
#include<stdio.h>
#include<stdlib.h>
#include<memory.h>

#include<X11/Xlib.h>
#include<X11/Xutil.h>
#include<X11/XKBlib.h>
#include<X11/keysym.h>

#include<GL/glew.h>
#include<GL/gl.h>
#include<GL/glx.h>

#include"vmath.h"
#include"Sphere.h"
#include"List.h"

//namespaces
using namespace std;
using namespace vmath;

//global variable declarations
bool bFullScreen = false;
Display *gpDisplay = NULL;
XVisualInfo *gpXVisualInfo = NULL;
Colormap gColormap;
Window gWindow;
int giWindowWidth = 800;
int giWindowHeight = 600;
int gNumVertices;
int gNumElements;

typedef GLXContext(*glxCreateContextAttribsARBProc)(Display*, GLXFBConfig, GLXContext, Bool, const int*);
glxCreateContextAttribsARBProc glxCreateContextAttribsARB = NULL;
GLXFBConfig gGLXFBConfig;
GLXContext gGLXContext;


GLuint shaderProgramObject;

GLuint vao_sphere;
GLuint vbo_sphere_position;
GLuint vbo_sphere_normal;
GLuint vbo_sphere_elements;

GLuint mUniform;
GLuint vUniform;
GLuint pUniform;
GLuint ldUniform;
GLuint kdUniform;
GLuint lightPositionUniform;
GLuint keyPressedUniform;
GLuint colorUniform;

float lightDiffuse[4] = {1.0f, 1.0f, 1.0f, 1.0f};
float materialDiffuse[4] = {1.0f, 1.0f, 1.0f};
float lightPosition[4] = {0.0f, 0.0f, 2.0f, 1.0f};
float sphere_vertices[1146];
float sphere_normals[1146];
float sphere_textures[764];
int elbow = 0, shoulder = 0;

unsigned short sphere_elements[2280];

bool startAnimation = false;
bool enableLighting = false;

list_t *modelData = NULL;

mat4 perspectiveProjectionMatrix;

enum 
{
    AMC_ATTRIBUTE_POSITION = 0,
    AMC_ATTRIBUTE_COLOR,
    AMC_ATTRIBUTE_NORMAL,
    AMC_ATTRIBUTE_TEXCOORD0
};

//entry point function
int main(void)
{
    //function prototype
    void CreateWindow(void);
    void ToggleFullScreen(void);
    void uninitialize();
    void initialize(void);
    void resize(int,int);
    void display(void);
    void update(void);

    //variable declarations
    int winWidth = giWindowWidth;
    int winHeight = giWindowHeight;
    bool bDone = false;
    char keys[26];
    
    //code
    CreateWindow();
    
    //initialize
    initialize();

    //Message Loop
    XEvent event;
    KeySym keysym;

    while(bDone == false)
    {
        while(XPending(gpDisplay))
        {
            XNextEvent(gpDisplay,&event);
            switch(event.type)
            {
                case MapNotify:
                    break;
                case KeyPress:
                    keysym = XkbKeycodeToKeysym(gpDisplay, event.xkey.keycode, 0, 0);
                    switch(keysym)
                    {
                        case XK_Escape:
                            bDone = true;
                            break;
                        case XK_F:
                        case XK_f:
                            if(bFullScreen == false)
                            {
                                ToggleFullScreen();
                                bFullScreen = true;
                            }
                            else
                            {
                                ToggleFullScreen();
                                bFullScreen = false;
                            }
                            break;
                    }
                    XLookupString(&event.xkey, keys, sizeof(keys), NULL, NULL);
                    switch(keys[0])
                    {
                        case 'A':
                        case 'a':
                            if(startAnimation == false)
                            {
                                startAnimation= true;
                            }
                            else
                            {
                                startAnimation = false;
                            }
                            break;
                        case 'L':
                        case 'l':
                            if(enableLighting == false)
                            {
                                enableLighting = true;
                            }
                            else
                            {
                                enableLighting = false;
                            }
                            break;
                        case 's':
                            printf("shoulder -: %d\n", shoulder);
                            shoulder = (shoulder - 3) % 360;
                            break;

                        case 'S':
                            printf("shoulder +: %d\n", shoulder);
                            shoulder = (shoulder + 3) % 360;
                            break;
                        
                        case 'e':
                            printf("elbow -: %d\n", elbow);
                            elbow = (elbow - 6) % 360;
                            break;  
                        case 'E':
                            printf("elbow +: %d\n", elbow);
                            elbow = (elbow + 6) % 360;
                            break;                          
                       default:
                            break;
                    }
                    break;
                case ButtonPress:
                   switch(event.xbutton.button)
                   {
                        case 1:                         //left button
                            break;
                        case 2:                         //middle button
                            break;
                        case 3:                         //right button
                            break;
                        default:
                            break;
                   }
                   break;
                case MotionNotify:
                   break;
                case ConfigureNotify:
                    winWidth = event.xconfigure.width;
                    winHeight =event.xconfigure.height;
                    resize(winWidth, winHeight);
                    break;
                case Expose:
                    break;
                case DestroyNotify:
                    break;
                case 33:                                //to handle close button and close menu
                    bDone = true;
                    break;
                default:
                    break;
            }
        }
        display();
    }
    uninitialize();
    return(0);
}

void CreateWindow(void)
{
    //function prototype
    void uninitialize(void);

    //variable declarations
    XSetWindowAttributes winAttribs;
    int defaultScreen;
    int defaultDepth;
    int styleMask;
    int iNumberOfFBConfigs = 0;					// number of FBConfigs
    GLXFBConfig *pGLXFBConfigs = NULL;			//array of FBConfigs
    GLXFBConfig bestGLXFBConfig;				//best FBConfig among all
    XVisualInfo *pTempXVisualInfo = NULL;		//temporary visual info

    // Initialize Frame Buffer Attributes
    static int frameBufferAttributes[] = {
        GLX_X_RENDERABLE, True,
        GLX_DRAWABLE_TYPE,GLX_WINDOW_BIT,
        GLX_RENDER_TYPE, GLX_RGBA_BIT,
        GLX_X_VISUAL_TYPE, GLX_TRUE_COLOR,
        GLX_DOUBLEBUFFER, True,
        GLX_RED_SIZE, 8,
        GLX_GREEN_SIZE , 8,
        GLX_BLUE_SIZE, 8,
        GLX_ALPHA_SIZE, 8,
        GLX_DEPTH_SIZE, 24,
        GLX_STENCIL_SIZE, 8,
        None};

    //code
    gpDisplay = XOpenDisplay(NULL);
    if(!gpDisplay)
    {
        printf("ERROR : Unable To Open X Display.\nExitting Now ...\n");
        uninitialize();
        exit(1);
    }

    defaultScreen = XDefaultScreen(gpDisplay);

	// Retrive All FBConfigs Driver Has
	pGLXFBConfigs = glXChooseFBConfig(gpDisplay, defaultScreen,	frameBufferAttributes, &iNumberOfFBConfigs);

	printf("Number Of FBConfigs Matching To FBConfig = %d\n", iNumberOfFBConfigs);
	// Now We Have Multiple FBConfigs, We Need To Find Best Out Of Them

	int bestFrameBufferConfig = -1;
	int bestNumberOfSamples   = -1;
	int worstFrameBufferConfig = -1;
	int worstNumberOfSamples  = 999;

	//loop to check all fb configs
	for(int i = 0; i < iNumberOfFBConfigs; i++)
	{
		// Getting visual from each obtained FBConfig
		pTempXVisualInfo = glXGetVisualFromFBConfig(gpDisplay, pGLXFBConfigs[i]);

		if(pTempXVisualInfo)
		{
			//get Number of samplebuffer from respective fbConfig
			int sampleBuffers, samples;
			glXGetFBConfigAttrib(gpDisplay,
				pGLXFBConfigs[i],
				GLX_SAMPLE_BUFFERS,
				&sampleBuffers);

			//get number of samples from respective FBConfig
			glXGetFBConfigAttrib(gpDisplay, pGLXFBConfigs[i], GLX_SAMPLES, &samples);

			//More the number of samples and sample buffer more the eligible config buffer is
			if(bestFrameBufferConfig < 0 || sampleBuffers && samples >bestNumberOfSamples)
			{
				bestFrameBufferConfig=i;
				bestNumberOfSamples =samples;
			}

			if(worstFrameBufferConfig < 0 || !sampleBuffers || samples < worstNumberOfSamples)
			{
				worstFrameBufferConfig = i;
				worstNumberOfSamples = samples;
			}
		}

		XFree(pTempXVisualInfo);
	}

	// Now assign the found best one
	bestGLXFBConfig = pGLXFBConfigs[bestFrameBufferConfig];

	//Now assign the local best to global best
	gGLXFBConfig = bestGLXFBConfig;

	//Free the obtaind GLXFBConfig array;
	XFree(pGLXFBConfigs);

	//Accordingly get best visual
	gpXVisualInfo = glXGetVisualFromFBConfig(gpDisplay,  bestGLXFBConfig);

    winAttribs.border_pixel = 0;
    winAttribs.background_pixmap = 0;
    winAttribs.colormap = XCreateColormap(gpDisplay, 
                                          RootWindow(gpDisplay, gpXVisualInfo->screen),
                                          gpXVisualInfo->visual,
                                          AllocNone);
    gColormap = winAttribs.colormap;

    winAttribs.background_pixel = BlackPixel(gpDisplay,defaultScreen);

    winAttribs.event_mask = ExposureMask | VisibilityChangeMask | ButtonPressMask |
                           KeyPressMask | PointerMotionMask | StructureNotifyMask;
   
    styleMask = CWBorderPixel | CWBackPixel | CWEventMask | CWColormap;

    gWindow = XCreateWindow(gpDisplay,
                            RootWindow(gpDisplay,gpXVisualInfo->screen),
                            0,
                            0,
                            giWindowWidth,
                            giWindowHeight,
                            0,                                              // border width
                            gpXVisualInfo->depth,                           // color depth
                            InputOutput,
                            gpXVisualInfo->visual,
                            styleMask,
                            &winAttribs);
    if(!gWindow)
    {
        printf("ERROR : Failed To Create Main Window.\nExitting Now ...\n");
        uninitialize();
        exit(1);
    }

    XStoreName(gpDisplay, gWindow, "Diffuse Light On Sphere");

    Atom windowManagerDelete = XInternAtom(gpDisplay, "WM_DELETE_WINDOW", True);
    XSetWMProtocols(gpDisplay, gWindow, &windowManagerDelete, 1);

    XMapWindow(gpDisplay, gWindow);
}

void ToggleFullScreen(void)
{
    //variable declarations
    Atom  wm_state;
    Atom fullscreen;
    XEvent xev = {0};

    //code
    wm_state = XInternAtom(gpDisplay, "_NET_WM_STATE", False);
    memset(&xev,0,sizeof(xev));

    xev.type = ClientMessage;
    xev.xclient.window = gWindow;
    xev.xclient.message_type = wm_state;
    xev.xclient.format = 32;
    xev.xclient.data.l[0] = bFullScreen ? 0 : 1;

    fullscreen = XInternAtom(gpDisplay, "_NET_WM_STATE_FULLSCREEN", False);
    xev.xclient.data.l[1] = fullscreen;
    
    XSendEvent(gpDisplay,
               RootWindow(gpDisplay, gpXVisualInfo->screen),
               False,
               StructureNotifyMask,
               &xev);
}

void uninitialize(void)
{
	if(vbo_sphere_elements)
    {
        glDeleteBuffers(1, &vbo_sphere_elements);
        vbo_sphere_elements = 0;
    }

    if(vbo_sphere_normal)
    {
        glDeleteBuffers(1, &vbo_sphere_normal);
        vbo_sphere_normal = 0;
    }

    if (vbo_sphere_position)
    {
        glDeleteBuffers(1, &vbo_sphere_position);
        vbo_sphere_position = 0;
    }

    if (vao_sphere)
    {
        glDeleteVertexArrays(1, &vao_sphere);
        vao_sphere = 0;
    }

    if(shaderProgramObject)
    {
        GLsizei shaderCount;
        GLsizei shaderNumber;

        glUseProgram(shaderProgramObject);
        glGetProgramiv(shaderProgramObject, GL_ATTACHED_SHADERS, &shaderCount);
        GLuint *pShaders = (GLuint *)malloc(sizeof(GLuint) * shaderCount);

        if(pShaders)
        {
            glGetAttachedShaders(shaderProgramObject, shaderCount, &shaderCount, pShaders);
            for(shaderNumber = 0; shaderNumber < shaderCount; shaderNumber++)
            {
                // Detach all shaaders one by one
                glDetachShader(shaderProgramObject, pShaders[shaderNumber]);
                // Delete the detached shader
                glDeleteShader(pShaders[shaderNumber]);
                pShaders[shaderNumber] = 0;
            }
            free(pShaders);
        }
        glDeleteProgram(shaderProgramObject);
        shaderProgramObject = 0;
        glUseProgram(0);
    }

    if(gWindow)
    {
        XDestroyWindow(gpDisplay,gWindow);
    }
    if(gColormap)
    {
        XFreeColormap(gpDisplay, gColormap);
    }
    if(gpXVisualInfo)
    {
        free(gpXVisualInfo);
        gpXVisualInfo = NULL;
    }
    if(gpDisplay)
    {
        XCloseDisplay(gpDisplay);
        gpDisplay = NULL;
    }
}

//initialize() function
void initialize(void)
{
    //variable declaration
    GLenum result;
    GLint iShaderCompileStatus = 0;
    GLint iProgramLinkStatus   = 0;
    GLint iInfoLogLength       = 0;
    GLchar *szInfoLog          = NULL;
    
    GLuint vertexShaderObject;
    GLuint fragmentShaderObject;

    glxCreateContextAttribsARB = (glxCreateContextAttribsARBProc)glXGetProcAddressARB((GLubyte *)"glXCreateContextAttribsARB");
    if(glxCreateContextAttribsARB == NULL)
    {
    	printf("Address not found for glXCreateContextAttribsARB\n");
    	uninitialize();
    }

     const int attribs[] = {
    	GLX_CONTEXT_MAJOR_VERSION_ARB, 4,
    	GLX_CONTEXT_MINOR_VERSION_ARB, 4,
    	GLX_CONTEXT_PROFILE_MASK_ARB, GLX_CONTEXT_CORE_PROFILE_BIT_ARB,
    	None
    };

    gGLXContext = glxCreateContextAttribsARB(gpDisplay, gGLXFBConfig, 0, True, attribs);

   

    if(!gGLXContext)
    {
    	//if obtained one is not highest one, specify the lowest one it will give you highest possible for it
    	const int glattribs[]={
    		GLX_CONTEXT_MAJOR_VERSION_ARB, 1,
    		GLX_CONTEXT_MINOR_VERSION_ARB, 0,
    		None
    	};

    	gGLXContext = glxCreateContextAttribsARB(gpDisplay, gGLXFBConfig, 0, True, glattribs);
    }

    //Check whether the obtained context is really a hardware rendering context or not
    if(!glXIsDirect(gpDisplay, gGLXContext))
    {
    	printf("The Obtained Context Is Not Hardware Rendering Context\n");
    }
    else
    {
    	printf("The Obtained Context Is Hardware Rendering Context\n");
    }

    //Make the Obtained Context as Current Rendering Context
    glXMakeCurrent(gpDisplay, gWindow, gGLXContext);

    // Initialize GLEW
    result = glewInit();
    if(result != GLEW_OK)
    {
        printf("glewInit() failed");
        uninitialize();
    }

    // Define Vertex Shader Object
    vertexShaderObject = glCreateShader(GL_VERTEX_SHADER);
    
    // Write Vertex Shader Code
    const GLchar *vertexShaderSourceCode =
    "	#version 430														                " \
    "																	                \n  " \
    "	in vec4 vPosition;													               " \
    "   in vec3 vNormal;                                                                    " \
    "																	                  \n  " \
    "	out vec3 diffuse_color;													              " \
    "																	                  \n  " \
    "	uniform mat4 u_m_matrix;											                   " \
    "	uniform mat4 u_v_matrix;											               " \
    "	uniform mat4 u_p_matrix;											" \
    "   uniform vec4 u_ld;                                                      " \
    "   uniform vec4 u_kd;                                                                  " \
    "   uniform vec4 u_light_position;                                                      " \
    "   uniform int u_enable_lighting;                                                      " \
    "   uniform vec3 u_color;                                                      " \
    "																	                \n  " \
    "	void main(void)														                " \
    "	{																	                " \
    "       if(u_enable_lighting == 1)                                                                   " \
    "       {                                                                               " \
    "           vec4 eyeCoordinates = u_v_matrix * u_m_matrix * vPosition;                  " \
    "           mat3 normalMatrix   = mat3(u_v_matrix * u_m_matrix);                        " \
    "           vec3 tNorm          = normalize(normalMatrix * vNormal);                    " \
    "           vec3 s              = normalize(vec3(u_light_position - eyeCoordinates));   " \
    "           diffuse_color       = u_ld.xyz * u_kd.xyz * max(dot(s, tNorm), 0.0);                " \
    "           diffuse_color       = diffuse_color * u_color;                                              " \
    "       }                                                                               " \
    "		gl_Position = u_p_matrix * u_v_matrix * u_m_matrix * vPosition;	" \
    "	}																	";

    // Specify Above Source Code To The Verex Shader Object
    glShaderSource(vertexShaderObject, 1, (const GLchar **)&vertexShaderSourceCode, NULL);

    // Compile Vertex Shader
    glCompileShader(vertexShaderObject);
    
    // Check Compilation Status
    glGetShaderiv(vertexShaderObject, GL_COMPILE_STATUS, &iShaderCompileStatus);

    //Error Checking
    if(iShaderCompileStatus == GL_FALSE)
    {
        glGetShaderiv(vertexShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
        if(iInfoLogLength > 0)
        {
            szInfoLog = (GLchar *)malloc(iInfoLogLength);
            if(szInfoLog != NULL)
            {
                GLsizei written;
                glGetShaderInfoLog(vertexShaderObject, iInfoLogLength, &written, szInfoLog);
                printf("VERTEX SHADER COMPILATION LOG : %s\n", szInfoLog);
                free(szInfoLog);
                uninitialize();
            }
        }
    }

    // Define Fragment Shader Object
    fragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);

    // Write fragment shader source code
    const GLchar *fragmentShaderSourceCode =
    "	#version 430												" \
    "															\n  " \
    "	in vec3 diffuse_color;										" \
    "															\n  " \
    "   uniform int u_enable_lighting;                              " \
    "   uniform vec3 u_color;                                       " \
    "                                                           \n  " \
    "	out vec4 FragColor;											" \
    "															\n  " \
    "	void main(void)												" \
    "	{															" \
    "       if(u_enable_lighting == 1)                              " \
    "       {                                                       " \
    "           FragColor = vec4(diffuse_color, 1.0);               " \
    "       }                                                       " \
    "       else                                                    " \
    "       {                                                       " \
    "           FragColor = vec4(u_color, 1.0);                     " \
    "       }                                                       " \
    "	}															";

    // Specify above source code to fragment shader object
    glShaderSource(fragmentShaderObject, 1, (const GLchar **)&fragmentShaderSourceCode, NULL);

    // Compile fragment shader
    iShaderCompileStatus = 0;
    iInfoLogLength =0;
    szInfoLog = NULL;

    glCompileShader(fragmentShaderObject);
    glGetShaderiv(fragmentShaderObject, GL_COMPILE_STATUS, &iShaderCompileStatus);

    if(iShaderCompileStatus == GL_FALSE)
    {
        glGetShaderiv(fragmentShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
        if(iInfoLogLength > 0)
        {
            szInfoLog = (GLchar *)malloc(iInfoLogLength);
            if (szInfoLog != NULL)
            {
                GLsizei written;
                glGetShaderInfoLog(fragmentShaderObject, iInfoLogLength, &written, szInfoLog);
                printf("FRAGMENT SHADER COMPILATION LOG : %s\n", szInfoLog);
                free(szInfoLog);
                uninitialize();
            }

        }
    }

    // Create Shader Program Object
    shaderProgramObject = glCreateProgram();

    // Attach Vertex Shader and Fragment Shader to the Shader Program
    glAttachShader(shaderProgramObject, vertexShaderObject);
    glAttachShader(shaderProgramObject, fragmentShaderObject);

    // Pre-Linking  binding to vertex attribute
    glBindAttribLocation(shaderProgramObject, AMC_ATTRIBUTE_POSITION, "vPosition");
    glBindAttribLocation(shaderProgramObject, AMC_ATTRIBUTE_NORMAL, "vNormal");

    // Link Shader Program
    glLinkProgram(shaderProgramObject);

    // Error Checking
    iProgramLinkStatus = 0;
    iInfoLogLength = 0;
    szInfoLog = NULL;
    glGetProgramiv(shaderProgramObject, GL_LINK_STATUS, &iProgramLinkStatus);
    if(iProgramLinkStatus == GL_FALSE)
    {
        glGetProgramiv(shaderProgramObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
        if(iInfoLogLength > 0)
        {
            szInfoLog = (GLchar *)malloc(iInfoLogLength);
            if(szInfoLog != NULL)
            {
                GLint written;
                glGetProgramInfoLog(shaderProgramObject, iInfoLogLength, &written, szInfoLog);
                free(szInfoLog);
                uninitialize();
            }
        }
    }

    // Post-Linking retrieving uniform location
    mUniform = glGetUniformLocation(shaderProgramObject, "u_m_matrix");
    vUniform = glGetUniformLocation(shaderProgramObject, "u_v_matrix");
    pUniform = glGetUniformLocation(shaderProgramObject, "u_p_matrix");
    ldUniform = glGetUniformLocation(shaderProgramObject, "u_ld");
    kdUniform = glGetUniformLocation(shaderProgramObject, "u_kd");
    lightPositionUniform = glGetUniformLocation(shaderProgramObject, "u_light_position");
    keyPressedUniform = glGetUniformLocation(shaderProgramObject, "u_enable_lighting");
    colorUniform = glGetUniformLocation(shaderProgramObject, "u_color");

    getSphereVertexData(sphere_vertices, sphere_normals, sphere_textures, sphere_elements);
    gNumVertices = getNumberOfSphereVertices();
    gNumElements = getNumberOfSphereElements();

    // ***** SPHERE *****
    glGenVertexArrays(1, &vao_sphere);
    glBindVertexArray(vao_sphere);

    // Buffer for position
    glGenBuffers(1, &vbo_sphere_position);
    glBindBuffer(GL_ARRAY_BUFFER, vbo_sphere_position);
    glBufferData(GL_ARRAY_BUFFER, sizeof(sphere_vertices), sphere_vertices, GL_STATIC_DRAW);
    glVertexAttribPointer(AMC_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
    glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    
    // Buffer for normal
    glGenBuffers(1, &vbo_sphere_normal);
    glBindBuffer(GL_ARRAY_BUFFER, vbo_sphere_normal);
    glBufferData(GL_ARRAY_BUFFER, sizeof(sphere_normals), sphere_normals, GL_STATIC_DRAW);
    glVertexAttribPointer(AMC_ATTRIBUTE_NORMAL, 3, GL_FLOAT, GL_FALSE, 0, NULL);
    glEnableVertexAttribArray(AMC_ATTRIBUTE_NORMAL);
    glBindBuffer(GL_ARRAY_BUFFER, 0);

    // Buffer for elements
    glGenBuffers(1, &vbo_sphere_elements);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_elements);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(sphere_elements), sphere_elements, GL_STATIC_DRAW);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

    // Unbind with vao
    glBindVertexArray(0);

    //glDisable(GL_CULL_FACE);
    glClearDepth(1.0f);
    glEnable(GL_DEPTH_TEST);
    glDepthFunc(GL_LEQUAL);

    glClearColor(0.0f,0.0f,0.0f,1.0f);

    perspectiveProjectionMatrix = mat4::identity();

    modelData = CreateList();

}

//resize() function
void resize(int width, int height)
{
    if(height == 0)
        height =1;
    glViewport(0,0,(GLsizei)width, (GLsizei)height);

    perspectiveProjectionMatrix = perspective(45.0f, (GLfloat)width/(GLfloat)height, 0.1f, 100.0f);
}

// update function
void update(void)
{
    //code
}

//display function
void display(void)
{
    // Declaration of function
    void update(void);

    // Declaration of matrices
    mat4 modelMatrix;
    mat4 viewMatrix;
    mat4 projectionMatrix;
    mat4 translationMatrix;
    mat4 rotationMatrix;
    mat4 scaleMatrix;

    // Initialize above matrices to identity
    modelMatrix 		= mat4::identity();
    viewMatrix 			= mat4::identity();
    projectionMatrix 	= mat4::identity();
    translationMatrix 	= mat4::identity();
    scaleMatrix 		= mat4::identity();
    rotationMatrix 		= mat4::identity();

    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    

    glUseProgram(shaderProgramObject);

    //do necessary matrix multiplication
    translationMatrix = translate(0.0f, 0.0f, -12.0f);

    modelMatrix         = translationMatrix;

    push(modelData, modelMatrix);

    translationMatrix = translationMatrix * translate(1.0f, 0.0f, 0.0f);
    rotationMatrix = rotate((float)shoulder, 0.0f, 0.0f, 1.0f);
    modelMatrix = rotationMatrix * translationMatrix * scaleMatrix ;
    projectionMatrix = projectionMatrix * perspectiveProjectionMatrix;
    
    push(modelData, modelMatrix);

    scaleMatrix = scale(2.0f, 0.5f, 1.0f);
    modelMatrix = modelMatrix * scaleMatrix;

    if(enableLighting == true)
    {
        glUniform1i(keyPressedUniform, 1);
        glUniform4fv(ldUniform, 1, lightDiffuse);
        glUniform4fv(kdUniform, 1, materialDiffuse);
        glUniform4fv(lightPositionUniform, 1, lightPosition);
    }
    else
    {
        glUniform1i(keyPressedUniform, 0);
    }

    // Send matrices to shaders in respective uniforms
    glUniformMatrix4fv(mUniform, 1, GL_FALSE, modelMatrix);
    glUniformMatrix4fv(vUniform, 1, GL_FALSE, viewMatrix);
    glUniformMatrix4fv(pUniform, 1, GL_FALSE, projectionMatrix);
    glUniform3fv(colorUniform, 1, vec3(0.87f, 0.72f, 0.53f));

    // ***** SPHERE *****
    //-----------------------------------------------
    // Bind to vao for Sphere
    glBindVertexArray(vao_sphere);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_elements);
    glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT,0);

    // Unbind to vao
    glBindVertexArray(0);
    //-----------------------------------------------
    // Unuse program
    glUseProgram(0);

    pop(modelData, &modelMatrix);

    // Elbow
    // Initialize above matrices to identity
  //  modelMatrix         = mat4::identity();
    viewMatrix          = mat4::identity();
    projectionMatrix    = mat4::identity();
    translationMatrix   = mat4::identity();
    scaleMatrix         = mat4::identity();
    rotationMatrix      = mat4::identity();

    glUseProgram(shaderProgramObject);

    //do necessary matrix multiplication
    translationMatrix = translate(1.0f, 0.0f, 0.0f);
    rotationMatrix = rotate((float)elbow, 0.0f, 0.0f, 1.0f);
    modelMatrix = modelMatrix  * translationMatrix * rotationMatrix *  scaleMatrix * translationMatrix ;
    projectionMatrix = projectionMatrix * perspectiveProjectionMatrix;

    push(modelData, modelMatrix);

    scaleMatrix = scale(2.0f, 0.5f, 1.0f);
    modelMatrix = modelMatrix * scaleMatrix;

    if(enableLighting == true)
    {
        glUniform1i(keyPressedUniform, 1);
        glUniform4fv(ldUniform, 1, lightDiffuse);
        glUniform4fv(kdUniform, 1, materialDiffuse);
        glUniform4fv(lightPositionUniform, 1, lightPosition);
    }
    else
    {
        glUniform1i(keyPressedUniform, 0);
    }

    //send necessary matrices to shader in respective uniforms
    glUniformMatrix4fv(mUniform, 1, GL_FALSE, modelMatrix);
    glUniformMatrix4fv(vUniform, 1, GL_FALSE, viewMatrix);
    glUniformMatrix4fv(pUniform, 1, GL_FALSE, projectionMatrix);
    glUniform3fv(colorUniform, 1, vec3(0.87f, 0.72f, 0.53f));

    //bind to vao_sphere
    glBindVertexArray(vao_sphere);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_elements);
    glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);
    
    //unbind vao
    glBindVertexArray(0);
    glUseProgram(0);
   
    pop(modelData, &modelMatrix);
    pop(modelData, &modelMatrix);
   
    glXSwapBuffers(gpDisplay, gWindow);
}




