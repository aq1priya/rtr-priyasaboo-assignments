#include<iostream>
#include<stdio.h>
#include<stdlib.h>
#include<memory.h>

#include<X11/Xlib.h>
#include<X11/Xutil.h>
#include<X11/XKBlib.h>
#include<X11/keysym.h>

#include<GL/glew.h>
#include<GL/gl.h>
#include<GL/glx.h>

#include"vmath.h"
#include"Sphere.h"

//namespaces
using namespace std;
using namespace vmath;

enum 
{
    AMC_ATTRIBUTE_POSITION = 0,
    AMC_ATTRIBUTE_COLOR,
    AMC_ATTRIBUTE_NORMAL,
    AMC_ATTRIBUTE_TEXCOORD0
};


//global variable declarations
bool bFullScreen = false;
Display *gpDisplay = NULL;
XVisualInfo *gpXVisualInfo = NULL;
Colormap gColormap;
Window gWindow;
int giWindowWidth = 800;
int giWindowHeight = 600;

typedef GLXContext(*glxCreateContextAttribsARBProc)(Display*, GLXFBConfig, GLXContext, Bool, const int*);
glxCreateContextAttribsARBProc glxCreateContextAttribsARB = NULL;
GLXFBConfig gGLXFBConfig;
GLXContext gGLXContext;

// global variables for sphere
float sphere_vertices[1146];
float sphere_normals[1146];
float sphere_textures[764];
unsigned short sphere_elements[2280];
int gNumVertices;
int gNumElements;

GLuint gShaderProgramObject_layered;
GLuint gShaderProgramObject_display;


GLuint vao_cube;
GLuint vbo_position_cube;
GLuint vbo_texcoord_cube;
GLuint vbo_normal_cube;

GLuint vao_sphere;
GLuint vbo_position_sphere;
GLuint vbo_element_sphere;
GLuint vbo_normal_sphere;

GLuint fbo;
GLuint color_attachment;
GLuint depth_attachment;

GLuint mUniform;
GLuint vUniform;
GLuint pUniform;
GLuint laUniform;
GLuint ldUniform;
GLuint lsUniform;
GLuint lightPositionUniform;
GLuint keyPressedUniform;
GLuint texArraySamplerUniform;

mat4 perspectiveProjectionMatrix;

GLfloat g_fCurrentWidth;
GLfloat g_fCurrentHeight;
GLfloat angle_cube = 0.0f;
GLfloat theta = 0.0f;

float lightDiffuse[4] = { 1.0f, 1.0f, 1.0f, 1.0f };
float lightAmbient[4] = { 0.0f, 0.0f, 0.0f, 0.0f };
float lightSpecular[4] = { 1.0f, 1.0f, 1.0f, 1.0f };
float lightPosition[4] = { 0.0f, 0.0f, 0.0f, 1.0f };

bool toggleLighting = false;
bool rotate_x = false;
bool rotate_y = false;
bool rotate_z = false;


//entry point function
int main(void)
{
    //function prototype
    void CreateWindow(void);
    void ToggleFullScreen(void);
    void uninitialize();
    void initialize(void);
    void resize(int,int);
    void display(void);
    void update(void);

    //variable declarations
    int winWidth = giWindowWidth;
    int winHeight = giWindowHeight;
    bool bDone = false;

    //code
    CreateWindow();
    
    //initialize
    initialize();

    //Message Loop
    XEvent event;
    KeySym keysym;

    while(bDone == false)
    {
        while(XPending(gpDisplay))
        {
            XNextEvent(gpDisplay,&event);
            switch(event.type)
            {
                case MapNotify:
                    break;
                case KeyPress:
                    keysym = XkbKeycodeToKeysym(gpDisplay, event.xkey.keycode, 0, 0);
                    switch(keysym)
                    {
                        case XK_Escape:
                            bDone = true;
                            break;

                        case XK_F:
                        case XK_f:
                            if(bFullScreen == false)
                            {
                                ToggleFullScreen();
                                bFullScreen = true;
                            }
                            else
                            {
                                ToggleFullScreen();
                                bFullScreen = false;
                            }
                            break;
                        
                        case XK_L:
                        case XK_l:
                            if(toggleLighting == false)
                            {
                                toggleLighting = true;
                            }
                            else
                            {
                                toggleLighting = false;
                            }
                            break;
                        
                        case XK_x:
                        case XK_X:
                            rotate_x = true;
                            rotate_y = false;
                            rotate_z = false;
                            theta = 0.0f;
                            break;
                        
                        case XK_y:
                        case XK_Y:
                            rotate_x = false;
                            rotate_y = true;
                            rotate_z = false;
                            theta = 0.0f;
                            break;
                        
                        case XK_z:
                        case XK_Z:
                            rotate_x = false;
                            rotate_y = false;
                            rotate_z = true;
                            theta = 0.0f;
                            break;
                        
                        default:
                            break;
                    }
                    break;
                case ButtonPress:
                   switch(event.xbutton.button)
                   {
                        case 1:                         //left button
                            break;
                        case 2:                         //middle button
                            break;
                        case 3:                         //right button
                            break;
                        default:
                            break;
                   }
                   break;
                case MotionNotify:
                   break;
                case ConfigureNotify:
                    winWidth = event.xconfigure.width;
                    winHeight =event.xconfigure.height;
                    resize(winWidth, winHeight);
                    break;
                case Expose:
                    break;
                case DestroyNotify:
                    break;
                case 33:                                //to handle close button and close menu
                    bDone = true;
                    break;
                default:
                    break;
            }
        }
        display();
    }
    uninitialize();
    return(0);
}

void CreateWindow(void)
{
    //function prototype
    void uninitialize(void);

    //variable declarations
    XSetWindowAttributes winAttribs;
    int defaultScreen;
    int defaultDepth;
    int styleMask;
    int iNumberOfFBConfigs = 0;					// number of FBConfigs
    GLXFBConfig *pGLXFBConfigs = NULL;			//array of FBConfigs
    GLXFBConfig bestGLXFBConfig;				//best FBConfig among all
    XVisualInfo *pTempXVisualInfo = NULL;		//temporary visual info

    // Initialize Frame Buffer Attributes
    static int frameBufferAttributes[] = {
        GLX_X_RENDERABLE, True,
        GLX_DRAWABLE_TYPE,GLX_WINDOW_BIT,
        GLX_RENDER_TYPE, GLX_RGBA_BIT,
        GLX_X_VISUAL_TYPE, GLX_TRUE_COLOR,
        GLX_DOUBLEBUFFER, True,
        GLX_RED_SIZE, 8,
        GLX_GREEN_SIZE , 8,
        GLX_BLUE_SIZE, 8,
        GLX_ALPHA_SIZE, 8,
        GLX_DEPTH_SIZE, 24,
        GLX_STENCIL_SIZE, 8,
        None};

    //code
    gpDisplay = XOpenDisplay(NULL);
    if(!gpDisplay)
    {
        printf("ERROR : Unable To Open X Display.\nExitting Now ...\n");
        uninitialize();
        exit(1);
    }

    defaultScreen = XDefaultScreen(gpDisplay);

	// Retrive All FBConfigs Driver Has
	pGLXFBConfigs = glXChooseFBConfig(gpDisplay, defaultScreen,	frameBufferAttributes, &iNumberOfFBConfigs);

	printf("Number Of FBConfigs Matching To FBConfig = %d\n", iNumberOfFBConfigs);
	// Now We Have Multiple FBConfigs, We Need To Find Best Out Of Them

	int bestFrameBufferConfig = -1;
	int bestNumberOfSamples   = -1;
	int worstFrameBufferConfig = -1;
	int worstNumberOfSamples  = 999;

	//loop to check all fb configs
	for(int i = 0; i < iNumberOfFBConfigs; i++)
	{
		// Getting visual from each obtained FBConfig
		pTempXVisualInfo = glXGetVisualFromFBConfig(gpDisplay, pGLXFBConfigs[i]);

		if(pTempXVisualInfo)
		{
			//get Number of samplebuffer from respective fbConfig
			int sampleBuffers, samples;
			glXGetFBConfigAttrib(gpDisplay,
				pGLXFBConfigs[i],
				GLX_SAMPLE_BUFFERS,
				&sampleBuffers);

			//get number of samples from respective FBConfig
			glXGetFBConfigAttrib(gpDisplay, pGLXFBConfigs[i], GLX_SAMPLES, &samples);

			//More the number of samples and sample buffer more the eligible config buffer is
			if(bestFrameBufferConfig < 0 || sampleBuffers && samples >bestNumberOfSamples)
			{
				bestFrameBufferConfig=i;
				bestNumberOfSamples =samples;
			}

			if(worstFrameBufferConfig < 0 || !sampleBuffers || samples < worstNumberOfSamples)
			{
				worstFrameBufferConfig = i;
				worstNumberOfSamples = samples;
			}
		}

		XFree(pTempXVisualInfo);
	}

	// Now assign the found best one
	bestGLXFBConfig = pGLXFBConfigs[bestFrameBufferConfig];

	//Now assign the local best to global best
	gGLXFBConfig = bestGLXFBConfig;

	//Free the obtaind GLXFBConfig array;
	XFree(pGLXFBConfigs);

	//Accordingly get best visual
	gpXVisualInfo = glXGetVisualFromFBConfig(gpDisplay,  bestGLXFBConfig);

    winAttribs.border_pixel = 0;
    winAttribs.background_pixmap = 0;
    winAttribs.colormap = XCreateColormap(gpDisplay, 
                                          RootWindow(gpDisplay, gpXVisualInfo->screen),
                                          gpXVisualInfo->visual,
                                          AllocNone);
    gColormap = winAttribs.colormap;

    winAttribs.background_pixel = BlackPixel(gpDisplay,defaultScreen);

    winAttribs.event_mask = ExposureMask | VisibilityChangeMask | ButtonPressMask |
                           KeyPressMask | PointerMotionMask | StructureNotifyMask;
   
    styleMask = CWBorderPixel | CWBackPixel | CWEventMask | CWColormap;

    gWindow = XCreateWindow(gpDisplay,
                            RootWindow(gpDisplay,gpXVisualInfo->screen),
                            0,
                            0,
                            giWindowWidth,
                            giWindowHeight,
                            0,                                              // border width
                            gpXVisualInfo->depth,                           // color depth
                            InputOutput,
                            gpXVisualInfo->visual,
                            styleMask,
                            &winAttribs);
    if(!gWindow)
    {
        printf("ERROR : Failed To Create Main Window.\nExitting Now ...\n");
        uninitialize();
        exit(1);
    }

    XStoreName(gpDisplay, gWindow, "Diffuse Light On Sphere");

    Atom windowManagerDelete = XInternAtom(gpDisplay, "WM_DELETE_WINDOW", True);
    XSetWMProtocols(gpDisplay, gWindow, &windowManagerDelete, 1);

    XMapWindow(gpDisplay, gWindow);
}

void ToggleFullScreen(void)
{
    //variable declarations
    Atom  wm_state;
    Atom fullscreen;
    XEvent xev = {0};

    //code
    wm_state = XInternAtom(gpDisplay, "_NET_WM_STATE", False);
    memset(&xev,0,sizeof(xev));

    xev.type = ClientMessage;
    xev.xclient.window = gWindow;
    xev.xclient.message_type = wm_state;
    xev.xclient.format = 32;
    xev.xclient.data.l[0] = bFullScreen ? 0 : 1;

    fullscreen = XInternAtom(gpDisplay, "_NET_WM_STATE_FULLSCREEN", False);
    xev.xclient.data.l[1] = fullscreen;
    
    XSendEvent(gpDisplay,
               RootWindow(gpDisplay, gpXVisualInfo->screen),
               False,
               StructureNotifyMask,
               &xev);
}

void uninitialize(void)
{
	if(vao_sphere)
    {
        glDeleteVertexArrays(1, &vao_sphere);
        vao_sphere = 0;
    }
    if(vbo_element_sphere)
    {
        glDeleteBuffers(1, &vbo_element_sphere);
        vbo_element_sphere = 0;
    }
    if(vbo_normal_sphere)
    {
        glDeleteBuffers(1, &vbo_normal_sphere);
        vbo_normal_sphere = 0;
    }
    if(vbo_position_sphere)
    {
        glDeleteBuffers(1, &vbo_position_sphere);
        vbo_position_sphere = 0;
    }
    if (gShaderProgramObject_layered)
    {
        GLsizei shaderCount;
        GLsizei shaderNumber;

        glUseProgram(gShaderProgramObject_layered);
        glGetProgramiv(gShaderProgramObject_layered, GL_ATTACHED_SHADERS, &shaderCount);
        GLuint *pShaders = (GLuint *)malloc(sizeof(GLuint) * shaderCount);

        if (pShaders)
        {
            glGetAttachedShaders(gShaderProgramObject_layered, shaderCount, &shaderCount, pShaders);
            for (shaderNumber = 0; shaderNumber < shaderCount; shaderNumber++)
            {
                //detach all the shader one by one
                glDetachShader(gShaderProgramObject_layered, pShaders[shaderNumber]);
                //delete detached shader
                glDeleteShader(pShaders[shaderNumber]);
                pShaders[shaderNumber] = 0;
            }
            free(pShaders);
        }
        glDeleteProgram(gShaderProgramObject_layered);
        gShaderProgramObject_layered = 0;
        glUseProgram(0);
    }

    if(gWindow)
    {
        XDestroyWindow(gpDisplay,gWindow);
    }
    if(gColormap)
    {
        XFreeColormap(gpDisplay, gColormap);
    }
    if(gpXVisualInfo)
    {
        free(gpXVisualInfo);
        gpXVisualInfo = NULL;
    }
    if(gpDisplay)
    {
        XCloseDisplay(gpDisplay);
        gpDisplay = NULL;
    }
}

//initialize() function
void initialize(void)
{
    //variable declaration
    GLenum result;
    GLint iShaderCompileStatus = 0;
    GLint iProgramLinkStatus   = 0;
    GLint iInfoLogLength       = 0;
    GLchar *szInfoLog          = NULL;
    
    GLuint vertexShaderObject_layered;
    GLuint geometryShaderObject_layered;
    GLuint fragmentShaderObject_layered;
    GLuint vertexShaderObject_display;
    GLuint fragmentShaderObject_display;

    glxCreateContextAttribsARB = (glxCreateContextAttribsARBProc)glXGetProcAddressARB((GLubyte *)"glXCreateContextAttribsARB");
    if(glxCreateContextAttribsARB == NULL)
    {
    	printf("Address not found for glXCreateContextAttribsARB\n");
    	uninitialize();
    }

     const int attribs[] = {
    	GLX_CONTEXT_MAJOR_VERSION_ARB, 4,
    	GLX_CONTEXT_MINOR_VERSION_ARB, 4,
    	GLX_CONTEXT_PROFILE_MASK_ARB, GLX_CONTEXT_CORE_PROFILE_BIT_ARB,
    	None
    };

    gGLXContext = glxCreateContextAttribsARB(gpDisplay, gGLXFBConfig, 0, True, attribs);

   

    if(!gGLXContext)
    {
    	//if obtained one is not highest one, specify the lowest one it will give you highest possible for it
    	const int glattribs[]={
    		GLX_CONTEXT_MAJOR_VERSION_ARB, 1,
    		GLX_CONTEXT_MINOR_VERSION_ARB, 0,
    		None
    	};

    	gGLXContext = glxCreateContextAttribsARB(gpDisplay, gGLXFBConfig, 0, True, glattribs);
    }

    //Check whether the obtained context is really a hardware rendering context or not
    if(!glXIsDirect(gpDisplay, gGLXContext))
    {
    	printf("The Obtained Context Is Not Hardware Rendering Context\n");
    }
    else
    {
    	printf("The Obtained Context Is Hardware Rendering Context\n");
    }

    //Make the Obtained Context as Current Rendering Context
    glXMakeCurrent(gpDisplay, gWindow, gGLXContext);

    // Initialize GLEW
    result = glewInit();
    if(result != GLEW_OK)
    {
        printf("glewInit() failed");
        uninitialize();
    }

      // define vertex shader object
    vertexShaderObject_layered = glCreateShader(GL_VERTEX_SHADER);

    //write vertex shader code
    const GLchar *VertexShaderSourceCode_l =
        "   #version 430 core                                                           " \
        "                                                                           \n  " \
        "   layout (location = 0) in vec4 vPosition;                                \n  " \
        "   layout (location = 2) in vec3 vNormal;                                  \n  " \
        "                                                                           \n  " \
        "   out vec3 v_out_normal;                                                      " \
        "   out vec3 v_out_tNorm;                                                       " \
        "   out vec3 v_out_viewerVector;                                                " \
        "   out vec3 v_out_lightDirection;                                              " \
        "                                                                           \n  " \
        "   uniform mat4 u_m_matrix;                                                    " \
        "   uniform mat4 u_v_matrix;                                                    " \
        "   uniform mat4 u_p_matrix;                                                    " \
        "   uniform vec4 u_light_position;                                              " \
        "   uniform int u_l_key_is_pressed;                                             " \
        "                                                                           \n  " \
        "   void main(void)                                                             " \
        "   {                                                                           " \
        "       vec4 eyeCoordinates;                                                    " \
        "       if(u_l_key_is_pressed == 1)                                             " \
        "       {                                                                       " \
        "           eyeCoordinates = u_v_matrix * u_m_matrix * vPosition;               " \
        "           v_out_tNorm        = mat3(u_v_matrix * u_m_matrix) * vNormal;       " \
        "           v_out_viewerVector   = vec3(-eyeCoordinates);                       " \
        "           v_out_lightDirection = vec3(u_light_position - eyeCoordinates);     " \
        "       }                                                                       " \
        "       gl_Position = vPosition;                                                " \
        "       v_out_normal = vNormal;                                                 " \
        "   }                                                                           ";

    //specify above source code to the vertex shader object
    glShaderSource(vertexShaderObject_layered, 1, (const GLchar **)&VertexShaderSourceCode_l, NULL);

    //compile the vertex shader
    glCompileShader(vertexShaderObject_layered);

    iShaderCompileStatus = 0;
    iInfoLogLength = 0;
    szInfoLog = NULL;

    glGetShaderiv(vertexShaderObject_layered, GL_COMPILE_STATUS, &iShaderCompileStatus);
    if (iShaderCompileStatus == GL_FALSE)
    {
        glGetShaderiv(vertexShaderObject_layered, GL_INFO_LOG_LENGTH, &iInfoLogLength);
        if (iInfoLogLength > 0)
        {
            szInfoLog = (GLchar *)malloc(iInfoLogLength);
            if (szInfoLog != NULL)
            {
                GLsizei written;
                glGetShaderInfoLog(vertexShaderObject_layered, iInfoLogLength, &written, szInfoLog);
                printf("LAYERED RENDERING VERTEX SHADER COMPILATION LOG : %s\n", szInfoLog);
                free(szInfoLog);
                uninitialize();
            }
        }
    }

    // define geometry shader object
    geometryShaderObject_layered = glCreateShader(GL_GEOMETRY_SHADER);

    //write geometry shader source code
    const GLchar *geometryShaderSourceCode_layered =
        "   #version 430 core                                                                           " \
        "                                                                                           \n  " \
        "   layout (invocations = 24, triangles) in;                                                    " \
        "   layout (triangle_strip, max_vertices = 3) out;                                              " \
        "                                                                                               " \
        "   in vec3 v_out_normal[];                                                                     " \
        "   in vec3 v_out_tNorm[];                                                                      " \
        "   in vec3 v_out_viewerVector[];                                                               " \
        "   in vec3 v_out_lightDirection[];                                                             " \
        "                                                                                               " \
        "   out vec4 g_out_color;                                                                       " \
        "   out vec4 g_out_materialAmbient;                                                             " \
        "   out vec4 g_out_materialDiffuse;                                                             " \
        "   out vec4 g_out_materialSpecular;                                                            " \
        "   out float g_out_materialShininess;                                                          " \
        "   out vec3 g_out_normal;                                                                      " \
        "   out vec3 g_out_tNorm;                                                                       " \
        "   out vec3 g_out_viewerVector;                                                                " \
        "   out vec3 g_out_lightDirection;                                                              " \
        "                                                                                               " \
        "   uniform mat4 u_m_matrix;                                                                    " \
        "   uniform mat4 u_v_matrix;                                                                    " \
        "   uniform mat4 u_p_matrix;                                                                    " \
        "                                                                                               " \
        "   void main(void)                                                                             " \
        "   {                                                                                           " \
        "       int i;                                                                                  " \
        "                                                                                           \n  " \
        "       const vec4 colors[24] = vec4[24](                                                       " \
        "           vec4(0.0, 0.0, 1.0, 1.0),                                                           " \
        "           vec4(0.0, 1.0, 0.0, 1.0),                                                           " \
        "           vec4(1.0, 0.0, 0.0, 1.0),                                                           " \
        "           vec4(1.0, 1.0, 0.0, 1.0),                                                           " \
        "           vec4(0.0, 1.0, 1.0, 1.0),                                                           " \
        "           vec4(1.0, 0.0, 1.0, 1.0),                                                           " \
        "           vec4(0.0, 0.0, 0.5, 1.0),                                                           " \
        "           vec4(0.0, 0.5, 0.0, 1.0),                                                           " \
        "           vec4(0.5, 0.0, 0.0, 1.0),                                                           " \
        "           vec4(0.0, 0.5, 0.5, 1.0),                                                           " \
        "           vec4(0.5, 0.0, 0.5, 1.0),                                                           " \
        "           vec4(0.5, 0.5, 0.0, 1.0),                                                           " \
        "           vec4(0.5, 0.5, 0.5, 1.0),                                                           " \
        "           vec4(1.0, 0.5, 0.5, 1.0),                                                           " \
        "           vec4(0.5, 1.0, 0.5, 1.0),                                                           " \
        "           vec4(0.5, 0.5, 1.0, 1.0),                                                           " \
        "           vec4(0.0, 0.0, 1.0, 1.0),                                                           " \
        "           vec4(0.0, 1.0, 0.0, 1.0),                                                           " \
        "           vec4(1.0, 0.0, 0.0, 1.0),                                                           " \
        "           vec4(1.0, 1.0, 0.0, 1.0),                                                           " \
        "           vec4(0.0, 1.0, 1.0, 1.0),                                                           " \
        "           vec4(1.0, 0.0, 1.0, 1.0),                                                           " \
        "           vec4(0.0, 0.0, 0.5, 1.0),                                                           " \
        "           vec4(0.0, 0.5, 0.0, 1.0)                                                            " \
        "       );                                                                                      " \
        "                                                                                           \n  " \
        "       const vec4 sphereMaterialAmbient[24] = vec4[24](                                        " \
        "           vec4(0.1, 0.18725, 0.1745, 1.0),                                                    " \
        "           vec4(0.19225, 0.19225, 0.19225, 1.0),                                               " \
        "           vec4(0.0, 0.0, 0.0, 1.0),                                                           " \
        "           vec4(0.05, 0.05, 0.0, 1.0),                                                         " \
        "           vec4(0.1745, 0.01175, 0.01175, 1.0),                                                " \
        "           vec4(0.24725, 0.1995, 0.0745, 1.0),                                                 " \
        "           vec4(0.0, 0.0, 0.0, 1.0),                                                           " \
        "           vec4(0.05, 0.05, 0.05, 1.0),                                                        " \
        "           vec4(0.25, 0.20725, 0.20725, 1.0),                                                  " \
        "           vec4(0.19125, 0.0735, 0.0225, 1.0),                                                 " \
        "           vec4(0.0, 0.0, 0.0, 1.0),                                                           " \
        "           vec4(0.05, 0.0, 0.0, 1.0),                                                          " \
        "           vec4(0.05375, 0.05, 0.06625, 1.0),                                                  " \
        "           vec4( 0.25, 0.25, 0.25, 1.0),                                                       " \
        "           vec4(0.0, 0.0, 0.0, 1.0),                                                           " \
        "           vec4(0.0, 0.05, 0.0, 1.0),                                                          " \
        "           vec4(0.135, 0.2225, 0.1575, 1.0),                                                   " \
        "           vec4(0.2125, 0.1275, 0.054, 1.0),                                                   " \
        "           vec4(0.0, 0.1, 0.06, 1.0),                                                          " \
        "           vec4(0.0, 0.05, 0.05, 1.0),                                                         " \
        "           vec4(0.0215, 0.1745, 0.0215, 1.0),                                                  " \
        "           vec4(0.329412, 0.223529, 0.027451, 1.0),                                            " \
        "           vec4(0.0, 0.0, 0.0, 1.0),                                                           " \
        "           vec4(0.02, 0.02, 0.02, 1.0)                                                         " \
        "       );                                                                                      " \
        "                                                                                           \n  " \
        "       const vec4 sphereMaterialDiffuse[24] = vec4[24](                                        " \
        "           vec4(0.396, 0.074151, 0.69102, 1.0),                                                " \
        "           vec4(0.50754, 0.50754, 0.50754, 1.0),                                               " \
        "           vec4(0.5, 0.5, 0.0, 1.0),                                                           " \
        "           vec4(0.50, 0.50, 0.40, 1.0),                                                        " \
        "           vec4(0.61424, 0.04136, 0.04136, 1.0),                                               " \
        "           vec4(0.75164, 0.555802, 0.366065, 1.0),                                             " \
        "           vec4(0.55, 0.55, 0.55, 1.0),                                                        " \
        "           vec4(0.5, 0.5, 0.5, 1.0),                                                           " \
        "           vec4(1.0, 0.829, 0.829, 1.0),                                                       " \
        "           vec4(0.7038, 0.27048, 0.0828, 1.0),                                                 " \
        "           vec4(0.5, 0.0, 0.0, 1.0),                                                           " \
        "           vec4(0.5, 0.4, 0.4, 1.0),                                                           " \
        "           vec4(0.18275, 0.17, 0.22525, 1.0),                                                  " \
        "           vec4(0.4, 0.4, 0.4, 1.0),                                                           " \
        "           vec4(0.1, 0.35, 0.1, 1.0),                                                          " \
        "           vec4(0.4, 0.5, 0.4, 1.0),                                                           " \
        "           vec4(0.54, 0.89, 0.63, 1.0),                                                        " \
        "           vec4(0.714, 0.4284, 0.18144, 1.0),                                                  " \
        "           vec4(0.0, 0.50980392, 0.50980392, 1.0),                                             " \
        "           vec4(0.40, 0.50, 0.50, 1.0),                                                        " \
        "           vec4(0.07568, 0.61424, 0.07568, 1.0),                                               " \
        "           vec4(0.780392, 0.568627, 0.113725, 1.0),                                            " \
        "           vec4(0.01, 0.01, 0.01, 1.0),                                                        " \
        "           vec4(0.01, 0.01, 0.01, 1.0)                                                         " \
        "       );                                                                                      " \
        "                                                                                           \n  " \
        "       const vec4 sphereMaterialSpecular[24] = vec4[24](                                       " \
        "           vec4(0.633, 0.727811, 0.633, 1.0),                                                  " \
        "           vec4(0.316228, 0.316228, 0.316228, 1.0),                                            " \
        "           vec4(0.332741, 0.328634, 0.346435, 1.0),                                            " \
        "           vec4(0.296648, 0.296648, 0.296648, 1.0),                                            " \
        "           vec4(0.727811, 0.626959, 0.626959, 1.0),                                            " \
        "           vec4(0.297254, 0.30829, 0.306678, 1.0),                                             " \
        "           vec4(0.992157, 0.941176, 0.807843, 1.0),                                            " \
        "           vec4(0.393548, 0.271906, 0.166721, 1.0),                                            " \
        "           vec4(0.774597, 0.774597, 0.774597, 1.0),                                            " \
        "           vec4(0.256777, 0.137622, 0.086014, 1.0),                                            " \
        "           vec4(0.628281, 0.555802, 0.366065, 1.0),                                            " \
        "           vec4(0.508273, 0.508273, 0.508273, 1.0),                                            " \
        "           vec4(0.50, 0.50, 0.50, 1.0),                                                        " \
        "           vec4(0.50196078, 0.50196078, 0.50196078, 1.0),                                      " \
        "           vec4(0.45, 0.55, 0.45, 1.0),                                                        " \
        "           vec4(0.7, 0.6, 0.6, 1.0),                                                           " \
        "           vec4(0.70, 0.70, 0.70, 1.0),                                                        " \
        "           vec4(0.60, 0.60, 0.50, 1.0),                                                        " \
        "           vec4(0.04, 0.7, 0.7, 1.0),                                                          " \
        "           vec4(0.04, 0.7, 0.04, 1.0),                                                         " \
        "           vec4(0.7, 0.04, 0.04, 1.0),                                                         " \
        "           vec4(0.70, 0.70, 0.70, 1.0),                                                        " \
        "           vec4(0.70, 0.70, 0.04, 1.0),                                                        " \
        "           vec4(0.4, 0.40, 0.40, 1.0)                                                          " \
        "       );                                                                                      " \
        "                                                                                           \n  " \
        "       const float sphereMaterialShininess[24] = {                                             " \
        "           0.6 * 128.0,                                                                        " \
        "           0.1 * 128.0,                                                                        " \
        "           0.3 * 128.0,                                                                        " \
        "           0.88 * 128.0,                                                                       " \
        "           0.6 * 128.0,                                                                        " \
        "           0.1 * 128.0,                                                                        " \
        "           0.21794872 * 128.0,                                                                 " \
        "           0.2 * 128.0,                                                                        " \
        "           0.6 * 128.0,                                                                        " \
        "           0.1 * 128.0,                                                                        " \
        "           0.4 * 128.0,                                                                        " \
        "           0.4 * 128.0,                                                                        " \
        "           0.25 * 128.0,                                                                       " \
        "           0.25 * 128.0,                                                                       " \
        "           0.25 * 128.0,                                                                       " \
        "           0.25 * 128.0,                                                                       " \
        "           0.25 * 128.0,                                                                       " \
        "           0.25 * 128.0,                                                                       " \
        "           0.078125 * 128.0,                                                                   " \
        "           0.078125 * 128.0,                                                                   " \
        "           0.078125 * 128.0,                                                                   " \
        "           0.078125 * 128.0,                                                                   " \
        "           0.078125 * 128.0,                                                                   " \
        "           0.078125 * 128.0                                                                    " \
        "       };                                                                                      " \
        "                                                                                           \n  " \
        "       for (i = 0; i < gl_in.length(); i++)                                                    " \
        "       {                                                                                       " \
        "           g_out_color = colors[gl_InvocationID];                                              " \
        "                                                                                           \n  " \
        "           g_out_materialAmbient = sphereMaterialAmbient[gl_InvocationID];                     " \
        "           g_out_materialDiffuse = sphereMaterialDiffuse[gl_InvocationID];                     " \
        "           g_out_materialSpecular = sphereMaterialSpecular[gl_InvocationID];                   " \
        "           g_out_materialShininess = sphereMaterialShininess[gl_InvocationID];                 " \
        "                                                                                           \n  " \
        "           g_out_normal = mat3(u_p_matrix * u_v_matrix * u_m_matrix) * v_out_normal[i];        " \
        "                                                                                           \n  " \
        "           g_out_tNorm             = v_out_tNorm[i];                                           " \
        "           g_out_viewerVector      = v_out_viewerVector[i];                                    " \
        "           g_out_lightDirection    = v_out_lightDirection[i];                                  " \
        "                                                                                           \n  " \
        "           gl_Position = u_p_matrix * u_v_matrix * u_m_matrix * gl_in[i].gl_Position;          " \
        "           gl_Layer    = gl_InvocationID;                                                      " \
        "           EmitVertex();                                                                       " \
        "       }                                                                                       " \
        "       EndPrimitive();                                                                         " \
        "   }                                                                                           ";

    //specify above source code to the vertex shader object
    glShaderSource(geometryShaderObject_layered, 1, (const GLchar **)&geometryShaderSourceCode_layered, NULL);

    //compile the vertex shader
    glCompileShader(geometryShaderObject_layered);

    iShaderCompileStatus = 0;
    iInfoLogLength = 0;
    szInfoLog = NULL;

    glGetShaderiv(geometryShaderObject_layered, GL_COMPILE_STATUS, &iShaderCompileStatus);
    if (iShaderCompileStatus == GL_FALSE)
    {
        glGetShaderiv(geometryShaderObject_layered, GL_INFO_LOG_LENGTH, &iInfoLogLength);
        if (iInfoLogLength > 0)
        {
            szInfoLog = (GLchar *)malloc(iInfoLogLength);
            if (szInfoLog != NULL)
            {
                GLsizei written;
                glGetShaderInfoLog(geometryShaderObject_layered, iInfoLogLength, &written, szInfoLog);
                printf(" LAYERED RENDERING GEOMETRY SHADER COMPILATION LOG : %s\n", szInfoLog);
                free(szInfoLog);
                uninitialize();
            }
        }
    }
    
    //define fragment shader object
    fragmentShaderObject_layered = glCreateShader(GL_FRAGMENT_SHADER);
    
    //write fragment shader code
    const GLchar *fragmentShaderSourceCode_layered =
        "   #version 430 core                                                                                   " \
        "                                                                                                   \n  " \
        "   out vec4 FragColor;                                                                                 " \
        "                                                                                                   \n  " \
        "   in vec4 g_out_color;                                                                                " \
        "   in vec3 g_out_normal;                                                                               " \
        "                                                                                                   \n  " \
        "   in vec3 g_out_tNorm;                                                                                " \
        "   in vec3 g_out_viewerVector;                                                                         " \
        "   in vec3 g_out_lightDirection;                                                                       " \
        "   in vec4 g_out_materialAmbient;                                                                      " \
        "   in vec4 g_out_materialDiffuse;                                                                      " \
        "   in vec4 g_out_materialSpecular;                                                                     " \
        "   in float g_out_materialShininess;                                                                   " \
        "                                                                                                   \n  " \
        "   uniform vec3 u_la;                                                                              \n  " \
        "   uniform vec3 u_ld;                                                                              \n  " \
        "   uniform vec3 u_ls;                                                                              \n  " \
        "   uniform int u_l_key_is_pressed;                                                                 \n  " \
        "   uniform float u_material_shininess;                                                             \n  " \
        "                                                                                                   \n  " \
        "   void main(void)                                                                                     " \
        "   {                                                                                                   " \
        "       vec3 phong_ads_light;                                                                       \n  " \
        "                                                                                                   \n  " \
        "       if(u_l_key_is_pressed == 1)                                                                 \n  " \
        "       {                                                                                           \n  " \
        "           vec3 normalized_tNorm = normalize(g_out_tNorm);                                         \n  " \
        "           vec3 normalized_vv    = normalize(g_out_viewerVector);                                  \n  " \
        "           vec3 normalized_lightDirection = normalize(g_out_lightDirection);                       \n  " \
        "                                                                                                   \n  " \
        "           vec3 lightReflectionVector = reflect(-normalized_lightDirection, normalized_tNorm);     \n  " \
        "                                                                                                   \n  " \
        "           float tnDotld = max(dot(normalized_tNorm, normalized_lightDirection), 0.0);             \n  " \
        "           float rvDotvv = max(dot(lightReflectionVector, normalized_vv), 0.0);                    \n  " \
        "                                                                                                   \n  " \
        "           vec3 Ambient = u_la * g_out_materialAmbient.rgb;                                            \n  " \
        "           vec3 Diffuse = u_ld * g_out_materialDiffuse.rgb * tnDotld;                                  \n  " \
        "           vec3 Specular = u_ls * g_out_materialSpecular.rgb * pow(rvDotvv, g_out_materialShininess);  \n  " \
        "                                                                                                   \n  " \
        "           phong_ads_light = Ambient + Diffuse + Specular;                                         \n  " \
        "           FragColor = vec4(phong_ads_light.r,phong_ads_light.g,phong_ads_light.b, 1.0);           \n  " \
        "       }                                                                                           \n  " \
        "       else                                                                                        \n  " \
        "       {                                                                                           \n  " \
        "           phong_ads_light = vec3(1.0, 1.0, 1.0);                                                  \n  " \
        "           FragColor = vec4(phong_ads_light.r,phong_ads_light.g,phong_ads_light.b, 1.0);                                                   \n  " \
        "       }                                                                                           \n  " \
        "   }                                                                                                   " ;
        //"       FragColor = vec4(abs(out_gnormal.z)) * out_gcolor;                                          " \
        //"         FragColor = vec4(Diffuse, 1.0);         \n  " \
    
    //specify above source code to the fragment shader object
    glShaderSource(fragmentShaderObject_layered, 1, (const GLchar **)&fragmentShaderSourceCode_layered, NULL);

    //compile the vertex shader
    glCompileShader(fragmentShaderObject_layered);

    iShaderCompileStatus = 0;
    iInfoLogLength = 0;
    szInfoLog = NULL;

    glGetShaderiv(fragmentShaderObject_layered, GL_COMPILE_STATUS, &iShaderCompileStatus);
    if (iShaderCompileStatus == GL_FALSE)
    {
        glGetShaderiv(fragmentShaderObject_layered, GL_INFO_LOG_LENGTH, &iInfoLogLength);
        if (iInfoLogLength > 0)
        {
            szInfoLog = (GLchar *)malloc(iInfoLogLength);
            if (szInfoLog != NULL)
            {
                GLsizei written;
                glGetShaderInfoLog(fragmentShaderObject_layered, iInfoLogLength, &written, szInfoLog);
                printf(" LAYERED RENDERING FRAGMENT SHADER COMPILATION LOG : %s\n", szInfoLog);
                free(szInfoLog);
                uninitialize();
            }
        }
    }
 
    //create shader program object
    gShaderProgramObject_layered = glCreateProgram();

    //attach vertex shader to the shader program 
    glAttachShader(gShaderProgramObject_layered, vertexShaderObject_layered);
    // attach geometry shader to the shader program
    glAttachShader(gShaderProgramObject_layered, geometryShaderObject_layered);
    //attach fragment shader 
    glAttachShader(gShaderProgramObject_layered, fragmentShaderObject_layered);

    //prelinking binding to vertex attribute
    glBindAttribLocation(gShaderProgramObject_layered, AMC_ATTRIBUTE_POSITION, "vPosition");
    glBindAttribLocation(gShaderProgramObject_layered, AMC_ATTRIBUTE_TEXCOORD0, "vTexcoord");
    glBindAttribLocation(gShaderProgramObject_layered, AMC_ATTRIBUTE_NORMAL, "vNormal");

    // link shader program object
    glLinkProgram(gShaderProgramObject_layered);

    //error checking
    iProgramLinkStatus = 0;
    iInfoLogLength = 0;
    szInfoLog = NULL;

    glGetProgramiv(gShaderProgramObject_layered, GL_LINK_STATUS, &iProgramLinkStatus);
    if (iProgramLinkStatus == GL_FALSE)
    {
        glGetProgramiv(gShaderProgramObject_layered, GL_INFO_LOG_LENGTH, &iInfoLogLength);
        if (iInfoLogLength > 0)
        {
            szInfoLog = (GLchar *)malloc(iInfoLogLength);
            if (szInfoLog != NULL)
            {
                GLint written;
                glGetProgramInfoLog(gShaderProgramObject_layered, iInfoLogLength, &written, szInfoLog);
                printf( "LAYERED RENDERING SHADER PROGRAM LINK LOG : %s\n", szInfoLog);
                free(szInfoLog);
                uninitialize();
            }
        }
    }

    // post linking retrieving uniform location
    mUniform = glGetUniformLocation(gShaderProgramObject_layered, "u_m_matrix");
    vUniform = glGetUniformLocation(gShaderProgramObject_layered, "u_v_matrix");
    pUniform = glGetUniformLocation(gShaderProgramObject_layered, "u_p_matrix");
    laUniform = glGetUniformLocation(gShaderProgramObject_layered, "u_la");
    ldUniform = glGetUniformLocation(gShaderProgramObject_layered, "u_ld");
    lsUniform = glGetUniformLocation(gShaderProgramObject_layered, "u_ls");
    lightPositionUniform = glGetUniformLocation(gShaderProgramObject_layered, "u_light_position");
    keyPressedUniform = glGetUniformLocation(gShaderProgramObject_layered, "u_l_key_is_pressed");
    
    // vertex shader for displaying array texture
    vertexShaderObject_display = glCreateShader(GL_VERTEX_SHADER);

    const GLchar* vertexShaderSourceCode_display =
        "   #version 430 core                                                           " \
        "                                                                           \n  " \
        "   out vec3 out_texcoord;                                                  \n  " \
        "                                                                           \n  " \
        "   void main(void)                                                         \n  " \
        "   {                                                                       \n  " \
        "       int vid = gl_VertexID;                                              \n  " \
        "       int iid = gl_InstanceID;                                            \n  " \
        "       float inst_x = float(iid % 4) / 2.0;                                \n  " \
        "       float inst_y = float(iid >> 2) / 3.2;                               \n  " \
        "                                                                           \n  " \
        "       const vec4 vertices[] = vec4[](vec4(-0.5, -0.5, 0.0, 1.0),          \n  " \
        "                                      vec4( 0.5, -0.5, 0.0, 1.0),          \n  " \
        "                                      vec4( 0.5,  0.5, 0.0, 1.0),          \n  " \
        "                                      vec4(-0.5,  0.5, 0.0, 1.0));         \n  " \
        "                                                                           \n  " \
        "       vec4 offs = vec4(inst_x - 0.75, inst_y - 0.75, 0.0, 0.0);           \n  " \
        "                                                                           \n  " \
        "       gl_Position = vertices[vid] * vec4(0.25, 0.25, 1.0, 1.0) + offs;    \n  " \
        "       out_texcoord = vec3(vertices[vid].xy + vec2(0.5), float(iid));      \n  " \
        "   }                                                                       \n  ";

    //specify above source code to the vertex shader object
    glShaderSource(vertexShaderObject_display, 1, (const GLchar **)&vertexShaderSourceCode_display, NULL);

    //compile the vertex shader
    glCompileShader(vertexShaderObject_display);

    iShaderCompileStatus = 0;
    iInfoLogLength = 0;
    szInfoLog = NULL;

    glGetShaderiv(vertexShaderObject_display, GL_COMPILE_STATUS, &iShaderCompileStatus);
    if (iShaderCompileStatus == GL_FALSE)
    {
        glGetShaderiv(vertexShaderObject_display, GL_INFO_LOG_LENGTH, &iInfoLogLength);
        if (iInfoLogLength > 0)
        {
            szInfoLog = (GLchar *)malloc(iInfoLogLength);
            if (szInfoLog != NULL)
            {
                GLsizei written;
                glGetShaderInfoLog(vertexShaderObject_display, iInfoLogLength, &written, szInfoLog);
                printf("VERTEX SHADER COMPILATION LOG - DISPLAY : %s\n", szInfoLog);
                free(szInfoLog);
                uninitialize();
            }
        }
    }

    // fragment shader for displaying layered rendering
    fragmentShaderObject_display = glCreateShader(GL_FRAGMENT_SHADER);

    const GLchar *fragmentShaderSourceCode_display =
        "   #version 430 core                                                   " \
        "                                                                   \n  " \
        "   in vec3 out_texcoord;                                           \n  " \
        "                                                                   \n  " \
        "   out vec4 FragColor;                                             \n  " \
        "                                                                   \n  " \
        "   uniform sampler2DArray u_tex_array_sampler;                     \n  " \
        "                                                                   \n  " \
        "   void main(void)                                                 \n  " \
        "   {                                                               \n  " \
        "       FragColor = texture(u_tex_array_sampler, out_texcoord);     \n  " \
        "   }                                                               \n  ";
    
    //specify above source code to the fragment shader object
    glShaderSource(fragmentShaderObject_display, 1, (const GLchar **)&fragmentShaderSourceCode_display, NULL);

    //compile the vertex shader
    glCompileShader(fragmentShaderObject_display);

    iShaderCompileStatus = 0;
    iInfoLogLength = 0;
    szInfoLog = NULL;

    glGetShaderiv(fragmentShaderObject_display, GL_COMPILE_STATUS, &iShaderCompileStatus);
    if (iShaderCompileStatus == GL_FALSE)
    {
        glGetShaderiv(fragmentShaderObject_display, GL_INFO_LOG_LENGTH, &iInfoLogLength);
        if (iInfoLogLength > 0)
        {
            szInfoLog = (GLchar *)malloc(iInfoLogLength);
            if (szInfoLog != NULL)
            {
                GLsizei written;
                glGetShaderInfoLog(fragmentShaderObject_display, iInfoLogLength, &written, szInfoLog);
                printf(" FRAGMENT SHADER COMPILATION LOG - DISPlAY: %s\n", szInfoLog);
                free(szInfoLog);
                uninitialize();
            }
        }
    }

    //create shader program object
    gShaderProgramObject_display = glCreateProgram();

    //attach vertex shader to the shader program 
    glAttachShader(gShaderProgramObject_display, vertexShaderObject_display);
    //attach fragment shader 
    glAttachShader(gShaderProgramObject_display, fragmentShaderObject_display);

    // link shader program object
    glLinkProgram(gShaderProgramObject_display);

    //error checking
    iProgramLinkStatus = 0;
    iInfoLogLength = 0;
    szInfoLog = NULL;

    glGetProgramiv(gShaderProgramObject_display, GL_LINK_STATUS, &iProgramLinkStatus);
    if (iProgramLinkStatus == GL_FALSE)
    {
        glGetProgramiv(gShaderProgramObject_display, GL_INFO_LOG_LENGTH, &iInfoLogLength);
        if (iInfoLogLength > 0)
        {
            szInfoLog = (GLchar *)malloc(iInfoLogLength);
            if (szInfoLog != NULL)
            {
                GLint written;
                glGetProgramInfoLog(gShaderProgramObject_display, iInfoLogLength, &written, szInfoLog);
                printf("LAYERED RENDERING SHADER PROGRAM LINK LOG : %s\n", szInfoLog);
                free(szInfoLog);
                uninitialize();
            }
        }
    }

    // post linking retrieving uniform location
    texArraySamplerUniform = glGetUniformLocation(gShaderProgramObject_display, "u_tex_array_sampler");

    getSphereVertexData(sphere_vertices, sphere_normals, sphere_textures, sphere_elements);
    gNumVertices = getNumberOfSphereVertices();
    gNumElements = getNumberOfSphereElements();

    // ***** SPHERE *****
    glGenVertexArrays(1, &vao_sphere);
    glBindVertexArray(vao_sphere);

    // Buffer for position
    glGenBuffers(1, &vbo_position_sphere);
    glBindBuffer(GL_ARRAY_BUFFER, vbo_position_sphere);
    glBufferData(GL_ARRAY_BUFFER, sizeof(sphere_vertices), sphere_vertices, GL_STATIC_DRAW);
    glVertexAttribPointer(AMC_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
    glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    
    // Buffer for normal
    glGenBuffers(1, &vbo_normal_sphere);
    glBindBuffer(GL_ARRAY_BUFFER, vbo_normal_sphere);
    glBufferData(GL_ARRAY_BUFFER, sizeof(sphere_normals), sphere_normals, GL_STATIC_DRAW);
    glVertexAttribPointer(AMC_ATTRIBUTE_NORMAL, 3, GL_FLOAT, GL_FALSE, 0, NULL);
    glEnableVertexAttribArray(AMC_ATTRIBUTE_NORMAL);
    glBindBuffer(GL_ARRAY_BUFFER, 0);

    // Buffer for elements
    glGenBuffers(1, &vbo_element_sphere);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_element_sphere);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(sphere_elements), sphere_elements, GL_STATIC_DRAW);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

    // Unbind with vao
    glBindVertexArray(0);

    // setting layered frame buffer

    //create a texture for our color attachment bind it, and allocate storage for it
    // This will be 512 x 512 with 16 layers
    glGenTextures(1, &color_attachment);
    glBindTexture(GL_TEXTURE_2D_ARRAY, color_attachment);
    //glTexImage3D(GL_TEXTURE_2D_ARRAY, 0, GL_RGBA8, 512, 512, 16, 0, GL_RGBA8, GL_FLOAT, NULL);
    glTexStorage3D(GL_TEXTURE_2D_ARRAY, 1, GL_RGBA8, 512, 512, 24);

    //create a texture for our depth attachment bind it, and allocate storage for it
    // This will be 512 x 512 with 16 layers
    glGenTextures(1, &depth_attachment);
    glBindTexture(GL_TEXTURE_2D_ARRAY, depth_attachment);
    //glTexImage3D(GL_TEXTURE_2D_ARRAY, 0, GL_RGBA, 512, 512, 16, 0, GL_RGBA, GL_FLOAT, NULL);
    glTexStorage3D(GL_TEXTURE_2D_ARRAY, 1, GL_DEPTH_COMPONENT32, 512, 512, 24);

    // Now create a framebuffer object and bind our textures to it
    glGenFramebuffers(1, &fbo);
    glBindFramebuffer(GL_FRAMEBUFFER, fbo);
    glFramebufferTexture(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, color_attachment, 0);
    glFramebufferTexture(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, depth_attachment, 0);

    // finally tell opengl that we plan to render to the color attachment
    glDrawBuffer(GL_COLOR_ATTACHMENT0);
    glBindFramebuffer(GL_FRAMEBUFFER, 0);

    //glClearColor(1.0f, 1.0f, 1.0f, 1.0f); //r,g,b,a   //clear the screen by opengl color
    glClearColor(0.25, 0.25, 0.25, 1.0f);   //r,g,b,a   //clear the screen by opengl color

    glClearDepth(1.0f);
    glEnable(GL_TEXTURE_2D_ARRAY);

    perspectiveProjectionMatrix = mat4::identity();
}

//resize() function
void resize(int width, int height)
{
    if(height == 0)
        height =1;
    
    g_fCurrentWidth = width;
    g_fCurrentHeight = height;

    glViewport(0,0,(GLsizei)width, (GLsizei)height);

    perspectiveProjectionMatrix = perspective(45.0f, (GLfloat)width/(GLfloat)height, 0.1f, 100.0f);
}

// update function
void update(void)
{
    // Code
    if (theta >= 360.0f)
        theta = 0.0f;
    theta += 0.02f;
}

//display function
void display(void)
{
	// declaration of function
	void update(void);
	
    // Declaration of matrices
    mat4 modelMatrix;
    mat4 viewMatrix;
    mat4 projectionMatrix;
    mat4 translationMatrix;
    mat4 rotationMatrix;
    mat4 scaleMatrix;

    // Initialize above matrices to identity
    modelMatrix 		= mat4::identity();
    viewMatrix 			= mat4::identity();
    projectionMatrix 	= mat4::identity();
    translationMatrix 	= mat4::identity();
    scaleMatrix 		= mat4::identity();
    rotationMatrix 		= mat4::identity();

    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    // Necessary transformations
    translationMatrix 	= translate(0.0f, 0.0f, -1.5f);
    modelMatrix			= translationMatrix * scaleMatrix * rotationMatrix;
    projectionMatrix 	= perspectiveProjectionMatrix;

    if (rotate_x == true)       // x will be unchanged
    {
        GLfloat y = 10 * cosf(theta);
        GLfloat z = 10 * sinf(theta);
        lightPosition[0] = 0.0f;
        lightPosition[1] = y;
        lightPosition[2] = z;
        lightPosition[3] = 1.0f;
    }
    else if (rotate_y == true)  // y will be unchanged
    {
        GLfloat x = 10 * cosf(theta);
        GLfloat z = 10 * sinf(theta);
        lightPosition[0] = x;
        lightPosition[1] = 0.0f;
        lightPosition[2] = z;
        lightPosition[3] = 1.0f;
    }
    else if (rotate_z == true)  // z will be unchanged
    {
        GLfloat x = 5 * cosf(theta);
        GLfloat y = 5 * sinf(theta);
        lightPosition[0] = x;
        lightPosition[1] = y;
        lightPosition[2] = 0.0f;
        lightPosition[3] = 1.0f;
    }

    glBindFramebuffer(GL_FRAMEBUFFER, fbo);
    glClearBufferfv(GL_COLOR, 0, vec4(0.25f, 0.25f, 0.25f, 1.0f));
    glClearBufferfv(GL_DEPTH, 0, vec4(1.0f, 1.0f, 1.0f, 0.0f));

    glViewport(0,0,512,512);
    glUseProgram(gShaderProgramObject_layered);

    glEnable(GL_DEPTH_TEST);
    glDepthFunc(GL_LEQUAL);

    glUniformMatrix4fv(mUniform, 1, GL_FALSE, modelMatrix);
    glUniformMatrix4fv(vUniform, 1, GL_FALSE, viewMatrix);
    glUniformMatrix4fv(pUniform, 1, GL_FALSE, projectionMatrix);
    
    glUniform3fv(laUniform, 1, lightDiffuse);
    glUniform3fv(ldUniform, 1, lightDiffuse);
    glUniform3fv(lsUniform, 1, lightSpecular);
    
    glUniform4fv(lightPositionUniform, 1, lightPosition);
    if (toggleLighting == false)
        glUniform1i(keyPressedUniform, 0);          // lighting disabled
    else
        glUniform1i(keyPressedUniform, 1);          // lighting enabled

    glBindTexture(GL_TEXTURE_2D_ARRAY, color_attachment);
    
    // draw sphere in framebuffer
    glBindVertexArray(vao_sphere);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_element_sphere);
    glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);

    glBindTexture(GL_TEXTURE_2D_ARRAY, 0);
    glUseProgram(0);
    glBindFramebuffer(GL_FRAMEBUFFER, 0);

    glViewport(0, 0, g_fCurrentWidth, g_fCurrentHeight);
    glUseProgram(gShaderProgramObject_display);
    
    glBindTexture(GL_TEXTURE_2D_ARRAY, fbo);
    glUniform1i(texArraySamplerUniform, 0);
    //glClearBufferfv(GL_COLOR, 0, vec4(1.0f, 0.75f, 0.75f, 1.0f));
    //glBindVertexArray(vao_cube);
    glDisable(GL_DEPTH_TEST);
    glDrawArraysInstanced(GL_TRIANGLE_FAN, 0, 4, 24);
    //glBindVertexArray(0);
    glBindTexture(GL_TEXTURE_2D_ARRAY, 0);

    // Unuse program
    glUseProgram(0);
    glXSwapBuffers(gpDisplay, gWindow);

    update();
}




