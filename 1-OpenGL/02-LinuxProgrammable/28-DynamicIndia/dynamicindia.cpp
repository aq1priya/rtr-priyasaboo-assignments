#include<iostream>
#include<stdio.h>
#include<stdlib.h>
#include<memory.h>

#include<X11/Xlib.h>
#include<X11/Xutil.h>
#include<X11/XKBlib.h>
#include<X11/keysym.h>

#include<GL/glew.h>
#include<GL/gl.h>
#include<GL/glx.h>

#include"vmath.h"

//namespaces
using namespace std;
using namespace vmath;

//global variable declarations
bool bFullScreen = false;
Display *gpDisplay = NULL;
XVisualInfo *gpXVisualInfo = NULL;
Colormap gColormap;
Window gWindow;
int giWindowWidth = 800;
int giWindowHeight = 600;

typedef GLXContext(*glxCreateContextAttribsARBProc)(Display*, GLXFBConfig, GLXContext, Bool, const int*);
glxCreateContextAttribsARBProc glxCreateContextAttribsARB = NULL;
GLXFBConfig gGLXFBConfig;
GLXContext gGLXContext;


GLuint gShaderProgramObject;
GLuint shaderProgramObject_part2;

GLuint mvpUniform;
GLuint alphaUniform;

GLuint mvpUniform_part2;
GLuint alphaUniform_part2;
GLuint colorUniform_part2;

GLuint vao_i1;			// I
GLuint vbo_color_i1;
GLuint vbo_position_i1;
GLuint vao_n;			// N
GLuint vbo_color_n;
GLuint vbo_position_n;
GLuint vao_d;			// D
GLuint vbo_color_d;
GLuint vbo_position_d;
GLuint vao_i2;			// I
GLuint vbo_color_i2;
GLuint vbo_position_i2;
GLuint vao_a;			// A
GLuint vbo_color_a;
GLuint vbo_position_a;
GLuint vao_flag;		// Flag
GLuint vbo_position_flag;
GLuint vbo_color_flag;
GLuint vao_plane;		//Plane
GLuint vbo_position_plane;
GLuint vbo_color_plane;
GLuint vao_path1;
GLuint vbo_position_path1;
GLuint vao_path2;
GLuint vbo_position_path2;
GLuint vao_path3;
GLuint vbo_position_path3;

GLfloat trans_i1	= 0.0f;
GLfloat trans_n		= 0.0f;
GLfloat trans_d		= 0.0f;
GLfloat trans_i2	= 0.0f;
GLfloat trans_a		= 0.0f;
GLfloat path1[1500];
GLfloat path2[1500];
GLfloat path3[1500];

bool translateI1	= true;
bool translateN		= false;
bool translateD		= false;
bool translateI2	= false;
bool translateA		= false;
bool translatePlane = false;
bool showFlag		= false;

float alphaValue = 0.0f;
float alphaValue_part2 = 1.0f;
float delta = 0.6f;
float angle1 = -60.0f;
float angle2 = -300.0f;
int ip;
int pointCount = 0;
long int anim = 0;

mat4 perspectiveProjectionMatrix;

enum 
{
    AMC_ATTRIBUTE_POSITION = 0,
    AMC_ATTRIBUTE_COLOR,
    AMC_ATTRIBUTE_NORMAL,
    AMC_ATTRIBUTE_TEXCOORD0
};

//entry point function
int main(void)
{
    //function prototype
    void CreateWindow(void);
    void ToggleFullScreen(void);
    void uninitialize();
    void initialize(void);
    void resize(int,int);
    void display(void);

    //variable declarations
    int winWidth = giWindowWidth;
    int winHeight = giWindowHeight;
    bool bDone = false;

    //code
    CreateWindow();
    
    //initialize
    initialize();

    //Message Loop
    XEvent event;
    KeySym keysym;

    while(bDone == false)
    {
        while(XPending(gpDisplay))
        {
            XNextEvent(gpDisplay,&event);
            switch(event.type)
            {
                case MapNotify:
                    break;
                case KeyPress:
                    keysym = XkbKeycodeToKeysym(gpDisplay, event.xkey.keycode, 0, 0);
                    switch(keysym)
                    {
                        case XK_Escape:
                            bDone = true;
                            break;
                        case XK_F:
                        case XK_f:
                            if(bFullScreen == false)
                            {
                                ToggleFullScreen();
                                bFullScreen = true;
                            }
                            else
                            {
                                ToggleFullScreen();
                                bFullScreen = false;
                            }
                            break;
                       default:
                            break;
                    }
                    break;
                case ButtonPress:
                   switch(event.xbutton.button)
                   {
                        case 1:                         //left button
                            break;
                        case 2:                         //middle button
                            break;
                        case 3:                         //right button
                            break;
                        default:
                            break;
                   }
                   break;
                case MotionNotify:
                   break;
                case ConfigureNotify:
                    winWidth = event.xconfigure.width;
                    winHeight =event.xconfigure.height;
                    resize(winWidth, winHeight);
                    break;
                case Expose:
                    break;
                case DestroyNotify:
                    break;
                case 33:                                //to handle close button and close menu
                    bDone = true;
                    break;
                default:
                    break;
            }
        }
        display();
    }
    uninitialize();
    return(0);
}

void CreateWindow(void)
{
    //function prototype
    void uninitialize(void);

    //variable declarations
    XSetWindowAttributes winAttribs;
    int defaultScreen;
    int defaultDepth;
    int styleMask;
    int iNumberOfFBConfigs = 0;					// number of FBConfigs
    GLXFBConfig *pGLXFBConfigs = NULL;			//array of FBConfigs
    GLXFBConfig bestGLXFBConfig;				//best FBConfig among all
    XVisualInfo *pTempXVisualInfo = NULL;		//temporary visual info

    // Initialize Frame Buffer Attributes
    static int frameBufferAttributes[] = {
        GLX_X_RENDERABLE, True,
        GLX_DRAWABLE_TYPE,GLX_WINDOW_BIT,
        GLX_RENDER_TYPE, GLX_RGBA_BIT,
        GLX_X_VISUAL_TYPE, GLX_TRUE_COLOR,
        GLX_DOUBLEBUFFER, True,
        GLX_RED_SIZE, 8,
        GLX_GREEN_SIZE , 8,
        GLX_BLUE_SIZE, 8,
        GLX_ALPHA_SIZE, 8,
        GLX_DEPTH_SIZE, 24,
        GLX_STENCIL_SIZE, 8,
        None};

    //code
    gpDisplay = XOpenDisplay(NULL);
    if(!gpDisplay)
    {
        printf("ERROR : Unable To Open X Display.\nExitting Now ...\n");
        uninitialize();
        exit(1);
    }

    defaultScreen = XDefaultScreen(gpDisplay);

	// Retrive All FBConfigs Driver Has
	pGLXFBConfigs = glXChooseFBConfig(gpDisplay, defaultScreen,	frameBufferAttributes, &iNumberOfFBConfigs);

	printf("Number Of FBConfigs Matching To FBConfig = %d\n", iNumberOfFBConfigs);
	// Now We Have Multiple FBConfigs, We Need To Find Best Out Of Them

	int bestFrameBufferConfig = -1;
	int bestNumberOfSamples   = -1;
	int worstFrameBufferConfig = -1;
	int worstNumberOfSamples  = 999;

	//loop to check all fb configs
	for(int i = 0; i < iNumberOfFBConfigs; i++)
	{
		// Getting visual from each obtained FBConfig
		pTempXVisualInfo = glXGetVisualFromFBConfig(gpDisplay, pGLXFBConfigs[i]);

		if(pTempXVisualInfo)
		{
			//get Number of samplebuffer from respective fbConfig
			int sampleBuffers, samples;
			glXGetFBConfigAttrib(gpDisplay,
				pGLXFBConfigs[i],
				GLX_SAMPLE_BUFFERS,
				&sampleBuffers);

			//get number of samples from respective FBConfig
			glXGetFBConfigAttrib(gpDisplay, pGLXFBConfigs[i], GLX_SAMPLES, &samples);

			//More the number of samples and sample buffer more the eligible config buffer is
			if(bestFrameBufferConfig < 0 || sampleBuffers && samples >bestNumberOfSamples)
			{
				bestFrameBufferConfig=i;
				bestNumberOfSamples =samples;
			}

			if(worstFrameBufferConfig < 0 || !sampleBuffers || samples < worstNumberOfSamples)
			{
				worstFrameBufferConfig = i;
				worstNumberOfSamples = samples;
			}
		}

		XFree(pTempXVisualInfo);
	}

	// Now assign the found best one
	bestGLXFBConfig = pGLXFBConfigs[bestFrameBufferConfig];

	//Now assign the local best to global best
	gGLXFBConfig = bestGLXFBConfig;

	//Free the obtaind GLXFBConfig array;
	XFree(pGLXFBConfigs);

	//Accordingly get best visual
	gpXVisualInfo = glXGetVisualFromFBConfig(gpDisplay,  bestGLXFBConfig);

    winAttribs.border_pixel = 0;
    winAttribs.background_pixmap = 0;
    winAttribs.colormap = XCreateColormap(gpDisplay, 
                                          RootWindow(gpDisplay, gpXVisualInfo->screen),
                                          gpXVisualInfo->visual,
                                          AllocNone);
    gColormap = winAttribs.colormap;

    winAttribs.background_pixel = BlackPixel(gpDisplay,defaultScreen);

    winAttribs.event_mask = ExposureMask | VisibilityChangeMask | ButtonPressMask |
                           KeyPressMask | PointerMotionMask | StructureNotifyMask;
   
    styleMask = CWBorderPixel | CWBackPixel | CWEventMask | CWColormap;

    gWindow = XCreateWindow(gpDisplay,
                            RootWindow(gpDisplay,gpXVisualInfo->screen),
                            0,
                            0,
                            giWindowWidth,
                            giWindowHeight,
                            0,                                              // border width
                            gpXVisualInfo->depth,                           // color depth
                            InputOutput,
                            gpXVisualInfo->visual,
                            styleMask,
                            &winAttribs);
    if(!gWindow)
    {
        printf("ERROR : Failed To Create Main Window.\nExitting Now ...\n");
        uninitialize();
        exit(1);
    }

    XStoreName(gpDisplay, gWindow, "First XWindow");

    Atom windowManagerDelete = XInternAtom(gpDisplay, "WM_DELETE_WINDOW", True);
    XSetWMProtocols(gpDisplay, gWindow, &windowManagerDelete, 1);

    XMapWindow(gpDisplay, gWindow);
}

void ToggleFullScreen(void)
{
    //variable declarations
    Atom  wm_state;
    Atom fullscreen;
    XEvent xev = {0};

    //code
    wm_state = XInternAtom(gpDisplay, "_NET_WM_STATE", False);
    memset(&xev,0,sizeof(xev));

    xev.type = ClientMessage;
    xev.xclient.window = gWindow;
    xev.xclient.message_type = wm_state;
    xev.xclient.format = 32;
    xev.xclient.data.l[0] = bFullScreen ? 0 : 1;

    fullscreen = XInternAtom(gpDisplay, "_NET_WM_STATE_FULLSCREEN", False);
    xev.xclient.data.l[1] = fullscreen;
    
    XSendEvent(gpDisplay,
               RootWindow(gpDisplay, gpXVisualInfo->screen),
               False,
               StructureNotifyMask,
               &xev);
}



//initialize() function
void initialize(void)
{
	// function declaration
	void uninitialize(void);

    //variable declaration
    GLenum result;
    GLint iShaderCompileStatus = 0;
    GLint iProgramLinkStatus   = 0;
    GLint iInfoLogLength       = 0;
    GLchar *szInfoLog          = NULL;
    
    GLuint gVertexShaderObject;
    GLuint gFragmentShaderObject;
	GLuint vertexShaderObject_part2;
	GLuint fragmentShaderObject_part2;

    glxCreateContextAttribsARB = (glxCreateContextAttribsARBProc)glXGetProcAddressARB((GLubyte *)"glXCreateContextAttribsARB");
    if(glxCreateContextAttribsARB == NULL)
    {
    	printf("Address not found for glXCreateContextAttribsARB\n");
    	uninitialize();
    }

     const int attribs[] = {
    	GLX_CONTEXT_MAJOR_VERSION_ARB, 4,
    	GLX_CONTEXT_MINOR_VERSION_ARB, 4,
    	GLX_CONTEXT_PROFILE_MASK_ARB, GLX_CONTEXT_CORE_PROFILE_BIT_ARB,
    	None
    };

    gGLXContext = glxCreateContextAttribsARB(gpDisplay, gGLXFBConfig, 0, True, attribs);

   

    if(!gGLXContext)
    {
    	//if obtained one is not highest one, specify the lowest one it will give you highest possible for it
    	const int glattribs[]={
    		GLX_CONTEXT_MAJOR_VERSION_ARB, 1,
    		GLX_CONTEXT_MINOR_VERSION_ARB, 0,
    		None
    	};

    	gGLXContext = glxCreateContextAttribsARB(gpDisplay, gGLXFBConfig, 0, True, glattribs);
    }

    //Check whether the obtained context is really a hardware rendering context or not
    if(!glXIsDirect(gpDisplay, gGLXContext))
    {
    	printf("The Obtained Context Is Not Hardware Rendering Context\n");
    }
    else
    {
    	printf("The Obtained Context Is Hardware Rendering Context\n");
    }

    //Make the Obtained Context as Current Rendering Context
    glXMakeCurrent(gpDisplay, gWindow, gGLXContext);

    // Initialize GLEW
    result = glewInit();
    if(result != GLEW_OK)
    {
        printf("glewInit() failed");
        uninitialize();
    }

    // Define Vertex Shader Object
    gVertexShaderObject = glCreateShader(GL_VERTEX_SHADER);
    
    //write vertex shader code
	const GLchar *VertexShaderSourceCode =
		"	#version 430 core								" \
		"												\n	" \
		"	in vec4 vPosition;								" \
		"	in vec3 vColor;									" \
		"												\n	" \
		"	out vec3 out_color;								" \
		"												\n	" \
		"	uniform mat4 u_mvp_matrix;						" \
		"												\n	" \
		"	void main(void)									" \
		"	{												" \
		"		gl_Position = u_mvp_matrix * vPosition;		" \
		"		out_color = vColor;							" \
		"	}												";

    
	//specify above source code to the vertex shader object
	glShaderSource(gVertexShaderObject, 1, (const GLchar **)&VertexShaderSourceCode, NULL);
	
	//compile the vertex shader
	glCompileShader(gVertexShaderObject);
	glGetShaderiv(gVertexShaderObject, GL_COMPILE_STATUS, &iShaderCompileStatus);
	if (iShaderCompileStatus == GL_FALSE)
	{
		glGetShaderiv(gVertexShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (GLchar *)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(gVertexShaderObject, iInfoLogLength, &written, szInfoLog);
				printf("VERTEX SHADER COMPILATION LOG : %s\n", szInfoLog);
				free(szInfoLog);
				uninitialize();
			}
		}
	}

   //define fragment shader object
	gFragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);
	//write fragment shader code
	const GLchar *FragmentShaderSourceCode =
		"	#version 430 core									" \
		"	\n													" \
		"	in vec3 out_color;									" \
		"													\n	" \
		"	out vec4 FragColor;									" \
		"													\n	" \
		"	uniform float u_alpha_value;						" \
		"													\n	" \
		"	void main(void)										" \
		"	{													" \
		"		FragColor = vec4(out_color, u_alpha_value);		" \
		"	}													";

	//specify above source code to fragmnet shader object
	glShaderSource(gFragmentShaderObject, 1, (const GLchar **)&FragmentShaderSourceCode, NULL);
	
	//compile fragment shader
	iShaderCompileStatus = 0;
	iInfoLogLength = 0;
	szInfoLog = NULL;
	glCompileShader(gFragmentShaderObject);
	glGetShaderiv(gFragmentShaderObject, GL_COMPILE_STATUS, &iShaderCompileStatus);
	if (iShaderCompileStatus == GL_FALSE)
	{
		glGetShaderiv(gFragmentShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (GLchar *)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(gFragmentShaderObject, iInfoLogLength, &written, szInfoLog);
				printf("FRAGMENT SHADER COMPILATION LOG : %s\n", szInfoLog);
				free(szInfoLog);
				uninitialize();
			}
		}
	}


    //create shader program object
	gShaderProgramObject = glCreateProgram();
	
	//attach vertex shader to the shader program 
	glAttachShader(gShaderProgramObject, gVertexShaderObject);
	//attach fragment shader 
	glAttachShader(gShaderProgramObject, gFragmentShaderObject);
	
	//prelinking binding to vertex attribute
	glBindAttribLocation(gShaderProgramObject, AMC_ATTRIBUTE_POSITION, "vPosition");
	glBindAttribLocation(gShaderProgramObject, AMC_ATTRIBUTE_COLOR, "vColor");

	//link shader program
	glLinkProgram(gShaderProgramObject);
	//error checking
	iProgramLinkStatus = 0;
	iInfoLogLength = 0;
	szInfoLog = NULL;
	glGetProgramiv(gShaderProgramObject, GL_LINK_STATUS, &iProgramLinkStatus);
	if (iProgramLinkStatus == GL_FALSE)
	{
		glGetProgramiv(gShaderProgramObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (GLchar *)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLint written;
				glGetProgramInfoLog(gShaderProgramObject, iInfoLogLength, &written, szInfoLog);
				printf("SHADER PROGRAM LINK LOG : %s\n", szInfoLog);
				free(szInfoLog);
				uninitialize();
			}
		}
	}

    // Post-Linking retrieving uniform location
    mvpUniform = glGetUniformLocation(gShaderProgramObject, "u_mvp_matrix");
	alphaUniform = glGetUniformLocation(gShaderProgramObject, "u_alpha_value");

// ************************************ Shader for part 2 ******************************
	//define vertex shader object
	vertexShaderObject_part2 = glCreateShader(GL_VERTEX_SHADER);

	//write vertex shader code
	const GLchar *VertexShaderSourceCode_part2 =
		"	#version 430 core								" \
		"												\n	" \
		"	in vec4 vPosition;								" \
		"												\n	" \
		"	uniform mat4 u_mvp_matrix;						" \
		"												\n	" \
		"	void main(void)									" \
		"	{												" \
		"		gl_Position = u_mvp_matrix * vPosition;		" \
		"	}												";

	//specify above source code to the vertex shader object
	glShaderSource(vertexShaderObject_part2, 1, (const GLchar **)&VertexShaderSourceCode_part2, NULL);

	//compile the vertex shader
	glCompileShader(vertexShaderObject_part2);
	glGetShaderiv(vertexShaderObject_part2, GL_COMPILE_STATUS, &iShaderCompileStatus);
	if (iShaderCompileStatus == GL_FALSE)
	{
		glGetShaderiv(vertexShaderObject_part2, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (GLchar *)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(vertexShaderObject_part2, iInfoLogLength, &written, szInfoLog);
				printf("VERTEX SHADER PART 2 COMPILATION LOG : %s\n", szInfoLog);
				free(szInfoLog);
				uninitialize();
			}
		}
	}
	//define fragment shader object
	fragmentShaderObject_part2 = glCreateShader(GL_FRAGMENT_SHADER);
	//write fragment shader code
	const GLchar *FragmentShaderSourceCode_part2 =
		"	#version 430 core									" \
		"													\n	" \
		"	out vec4 FragColor;									" \
		"													\n	" \
		"	uniform float u_alpha_value;						" \
		"	uniform vec3 u_color_value;						\n	" \
		"													\n	" \
		"	void main(void)										" \
		"	{													" \
		"		FragColor = vec4(u_color_value, u_alpha_value);	" \
		"	}													";

	//specify above source code to fragmnet shader object
	glShaderSource(fragmentShaderObject_part2, 1, (const GLchar **)&FragmentShaderSourceCode_part2, NULL);

	//compile fragment shader
	iShaderCompileStatus = 0;
	iInfoLogLength = 0;
	szInfoLog = NULL;
	glCompileShader(fragmentShaderObject_part2);
	glGetShaderiv(fragmentShaderObject_part2, GL_COMPILE_STATUS, &iShaderCompileStatus);
	if (iShaderCompileStatus == GL_FALSE)
	{
		glGetShaderiv(fragmentShaderObject_part2, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (GLchar *)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(fragmentShaderObject_part2, iInfoLogLength, &written, szInfoLog);
				printf("FRAGMENT SHADER PART 2 COMPILATION LOG : %s\n", szInfoLog);
				free(szInfoLog);
				uninitialize();
			}
		}
	}

	//create shader program object
	shaderProgramObject_part2 = glCreateProgram();

	//attach vertex shader to the shader program 
	glAttachShader(shaderProgramObject_part2, vertexShaderObject_part2);
	//attach fragment shader 
	glAttachShader(shaderProgramObject_part2, fragmentShaderObject_part2);

	//prelinking binding to vertex attribute
	glBindAttribLocation(shaderProgramObject_part2, AMC_ATTRIBUTE_POSITION, "vPosition");

	//link shader program
	glLinkProgram(shaderProgramObject_part2);
	
	//error checking
	iProgramLinkStatus = 0;
	iInfoLogLength = 0;
	szInfoLog = NULL;
	
	glGetProgramiv(shaderProgramObject_part2, GL_LINK_STATUS, &iProgramLinkStatus);
	if (iProgramLinkStatus == GL_FALSE)
	{
		glGetProgramiv(shaderProgramObject_part2, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (GLchar *)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLint written;
				glGetProgramInfoLog(shaderProgramObject_part2, iInfoLogLength, &written, szInfoLog);
				printf("SHADER PROGRAM PART 2 LINK LOG : %s\n", szInfoLog);
				free(szInfoLog);
				uninitialize();
			}
		}
	}

	//postlinking retieving uniform location
	mvpUniform_part2 = glGetUniformLocation(shaderProgramObject_part2, "u_mvp_matrix");
	alphaUniform_part2 = glGetUniformLocation(shaderProgramObject_part2, "u_alpha_value");
	colorUniform_part2 = glGetUniformLocation(shaderProgramObject_part2, "u_color_value");

//----------------------------------- I ----------------------------------------
	const GLfloat i1Vertices[] = {
		-1.20f, 0.5f, 0.0f,
		- 0.88f, 0.5f, 0.0f,
		-1.04f, 0.5f, 0.0f,
		-1.04f, -0.5f, 0.0f,
		-1.20f, -0.5f, 0.0f,
		- 0.88f, -0.5f, 0.0f,

	};
	
	const GLfloat i1Color[] = {
		1.0f, 0.6f, 0.2f,
		1.0f, 0.6f, 0.2f,
		1.0f, 0.6f, 0.2f,
		0.070f, 0.533f, 0.027f,
		0.070f, 0.533f, 0.027f,
		0.070f, 0.533f, 0.027f,
	};

	glGenVertexArrays(1, &vao_i1);
	glBindVertexArray(vao_i1);
	// For position buffer
	glGenBuffers(1, &vbo_position_i1);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_position_i1);
	glBufferData(GL_ARRAY_BUFFER, sizeof(i1Vertices), i1Vertices, GL_STATIC_DRAW);
	glVertexAttribPointer(AMC_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	// For color buffer
	glGenBuffers(1, &vbo_color_i1);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_color_i1);
	glBufferData(GL_ARRAY_BUFFER, sizeof(i1Color), i1Color, GL_STATIC_DRAW);
	glVertexAttribPointer(AMC_ATTRIBUTE_COLOR, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AMC_ATTRIBUTE_COLOR);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	// Unbind with vao of I
	glBindVertexArray(0);
	//-----------------------------------------------------------------------------

	//------------------------------------- N --------------------------------------
	const GLfloat nVertices[] = {
		-0.68f, 0.52f, 0.0f,
		-0.68f, -0.52f, 0.0f,
		-0.68f, 0.52f, 0.0f,
		-0.36f, -0.51f, 0.0f,
		-0.36f, 0.52f, 0.0f,
		-0.36f, -0.52f, 0.0f
	};

	const GLfloat nColor[] = {
		1.0f, 0.6f, 0.2f,
		0.070f, 0.533f, 0.027f,
		1.0f, 0.6f, 0.2f,
		0.070f, 0.533f, 0.027f,
		1.0f, 0.6f, 0.2f,
		0.070f, 0.533f, 0.027f
	};

	glGenVertexArrays(1, &vao_n);
	glBindVertexArray(vao_n);
	// For position buffer
	glGenBuffers(1, &vbo_position_n);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_position_n);
	glBufferData(GL_ARRAY_BUFFER, sizeof(nVertices), nVertices, GL_STATIC_DRAW);
	glVertexAttribPointer(AMC_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	// For color buffer
	glGenBuffers(1, &vbo_color_n);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_color_n);
	glBufferData(GL_ARRAY_BUFFER, sizeof(nColor), nColor, GL_STATIC_DRAW);
	glVertexAttribPointer(AMC_ATTRIBUTE_COLOR, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AMC_ATTRIBUTE_COLOR);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	// Unbind with vao of N
	glBindVertexArray(0);
	//------------------------------------------------------------------------------
	
	// -------------------------------------- D -------------------------------------
	const GLfloat dVertices[] = {
		-0.10f, 0.5f, 0.0f,
		-0.10f, -0.5f, 0.0f,
		-0.17f, 0.5f, 0.0f,
		0.17f, 0.5f, 0.0f,
		0.17f, 0.5f, 0.0f,
		0.17f, -0.5f, 0.0f,
		-0.17f, -0.5f, 0.0f,
		0.17f, -0.5f, 0.0f
	};

	const GLfloat dColor[] = {
		1.0f, 0.6f, 0.2f,
		0.070f, 0.533f, 0.027f,
		1.0f, 0.6f, 0.2f,
		1.0f, 0.6f, 0.2f,
		1.0f, 0.6f, 0.2f,
		0.070f, 0.533f, 0.027f,
		0.070f, 0.533f, 0.027f,
		0.070f, 0.533f, 0.027f
	};
	
	glGenVertexArrays(1, &vao_d);
	glBindVertexArray(vao_d);
	// For position buffer
	glGenBuffers(1, &vbo_position_d);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_position_d);
	glBufferData(GL_ARRAY_BUFFER, sizeof(dVertices), dVertices, GL_STATIC_DRAW);
	glVertexAttribPointer(AMC_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	// For color buffer
	glGenBuffers(1, &vbo_color_d);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_color_d);
	glBufferData(GL_ARRAY_BUFFER, sizeof(dColor), dColor, GL_STATIC_DRAW);
	glVertexAttribPointer(AMC_ATTRIBUTE_COLOR, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AMC_ATTRIBUTE_COLOR);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	// Unbind with vao of D
	glBindVertexArray(0);
	//-------------------------------------------------------------------------------
	
	//-------------------------------- I2 -------------------------------------------
	const GLfloat i2Vertices[] = {
		0.36f, 0.5f, 0.0f,
		0.68f, 0.5f, 0.0f,
		0.52f, 0.5f, 0.0f,
		0.52f, -0.5f, 0.0f,
		0.36f, -0.5f, 0.0f,
		0.68f, -0.5f, 0.0f
	};
	
	const GLfloat i2Color[] = {
		1.0f, 0.6f, 0.2f,
		1.0f, 0.6f, 0.2f,
		1.0f, 0.6f, 0.2f,
		0.070f, 0.533f, 0.027f,
		0.070f, 0.533f, 0.027f,
		0.070f, 0.533f, 0.027f
	};

	glGenVertexArrays(1, &vao_i2);
	glBindVertexArray(vao_i2);
	// For position buffer
	glGenBuffers(1, &vbo_position_i2);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_position_i2);
	glBufferData(GL_ARRAY_BUFFER, sizeof(i2Vertices), i2Vertices, GL_STATIC_DRAW);
	glVertexAttribPointer(AMC_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	// For color buffer
	glGenBuffers(1, &vbo_color_i2);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_color_i2);
	glBufferData(GL_ARRAY_BUFFER, sizeof(i2Color), i2Color, GL_STATIC_DRAW);
	glVertexAttribPointer(AMC_ATTRIBUTE_COLOR, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AMC_ATTRIBUTE_COLOR);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	// Unbind with vao of I2
	glBindVertexArray(0);
	//-----------------------------------------------------------------------------

	//--------------------------------------- A -----------------------------------------
	const GLfloat aVertices[] = {
		0.83f, -0.52f, 0.0f,
		1.04f, 0.52f, 0.0f,
		1.04f, 0.52f, 0.0f,
		1.25f, -0.52f, 0.0f
	};

	const GLfloat aColor[] = {
		0.070f, 0.533f, 0.027f,
		1.0f, 0.6f, 0.2f,
		1.0f, 0.6f, 0.2f,
		0.070f, 0.533f, 0.027f
	};

	glGenVertexArrays(1, &vao_a);
	glBindVertexArray(vao_a);
	// For position buffer
	glGenBuffers(1, &vbo_position_a);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_position_a);
	glBufferData(GL_ARRAY_BUFFER, sizeof(aVertices), aVertices, GL_STATIC_DRAW);
	glVertexAttribPointer(AMC_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	// For color buffer
	glGenBuffers(1, &vbo_color_a);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_color_a);
	glBufferData(GL_ARRAY_BUFFER, sizeof(aColor), aColor, GL_STATIC_DRAW);
	glVertexAttribPointer(AMC_ATTRIBUTE_COLOR, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AMC_ATTRIBUTE_COLOR);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	// Unbind with vao of A
	glBindVertexArray(0);
	//----------------------------------------------------------------------------------
	
	//--------------------------------------- FLAG -----------------------------------------
	const GLfloat flagVertices[] = {
		0.953f, 0.015f, 0.0f,
		1.128f, 0.015f, 0.0f,
		1.122f, 0.045f, 0.0f,
		0.958f, 0.045f, 0.0f,
		0.947f, -0.015f, 0.0f,
		1.135f, -0.015f, 0.0f,
		1.128f, 0.015f, 0.0f,
		0.953f, 0.015f, 0.0f,
		1.135f, -0.015f, 0.0f,
		0.947f, -0.015f, 0.0f,
		0.94f, -0.045f, 0.0f,
		1.14f, -0.045f, 0.0f

	}; // 4 saffron quad - 4 white quad - 4 green quad

	const GLfloat flagColor[] = {
		1.0f, 0.6f, 0.2f,
		1.0f, 0.6f, 0.2f,
		1.0f, 0.6f, 0.2f,
		1.0f, 0.6f, 0.2f,
		1.0f, 1.0f, 1.0f,
		1.0f, 1.0f, 1.0f,
		1.0f, 1.0f, 1.0f,
		1.0f, 1.0f, 1.0f,
		0.070f, 0.533f, 0.027f,
		0.070f, 0.533f, 0.027f,
		0.070f, 0.533f, 0.027f,
		0.070f, 0.533f, 0.027f
	};

	glGenVertexArrays(1, &vao_flag);
	glBindVertexArray(vao_flag);
	// For position buffer
	glGenBuffers(1, &vbo_position_flag);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_position_flag);
	glBufferData(GL_ARRAY_BUFFER, sizeof(flagVertices), flagVertices, GL_STATIC_DRAW);
	glVertexAttribPointer(AMC_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	// For color buffer
	glGenBuffers(1, &vbo_color_flag);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_color_flag);
	glBufferData(GL_ARRAY_BUFFER, sizeof(flagColor), flagColor, GL_STATIC_DRAW);
	glVertexAttribPointer(AMC_ATTRIBUTE_COLOR, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AMC_ATTRIBUTE_COLOR);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	// Unbind with vao of flag
	glBindVertexArray(0);
	//----------------------------------------------------------------------------------


	//------------------------------------ PLANE ---------------------------------------
	const GLfloat planeVertices[] = {
		
		0.3f,    0.04f,  0.0f,	// 4
		0.3f,	-0.04f,  0.0f,	// 2
		0.4f,	 0.0f,   0.0f,	// 3
		0.0f,	-0.04f,  0.0f,	// 1
		0.3f,	-0.04f,  0.0f,	// 2
		0.3f,    0.04f,  0.0f,	// 4
		0.0f,    0.04f,  0.0f,	// 5
		0.0f,	-0.04f,  0.0f,	// 1
		0.0f,    0.04f,  0.0f,	// 5
		-0.08f,  0.09f,  0.0f,	// 6
		-0.08f, -0.09f,  0.0f,	// 7	BODY
		0.2f,    0.03f,  0.0f,
		0.10f,   0.2f,   0.0f,
		0.0f,    0.2f,   0.0f,
		0.05f,	 0.03f,  0.0f,		// UPPER WING
		0.2f,	-0.03f,  0.0f,
		0.10f,	-0.2f,   0.0f,
		0.0f,	-0.2f,   0.0f,
		0.05f,	-0.03f,  0.0f,		// LOWER WING
		0.03f,   0.02f,  0.0f,
		0.08f,   0.02f,  0.0f,
		0.03f,	-0.02f,  0.0f,
		0.08f,  -0.02f,  0.0f,
		0.055f,  0.02f,  0.0f,
		0.055f, -0.02f,  0.0f,	// I
		0.10f,   0.02f,  0.0f,
		0.10f,	-0.025f, 0.0f,
		0.14f,   0.02f,  0.0f,
		0.14f,	-0.025f, 0.0f,
		0.10f,   0.02f,  0.0f,
		0.14f,   0.02f,  0.0f,
		0.10f,   0.0f,   0.0f,
		0.14f,   0.0f,   0.0f,	// A
		0.16f,   0.02f,  0.0f,
		0.16f,	-0.025f, 0.0f,
		0.16f,   0.02f,  0.0f,
		0.20f,   0.02f,  0.0f,
		0.16f,   0.0f,   0.0f,
		0.19f,   0.0f,   0.0f	// F
	};
	
	const GLfloat planeColor[] = {
		
		0.72f, 0.886f, 0.933f,
		0.72f, 0.886f, 0.933f,
		0.72f, 0.886f, 0.933f,
		0.72f, 0.886f, 0.933f,
		0.72f, 0.886f, 0.933f,
		0.72f, 0.886f, 0.933f,
		0.72f, 0.886f, 0.933f,
		0.72f, 0.886f, 0.933f,
		0.72f, 0.886f, 0.933f,
		0.72f, 0.886f, 0.933f,
		0.72f, 0.886f, 0.933f,
		0.72f, 0.886f, 0.933f, // body
		0.72f, 0.886f, 0.933f,
		0.72f, 0.886f, 0.933f,
		0.72f, 0.886f, 0.933f,
		0.72f, 0.886f, 0.933f,
		0.72f, 0.886f, 0.933f,
		0.72f, 0.886f, 0.933f,
		0.72f, 0.886f, 0.933f,
		0.72f, 0.886f, 0.933f,
		1.0f, 1.0f, 1.0f,
		1.0f, 1.0f, 1.0f,
		1.0f, 1.0f, 1.0f,
		1.0f, 1.0f, 1.0f,
		1.0f, 1.0f, 1.0f,
		1.0f, 1.0f, 1.0f,
		1.0f, 1.0f, 1.0f,
		1.0f, 1.0f, 1.0f,
		1.0f, 1.0f, 1.0f,
		1.0f, 1.0f, 1.0f,
		1.0f, 1.0f, 1.0f,
		1.0f, 1.0f, 1.0f,
		1.0f, 1.0f, 1.0f,
		1.0f, 1.0f, 1.0f,
		1.0f, 1.0f, 1.0f,
		1.0f, 1.0f, 1.0f,
		1.0f, 1.0f, 1.0f,
		1.0f, 1.0f, 1.0f,
		1.0f, 1.0f, 1.0f,
		1.0f, 1.0f, 1.0f
	};

	glGenVertexArrays(1, &vao_plane);
	glBindVertexArray(vao_plane);
	// For position buffer
	glGenBuffers(1, &vbo_position_plane);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_position_plane);
	glBufferData(GL_ARRAY_BUFFER, sizeof(planeVertices), planeVertices, GL_STATIC_DRAW);
	glVertexAttribPointer(AMC_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	// For color buffer
	glGenBuffers(1, &vbo_color_plane);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_color_plane);
	glBufferData(GL_ARRAY_BUFFER, sizeof(planeColor), planeColor, GL_STATIC_DRAW);
	glVertexAttribPointer(AMC_ATTRIBUTE_COLOR, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AMC_ATTRIBUTE_COLOR);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	// Unbind with vao of plane
	glBindVertexArray(0);
	//----------------------------------------------------------------------------------

	// -----------------------------------Plane Path -----------------------------------
	// **** path2 ****
	float path2Value = -2.2f;

	for (int i = 0; i < 1500; i = i + 3)
	{
			path2[i] = path2Value;
			path2[i + 1] = 0;
			path2[i + 2] = 0;

		if (path2Value < -1.2f)
			path2Value = path2Value + 0.01f;
		else if (path2Value >= -1.2f && path2Value < 1.2)
			path2Value = path2Value + 0.008f;
		else if (path2Value >= 1.2f && path2Value <= 2.2f)
			path2Value = path2Value + 0.01f;
	}

	float path1StartingValue_x1 = -1.0f;	 // -1 to 0
	float path1StartingValue_x2 = 0.0f;		 // 0 to 1
	float path1Value_y = 0.0f;

	for (int i = 0; i < 1500; i = i + 3)
	{
		if (i < 300)
		{
			path1Value_y = 1.2f * path1StartingValue_x1 * path1StartingValue_x1;

			path1[i] = path1StartingValue_x1 - 1.2f;
			path1[i + 1] = path1Value_y;
			path1[i + 2] = 0.0f;

			path1StartingValue_x1 = path1StartingValue_x1 + 0.01f;
		}
		if ((i >= 300) && (i < 1200))
		{
			path1Value_y = 0.0f;

			path1[i] = path1StartingValue_x1 - 1.2f;
			path1[i + 1] = path1Value_y;
			path1[i + 2] = 0.0f;

			path1StartingValue_x1 = path1StartingValue_x1 + 0.008f;

		}
		if ((i >= 1200) && (i < 1500))
		{
			path1Value_y = 1.2f * path1StartingValue_x2 * path1StartingValue_x2;

			path1[i] = path1StartingValue_x2 + 1.2f;
			path1[i + 1] = path1Value_y;
			path1[i + 2] = 0.0f;

			path1StartingValue_x2 = path1StartingValue_x2 + 0.01f;
		}
	}

	float path3StartingValue_x1 = -1.0f;	 // -1 to 0
	float path3StartingValue_x2 = 0.0f;		 // 0 to 1
	float path3Value_y = 0.0f;
	for (int i = 0; i < 1500; i = i + 3)
	{
		if (i < 300)
		{
			path3Value_y = 1.2f * path3StartingValue_x1 * path3StartingValue_x1;

			path3[i] = path3StartingValue_x1 - 1.2f;
			path3[i + 1] = -path3Value_y;
			path3[i + 2] = 0.0f;

			path3StartingValue_x1 = path3StartingValue_x1 + 0.01f;
		}
		if ((i >= 300) && (i < 1200))
		{
			path3Value_y = 0.0f;

			path3[i] = path3StartingValue_x1 - 1.2f;
			path3[i + 1] = -path3Value_y;
			path3[i + 2] = 0.0f;

			path3StartingValue_x1 = path3StartingValue_x1 + 0.008f;

		}
		if ((i >= 1200) && (i < 1500))
		{
			path3Value_y = 1.2f * path3StartingValue_x2 * path3StartingValue_x2;

			path3[i] = path3StartingValue_x2 + 1.2f;
			path3[i + 1] = -path3Value_y;
			path3[i + 2] = 0.0f;

			path3StartingValue_x2 = path3StartingValue_x2 + 0.01f;
		}

	}


	// PATH 1
	glGenVertexArrays(1, &vao_path1);
	glBindVertexArray(vao_path1);
	// For position buffer
	glGenBuffers(1, &vbo_position_path1);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_position_path1);
	glBufferData(GL_ARRAY_BUFFER, sizeof(path1), path1, GL_STATIC_DRAW);
	glVertexAttribPointer(AMC_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindVertexArray(0);


	// PATH 2
	glGenVertexArrays(1, &vao_path2);
	glBindVertexArray(vao_path2);
	// For position buffer
	glGenBuffers(1, &vbo_position_path2);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_position_path2);
	glBufferData(GL_ARRAY_BUFFER, sizeof(path2), path2, GL_STATIC_DRAW);
	glVertexAttribPointer(AMC_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindVertexArray(0);


	// PATH 3
	glGenVertexArrays(1, &vao_path3);
	glBindVertexArray(vao_path3);
	// For position buffer
	glGenBuffers(1, &vbo_position_path3);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_position_path3);
	glBufferData(GL_ARRAY_BUFFER, sizeof(path3), path3, GL_STATIC_DRAW);
	glVertexAttribPointer(AMC_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindVertexArray(0);
	//----------------------------------------------------------------------------------

	trans_i1 = -2.0f;
	trans_n = 2.0f;
	trans_i2 = -2.0f;
	trans_a = 2.0f;

    glClearDepth(1.0f);
    glEnable(GL_DEPTH_TEST);
    glDepthFunc(GL_LEQUAL);

    glClearColor(0.0f,0.0f,0.0f,1.0f);

    perspectiveProjectionMatrix = mat4::identity();
}

//resize() function
void resize(int width, int height)
{
    if(height == 0)
        height =1;
    glViewport(0,0,(GLsizei)width, (GLsizei)height);

    perspectiveProjectionMatrix = perspective(45.0f, (GLfloat)width/(GLfloat)height, 0.1f, 100.0f);
}

void update(void)
{
	if (translateI1 == true)
	{
		if (trans_i1 >= 0.0f)
		{
			trans_i1 = 0.0f;
			translateA = true;
		}
		else
		{
			trans_i1 += 0.0005f;
		}
	}
	if (translateA == true)
	{
		if (trans_a <= 0.0f)
		{
			trans_a = 0.0f;
			translateN = true;
		}
		else
		{
			trans_a -= 0.0005f;
		}
	}
	if (translateN == true)
	{
		if (trans_n <= 0.0f)
		{
			trans_n = 0.0f;
			translateI2 = true;
		}
		else
		{
			trans_n -= 0.0005f;
		}
	}
	if (translateI2 == true)
	{
		if (trans_i2 >= 0.0f)
		{
			trans_i2 = 0.0f;
			translateD = true;
		}
		else
		{
			trans_i2 += 0.0005f;
		}
	}

	if (translateD == true && alphaValue < 1.0f)
	{
		alphaValue = alphaValue + 0.0005f;
		if (alphaValue >= 1.0f)
			translatePlane = true;
	}

	if (translatePlane == true)
	{
		if ((anim % 20) == 0)
		{
			if (ip < 1500)
				ip = ip + 3;
			pointCount++;
			if (pointCount > 450)
				showFlag = true;
			if (pointCount < 100)
			{
				angle1 = angle1 + delta;
				angle2 = angle2 - delta;
			}
			else if (pointCount < 400)
			{
				angle1 = 0.0f;
				angle2 = 0.0f;
			}
			else if (pointCount < 500)
			{
				angle1 = angle1 + delta;
				angle2 = angle2 - delta;
			}
		}
	}
	
	if (pointCount > 500)
	{
		alphaValue_part2 = alphaValue_part2 - 0.0005f;
	}
	anim++;
}

//display function
void display(void)
{
	// function declaration
	void update(void);

    //declaration of matrices
	mat4 modelViewMatrix;
	mat4 modelViewProjectionMatrix;
	mat4 translationMatrix;
	mat4 rotationMatrix;

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	//initialize above matrices to identity
	modelViewMatrix = mat4::identity();
	modelViewProjectionMatrix = mat4::identity();
	translationMatrix = mat4::identity();
	rotationMatrix = mat4::identity();

	if (translateI1 == true)
	{
		glUseProgram(gShaderProgramObject);

		//do necessary transformation
		translationMatrix = translate(trans_i1, 0.0f, -3.0f);
		modelViewMatrix = modelViewMatrix * translationMatrix;
		modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;

		//send necessary matrices to shader in respective uniforms
		glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);
		glUniform1f(alphaUniform, 1.0f);

		glLineWidth(20.0f);
		// ******************* I ************************
		glBindVertexArray(vao_i1);
		glDrawArrays(GL_LINES, 0, 2);
		glDrawArrays(GL_LINES, 2, 2);
		glDrawArrays(GL_LINES, 4, 2);
		//unbind vao
		glBindVertexArray(0);
		// **********************************************
		glUseProgram(0);

	}
	if (translateN == true)
	{
		glUseProgram(gShaderProgramObject);

		//initialize above matrices to identity
		modelViewMatrix = mat4::identity();
		modelViewProjectionMatrix = mat4::identity();
		translationMatrix = mat4::identity();

		//do necessary transformation
		translationMatrix = translate(0.0f, trans_n, -3.0f);
		modelViewMatrix = modelViewMatrix * translationMatrix;
		modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;

		//send necessary matrices to shader in respective uniforms
		glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);
		glUniform1f(alphaUniform, 1.0f);

		glLineWidth(20.0f);
		// ******************* N ************************
		glBindVertexArray(vao_n);
		glDrawArrays(GL_LINES, 0, 2);
		glDrawArrays(GL_LINES, 2, 2);
		glDrawArrays(GL_LINES, 4, 2);
		//unbind vao
		glBindVertexArray(0);
		// **********************************************
		glUseProgram(0);

	}
	if (translateI2)
	{
		glUseProgram(gShaderProgramObject);

		//initialize above matrices to identity
		modelViewMatrix = mat4::identity();
		modelViewProjectionMatrix = mat4::identity();
		translationMatrix = mat4::identity();

		//do necessary transformation
		translationMatrix = translate(0.0f, trans_i2, -3.0f);
		modelViewMatrix = modelViewMatrix * translationMatrix;
		modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;

		//send necessary matrices to shader in respective uniforms
		glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);
		glUniform1f(alphaUniform, 1.0f);

		glLineWidth(20.0f);
		// ******************* I ************************
		glBindVertexArray(vao_i2);
		glDrawArrays(GL_LINES, 0, 2);
		glDrawArrays(GL_LINES, 2, 2);
		glDrawArrays(GL_LINES, 4, 2);
		//unbind vao
		glBindVertexArray(0);
		// **********************************************
		glUseProgram(0);

	}
	if (translateA == true)
	{
		glUseProgram(gShaderProgramObject);

		//initialize above matrices to identity
		modelViewMatrix = mat4::identity();
		modelViewProjectionMatrix = mat4::identity();
		translationMatrix = mat4::identity();

		//do necessary transformation
		translationMatrix = translate(trans_a, 0.0f, -3.0f);
		modelViewMatrix = modelViewMatrix * translationMatrix;
		modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;

		//send necessary matrices to shader in respective uniforms
		glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);
		glUniform1f(alphaUniform, 1.0f);

		glLineWidth(20.0f);
		// ******************* A ************************
		glBindVertexArray(vao_a);
		glDrawArrays(GL_LINES, 0, 2);
		glDrawArrays(GL_LINES, 2, 2);
		//unbind vao
		glBindVertexArray(0);
		// **********************************************
		glUseProgram(0);

	}
	if (translateD == true)
	{
		glUseProgram(gShaderProgramObject);

		//initialize above matrices to identity
		modelViewMatrix = mat4::identity();
		modelViewProjectionMatrix = mat4::identity();
		translationMatrix = mat4::identity();

		//do necessary transformation
		translationMatrix = translate(trans_a, 0.0f, -3.0f);
		modelViewMatrix = modelViewMatrix * translationMatrix;
		modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;

		//send necessary matrices to shader in respective uniforms
		glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);
		glUniform1f(alphaUniform, alphaValue);
		
		glLineWidth(20.0f);

		// ******************* D ************************
		glEnable(GL_BLEND);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
		glBindVertexArray(vao_d);
		glDrawArrays(GL_LINES, 0, 2);
		glDrawArrays(GL_LINES, 2, 2);
		glDrawArrays(GL_LINES, 4, 2);
		glDrawArrays(GL_LINES, 6, 2);
		//unbind vao
		glBindVertexArray(0);
		glDisable(GL_BLEND);
		// **********************************************
		glUseProgram(0);

	}
	
	if (translatePlane == true)
	{
		// ***************** DRAW PATHS ******************

		// --------------------- PATH 1 ---------------------
		// PATH 1 - SAFFRON
		glUseProgram(shaderProgramObject_part2);

		//initialize above matrices to identity
		modelViewMatrix = mat4::identity();
		modelViewProjectionMatrix = mat4::identity();
		translationMatrix = mat4::identity();

		//do necessary transformation
		translationMatrix = translate(0.0f, 0.03f, -3.0f);
		modelViewMatrix = modelViewMatrix * translationMatrix;
		modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;

		//send necessary matrices to shader in respective uniforms
		glUniformMatrix4fv(mvpUniform_part2, 1, GL_FALSE, modelViewProjectionMatrix);
		if (pointCount < 500)
			glUniform1f(alphaUniform_part2, 1.0f);
		else
			glUniform1f(alphaUniform_part2, alphaValue_part2);
		glUniform3fv(colorUniform_part2, 1, vec3(1.0f, 0.6f, 0.2f));

		glEnable(GL_POINT_SMOOTH);
		glPointSize(10.0f);
		glEnable(GL_BLEND);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
		glBindVertexArray(vao_path1);
		glDrawArrays(GL_POINTS, 0, pointCount);
		glBindVertexArray(0);
		glDisable(GL_BLEND);
		glDisable(GL_POINT_SMOOTH);

		// PATH 1 - WHITE
		//initialize above matrices to identity
		modelViewMatrix = mat4::identity();
		modelViewProjectionMatrix = mat4::identity();
		translationMatrix = mat4::identity();

		//do necessary transformation
		translationMatrix = translate(0.0f, 0.0f, -3.0f);
		modelViewMatrix = modelViewMatrix * translationMatrix;
		modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;

		//send necessary matrices to shader in respective uniforms
		glUniformMatrix4fv(mvpUniform_part2, 1, GL_FALSE, modelViewProjectionMatrix);
		if (pointCount < 500)
			glUniform1f(alphaUniform_part2, 1.0f);
		else
			glUniform1f(alphaUniform_part2, alphaValue_part2);
		glUniform3fv(colorUniform_part2, 1, vec3(1.0f, 1.0f, 1.0f));

		glEnable(GL_BLEND);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
		glEnable(GL_POINT_SMOOTH);
		glPointSize(10.0f);
		glBindVertexArray(vao_path1);
		glDrawArrays(GL_POINTS, 0, pointCount);
		glBindVertexArray(0);
		glDisable(GL_POINT_SMOOTH);
		glDisable(GL_BLEND);

		// PATH 1 - GREEN
		//initialize above matrices to identity
		modelViewMatrix = mat4::identity();
		modelViewProjectionMatrix = mat4::identity();
		translationMatrix = mat4::identity();

		//do necessary transformation
		translationMatrix = translate(0.0f, -0.03f, -3.0f);
		modelViewMatrix = modelViewMatrix * translationMatrix;
		modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;

		//send necessary matrices to shader in respective uniforms
		glUniformMatrix4fv(mvpUniform_part2, 1, GL_FALSE, modelViewProjectionMatrix);
		if (pointCount < 500)
			glUniform1f(alphaUniform_part2, 1.0f);
		else
			glUniform1f(alphaUniform_part2, alphaValue_part2);
		glUniform3fv(colorUniform_part2, 1, vec3(0.070f, 0.533f, 0.027f));

		glEnable(GL_BLEND);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
		glEnable(GL_POINT_SMOOTH);
		glPointSize(10.0f);
		glBindVertexArray(vao_path1);
		glDrawArrays(GL_POINTS, 0, pointCount);
		glBindVertexArray(0);
		glDisable(GL_POINT_SMOOTH);
		glDisable(GL_BLEND);

		glUseProgram(0);


		// --------------------- PATH 3 ---------------------
		// PATH 3 - SAFFRON
		glUseProgram(shaderProgramObject_part2);

		//initialize above matrices to identity
		modelViewMatrix = mat4::identity();
		modelViewProjectionMatrix = mat4::identity();
		translationMatrix = mat4::identity();

		//do necessary transformation
		translationMatrix = translate(0.0f, 0.03f, -3.0f);
		modelViewMatrix = modelViewMatrix * translationMatrix;
		modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;

		//send necessary matrices to shader in respective uniforms
		glUniformMatrix4fv(mvpUniform_part2, 1, GL_FALSE, modelViewProjectionMatrix);
		if (pointCount < 500)
			glUniform1f(alphaUniform_part2, 1.0f);
		else
			glUniform1f(alphaUniform_part2, alphaValue_part2);
		glUniform3fv(colorUniform_part2, 1, vec3(1.0f, 0.6f, 0.2f));

		glEnable(GL_BLEND);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
		glEnable(GL_POINT_SMOOTH);
		glPointSize(10.0f);
		glBindVertexArray(vao_path3);
		glDrawArrays(GL_POINTS, 0, pointCount);
		glBindVertexArray(0);
		glDisable(GL_POINT_SMOOTH);
		glDisable(GL_BLEND);

		// PATH 1 - WHITE
		//initialize above matrices to identity
		modelViewMatrix = mat4::identity();
		modelViewProjectionMatrix = mat4::identity();
		translationMatrix = mat4::identity();

		//do necessary transformation
		translationMatrix = translate(0.0f, 0.0f, -3.0f);
		modelViewMatrix = modelViewMatrix * translationMatrix;
		modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;

		//send necessary matrices to shader in respective uniforms
		glUniformMatrix4fv(mvpUniform_part2, 1, GL_FALSE, modelViewProjectionMatrix);
		if (pointCount < 500)
			glUniform1f(alphaUniform_part2, 1.0f);
		else
			glUniform1f(alphaUniform_part2, alphaValue_part2);
		glUniform3fv(colorUniform_part2, 1, vec3(1.0f, 1.0f, 1.0f));

		glEnable(GL_BLEND);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
		glEnable(GL_POINT_SMOOTH);
		glPointSize(10.0f);
		glBindVertexArray(vao_path3);
		glDrawArrays(GL_POINTS, 0, pointCount);
		glBindVertexArray(0);
		glDisable(GL_POINT_SMOOTH);
		glDisable(GL_BLEND);

		// PATH 1 - GREEN
		//initialize above matrices to identity
		modelViewMatrix = mat4::identity();
		modelViewProjectionMatrix = mat4::identity();
		translationMatrix = mat4::identity();

		//do necessary transformation
		translationMatrix = translate(0.0f, -0.03f, -3.0f);
		modelViewMatrix = modelViewMatrix * translationMatrix;
		modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;

		//send necessary matrices to shader in respective uniforms
		glUniformMatrix4fv(mvpUniform_part2, 1, GL_FALSE, modelViewProjectionMatrix);
		if (pointCount < 500)
			glUniform1f(alphaUniform_part2, 1.0f);
		else
			glUniform1f(alphaUniform_part2, alphaValue_part2);
		glUniform3fv(colorUniform_part2, 1, vec3(0.070f, 0.533f, 0.027f));

		glEnable(GL_BLEND);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
		glEnable(GL_POINT_SMOOTH);
		glPointSize(10.0f);
		glBindVertexArray(vao_path3);
		glDrawArrays(GL_POINTS, 0, pointCount);
		glBindVertexArray(0);
		glDisable(GL_POINT_SMOOTH);
		glDisable(GL_BLEND);

		glUseProgram(0);


		// --------------------- PATH 2 ---------------------
		// PATH 2 - SAFFRON
		glUseProgram(shaderProgramObject_part2);

		//initialize above matrices to identity
		modelViewMatrix = mat4::identity();
		modelViewProjectionMatrix = mat4::identity();
		translationMatrix = mat4::identity();

		//do necessary transformation
		translationMatrix = translate(0.0f, 0.03f, -3.0f);
		modelViewMatrix = modelViewMatrix * translationMatrix;
		modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;

		//send necessary matrices to shader in respective uniforms
		glUniformMatrix4fv(mvpUniform_part2, 1, GL_FALSE, modelViewProjectionMatrix);
		if (pointCount < 500)
			glUniform1f(alphaUniform_part2, 1.0f);
		else
			glUniform1f(alphaUniform_part2, alphaValue_part2);
		glUniform3fv(colorUniform_part2, 1, vec3(1.0f, 0.6f, 0.2f));

		glEnable(GL_BLEND);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
		glPointSize(10.0f);
		glBindVertexArray(vao_path2);
		glDrawArrays(GL_POINTS, 0, pointCount);
		glBindVertexArray(0);
		glDisable(GL_BLEND);

		// PATH 2 - WHITE
		//initialize above matrices to identity
		modelViewMatrix = mat4::identity();
		modelViewProjectionMatrix = mat4::identity();
		translationMatrix = mat4::identity();

		//do necessary transformation
		translationMatrix = translate(0.0f, 0.0f, -3.0f);
		modelViewMatrix = modelViewMatrix * translationMatrix;
		modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;

		//send necessary matrices to shader in respective uniforms
		glUniformMatrix4fv(mvpUniform_part2, 1, GL_FALSE, modelViewProjectionMatrix);
		if (pointCount < 500)
			glUniform1f(alphaUniform_part2, 1.0f);
		else
			glUniform1f(alphaUniform_part2, alphaValue_part2);
		glUniform3fv(colorUniform_part2, 1, vec3(1.0f, 1.0f, 1.0f));

		glEnable(GL_BLEND);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
		glEnable(GL_POINT_SMOOTH);
		glPointSize(10.0f);
		glBindVertexArray(vao_path2);
		glDrawArrays(GL_POINTS, 0, pointCount);
		glBindVertexArray(0);
		glDisable(GL_BLEND);

		// PATH 2 - Green
		//initialize above matrices to identity
		modelViewMatrix = mat4::identity();
		modelViewProjectionMatrix = mat4::identity();
		translationMatrix = mat4::identity();

		//do necessary transformation
		translationMatrix = translate(0.0f, -0.03f, -3.0f);
		modelViewMatrix = modelViewMatrix * translationMatrix;
		modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;

		//send necessary matrices to shader in respective uniforms
		glUniformMatrix4fv(mvpUniform_part2, 1, GL_FALSE, modelViewProjectionMatrix);
		if (pointCount < 500)
			glUniform1f(alphaUniform_part2, 1.0f);
		else
			glUniform1f(alphaUniform_part2, alphaValue_part2);
		glUniform3fv(colorUniform_part2, 1, vec3(0.070f, 0.533f, 0.027f));

		glEnable(GL_BLEND);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
		glPointSize(10.0f);
		glBindVertexArray(vao_path2);
		glDrawArrays(GL_POINTS, 0, pointCount);
		glBindVertexArray(0);
		glDisable(GL_BLEND);

		//unuse program
		glUseProgram(0);
		// ************************************************

		glUseProgram(gShaderProgramObject);
		//initialize above matrices to identity
		modelViewMatrix = mat4::identity();
		modelViewProjectionMatrix = mat4::identity();
		translationMatrix = mat4::identity();
		rotationMatrix = mat4::identity();

		//do necessary transformation
		translationMatrix = translate(path1[ip]+0.02f, path1[ip + 1], -3.0f);
		if (pointCount < 100)
			rotationMatrix = rotate(angle1, 0.0f, 0.0f, 1.0f);
		else if(pointCount < 400)
			rotationMatrix = mat4::identity();
		else if (pointCount <500)
			rotationMatrix = rotate(angle1, 0.0f, 0.0f, 1.0f);

		modelViewMatrix = modelViewMatrix * translationMatrix * rotationMatrix;
		modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;

		//send necessary matrices to shader in respective uniforms
		glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);
		if (pointCount < 500)
			glUniform1f(alphaUniform, 1.0f);
		else
			glUniform1f(alphaUniform, 0.0f);

		// ******************* PLANE 1 *********************
		glLineWidth(2.0f);
		glEnable(GL_BLEND);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
		glBindVertexArray(vao_plane);
		glDrawArrays(GL_TRIANGLES, 0, 3);
		glDrawArrays(GL_TRIANGLE_FAN, 3, 4);
		glDrawArrays(GL_TRIANGLE_FAN, 7, 4);
		glDrawArrays(GL_TRIANGLE_FAN, 11, 4);
		glDrawArrays(GL_TRIANGLE_FAN, 15, 4);
		glDrawArrays(GL_LINES, 19, 20);
		glBindVertexArray(0);
		glDisable(GL_BLEND);

		// ******************* PLANE 2 *********************
		//initialize above matrices to identity
		modelViewMatrix = mat4::identity();
		modelViewProjectionMatrix = mat4::identity();
		translationMatrix = mat4::identity();

		//do necessary transformation
		translationMatrix = translate(path2[ip]+0.02f, path2[ip + 1], -3.0f);
		modelViewMatrix = modelViewMatrix * translationMatrix;
		modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;

		//send necessary matrices to shader in respective uniforms
		glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);
		if (pointCount < 500)
			glUniform1f(alphaUniform, 1.0f);
		else
			glUniform1f(alphaUniform, 0.0f);

		glLineWidth(2.0f);
		glEnable(GL_BLEND);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
		glBindVertexArray(vao_plane);
		glDrawArrays(GL_TRIANGLES, 0, 3);
		glDrawArrays(GL_TRIANGLE_FAN, 3, 4);
		glDrawArrays(GL_TRIANGLE_FAN, 7, 4);
		glDrawArrays(GL_TRIANGLE_FAN, 11, 4);
		glDrawArrays(GL_TRIANGLE_FAN, 15, 4);
		glDrawArrays(GL_LINES, 19, 20);
		glBindVertexArray(0);
		glDisable(GL_BLEND);

		// ******************* PLANE 3 *********************

		//initialize above matrices to identity
		modelViewMatrix = mat4::identity();
		modelViewProjectionMatrix = mat4::identity();
		translationMatrix = mat4::identity();
		rotationMatrix = mat4::identity();

		//do necessary transformation
		translationMatrix = translate(path3[ip]+0.02f, path3[ip + 1], -3.0f);
		if (pointCount < 100)
			rotationMatrix = rotate(angle2, 0.0f, 0.0f, 1.0f);
		else if (pointCount < 400)
			rotationMatrix = mat4::identity();
		else if (pointCount <500)
			rotationMatrix = rotate(angle2, 0.0f, 0.0f, 1.0f);

		modelViewMatrix = modelViewMatrix * translationMatrix * rotationMatrix;
		modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;

		//send necessary matrices to shader in respective uniforms
		glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);
		if (pointCount < 500)
			glUniform1f(alphaUniform, 1.0f);
		else
			glUniform1f(alphaUniform, 0.0f);

		glLineWidth(2.0f);
		glEnable(GL_BLEND);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
		glBindVertexArray(vao_plane);
		glDrawArrays(GL_TRIANGLES, 0, 3);
		glDrawArrays(GL_TRIANGLE_FAN, 3, 4);
		glDrawArrays(GL_TRIANGLE_FAN, 7, 4);
		glDrawArrays(GL_TRIANGLE_FAN, 11, 4);
		glDrawArrays(GL_TRIANGLE_FAN, 15, 4);
		glDrawArrays(GL_LINES, 19, 20);
		glBindVertexArray(0);
		glDisable(GL_BLEND);

		glUseProgram(0);
		// ***********************************************
	}


	if (showFlag == true)
	{
		glUseProgram(gShaderProgramObject);

		// initialize matrices to identity
		modelViewMatrix = mat4::identity();
		modelViewProjectionMatrix = mat4::identity();
		translationMatrix = mat4::identity();

		//do necessary transformation
		translationMatrix = translate(0.0f, 0.0f, -3.0f);
		modelViewMatrix = modelViewMatrix * translationMatrix;
		modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;

		//send necessary matrices to shader in respective uniforms
		glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);
		glUniform1f(alphaUniform, 1.0f);

		// ***************** FLAG ***********************
		glBindVertexArray(vao_flag);
		glDrawArrays(GL_TRIANGLE_FAN, 0, 4);
		glDrawArrays(GL_TRIANGLE_FAN, 4, 4);
		glDrawArrays(GL_TRIANGLE_FAN, 8, 4);
		//unbind vao
		glBindVertexArray(0);
		// **********************************************
	}

    glXSwapBuffers(gpDisplay, gWindow);
    update();
}

void uninitialize(void)
{if (vbo_color_i1)
	{
		glDeleteBuffers(1, &vbo_color_i1);
		vbo_color_i1 = 0;
	}
	if (vbo_position_i1)
	{
		glDeleteBuffers(1, &vbo_position_i1);
		vbo_position_i1 = 0;
	}
	if (vao_i1)
	{
		glDeleteVertexArrays(1, &vao_i1);
		vao_i1 = 0;
	}
	if (vbo_color_n)
	{
		glDeleteBuffers(1, &vbo_color_n);
		vbo_color_n = 0;
	}
	if (vbo_position_n)
	{
		glDeleteBuffers(1, &vbo_position_n);
		vbo_position_n = 0;
	}
	if (vao_n)
	{
		glDeleteVertexArrays(1, &vao_n);
		vao_n = 0;
	}

	if (vbo_color_d)
	{
		glDeleteBuffers(1, &vbo_color_d);
		vbo_color_d = 0;
	}
	if (vbo_position_d)
	{
		glDeleteBuffers(1, &vbo_position_d);
		vbo_position_d = 0;
	}
	if (vao_d)
	{
		glDeleteVertexArrays(1, &vao_d);
		vao_d = 0;
	}

	if (vbo_color_i2)
	{
		glDeleteBuffers(1, &vbo_color_i2);
		vbo_color_i2 = 0;
	}
	if (vbo_position_i2)
	{
		glDeleteBuffers(1, &vbo_position_i2);
		vbo_position_i2 = 0;
	}
	if (vao_i2)
	{
		glDeleteVertexArrays(1, &vao_i2);
		vao_i2 = 0;
	}

	if (vbo_color_a)
	{
		glDeleteBuffers(1, &vbo_color_a);
		vbo_color_a = 0;
	}
	if (vbo_position_a)
	{
		glDeleteBuffers(1, &vbo_position_a);
		vbo_position_a = 0;
	}
	if (vao_a)
	{
		glDeleteVertexArrays(1, &vao_a);
		vao_a = 0;
	}

	if (vbo_color_flag)
	{
		glDeleteBuffers(1, &vbo_color_flag);
		vbo_color_flag = 0;
	}
	if (vbo_position_flag)
	{
		glDeleteBuffers(1, &vbo_position_flag);
		vbo_position_flag = 0;
	}
	if (vao_flag)
	{
		glDeleteVertexArrays(1, &vao_flag);
		vao_flag = 0;
	}

    if(gShaderProgramObject)
    {
        GLsizei shaderCount;
        GLsizei shaderNumber;

        glUseProgram(gShaderProgramObject);
        glGetProgramiv(gShaderProgramObject, GL_ATTACHED_SHADERS, &shaderCount);
        GLuint *pShaders = (GLuint *)malloc(sizeof(GLuint) * shaderCount);

        if(pShaders)
        {
            glGetAttachedShaders(gShaderProgramObject, shaderCount, &shaderCount, pShaders);
            for(shaderNumber = 0; shaderNumber < shaderCount; shaderNumber++)
            {
                // Detach all shaaders one by one
                glDetachShader(gShaderProgramObject, pShaders[shaderNumber]);
                // Delete the detached shader
                glDeleteShader(pShaders[shaderNumber]);
                pShaders[shaderNumber] = 0;
            }
            free(pShaders);
        }
        glDeleteProgram(gShaderProgramObject);
        gShaderProgramObject = 0;
        glUseProgram(0);
    }

	if(shaderProgramObject_part2)
    {
        GLsizei shaderCount;
        GLsizei shaderNumber;

        glUseProgram(shaderProgramObject_part2);
        glGetProgramiv(shaderProgramObject_part2, GL_ATTACHED_SHADERS, &shaderCount);
        GLuint *pShaders = (GLuint *)malloc(sizeof(GLuint) * shaderCount);

        if(pShaders)
        {
            glGetAttachedShaders(shaderProgramObject_part2, shaderCount, &shaderCount, pShaders);
            for(shaderNumber = 0; shaderNumber < shaderCount; shaderNumber++)
            {
                // Detach all shaaders one by one
                glDetachShader(shaderProgramObject_part2, pShaders[shaderNumber]);
                // Delete the detached shader
                glDeleteShader(pShaders[shaderNumber]);
                pShaders[shaderNumber] = 0;
            }
            free(pShaders);
        }
        glDeleteProgram(shaderProgramObject_part2);
        shaderProgramObject_part2 = 0;
        glUseProgram(0);
    }

    if(gWindow)
    {
        XDestroyWindow(gpDisplay,gWindow);
    }
    if(gColormap)
    {
        XFreeColormap(gpDisplay, gColormap);
    }
    if(gpXVisualInfo)
    {
        free(gpXVisualInfo);
        gpXVisualInfo = NULL;
    }
    if(gpDisplay)
    {
        XCloseDisplay(gpDisplay);
        gpDisplay = NULL;
    }
}


