#include<iostream>
#include<stdio.h>
#include<stdlib.h>
#include<memory.h>

#include<X11/Xlib.h>
#include<X11/Xutil.h>
#include<X11/XKBlib.h>
#include<X11/keysym.h>

#include<GL/glew.h>
#include<GL/gl.h>
#include<GL/glx.h>

#include"vmath.h"
#include"Sphere.h"

//namespaces
using namespace std;
using namespace vmath;

//global variable declarations
bool bFullScreen = false;
Display *gpDisplay = NULL;
XVisualInfo *gpXVisualInfo = NULL;
Colormap gColormap;
Window gWindow;
int giWindowWidth = 800;
int giWindowHeight = 600;
int gNumVertices;
int gNumElements;

typedef GLXContext(*glxCreateContextAttribsARBProc)(Display*, GLXFBConfig, GLXContext, Bool, const int*);
glxCreateContextAttribsARBProc glxCreateContextAttribsARB = NULL;
GLXFBConfig gGLXFBConfig;
GLXContext gGLXContext;


GLuint shaderProgramObject_v;
GLuint shaderProgramObject_f;

GLuint vao_sphere;
GLuint vbo_sphere_position;
GLuint vbo_sphere_normal;
GLuint vbo_sphere_elements;

// Uniforms for per Vertex Lightings
GLuint mUniform_v;
GLuint vUniform_v;
GLuint pUniform_v;

// blue light
GLuint blue_ldUniform_v;
GLuint blue_laUniform_v;
GLuint blue_lsUniform_v;
GLuint blue_lightPositionUniform_v;

// green light
GLuint green_ldUniform_v;
GLuint green_laUniform_v;
GLuint green_lsUniform_v;
GLuint green_lightPositionUniform_v;

//red light
GLuint red_ldUniform_v;
GLuint red_laUniform_v;
GLuint red_lsUniform_v;
GLuint red_lightPositionUniform_v;

// material
GLuint material_kdUniform_v;
GLuint material_kaUniform_v;
GLuint material_ksUniform_v;
GLuint materialShininessUniform_v;
GLuint keyPressedUniform_v;

// Uniforms for per fragment lighting
GLuint mUniform_f;
GLuint vUniform_f;
GLuint pUniform_f;

//red light
GLuint red_ldUniform_f;
GLuint red_laUniform_f;
GLuint red_lsUniform_f;
GLuint red_lightPositionUniform_f;

//green light
GLuint green_ldUniform_f;
GLuint green_laUniform_f;
GLuint green_lsUniform_f;
GLuint green_lightPositionUniform_f;

//blue light
GLuint blue_ldUniform_f;
GLuint blue_laUniform_f;
GLuint blue_lsUniform_f;
GLuint blue_lightPositionUniform_f;

//material
GLuint material_kdUniform_f;
GLuint material_kaUniform_f;
GLuint material_ksUniform_f;
GLuint materialShininessUniform_f;
GLuint keyPressedUniform_f;

// Light and material property values
float red_lightDiffuse[4]   = {1.0f, 0.0f, 0.0f, 1.0f};
float red_lightAmbient[4]   = {0.0f, 0.0f, 0.0f, 0.0f};
float red_lightSpecular[4]  = {1.0f, 0.0f, 0.0f, 1.0f};
float red_lightPosition[4]  = {0.0f, 0.0f, 10.0f, 1.0f};

float green_lightDiffuse[4]  = {0.0f, 1.0f, 0.0f, 1.0f};
float green_lightAmbient[4]  = {0.0f, 0.0f, 0.0f, 0.0f};
float green_lightSpecular[4] = {0.0f, 1.0f, 0.0f, 1.0f};
float green_lightPosition[4] = {0.0f, 0.0f, 10.0f, 1.0f};

float blue_lightDiffuse[4]  = {0.0f, 0.0f, 1.0f, 1.0f};
float blue_lightAmbient[4]  = {0.0f, 0.0f, 0.0f, 0.0f};
float blue_lightSpecular[4] = {0.0f, 1.0f, 1.0f, 1.0f};
float blue_lightPosition[4] = {0.0f, 0.0f, 10.0f, 1.0f};

float materialDiffuse[4]    = {1.0f, 1.0f, 1.0f, 1.0f};
float materialAmbient[4]    = {0.0f, 0.0f, 0.0f, 0.0f};
float materialSpecular[4]   = {1.0f, 1.0f, 1.0f, 1.0f};
float materialShininess     = 128.0f;

float theta;

float sphere_vertices[1146];
float sphere_normals[1146];
float sphere_textures[764];
unsigned short sphere_elements[2280];

bool startAnimation = false;
bool enableLighting = false;
bool toggleLightingType = false;

mat4 perspectiveProjectionMatrix;

enum 
{
    AMC_ATTRIBUTE_POSITION = 0,
    AMC_ATTRIBUTE_COLOR,
    AMC_ATTRIBUTE_NORMAL,
    AMC_ATTRIBUTE_TEXCOORD0
};

//entry point function
int main(void)
{
    //function prototype
    void CreateWindow(void);
    void ToggleFullScreen(void);
    void uninitialize();
    void initialize(void);
    void resize(int,int);
    void display(void);
    void update(void);

    //variable declarations
    int winWidth = giWindowWidth;
    int winHeight = giWindowHeight;
    bool bDone = false;

    //code
    CreateWindow();
    
    //initialize
    initialize();

    //Message Loop
    XEvent event;
    KeySym keysym;

    while(bDone == false)
    {
        while(XPending(gpDisplay))
        {
            XNextEvent(gpDisplay,&event);
            switch(event.type)
            {
                case MapNotify:
                    break;
                case KeyPress:
                    keysym = XkbKeycodeToKeysym(gpDisplay, event.xkey.keycode, 0, 0);
                    switch(keysym)
                    {
                        case XK_Escape:
                            bDone = true;
                            break;
                        case XK_F:
                        case XK_f:
                            if(bFullScreen == false)
                            {
                                ToggleFullScreen();
                                bFullScreen = true;
                            }
                            else
                            {
                                ToggleFullScreen();
                                bFullScreen = false;
                            }
                            break;
                        case XK_A:
                        case XK_a:
                            if(startAnimation == false)
                            {
                                startAnimation= true;
                            }
                            else
                            {
                                startAnimation = false;
                            }
                            break;
                        case XK_L:
                        case XK_l:
                            if(enableLighting == false)
                            {
                                enableLighting = true;
                            }
                            else
                            {
                                enableLighting = false;
                            }
                            break;
                        case XK_t:
                        case XK_T:
                            if(toggleLightingType == false)
                            {
                                toggleLightingType = true;
                            }
                            else
                            {
                                toggleLightingType = false;
                            }
                            break;
                       default:
                            break;
                    }
                    break;
                case ButtonPress:
                   switch(event.xbutton.button)
                   {
                        case 1:                         //left button
                            break;
                        case 2:                         //middle button
                            break;
                        case 3:                         //right button
                            break;
                        default:
                            break;
                   }
                   break;
                case MotionNotify:
                   break;
                case ConfigureNotify:
                    winWidth = event.xconfigure.width;
                    winHeight =event.xconfigure.height;
                    resize(winWidth, winHeight);
                    break;
                case Expose:
                    break;
                case DestroyNotify:
                    break;
                case 33:                                //to handle close button and close menu
                    bDone = true;
                    break;
                default:
                    break;
            }
        }
        update();
        display();
    }
    uninitialize();
    return(0);
}

void CreateWindow(void)
{
    //function prototype
    void uninitialize(void);

    //variable declarations
    XSetWindowAttributes winAttribs;
    int defaultScreen;
    int defaultDepth;
    int styleMask;
    int iNumberOfFBConfigs = 0;					// number of FBConfigs
    GLXFBConfig *pGLXFBConfigs = NULL;			//array of FBConfigs
    GLXFBConfig bestGLXFBConfig;				//best FBConfig among all
    XVisualInfo *pTempXVisualInfo = NULL;		//temporary visual info

    // Initialize Frame Buffer Attributes
    static int frameBufferAttributes[] = {
        GLX_X_RENDERABLE, True,
        GLX_DRAWABLE_TYPE,GLX_WINDOW_BIT,
        GLX_RENDER_TYPE, GLX_RGBA_BIT,
        GLX_X_VISUAL_TYPE, GLX_TRUE_COLOR,
        GLX_DOUBLEBUFFER, True,
        GLX_RED_SIZE, 8,
        GLX_GREEN_SIZE , 8,
        GLX_BLUE_SIZE, 8,
        GLX_ALPHA_SIZE, 8,
        GLX_DEPTH_SIZE, 24,
        GLX_STENCIL_SIZE, 8,
        None};

    //code
    gpDisplay = XOpenDisplay(NULL);
    if(!gpDisplay)
    {
        printf("ERROR : Unable To Open X Display.\nExitting Now ...\n");
        uninitialize();
        exit(1);
    }

    defaultScreen = XDefaultScreen(gpDisplay);

	// Retrive All FBConfigs Driver Has
	pGLXFBConfigs = glXChooseFBConfig(gpDisplay, defaultScreen,	frameBufferAttributes, &iNumberOfFBConfigs);

	printf("Number Of FBConfigs Matching To FBConfig = %d\n", iNumberOfFBConfigs);
	// Now We Have Multiple FBConfigs, We Need To Find Best Out Of Them

	int bestFrameBufferConfig = -1;
	int bestNumberOfSamples   = -1;
	int worstFrameBufferConfig = -1;
	int worstNumberOfSamples  = 999;

	//loop to check all fb configs
	for(int i = 0; i < iNumberOfFBConfigs; i++)
	{
		// Getting visual from each obtained FBConfig
		pTempXVisualInfo = glXGetVisualFromFBConfig(gpDisplay, pGLXFBConfigs[i]);

		if(pTempXVisualInfo)
		{
			//get Number of samplebuffer from respective fbConfig
			int sampleBuffers, samples;
			glXGetFBConfigAttrib(gpDisplay,
				pGLXFBConfigs[i],
				GLX_SAMPLE_BUFFERS,
				&sampleBuffers);

			//get number of samples from respective FBConfig
			glXGetFBConfigAttrib(gpDisplay, pGLXFBConfigs[i], GLX_SAMPLES, &samples);

			//More the number of samples and sample buffer more the eligible config buffer is
			if(bestFrameBufferConfig < 0 || sampleBuffers && samples >bestNumberOfSamples)
			{
				bestFrameBufferConfig=i;
				bestNumberOfSamples =samples;
			}

			if(worstFrameBufferConfig < 0 || !sampleBuffers || samples < worstNumberOfSamples)
			{
				worstFrameBufferConfig = i;
				worstNumberOfSamples = samples;
			}
		}

		XFree(pTempXVisualInfo);
	}

	// Now assign the found best one
	bestGLXFBConfig = pGLXFBConfigs[bestFrameBufferConfig];

	//Now assign the local best to global best
	gGLXFBConfig = bestGLXFBConfig;

	//Free the obtaind GLXFBConfig array;
	XFree(pGLXFBConfigs);

	//Accordingly get best visual
	gpXVisualInfo = glXGetVisualFromFBConfig(gpDisplay,  bestGLXFBConfig);

    winAttribs.border_pixel = 0;
    winAttribs.background_pixmap = 0;
    winAttribs.colormap = XCreateColormap(gpDisplay, 
                                          RootWindow(gpDisplay, gpXVisualInfo->screen),
                                          gpXVisualInfo->visual,
                                          AllocNone);
    gColormap = winAttribs.colormap;

    winAttribs.background_pixel = BlackPixel(gpDisplay,defaultScreen);

    winAttribs.event_mask = ExposureMask | VisibilityChangeMask | ButtonPressMask |
                           KeyPressMask | PointerMotionMask | StructureNotifyMask;
   
    styleMask = CWBorderPixel | CWBackPixel | CWEventMask | CWColormap;

    gWindow = XCreateWindow(gpDisplay,
                            RootWindow(gpDisplay,gpXVisualInfo->screen),
                            0,
                            0,
                            giWindowWidth,
                            giWindowHeight,
                            0,                                              // border width
                            gpXVisualInfo->depth,                           // color depth
                            InputOutput,
                            gpXVisualInfo->visual,
                            styleMask,
                            &winAttribs);
    if(!gWindow)
    {
        printf("ERROR : Failed To Create Main Window.\nExitting Now ...\n");
        uninitialize();
        exit(1);
    }

    XStoreName(gpDisplay, gWindow, "Diffuse Light On Sphere");

    Atom windowManagerDelete = XInternAtom(gpDisplay, "WM_DELETE_WINDOW", True);
    XSetWMProtocols(gpDisplay, gWindow, &windowManagerDelete, 1);

    XMapWindow(gpDisplay, gWindow);
}

void ToggleFullScreen(void)
{
    //variable declarations
    Atom  wm_state;
    Atom fullscreen;
    XEvent xev = {0};

    //code
    wm_state = XInternAtom(gpDisplay, "_NET_WM_STATE", False);
    memset(&xev,0,sizeof(xev));

    xev.type = ClientMessage;
    xev.xclient.window = gWindow;
    xev.xclient.message_type = wm_state;
    xev.xclient.format = 32;
    xev.xclient.data.l[0] = bFullScreen ? 0 : 1;

    fullscreen = XInternAtom(gpDisplay, "_NET_WM_STATE_FULLSCREEN", False);
    xev.xclient.data.l[1] = fullscreen;
    
    XSendEvent(gpDisplay,
               RootWindow(gpDisplay, gpXVisualInfo->screen),
               False,
               StructureNotifyMask,
               &xev);
}

void uninitialize(void)
{
	if(vbo_sphere_elements)
    {
        glDeleteBuffers(1, &vbo_sphere_elements);
        vbo_sphere_elements = 0;
    }

    if(vbo_sphere_normal)
    {
        glDeleteBuffers(1, &vbo_sphere_normal);
        vbo_sphere_normal = 0;
    }

    if (vbo_sphere_position)
    {
        glDeleteBuffers(1, &vbo_sphere_position);
        vbo_sphere_position = 0;
    }

    if (vao_sphere)
    {
        glDeleteVertexArrays(1, &vao_sphere);
        vao_sphere = 0;
    }

    if(shaderProgramObject_f)
    {
        GLsizei shaderCount;
        GLsizei shaderNumber;

        glUseProgram(shaderProgramObject_f);
        glGetProgramiv(shaderProgramObject_f, GL_ATTACHED_SHADERS, &shaderCount);
        GLuint *pShaders = (GLuint *)malloc(sizeof(GLuint) * shaderCount);

        if(pShaders)
        {
            glGetAttachedShaders(shaderProgramObject_f, shaderCount, &shaderCount, pShaders);
            for(shaderNumber = 0; shaderNumber < shaderCount; shaderNumber++)
            {
                // Detach all shaaders one by one
                glDetachShader(shaderProgramObject_f, pShaders[shaderNumber]);
                // Delete the detached shader
                glDeleteShader(pShaders[shaderNumber]);
                pShaders[shaderNumber] = 0;
            }
            free(pShaders);
        }
        glDeleteProgram(shaderProgramObject_f);
        shaderProgramObject_f = 0;
        glUseProgram(0);
    }

    // uninitialize shader program object of per vertex lighting
    if(shaderProgramObject_v)
    {
        GLsizei shaderCount;
        GLsizei shaderNumber;

        glUseProgram(shaderProgramObject_v);
        glGetProgramiv(shaderProgramObject_v, GL_ATTACHED_SHADERS, &shaderCount);
        GLuint *pShaders = (GLuint *)malloc(sizeof(GLuint) * shaderCount);

        if(pShaders)
        {
            glGetAttachedShaders(shaderProgramObject_v, shaderCount, &shaderCount, pShaders);
            for(shaderNumber = 0; shaderNumber < shaderCount; shaderNumber++)
            {
                // Detach all shaaders one by one
                glDetachShader(shaderProgramObject_v, pShaders[shaderNumber]);
                // Delete the detached shader
                glDeleteShader(pShaders[shaderNumber]);
                pShaders[shaderNumber] = 0;
            }
            free(pShaders);
        }
        glDeleteProgram(shaderProgramObject_v);
        shaderProgramObject_f = 0;
        glUseProgram(0);
    }
    if(gWindow)
    {
        XDestroyWindow(gpDisplay,gWindow);
    }
    if(gColormap)
    {
        XFreeColormap(gpDisplay, gColormap);
    }
    if(gpXVisualInfo)
    {
        free(gpXVisualInfo);
        gpXVisualInfo = NULL;
    }
    if(gpDisplay)
    {
        XCloseDisplay(gpDisplay);
        gpDisplay = NULL;
    }
}

//initialize() function
void initialize(void)
{
    //variable declaration
    GLenum result;
    GLint iShaderCompileStatus = 0;
    GLint iProgramLinkStatus   = 0;
    GLint iInfoLogLength       = 0;
    GLchar *szInfoLog          = NULL;
    
    GLuint vertexShaderObject_v;
    GLuint fragmentShaderObject_v;

    GLuint vertexShaderObject_f;
    GLuint fragmentShaderObject_f;

    glxCreateContextAttribsARB = (glxCreateContextAttribsARBProc)glXGetProcAddressARB((GLubyte *)"glXCreateContextAttribsARB");
    if(glxCreateContextAttribsARB == NULL)
    {
    	printf("Address not found for glXCreateContextAttribsARB\n");
    	uninitialize();
    }

     const int attribs[] = {
    	GLX_CONTEXT_MAJOR_VERSION_ARB, 4,
    	GLX_CONTEXT_MINOR_VERSION_ARB, 4,
    	GLX_CONTEXT_PROFILE_MASK_ARB, GLX_CONTEXT_CORE_PROFILE_BIT_ARB,
    	None
    };

    gGLXContext = glxCreateContextAttribsARB(gpDisplay, gGLXFBConfig, 0, True, attribs);

   

    if(!gGLXContext)
    {
    	//if obtained one is not highest one, specify the lowest one it will give you highest possible for it
    	const int glattribs[]={
    		GLX_CONTEXT_MAJOR_VERSION_ARB, 1,
    		GLX_CONTEXT_MINOR_VERSION_ARB, 0,
    		None
    	};

    	gGLXContext = glxCreateContextAttribsARB(gpDisplay, gGLXFBConfig, 0, True, glattribs);
    }

    //Check whether the obtained context is really a hardware rendering context or not
    if(!glXIsDirect(gpDisplay, gGLXContext))
    {
    	printf("The Obtained Context Is Not Hardware Rendering Context\n");
    }
    else
    {
    	printf("The Obtained Context Is Hardware Rendering Context\n");
    }

    //Make the Obtained Context as Current Rendering Context
    glXMakeCurrent(gpDisplay, gWindow, gGLXContext);

    // Initialize GLEW
    result = glewInit();
    if(result != GLEW_OK)
    {
        printf("glewInit() failed");
        uninitialize();
    }

    //----------------------------------------------------------------------------------------
    //******************************* PER FRAGMENT LIGHTING **********************************
    //----------------------------------------------------------------------------------------

    // Define Vertex Shader Object
    vertexShaderObject_f = glCreateShader(GL_VERTEX_SHADER);
    
    // Write Vertex Shader Code
    const GLchar *vertexShaderSourceCode_f =
    "	#version 430														                " \
    "																	                \n  " \
    "	in vec4 vPosition;													                " \
    "   in vec3 vNormal;                                                                    " \
    "																	                \n  " \
    "   out vec3 tNorm;                                                                     " \
    "   out vec3 viewerVector;                                                              " \
    "   out vec3 redlightDirection;                                                            " \
    "   out vec3 greenlightDirection;                                                            " \
    "   out vec3 bluelightDirection;                                                            " \
    "                                                                                   \n  " \
    "	uniform mat4 u_m_matrix;											                " \
    "	uniform mat4 u_v_matrix;											                " \
    "	uniform mat4 u_p_matrix;											                " \
    "                                                                                   \n  " \
    "   uniform vec4 u_red_light_position;                                                      " \
    "   uniform vec4 u_green_light_position;                                                      " \
    "   uniform vec4 u_blue_light_position;                                                      " \
    "   uniform int u_enable_lighting;                                                      " \
    "																	                \n  " \
    "	void main(void)														                " \
    "	{																	                " \
    "       if(u_enable_lighting == 1)                                                      " \
    "       {                                                                               " \
    "           vec4 eyeCoordinates = u_v_matrix * u_m_matrix * vPosition;                       " \
    "           tNorm = mat3(u_v_matrix * u_m_matrix) * vNormal;                            " \
    "           viewerVector = vec3(-eyeCoordinates);                                       " \
    "                                                                                   \n  " \
    "           redlightDirection = vec3(u_red_light_position - eyeCoordinates);                   " \
    "           greenlightDirection = vec3(u_green_light_position - eyeCoordinates);                   " \
    "           bluelightDirection = vec3(u_blue_light_position - eyeCoordinates);                   " \
    "       }                                                                               " \
    "		gl_Position = u_p_matrix * u_v_matrix * u_m_matrix * vPosition;	                " \
    "	}																	                ";

    // Specify Above Source Code To The Verex Shader Object
    glShaderSource(vertexShaderObject_f, 1, (const GLchar **)&vertexShaderSourceCode_f, NULL);

    // Compile Vertex Shader
    glCompileShader(vertexShaderObject_f);
    
    // Check Compilation Status
    glGetShaderiv(vertexShaderObject_f, GL_COMPILE_STATUS, &iShaderCompileStatus);

    //Error Checking
    if(iShaderCompileStatus == GL_FALSE)
    {
        glGetShaderiv(vertexShaderObject_f, GL_INFO_LOG_LENGTH, &iInfoLogLength);
        if(iInfoLogLength > 0)
        {
            szInfoLog = (GLchar *)malloc(iInfoLogLength);
            if(szInfoLog != NULL)
            {
                GLsizei written;
                glGetShaderInfoLog(vertexShaderObject_f, iInfoLogLength, &written, szInfoLog);
                printf("VERTEX SHADER COMPILATION LOG FOR PER FRAGMENT LIGHTING : %s\n", szInfoLog);
                free(szInfoLog);
                uninitialize();
            }
        }
    }

    // Define Fragment Shader Object
    fragmentShaderObject_f = glCreateShader(GL_FRAGMENT_SHADER);

    // Write fragment shader source code
    const GLchar *fragmentShaderSourceCode_f =
    "	#version 430											                               	                            " \
    "															                                                        \n  " \
    "   in vec3 redlightDirection;                                                                                          " \
    "   in vec3 greenlightDirection;                                                                                        " \
    "   in vec3 bluelightDirection;                                                                                         " \
    "   in vec3 viewerVector;                                                                                               " \
    "   in vec3 tNorm;                                                                                                      " \
    "															                                                        \n  " \
    "	out vec4 FragColor;										                             	                            " \
    "                                                                                                                   \n  " \
    "   uniform vec3 u_red_la;                                                                                          \n  " \
    "   uniform vec3 u_red_ld;                                                                                          \n  " \
    "   uniform vec3 u_red_ls;                                                                                          \n  " \
    "                                                                                                                   \n  " \
    "   uniform vec3 u_green_la;                                                                                        \n  " \
    "   uniform vec3 u_green_ld;                                                                                        \n  " \
    "   uniform vec3 u_green_ls;                                                                                        \n  " \
    "                                                                                                                   \n  " \
    "   uniform vec3 u_blue_la;                                                                                         \n  " \
    "   uniform vec3 u_blue_ld;                                                                                         \n  " \
    "   uniform vec3 u_blue_ls;                                                                                         \n  " \
    "                                                                                                                   \n  " \
    "   uniform vec3 u_ka;                                                                                                  " \
    "   uniform vec3 u_kd;                                                                                                  " \
    "   uniform vec3 u_ks;                                                                                                  " \
    "                                                                                                                   \n  " \
    "   uniform float u_materialShininess;                                                                                  " \
    "   uniform int u_enable_lighting;                                                                                      " \
    "															                                                        \n  " \
    "	void main(void)											                                                        	" \
    "	{														                               	                            " \
    "       vec3 phong_ads_lights;                                                                                          " \
    "                                                                                                                   \n  " \
    "       if(u_enable_lighting == 1)                                                                                      " \
    "       {                                                                                                               " \
    "           vec3 normalized_tNorm = normalize(tNorm);                                                                   " \
    "           vec3 normalized_viewerVector = normalize(viewerVector);                                                     " \
    "           vec3 normalized_RedLightDirection = normalize(redlightDirection);                                           " \
    "           vec3 normalized_GreenLightDirection = normalize(greenlightDirection);                                            " \
    "           vec3 normalized_BlueLightDirection = normalize(bluelightDirection);                                              " \
    "                                                                                                                   \n  " \
    "           vec3 redLightReflectionVector = normalize(reflect(-normalized_RedLightDirection, normalized_tNorm));         " \
    "           vec3 greenLightReflectionVector = normalize(reflect(-normalized_GreenLightDirection, normalized_tNorm));     " \
    "           vec3 blueLightReflectionVector = normalize(reflect(-normalized_BlueLightDirection, normalized_tNorm));       " \
    "                                                                                                                   \n  " \
    "           float red_tnDotld = max(dot(normalized_RedLightDirection, normalized_tNorm), 0.0);                           " \
    "           float red_rvDotvv = max(dot(redLightReflectionVector, normalized_viewerVector), 0.0);                                " \
    "           float green_tnDotld = max(dot(normalized_GreenLightDirection, normalized_tNorm), 0.0);                       " \
    "           float green_rvDotvv = max(dot(greenLightReflectionVector, normalized_viewerVector), 0.0);                              " \
    "           float blue_tnDotld = max(dot(normalized_BlueLightDirection, normalized_tNorm), 0.0);                         " \
    "           float blue_rvDotvv = max(dot(blueLightReflectionVector, normalized_viewerVector), 0.0);                               " \
    "                                                                                                                   \n  " \
    "           vec3 red_ambient = u_red_la * u_ka;                                                                                 " \
    "           vec3 red_diffuse = u_red_ld * u_kd * red_tnDotld;                                                                       " \
    "           vec3 red_specular = u_red_ls * u_ks * pow(red_rvDotvv, u_materialShininess);                                            " \
    "                                                                                                                   \n  " \
    "           vec3 green_ambient = u_green_la * u_ka;                                                                                 " \
    "           vec3 green_diffuse = u_green_ld * u_kd * green_tnDotld;                                                                       " \
    "           vec3 green_specular = u_green_ls * u_ks * pow(green_rvDotvv, u_materialShininess);                                            " \
    "                                                                                                                   \n  " \
    "           vec3 blue_ambient = u_blue_la * u_ka;                                                                                 " \
    "           vec3 blue_diffuse = u_blue_ld * u_kd * blue_tnDotld;                                                                       " \
    "           vec3 blue_specular = u_blue_ls * u_ks * pow(blue_rvDotvv, u_materialShininess);                                            " \
    "                                                                                                                   \n  " \
    "           vec3 Ambient = red_ambient + green_ambient + blue_ambient;                                              \n  " \
    "           vec3 Diffuse = red_diffuse + green_diffuse + blue_diffuse;                                            \n  " \
    "           vec3 Specular = red_specular + green_specular + blue_specular;                                          \n  " \
    "                                                                                                                   \n  " \
    "           phong_ads_lights =Ambient + Diffuse + Specular;                                                           " \
    "       }                                                                                                               " \
    "       else                                                                                                            " \
    "       {                                                                                                               " \
    "           phong_ads_lights    = vec3(1.0, 0.0, 1.0);                                                                  " \
    "       }                                                                                                               " \
    "       FragColor = vec4(phong_ads_lights, 1.0);                                                                        " \
    "	}															                                                        ";

    // Specify above source code to fragment shader object
    glShaderSource(fragmentShaderObject_f, 1, (const GLchar **)&fragmentShaderSourceCode_f, NULL);

    // Compile fragment shader
    iShaderCompileStatus = 0;
    iInfoLogLength =0;
    szInfoLog = NULL;

    glCompileShader(fragmentShaderObject_f);
    glGetShaderiv(fragmentShaderObject_f, GL_COMPILE_STATUS, &iShaderCompileStatus);

    if(iShaderCompileStatus == GL_FALSE)
    {
        glGetShaderiv(fragmentShaderObject_f, GL_INFO_LOG_LENGTH, &iInfoLogLength);
        if(iInfoLogLength > 0)
        {
            szInfoLog = (GLchar *)malloc(iInfoLogLength);
            if (szInfoLog != NULL)
            {
                GLsizei written;
                glGetShaderInfoLog(fragmentShaderObject_f, iInfoLogLength, &written, szInfoLog);
                printf("FRAGMENT SHADER COMPILATION LOG FOR PER FRAGMENT LIGHTING: %s\n", szInfoLog);
                free(szInfoLog);
                uninitialize();
            }

        }
    }

    // Create Shader Program Object
    shaderProgramObject_f = glCreateProgram();

    // Attach Vertex Shader and Fragment Shader to the Shader Program
    glAttachShader(shaderProgramObject_f, vertexShaderObject_f);
    glAttachShader(shaderProgramObject_f, fragmentShaderObject_f);

    // Pre-Linking  binding to vertex attribute
    glBindAttribLocation(shaderProgramObject_f, AMC_ATTRIBUTE_POSITION, "vPosition");
    glBindAttribLocation(shaderProgramObject_f, AMC_ATTRIBUTE_NORMAL, "vNormal");

    // Link Shader Program
    glLinkProgram(shaderProgramObject_f);

    // Error Checking
    iProgramLinkStatus = 0;
    iInfoLogLength = 0;
    szInfoLog = NULL;
    glGetProgramiv(shaderProgramObject_f, GL_LINK_STATUS, &iProgramLinkStatus);
    if(iProgramLinkStatus == GL_FALSE)
    {
        glGetProgramiv(shaderProgramObject_f, GL_INFO_LOG_LENGTH, &iInfoLogLength);
        if(iInfoLogLength > 0)
        {
            szInfoLog = (GLchar *)malloc(iInfoLogLength);
            if(szInfoLog != NULL)
            {
                GLint written;
                glGetProgramInfoLog(shaderProgramObject_f, iInfoLogLength, &written, szInfoLog);
                free(szInfoLog);
                uninitialize();
            }
        }
    }

    // Post-Linking retrieving uniform location
    // Model , View and projection matrix
    mUniform_f = glGetUniformLocation(shaderProgramObject_f, "u_m_matrix");
    vUniform_f = glGetUniformLocation(shaderProgramObject_f, "u_v_matrix");
    pUniform_f = glGetUniformLocation(shaderProgramObject_f, "u_p_matrix");
    //red light
    red_laUniform_f = glGetUniformLocation(shaderProgramObject_f, "u_red_la");
    red_ldUniform_f = glGetUniformLocation(shaderProgramObject_f, "u_red_ld");
    red_lsUniform_f = glGetUniformLocation(shaderProgramObject_f, "u_red_ls");
    // green light
    green_laUniform_f = glGetUniformLocation(shaderProgramObject_f, "u_green_la");
    green_ldUniform_f = glGetUniformLocation(shaderProgramObject_f, "u_green_ld");
    green_lsUniform_f = glGetUniformLocation(shaderProgramObject_f, "u_green_ls");
 
    // blue light
    blue_laUniform_f = glGetUniformLocation(shaderProgramObject_f, "u_blue_la");
    blue_ldUniform_f = glGetUniformLocation(shaderProgramObject_f, "u_blue_ld");
    blue_lsUniform_f = glGetUniformLocation(shaderProgramObject_f, "u_blue_ls");
 
    // light position
    red_lightPositionUniform_f   = glGetUniformLocation(shaderProgramObject_f, "u_red_light_position");
    green_lightPositionUniform_f = glGetUniformLocation(shaderProgramObject_f, "u_green_light_position");
    blue_lightPositionUniform_f  = glGetUniformLocation(shaderProgramObject_f, "u_blue_light_position");

    // material
    material_kaUniform_f       = glGetUniformLocation(shaderProgramObject_f, "u_ka");
    material_kdUniform_f       = glGetUniformLocation(shaderProgramObject_f, "u_kd");
    material_ksUniform_f       = glGetUniformLocation(shaderProgramObject_f, "u_ks");
    materialShininessUniform_f = glGetUniformLocation(shaderProgramObject_f, "u_materialShininess");

    keyPressedUniform_f = glGetUniformLocation(shaderProgramObject_f, "u_enable_lighting");


    //-----------------------------------------------------------------------------------------------
    //******************************** PER VERTEX LIGHTING ******************************************
    //-----------------------------------------------------------------------------------------------
    // Define Vertex Shader Object
    vertexShaderObject_v = glCreateShader(GL_VERTEX_SHADER);
    
    // Write Vertex Shader Code
    const GLchar *vertexShaderSourceCode_v =
    "   #version 430                                                                        " \
    "                                                                                   \n  " \
    "   in vec4 vPosition;                                                                  " \
    "   in vec3 vNormal;                                                                    " \
    "                                                                                   \n  " \
    "   out vec3 phong_ads_lights;                                                          " \
    "                                                                                   \n  " \
    "   uniform mat4 u_m_matrix;                                                            " \
    "   uniform mat4 u_v_matrix;                                                            " \
    "   uniform mat4 u_p_matrix;                                                            " \
    "                                                                                   \n  " \
    "   uniform vec3 u_red_la;                                                                                          \n  " \
    "   uniform vec3 u_red_ld;                                                                                          \n  " \
    "   uniform vec3 u_red_ls;                                                                                          \n  " \
    "                                                                                                                   \n  " \
    "   uniform vec3 u_green_la;                                                                                        \n  " \
    "   uniform vec3 u_green_ld;                                                                                        \n  " \
    "   uniform vec3 u_green_ls;                                                                                        \n  " \
    "                                                                                                                   \n  " \
    "   uniform vec3 u_blue_la;                                                                                         \n  " \
    "   uniform vec3 u_blue_ld;                                                                                         \n  " \
    "   uniform vec3 u_blue_ls;                                                                                         \n  " \
    "                                                                                                                   \n  " \
    "   uniform vec3 u_ka;                                                                                                  " \
    "   uniform vec3 u_kd;                                                                                                  " \
    "   uniform vec3 u_ks;                                                                                                  " \
    "   uniform float u_shininess;                                                          " \
    "                                                                                                                   \n  " \
    "   uniform vec4 u_blue_light_position;                                                         " \
    "   uniform vec4 u_red_light_position;                                                          " \
    "   uniform vec4 u_green_light_position;                                                        " \
    "   uniform int u_enable_lighting;                                                                                      " \
    "                                                                                   \n  " \
    "   void main(void)                                                                     " \
    "   {                                                                                   " \
    "       if(u_enable_lighting == 1)                                                      " \
    "       {                                                                               " \
    "           vec4 eyeCoordinates = u_v_matrix * u_m_matrix * vPosition;                  " \
    "           mat3 normalMatrix   = mat3(u_v_matrix * u_m_matrix);                        " \
    "           vec3 tNorm          = normalize(normalMatrix * vNormal);                    " \
    "           vec3 viewerVector   = normalize(vec3(-eyeCoordinates));   " \
    "                                                                                   \n  " \
    "           vec3 red_lightDirection     = normalize(vec3(u_red_light_position - eyeCoordinates));   " \
    "           vec3 red_reflectionVector   = reflect(-red_lightDirection, tNorm);                    " \
    "           float red_tnDotld           = max(dot(tNorm, red_lightDirection), 0.0);                 " \
    "           float red_rvDotvv           = max(dot(red_reflectionVector, viewerVector), 0.0);        " \
    "                                                                                   \n  " \
    "           vec3 green_lightDirection   = normalize(vec3(u_green_light_position - eyeCoordinates));   " \
    "           vec3 green_reflectionVector = reflect(-green_lightDirection, tNorm);                    " \
    "           float green_tnDotld         = max(dot(tNorm, green_lightDirection), 0.0);                 " \
    "           float green_rvDotvv         = max(dot(green_reflectionVector, viewerVector), 0.0);        " \
    "                                                                                   \n  " \
    "           vec3 blue_lightDirection    = normalize(vec3(u_blue_light_position - eyeCoordinates));   " \
    "           vec3 blue_reflectionVector  = reflect(-blue_lightDirection, tNorm);                    " \
    "           float blue_tnDotld          = max(dot(tNorm, blue_lightDirection), 0.0);                 " \
    "           float blue_rvDotvv          = max(dot(blue_reflectionVector, viewerVector), 0.0);        " \
    "                                                                                   \n  " \
    "           vec3 red_ambient    = u_red_la * u_ka;                                          " \
    "           vec3 red_diffuse    = u_red_ld * u_kd * red_tnDotld;                                " \
    "           vec3 red_specular   = u_red_ls * u_ks * pow(red_rvDotvv, u_shininess);              " \
    "                                                                                   \n  " \
    "           vec3 green_ambient  = u_green_la * u_ka;                                          " \
    "           vec3 green_diffuse  = u_green_ld * u_kd * green_tnDotld;                                " \
    "           vec3 green_specular = u_green_ls * u_ks * pow(green_rvDotvv, u_shininess);              " \
    "                                                                                   \n  " \
    "           vec3 blue_ambient   = u_blue_la * u_ka;                                          " \
    "           vec3 blue_diffuse   = u_blue_ld * u_kd * blue_tnDotld;                                " \
    "           vec3 blue_specular  = u_blue_ls * u_ks * pow(blue_rvDotvv, u_shininess);              " \
    "                                                                                   \n  " \
    "           vec3 Ambient  = blue_ambient + red_ambient + green_ambient;                            " \
    "           vec3 Diffuse  = blue_diffuse + red_diffuse + green_diffuse;                            " \
    "           vec3 Specular = blue_specular + red_specular + green_specular;                         " \
    "           phong_ads_lights   = Ambient + Diffuse + Specular;                                   " \
    "       }                                                                               " \
    "       else                                                                            " \
    "       {                                                                               " \
    "           phong_ads_lights   = vec3(1.0, 1.0, 1.0);                                   " \
    "       }                                                                               " \
    "                                                                                   \n  " \
    "       gl_Position = u_p_matrix * u_v_matrix * u_m_matrix * vPosition; " \
    "   }                                                                   ";

    // Specify Above Source Code To The Verex Shader Object
    glShaderSource(vertexShaderObject_v, 1, (const GLchar **)&vertexShaderSourceCode_v, NULL);

    // Compile Vertex Shader
    glCompileShader(vertexShaderObject_v);
    
    // Check Compilation Status
    glGetShaderiv(vertexShaderObject_v, GL_COMPILE_STATUS, &iShaderCompileStatus);

    //Error Checking
    if(iShaderCompileStatus == GL_FALSE)
    {
        glGetShaderiv(vertexShaderObject_v, GL_INFO_LOG_LENGTH, &iInfoLogLength);
        if(iInfoLogLength > 0)
        {
            szInfoLog = (GLchar *)malloc(iInfoLogLength);
            if(szInfoLog != NULL)
            {
                GLsizei written;
                glGetShaderInfoLog(vertexShaderObject_v, iInfoLogLength, &written, szInfoLog);
                printf("VERTEX SHADER COMPILATION LOG FOR PER VERTEX LIGHTING: %s\n", szInfoLog);
                free(szInfoLog);
                uninitialize();
            }
        }
    }

    // Define Fragment Shader Object
    fragmentShaderObject_v = glCreateShader(GL_FRAGMENT_SHADER);

    // Write fragment shader source code
    const GLchar *fragmentShaderSourceCode_v =
    "   #version 430                                                " \
    "                                                           \n  " \
    "   in vec3 phong_ads_lights;                                   " \
    "                                                           \n  " \
    "   out vec4 FragColor;                                         " \
    "                                                           \n  " \
    "   void main(void)                                             " \
    "   {                                                           " \
    "           FragColor = vec4(phong_ads_lights, 1.0);            " \
    "   }                                                           ";

    // Specify above source code to fragment shader object
    glShaderSource(fragmentShaderObject_v, 1, (const GLchar **)&fragmentShaderSourceCode_v, NULL);

    // Compile fragment shader
    iShaderCompileStatus = 0;
    iInfoLogLength =0;
    szInfoLog = NULL;

    glCompileShader(fragmentShaderObject_v);
    glGetShaderiv(fragmentShaderObject_v, GL_COMPILE_STATUS, &iShaderCompileStatus);

    if(iShaderCompileStatus == GL_FALSE)
    {
        glGetShaderiv(fragmentShaderObject_v, GL_INFO_LOG_LENGTH, &iInfoLogLength);
        if(iInfoLogLength > 0)
        {
            szInfoLog = (GLchar *)malloc(iInfoLogLength);
            if (szInfoLog != NULL)
            {
                GLsizei written;
                glGetShaderInfoLog(fragmentShaderObject_v, iInfoLogLength, &written, szInfoLog);
                printf("FRAGMENT SHADER COMPILATION LOG FOR PER VERTEX LIGHTING: %s\n", szInfoLog);
                free(szInfoLog);
                uninitialize();
            }

        }
    }

    // Create Shader Program Object
    shaderProgramObject_v = glCreateProgram();

    // Attach Vertex Shader and Fragment Shader to the Shader Program
    glAttachShader(shaderProgramObject_v, vertexShaderObject_v);
    glAttachShader(shaderProgramObject_v, fragmentShaderObject_v);

    // Pre-Linking  binding to vertex attribute
    glBindAttribLocation(shaderProgramObject_v, AMC_ATTRIBUTE_POSITION, "vPosition");
    glBindAttribLocation(shaderProgramObject_v, AMC_ATTRIBUTE_NORMAL, "vNormal");

    // Link Shader Program
    glLinkProgram(shaderProgramObject_v);

    // Error Checking
    iProgramLinkStatus = 0;
    iInfoLogLength = 0;
    szInfoLog = NULL;
    glGetProgramiv(shaderProgramObject_v, GL_LINK_STATUS, &iProgramLinkStatus);
    if(iProgramLinkStatus == GL_FALSE)
    {
        glGetProgramiv(shaderProgramObject_v, GL_INFO_LOG_LENGTH, &iInfoLogLength);
        if(iInfoLogLength > 0)
        {
            szInfoLog = (GLchar *)malloc(iInfoLogLength);
            if(szInfoLog != NULL)
            {
                GLint written;
                glGetProgramInfoLog(shaderProgramObject_v, iInfoLogLength, &written, szInfoLog);
                free(szInfoLog);
                uninitialize();
            }
        }
    }

    // Post-Linking retrieving uniform location
    // Model , View and projection matrix
    mUniform_v = glGetUniformLocation(shaderProgramObject_v, "u_m_matrix");
    vUniform_v = glGetUniformLocation(shaderProgramObject_v, "u_v_matrix");
    pUniform_v = glGetUniformLocation(shaderProgramObject_v, "u_p_matrix");
    //red light
    red_laUniform_v = glGetUniformLocation(shaderProgramObject_v, "u_red_la");
    red_ldUniform_v = glGetUniformLocation(shaderProgramObject_v, "u_red_ld");
    red_lsUniform_v = glGetUniformLocation(shaderProgramObject_v, "u_red_ls");
    // green light
    green_laUniform_v = glGetUniformLocation(shaderProgramObject_v, "u_green_la");
    green_ldUniform_v = glGetUniformLocation(shaderProgramObject_v, "u_green_ld");
    green_lsUniform_v = glGetUniformLocation(shaderProgramObject_v, "u_green_ls");
    // blue light
    blue_laUniform_v = glGetUniformLocation(shaderProgramObject_v, "u_blue_la");
    blue_ldUniform_v = glGetUniformLocation(shaderProgramObject_v, "u_blue_ld");
    blue_lsUniform_v = glGetUniformLocation(shaderProgramObject_v, "u_blue_ls");
    // light position
    red_lightPositionUniform_v   = glGetUniformLocation(shaderProgramObject_v, "u_red_light_position");
    green_lightPositionUniform_v = glGetUniformLocation(shaderProgramObject_v, "u_green_light_position");
    blue_lightPositionUniform_v  = glGetUniformLocation(shaderProgramObject_v, "u_blue_light_position");
    // material
    material_kaUniform_v       = glGetUniformLocation(shaderProgramObject_v, "u_ka");
    material_kdUniform_v       = glGetUniformLocation(shaderProgramObject_v, "u_kd");
    material_ksUniform_v       = glGetUniformLocation(shaderProgramObject_v, "u_ks");
    materialShininessUniform_v = glGetUniformLocation(shaderProgramObject_v, "u_shininess");

    keyPressedUniform_v = glGetUniformLocation(shaderProgramObject_v, "u_enable_lighting");
//-----------------------------------------------------------------------------------------------

    getSphereVertexData(sphere_vertices, sphere_normals, sphere_textures, sphere_elements);
    gNumVertices = getNumberOfSphereVertices();
    gNumElements = getNumberOfSphereElements();

    // ***** SPHERE *****
    glGenVertexArrays(1, &vao_sphere);
    glBindVertexArray(vao_sphere);

    // Buffer for position
    glGenBuffers(1, &vbo_sphere_position);
    glBindBuffer(GL_ARRAY_BUFFER, vbo_sphere_position);
    glBufferData(GL_ARRAY_BUFFER, sizeof(sphere_vertices), sphere_vertices, GL_STATIC_DRAW);
    glVertexAttribPointer(AMC_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
    glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    
    // Buffer for normal
    glGenBuffers(1, &vbo_sphere_normal);
    glBindBuffer(GL_ARRAY_BUFFER, vbo_sphere_normal);
    glBufferData(GL_ARRAY_BUFFER, sizeof(sphere_normals), sphere_normals, GL_STATIC_DRAW);
    glVertexAttribPointer(AMC_ATTRIBUTE_NORMAL, 3, GL_FLOAT, GL_FALSE, 0, NULL);
    glEnableVertexAttribArray(AMC_ATTRIBUTE_NORMAL);
    glBindBuffer(GL_ARRAY_BUFFER, 0);

    // Buffer for elements
    glGenBuffers(1, &vbo_sphere_elements);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_elements);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(sphere_elements), sphere_elements, GL_STATIC_DRAW);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

    // Unbind with vao
    glBindVertexArray(0);

    //glDisable(GL_CULL_FACE);
    glClearDepth(1.0f);
    glEnable(GL_DEPTH_TEST);
    glDepthFunc(GL_LEQUAL);

    glClearColor(0.225f,0.225f,0.225f,1.0f);

    perspectiveProjectionMatrix = mat4::identity();
}

//resize() function
void resize(int width, int height)
{
    if(height == 0)
        height =1;
    glViewport(0,0,(GLsizei)width, (GLsizei)height);

    perspectiveProjectionMatrix = perspective(45.0f, (GLfloat)width/(GLfloat)height, 0.1f, 100.0f);
}

// update function
void update(void)
{
    // Code
    theta = theta + 0.005f;
    if (theta > 360)
    {
        theta = 0.0f;
    }
}

//display function
void display(void)
{
    // Declaration of matrices
    mat4 modelMatrix;
    mat4 viewMatrix;
    mat4 projectionMatrix;
    mat4 translationMatrix;
    mat4 rotationMatrix;
    mat4 scaleMatrix;

    // Initialize above matrices to identity
    modelMatrix 		= mat4::identity();
    viewMatrix 			= mat4::identity();
    projectionMatrix 	= mat4::identity();
    translationMatrix 	= mat4::identity();
    scaleMatrix 		= mat4::identity();
    rotationMatrix 		= mat4::identity();

    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    // Necessary transformations
    translationMatrix 	= translate(0.0f, 0.0f, -3.0f);
    modelMatrix			= translationMatrix * scaleMatrix * rotationMatrix;
    projectionMatrix 	= perspectiveProjectionMatrix;

    if(toggleLightingType == false)
    {
        glUseProgram(shaderProgramObject_v);

        if(enableLighting == true)
        {
            blue_lightPosition[0] = 5 * cosf(theta);
            blue_lightPosition[1] = 5 * sinf(theta);
            blue_lightPosition[2] = 0.0f;

            green_lightPosition[0] = 5 * cosf(theta);
            green_lightPosition[1] = 0.0f;
            green_lightPosition[2] = 5 * sinf(theta);

            red_lightPosition[0] = 0.0f;
            red_lightPosition[1] = 5 * cosf(theta);
            red_lightPosition[2] = 5 * sinf(theta);


            glUniform1i(keyPressedUniform_v, 1);

            glUniform3fv(red_laUniform_v, 1, red_lightAmbient);
            glUniform3fv(red_ldUniform_v, 1, red_lightDiffuse);
            glUniform3fv(red_lsUniform_v, 1, red_lightSpecular);
      
            glUniform3fv(green_laUniform_v, 1, green_lightAmbient);
            glUniform3fv(green_ldUniform_v, 1, green_lightDiffuse);
            glUniform3fv(green_lsUniform_v, 1, green_lightSpecular);
      
            glUniform3fv(blue_laUniform_v, 1, blue_lightAmbient);
            glUniform3fv(blue_ldUniform_v, 1, blue_lightDiffuse);
            glUniform3fv(blue_lsUniform_v, 1, blue_lightSpecular);

            glUniform4fv(red_lightPositionUniform_v, 1, red_lightPosition);
            glUniform4fv(green_lightPositionUniform_v, 1, green_lightPosition);
            glUniform4fv(blue_lightPositionUniform_v, 1, blue_lightPosition);
      
            glUniform3fv(material_kaUniform_v, 1, materialAmbient);
            glUniform3fv(material_kdUniform_v, 1, materialDiffuse);
            glUniform3fv(material_ksUniform_v, 1, materialSpecular);
            glUniform1f(materialShininessUniform_v, materialShininess);
      
        }
        else
        {
            glUniform1i(keyPressedUniform_v, 0);
        }

        // Send matrices to shaders in respective uniforms
        glUniformMatrix4fv(mUniform_v, 1, GL_FALSE, modelMatrix);
        glUniformMatrix4fv(vUniform_v, 1, GL_FALSE, viewMatrix);
        glUniformMatrix4fv(pUniform_v, 1, GL_FALSE, projectionMatrix);

        // ***** SPHERE *****
        //-----------------------------------------------
        // Bind to vao for Sphere
        glBindVertexArray(vao_sphere);
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_elements);
        glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT,0);

        // Unbind to vao
        glBindVertexArray(0);
        //-----------------------------------------------

        // Unuse program
        glUseProgram(0);

    }
    else
    {
        glUseProgram(shaderProgramObject_f);
         if(enableLighting == true)
        {
            blue_lightPosition[0] = 5 * cosf(theta);
            blue_lightPosition[1] = 5 * sinf(theta);
            blue_lightPosition[2] = 0.0f;

            green_lightPosition[0] = 5 * cosf(theta);
            green_lightPosition[1] = 0.0f;
            green_lightPosition[2] = 5 * sinf(theta);

            red_lightPosition[0] = 0.0f;
            red_lightPosition[1] = 5 * cosf(theta);
            red_lightPosition[2] = 5 * sinf(theta);


            glUniform1i(keyPressedUniform_f, 1);

            glUniform3fv(red_laUniform_f, 1, red_lightAmbient);
            glUniform3fv(red_ldUniform_f, 1, red_lightDiffuse);
            glUniform3fv(red_lsUniform_f, 1, red_lightSpecular);
      
            glUniform3fv(green_laUniform_f, 1, green_lightAmbient);
            glUniform3fv(green_ldUniform_f, 1, green_lightDiffuse);
            glUniform3fv(green_lsUniform_f, 1, green_lightSpecular);
      
            glUniform3fv(blue_laUniform_f, 1, blue_lightAmbient);
            glUniform3fv(blue_ldUniform_f, 1, blue_lightDiffuse);
            glUniform3fv(blue_lsUniform_f, 1, blue_lightSpecular);

            glUniform4fv(red_lightPositionUniform_f, 1, red_lightPosition);
            glUniform4fv(green_lightPositionUniform_f, 1, green_lightPosition);
            glUniform4fv(blue_lightPositionUniform_f, 1, blue_lightPosition);
      
            glUniform3fv(material_kaUniform_f, 1, materialAmbient);
            glUniform3fv(material_kdUniform_f, 1, materialDiffuse);
            glUniform3fv(material_ksUniform_f, 1, materialSpecular);
            glUniform1f(materialShininessUniform_f, materialShininess);
       }
        else
        {
            glUniform1i(keyPressedUniform_f, 0);
        }

        // Send matrices to shaders in respective uniforms
        glUniformMatrix4fv(mUniform_f, 1, GL_FALSE, modelMatrix);
        glUniformMatrix4fv(vUniform_f, 1, GL_FALSE, viewMatrix);
        glUniformMatrix4fv(pUniform_f, 1, GL_FALSE, projectionMatrix);

        // ***** SPHERE *****
        //-----------------------------------------------
        // Bind to vao for Sphere
        glBindVertexArray(vao_sphere);
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_elements);
        glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT,0);

        // Unbind to vao
        glBindVertexArray(0);
        //-----------------------------------------------

        // Unuse program
        glUseProgram(0);
    }
   
    glXSwapBuffers(gpDisplay, gWindow);
}




