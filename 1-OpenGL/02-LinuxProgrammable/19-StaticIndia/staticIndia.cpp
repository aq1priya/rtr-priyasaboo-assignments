#include<iostream>
#include<stdio.h>
#include<stdlib.h>
#include<memory.h>

#include<X11/Xlib.h>
#include<X11/Xutil.h>
#include<X11/XKBlib.h>
#include<X11/keysym.h>

#include<GL/glew.h>
#include<GL/gl.h>
#include<GL/glx.h>

#include"vmath.h"

//namespaces
using namespace std;
using namespace vmath;

//global variable declarations
bool bFullScreen = false;
Display *gpDisplay = NULL;
XVisualInfo *gpXVisualInfo = NULL;
Colormap gColormap;
Window gWindow;
int giWindowWidth = 800;
int giWindowHeight = 600;

typedef GLXContext(*glxCreateContextAttribsARBProc)(Display*, GLXFBConfig, GLXContext, Bool, const int*);
glxCreateContextAttribsARBProc glxCreateContextAttribsARB = NULL;
GLXFBConfig gGLXFBConfig;
GLXContext gGLXContext;


GLuint shaderProgramObject;

GLuint vao_triangle;
GLuint vbo_triangle_position;
GLuint vao_square;
GLuint vbo_square_position;
GLuint vao_i1;          // I
GLuint vbo_color_i1;
GLuint vbo_position_i1;
GLuint vao_n;           // N
GLuint vbo_color_n;
GLuint vbo_position_n;
GLuint vao_d;           // D
GLuint vbo_color_d;
GLuint vbo_position_d;
GLuint vao_i2;          // I
GLuint vbo_color_i2;
GLuint vbo_position_i2;
GLuint vao_a;           // A
GLuint vbo_color_a;
GLuint vbo_position_a;
GLuint vao_flag;
GLuint vbo_position_flag;
GLuint vbo_color_flag;

GLuint mvpUniform;
mat4 perspectiveProjectionMatrix;

enum 
{
    AMC_ATTRIBUTE_POSITION = 0,
    AMC_ATTRIBUTE_COLOR,
    AMC_ATTRIBUTE_NORMAL,
    AMC_ATTRIBUTE_TEXCOORD0
};

//entry point function
int main(void)
{
    //function prototype
    void CreateWindow(void);
    void ToggleFullScreen(void);
    void uninitialize();
    void initialize(void);
    void resize(int,int);
    void display(void);

    //variable declarations
    int winWidth = giWindowWidth;
    int winHeight = giWindowHeight;
    bool bDone = false;

    //code
    CreateWindow();
    
    //initialize
    initialize();

    //Message Loop
    XEvent event;
    KeySym keysym;

    while(bDone == false)
    {
        while(XPending(gpDisplay))
        {
            XNextEvent(gpDisplay,&event);
            switch(event.type)
            {
                case MapNotify:
                    break;
                case KeyPress:
                    keysym = XkbKeycodeToKeysym(gpDisplay, event.xkey.keycode, 0, 0);
                    switch(keysym)
                    {
                        case XK_Escape:
                            bDone = true;
                            break;
                        case XK_F:
                        case XK_f:
                            if(bFullScreen == false)
                            {
                                ToggleFullScreen();
                                bFullScreen = true;
                            }
                            else
                            {
                                ToggleFullScreen();
                                bFullScreen = false;
                            }
                            break;
                       default:
                            break;
                    }
                    break;
                case ButtonPress:
                   switch(event.xbutton.button)
                   {
                        case 1:                         //left button
                            break;
                        case 2:                         //middle button
                            break;
                        case 3:                         //right button
                            break;
                        default:
                            break;
                   }
                   break;
                case MotionNotify:
                   break;
                case ConfigureNotify:
                    winWidth = event.xconfigure.width;
                    winHeight =event.xconfigure.height;
                    resize(winWidth, winHeight);
                    break;
                case Expose:
                    break;
                case DestroyNotify:
                    break;
                case 33:                                //to handle close button and close menu
                    bDone = true;
                    break;
                default:
                    break;
            }
        }
        display();
    }
    uninitialize();
    return(0);
}

void CreateWindow(void)
{
    //function prototype
    void uninitialize(void);

    //variable declarations
    XSetWindowAttributes winAttribs;
    int defaultScreen;
    int defaultDepth;
    int styleMask;
    int iNumberOfFBConfigs = 0;					// number of FBConfigs
    GLXFBConfig *pGLXFBConfigs = NULL;			//array of FBConfigs
    GLXFBConfig bestGLXFBConfig;				//best FBConfig among all
    XVisualInfo *pTempXVisualInfo = NULL;		//temporary visual info

    // Initialize Frame Buffer Attributes
    static int frameBufferAttributes[] = {
        GLX_X_RENDERABLE, True,
        GLX_DRAWABLE_TYPE,GLX_WINDOW_BIT,
        GLX_RENDER_TYPE, GLX_RGBA_BIT,
        GLX_X_VISUAL_TYPE, GLX_TRUE_COLOR,
        GLX_DOUBLEBUFFER, True,
        GLX_RED_SIZE, 8,
        GLX_GREEN_SIZE , 8,
        GLX_BLUE_SIZE, 8,
        GLX_ALPHA_SIZE, 8,
        GLX_DEPTH_SIZE, 24,
        GLX_STENCIL_SIZE, 8,
        None};

    //code
    gpDisplay = XOpenDisplay(NULL);
    if(!gpDisplay)
    {
        printf("ERROR : Unable To Open X Display.\nExitting Now ...\n");
        uninitialize();
        exit(1);
    }

    defaultScreen = XDefaultScreen(gpDisplay);

	// Retrive All FBConfigs Driver Has
	pGLXFBConfigs = glXChooseFBConfig(gpDisplay, defaultScreen,	frameBufferAttributes, &iNumberOfFBConfigs);

	printf("Number Of FBConfigs Matching To FBConfig = %d\n", iNumberOfFBConfigs);
	// Now We Have Multiple FBConfigs, We Need To Find Best Out Of Them

	int bestFrameBufferConfig = -1;
	int bestNumberOfSamples   = -1;
	int worstFrameBufferConfig = -1;
	int worstNumberOfSamples  = 999;

	//loop to check all fb configs
	for(int i = 0; i < iNumberOfFBConfigs; i++)
	{
		// Getting visual from each obtained FBConfig
		pTempXVisualInfo = glXGetVisualFromFBConfig(gpDisplay, pGLXFBConfigs[i]);

		if(pTempXVisualInfo)
		{
			//get Number of samplebuffer from respective fbConfig
			int sampleBuffers, samples;
			glXGetFBConfigAttrib(gpDisplay,
				pGLXFBConfigs[i],
				GLX_SAMPLE_BUFFERS,
				&sampleBuffers);

			//get number of samples from respective FBConfig
			glXGetFBConfigAttrib(gpDisplay, pGLXFBConfigs[i], GLX_SAMPLES, &samples);

			//More the number of samples and sample buffer more the eligible config buffer is
			if(bestFrameBufferConfig < 0 || sampleBuffers && samples >bestNumberOfSamples)
			{
				bestFrameBufferConfig=i;
				bestNumberOfSamples =samples;
			}

			if(worstFrameBufferConfig < 0 || !sampleBuffers || samples < worstNumberOfSamples)
			{
				worstFrameBufferConfig = i;
				worstNumberOfSamples = samples;
			}
		}

		XFree(pTempXVisualInfo);
	}

	// Now assign the found best one
	bestGLXFBConfig = pGLXFBConfigs[bestFrameBufferConfig];

	//Now assign the local best to global best
	gGLXFBConfig = bestGLXFBConfig;

	//Free the obtaind GLXFBConfig array;
	XFree(pGLXFBConfigs);

	//Accordingly get best visual
	gpXVisualInfo = glXGetVisualFromFBConfig(gpDisplay,  bestGLXFBConfig);

    winAttribs.border_pixel = 0;
    winAttribs.background_pixmap = 0;
    winAttribs.colormap = XCreateColormap(gpDisplay, 
                                          RootWindow(gpDisplay, gpXVisualInfo->screen),
                                          gpXVisualInfo->visual,
                                          AllocNone);
    gColormap = winAttribs.colormap;

    winAttribs.background_pixel = BlackPixel(gpDisplay,defaultScreen);

    winAttribs.event_mask = ExposureMask | VisibilityChangeMask | ButtonPressMask |
                           KeyPressMask | PointerMotionMask | StructureNotifyMask;
   
    styleMask = CWBorderPixel | CWBackPixel | CWEventMask | CWColormap;

    gWindow = XCreateWindow(gpDisplay,
                            RootWindow(gpDisplay,gpXVisualInfo->screen),
                            0,
                            0,
                            giWindowWidth,
                            giWindowHeight,
                            0,                                              // border width
                            gpXVisualInfo->depth,                           // color depth
                            InputOutput,
                            gpXVisualInfo->visual,
                            styleMask,
                            &winAttribs);
    if(!gWindow)
    {
        printf("ERROR : Failed To Create Main Window.\nExitting Now ...\n");
        uninitialize();
        exit(1);
    }

    XStoreName(gpDisplay, gWindow, "First XWindow");

    Atom windowManagerDelete = XInternAtom(gpDisplay, "WM_DELETE_WINDOW", True);
    XSetWMProtocols(gpDisplay, gWindow, &windowManagerDelete, 1);

    XMapWindow(gpDisplay, gWindow);
}

void ToggleFullScreen(void)
{
    //variable declarations
    Atom  wm_state;
    Atom fullscreen;
    XEvent xev = {0};

    //code
    wm_state = XInternAtom(gpDisplay, "_NET_WM_STATE", False);
    memset(&xev,0,sizeof(xev));

    xev.type = ClientMessage;
    xev.xclient.window = gWindow;
    xev.xclient.message_type = wm_state;
    xev.xclient.format = 32;
    xev.xclient.data.l[0] = bFullScreen ? 0 : 1;

    fullscreen = XInternAtom(gpDisplay, "_NET_WM_STATE_FULLSCREEN", False);
    xev.xclient.data.l[1] = fullscreen;
    
    XSendEvent(gpDisplay,
               RootWindow(gpDisplay, gpXVisualInfo->screen),
               False,
               StructureNotifyMask,
               &xev);
}

void uninitialize(void)
{
    if (vbo_square_position)
    {
        glDeleteBuffers(1, &vbo_square_position);
        vbo_square_position = 0;
    }

    if (vao_square)
    {
        glDeleteVertexArrays(1, &vao_square);
        vao_square = 0;
    }

    if(vbo_triangle_position)
    {
        glDeleteBuffers(1, &vbo_triangle_position);
        vbo_triangle_position = 0;
    }

    if(vao_square)
    {
        glDeleteVertexArrays(1, &vao_triangle);
        vao_triangle = 0;
    }

    if (vbo_color_i1)
    {
        glDeleteBuffers(1, &vbo_color_i1);
        vbo_color_i1 = 0;
    }
    if (vbo_position_i1)
    {
        glDeleteBuffers(1, &vbo_position_i1);
        vbo_position_i1 = 0;
    }
    if (vao_i1)
    {
        glDeleteVertexArrays(1, &vao_i1);
        vao_i1 = 0;
    }
    if (vbo_color_n)
    {
        glDeleteBuffers(1, &vbo_color_n);
        vbo_color_n = 0;
    }
    if (vbo_position_n)
    {
        glDeleteBuffers(1, &vbo_position_n);
        vbo_position_n = 0;
    }
    if (vao_n)
    {
        glDeleteVertexArrays(1, &vao_n);
        vao_n = 0;
    }

    if (vbo_color_d)
    {
        glDeleteBuffers(1, &vbo_color_d);
        vbo_color_d = 0;
    }
    if (vbo_position_d)
    {
        glDeleteBuffers(1, &vbo_position_d);
        vbo_position_d = 0;
    }
    if (vao_d)
    {
        glDeleteVertexArrays(1, &vao_d);
        vao_d = 0;
    }

    if (vbo_color_i2)
    {
        glDeleteBuffers(1, &vbo_color_i2);
        vbo_color_i2 = 0;
    }
    if (vbo_position_i2)
    {
        glDeleteBuffers(1, &vbo_position_i2);
        vbo_position_i2 = 0;
    }
    if (vao_i2)
    {
        glDeleteVertexArrays(1, &vao_i2);
        vao_i2 = 0;
    }

    if (vbo_color_a)
    {
        glDeleteBuffers(1, &vbo_color_a);
        vbo_color_a = 0;
    }
    if (vbo_position_a)
    {
        glDeleteBuffers(1, &vbo_position_a);
        vbo_position_a = 0;
    }
    if (vao_a)
    {
        glDeleteVertexArrays(1, &vao_a);
        vao_a = 0;
    }

    if (vbo_color_flag)
    {
        glDeleteBuffers(1, &vbo_color_flag);
        vbo_color_flag = 0;
    }
    if (vbo_position_flag)
    {
        glDeleteBuffers(1, &vbo_position_flag);
        vbo_position_flag = 0;
    }
    if (vao_flag)
    {
        glDeleteVertexArrays(1, &vao_flag);
        vao_flag = 0;
    }
    if(shaderProgramObject)
    {
        GLsizei shaderCount;
        GLsizei shaderNumber;

        glUseProgram(shaderProgramObject);
        glGetProgramiv(shaderProgramObject, GL_ATTACHED_SHADERS, &shaderCount);
        GLuint *pShaders = (GLuint *)malloc(sizeof(GLuint) * shaderCount);

        if(pShaders)
        {
            glGetAttachedShaders(shaderProgramObject, shaderCount, &shaderCount, pShaders);
            for(shaderNumber = 0; shaderNumber < shaderCount; shaderNumber++)
            {
                // Detach all shaaders one by one
                glDetachShader(shaderProgramObject, pShaders[shaderNumber]);
                // Delete the detached shader
                glDeleteShader(pShaders[shaderNumber]);
                pShaders[shaderNumber] = 0;
            }
            free(pShaders);
        }
        glDeleteProgram(shaderProgramObject);
        shaderProgramObject = 0;
        glUseProgram(0);
    }

    if(gWindow)
    {
        XDestroyWindow(gpDisplay,gWindow);
    }
    if(gColormap)
    {
        XFreeColormap(gpDisplay, gColormap);
    }
    if(gpXVisualInfo)
    {
        free(gpXVisualInfo);
        gpXVisualInfo = NULL;
    }
    if(gpDisplay)
    {
        XCloseDisplay(gpDisplay);
        gpDisplay = NULL;
    }
}

//initialize() function
void initialize(void)
{
    //variable declaration
    GLenum result;
    GLint iShaderCompileStatus = 0;
    GLint iProgramLinkStatus   = 0;
    GLint iInfoLogLength       = 0;
    GLchar *szInfoLog          = NULL;
    
    GLuint vertexShaderObject;
    GLuint fragmentShaderObject;

    glxCreateContextAttribsARB = (glxCreateContextAttribsARBProc)glXGetProcAddressARB((GLubyte *)"glXCreateContextAttribsARB");
    if(glxCreateContextAttribsARB == NULL)
    {
    	printf("Address not found for glXCreateContextAttribsARB\n");
    	uninitialize();
    }

     const int attribs[] = {
    	GLX_CONTEXT_MAJOR_VERSION_ARB, 4,
    	GLX_CONTEXT_MINOR_VERSION_ARB, 4,
    	GLX_CONTEXT_PROFILE_MASK_ARB, GLX_CONTEXT_CORE_PROFILE_BIT_ARB,
    	None
    };

    gGLXContext = glxCreateContextAttribsARB(gpDisplay, gGLXFBConfig, 0, True, attribs);

   

    if(!gGLXContext)
    {
    	//if obtained one is not highest one, specify the lowest one it will give you highest possible for it
    	const int glattribs[]={
    		GLX_CONTEXT_MAJOR_VERSION_ARB, 1,
    		GLX_CONTEXT_MINOR_VERSION_ARB, 0,
    		None
    	};

    	gGLXContext = glxCreateContextAttribsARB(gpDisplay, gGLXFBConfig, 0, True, glattribs);
    }

    //Check whether the obtained context is really a hardware rendering context or not
    if(!glXIsDirect(gpDisplay, gGLXContext))
    {
    	printf("The Obtained Context Is Not Hardware Rendering Context\n");
    }
    else
    {
    	printf("The Obtained Context Is Hardware Rendering Context\n");
    }

    //Make the Obtained Context as Current Rendering Context
    glXMakeCurrent(gpDisplay, gWindow, gGLXContext);

    // Initialize GLEW
    result = glewInit();
    if(result != GLEW_OK)
    {
        printf("glewInit() failed");
        uninitialize();
    }

    // Define Vertex Shader Object
    vertexShaderObject = glCreateShader(GL_VERTEX_SHADER);
    
    // Write Vertex Shader Code
    const GLchar *vertexShaderSourceCode =
    "   #version 430                                    " \
    "                                               \n  " \
    "   in vec4 vPosition;                              " \
    "   in vec4 vColor;                              " \
    "                                               \n  " \
    "   out vec4 out_color;                              " \
    "                                               \n  " \
    "   uniform mat4 u_mvp_matrix;                      " \
    "                                               \n  " \
    "   void main(void)                                 " \
    "   {                                               " \
    "      gl_Position = u_mvp_matrix * vPosition;      " \
    "      out_color = vColor;                      \n  " \
    "   }                                               ";

    // Specify Above Source Code To The Verex Shader Object
    glShaderSource(vertexShaderObject, 1, (const GLchar **)&vertexShaderSourceCode, NULL);

    // Compile Vertex Shader
    glCompileShader(vertexShaderObject);
    
    // Check Compilation Status
    glGetShaderiv(vertexShaderObject, GL_COMPILE_STATUS, &iShaderCompileStatus);

    //Error Checking
    if(iShaderCompileStatus == GL_FALSE)
    {
        glGetShaderiv(vertexShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
        if(iInfoLogLength > 0)
        {
            szInfoLog = (GLchar *)malloc(iInfoLogLength);
            if(szInfoLog != NULL)
            {
                GLsizei written;
                glGetShaderInfoLog(vertexShaderObject, iInfoLogLength, &written, szInfoLog);
                printf("VERTEX SHADER COMPILATION LOG : %s\n", szInfoLog);
                free(szInfoLog);
                uninitialize();
            }
        }
    }

    // Define Fragment Shader Object
    fragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);

    // Write fragment shader source code
    const GLchar *fragmentShaderSourceCode =
    "   #version 430                                    " \
    "                                               \n  " \
    "   in vec4 out_color;                          \n  " \
    "                                               \n  " \
    "   out vec4 FragColor;                             " \
    "                                               \n  " \
    "   void main(void)                                 " \
    "   {                                               " \
    "       FragColor = out_color;                      " \
    "   }                                               ";

    // Specify above source code to fragment shader object
    glShaderSource(fragmentShaderObject, 1, (const GLchar **)&fragmentShaderSourceCode, NULL);

    // Compile fragment shader
    iShaderCompileStatus = 0;
    iInfoLogLength =0;
    szInfoLog = NULL;

    glCompileShader(fragmentShaderObject);
    glGetShaderiv(fragmentShaderObject, GL_COMPILE_STATUS, &iShaderCompileStatus);

    if(iShaderCompileStatus == GL_FALSE)
    {
        glGetShaderiv(fragmentShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
        if(iInfoLogLength > 0)
        {
            szInfoLog = (GLchar *)malloc(iInfoLogLength);
            if (szInfoLog != NULL)
            {
                GLsizei written;
                glGetShaderInfoLog(fragmentShaderObject, iInfoLogLength, &written, szInfoLog);
                printf("FRAGMENT SHADER COMPILATION LOG : %s\n", szInfoLog);
                free(szInfoLog);
                uninitialize();
            }

        }
    }

    // Create Shader Program Object
    shaderProgramObject = glCreateProgram();

    // Attach Vertex Shader and Fragment Shader to the Shader Program
    glAttachShader(shaderProgramObject, vertexShaderObject);
    glAttachShader(shaderProgramObject, fragmentShaderObject);

    // Pre-Linking  binding to vertex attribute
    glBindAttribLocation(shaderProgramObject, AMC_ATTRIBUTE_POSITION, "vPosition");
    glBindAttribLocation(shaderProgramObject, AMC_ATTRIBUTE_COLOR, "vColor");

    // Link Shader Program
    glLinkProgram(shaderProgramObject);

    // Error Checking
    iProgramLinkStatus = 0;
    iInfoLogLength = 0;
    szInfoLog = NULL;

    glGetProgramiv(shaderProgramObject, GL_LINK_STATUS, &iProgramLinkStatus);
    if(iProgramLinkStatus == GL_FALSE)
    {
        glGetProgramiv(shaderProgramObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
        if(iInfoLogLength > 0)
        {
            szInfoLog = (GLchar *)malloc(iInfoLogLength);
            if(szInfoLog != NULL)
            {
                GLint written;
                glGetProgramInfoLog(shaderProgramObject, iInfoLogLength, &written, szInfoLog);
                free(szInfoLog);
                uninitialize();
            }
        }
    }

    // Post-Linking retrieving uniform location
    mvpUniform = glGetUniformLocation(shaderProgramObject, "u_mvp_matrix");

    const GLfloat triangleVertices[] ={
        0.0f, 1.0f, 0.0f,
        -1.0f, -1.0f, 0.0f,
        1.0f, -1.0f, 0.0f
    };

    const GLfloat squareVertices[] = {
        1.0f, 1.0f, 0.0f,
        -1.0f, 1.0f, 0.0f,
        -1.0f, -1.0f, 0.0f,
        1.0f, -1.0f, 0.0f
    };

    // ***** TRIANGLE *****

    glGenVertexArrays(1, &vao_triangle);
    glBindVertexArray(vao_triangle);

    glGenBuffers(1, &vbo_triangle_position);
    glBindBuffer(GL_ARRAY_BUFFER, vbo_triangle_position);
    glBufferData(GL_ARRAY_BUFFER, sizeof(triangleVertices), triangleVertices, GL_STATIC_DRAW);
    glVertexAttribPointer(AMC_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
    glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glBindVertexArray(0);

    // ***** RECTANGLE *****

    glGenVertexArrays(1, &vao_square);
    glBindVertexArray(vao_square);

    glGenBuffers(1, &vbo_square_position);
    glBindBuffer(GL_ARRAY_BUFFER, vbo_square_position);
    glBufferData(GL_ARRAY_BUFFER, sizeof(squareVertices), squareVertices, GL_STATIC_DRAW);
    glVertexAttribPointer(AMC_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
    glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glBindVertexArray(0);

    //-------------------------------- I -------------------------------------------
    const GLfloat i1Vertices[] = {
        -1.20f, 0.5f, 0.0f,
        - 0.88f, 0.5f, 0.0f,
        -1.04f, 0.5f, 0.0f,
        -1.04f, -0.5f, 0.0f,
        -1.20f, -0.5f, 0.0f,
        - 0.88f, -0.5f, 0.0f,

    };
    
    const GLfloat i1Color[] = {
        1.0f, 0.6f, 0.2f,
        1.0f, 0.6f, 0.2f,
        1.0f, 0.6f, 0.2f,
        0.070f, 0.533f, 0.027f,
        0.070f, 0.533f, 0.027f,
        0.070f, 0.533f, 0.027f,
    };

    glGenVertexArrays(1, &vao_i1);
    glBindVertexArray(vao_i1);
    // For position buffer
    glGenBuffers(1, &vbo_position_i1);
    glBindBuffer(GL_ARRAY_BUFFER, vbo_position_i1);
    glBufferData(GL_ARRAY_BUFFER, sizeof(i1Vertices), i1Vertices, GL_STATIC_DRAW);
    glVertexAttribPointer(AMC_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
    glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    // For color buffer
    glGenBuffers(1, &vbo_color_i1);
    glBindBuffer(GL_ARRAY_BUFFER, vbo_color_i1);
    glBufferData(GL_ARRAY_BUFFER, sizeof(i1Color), i1Color, GL_STATIC_DRAW);
    glVertexAttribPointer(AMC_ATTRIBUTE_COLOR, 3, GL_FLOAT, GL_FALSE, 0, NULL);
    glEnableVertexAttribArray(AMC_ATTRIBUTE_COLOR);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    // Unbind with vao of I
    glBindVertexArray(0);
    //-----------------------------------------------------------------------------

    //------------------------------------- N --------------------------------------
    const GLfloat nVertices[] = {
        -0.68f, 0.52f, 0.0f,
        -0.68f, -0.52f, 0.0f,
        -0.68f, 0.52f, 0.0f,
        -0.36f, -0.51f, 0.0f,
        -0.36f, 0.52f, 0.0f,
        -0.36f, -0.52f, 0.0f
    };

    const GLfloat nColor[] = {
        1.0f, 0.6f, 0.2f,
        0.070f, 0.533f, 0.027f,
        1.0f, 0.6f, 0.2f,
        0.070f, 0.533f, 0.027f,
        1.0f, 0.6f, 0.2f,
        0.070f, 0.533f, 0.027f
    };
    glGenVertexArrays(1, &vao_n);
    glBindVertexArray(vao_n);
    // For position buffer
    glGenBuffers(1, &vbo_position_n);
    glBindBuffer(GL_ARRAY_BUFFER, vbo_position_n);
    glBufferData(GL_ARRAY_BUFFER, sizeof(nVertices), nVertices, GL_STATIC_DRAW);
    glVertexAttribPointer(AMC_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
    glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    // For color buffer
    glGenBuffers(1, &vbo_color_n);
    glBindBuffer(GL_ARRAY_BUFFER, vbo_color_n);
    glBufferData(GL_ARRAY_BUFFER, sizeof(nColor), nColor, GL_STATIC_DRAW);
    glVertexAttribPointer(AMC_ATTRIBUTE_COLOR, 3, GL_FLOAT, GL_FALSE, 0, NULL);
    glEnableVertexAttribArray(AMC_ATTRIBUTE_COLOR);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    // Unbind with vao of N
    glBindVertexArray(0);
    //------------------------------------------------------------------------------
    
    // -------------------------------------- D -------------------------------------
    const GLfloat dVertices[] = {
        -0.10f, 0.5f, 0.0f,
        -0.10f, -0.5f, 0.0f,
        -0.17f, 0.5f, 0.0f,
        0.17f, 0.5f, 0.0f,
        0.17f, 0.5f, 0.0f,
        0.17f, -0.5f, 0.0f,
        -0.17f, -0.5f, 0.0f,
        0.17f, -0.5f, 0.0f
    };

    const GLfloat dColor[] = {
        1.0f, 0.6f, 0.2f,
        0.070f, 0.533f, 0.027f,
        1.0f, 0.6f, 0.2f,
        1.0f, 0.6f, 0.2f,
        1.0f, 0.6f, 0.2f,
        0.070f, 0.533f, 0.027f,
        0.070f, 0.533f, 0.027f,
        0.070f, 0.533f, 0.027f
    };
    
    glGenVertexArrays(1, &vao_d);
    glBindVertexArray(vao_d);
    // For position buffer
    glGenBuffers(1, &vbo_position_d);
    glBindBuffer(GL_ARRAY_BUFFER, vbo_position_d);
    glBufferData(GL_ARRAY_BUFFER, sizeof(dVertices), dVertices, GL_STATIC_DRAW);
    glVertexAttribPointer(AMC_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
    glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    // For color buffer
    glGenBuffers(1, &vbo_color_d);
    glBindBuffer(GL_ARRAY_BUFFER, vbo_color_d);
    glBufferData(GL_ARRAY_BUFFER, sizeof(dColor), dColor, GL_STATIC_DRAW);
    glVertexAttribPointer(AMC_ATTRIBUTE_COLOR, 3, GL_FLOAT, GL_FALSE, 0, NULL);
    glEnableVertexAttribArray(AMC_ATTRIBUTE_COLOR);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    // Unbind with vao of D
    glBindVertexArray(0);
    //-------------------------------------------------------------------------------
    
    //-------------------------------- I2 -------------------------------------------
    const GLfloat i2Vertices[] = {
        0.36f, 0.5f, 0.0f,
        0.68f, 0.5f, 0.0f,
        0.52f, 0.5f, 0.0f,
        0.52f, -0.5f, 0.0f,
        0.36f, -0.5f, 0.0f,
        0.68f, -0.5f, 0.0f
    };
    
    const GLfloat i2Color[] = {
        1.0f, 0.6f, 0.2f,
        1.0f, 0.6f, 0.2f,
        1.0f, 0.6f, 0.2f,
        0.070f, 0.533f, 0.027f,
        0.070f, 0.533f, 0.027f,
        0.070f, 0.533f, 0.027f
    };

    glGenVertexArrays(1, &vao_i2);
    glBindVertexArray(vao_i2);
    // For position buffer
    glGenBuffers(1, &vbo_position_i2);
    glBindBuffer(GL_ARRAY_BUFFER, vbo_position_i2);
    glBufferData(GL_ARRAY_BUFFER, sizeof(i2Vertices), i2Vertices, GL_STATIC_DRAW);
    glVertexAttribPointer(AMC_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
    glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    // For color buffer
    glGenBuffers(1, &vbo_color_i2);
    glBindBuffer(GL_ARRAY_BUFFER, vbo_color_i2);
    glBufferData(GL_ARRAY_BUFFER, sizeof(i2Color), i2Color, GL_STATIC_DRAW);
    glVertexAttribPointer(AMC_ATTRIBUTE_COLOR, 3, GL_FLOAT, GL_FALSE, 0, NULL);
    glEnableVertexAttribArray(AMC_ATTRIBUTE_COLOR);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    // Unbind with vao of I2
    glBindVertexArray(0);
    //-----------------------------------------------------------------------------

    //--------------------------------------- A -----------------------------------------
    const GLfloat aVertices[] = {
        0.83f, -0.52f, 0.0f,
        1.04f, 0.52f, 0.0f,
        1.04f, 0.52f, 0.0f,
        1.25f, -0.52f, 0.0f
    };

    const GLfloat aColor[] = {
        0.070f, 0.533f, 0.027f,
        1.0f, 0.6f, 0.2f,
        1.0f, 0.6f, 0.2f,
        0.070f, 0.533f, 0.027f
    };

    glGenVertexArrays(1, &vao_a);
    glBindVertexArray(vao_a);
    // For position buffer
    glGenBuffers(1, &vbo_position_a);
    glBindBuffer(GL_ARRAY_BUFFER, vbo_position_a);
    glBufferData(GL_ARRAY_BUFFER, sizeof(aVertices), aVertices, GL_STATIC_DRAW);
    glVertexAttribPointer(AMC_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
    glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    // For color buffer
    glGenBuffers(1, &vbo_color_a);
    glBindBuffer(GL_ARRAY_BUFFER, vbo_color_a);
    glBufferData(GL_ARRAY_BUFFER, sizeof(aColor), aColor, GL_STATIC_DRAW);
    glVertexAttribPointer(AMC_ATTRIBUTE_COLOR, 3, GL_FLOAT, GL_FALSE, 0, NULL);
    glEnableVertexAttribArray(AMC_ATTRIBUTE_COLOR);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    // Unbind with vao of A
    glBindVertexArray(0);
    //----------------------------------------------------------------------------------
    
    //--------------------------------------- FLAG -----------------------------------------
    const GLfloat flagVertices[] = {
        0.953f, 0.015f, 0.0f,
        1.128f, 0.015f, 0.0f,
        1.122f, 0.045f, 0.0f,
        0.958f, 0.045f, 0.0f,
        0.947f, -0.015f, 0.0f,
        1.135f, -0.015f, 0.0f,
        1.128f, 0.015f, 0.0f,
        0.953f, 0.015f, 0.0f,
        1.135f, -0.015f, 0.0f,
        0.947f, -0.015f, 0.0f,
        0.94f, -0.045f, 0.0f,
        1.14f, -0.045f, 0.0f

    }; // 4 saffron quad - 4 white quad - 4 green quad

    const GLfloat flagColor[] = {
        1.0f, 0.6f, 0.2f,
        1.0f, 0.6f, 0.2f,
        1.0f, 0.6f, 0.2f,
        1.0f, 0.6f, 0.2f,
        1.0f, 1.0f, 1.0f,
        1.0f, 1.0f, 1.0f,
        1.0f, 1.0f, 1.0f,
        1.0f, 1.0f, 1.0f,
        0.070f, 0.533f, 0.027f,
        0.070f, 0.533f, 0.027f,
        0.070f, 0.533f, 0.027f,
        0.070f, 0.533f, 0.027f
    };

    glGenVertexArrays(1, &vao_flag);
    glBindVertexArray(vao_flag);
    // For position buffer
    glGenBuffers(1, &vbo_position_flag);
    glBindBuffer(GL_ARRAY_BUFFER, vbo_position_flag);
    glBufferData(GL_ARRAY_BUFFER, sizeof(flagVertices), flagVertices, GL_STATIC_DRAW);
    glVertexAttribPointer(AMC_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
    glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    // For color buffer
    glGenBuffers(1, &vbo_color_flag);
    glBindBuffer(GL_ARRAY_BUFFER, vbo_color_flag);
    glBufferData(GL_ARRAY_BUFFER, sizeof(flagColor), flagColor, GL_STATIC_DRAW);
    glVertexAttribPointer(AMC_ATTRIBUTE_COLOR, 3, GL_FLOAT, GL_FALSE, 0, NULL);
    glEnableVertexAttribArray(AMC_ATTRIBUTE_COLOR);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    // Unbind with vao of flag
    glBindVertexArray(0);
    //----------------------------------------------------------------------------------

    glClearDepth(1.0f);
    glEnable(GL_DEPTH_TEST);
    glDepthFunc(GL_LEQUAL);

    glClearColor(0.0f,0.0f,0.0f,1.0f);

    perspectiveProjectionMatrix = mat4::identity();
}

//resize() function
void resize(int width, int height)
{
    if(height == 0)
        height =1;
    glViewport(0,0,(GLsizei)width, (GLsizei)height);

    perspectiveProjectionMatrix = perspective(45.0f, (GLfloat)width/(GLfloat)height, 0.1f, 100.0f);
}

//display function
void display(void)
{
    // Declaration of matrices
    mat4 modelViewMatrix;
    mat4 modelViewProjectionMatrix;

    // Initialize above matrices to identity
    modelViewMatrix = mat4::identity();
    modelViewProjectionMatrix = mat4::identity();

    glClear(GL_COLOR_BUFFER_BIT);
    glUseProgram(shaderProgramObject);

    // transformations
    modelViewMatrix = translate(0.0f,0.0f,-3.0f);
    modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;

    // Send necessary matrices to shader in respective uniforms
    glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);

    glLineWidth(15.0f);
    // ******************* I ************************
    // Bind to vao for triangle
    glBindVertexArray(vao_i1);
    glDrawArrays(GL_LINES, 0, 6);
    // Unbind vao
    glBindVertexArray(0);
    //-----------------------------------------------

    // ******************* N ************************
    glBindVertexArray(vao_n);
    glDrawArrays(GL_LINES, 0, 6);
    //unbind vao
    glBindVertexArray(0);
    // **********************************************

    // ******************* D ************************
    glBindVertexArray(vao_d);
    glDrawArrays(GL_LINES, 0, 8);
    //unbind vao
    glBindVertexArray(0);
    // **********************************************

    // ******************* I ************************
    glBindVertexArray(vao_i2);
    glDrawArrays(GL_LINES, 0, 6);
    //unbind vao
    glBindVertexArray(0);
    // **********************************************

    // ******************* A ************************
    glBindVertexArray(vao_a);
    glDrawArrays(GL_LINES, 0, 4);
    //unbind vao
    glBindVertexArray(0);
    // **********************************************

    // ***************** FLAG ***********************
    glBindVertexArray(vao_flag);
    glDrawArrays(GL_TRIANGLE_FAN, 0, 4);
    glDrawArrays(GL_TRIANGLE_FAN, 4, 4);
    glDrawArrays(GL_TRIANGLE_FAN, 8, 4);
    //unbind vao
    glBindVertexArray(0);
    // **********************************************

   
    //-----------------------------------------------

    // Unuse program
    glUseProgram(0);
    glXSwapBuffers(gpDisplay, gWindow);
}




