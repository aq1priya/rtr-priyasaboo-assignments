#include<iostream>
#include<stdio.h>
#include<stdlib.h>
#include<memory.h>

#include<X11/Xlib.h>
#include<X11/Xutil.h>
#include<X11/XKBlib.h>
#include<X11/keysym.h>

#include<GL/glew.h>
#include<GL/gl.h>
#include<GL/glx.h>
#include<SOIL/SOIL.h>

#include"vmath.h"

//macros
#define BUFFER_SIZE 1024

//namespaces
using namespace std;
using namespace vmath;

//structures
struct vec_int
{
	int *p;
	int size;
};

struct vec_float
{
	float *pf;
	int size;
};

//global variable declarations
bool bFullScreen = false;
Display *gpDisplay = NULL;
XVisualInfo *gpXVisualInfo = NULL;
Colormap gColormap;
Window gWindow;
int giWindowWidth = 800;
int giWindowHeight = 600;

typedef GLXContext(*glxCreateContextAttribsARBProc)(Display*, GLXFBConfig, GLXContext, Bool, const int*);
glxCreateContextAttribsARBProc glxCreateContextAttribsARB = NULL;
GLXFBConfig gGLXFBConfig;
GLXContext gGLXContext;

GLuint gShaderProgramObject;

GLuint vao_model;
GLuint vbo_model_position;
GLuint vbo_model_texture;
GLuint vbo_model_normal;
GLuint vbo_element_model;
GLuint textureMarble;

GLuint mUniform, vUniform, pUniform;	// ModelView Matrix Uniform and Projection Matrix Uniform
GLuint LdUniform, KdUniform;			// Diffuse property of Light and diffuse property of material
GLuint LaUniform, KaUniform;			// Ambient property of Light and ambient property of material
GLuint LsUniform, KsUniform;			// Specular property of Light and specular property of material
GLuint materialShininessUniform;		// material shininess
GLuint lightPositionUniform;			// light position
GLuint keyPressedUniform;				// is L key Pressed, to enable lighting effect
GLuint samplerUniform;

bool enableLighting = false;

float rotationAngle = 0.0f;

float lightAmbient[4] = { 0.5f, 0.5f, 0.5f, 0.0f };
float lightDiffuse[4] = { 1.0, 1.0f, 1.0f, 1.0f };
float lightSpecular[4] = { 1.0f, 1.0f, 1.0f, 1.0f };
float lightPosition[4] = { 10.0f, 10.0f, 10.0f, 1.0f };

float materialAmbient[4] = { 0.5f, 0.5f, 0.5f, 0.0f };
float materialDiffuse[4] = { 1.0f, 1.0f, 1.0f, 1.0f };
float materialSpecular[4] = { 1.0f, 1.0f, 1.0f, 1.0f };
float materialShininess = 128.0f;

FILE *gp_mesh_file;
char buffer[BUFFER_SIZE];
struct vec_float *gp_vertex, *gp_texture, *gp_normal;
struct vec_float *gp_vertex_sorted, *gp_texture_sorted, *gp_normal_sorted;
struct vec_int *gp_vertex_indices, *gp_texture_indices, *gp_normal_indices;

mat4 perspectiveProjectionMatrix;

enum 
{
    AMC_ATTRIBUTE_POSITION = 0,
    AMC_ATTRIBUTE_COLOR,
    AMC_ATTRIBUTE_NORMAL,
    AMC_ATTRIBUTE_TEXCOORD0
};

//entry point function
int main(void)
{
    //function prototype
    void CreateWindow(void);
    void ToggleFullScreen(void);
    void uninitialize();
    void initialize(void);
    void resize(int,int);
    void display(void);

    //variable declarations
    int winWidth = giWindowWidth;
    int winHeight = giWindowHeight;
    bool bDone = false;

    //code
    CreateWindow();
    
    //initialize
    initialize();

    //Message Loop
    XEvent event;
    KeySym keysym;

    while(bDone == false)
    {
        while(XPending(gpDisplay))
        {
            XNextEvent(gpDisplay,&event);
            switch(event.type)
            {
                case MapNotify:
                    break;
                case KeyPress:
                    keysym = XkbKeycodeToKeysym(gpDisplay, event.xkey.keycode, 0, 0);
                    switch(keysym)
                    {
                        case XK_Escape:
                            bDone = true;
                            break;
                        case XK_F:
                        case XK_f:
                            if(bFullScreen == false)
                            {
                                ToggleFullScreen();
                                bFullScreen = true;
                            }
                            else
                            {
                                ToggleFullScreen();
                                bFullScreen = false;
                            }
                            break;
                        case XK_l:
                        case XK_L:
                            if(enableLighting == false)
                            	enableLighting= true;
                            else
                            	enableLighting = false;
                            break;
                       default:
                            break;
                    }
                    break;
                case ButtonPress:
                   switch(event.xbutton.button)
                   {
                        case 1:                         //left button
                            break;
                        case 2:                         //middle button
                            break;
                        case 3:                         //right button
                            break;
                        default:
                            break;
                   }
                   break;
                case MotionNotify:
                   break;
                case ConfigureNotify:
                    winWidth = event.xconfigure.width;
                    winHeight =event.xconfigure.height;
                    resize(winWidth, winHeight);
                    break;
                case Expose:
                    break;
                case DestroyNotify:
                    break;
                case 33:                                //to handle close button and close menu
                    bDone = true;
                    break;
                default:
                    break;
            }
        }
        display();
    }
    uninitialize();
    return(0);
}

void CreateWindow(void)
{
    //function prototype
    void uninitialize(void);

    //variable declarations
    XSetWindowAttributes winAttribs;
    int defaultScreen;
    int defaultDepth;
    int styleMask;
    int iNumberOfFBConfigs = 0;					// number of FBConfigs
    GLXFBConfig *pGLXFBConfigs = NULL;			//array of FBConfigs
    GLXFBConfig bestGLXFBConfig;				//best FBConfig among all
    XVisualInfo *pTempXVisualInfo = NULL;		//temporary visual info

    // Initialize Frame Buffer Attributes
    static int frameBufferAttributes[] = {
        GLX_X_RENDERABLE, True,
        GLX_DRAWABLE_TYPE,GLX_WINDOW_BIT,
        GLX_RENDER_TYPE, GLX_RGBA_BIT,
        GLX_X_VISUAL_TYPE, GLX_TRUE_COLOR,
        GLX_DOUBLEBUFFER, True,
        GLX_RED_SIZE, 8,
        GLX_GREEN_SIZE , 8,
        GLX_BLUE_SIZE, 8,
        GLX_ALPHA_SIZE, 8,
        GLX_DEPTH_SIZE, 24,
        GLX_STENCIL_SIZE, 8,
        None};

    //code
    gpDisplay = XOpenDisplay(NULL);
    if(!gpDisplay)
    {
        printf("ERROR : Unable To Open X Display.\nExitting Now ...\n");
        uninitialize();
        exit(1);
    }

    defaultScreen = XDefaultScreen(gpDisplay);

	// Retrive All FBConfigs Driver Has
	pGLXFBConfigs = glXChooseFBConfig(gpDisplay, defaultScreen,	frameBufferAttributes, &iNumberOfFBConfigs);

	printf("Number Of FBConfigs Matching To FBConfig = %d\n", iNumberOfFBConfigs);
	// Now We Have Multiple FBConfigs, We Need To Find Best Out Of Them

	int bestFrameBufferConfig = -1;
	int bestNumberOfSamples   = -1;
	int worstFrameBufferConfig = -1;
	int worstNumberOfSamples  = 999;

	//loop to check all fb configs
	for(int i = 0; i < iNumberOfFBConfigs; i++)
	{
		// Getting visual from each obtained FBConfig
		pTempXVisualInfo = glXGetVisualFromFBConfig(gpDisplay, pGLXFBConfigs[i]);

		if(pTempXVisualInfo)
		{
			//get Number of samplebuffer from respective fbConfig
			int sampleBuffers, samples;
			glXGetFBConfigAttrib(gpDisplay,
				pGLXFBConfigs[i],
				GLX_SAMPLE_BUFFERS,
				&sampleBuffers);

			//get number of samples from respective FBConfig
			glXGetFBConfigAttrib(gpDisplay, pGLXFBConfigs[i], GLX_SAMPLES, &samples);

			//More the number of samples and sample buffer more the eligible config buffer is
			if(bestFrameBufferConfig < 0 || sampleBuffers && samples >bestNumberOfSamples)
			{
				bestFrameBufferConfig=i;
				bestNumberOfSamples =samples;
			}

			if(worstFrameBufferConfig < 0 || !sampleBuffers || samples < worstNumberOfSamples)
			{
				worstFrameBufferConfig = i;
				worstNumberOfSamples = samples;
			}
		}

		XFree(pTempXVisualInfo);
	}

	// Now assign the found best one
	bestGLXFBConfig = pGLXFBConfigs[bestFrameBufferConfig];

	//Now assign the local best to global best
	gGLXFBConfig = bestGLXFBConfig;

	//Free the obtaind GLXFBConfig array;
	XFree(pGLXFBConfigs);

	//Accordingly get best visual
	gpXVisualInfo = glXGetVisualFromFBConfig(gpDisplay,  bestGLXFBConfig);

    winAttribs.border_pixel = 0;
    winAttribs.background_pixmap = 0;
    winAttribs.colormap = XCreateColormap(gpDisplay, 
                                          RootWindow(gpDisplay, gpXVisualInfo->screen),
                                          gpXVisualInfo->visual,
                                          AllocNone);
    gColormap = winAttribs.colormap;

    winAttribs.background_pixel = BlackPixel(gpDisplay,defaultScreen);

    winAttribs.event_mask = ExposureMask | VisibilityChangeMask | ButtonPressMask |
                           KeyPressMask | PointerMotionMask | StructureNotifyMask;
   
    styleMask = CWBorderPixel | CWBackPixel | CWEventMask | CWColormap;

    gWindow = XCreateWindow(gpDisplay,
                            RootWindow(gpDisplay,gpXVisualInfo->screen),
                            0,
                            0,
                            giWindowWidth,
                            giWindowHeight,
                            0,                                              // border width
                            gpXVisualInfo->depth,                           // color depth
                            InputOutput,
                            gpXVisualInfo->visual,
                            styleMask,
                            &winAttribs);
    if(!gWindow)
    {
        printf("ERROR : Failed To Create Main Window.\nExitting Now ...\n");
        uninitialize();
        exit(1);
    }

    XStoreName(gpDisplay, gWindow, "Static Smiley");

    Atom windowManagerDelete = XInternAtom(gpDisplay, "WM_DELETE_WINDOW", True);
    XSetWMProtocols(gpDisplay, gWindow, &windowManagerDelete, 1);

    XMapWindow(gpDisplay, gWindow);
}

void ToggleFullScreen(void)
{
    //variable declarations
    Atom  wm_state;
    Atom fullscreen;
    XEvent xev = {0};

    //code
    wm_state = XInternAtom(gpDisplay, "_NET_WM_STATE", False);
    memset(&xev,0,sizeof(xev));

    xev.type = ClientMessage;
    xev.xclient.window = gWindow;
    xev.xclient.message_type = wm_state;
    xev.xclient.format = 32;
    xev.xclient.data.l[0] = bFullScreen ? 0 : 1;

    fullscreen = XInternAtom(gpDisplay, "_NET_WM_STATE_FULLSCREEN", False);
    xev.xclient.data.l[1] = fullscreen;
    
    XSendEvent(gpDisplay,
               RootWindow(gpDisplay, gpXVisualInfo->screen),
               False,
               StructureNotifyMask,
               &xev);
}

void uninitialize(void)
{
    int destroy_vec_int(struct vec_int *p_vec_int);
	int destroy_vec_float(struct vec_float *p_vec_float);

	if (vbo_model_position)
	{
		glDeleteBuffers(1, &vbo_model_position);
		vbo_model_position = 0;
	}

    if (vbo_model_normal)
    {
        glDeleteBuffers(1, &vbo_model_normal);
        vbo_model_normal = 0;
    }


    if (vbo_model_texture)
    {
        glDeleteBuffers(1, &vbo_model_texture);
        vbo_model_texture = 0;
    }

	if (vbo_element_model)
	{
		glDeleteBuffers(1, &vbo_element_model);
		vbo_element_model = 0;
	}
	if (vao_model)
	{
		glDeleteVertexArrays(1, &vao_model);
		vao_model = 0;
	}

	destroy_vec_float(gp_vertex);
	gp_vertex = NULL;
	destroy_vec_float(gp_texture);
	gp_texture = NULL;
	destroy_vec_float(gp_normal);
	gp_normal = NULL;

	destroy_vec_float(gp_vertex_sorted);
	gp_vertex_sorted = NULL;
	destroy_vec_float(gp_texture_sorted);
	gp_texture_sorted = NULL;
	destroy_vec_float(gp_normal_sorted);
	gp_normal_sorted = NULL;

	destroy_vec_int(gp_vertex_indices);
	gp_vertex_indices = NULL;
	destroy_vec_int(gp_texture_indices);
	gp_texture_indices = NULL;
	destroy_vec_int(gp_normal_indices);
	gp_normal_indices = NULL;


	if (gShaderProgramObject)
	{
		GLsizei shaderCount;
		GLsizei shaderNumber;

		glUseProgram(gShaderProgramObject);
		glGetProgramiv(gShaderProgramObject, GL_ATTACHED_SHADERS, &shaderCount);
		GLuint *pShaders = (GLuint *)malloc(sizeof(GLuint) * shaderCount);

		if (pShaders)
		{
			glGetAttachedShaders(gShaderProgramObject, shaderCount, &shaderCount, pShaders);
			for (shaderNumber = 0; shaderNumber < shaderCount; shaderNumber++)
			{
				//detach all the shader one by one
				glDetachShader(gShaderProgramObject, pShaders[shaderNumber]);
				//delete detached shader
				glDeleteShader(pShaders[shaderNumber]);
				pShaders[shaderNumber] = 0;
			}
			free(pShaders);
		}
		glDeleteProgram(gShaderProgramObject);
		gShaderProgramObject = 0;
		glUseProgram(0);
    }

    if(gWindow)
    {
        XDestroyWindow(gpDisplay,gWindow);
    }
    if(gColormap)
    {
        XFreeColormap(gpDisplay, gColormap);
    }
    if(gpXVisualInfo)
    {
        free(gpXVisualInfo);
        gpXVisualInfo = NULL;
    }
    if(gpDisplay)
    {
        XCloseDisplay(gpDisplay);
        gpDisplay = NULL;
    }
}

//initialize() function
void initialize(void)
{
	//function declarations
	void resize(int, int);
	void uninitialize(void);
	void load_mesh(void);

    //variable declaration
    GLenum result;
    GLint iShaderCompileStatus = 0;
    GLint iProgramLinkStatus   = 0;
    GLint iInfoLogLength       = 0;
    GLchar *szInfoLog          = NULL;
    
    GLuint vertexShaderObject;
    GLuint fragmentShaderObject;

    bool bCheck = false;

    //function declaration
    bool loadTexture(GLuint *, const char *);


    glxCreateContextAttribsARB = (glxCreateContextAttribsARBProc)glXGetProcAddressARB((GLubyte *)"glXCreateContextAttribsARB");
    if(glxCreateContextAttribsARB == NULL)
    {
    	printf("Address not found for glXCreateContextAttribsARB\n");
    	uninitialize();
    }

     const int attribs[] = {
    	GLX_CONTEXT_MAJOR_VERSION_ARB, 4,
    	GLX_CONTEXT_MINOR_VERSION_ARB, 4,
    	GLX_CONTEXT_PROFILE_MASK_ARB, GLX_CONTEXT_CORE_PROFILE_BIT_ARB,
    	None
    };

    gGLXContext = glxCreateContextAttribsARB(gpDisplay, gGLXFBConfig, 0, True, attribs);

   

    if(!gGLXContext)
    {
    	//if obtained one is not highest one, specify the lowest one it will give you highest possible for it
    	const int glattribs[]={
    		GLX_CONTEXT_MAJOR_VERSION_ARB, 1,
    		GLX_CONTEXT_MINOR_VERSION_ARB, 0,
    		None
    	};

    	gGLXContext = glxCreateContextAttribsARB(gpDisplay, gGLXFBConfig, 0, True, glattribs);
    }

    //Check whether the obtained context is really a hardware rendering context or not
    if(!glXIsDirect(gpDisplay, gGLXContext))
    {
    	printf("The Obtained Context Is Not Hardware Rendering Context\n");
    }
    else
    {
    	printf("The Obtained Context Is Hardware Rendering Context\n");
    }

    //Make the Obtained Context as Current Rendering Context
    glXMakeCurrent(gpDisplay, gWindow, gGLXContext);

    // Initialize GLEW
    result = glewInit();
    if(result != GLEW_OK)
    {
        printf("glewInit() failed");
        uninitialize();
    }

    // Define Vertex Shader Object
    vertexShaderObject = glCreateShader(GL_VERTEX_SHADER);
    
    // Write Vertex Shader Code
    const GLchar *vertexShaderSourceCode =
   		"	#version 430 core																	" \
		"	\n																					" \
		"	in vec4 vPosition;																	" \
		"	in vec3 vNormal;																	" \
		"	in vec2 vTexcoord;																	" \
		"																					  \n" \
		"	out vec4 eyeCoordinates;															" \
		"	out vec3 tNorm;																		" \
		"	out vec3 lightDirection;															" \
		"	out vec3 viewerVector;																" \
		"	out vec2 out_texcoord;																" \
		"																					  \n" \
		"	uniform mat4 u_m_matrix;															" \
		"	uniform mat4 u_v_matrix;															" \
		"	uniform mat4 u_p_matrix;															" \
		"	uniform int u_l_key_is_pressed;														" \
		"	uniform vec4 u_light_position;														" \
		"																					  \n" \
		"	void main(void)																		" \
		"	{																					" \
		"		if(u_l_key_is_pressed == 1)														" \
		"		{																				" \
		"			eyeCoordinates = u_v_matrix * u_m_matrix * vPosition;						" \
		"			tNorm = mat3(u_v_matrix * u_m_matrix) * vNormal;							" \
		"			lightDirection = vec3(u_light_position - eyeCoordinates);					" \
		"			viewerVector = vec3(-eyeCoordinates);										" \
		"		}																				" \
		"	out_texcoord = vTexcoord;														  \n" \
		"	gl_Position = u_v_matrix * u_p_matrix * u_m_matrix * vPosition;						" \
		"	}																					";


    // Specify Above Source Code To The Verex Shader Object
    glShaderSource(vertexShaderObject, 1, (const GLchar **)&vertexShaderSourceCode, NULL);

    // Compile Vertex Shader
    glCompileShader(vertexShaderObject);
    
    // Check Compilation Status
    glGetShaderiv(vertexShaderObject, GL_COMPILE_STATUS, &iShaderCompileStatus);

    //Error Checking
    if(iShaderCompileStatus == GL_FALSE)
    {
        glGetShaderiv(vertexShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
        if(iInfoLogLength > 0)
        {
            szInfoLog = (GLchar *)malloc(iInfoLogLength);
            if(szInfoLog != NULL)
            {
                GLsizei written;
                glGetShaderInfoLog(vertexShaderObject, iInfoLogLength, &written, szInfoLog);
                printf("VERTEX SHADER COMPILATION LOG : %s\n", szInfoLog);
                free(szInfoLog);
                uninitialize();
            }
        }
    }

    // Define Fragment Shader Object
    fragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);

    // Write fragment shader source code
    const GLchar *fragmentShaderSourceCode =
		"	#version 430 core																		" \
		"	\n																						" \
		"	in vec3 tNorm;																			" \
		"	in vec3 lightDirection;																	" \
		"	in vec3 viewerVector;																	" \
		"	in vec2 out_texcoord;																	" \
		"																						  \n" \
		"	out vec4 fragColor;																		" \
		"																						  \n" \
		"	uniform vec3 u_ld;																		" \
		"	uniform vec3 u_kd;																		" \
		"	uniform vec3 u_la;																		" \
		"	uniform vec3 u_ka;																		" \
		"	uniform vec3 u_ls;																		" \
		"	uniform vec3 u_ks;																		" \
		"	uniform float u_material_shininess;														" \
		"	uniform int u_l_key_is_pressed;															" \
		"	uniform sampler2D u_sampler;														  \n" \
		"																						  \n" \
		"	void main(void)																			" \
		"	{																						" \
		"		vec3 phong_ads_light;																" \
		"		vec4 texColor;																" \
		"		if(u_l_key_is_pressed == 1)															" \
		"		{																					" \
		"			vec3 normalizedtNorm = normalize(tNorm);										" \
		"			vec3 normalizedlightDirection = normalize(lightDirection);						" \
		"			vec3 normalizedviewerVector = normalize(viewerVector);							" \
		"																						  \n" \
		"			vec3 reflectionVector = normalize(reflect(-normalizedlightDirection, normalizedtNorm));" \
		"			float tnDotld = max(dot(normalizedlightDirection, normalizedtNorm), 0.0);		" \
		"			float rvDotvv = max(dot(reflectionVector, normalizedviewerVector), 0.0);		" \
		"																						  \n" \
		"			vec3 ambient = u_la * u_ka;														" \
		"			vec3 diffuse = u_ld * u_kd * tnDotld;											" \
		"			vec3 specular = u_ls * u_ks * pow(rvDotvv, u_material_shininess);				" \
		"			phong_ads_light = ambient + diffuse + specular;									" \
		"																						  \n" \
		"			texColor = texture(u_sampler, out_texcoord);								  \n" \
		"			fragColor = texColor * vec4(phong_ads_light, 1.0);								" \
		"		}																					" \
		"		else																				" \
		"		{																					" \
		"			texColor = texture(u_sampler, out_texcoord);								  \n" \
		"			phong_ads_light = vec3(1.0, 1.0, 1.0);											" \
		"			fragColor = texColor * vec4(phong_ads_light, 1.0);								" \
		"		}																					" \
		"	}																						";

    // Specify above source code to fragment shader object
    glShaderSource(fragmentShaderObject, 1, (const GLchar **)&fragmentShaderSourceCode, NULL);

    // Compile fragment shader
    iShaderCompileStatus = 0;
    iInfoLogLength =0;
    szInfoLog = NULL;

    glCompileShader(fragmentShaderObject);
    glGetShaderiv(fragmentShaderObject, GL_COMPILE_STATUS, &iShaderCompileStatus);

    if(iShaderCompileStatus == GL_FALSE)
    {
        glGetShaderiv(fragmentShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
        if(iInfoLogLength > 0)
        {
            szInfoLog = (GLchar *)malloc(iInfoLogLength);
            if (szInfoLog != NULL)
            {
                GLsizei written;
                glGetShaderInfoLog(fragmentShaderObject, iInfoLogLength, &written, szInfoLog);
                printf("FRAGMENT SHADER COMPILATION LOG : %s\n", szInfoLog);
                free(szInfoLog);
                uninitialize();
            }

        }
    }

    // Create Shader Program Object
    gShaderProgramObject = glCreateProgram();

    // Attach Vertex Shader and Fragment Shader to the Shader Program
    glAttachShader(gShaderProgramObject, vertexShaderObject);
    glAttachShader(gShaderProgramObject, fragmentShaderObject);

    // Pre-Linking  binding to vertex attribute
    glBindAttribLocation(gShaderProgramObject, AMC_ATTRIBUTE_POSITION, "vPosition");
    glBindAttribLocation(gShaderProgramObject, AMC_ATTRIBUTE_NORMAL, "vNormal");
    glBindAttribLocation(gShaderProgramObject, AMC_ATTRIBUTE_TEXCOORD0, "vTexcoord");

    // Link Shader Program
    glLinkProgram(gShaderProgramObject);

    // Error Checking
    iProgramLinkStatus = 0;
    iInfoLogLength = 0;
    szInfoLog = NULL;

    glGetProgramiv(gShaderProgramObject, GL_LINK_STATUS, &iProgramLinkStatus);
    if(iProgramLinkStatus == GL_FALSE)
    {
        glGetProgramiv(gShaderProgramObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
        if(iInfoLogLength > 0)
        {
            szInfoLog = (GLchar *)malloc(iInfoLogLength);
            if(szInfoLog != NULL)
            {
                GLint written;
                glGetProgramInfoLog(gShaderProgramObject, iInfoLogLength, &written, szInfoLog);
                free(szInfoLog);
                uninitialize();
            }
        }
    }

    // Post-Linking retrieving uniform location
  	vUniform = glGetUniformLocation(gShaderProgramObject, "u_v_matrix");
	mUniform = glGetUniformLocation(gShaderProgramObject, "u_m_matrix");
	pUniform = glGetUniformLocation(gShaderProgramObject, "u_p_matrix");
	LdUniform = glGetUniformLocation(gShaderProgramObject, "u_ld");
	KdUniform = glGetUniformLocation(gShaderProgramObject, "u_kd");
	LaUniform = glGetUniformLocation(gShaderProgramObject, "u_la");
	KaUniform = glGetUniformLocation(gShaderProgramObject, "u_ka");
	LsUniform = glGetUniformLocation(gShaderProgramObject, "u_ls");
	KsUniform = glGetUniformLocation(gShaderProgramObject, "u_ks");
	materialShininessUniform = glGetUniformLocation(gShaderProgramObject, "u_material_shininess");
	lightPositionUniform = glGetUniformLocation(gShaderProgramObject, "u_light_position");
	keyPressedUniform = glGetUniformLocation(gShaderProgramObject, "u_l_key_is_pressed");
	samplerUniform = glGetUniformLocation(gShaderProgramObject, "u_Sampler");

   
	// load model data
	load_mesh();

    glGenVertexArrays(1, &vao_model);
	glBindVertexArray(vao_model);

	// buffer for position
	glGenBuffers(1, &vbo_model_position);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_model_position);
	glBufferData(GL_ARRAY_BUFFER, (gp_vertex_sorted->size)*sizeof(GLfloat), gp_vertex_sorted->pf, GL_STATIC_DRAW);
	glVertexAttribPointer(AMC_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	// buffer for normal
	glGenBuffers(1, &vbo_model_normal);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_model_normal);
	glBufferData(GL_ARRAY_BUFFER, (gp_normal_sorted->size) * sizeof(GLfloat), gp_normal_sorted->pf, GL_STATIC_DRAW);
	glVertexAttribPointer(AMC_ATTRIBUTE_NORMAL, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AMC_ATTRIBUTE_NORMAL);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	// buffer for texture
	glGenBuffers(1, &vbo_model_texture);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_model_texture);
	glBufferData(GL_ARRAY_BUFFER, (gp_texture_sorted->size) * sizeof(GLfloat), gp_texture_sorted->pf, GL_STATIC_DRAW);
	glVertexAttribPointer(AMC_ATTRIBUTE_TEXCOORD0, 2, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AMC_ATTRIBUTE_TEXCOORD0);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	// buffer for elements
	glGenBuffers(1, &vbo_element_model);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_element_model);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, gp_vertex_indices->size * sizeof(int), gp_vertex_indices->p, GL_STATIC_DRAW);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

    // Unbind with vao
    glBindVertexArray(0);

    bCheck = loadTexture(&textureMarble, "./marble.bmp");
    if(bCheck == false)
    {
        printf(" Texture Loading Failed\n");
    }

    glClearDepth(1.0f);
    glEnable(GL_DEPTH_TEST);
    glDepthFunc(GL_LEQUAL);

    glClearColor(0.0f, 0.0f, 0.0f, 1.0f);

    perspectiveProjectionMatrix = mat4::identity();
}

//resize() function
void resize(int width, int height)
{
    if(height == 0)
        height =1;
    glViewport(0,0,(GLsizei)width, (GLsizei)height);

    perspectiveProjectionMatrix = perspective(45.0f, (GLfloat)width/(GLfloat)height, 0.1f, 100.0f);
}

//display function
void display(void)
{
    // Declaration of matrices
   	mat4 modelMatrix;
	mat4 ViewMatrix;
	mat4 projectionMatrix;
	mat4 translationMatrix;
	mat4 rotationMatrix;
	mat4 scaleMatrix;

	//initialize above matrices to identity
	modelMatrix = mat4::identity();
	ViewMatrix = mat4::identity();
	projectionMatrix = mat4::identity();
	translationMatrix = mat4::identity();
	rotationMatrix = mat4::identity();
	scaleMatrix = mat4::identity();

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
   
    glUseProgram(gShaderProgramObject);

   	//do necessary matrix multiplication
	translationMatrix = translate(0.0f, 0.0f, -20.0f);
	scaleMatrix = scale(0.3f, 0.3f, 0.3f);
	rotationMatrix = rotate(rotationAngle, 0.0f, 1.0f, 0.0f);
	modelMatrix = translationMatrix * scaleMatrix * rotationMatrix;
	projectionMatrix = projectionMatrix * perspectiveProjectionMatrix;

	if (enableLighting == true)
	{
		glUniform1i(keyPressedUniform, 1);
		glUniform3fv(LdUniform, 1, lightDiffuse);
		glUniform3fv(LaUniform, 1, lightAmbient);
		glUniform3fv(LsUniform, 1, lightSpecular);

		glUniform3fv(KdUniform, 1, materialDiffuse);
		glUniform3fv(KaUniform, 1, materialAmbient);
		glUniform3fv(KsUniform, 1, materialSpecular);

		glUniform1f(materialShininessUniform, materialShininess);

		glUniform4fv(lightPositionUniform, 1, lightPosition);
	}
	else
	{
		glUniform1i(keyPressedUniform, 0);
	}

		//send necessary matrices to shader in respective uniforms
	glUniformMatrix4fv(mUniform, 1, GL_FALSE, modelMatrix);
	glUniformMatrix4fv(vUniform, 1, GL_FALSE, ViewMatrix);
	glUniformMatrix4fv(pUniform, 1, GL_FALSE, projectionMatrix);

	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, textureMarble);
	glUniform1i(samplerUniform, 0);

    //----------------------------------------------------------
   	glBindVertexArray(vao_model);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_element_model);
	glDrawElements(GL_TRIANGLES, (gp_vertex_indices->size), GL_UNSIGNED_INT, NULL);
	//unbind vao
	glBindVertexArray(0);
	
	//----------------------------------------------------------
    // Unuse program
    glUseProgram(0);

    glXSwapBuffers(gpDisplay, gWindow);

    rotationAngle = rotationAngle + 0.05f;
	if (rotationAngle > 360.0f)
		rotationAngle = 0.0f;
}

bool loadTexture(GLuint *texture, const char *path)
{
    int imageWidth, imageHeight;
    bool bStatus = false;
    unsigned char *imageData = NULL;

    imageData = SOIL_load_image(path, &imageWidth, &imageHeight, 0, SOIL_LOAD_RGB);
    if(imageData == NULL)
    {
        bStatus = false;
        printf("Image Data Is NULL\n");
        return(bStatus);
    }
    else
        bStatus = true;

    glEnable(GL_TEXTURE_2D);
    glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
    glGenTextures(1, texture);
    glBindTexture(GL_TEXTURE_2D, *texture);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
    glTexImage2D(GL_TEXTURE_2D,
        0,
        GL_RGB,
        imageWidth,
        imageHeight,
        0,
        GL_RGB,
        GL_UNSIGNED_BYTE,
        imageData);
    glGenerateMipmap(GL_TEXTURE_2D);
    SOIL_free_image_data(imageData);
    return(bStatus);
}

// load mesh function
void load_mesh(void)
{
	// function declarations
	struct vec_int *create_vec_int();
	struct vec_float *create_vec_float();
	int push_back_vec_int(struct vec_int *p_vec_int, int data);
	int push_back_vec_float(struct vec_float *p_vec_float, float data);
	void show_vec_float(struct vec_float *p_vec_float);
	void show_vec_int(struct vec_int *p_vec_int);
	int destroy_vec_float(struct vec_float *p_vec_float);

	// variable declarations
	const char *space = " ", *slash = "/";
	char *first_token = NULL, *token;
	char *f_entries[3] = { NULL, NULL, NULL };
	int nr_pos_cords = 0;
	int nr_tex_cords = 0;
	int nr_normal_cords = 0;
	int nr_faces = 0;
	int i, vi;

	gp_mesh_file = fopen("./teapot1.obj", "r");
	if (gp_mesh_file == NULL)
	{
		printf("Mesh file %s can not be opened.\n", "untitled.obj");
	}
	else
		printf("Mesh file %s loaded successfully.\n", "untitled.obj");

	gp_vertex = create_vec_float();
	gp_texture = create_vec_float();
	gp_normal = create_vec_float();

	gp_vertex_indices = create_vec_int();
	gp_texture_indices = create_vec_int();
	gp_normal_indices = create_vec_int();

	while (fgets(buffer, BUFFER_SIZE, gp_mesh_file) != NULL)
	{
		first_token = strtok(buffer, space);

		if (strcmp(first_token, "v") == 0)
		{
			nr_pos_cords++;
			while ((token = strtok(NULL, space)) != NULL)
				push_back_vec_float(gp_vertex, atof(token));

		}
		else if (strcmp(first_token, "vt") == 0)
		{
			nr_tex_cords++;
			while ((token = strtok(NULL, space)) != NULL)
				push_back_vec_float(gp_texture, atof(token));

		}
		else if (strcmp(first_token, "vn") == 0)
		{
			nr_normal_cords++;
			while ((token = strtok(NULL, space)) != NULL)
				push_back_vec_float(gp_normal, atof(token));

		}
		else if (strcmp(first_token, "f") == 0)
		{
			nr_faces++;
			for (i = 0; i < 3; i++)
			{
				f_entries[i] = strtok(NULL, space);
			}

			for (i = 0; i < 3; i++)
			{
				token = strtok(f_entries[i], slash);
				push_back_vec_int(gp_vertex_indices, atoi(token) - 1);
				token = strtok(NULL, slash);
				push_back_vec_int(gp_texture_indices, atoi(token) - 1);
				token = strtok(NULL, slash);
				push_back_vec_int(gp_normal_indices, atoi(token) - 1);
			}
		}
	}

	gp_vertex_sorted = create_vec_float();
	for (i = 0; i < gp_vertex_indices->size; i++)
		push_back_vec_float(gp_vertex_sorted, gp_vertex->pf[i]);

	gp_texture_sorted = create_vec_float();
	for (i = 0; i < gp_texture_indices->size; i++)
		push_back_vec_float(gp_texture_sorted, gp_texture->pf[i]);

	gp_normal_sorted = create_vec_float();
	for (i = 0; i < gp_normal_indices->size; i++)
		push_back_vec_float(gp_normal_sorted, gp_normal->pf[i]);
	
	fclose(gp_mesh_file);
	gp_mesh_file = NULL;

	/*fprintf(gpFile, "vertices:%d texture:%d normal:%d faces:%d\n", nr_pos_cords, nr_tex_cords, nr_normal_cords, nr_faces);
	fprintf(gpFile, "vert size:%d texture size:%d normal size:%d\n", gp_vertex->size, gp_texture->size, gp_normal->size);
	fprintf(gpFile, "vert indices :%d texture indices :%d normal indices:%d\n", gp_vertex_indices->size, gp_texture_indices->size,
		gp_normal_indices->size);

	fprintf(gpFile, "Vertex Array\n");
	show_vec_float(gp_vertex);

	fprintf(gpFile, "Normal Array\n");
	show_vec_float(gp_normal);

	fprintf(gpFile, "Vertex Array Indices\n");
	show_vec_int(gp_vertex_indices);

	fprintf(gpFile, "Normal Array Indices\n");
	show_vec_int(gp_normal_indices);
	*/
}

struct vec_int *create_vec_int()
{
	struct vec_int *p = (struct vec_int *)malloc(sizeof(struct vec_int));
	memset(p, 0, sizeof(struct vec_int));
	return(p);
}

struct vec_float *create_vec_float()
{
	struct vec_float *p = (struct vec_float *)malloc(sizeof(struct vec_float));
	memset(p, 0, sizeof(struct vec_float));
	return(p);
}

int push_back_vec_int(struct vec_int *p_vec_int, int data)
{
	p_vec_int->p = (int*)realloc(p_vec_int->p, (p_vec_int->size + 1) * sizeof(int));
	p_vec_int->size = p_vec_int->size + 1;
	p_vec_int->p[p_vec_int->size - 1] = data;
	return(0);
}

int push_back_vec_float(struct vec_float *p_vec_float, float data)
{
	p_vec_float->pf = (float*)realloc(p_vec_float->pf, (p_vec_float->size + 1) * sizeof(float));
	p_vec_float->size = p_vec_float->size + 1;
	p_vec_float->pf[p_vec_float->size - 1] = data;
	return(0);
}

int destroy_vec_int(struct vec_int *p_vec_int)
{
	free(p_vec_int->p);
	free(p_vec_int);
	return(0);
}

int destroy_vec_float(struct vec_float *p_vec_float)
{
	free(p_vec_float->pf);
	free(p_vec_float);
	return (0);
}

void show_vec_float(struct vec_float *p_vec_float)
{
	int i;
	for (i = 0; i < p_vec_float->size; i++)
		printf("%f\n", p_vec_float->pf[i]);
}

void show_vec_int(struct vec_int *p_vec_int)
{
	int i;
	for (i = 0; i < p_vec_int->size; i++)
		printf("%d\n", p_vec_int->p[i]);
}

