package rtr.example.framebuffer;

//packages added by me
import android.content.Context;
import android.graphics.Color;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.GestureDetector;
import android.view.GestureDetector.OnGestureListener;
import android.view.GestureDetector.OnDoubleTapListener;
import rtr.example.framebuffer.GLESMacros;

import android.opengl.GLSurfaceView;
import android.opengl.GLES32;
import android.opengl.GLES20;

import javax.microedition.khronos.opengles.GL10;
import javax.microedition.khronos.egl.EGLConfig;

//for opengl buffers
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;

//for texture
import android.graphics.BitmapFactory;
import android.graphics.Bitmap;
import android.opengl.GLUtils;

//for matrix mathematics
import android.opengl.Matrix;

public class GLESView extends GLSurfaceView implements GLSurfaceView.Renderer, OnGestureListener, OnDoubleTapListener{
    private GestureDetector gestureDetector;
	private final Context context;
	
	private int vertexShaderObject;
	private int fragmentShaderObject;
	private int shaderProgramObject;

	private int[] framebufferObject = new int[1];
	private int[] color_texture = new int[1];
	private int[] depth_texture = new int[1];
	private int[] vao_cube 	= new int[1];
	private int[] vbo_position_cube	= new int[1];
	private int[] vbo_texcoord_cube	= new int[1];
	private int[] texture_kundali = new int[1];
	private int[] drawBuffers = new int[] {GLES32.GL_COLOR_ATTACHMENT0};

	private int mvpUniform;
	private int samplerUniform;

	private float cube_angle 	= 0.0f;
	
	private int checkAnimationCount = 0;
	private int gwidth, gheight;
	
	private float[] perspectiveProjectionMatrix = new float[16];		//4x4 matrix
	
	private boolean enableAnimation = false;

	//constructor
    public GLESView(Context drawingContext){
        super(drawingContext);
        context = drawingContext;
        
        setEGLContextClientVersion(3); 
        setRenderer(this);
        setRenderMode(GLSurfaceView.RENDERMODE_WHEN_DIRTY);

        gestureDetector =new GestureDetector(drawingContext, this, null, false );
        gestureDetector.setOnDoubleTapListener(this);
    }

    //handling 'onTouchEvent' is the most important,
	//because it triggers all gesture and tap events
	@Override
	public boolean onTouchEvent(MotionEvent event)
	{
		//code
		int eventaction = event.getAction();
		if(!gestureDetector.onTouchEvent(event))
			super.onTouchEvent(event);
		return(true);
	}

    //abstract method from OnDoubleTapListener so must be implemented
	@Override
	public boolean onDoubleTap(MotionEvent e)
	{
		return(true);
    }
    
    //abstract method from OnDoubleTapListener so must be implemented
	@Override
	public boolean onDoubleTapEvent(MotionEvent e)
	{
		//do not write any code here because already written in 'onDoubleTap'
		
		return(true);
    }
    
    //abstract  method from OnDoubleTapListener so must be implemented
	@Override
	public boolean onSingleTapConfirmed(MotionEvent e)
	{
	//	setText("Single Tap");
		checkAnimationCount++;
		if((checkAnimationCount%2)==0)
			enableAnimation = false;
		else
			enableAnimation = true;

		return(true);
    }
    
    //abstract  method from OnGestureListener so must be implemented
	@Override
	public boolean onDown(MotionEvent e)
	{
		//do not write any code here because already written in 'onSingleTapConfirmed'
		return(true);
    }
    
    //abstract  method from OnGestureListener so must be implemented
	@Override
	public boolean onFling(MotionEvent e1,MotionEvent e2, float velocityX, float velocityY)
	{
		return(true);
	}

	//abstract  method from OnGestureListener so must be implemented
	@Override
	public void onLongPress(MotionEvent e)
	{
		//setText("Long Press");
	}

	//abstract  method from OnGestureListener so must be implemented
	@Override
	public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY)
	{
		//setText("Scroll");
		uninitialize();
		System.exit(0);
		return(true);
	}

	//abstract  method from OnGestureListener so must be implemented
	@Override
	public void onShowPress(MotionEvent e)
	{
	
	}

	//abstract  method from OnGestureListener so must be implemented
	@Override
	public boolean onSingleTapUp(MotionEvent e)
	{
		return(true);
    }
    
    //implement GLSurfaceView.Renderer methods
    @Override
    public void onSurfaceCreated(GL10 gl, EGLConfig config){
		System.out.println("RTR: Called onSurfaceCreated");

        String version = gl.glGetString(GL10.GL_VERSION);
		System.out.println("RTR:" + version);
		
		version = gl.glGetString(GLES32.GL_SHADING_LANGUAGE_VERSION);
		System.out.println("RTR:" + version);
        initialize();
    }

    @Override
    public void onSurfaceChanged(GL10 unused, int width, int height)
    {
		System.out.println("RTR: Called onSurfaceChanged");

        resize(width, height);
    }

    @Override
    public void onDrawFrame(GL10 unused){
        display();
    }

	//our custom methods
	private void uninitialize()
	{
		System.out.println("RTR: Called uninitialize");
		
		if(shaderProgramObject != 0)
		{
			int[] shaderCount = new int[1];
			int shaderNumber;

			GLES32.glUseProgram(shaderProgramObject);

			GLES32.glGetProgramiv(shaderProgramObject, GLES32.GL_ATTACHED_SHADERS, shaderCount, 0);
			System.out.println("RTR: uninitialize => " + shaderCount[0]);
		
			int[] shaders =new int[shaderCount[0]];

			if(shaderCount[0] != 0)
			{
				GLES32.glGetAttachedShaders(shaderProgramObject, shaderCount[0], shaderCount, 0, shaders, 0);
				System.out.println("RTR: uninitialize => if shadercount != 0 ::" + shaderCount[0]);

				for(shaderNumber =0;shaderNumber<shaderCount[0]; shaderNumber++)
				{
					GLES32.glDetachShader(shaderProgramObject, shaders[shaderNumber]);
					GLES32.glDeleteShader(shaders[shaderNumber]);
					shaders[shaderNumber]=0;
				}
			}
			else{
				System.out.println("RTR: uninitialize => shader count is 0");
			}
			GLES32.glDeleteProgram(shaderProgramObject);
			shaderProgramObject = 0;
			GLES32.glUseProgram(0);
		}
		if(vbo_position_cube[0] != 0)
		{
			System.out.println("RTR: uninitialize => deleting vbo_position_cube");

			GLES32.glDeleteBuffers(1, vbo_position_cube, 0);
			vbo_position_cube[0] = 0;
		}
		if(vbo_texcoord_cube[0] != 0)
		{
			System.out.println("RTR: uninitialize => deleting vbo_texcoord_cube");

			GLES32.glDeleteBuffers(1, vbo_texcoord_cube, 0);
			vbo_texcoord_cube[0] = 0;
		}
		if(vao_cube[0] != 0)
		{
			System.out.println("RTR: uninitialize => deleting vao_cube");

			GLES32.glDeleteVertexArrays(1, vao_cube, 0);
			vao_cube[0]=0;
		}
	}

    private void initialize()
	{
		System.out.println("RTR: Called initialize");

		vertexShaderObject = GLES32.glCreateShader(GLES32.GL_VERTEX_SHADER);
		final String vertexShaderSourceCode = String.format
		(
		"	#version 320 es									" +
		"												\n	" +
		"	in vec4 vPosition;								" +
		"	in vec2 vTexcoord;								" +
		"												\n	" +
		"	uniform mat4 u_mvp_matrix;						" +
		"												\n	" +
		"	out vec2 out_texcoord;							" +
		"												\n	" +
		"	void main(void)									" +
		"	{												" +
		"		gl_Position = u_mvp_matrix * vPosition;		" +
		"		out_texcoord = vTexcoord;					" +
		"	}												"
		);

		GLES32.glShaderSource(vertexShaderObject, vertexShaderSourceCode);
		GLES32.glCompileShader(vertexShaderObject);
		int[] iShaderCompileStatus = new int[1];
		int[] iInfoLogLength = new int[1];
		String szInfoLog = null;

		GLES32.glGetShaderiv(vertexShaderObject, GLES32.GL_COMPILE_STATUS, iShaderCompileStatus, 0);

		if(iShaderCompileStatus[0] == GLES32.GL_FALSE)
		{
			GLES32.glGetShaderiv(vertexShaderObject, GLES32.GL_INFO_LOG_LENGTH, iInfoLogLength, 0);
			if(iInfoLogLength[0] > 0)
			{
				szInfoLog = GLES32.glGetShaderInfoLog(vertexShaderObject);
				System.out.println("RTR: VERTEX SHADER COMPILATION LOG:" + szInfoLog);
				uninitialize();
				System.exit(0);
			}
		}

		//fragment shader
		fragmentShaderObject = GLES32.glCreateShader(GLES32.GL_FRAGMENT_SHADER);

		final String fragmentShaderSourceCode = String.format
		(
		"	#version 320 es										" +
		"													\n	" +
		"	precision highp float;								" +
		"	precision mediump int;								" +
		"													\n	" +
		"	in vec2 out_texcoord;								" +
		"													\n	" +
		"	out vec4 FragColor;									" +
		"													\n	" +
		"	uniform sampler2D u_sampler;						" +
		"													\n	" +
		"	void main(void)										" +
		"	{													" +
		"		FragColor = texture(u_sampler, out_texcoord);	" +
		"	}													"
		);

		GLES32.glShaderSource(fragmentShaderObject, fragmentShaderSourceCode);
		GLES32.glCompileShader(fragmentShaderObject);
		iShaderCompileStatus[0] = 0;
		iInfoLogLength[0] = 0;
		szInfoLog = null;

		GLES32.glGetShaderiv(fragmentShaderObject, GLES32.GL_COMPILE_STATUS, iShaderCompileStatus, 0);
		
		if(iShaderCompileStatus[0] == GLES32.GL_FALSE)
		{
			GLES32.glGetShaderiv(fragmentShaderObject, GLES32.GL_INFO_LOG_LENGTH, iInfoLogLength, 0);

			if(iInfoLogLength[0] > 0)
			{
				szInfoLog = GLES32.glGetShaderInfoLog(fragmentShaderObject);
				System.out.println("RTR: FRAGMENT SHADER COMPILATION LOG:" + szInfoLog);
				uninitialize();
				System.exit(0);
			}
		}

		//shader program
		shaderProgramObject = GLES32.glCreateProgram();

		GLES32.glAttachShader(shaderProgramObject, vertexShaderObject);
		GLES32.glAttachShader(shaderProgramObject, fragmentShaderObject);

		//prelinking binding to attributes
		GLES32.glBindAttribLocation(shaderProgramObject, GLESMacros.PS_ATTRIBUTE_POSITION, "vPosition" );
		GLES32.glBindAttribLocation(shaderProgramObject, GLESMacros.PS_ATTRIBUTE_TEXCOORD0, "vTexcoord");
		
		//link
		GLES32.glLinkProgram(shaderProgramObject);
		
		//error checking
		int[] iProgramLinkStatus = new int[1];

		iInfoLogLength[0] = 0;
		szInfoLog = null;

		GLES32.glGetProgramiv(shaderProgramObject, GLES32.GL_LINK_STATUS, iProgramLinkStatus, 0);

		if(iProgramLinkStatus[0] == GLES32.GL_FALSE)
		{
			System.out.println("RTR SHADER PROGRAM LINKING FAILED");

			GLES32.glGetProgramiv(shaderProgramObject, GLES32.GL_INFO_LOG_LENGTH, iInfoLogLength, 0);
			if(iInfoLogLength[0] > 0)
			{
				szInfoLog = GLES32.glGetShaderInfoLog(shaderProgramObject);
				System.out.println("RTR SHADER PROGRAM LINKING LOG:" + szInfoLog);
				uninitialize();
				System.exit(0);
			}
		}

		//Get uniform location
		mvpUniform 		= GLES32.glGetUniformLocation(shaderProgramObject, "u_mvp_matrix");
		samplerUniform 	= GLES32.glGetUniformLocation(shaderProgramObject, "u_sampler");

		//pyramid position vertices
		final float[] cube_vertices = new float[]
		{
		 	 1.0f,  1.0f,  1.0f, 	 
			-1.0f,  1.0f,  1.0f, 	
			-1.0f, -1.0f,  1.0f, 	
			 1.0f, -1.0f,  1.0f, 	
			 1.0f,  1.0f, -1.0f, 	 
			 1.0f,  1.0f,  1.0f, 	
			 1.0f, -1.0f,  1.0f, 	
			 1.0f, -1.0f, -1.0f, 	
			 1.0f,  1.0f, -1.0f, 	 
			-1.0f,  1.0f, -1.0f, 	
			-1.0f, -1.0f, -1.0f, 	
			 1.0f, -1.0f, -1.0f, 	
			-1.0f,  1.0f, -1.0f, 	 
			-1.0f,  1.0f,  1.0f, 	
			-1.0f, -1.0f,  1.0f, 	
			-1.0f, -1.0f, -1.0f, 	
			 1.0f,  1.0f, -1.0f, 	 
			-1.0f,  1.0f, -1.0f, 	
			-1.0f,  1.0f,  1.0f, 	
			 1.0f,  1.0f,  1.0f, 	
			 1.0f, -1.0f, -1.0f, 	 
			-1.0f, -1.0f, -1.0f, 	
			-1.0f, -1.0f,  1.0f, 	
			 1.0f, -1.0f,  1.0f 	
		};

		final float[] cube_texcoord = new float[]
		{
		 	1.0f, 1.0f,
			0.0f, 1.0f,
			0.0f, 0.0f,
			1.0f, 0.0f,
			1.0f, 0.0f,
			1.0f, 1.0f,
			0.0f, 1.0f,
			0.0f, 0.0f,
		 	0.0f, 0.0f,
			0.0f, 1.0f,
			1.0f, 1.0f,
			1.0f, 0.0f,
			1.0f, 0.0f,
			0.0f, 0.0f,
			0.0f, 1.0f,
			1.0f, 1.0f,
		 	0.0f, 1.0f,
			0.0f, 0.0f,
			1.0f, 0.0f,
			1.0f, 1.0f,
			1.0f, 0.0f,
			0.0f, 0.0f,
			0.0f, 1.0f,
			1.0f, 1.0f
		};

		//*********************** FRAMEBUFFER ********************************//
		GLES32.glGenFramebuffers(1, framebufferObject, 0);
		GLES32.glBindFramebuffer(GLES32.GL_FRAMEBUFFER, framebufferObject[0]);

		GLES32.glGenTextures(1, color_texture, 0);
		GLES32.glBindTexture(GLES32.GL_TEXTURE_2D, color_texture[0]);
		GLES32.glTexParameteri(GLES32.GL_TEXTURE_2D, GLES32.GL_TEXTURE_MIN_FILTER, GLES32.GL_LINEAR);
		GLES32.glTexParameteri(GLES32.GL_TEXTURE_2D, GLES32.GL_TEXTURE_MAG_FILTER, GLES32.GL_LINEAR);
		GLES32.glTexImage2D(GLES32.GL_TEXTURE_2D, 0, GLES32.GL_RGBA, 800, 600, 0, GLES32.GL_RGBA, GLES32.GL_UNSIGNED_BYTE, null);
		GLES32.glTexStorage2D(GLES32.GL_TEXTURE_2D, 1, GLES32.GL_RGBA, 800, 600);
		GLES32.glBindTexture(GLES32.GL_TEXTURE_2D, 0);

		// Create a texture that will be our FBO's depth buffer
		GLES32.glGenTextures(1, depth_texture, 0);
		GLES32.glBindTexture(GLES32.GL_TEXTURE_2D, depth_texture[0]);
		GLES32.glTexImage2D(GLES32.GL_TEXTURE_2D, 0, GLES32.GL_RGBA, 800, 600, 0, GLES32.GL_RGBA, GLES32.GL_UNSIGNED_BYTE, null);
		GLES32.glTexStorage2D(GLES32.GL_TEXTURE_2D, 1, GLES32.GL_DEPTH_COMPONENT32F, 800, 600);
		GLES32.glBindTexture(GLES32.GL_TEXTURE_2D, 0);

		//attach to framebuffer
		GLES32.glFramebufferTexture2D(GLES32.GL_FRAMEBUFFER, GLES32.GL_COLOR_ATTACHMENT0, GLES32.GL_TEXTURE_2D,color_texture[0], 0);
		GLES32.glFramebufferTexture2D(GLES32.GL_FRAMEBUFFER, GLES32.GL_DEPTH_ATTACHMENT, GLES32.GL_TEXTURE_2D,depth_texture[0], 0);

		int FBOstatus = GLES20.glCheckFramebufferStatus(GLES20.GL_FRAMEBUFFER);
    	if (FBOstatus != GLES20.GL_FRAMEBUFFER_COMPLETE)
       	System.out.println("RTR: initFBO failed, status: " + FBOstatus);

		GLES32.glDrawBuffers(1, drawBuffers, 0);

		GLES32.glBindFramebuffer(GLES32.GL_FRAMEBUFFER, 0);
		
		//************************** CUBE ********************************//
		ByteBuffer byteBuffer;
		FloatBuffer floatBuffer;

		//create and bind with vao
		GLES32.glGenVertexArrays(1, vao_cube, 0);
		GLES32.glBindVertexArray(vao_cube[0]);

		//	* Position buffer *
		//Create buffer and bind with vbo
		GLES32.glGenBuffers(1, vbo_position_cube, 0);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vbo_position_cube[0]);

		//Allocate the buffer directly from native memory
		byteBuffer = ByteBuffer.allocateDirect(cube_vertices.length * 4); //4 is size of float in java
		byteBuffer.order(ByteOrder.nativeOrder());
		floatBuffer = byteBuffer.asFloatBuffer();
		floatBuffer.put(cube_vertices);
		floatBuffer.position(0);

		//data to the buffer 
		GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER, cube_vertices.length * 4, floatBuffer, GLES32.GL_STATIC_DRAW);
		
		// attrib pointer for position, color, normal, texcoord
		GLES32.glVertexAttribPointer(GLESMacros.PS_ATTRIBUTE_POSITION, 3, GLES32.GL_FLOAT, false, 0, 0);	
		GLES32.glEnableVertexAttribArray(GLESMacros.PS_ATTRIBUTE_POSITION);
		
		//unbind to buffer
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0);
		
		// * Texcoord buffer *
		//Create buffer and bind with vbo
		GLES32.glGenBuffers(1, vbo_texcoord_cube, 0);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vbo_texcoord_cube[0]);

		//Allocate the buffer directly from native memory
		byteBuffer = ByteBuffer.allocateDirect(cube_texcoord.length * 4); //4 is size of float in java
		byteBuffer.order(ByteOrder.nativeOrder());
		floatBuffer = byteBuffer.asFloatBuffer();
		floatBuffer.put(cube_texcoord);
		floatBuffer.position(0);

		//data to the buffer 
		GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER, cube_texcoord.length * 4, floatBuffer, GLES32.GL_STATIC_DRAW);
		
		// attrib pointer for position, color, normal, texcoord
		GLES32.glVertexAttribPointer(GLESMacros.PS_ATTRIBUTE_TEXCOORD0, 2, GLES32.GL_FLOAT, false, 0, 0);	
		GLES32.glEnableVertexAttribArray(GLESMacros.PS_ATTRIBUTE_TEXCOORD0);
		
		//unbind to buffer
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0);
		//unbind to cube vao
		GLES32.glBindVertexArray(0);

		GLES32.glClearColor(0.0f,0.0f,0.0f,1.0f);
		GLES32.glEnable(GLES32.GL_DEPTH_TEST);
		GLES32.glDepthFunc(GLES32.GL_LEQUAL);

		// load textures		
		texture_kundali[0] = loadTexture(R.raw.kundali);

		Matrix.setIdentityM(perspectiveProjectionMatrix,0);
		//no warmup call to resize as we are already in fullscreen. 
		//we dont run multiple app with screen sharing in android unlike windows.
    }

    private void resize(int width, int height)
    {
		gwidth = width;
		gheight = height;

        if(height<0)
        {
            height =1;
        }
		GLES32.glViewport(0,0,width, height);
		Matrix.perspectiveM(perspectiveProjectionMatrix, 0, 45.0f, (float)width/ (float)height, 0.1f, 100.0f);
    }

	private void update()
	{
		if(enableAnimation == true)
		{
			cube_angle = cube_angle + 0.2f;
		}
		if(cube_angle > 360.0f)
		{
			cube_angle = 0.0f;
		}
	}
	private void display()
	{
		
		//variable declarations
		float[] scaleMatrix = new float[16];
		float[] rotationMatrix = new float[16];
		float[] translationMatrix = new float[16];
		float[] modelViewMatrix = new float[16];
		float[] modelViewProjectionMatrix = new float[16];
		
		//code
		GLES32.glClear(GLES32.GL_COLOR_BUFFER_BIT | GLES32.GL_DEPTH_BUFFER_BIT);
		

		//making matrices identity
		Matrix.setIdentityM(modelViewMatrix,0);
		Matrix.setIdentityM(modelViewProjectionMatrix, 0);
		Matrix.setIdentityM(translationMatrix, 0);
		Matrix.setIdentityM(rotationMatrix, 0);
		Matrix.setIdentityM(scaleMatrix, 0);

		//matrix transformation
		// translation on negative z axis
		Matrix.translateM(translationMatrix, 0, 0.0f, 0.0f, -5.0f);
		Matrix.multiplyMM(modelViewMatrix, 0,
						  modelViewMatrix, 0,
						  translationMatrix, 0);

		// rotate across all axis
		Matrix.setRotateM(rotationMatrix, 0, cube_angle, 1.0f, 0.0f, 0.0f);
		Matrix.multiplyMM(modelViewMatrix, 0,
						  modelViewMatrix, 0,
						  rotationMatrix, 0);

		Matrix.setRotateM(rotationMatrix, 0, cube_angle, 0.0f, 1.0f, 0.0f);
		Matrix.multiplyMM(modelViewMatrix, 0,
						  modelViewMatrix, 0,
						  rotationMatrix, 0);

		Matrix.setRotateM(rotationMatrix, 0, cube_angle, 0.0f, 0.0f, 1.0f);
		Matrix.multiplyMM(modelViewMatrix, 0,
						  modelViewMatrix, 0,
						  rotationMatrix, 0);

		// final model view projection matrix
		Matrix.multiplyMM(modelViewProjectionMatrix, 0,
						  perspectiveProjectionMatrix, 0,
						  modelViewMatrix, 0);

		//Bind to framebuffer
		final float[] clearBufferColor = new float[] {0.0f, 1.0f, 0.0f, 1.0f};
		final float[] clearBufferDepth = new float[] {1.0f, 1.0f, 1.0f, 0.0f};
		
		GLES32.glBindFramebuffer(GLES32.GL_FRAMEBUFFER, framebufferObject[0]);
		GLES32.glClearBufferfv(GLES32.GL_COLOR, 0, clearBufferColor, 0);
		GLES32.glClearBufferfv(GLES32.GL_DEPTH, 0, clearBufferDepth, 0);

		GLES32.glUseProgram(shaderProgramObject);

		//send necessary matrices to shader in respective uniforms
		GLES32.glUniformMatrix4fv(mvpUniform, 1, false, modelViewProjectionMatrix, 0);

		//ABU
		//GLES32.glActiveTexture(GLES32.GL_TEXTURE0);
		GLES32.glBindTexture(GLES32.GL_TEXTURE_2D, texture_kundali[0]);
		//GLES32.glUniform1i(samplerUniform, 0);

		// bind vao
		GLES32.glBindVertexArray(vao_cube[0]);

		GLES32.glDrawArrays(GLES32.GL_TRIANGLE_FAN, 0, 4);
		GLES32.glDrawArrays(GLES32.GL_TRIANGLE_FAN, 4, 4);
		GLES32.glDrawArrays(GLES32.GL_TRIANGLE_FAN, 8, 4);
		GLES32.glDrawArrays(GLES32.GL_TRIANGLE_FAN, 12, 4);
		GLES32.glDrawArrays(GLES32.GL_TRIANGLE_FAN, 16, 4);
		GLES32.glDrawArrays(GLES32.GL_TRIANGLE_FAN, 20, 4);

		//unbind with vao
		GLES32.glBindVertexArray(0);
		GLES32.glViewport(0,0,gwidth, gheight);
		//unuse program
		GLES32.glUseProgram(0);
		GLES32.glBindTexture(GLES32.GL_TEXTURE_2D, 0);
		GLES32.glBindFramebuffer(GLES32.GL_FRAMEBUFFER, 0);
		// ---------------------------------------------------------------

		//making matrices identity
		Matrix.setIdentityM(modelViewMatrix,0);
		Matrix.setIdentityM(modelViewProjectionMatrix, 0);
		Matrix.setIdentityM(translationMatrix, 0);
		Matrix.setIdentityM(rotationMatrix, 0);
		Matrix.setIdentityM(scaleMatrix, 0);

		//matrix transformation
		// translation on negative z axis
		Matrix.translateM(translationMatrix, 0, 0.0f, 0.0f, -5.0f);
		Matrix.multiplyMM(modelViewMatrix, 0,
						  modelViewMatrix, 0,
						  translationMatrix, 0);

		// rotate across all axis
		Matrix.setRotateM(rotationMatrix, 0, cube_angle-0.1f, 1.0f, 0.0f, 0.0f);
		Matrix.multiplyMM(modelViewMatrix, 0,
						  modelViewMatrix, 0,
						  rotationMatrix, 0);

		Matrix.setRotateM(rotationMatrix, 0, cube_angle-0.1f, 0.0f, 1.0f, 0.0f);
		Matrix.multiplyMM(modelViewMatrix, 0,
						  modelViewMatrix, 0,
						  rotationMatrix, 0);

		Matrix.setRotateM(rotationMatrix, 0, cube_angle-0.1f, 0.0f, 0.0f, 1.0f);
		Matrix.multiplyMM(modelViewMatrix, 0,
						  modelViewMatrix, 0,
						  rotationMatrix, 0);

		// final model view projection matrix
		Matrix.multiplyMM(modelViewProjectionMatrix, 0,
						  perspectiveProjectionMatrix, 0,
						  modelViewMatrix, 0);

		GLES32.glUseProgram(shaderProgramObject);

		GLES32.glUniformMatrix4fv(mvpUniform, 1, false, modelViewProjectionMatrix, 0);

		GLES32.glBindTexture(GLES32.GL_TEXTURE_2D, color_texture[0]);

		GLES32.glBindVertexArray(vao_cube[0]);

		GLES32.glDrawArrays(GLES32.GL_TRIANGLE_FAN, 0, 4);
		GLES32.glDrawArrays(GLES32.GL_TRIANGLE_FAN, 4, 4);
		GLES32.glDrawArrays(GLES32.GL_TRIANGLE_FAN, 8, 4);
		GLES32.glDrawArrays(GLES32.GL_TRIANGLE_FAN, 12, 4);
		GLES32.glDrawArrays(GLES32.GL_TRIANGLE_FAN, 16, 4);
		GLES32.glDrawArrays(GLES32.GL_TRIANGLE_FAN, 20, 4);

		GLES32.glBindVertexArray(0);
		GLES32.glBindTexture(GLES32.GL_TEXTURE_2D, 0);

		GLES32.glViewport(0, 0, 800,600);
		GLES32.glUseProgram(0);

        requestRender();

		update();
	}
	
	private int loadTexture(int imageFileResourceId)
	{
		System.out.println("RTR: In loadTexture");
		int[] texture = new int[1];
		BitmapFactory.Options options = new BitmapFactory.Options();

		options.inScaled = false;

		Bitmap bitmap = BitmapFactory.decodeResource(context.getResources(),imageFileResourceId);
		GLES32.glGenTextures(1, texture, 0);
		GLES32.glBindTexture(GLES32.GL_TEXTURE_2D, texture[0]);
		GLES32.glPixelStorei(GLES32.GL_UNPACK_ALIGNMENT, 1);
		GLES32.glTexParameteri(GLES32.GL_TEXTURE_2D, GLES32.GL_TEXTURE_MAG_FILTER, GLES32.GL_LINEAR);
		GLES32.glTexParameteri(GLES32.GL_TEXTURE_2D, GLES32.GL_TEXTURE_MIN_FILTER, GLES32.GL_LINEAR_MIPMAP_LINEAR);
		GLUtils.texImage2D(GLES32.GL_TEXTURE_2D, //target
			0,		//mipmap levels
			bitmap,	//rest of the parameter of glTexImage2d are present inside bitmap
			0);	//border width
		GLES32.glGenerateMipmap(GLES32.GL_TEXTURE_2D);
		GLES32.glBindTexture(GLES32.GL_TEXTURE_2D, 0);
		return(texture[0]);
	}

}

