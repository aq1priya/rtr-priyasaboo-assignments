package rtr.example.pctexture;

//packages added by me
import android.content.Context;
import android.graphics.Color;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.GestureDetector;
import android.view.GestureDetector.OnGestureListener;
import rtr.example.pctexture.GLESMacros;
import android.view.GestureDetector.OnDoubleTapListener;

import android.opengl.GLSurfaceView;
import android.opengl.GLES32;

import javax.microedition.khronos.opengles.GL10;
import javax.microedition.khronos.egl.EGLConfig;

//for opengl buffers
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;

//for matrix mathematics
import android.opengl.Matrix;

//for texture
import android.graphics.BitmapFactory;
import android.graphics.Bitmap;
import android.opengl.GLUtils;

public class GLESView extends GLSurfaceView implements GLSurfaceView.Renderer, OnGestureListener, OnDoubleTapListener{
    private GestureDetector gestureDetector;
    private final Context context;
	private int vertexShaderObject;
	private int fragmentShaderObject;
	private int shaderProgramObject;
	private int samplerUniform;
	private int mvpUniform;
	private int[] texture_stone = new int[1];
	private int[] texture_kundali = new int[1];
	private int[] vao_pyramid = new int[1];
	private int[] vao_cube = new int[1];
	private int[] vbo_texcoord_pyramid = new int[1];
	private int[] vbo_position_pyramid = new int[1];
	private int[] vbo_texcoord_cube	= new int[1];
	private int[] vbo_position_cube = new int[1];
	private float[] perspectiveProjectionMatrix = new float[16];		//4x4 matrix
	private	float cube_rotationangle = 0.0f;
	private float pyramid_rotationangle = 0.0f;

	//constructor
    public GLESView(Context drawingContext){
        super(drawingContext);
        context = drawingContext;
        
        setEGLContextClientVersion(3);
        setRenderer(this);
        setRenderMode(GLSurfaceView.RENDERMODE_WHEN_DIRTY);

        gestureDetector =new GestureDetector(drawingContext, this, null, false );
        gestureDetector.setOnDoubleTapListener(this);
    }

    //handling 'onTouchEvent' is the most important,
	//because it triggers all gesture and tap events
	@Override
	public boolean onTouchEvent(MotionEvent event)
	{
		//code
		int eventaction = event.getAction();
		if(!gestureDetector.onTouchEvent(event))
			super.onTouchEvent(event);
		return(true);
	}

    //abstract method from OnDoubleTapListener so must be implemented
	@Override
	public boolean onDoubleTap(MotionEvent e)
	{
		//setText("Double Tap");
		return(true);
    }
    
    //abstract method from OnDoubleTapListener so must be implemented
	@Override
	public boolean onDoubleTapEvent(MotionEvent e)
	{
		//do not write any code here because already written in 'onDoubleTap'
		return(true);
    }
    
    //abstract  method from OnDoubleTapListener so must be implemented
	@Override
	public boolean onSingleTapConfirmed(MotionEvent e)
	{
	//	setText("Single Tap");
		return(true);
    }
    
    //abstract  method from OnGestureListener so must be implemented
	@Override
	public boolean onDown(MotionEvent e)
	{
		//do not write any code here because already written in 'onSingleTapConfirmed'
		return(true);
    }
    
    //abstract  method from OnGestureListener so must be implemented
	@Override
	public boolean onFling(MotionEvent e1,MotionEvent e2, float velocityX, float velocityY)
	{
		return(true);
	}

	//abstract  method from OnGestureListener so must be implemented
	@Override
	public void onLongPress(MotionEvent e)
	{
		//setText("Long Press");
	}

	//abstract  method from OnGestureListener so must be implemented
	@Override
	public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY)
	{
		//setText("Scroll");
		uninitialize();
		System.exit(0);
		return(true);
	}

	//abstract  method from OnGestureListener so must be implemented
	@Override
	public void onShowPress(MotionEvent e)
	{
	
	}

	//abstract  method from OnGestureListener so must be implemented
	@Override
	public boolean onSingleTapUp(MotionEvent e)
	{
		return(true);
    }
    
    //implement GLSurfaceView.Renderer methods
    @Override
    public void onSurfaceCreated(GL10 gl, EGLConfig config){
		System.out.println("RTR: Called onSurfaceCreated");

        String version = gl.glGetString(GL10.GL_VERSION);
		System.out.println("RTR:" + version);
		
		version = gl.glGetString(GLES32.GL_SHADING_LANGUAGE_VERSION);
		System.out.println("RTR:" + version);
        initialize();
    }

    @Override
    public void onSurfaceChanged(GL10 unused, int width, int height)
    {
		System.out.println("RTR: Called onSurfaceChabged");

        resize(width, height);
    }

    @Override
    public void onDrawFrame(GL10 unused){
        display();
    }

	//our custom methods
	private void uninitialize()
	{
		System.out.println("RTR: Called uninitialize");
		
		if(shaderProgramObject != 0)
		{
			int[] shaderCount = new int[1];
			int shaderNumber;

			GLES32.glUseProgram(shaderProgramObject);

			GLES32.glGetProgramiv(shaderProgramObject, GLES32.GL_ATTACHED_SHADERS, shaderCount, 0);
			System.out.println("RTR: uninitialize => " + shaderCount[0]);
		
			int[] shaders =new int[shaderCount[0]];

			if(shaderCount[0] != 0)
			{
				GLES32.glGetAttachedShaders(shaderProgramObject, shaderCount[0], shaderCount, 0, shaders, 0);
				System.out.println("RTR: uninitialize => if shadercount != 0 ::" + shaderCount[0]);

				for(shaderNumber =0;shaderNumber<shaderCount[0]; shaderNumber++)
				{
					GLES32.glDetachShader(shaderProgramObject, shaders[shaderNumber]);
					GLES32.glDeleteShader(shaders[shaderNumber]);
					shaders[shaderNumber]=0;
				}
			}
			else{
				System.out.println("RTR: uninitialize => shader count is 0");
			}
			GLES32.glDeleteProgram(shaderProgramObject);
			shaderProgramObject = 0;
			GLES32.glUseProgram(0);
		}
		if(vbo_texcoord_pyramid[0] != 0)
		{
			System.out.println("RTR: uninitialize => deleting vbo_color_pyramid");

			GLES32.glDeleteBuffers(1, vbo_texcoord_pyramid, 0);
			vbo_texcoord_pyramid[0] = 0;
		}
		if(vbo_position_pyramid[0] != 0)
		{
			System.out.println("RTR: uninitialize => deleting vbo_position_pyramid");

			GLES32.glDeleteBuffers(1, vbo_position_pyramid, 0);
			vbo_position_pyramid[0] = 0;
		}
		if(vao_pyramid[0] != 0)
		{
			System.out.println("RTR: uninitialize => deleting vao_pyramid");

			GLES32.glDeleteVertexArrays(1, vao_pyramid, 0);
			vao_pyramid[0]=0;
		}
		if(vbo_texcoord_cube[0] != 0)
		{
			System.out.println("RTR: uninitialize => deleting vbo_color_cube");

			GLES32.glDeleteBuffers(1, vbo_texcoord_cube, 0);
			vbo_texcoord_cube[0] = 0;
		}
		if(vbo_position_cube[0] != 0)
		{
			System.out.println("RTR: uninitialize => deleting vbo_position_cube");

			GLES32.glDeleteBuffers(1, vbo_position_cube, 0);
			vbo_position_cube[0] = 0;
		}
		if(vao_cube[0] != 0)
		{
			System.out.println("RTR: uninitialize => deleting vao_cube");

			GLES32.glDeleteVertexArrays(1, vao_cube, 0);
			vao_cube[0]=0;
		}
	}

    private void initialize()
	{
		System.out.println("RTR: Called initialize");

		vertexShaderObject = GLES32.glCreateShader(GLES32.GL_VERTEX_SHADER);
		final String vertexShaderSourceCode = String.format
		(
			"#version 320 es" +
			"\n" +
			"in vec4 vPosition;" +
			"in vec2 vTexcoord;" +
			"out vec2 out_texcoord;" +
			"uniform mat4 u_mvp_matrix;" +
			"void main(void)" +
			"{" +
			"gl_Position = u_mvp_matrix * vPosition;" +
			"out_texcoord = vTexcoord;" +
			"}"
		);
		GLES32.glShaderSource(vertexShaderObject, vertexShaderSourceCode);
		GLES32.glCompileShader(vertexShaderObject);
		int[] iShaderCompileStatus = new int[1];
		int[] iInfoLogLength = new int[1];
		String szInfoLog = null;

		GLES32.glGetShaderiv(vertexShaderObject, GLES32.GL_COMPILE_STATUS, iShaderCompileStatus, 0);

		if(iShaderCompileStatus[0] == GLES32.GL_FALSE)
		{
			GLES32.glGetShaderiv(vertexShaderObject, GLES32.GL_INFO_LOG_LENGTH, iInfoLogLength, 0);
			if(iInfoLogLength[0] > 0)
			{
				szInfoLog = GLES32.glGetShaderInfoLog(vertexShaderObject);
				System.out.println("RTR: VERTEX SHADER COMPILATION LOG:" + szInfoLog);
				uninitialize();
				System.exit(0);
			}
		}

		//fragment shader
		/*precision :  we don't need to set presion for desktop application. 
		There are 3 hardware unit present in ARM to perform floating point maths calculation; 
		i.e FPU(floating point unit), SIMD(Single Instruction Multiple Data unit), NEON()
		in embedded power and memory is not a luxury. so ypu can not use any precision anywhere.
		eg. low presision and medium precision for int is acceptable but using high precisio for integer calucaltion 
		is very battery consuming which is not acceptable. High precision calucation were completed from NEON
		Vertex shader is by default highp
		int is lowp or mediump
		in fragment shader , user must specify it, as maximum processing happens in fragment shader*/
		fragmentShaderObject = GLES32.glCreateShader(GLES32.GL_FRAGMENT_SHADER);
		final String fragmentShaderSourceCode = String.format
		(
			"#version 320 es" +
			"\n" +
			"precision highp float;" +
			"in vec2 out_texcoord;" +
			"out vec4 FragColor;" +
			"uniform sampler2D u_sampler;" +
			"void main(void)" +
			"{" +
			"FragColor = texture(u_sampler, out_texcoord);" +
			"}"
		);

		GLES32.glShaderSource(fragmentShaderObject, fragmentShaderSourceCode);
		GLES32.glCompileShader(fragmentShaderObject);
		iShaderCompileStatus[0] = 0;
		iInfoLogLength[0] = 0;
		szInfoLog = null;

		GLES32.glGetShaderiv(fragmentShaderObject, GLES32.GL_COMPILE_STATUS, iShaderCompileStatus, 0);
		
		if(iShaderCompileStatus[0] == GLES32.GL_FALSE)
		{
			GLES32.glGetShaderiv(fragmentShaderObject, GLES32.GL_INFO_LOG_LENGTH, iInfoLogLength, 0);

			if(iInfoLogLength[0] > 0)
			{
				szInfoLog = GLES32.glGetShaderInfoLog(fragmentShaderObject);
				System.out.println("RTR: FRAGMENT SHADER COMPILATION LOG:" + szInfoLog);
				uninitialize();
				System.exit(0);
			}
		}

		int[] shaderCount = new int[1];

		//shader program
		shaderProgramObject = GLES32.glCreateProgram();

		GLES32.glGetProgramiv(shaderProgramObject, GLES32.GL_ATTACHED_SHADERS, shaderCount, 0);
		System.out.println("RTR: initialize => BEFORE ATTACHING SHADERS shader count is :: " + shaderCount[0]);

		GLES32.glAttachShader(shaderProgramObject, vertexShaderObject);
		GLES32.glAttachShader(shaderProgramObject, fragmentShaderObject);

		GLES32.glGetProgramiv(shaderProgramObject, GLES32.GL_ATTACHED_SHADERS, shaderCount, 0);
		System.out.println("RTR: initialize => AFTER ATTACHING SHADERS shader count is :: " + shaderCount[0]);

		//prelinking binding to attributes
		GLES32.glBindAttribLocation(shaderProgramObject,GLESMacros.AMC_ATTRIBUTE_POSITION, "vPosition" );
		GLES32.glBindAttribLocation(shaderProgramObject, GLESMacros.AMC_ATTRIBUTE_TEXCOORD0, "vTexcoord");
		
		//link
		GLES32.glLinkProgram(shaderProgramObject);
		
		//error checking
		int[] iProgramLinkStatus = new int[1];

		iInfoLogLength[0] = 0;
		szInfoLog = null;

		GLES32.glGetProgramiv(shaderProgramObject, GLES32.GL_LINK_STATUS, iProgramLinkStatus, 0);

		if(iProgramLinkStatus[0] == GLES32.GL_FALSE)
		{
			GLES32.glGetProgramiv(shaderProgramObject, GLES32.GL_INFO_LOG_LENGTH, iInfoLogLength, 0);
			if(iInfoLogLength[0] > 0)
			{
				szInfoLog = GLES32.glGetShaderInfoLog(shaderProgramObject);
				System.out.println("RTR SHADER PROGRAM LINKING LOG:" + szInfoLog);
				uninitialize();
				System.exit(0);
			}
		}

		//Get uniform location
		mvpUniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_mvp_matrix");
		samplerUniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_sampler");

		// pyramid position vertices array, as it is inline initialization, no need of specifying size of array on rhs 
		final float[] pyramidVertices = new float[]
		{
			0.0f, 1.0f, 0.0f, -1.0f, -1.0f, 1.0f, 1.0f, -1.0f, 1.0f,
			0.0f, 1.0f, 0.0f, 1.0f, -1.0f, 1.0f, 1.0f, -1.0f, -1.0f,
			0.0f, 1.0f, 0.0f, 1.0f, -1.0f, -1.0f, -1.0f, -1.0f, -1.0f,
			0.0f, 1.0f, 0.0f, -1.0f, -1.0f, -1.0f, -1.0f, -1.0f, 1.0f
		};//front-right-back-left

		//cube position vertices
		final float[] cubeVertices = new float[]
		{
		1.0f, 1.0f, 1.0f, -1.0f, 1.0f, 1.0f, -1.0f, -1.0f, 1.0f, 1.0f, -1.0f, 1.0f,
		1.0f, 1.0f, -1.0f, 1.0f, 1.0f, 1.0f, 1.0f, -1.0f, 1.0f, 1.0f, -1.0f, -1.0f,
		1.0f, 1.0f, -1.0f, -1.0f, 1.0f, -1.0f, -1.0f, -1.0f, -1.0f, 1.0f, -1.0f, -1.0f,
		-1.0f, 1.0f, -1.0f,	-1.0f, 1.0f, 1.0f, -1.0f, -1.0f, 1.0f, -1.0f, -1.0f, -1.0f,
		1.0f, 1.0f, -1.0f, -1.0f, 1.0f, -1.0f, -1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f,
		1.0f, -1.0f, -1.0f, -1.0f, -1.0f, -1.0f, -1.0f, -1.0f, 1.0f, 1.0f, -1.0f, 1.0f
		};//front-right-back-left-top-bottom

		// pyramid color values
		final float[] pyramidTexcoord = new float[]
		{
		0.5f, 1.0f, 0.0f, 0.0f, 1.0f, 0.0f,
		0.5f, 1.0f, 1.0f, 0.0f, 0.0f, 0.0f,
		0.5f, 1.0f, 1.0f, 0.0f, 0.0f, 0.0f,
		0.5f, 1.0f, 0.0f, 0.0f, 1.0f, 0.0f
		};


		//cube color values
		final float[] cubeTexcoord = new float[]
		{
		1.0f, -1.0f, 0.0f, -1.0f, 0.0f, 0.0f, 1.0f, 0.0f,
		1.0f, 0.0f, 1.0f, -1.0f, 0.0f, -1.0f, 0.0f, 0.0f,
		0.0f, 0.0f, 0.0f, -1.0f, 1.0f, -1.0f, 1.0f, 0.0f,
		1.0f, 0.0f, 0.0f, 0.0f, 0.0f, -1.0f, 1.0f, -1.0f,
		0.0f, -1.0f, 0.0f, 0.0f, 1.0f, 0.0f, 1.0f, -1.0f,
		1.0f, 0.0f, 0.0f, 0.0f, 0.0f, -1.0f, 1.0f, -1.0f
		};

		//*******************************TRIANGLE******************************** */
		//create and bind with vao
		GLES32.glGenVertexArrays(1, vao_pyramid, 0);
		GLES32.glBindVertexArray(vao_pyramid[0]);

		//Create buffer for position and bind with vbo
		GLES32.glGenBuffers(1, vbo_position_pyramid, 0);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vbo_position_pyramid[0]);

		//Allocate the buffer directly from native memory
		ByteBuffer byteBuffer_pyramid_pos = ByteBuffer.allocateDirect(pyramidVertices.length*4); //4 is size of float in java
		//Arrange the byte order of buffer to the native byte order
		byteBuffer_pyramid_pos.order(ByteOrder.nativeOrder());
		//create the float type buffer and convert our byte type buffer into float type buffer
		FloatBuffer floatBuffer_pyramid_pos = byteBuffer_pyramid_pos.asFloatBuffer();
		//Now put your array into the "COOKED" buffer
		floatBuffer_pyramid_pos.put(pyramidVertices);
		//set the array at 0th position of the buffer as our array is not interleaved
		floatBuffer_pyramid_pos.position(0);

		//data to the buffer
		GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER, pyramidVertices.length * 4, floatBuffer_pyramid_pos,GLES32.GL_STATIC_DRAW);
		GLES32.glVertexAttribPointer(GLESMacros.AMC_ATTRIBUTE_POSITION, 
										3,				// break data after x y z, i.e. 3
										GLES32.GL_FLOAT, // type of data
										false, 			// normalize?
										0, 				//stride?-no
										0 );			//offset?-no(windows : NULL, in java null is not #define 0) therefore 0
		GLES32.glEnableVertexAttribArray(GLESMacros.AMC_ATTRIBUTE_POSITION);
		
		//unbind to buffer
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0);

		//create buffer for texture and bind with vbo
		GLES32.glGenBuffers(1, vbo_texcoord_pyramid, 0);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vbo_texcoord_pyramid[0]);

		//allocate the buffer directly from native memory
		ByteBuffer byteBuffer_pyramid_tex = ByteBuffer.allocateDirect(pyramidTexcoord.length * 4);
		byteBuffer_pyramid_tex.order(ByteOrder.nativeOrder());
		FloatBuffer floatBuffer_pyramid_tex = byteBuffer_pyramid_tex.asFloatBuffer();
		floatBuffer_pyramid_tex.put(pyramidTexcoord);
		floatBuffer_pyramid_tex.position(0);

		//color data to the buffer
		GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER, pyramidTexcoord.length * 4, floatBuffer_pyramid_tex, GLES32.GL_STATIC_DRAW);
		GLES32.glVertexAttribPointer(GLESMacros.AMC_ATTRIBUTE_TEXCOORD0,
										2,
										GLES32.GL_FLOAT,
										false,
										0,
										0);
		GLES32.glEnableVertexAttribArray(GLESMacros.AMC_ATTRIBUTE_TEXCOORD0);

		//unbind to color buffer
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER,0);

		//unbind to pyramid vao
		GLES32.glBindVertexArray(0);

		//*******************************SQUARE************************************//

		//create and bind with vao
		GLES32.glGenVertexArrays(1, vao_cube, 0);
		GLES32.glBindVertexArray(vao_cube[0]);

		//Create buffer for position and bind with vbo
		GLES32.glGenBuffers(1, vbo_position_cube, 0);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vbo_position_cube[0]);

		//Allocate the buffer directly from native memory
		ByteBuffer byteBuffer_cube_pos = ByteBuffer.allocateDirect(cubeVertices.length*4); //4 is size of float in java
		//Arrange the byte order of buffer to the native byte order
		byteBuffer_cube_pos.order(ByteOrder.nativeOrder());
		//create the float type buffer and convert our byte type buffer into float type buffer
		FloatBuffer floatBuffer_cube_pos = byteBuffer_cube_pos.asFloatBuffer();
		//Now put your array into the "COOKED" buffer
		floatBuffer_cube_pos.put(cubeVertices);
		//set the array at 0th position of the buffer as our array is not interleaved
		floatBuffer_cube_pos.position(0);

		//data to the buffer
		GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER, cubeVertices.length * 4, floatBuffer_cube_pos,GLES32.GL_STATIC_DRAW);
		GLES32.glVertexAttribPointer(GLESMacros.AMC_ATTRIBUTE_POSITION, 
										3,				// break data after x y z, i.e. 3
										GLES32.GL_FLOAT, // type of data
										false, 			// normalize?
										0, 				//stride?-no
										0 );			//offset?-no(windows : NULL, in java null is not #define 0) therefore 0
		GLES32.glEnableVertexAttribArray(GLESMacros.AMC_ATTRIBUTE_POSITION);
		
		//unbind to buffer
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0);

		//create buffer or texcoord and bind with vbo
		GLES32.glGenBuffers(1, vbo_texcoord_cube, 0);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vbo_texcoord_cube[0]);

		//allocate the buffer directly from native memory
		ByteBuffer byteBuffer_cube_tex = ByteBuffer.allocateDirect(cubeTexcoord.length * 4);
		byteBuffer_cube_tex.order(ByteOrder.nativeOrder());
		FloatBuffer floatBuffer_cube_tex = byteBuffer_cube_tex.asFloatBuffer();
		floatBuffer_cube_tex.put(cubeTexcoord);
		floatBuffer_cube_tex.position(0);

		//color data to the buffer
		GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER, cubeTexcoord.length * 4, floatBuffer_cube_tex, GLES32.GL_STATIC_DRAW);
		GLES32.glVertexAttribPointer(GLESMacros.AMC_ATTRIBUTE_TEXCOORD0,
										2,
										GLES32.GL_FLOAT,
										false,
										0,
										0);
		GLES32.glEnableVertexAttribArray(GLESMacros.AMC_ATTRIBUTE_TEXCOORD0);

		//unbind to color buffer
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER,0);

		//unbind to pyramid vao
		GLES32.glBindVertexArray(0);

		texture_stone[0] = loadTexture(R.raw.stone);
		texture_kundali[0] = loadTexture(R.raw.kundali);

		GLES32.glClearColor(0.0f,0.0f,0.0f,1.0f);
		GLES32.glEnable(GLES32.GL_DEPTH_TEST);
		GLES32.glDepthFunc(GLES32.GL_LEQUAL);
		GLES32.glDisable(GLES32.GL_CULL_FACE);

		Matrix.setIdentityM(perspectiveProjectionMatrix,0);
		//no warmup call to resize as we are already in fullscreen. 
		//we dont run multiple app with screen sharing in android unlike windows.
    }

    private void resize(int width, int height)
    {
        if(height<0)
        {
            height =1;
        }
		GLES32.glViewport(0,0,width, height);
		Matrix.perspectiveM(perspectiveProjectionMatrix, 0, 45.0f, (float)width/ (float)height, 0.1f, 100.0f);
    }

	private void display()
	{
		//variable declarations
		float[] modelViewMatrix = new float[16];
		float[] modelViewProjectionMatrix = new float[16];
		float[] rotationMatrix = new float[16];
		float[] translationMatrix = new float[16];
		float[] scaleMatrix = new float[16];

		//code
		GLES32.glClear(GLES32.GL_COLOR_BUFFER_BIT|GLES32.GL_DEPTH_BUFFER_BIT);
		GLES32.glUseProgram(shaderProgramObject);

		//*********** DRAW PYRAMID ***************//
		//making martices identity
		Matrix.setIdentityM(modelViewMatrix, 0);
		Matrix.setIdentityM(modelViewProjectionMatrix, 0);
		Matrix.setIdentityM(rotationMatrix, 0);
		Matrix.setIdentityM(translationMatrix, 0);

		//matrix transformation
		Matrix.translateM(translationMatrix, 0, -1.5f, 0.0f, -5.0f);
		Matrix.setRotateM(rotationMatrix, 0, pyramid_rotationangle, 0.0f, 1.0f, 0.0f);
		Matrix.multiplyMM(modelViewMatrix, 0,
						  modelViewMatrix, 0,
						  translationMatrix,0);
	
		Matrix.multiplyMM(modelViewMatrix, 0,
						  modelViewMatrix, 0,
						  rotationMatrix, 0);
		
		Matrix.multiplyMM(modelViewProjectionMatrix, 0,
						  perspectiveProjectionMatrix, 0,
						  modelViewMatrix, 0);

		//send matrix to shader
		GLES32.glUniformMatrix4fv(mvpUniform, 1, false, modelViewProjectionMatrix, 0);

		//ABU
		GLES32.glActiveTexture(GLES32.GL_TEXTURE0);
		GLES32.glBindTexture(GLES32.GL_TEXTURE_2D, texture_stone[0]);
		GLES32.glUniform1i(samplerUniform, 0);

		//bind with vao
		GLES32.glBindVertexArray(vao_pyramid[0]);
		GLES32.glDrawArrays(GLES32.GL_TRIANGLES, 0, 12);
		//unbind with vao
		GLES32.glBindVertexArray(0);

		//*************** DRAW CUBE ****************//
		//making matrices identity
		Matrix.setIdentityM(modelViewMatrix, 0);
		Matrix.setIdentityM(modelViewProjectionMatrix, 0);
		Matrix.setIdentityM(rotationMatrix, 0);
		Matrix.setIdentityM(translationMatrix, 0);
		Matrix.setIdentityM(scaleMatrix, 0);

		//matrix transformation
		Matrix.translateM(translationMatrix, 0, 1.5f, 0.0f, -5.0f);
		Matrix.scaleM(scaleMatrix, 0, 0.85f, 0.85f, 0.85f);
		Matrix.setRotateM(rotationMatrix, 0, cube_rotationangle, 1.0f, 1.0f, 1.0f);
		
		Matrix.multiplyMM(modelViewMatrix, 0,
						  modelViewMatrix, 0,
						  translationMatrix,0);
		Matrix.multiplyMM(modelViewMatrix, 0,
						  modelViewMatrix, 0,
						  scaleMatrix, 0);
		Matrix.multiplyMM(modelViewMatrix, 0,
						  modelViewMatrix, 0,
						  rotationMatrix, 0);
		Matrix.multiplyMM(modelViewProjectionMatrix, 0,
						  perspectiveProjectionMatrix, 0,
						  modelViewMatrix, 0);

		//send matrix to shader
		GLES32.glUniformMatrix4fv(mvpUniform, 1, false, modelViewProjectionMatrix, 0);

		//ABU
		GLES32.glActiveTexture(GLES32.GL_TEXTURE0);
		GLES32.glBindTexture(GLES32.GL_TEXTURE_2D, texture_kundali[0]);
		GLES32.glUniform1i(samplerUniform, 0);

		//bind with vao
		GLES32.glBindVertexArray(vao_cube[0]);
		GLES32.glDrawArrays(GLES32.GL_TRIANGLE_FAN, 0, 4);
		GLES32.glDrawArrays(GLES32.GL_TRIANGLE_FAN, 4, 4);
		GLES32.glDrawArrays(GLES32.GL_TRIANGLE_FAN, 8, 4);
		GLES32.glDrawArrays(GLES32.GL_TRIANGLE_FAN, 12, 4);
		GLES32.glDrawArrays(GLES32.GL_TRIANGLE_FAN, 16, 4);
		GLES32.glDrawArrays(GLES32.GL_TRIANGLE_FAN, 20, 4);


		//unbind with vao
		GLES32.glBindVertexArray(0);

		//unuse program
		GLES32.glUseProgram(0);
		requestRender();
		
		if(pyramid_rotationangle > 360.0f)
		{
			pyramid_rotationangle = 0.0f;
		}
		else
		{
			pyramid_rotationangle += 0.5f;
		}
		if(cube_rotationangle > 360.0f)
		{
			cube_rotationangle = 0.0f;
		}
		else
		{
			cube_rotationangle += 0.5f;
		}

    }
	private int loadTexture(int imageFileResourceId)
	{
		System.out.println("RTR: In loadTexture");
		int[] texture = new int[1];
		BitmapFactory.Options options = new BitmapFactory.Options();

		options.inScaled = false;

		Bitmap bitmap = BitmapFactory.decodeResource(context.getResources(),imageFileResourceId);
		GLES32.glGenTextures(1, texture, 0);
		GLES32.glBindTexture(GLES32.GL_TEXTURE_2D, texture[0]);
		GLES32.glPixelStorei(GLES32.GL_UNPACK_ALIGNMENT, 1);
		GLES32.glTexParameteri(GLES32.GL_TEXTURE_2D, GLES32.GL_TEXTURE_MAG_FILTER, GLES32.GL_LINEAR);
		GLES32.glTexParameteri(GLES32.GL_TEXTURE_2D, GLES32.GL_TEXTURE_MIN_FILTER, GLES32.GL_LINEAR_MIPMAP_LINEAR);
		GLUtils.texImage2D(GLES32.GL_TEXTURE_2D, //target
			0,		//mipmap levels
			bitmap,	//rest of the parameter of glTexImage2d are present inside bitmap
			0);	//border width
		GLES32.glGenerateMipmap(GLES32.GL_TEXTURE_2D);
		GLES32.glBindTexture(GLES32.GL_TEXTURE_2D, 0);
		return(texture[0]);
	}

}
