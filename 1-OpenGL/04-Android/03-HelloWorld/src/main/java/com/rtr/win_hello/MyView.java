package com.rtr.win_hello;

//added by me
//this is for AppCompatTextView 
import androidx.appcompat.widget.AppCompatTextView;
//this is for context
import android.content.Context;
//this is for color
import android.graphics.Color;
//this is for Gravity
import android.view.Gravity;

public class MyView extends AppCompatTextView{
	
	public MyView(Context drawingContext){
		super(drawingContext);
		setTextColor(Color.rgb(0,255,0));
		setTextSize(60);
		setGravity(Gravity.CENTER);
		setText("Hello World !!!");
	}
}
