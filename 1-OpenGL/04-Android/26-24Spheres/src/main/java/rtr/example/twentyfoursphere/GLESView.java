package rtr.example.twentyfourspheres;

//packages added by me
import android.content.Context;
import android.graphics.Color;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.GestureDetector;
import android.view.GestureDetector.OnGestureListener;
import android.view.GestureDetector.OnDoubleTapListener;
import rtr.example.twentyfourspheres.GLESMacros;
import rtr.example.twentyfourspheres.Sphere;

import android.opengl.GLSurfaceView;
import android.opengl.GLES32;

import javax.microedition.khronos.opengles.GL10;
import javax.microedition.khronos.egl.EGLConfig;

//for opengl buffers
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;
import java.nio.ShortBuffer;
import java.lang.Math; 

//for texture
import android.graphics.BitmapFactory;
import android.graphics.Bitmap;
import android.opengl.GLUtils;

//for matrix mathematics
import android.opengl.Matrix;

public class GLESView extends GLSurfaceView implements GLSurfaceView.Renderer, OnGestureListener, OnDoubleTapListener{
    private GestureDetector gestureDetector;
	private final Context context;
	
	private int vertexShaderObject_v;
	private int fragmentShaderObject_v;
	private int shaderProgramObject_v;
	private int vertexShaderObject_f;
	private int fragmentShaderObject_f;
	private int shaderProgramObject_f;
	
	//uniforms for per vertex lighting
	private int mUniform_v;
	private int vUniform_v;
	private int pUniform_v;
	private int ldUniform_v;
	private int laUniform_v;
	private int lsUniform_v;
	private int lightPositionUniform_v;
	private int kdUniform_v;
	private int kaUniform_v;
	private int ksUniform_v;
	private int ShininessUniform_v;
	private int enableLightsUniform_v;

	//uniforms for per fragment lighting
	private int mUniform_f;
	private int vUniform_f;
	private int pUniform_f;
	private int ldUniform_f;
	private int laUniform_f;
	private int lsUniform_f;
	private int lightPositionUniform_f;
	private int kdUniform_f;
	private int kaUniform_f;
	private int ksUniform_f;
	private int ShininessUniform_f;
	private int enableLightsUniform_f;


	private int[] vao_sphere = new int[1];
	private int[] vbo_sphere_position = new int[1];
	private int[] vbo_sphere_normal = new int[1];
	private int[] vbo_sphere_element = new int[1];

	private int numVertices;
	private int numElements;

	private float[] perspectiveProjectionMatrix = new float[16];		//4x4 matrix
	
	private float[] lightDiffuse = new float[] {1.0f, 1.0f, 1.0f, 1.0f};
	private float[] lightAmbient = new float[] {0.0f, 0.0f, 0.0f, 0.0f};
	private float[] lightSpecular = new float[] {1.0f, 1.0f, 1.0f, 1.0f};
	private float[] lightPosition = new float[] {0.0f, 0.0f, 0.0f, 1.0f};

	private float[] materialAmbient = new float[] {
		0.0215f, 0.1745f, 0.0215f, 1.0f,	//emrald
		0.135f, 0.2225f, 0.1575f, 1.0f,		//jade
		0.05375f, 0.05f, 0.06625f, 1.0f, 	//obsidian
		0.25f, 0.20725f, 0.20725f, 1.0f,	//pearl
		0.1745f, 0.01175f, 0.01175f, 1.0f, 	//ruby
		0.1f, 0.18725f, 0.1745f, 1.0f,		//turquoise

		0.329412f, 0.223529f, 0.027451f, 1.0f,	//brass
		0.2125f, 0.1275f, 0.054f, 1.0f,			//bronze
		0.25f, 0.25f, 0.25f, 1.0f,				//chrome
		0.19125f, 0.0735f, 0.0225f, 1.0f,		//copper
		0.24725f, 0.1995f, 0.0745f, 1.0f,		//gold
		0.19225f, 0.19225f, 0.19225f, 1.0f,		//silver

		0.0f, 0.0f, 0.0f, 1.0f,		//black plastic
		0.0f, 0.1f, 0.06f, 1.0f,	//cyan plastic
		0.0f, 0.0f, 0.0f, 1.0f,		//green plastic
		0.0f, 0.0f, 0.0f, 1.0f,		//red plastic
		0.0f, 0.0f, 0.0f, 1.0f,		//white plastic
		0.0f, 0.0f, 0.0f, 1.0f,		//yellow plastic

		0.02f, 0.02f, 0.02f, 1.0f, 	//black rubber
		0.0f, 0.05f, 0.05f, 1.0f,  	//cyan rubber
		0.0f, 0.05f, 0.0f, 1.0f,	//green rubber
		0.05f, 0.0f, 0.0f, 1.0f,	//red rubber
		0.05f, 0.05f, 0.05f, 1.0f,	//white rubber
		0.05f, 0.05f, 0.0f, 1.0f	//yellow rubber
	};
	private float[] materialDiffuse = new float[] {
		0.07568f, 0.61424f, 0.07568f, 1.0f,	//emrald
		0.54f, 0.89f, 0.63f, 1.0f,			//jade
		0.18275f, 0.17f, 0.22525f, 1.0f,	//obsidian
		1.0f, 0.829f, 0.829f, 1.0f,			//pearl
		0.61424f, 0.04136f, 0.04136f, 1.0f,	//ruby
		0.396f, 0.74151f, 0.69102f, 1.0f,	//turquoise

		0.780392f, 0.568627f, 0.113725f, 1.0f,	//brass
		0.714f, 0.4284f, 0.18144f, 1.0f,		//bronze
		0.4f, 0.4f, 0.4f, 1.0f,					//chrome
		0.7038f, 0.27048f, 0.0828f, 1.0f,		//copper
		0.24725f, 0.1995f, 0.0745f, 1.0f,		//gold
		0.50754f, 0.50754f, 0.50754f, 1.0f,		//silver

		0.01f, 0.01f, 0.01f, 1.0f,				//blackplastic
		0.0f, 0.50980392f, 0.50980392f, 1.0f,	//cyanplastic
		0.1f, 0.35f, 0.1f, 1.0f,				//green plastic
		0.5f, 0.0f, 0.0f, 1.0f,					//red plastic
		0.55f, 0.55f, 0.55f, 1.0f,				//white plastic
		0.5f, 0.5f, 0.0f, 1.0f,					//yellow plastic

		0.01f, 0.01f, 0.01f, 1.0f,	//black rubber
		0.40f, 0.50f, 0.50f, 1.0f,	//cyan rubber
		0.4f, 0.5f, 0.4f, 1.0f,		//green  rubber
		0.5f, 0.4f, 0.4f, 1.0f,		//red  rubber
		0.5f, 0.5f, 0.5f, 1.0f,		//white  rubber
		0.50f, 0.50f, 0.40f, 1.0f, 	//yellow  rubber
	};
	private float[] materialSpecular = new float[] {
		0.633f, 0.727811f, 0.633f, 1.0f,
		0.316228f, 0.316228f, 0.316228f, 1.0f,
		0.332741f, 0.328634f, 0.346435f, 1.0f,
		0.296648f, 0.296648f, 0.296648f, 1.0f,
		0.727811f, 0.626959f, 0.626959f, 1.0f,
		0.297254f, 0.30829f, 0.306678f, 1.0f,

		0.992157f, 0.941176f, 0.807843f, 1.0f,
		0.393548f, 0.271906f, 0.166721f, 1.0f,
		0.774597f, 0.774597f, 0.774597f, 1.0f,
		0.256777f, 0.137622f, 0.086014f, 1.0f,
		0.628281f, 0.555802f, 0.366065f, 1.0f,
		0.508273f, 0.508273f, 0.508273f, 1.0f,

		0.50f, 0.50f, 0.50f, 1.0f,
		0.50196078f, 0.50196078f, 0.50196078f, 1.0f,
		0.45f, 0.55f, 0.45f, 1.0f,
		0.7f, 0.6f, 0.6f, 1.0f,
		0.70f, 0.70f, 0.70f, 1.0f,
		0.60f, 0.60f, 0.50f, 1.0f,

		0.4f, 0.40f, 0.40f, 1.0f,	//black rubber	
		0.04f, 0.7f, 0.7f, 1.0f,	//cyan rubber
		0.04f, 0.7f, 0.04f, 1.0f,	//green  rubber
		0.7f, 0.04f, 0.04f, 1.0f,	//red  rubber
		0.70f, 0.70f, 0.70f, 1.0f,	//white  rubber
		0.70f, 0.70f, 0.04f, 1.0f	//yellow  rubber
	};
	private float[]  materialShininess = new float[] {
		0.6f * 128.0f,
		0.1f * 128.0f,
		0.3f * 128.0f,
		0.88f * 128.0f,
		0.6f * 128.0f,
		0.1f * 128.0f,

		0.21794872f * 128.0f,
		0.2f * 128.0f,
		0.6f * 128.0f,
		0.1f * 128.0f,
		0.4f * 128.0f,
		0.4f * 128.0f,

		0.25f * 128.0f,
		0.25f * 128.0f,
		0.25f * 128.0f,
		0.25f * 128.0f,
		0.25f * 128.0f,
		0.25f * 128.0f,

		0.078125f * 128.0f,
		0.078125f * 128.0f,
		0.078125f * 128.0f,
		0.078125f * 128.0f,
		0.078125f * 128.0f,
		0.078125f * 128.0f
	};

	private int gWidth;
	private int gHeight;
	private int axis = 0;	//1 : x, 2 : y, 3 : z
	private float theta;

	private int[]  sphereViewport = new int[24*4];

	

	private int enableLighting = 0;
	private boolean toggleLightingType = false; 	// false : per vertex, true: per fragment
    
	//constructor
    public GLESView(Context drawingContext){
        super(drawingContext);
        context = drawingContext;
        
        setEGLContextClientVersion(3); 
        setRenderer(this);
        setRenderMode(GLSurfaceView.RENDERMODE_WHEN_DIRTY);

        gestureDetector =new GestureDetector(drawingContext, this, null, false );
        gestureDetector.setOnDoubleTapListener(this);
    }

    //handling 'onTouchEvent' is the most important,
	//because it triggers all gesture and tap events
	@Override
	public boolean onTouchEvent(MotionEvent event)
	{
		//code
		int eventaction = event.getAction();
		if(!gestureDetector.onTouchEvent(event))
			super.onTouchEvent(event);
		return(true);
	}

    //abstract method from OnDoubleTapListener so must be implemented
	@Override
	public boolean onDoubleTap(MotionEvent e)
	{
		if(enableLighting == 0)
		{
			enableLighting = 1;
		}
		else
		{
			enableLighting = 0;
		}

		System.out.println("RTR: Double Tap Event Occured");
		return(true);
    }
    
    //abstract method from OnDoubleTapListener so must be implemented
	@Override
	public boolean onDoubleTapEvent(MotionEvent e)
	{
		//do not write any code here because already written in 'onDoubleTap'
		return(true);
    }
    
    //abstract  method from OnDoubleTapListener so must be implemented
	@Override
	public boolean onSingleTapConfirmed(MotionEvent e)
	{
		axis++;
		if(axis>3)
		{
			axis = 1;
		}
		
		return(true);
    }
    
    //abstract  method from OnGestureListener so must be implemented
	@Override
	public boolean onDown(MotionEvent e)
	{
		//do not write any code here because already written in 'onSingleTapConfirmed'
		return(true);
    }
    
    //abstract  method from OnGestureListener so must be implemented
	@Override
	public boolean onFling(MotionEvent e1,MotionEvent e2, float velocityX, float velocityY)
	{
		return(true);
	}

	//abstract  method from OnGestureListener so must be implemented
	@Override
	public void onLongPress(MotionEvent e)
	{
		//setText("Long Press");
		if(toggleLightingType == false)
		{
			toggleLightingType = true;
		}
		else
		{
			toggleLightingType = false;
		}
	}

	//abstract  method from OnGestureListener so must be implemented
	@Override
	public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY)
	{
		//setText("Scroll");
		uninitialize();
		System.exit(0);
		return(true);
	}

	//abstract  method from OnGestureListener so must be implemented
	@Override
	public void onShowPress(MotionEvent e)
	{
	
	}

	//abstract  method from OnGestureListener so must be implemented
	@Override
	public boolean onSingleTapUp(MotionEvent e)
	{
		return(true);
    }
    
    //implement GLSurfaceView.Renderer methods
    @Override
    public void onSurfaceCreated(GL10 gl, EGLConfig config){
		System.out.println("RTR: Called onSurfaceCreated");

        String version = gl.glGetString(GL10.GL_VERSION);
		System.out.println("RTR:" + version);
		
		version = gl.glGetString(GLES32.GL_SHADING_LANGUAGE_VERSION);
		System.out.println("RTR:" + version);
        initialize();
    }

    @Override
    public void onSurfaceChanged(GL10 unused, int width, int height)
    {
		System.out.println("RTR: Called onSurfaceChanged");

        resize(width, height);
    }

    @Override
    public void onDrawFrame(GL10 unused){
        display();
    }

	//our custom methods
	private void uninitialize()
	{
		System.out.println("RTR: Called uninitialize");
		
		if(shaderProgramObject_v != 0)
		{
			int[] shaderCount = new int[1];
			int shaderNumber;

			GLES32.glUseProgram(shaderProgramObject_v);

			GLES32.glGetProgramiv(shaderProgramObject_v, GLES32.GL_ATTACHED_SHADERS, shaderCount, 0);
			System.out.println("RTR: uninitialize => " + shaderCount[0]);
		
			int[] shaders =new int[shaderCount[0]];

			if(shaderCount[0] != 0)
			{
				GLES32.glGetAttachedShaders(shaderProgramObject_v, shaderCount[0], shaderCount, 0, shaders, 0);

				for(shaderNumber =0;shaderNumber<shaderCount[0]; shaderNumber++)
				{
					GLES32.glDetachShader(shaderProgramObject_v, shaders[shaderNumber]);
					GLES32.glDeleteShader(shaders[shaderNumber]);
					shaders[shaderNumber]=0;
				}
			}
			else{
				System.out.println("RTR: uninitialize => shader count is 0");
			}
			GLES32.glDeleteProgram(shaderProgramObject_v);
			shaderProgramObject_v = 0;
			GLES32.glUseProgram(0);
		}

		if(shaderProgramObject_f != 0)
		{
			int[] shaderCount = new int[1];
			int shaderNumber;

			GLES32.glUseProgram(shaderProgramObject_f);

			GLES32.glGetProgramiv(shaderProgramObject_f, GLES32.GL_ATTACHED_SHADERS, shaderCount, 0);
			System.out.println("RTR: uninitialize => " + shaderCount[0]);
		
			int[] shaders =new int[shaderCount[0]];

			if(shaderCount[0] != 0)
			{
				GLES32.glGetAttachedShaders(shaderProgramObject_f, shaderCount[0], shaderCount, 0, shaders, 0);

				for(shaderNumber =0;shaderNumber<shaderCount[0]; shaderNumber++)
				{
					GLES32.glDetachShader(shaderProgramObject_f, shaders[shaderNumber]);
					GLES32.glDeleteShader(shaders[shaderNumber]);
					shaders[shaderNumber]=0;
				}
			}
			else{
				System.out.println("RTR: uninitialize => shader count is 0");
			}
			GLES32.glDeleteProgram(shaderProgramObject_f);
			shaderProgramObject_f = 0;
			GLES32.glUseProgram(0);
		}

		if(vbo_sphere_position[0] != 0)
		{
			System.out.println("RTR: uninitialize => deleting vbo_sphere_position");

			GLES32.glDeleteBuffers(1, vbo_sphere_position, 0);
			vbo_sphere_position[0] = 0;
		}
		if(vbo_sphere_normal[0] != 0)
		{
			System.out.println("RTR: uninitialize => deleting vbo_sphere_normal");

			GLES32.glDeleteBuffers(1, vbo_sphere_normal, 0);
			vbo_sphere_normal[0] = 0;
		}
		if(vbo_sphere_element[0] != 0)
		{
			System.out.println("RTR: uninitialize => deleting vbo_sphere_element");

			GLES32.glDeleteBuffers(1, vbo_sphere_element, 0);
			vbo_sphere_element[0] = 0;
		}
		if(vao_sphere[0] != 0)
		{
			System.out.println("RTR: uninitialize => deleting vao_sphere");

			GLES32.glDeleteVertexArrays(1, vao_sphere, 0);
			vao_sphere[0]=0;
		}
	}

    private void initialize()
	{
		System.out.println("RTR: Called initialize");

		vertexShaderObject_v = GLES32.glCreateShader(GLES32.GL_VERTEX_SHADER);
		final String vertexShaderSourceCode_v = String.format
		(
			"	#version 320 es																					" +
			"																								\n	" +
			"	in vec4 vPosition;																				" +
			"	in vec3 vNormal;																				" +
			"																								\n	" +
			"	uniform mat4 u_m_matrix;																		" +
			"	uniform mat4 u_v_matrix;																		" +
			"	uniform mat4 u_p_matrix;																		" +
			"	uniform int u_enable_lights;																	" +
			"	uniform vec3 u_ld;																			" +
			"	uniform vec3 u_la;																			" +
			"	uniform vec3 u_ls;																		\n	" +
			"	uniform vec3 u_ka;																			\n	" +
			"	uniform vec3 u_kd;																			\n	" +
			"	uniform vec3 u_ks;																			\n	" +
			"	uniform vec4 u_light_position;																" +
			"	uniform float u_material_shininess;															\n	" +
			"																								\n	" +
			"	out vec3 phong_ads_light;																		" +
			"																								\n	" +
			"	void main(void)																					" +
			"	{																								" +
			"		if(u_enable_lights == 1)																	" +
			"		{																							" +
			"			vec4 eyeCoordinates = u_v_matrix * u_m_matrix * vPosition;								" +
			"			mat3 normalMatrix   = mat3(u_v_matrix * u_m_matrix);									" +
			"			vec3 tNorm 			= normalize(normalMatrix * vNormal);								" +
			"			vec3 viewerVector	= normalize(vec3(-eyeCoordinates));									" +
			"																								\n	" +
			"			vec3 lightDirection = normalize(vec3(u_light_position - eyeCoordinates));	\n	" +
			"			vec3 lightReflectionVector = reflect(-lightDirection, tNorm);				\n	" +
			"			float tnDotld = max(dot(tNorm, lightDirection), 0.0);						\n	" +
			"			float rvDotvv = max(dot(lightReflectionVector, viewerVector),0.0);	 		\n	" +
			"																								\n	" +
			"			vec3 Ambient = u_la * u_ka;												\n	" +
			"			vec3 Diffuse = u_ld * u_kd * tnDotld;									\n	" +
			"			vec3 Specular = u_ls * u_ks * pow(rvDotvv, u_material_shininess);		\n	" +
			"																								\n	" +
			"			phong_ads_light = Ambient + Diffuse + Specular;										\n	" +
			"		}																							" +
			"		else																						" +
			"		{																							" +
			"			phong_ads_light = vec3(1.0, 1.0, 1.0);													" +
			"		}																							" +
			"		gl_Position = u_p_matrix * u_v_matrix *u_m_matrix * vPosition;								" +
			"	}																								"
		);

		GLES32.glShaderSource(vertexShaderObject_v, vertexShaderSourceCode_v);
		
		GLES32.glCompileShader(vertexShaderObject_v);
		int[] iShaderCompileStatus = new int[1];
		int[] iInfoLogLength = new int[1];
		String szInfoLog = null;

		GLES32.glGetShaderiv(vertexShaderObject_v, GLES32.GL_COMPILE_STATUS, iShaderCompileStatus, 0);

		if(iShaderCompileStatus[0] == GLES32.GL_FALSE)
		{
			GLES32.glGetShaderiv(vertexShaderObject_v, GLES32.GL_INFO_LOG_LENGTH, iInfoLogLength, 0);
			if(iInfoLogLength[0] > 0)
			{
				szInfoLog = GLES32.glGetShaderInfoLog(vertexShaderObject_v);
				System.out.println("RTR: VERTEX SHADER COMPILATION LOG of PER VERTEX LIGHTING:" + szInfoLog);
				uninitialize();
				System.exit(0);
			}
		}

		//fragment shader
		fragmentShaderObject_v = GLES32.glCreateShader(GLES32.GL_FRAGMENT_SHADER);
		final String fragmentShaderSourceCode_v = String.format
		(
			"	#version 320 es											" +
			"														\n	" +
			"	precision highp float;									" +
			"	out vec4 FragColor;										" +
			"	in vec3 phong_ads_light;								" +
			"														\n	" +
			"	void main(void)											" +
			"	{														" +
			"			FragColor = vec4(phong_ads_light, 1.0);			" +
			"	}														"
		);

		GLES32.glShaderSource(fragmentShaderObject_v, fragmentShaderSourceCode_v);
		GLES32.glCompileShader(fragmentShaderObject_v);
		
		iShaderCompileStatus[0] = 0;
		iInfoLogLength[0] = 0;
		szInfoLog = null;

		GLES32.glGetShaderiv(fragmentShaderObject_v, GLES32.GL_COMPILE_STATUS, iShaderCompileStatus, 0);
		
		if(iShaderCompileStatus[0] == GLES32.GL_FALSE)
		{
			GLES32.glGetShaderiv(fragmentShaderObject_v, GLES32.GL_INFO_LOG_LENGTH, iInfoLogLength, 0);

			if(iInfoLogLength[0] > 0)
			{
				szInfoLog = GLES32.glGetShaderInfoLog(fragmentShaderObject_v);
				System.out.println("RTR: FRAGMENT SHADER COMPILATION LOG FOR PER VERTEX LIGHTING:" + szInfoLog);
				uninitialize();
				System.exit(0);
			}
		}

		int[] shaderCount = new int[1];

		//shader program
		shaderProgramObject_v = GLES32.glCreateProgram();

		GLES32.glGetProgramiv(shaderProgramObject_v, GLES32.GL_ATTACHED_SHADERS, shaderCount, 0);

		GLES32.glAttachShader(shaderProgramObject_v, vertexShaderObject_v);
		GLES32.glAttachShader(shaderProgramObject_v, fragmentShaderObject_v);

		GLES32.glGetProgramiv(shaderProgramObject_v, GLES32.GL_ATTACHED_SHADERS, shaderCount, 0);

		//prelinking binding to attributes
		GLES32.glBindAttribLocation(shaderProgramObject_v,GLESMacros.AMC_ATTRIBUTE_POSITION, "vPosition" );
		GLES32.glBindAttribLocation(shaderProgramObject_v,GLESMacros.AMC_ATTRIBUTE_NORMAL, "vNormal" );
		
		//link
		GLES32.glLinkProgram(shaderProgramObject_v);
		
		//error checking
		int[] iProgramLinkStatus = new int[1];
		iProgramLinkStatus[0] = 0;
		iInfoLogLength[0] = 0;
		szInfoLog = null;

		GLES32.glGetProgramiv(shaderProgramObject_v, GLES32.GL_LINK_STATUS, iProgramLinkStatus, 0);

		if(iProgramLinkStatus[0] == GLES32.GL_FALSE)
		{
			GLES32.glGetProgramiv(shaderProgramObject_v, GLES32.GL_INFO_LOG_LENGTH, iInfoLogLength, 0);
			if(iInfoLogLength[0] > 0)
			{
				szInfoLog = GLES32.glGetShaderInfoLog(shaderProgramObject_v);
				System.out.println("RTR SHADER PROGRAM LINKING LOG FOR PER VERTEX LIGHTING:" + szInfoLog);
				uninitialize();
				System.exit(0);
			}
		}

		//post linking retrieving uniform location
		// model, view, projection matrices uniform location
		mUniform_v = GLES32.glGetUniformLocation(shaderProgramObject_v, "u_m_matrix");
		vUniform_v = GLES32.glGetUniformLocation(shaderProgramObject_v, "u_v_matrix");
		pUniform_v = GLES32.glGetUniformLocation(shaderProgramObject_v, "u_p_matrix");
		// light uniform location
		ldUniform_v = GLES32.glGetUniformLocation(shaderProgramObject_v, "u_ld");
		laUniform_v = GLES32.glGetUniformLocation(shaderProgramObject_v, "u_la");
		lsUniform_v = GLES32.glGetUniformLocation(shaderProgramObject_v, "u_ls");
		lightPositionUniform_v = GLES32.glGetUniformLocation(shaderProgramObject_v, "u_light_position" );
		// material uniform location
		kaUniform_v = GLES32.glGetUniformLocation(shaderProgramObject_v, "u_ka");
		kdUniform_v = GLES32.glGetUniformLocation(shaderProgramObject_v, "u_kd");
		ksUniform_v = GLES32.glGetUniformLocation(shaderProgramObject_v, "u_ks");
		ShininessUniform_v = GLES32.glGetUniformLocation(shaderProgramObject_v, "u_material_shininess");
		// enable lighting
		enableLightsUniform_v  = GLES32.glGetUniformLocation(shaderProgramObject_v, "u_enable_lights");

		//-----------------------------------------------------------------------------------------------
		//**********************************PER FRAGMENT LIGHTING************************************** */
		//----------------------------------------------------------------------------------------------
		
		vertexShaderObject_f = GLES32.glCreateShader(GLES32.GL_VERTEX_SHADER);
		final String vertexShaderSourceCode_f = String.format
		(
			"	#version 320 es																					" +
			"																								\n	" +
			"	in vec4 vPosition;																				" +
			"	in vec3 vNormal;																				" +
			"																								\n	" +
			"	uniform mat4 u_m_matrix;																		" +
			"	uniform mat4 u_v_matrix;																		" +
			"	uniform mat4 u_p_matrix;																		" +
			"	uniform vec4 u_light_position;																	" +
			"																								\n	" +
			"	out vec3 tNorm;																					" +
			"	out vec3 lightDirection;																		" +
			"	out vec3 viewerVector;																			" +
			"																								\n	" +
			"	void main(void)																					" +
			"	{																								" +
			"		vec4 eyeCoordinates;																		" +
			"		mat3 normalMatrix;																			" +
			"			eyeCoordinates 	= u_v_matrix * u_m_matrix * vPosition;									" +
			"			normalMatrix   	= mat3(u_v_matrix * u_m_matrix);										" +
			"			tNorm 			= normalMatrix * vNormal;												" +
			"			viewerVector	= vec3(-eyeCoordinates);												" +
			"			lightDirection	= vec3(u_light_position - eyeCoordinates);							\n	" +
			"		gl_Position = u_p_matrix * u_v_matrix *u_m_matrix * vPosition;								" +
			"	}																								"
		);

		GLES32.glShaderSource(vertexShaderObject_f, vertexShaderSourceCode_f);
		
		GLES32.glCompileShader(vertexShaderObject_f);
		iShaderCompileStatus[0] = 0;
		iInfoLogLength[0] = 0;
		szInfoLog = null;

		GLES32.glGetShaderiv(vertexShaderObject_f, GLES32.GL_COMPILE_STATUS, iShaderCompileStatus, 0);

		if(iShaderCompileStatus[0] == GLES32.GL_FALSE)
		{
			GLES32.glGetShaderiv(vertexShaderObject_f, GLES32.GL_INFO_LOG_LENGTH, iInfoLogLength, 0);
			if(iInfoLogLength[0] > 0)
			{
				szInfoLog = GLES32.glGetShaderInfoLog(vertexShaderObject_f);
				System.out.println("RTR: VERTEX SHADER COMPILATION LOG FOR PERFRAGMENT LIGHTING:" + szInfoLog);
				uninitialize();
				System.exit(0);
			}
		}

		//fragment shader
		fragmentShaderObject_f= GLES32.glCreateShader(GLES32.GL_FRAGMENT_SHADER);
		final String fragmentShaderSourceCode_f = String.format
		(
			"	#version 320 es																						" +
			"																									\n	" +
			"	precision highp float;																				" +
			"																									\n	" +
			"	in vec4 eyeCoordinates;																				" +
			"	in vec3 tNorm;																						" +
			"	in vec3 lightDirection; 																			" +
			"	in vec3 viewerVector;																				" +
			"																									\n	" +
			"	out vec4 FragColor;																					" +
			"																									\n	" +
			"	uniform vec3 u_ld;																					" +
			"	uniform vec3 u_la;																					" +
			"	uniform vec3 u_ls;																					" +
			"	uniform vec3 u_kd;																					" +
			"	uniform vec3 u_ka;																					" +
			"	uniform vec3 u_ks;																					" +
			"	uniform float u_material_shininess;																	" +
			"	uniform int u_enable_lights;																		" +
			"																									\n	" +
			"	void main(void)																						" +
			"	{																									" +
			"		vec3 phong_lights;																				" +
			"		vec3 normalized_tNorm;																			" +
			"		vec3 normalized_lightDirection;																	" +
			"		vec3 normalized_viewerVector;																	" +
			"																									\n	" +
			"		if(u_enable_lights == 1)																		" +
			"		{																								" +
			"			normalized_tNorm = normalize(tNorm);														" +
			"			normalized_lightDirection = normalize(lightDirection);										" +
			"			normalized_viewerVector = normalize(viewerVector);											" +
			"																									\n	" +
			"			vec3 reflectionVector = reflect(-normalized_lightDirection, normalized_tNorm);	" +
			"			float tnDotld = max(dot(normalized_tNorm, normalized_lightDirection), 0.0);					" +
			"			float rvDotvv = max(dot(reflectionVector, normalized_viewerVector), 0.0);					" +
			"																									\n	" +
			"			vec3 ambient = u_la * u_ka;																	" +
			"			vec3 diffuse = u_ld * u_kd * tnDotld;														" +
			"			vec3 specular = u_ls * u_ks * pow(rvDotvv, u_material_shininess);							" +
			"			phong_lights = ambient + diffuse + specular;												" +
			"																									\n	" +
			"			FragColor = vec4(phong_lights, 1.0);														" +
			"		}																								" +
			"		else																						\n	" +
			"		{																							\n	" +
			"			phong_lights = vec3(1.0, 1.0, 1.0);														\n	" +
			"			FragColor = vec4(phong_lights, 1.0);													\n	" +
			"		}																							\n	" +
			"	}																									"
		);

		GLES32.glShaderSource(fragmentShaderObject_f, fragmentShaderSourceCode_f);
		GLES32.glCompileShader(fragmentShaderObject_f);
		
		iShaderCompileStatus[0] = 0;
		iInfoLogLength[0] = 0;
		szInfoLog = null;

		GLES32.glGetShaderiv(fragmentShaderObject_f, GLES32.GL_COMPILE_STATUS, iShaderCompileStatus, 0);
		
		if(iShaderCompileStatus[0] == GLES32.GL_FALSE)
		{
			GLES32.glGetShaderiv(fragmentShaderObject_f, GLES32.GL_INFO_LOG_LENGTH, iInfoLogLength, 0);

			if(iInfoLogLength[0] > 0)
			{
				szInfoLog = GLES32.glGetShaderInfoLog(fragmentShaderObject_f);
				System.out.println("RTR: FRAGMENT SHADER COMPILATION LOG FOR PER FRAGMENT LIGHTING:" + szInfoLog);
				uninitialize();
				System.exit(0);
			}
		}


		//shader program
		shaderProgramObject_f = GLES32.glCreateProgram();

		GLES32.glGetProgramiv(shaderProgramObject_f, GLES32.GL_ATTACHED_SHADERS, shaderCount, 0);

		GLES32.glAttachShader(shaderProgramObject_f	, vertexShaderObject_f);
		GLES32.glAttachShader(shaderProgramObject_f	, fragmentShaderObject_f);

		GLES32.glGetProgramiv(shaderProgramObject_f, GLES32.GL_ATTACHED_SHADERS, shaderCount, 0);

		//prelinking binding to attributes
		GLES32.glBindAttribLocation(shaderProgramObject_f, GLESMacros.AMC_ATTRIBUTE_POSITION, "vPosition" );
		GLES32.glBindAttribLocation(shaderProgramObject_f, GLESMacros.AMC_ATTRIBUTE_NORMAL, "vNormal" );
		
		//link
		GLES32.glLinkProgram(shaderProgramObject_f);
		
		//error checking
		iProgramLinkStatus[0] = 0;
		iInfoLogLength[0] = 0;
		szInfoLog = null;

		GLES32.glGetProgramiv(shaderProgramObject_f, GLES32.GL_LINK_STATUS, iProgramLinkStatus, 0);

		if(iProgramLinkStatus[0] == GLES32.GL_FALSE)
		{
			GLES32.glGetProgramiv(shaderProgramObject_f, GLES32.GL_INFO_LOG_LENGTH, iInfoLogLength, 0);
			if(iInfoLogLength[0] > 0)
			{
				szInfoLog = GLES32.glGetShaderInfoLog(shaderProgramObject_f);
				System.out.println("RTR SHADER PROGRAM LINKING LOG FOR PER FRAGMENT LIGHTING:" + szInfoLog);
				uninitialize();
				System.exit(0);
			}
		}

		//post linking retrieving uniform location
		// model, view, projection matrices uniform location
		mUniform_f = GLES32.glGetUniformLocation(shaderProgramObject_f, "u_m_matrix");
		vUniform_f = GLES32.glGetUniformLocation(shaderProgramObject_f, "u_v_matrix");
		pUniform_f = GLES32.glGetUniformLocation(shaderProgramObject_f, "u_p_matrix");
		// light uniform location
		ldUniform_f = GLES32.glGetUniformLocation(shaderProgramObject_f, "u_ld");
		laUniform_f = GLES32.glGetUniformLocation(shaderProgramObject_f, "u_la");
		lsUniform_f = GLES32.glGetUniformLocation(shaderProgramObject_f, "u_ls");
		lightPositionUniform_f = GLES32.glGetUniformLocation(shaderProgramObject_f, "u_light_position" );
		// material uniform location
		kaUniform_f = GLES32.glGetUniformLocation(shaderProgramObject_f, "u_ka");
		kdUniform_f = GLES32.glGetUniformLocation(shaderProgramObject_f, "u_kd");
		ksUniform_f = GLES32.glGetUniformLocation(shaderProgramObject_f, "u_ks");
		ShininessUniform_f = GLES32.glGetUniformLocation(shaderProgramObject_f, "u_material_shininess");
		// enable lighting
		enableLightsUniform_f  = GLES32.glGetUniformLocation(shaderProgramObject_f, "u_enable_lights");


		//******************************* CREATE SPHERE ************************************//
		Sphere sphere = new Sphere();
		float sphere_vertices[] = new float[1146];
		float sphere_normals[] = new float[1146];
		float sphere_textures[] = new float[764];
		short sphere_elements[] = new short[2280];
		sphere.getSphereVertexData(sphere_vertices, sphere_normals, sphere_textures, sphere_elements);
		numVertices = sphere.getNumberOfSphereVertices();
		numElements = sphere.getNumberOfSphereElements();

		//create and bind with vao
		GLES32.glGenVertexArrays(1, vao_sphere, 0);
		GLES32.glBindVertexArray(vao_sphere[0]);

		//Create buffer for position and bind with vbo
		GLES32.glGenBuffers(1, vbo_sphere_position, 0);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vbo_sphere_position[0]);

		//Create the buffer 
		ByteBuffer byteBuffer_pos = ByteBuffer.allocateDirect(sphere_vertices.length * 4); //4 is size of float in java
		byteBuffer_pos.order(ByteOrder.nativeOrder());
		FloatBuffer sphereVerticesBuffer = byteBuffer_pos.asFloatBuffer();
		sphereVerticesBuffer.put(sphere_vertices);
		sphereVerticesBuffer.position(0);

		//data to the buffer
		GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER, sphere_vertices.length * 4, sphereVerticesBuffer, GLES32.GL_STATIC_DRAW);
		GLES32.glVertexAttribPointer(GLESMacros.AMC_ATTRIBUTE_POSITION, 
										3,				// break data after x y z, i.e. 3
										GLES32.GL_FLOAT, // type of data
										false, 			// normalize?
										0, 				//stride?-no
										0 );			//offset?-no(windows : NULL, in java null is not #define 0) therefore 0
		GLES32.glEnableVertexAttribArray(GLESMacros.AMC_ATTRIBUTE_POSITION);
		
		//unbind to buffer
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0);

		//Create buffer for normal and bind with vbo
		GLES32.glGenBuffers(1, vbo_sphere_normal, 0);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vbo_sphere_normal[0]);

		//Allocate the buffer directly from native memory
		ByteBuffer byteBuffer_nor = ByteBuffer.allocateDirect(sphere_normals.length*4);
		byteBuffer_nor.order(ByteOrder.nativeOrder());
		FloatBuffer sphereNormalBuffer = byteBuffer_nor.asFloatBuffer();
		sphereNormalBuffer.put(sphere_normals);
		sphereNormalBuffer.position(0);
		
		//data to the buffer
		GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER, sphere_normals.length *4, sphereNormalBuffer, GLES32.GL_STATIC_DRAW);
		GLES32.glVertexAttribPointer(GLESMacros.AMC_ATTRIBUTE_NORMAL, 
										3,				// break data after x y z, i.e. 3
										GLES32.GL_FLOAT, // type of data
										false, 			// normalize?
										0, 				//stride?-no
										0 );			//offset?-no(windows : NULL, in java null is not #define 0) therefore 0
		GLES32.glEnableVertexAttribArray(GLESMacros.AMC_ATTRIBUTE_NORMAL);
		
		//unbind to buffer
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0);
		
		//Create buffer for element and bind with vbo
		GLES32.glGenBuffers(1, vbo_sphere_element, 0);
		GLES32.glBindBuffer(GLES32.GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_element[0]);

		//Allocate the buffer directly from native memory
		ByteBuffer byteBuffer_ele = ByteBuffer.allocateDirect(sphere_elements.length*2);
		byteBuffer_ele.order(ByteOrder.nativeOrder());
		ShortBuffer sphereElementsBuffer = byteBuffer_ele.asShortBuffer();
		sphereElementsBuffer.put(sphere_elements);
		sphereElementsBuffer.position(0);

		//data to the buffer
		GLES32.glBufferData(GLES32.GL_ELEMENT_ARRAY_BUFFER, sphere_elements.length * 2, sphereElementsBuffer, GLES32.GL_STATIC_DRAW);
		GLES32.glBindBuffer(GLES32.GL_ELEMENT_ARRAY_BUFFER, 0);
		GLES32.glBindVertexArray(0);

		GLES32.glClearColor(0.0f,0.0f,0.0f,1.0f);
		GLES32.glEnable(GLES32.GL_DEPTH_TEST);
		GLES32.glDepthFunc(GLES32.GL_LEQUAL);

		Matrix.setIdentityM(perspectiveProjectionMatrix,0);
		//no warmup call to resize as we are already in fullscreen. 
		//we dont run multiple app with screen sharing in android unlike windows.
		setViewports();
    }

    private void resize(int width, int height)
    {
        if(height < 0)
        {
            height = 1;
        }
		gWidth = width;
		gHeight = height;
		//GLES32.glViewport(0,0,width, height);
		setViewports();
		Matrix.perspectiveM(perspectiveProjectionMatrix, 0, 45.0f, (float)width/ (float)height, 0.1f, 100.0f);
    }

	private void display()
	{
		//variable declarations
		float[] modelMatrix = new float[16];
		float[] viewMatrix = new float[16];
		float[] projectionMatrix = new float[16];
		float[] translationMatrix = new float[16];
		float[] scaleMatrix = new float[16];
		float[] rotationMatrix = new float[16];

		//making matrices identity
		Matrix.setIdentityM(modelMatrix,0);
		Matrix.setIdentityM(viewMatrix,0);
		Matrix.setIdentityM(projectionMatrix,0);
		Matrix.setIdentityM(translationMatrix,0);
		Matrix.setIdentityM(scaleMatrix,0);
		Matrix.setIdentityM(rotationMatrix, 0);
		
		//code
		GLES32.glClear(GLES32.GL_COLOR_BUFFER_BIT|GLES32.GL_DEPTH_BUFFER_BIT);

		//matrix transformation
		Matrix.translateM(translationMatrix, 0, 0.0f, 0.0f, -3.0f);

		Matrix.multiplyMM(modelMatrix, 0,
						  modelMatrix, 0,
						  translationMatrix, 0);
		Matrix.multiplyMM(modelMatrix, 0,
						  modelMatrix, 0,
						  rotationMatrix, 0);
		Matrix.multiplyMM(projectionMatrix, 0,
						  projectionMatrix, 0,
						  perspectiveProjectionMatrix, 0);

		int offset = 0;

		for(int  i = 0;  i<24; i++)
		{
			offset = i * 4;

			if(toggleLightingType == false)
			{
				GLES32.glUseProgram(shaderProgramObject_v);

				if(enableLighting == 1)
				{
					if (axis == 3)	// z
					{
						lightPosition[0] = 15 * (float)Math.cos(theta);
						lightPosition[1] = 15 * (float)Math.sin(theta);
						lightPosition[2] = 0.0f;
					}
					if (axis == 2)	// y
					{
						lightPosition[0] = 15 * (float)Math.cos(theta);
						lightPosition[1] = 0.0f;
						lightPosition[2] = 15 * (float)Math.sin(theta);
					}
					if (axis == 1)	// x
					{
						lightPosition[0] = 0.0f;
						lightPosition[1] = 15 * (float)Math.cos(theta);
						lightPosition[2] = 15 * (float)Math.sin(theta);
					}

					GLES32.glUniform1i(enableLightsUniform_v, 1);
					GLES32.glUniform3fv(ldUniform_v, 1, lightDiffuse, 0);
					GLES32.glUniform3fv(laUniform_v, 1, lightAmbient, 0);
					GLES32.glUniform3fv(lsUniform_v, 1, lightSpecular, 0);			
					
					GLES32.glUniform3f(kdUniform_v, materialDiffuse[offset + 0],materialDiffuse[offset + 1], materialDiffuse[offset + 2]);
					GLES32.glUniform3f(kaUniform_v, materialAmbient[offset + 0],materialAmbient[offset + 1],materialAmbient[offset + 2]);
					GLES32.glUniform3f(ksUniform_v, materialSpecular[offset + 0],materialSpecular[offset + 1],materialSpecular[offset + 2]);			
				
					GLES32.glUniform1f(ShininessUniform_v, materialShininess[0]);			

					GLES32.glUniform4fv(lightPositionUniform_v, 1, lightPosition, 0);			
				}
				else
				{
					GLES32.glUniform1i(enableLightsUniform_v, 0);
				}
				//send matrix to shader
				GLES32.glUniformMatrix4fv(mUniform_v, 1, false, modelMatrix, 0);
				GLES32.glUniformMatrix4fv(vUniform_v, 1, false, viewMatrix, 0);
				GLES32.glUniformMatrix4fv(pUniform_v, 1, false, projectionMatrix, 0);

				System.out.println("RTR: in Display ");

				GLES32.glViewport(sphereViewport[offset + 0],sphereViewport[offset + 1],sphereViewport[offset + 2], sphereViewport[offset + 3]);
				//bind with vao
				GLES32.glBindVertexArray(vao_sphere[0]);

				GLES32.glBindBuffer(GLES32.GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_element[0]);
				GLES32.glDrawElements(GLES32.GL_TRIANGLES, numElements,GLES32.GL_UNSIGNED_SHORT, 0);
				GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0);

				//unbind with vao
				GLES32.glBindVertexArray(0);
				
				//unuse program
				GLES32.glUseProgram(0);
			}
			else if(toggleLightingType == true)
			{
				GLES32.glUseProgram(shaderProgramObject_f);
				if(enableLighting == 1)
				{
					if (axis == 3)	// z
					{
						lightPosition[0] = 15 * (float)Math.cos(theta);
						lightPosition[1] = 15 * (float)Math.sin(theta);
						lightPosition[2] = 0.0f;
					}
					if (axis == 1)	// y
					{
						lightPosition[0] = 15 * (float)Math.cos(theta);
						lightPosition[1] = 0.0f;
						lightPosition[2] = 15 * (float)Math.sin(theta);
					}
					if (axis == 2)	// x
					{
						lightPosition[0] = 0.0f;
						lightPosition[1] = 15 * (float)Math.cos(theta);
						lightPosition[2] = 15 * (float)Math.sin(theta);
					}

					GLES32.glUniform1i(enableLightsUniform_f, 1);
					GLES32.glUniform3fv(ldUniform_f, 1, lightDiffuse, 0);
					GLES32.glUniform3fv(laUniform_f, 1, lightAmbient, 0);
					GLES32.glUniform3fv(lsUniform_f, 1, lightSpecular, 0);			
					
					GLES32.glUniform3f(kdUniform_f, materialDiffuse[offset + 0],materialDiffuse[offset + 1], materialDiffuse[offset + 2]);
					GLES32.glUniform3f(kaUniform_f, materialAmbient[offset + 0],materialAmbient[offset + 1],materialAmbient[offset + 2]);
					GLES32.glUniform3f(ksUniform_f, materialSpecular[offset + 0],materialSpecular[offset + 1],materialSpecular[offset + 2]);			
				
					GLES32.glUniform1f(ShininessUniform_f, materialShininess[0]);			

					GLES32.glUniform4fv(lightPositionUniform_f, 1, lightPosition, 0);			
				}
				else
				{
					GLES32.glUniform1i(enableLightsUniform_f, 0);
				}
				//send matrix to shader
				GLES32.glUniformMatrix4fv(mUniform_f, 1, false, modelMatrix, 0);
				GLES32.glUniformMatrix4fv(vUniform_f, 1, false, viewMatrix, 0);
				GLES32.glUniformMatrix4fv(pUniform_f, 1, false, projectionMatrix, 0);

				System.out.println("RTR: in Display ");

				GLES32.glViewport(sphereViewport[offset + 0],sphereViewport[offset + 1],sphereViewport[offset + 2], sphereViewport[offset + 3]);
				//bind with vao
				GLES32.glBindVertexArray(vao_sphere[0]);

				GLES32.glBindBuffer(GLES32.GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_element[0]);
				GLES32.glDrawElements(GLES32.GL_TRIANGLES, numElements,GLES32.GL_UNSIGNED_SHORT, 0);
				GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0);

				//unbind with vao
				GLES32.glBindVertexArray(0);
				
				//unuse program
				GLES32.glUseProgram(0);
			}
		}
		
		theta = theta + 0.05f;
		if(theta > 360.0f)
			theta = 0.01f;

        requestRender();
	}
	
	private void setViewports()
	{
		//sphere 1
		sphereViewport[0] = -gWidth/6;
		sphereViewport[1] = -gHeight/8;
		sphereViewport[2] = gWidth/2;
		sphereViewport[3] = gHeight/2;
		//sphere 2
		sphereViewport[4] = 0;
		sphereViewport[5] = -gHeight/8;
		sphereViewport[6] = gWidth/2;
		sphereViewport[7] = gHeight/2;
		//sphere 3
		sphereViewport[8] = gWidth/6;
		sphereViewport[9] = -gHeight/8;
		sphereViewport[10] = gWidth/2;
		sphereViewport[11] = gHeight/2;
		//sphere 4
		sphereViewport[12] = 2*gWidth/6;
		sphereViewport[13] = -gHeight/8;
		sphereViewport[14] = gWidth/2;
		sphereViewport[15] = gHeight/2;
		//sphere 5
		sphereViewport[16] = 3*gWidth/6;
		sphereViewport[17] = -gHeight/8;
		sphereViewport[18] = gWidth/2;
		sphereViewport[19] = gHeight/2;
		//sphere 6
		sphereViewport[20] = 4*gWidth/6;
		sphereViewport[21] = -gHeight/8;
		sphereViewport[22] = gWidth/2;
		sphereViewport[23] = gHeight/2;

		//sphere 7
		sphereViewport[24] = -gWidth/6;
		sphereViewport[25] = 1*gHeight/8;
		sphereViewport[26] = gWidth/2;
		sphereViewport[27] = gHeight/2;
		//sphere 8
		sphereViewport[28] = 0;
		sphereViewport[29] = 1*gHeight/8;
		sphereViewport[30] = gWidth/2;
		sphereViewport[31] = gHeight/2;
		//sphere 9
		sphereViewport[32] = 1*gWidth/6;
		sphereViewport[33] = 1*gHeight/8;
		sphereViewport[34] = gWidth/2;
		sphereViewport[35] = gHeight/2;
		//sphere 10
		sphereViewport[36] = 2*gWidth/6;
		sphereViewport[37] = 1*gHeight/8;
		sphereViewport[38] = gWidth/2;
		sphereViewport[39] = gHeight/2;
		//sphere 11
		sphereViewport[40] = 3*gWidth/6;
		sphereViewport[41] = 1*gHeight/8;
		sphereViewport[42] = gWidth/2;
		sphereViewport[43] = gHeight/2;
		//sphere 12
		sphereViewport[44] = 4*gWidth/6;
		sphereViewport[45] = 1*gHeight/8;
		sphereViewport[46] = gWidth/2;
		sphereViewport[47] = gHeight/2;
		
		//sphere 13
		sphereViewport[48] = -gWidth/6;
		sphereViewport[49] = 3*gHeight/8;
		sphereViewport[50] = gWidth/2;
		sphereViewport[51] = gHeight/2;
		//sphere 14
		sphereViewport[52] = 0;
		sphereViewport[53] = 3*gHeight/8;
		sphereViewport[54] = gWidth/2;
		sphereViewport[55] = gHeight/2;
		//sphere 15
		sphereViewport[56] = gWidth/6;
		sphereViewport[57] = 3*gHeight/8;
		sphereViewport[58] = gWidth/2;
		sphereViewport[59] = gHeight/2;
		//sphere 16
		sphereViewport[60] = 2 * gWidth/6;
		sphereViewport[61] = 3*gHeight/8;
		sphereViewport[62] = gWidth/2;
		sphereViewport[63] = gHeight/2;
		//sphere 17
		sphereViewport[64] = 3 * gWidth/6;
		sphereViewport[65] = 3*gHeight/8;
		sphereViewport[66] = gWidth/2;
		sphereViewport[67] = gHeight/2;
		//sphere 18
		sphereViewport[68] = 4 * gWidth/6;
		sphereViewport[69] = 3*gHeight/8;
		sphereViewport[70] = gWidth/2;
		sphereViewport[71] = gHeight/2;

		//sphere 19
		sphereViewport[72] = -gWidth/6;
		sphereViewport[73] = 5*gHeight/8;
		sphereViewport[74] = gWidth/2;
		sphereViewport[75] = gHeight/2;
		//sphere 20
		sphereViewport[76] = 0;
		sphereViewport[77] = 5*gHeight/8;
		sphereViewport[78] = gWidth/2;
		sphereViewport[79] = gHeight/2;
		//sphere 21
		sphereViewport[80] = gWidth/6;
		sphereViewport[81] = 5*gHeight/8;
		sphereViewport[82] = gWidth/2;
		sphereViewport[83] = gHeight/2;
		//sphere 22
		sphereViewport[84] = 2 * gWidth/6;
		sphereViewport[85] = 5*gHeight/8;
		sphereViewport[86] = gWidth/2;
		sphereViewport[87] = gHeight/2;
		//sphere 23
		sphereViewport[88] = 3 * gWidth/6;
		sphereViewport[89] = 5*gHeight/8;
		sphereViewport[90] = gWidth/2;
		sphereViewport[91] = gHeight/2;
		//sphere 24
		sphereViewport[92] = 4 * gWidth/6;
		sphereViewport[93] = 5*gHeight/8;
		sphereViewport[94] = gWidth/2;
		sphereViewport[95] = gHeight/2;


	}
}

