package rtr.example.twentyfourspheres;

//default packages
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
//packages added by me
import android.view.Window;
import android.view.WindowManager;
import android.content.pm.ActivityInfo;
import android.graphics.Color;
import android.view.View;

public class MainActivity extends AppCompatActivity {
    private GLESView glesView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //setContentView(R.layout.activity_main);
		System.out.println("RTR: OnCreate");

        //get rid of title bar
        this.supportRequestWindowFeature(Window.FEATURE_NO_TITLE);

        //make fullscreen
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        //get rid of navigation bar
        this.getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_HIDE_NAVIGATION);

        //forced landscape orientation
        this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);

        //set background color
        this.getWindow().getDecorView().setBackgroundColor(Color.BLACK);

        //define our own view
        glesView = new GLESView(this);

        //now set this view as our default view
        setContentView(glesView);
    }

    @Override
    protected void onPause(){
        super.onPause();
    }

    @Override
    protected void onResume(){
        super.onResume();
    }
}
