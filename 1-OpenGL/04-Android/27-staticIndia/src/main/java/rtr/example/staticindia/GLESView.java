package rtr.example.staticindia;

//packages added by me
import android.content.Context;
import android.graphics.Color;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.GestureDetector;
import android.view.GestureDetector.OnGestureListener;
import rtr.example.staticindia.GLESMacros;
import android.view.GestureDetector.OnDoubleTapListener;

import android.opengl.GLSurfaceView;
import android.opengl.GLES32;

import javax.microedition.khronos.opengles.GL10;
import javax.microedition.khronos.egl.EGLConfig;

//for opengl buffers
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;

//for matrix mathematics
import android.opengl.Matrix;

public class GLESView extends GLSurfaceView implements GLSurfaceView.Renderer, OnGestureListener, OnDoubleTapListener{
    private GestureDetector gestureDetector;
    private final Context context;

	private int vertexShaderObject;
	private int fragmentShaderObject;
	private int shaderProgramObject;
	// I
	private int[] vao_i1 = new int[1];
	private int[] vbo_color_i1 = new int[1];
	private int[] vbo_position_i1 = new int[1];
	// N
	private int[] vao_n = new int[1];
	private int[] vbo_color_n = new int[1];
	private int[] vbo_position_n = new int[1];
	// D
	private int[] vao_d = new int[1];
	private int[] vbo_color_d = new int[1];
	private int[] vbo_position_d = new int[1];
	// I
	private int[] vao_i2 = new int[1];
	private int[] vbo_color_i2 = new int[1];
	private int[] vbo_position_i2 = new int[1];
	// A
	private int[] vao_a = new int[1];
	private int[] vbo_color_a = new int[1];
	private int[] vbo_position_a = new int[1];
	// Flag
	private int[] vao_flag = new int[1];
	private int[] vbo_color_flag = new int[1];
	private int[] vbo_position_flag = new int[1];

	private int mvpUniform;

	private float[] perspectiveProjectionMatrix = new float[16];		//4x4 matrix

    //constructor
    public GLESView(Context drawingContext){
        super(drawingContext);
        context = drawingContext;
        
        setEGLContextClientVersion(3);
        setRenderer(this);
        setRenderMode(GLSurfaceView.RENDERMODE_WHEN_DIRTY);

        gestureDetector =new GestureDetector(drawingContext, this, null, false );
        gestureDetector.setOnDoubleTapListener(this);
    }

    //handling 'onTouchEvent' is the most important,
	//because it triggers all gesture and tap events
	@Override
	public boolean onTouchEvent(MotionEvent event)
	{
		//code
		int eventaction = event.getAction();
		if(!gestureDetector.onTouchEvent(event))
			super.onTouchEvent(event);
		return(true);
	}

    //abstract method from OnDoubleTapListener so must be implemented
	@Override
	public boolean onDoubleTap(MotionEvent e)
	{
		//setText("Double Tap");
		return(true);
    }
    
    //abstract method from OnDoubleTapListener so must be implemented
	@Override
	public boolean onDoubleTapEvent(MotionEvent e)
	{
		//do not write any code here because already written in 'onDoubleTap'
		return(true);
    }
    
    //abstract  method from OnDoubleTapListener so must be implemented
	@Override
	public boolean onSingleTapConfirmed(MotionEvent e)
	{
	//	setText("Single Tap");
		return(true);
    }
    
    //abstract  method from OnGestureListener so must be implemented
	@Override
	public boolean onDown(MotionEvent e)
	{
		//do not write any code here because already written in 'onSingleTapConfirmed'
		return(true);
    }
    
    //abstract  method from OnGestureListener so must be implemented
	@Override
	public boolean onFling(MotionEvent e1,MotionEvent e2, float velocityX, float velocityY)
	{
		return(true);
	}

	//abstract  method from OnGestureListener so must be implemented
	@Override
	public void onLongPress(MotionEvent e)
	{
		//setText("Long Press");
	}

	//abstract  method from OnGestureListener so must be implemented
	@Override
	public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY)
	{
		//setText("Scroll");
		uninitialize();
		System.exit(0);
		return(true);
	}

	//abstract  method from OnGestureListener so must be implemented
	@Override
	public void onShowPress(MotionEvent e)
	{
	
	}

	//abstract  method from OnGestureListener so must be implemented
	@Override
	public boolean onSingleTapUp(MotionEvent e)
	{
		return(true);
    }
    
    //implement GLSurfaceView.Renderer methods
    @Override
    public void onSurfaceCreated(GL10 gl, EGLConfig config){
		System.out.println("RTR: Called onSurfaceCreated");

        String version = gl.glGetString(GL10.GL_VERSION);
		System.out.println("RTR:" + version);
		
		version = gl.glGetString(GLES32.GL_SHADING_LANGUAGE_VERSION);
		System.out.println("RTR:" + version);
        initialize();
    }

    @Override
    public void onSurfaceChanged(GL10 unused, int width, int height)
    {
		System.out.println("RTR: Called onSurfaceChabged");

        resize(width, height);
    }

    @Override
    public void onDrawFrame(GL10 unused){
        display();
    }

    //our custom methods
	private void uninitialize()
	{
		System.out.println("RTR: Called uninitialize");
		
		if(shaderProgramObject != 0)
		{
			int[] shaderCount = new int[1];
			int shaderNumber;

			GLES32.glUseProgram(shaderProgramObject);

			GLES32.glGetProgramiv(shaderProgramObject, GLES32.GL_ATTACHED_SHADERS, shaderCount, 0);
			System.out.println("RTR: uninitialize => " + shaderCount[0]);
		
			int[] shaders =new int[shaderCount[0]];

			if(shaderCount[0] != 0)
			{
				GLES32.glGetAttachedShaders(shaderProgramObject, shaderCount[0], shaderCount, 0, shaders, 0);
				System.out.println("RTR: uninitialize => if shadercount != 0 ::" + shaderCount[0]);

				for(shaderNumber =0;shaderNumber<shaderCount[0]; shaderNumber++)
				{
					GLES32.glDetachShader(shaderProgramObject, shaders[shaderNumber]);
					GLES32.glDeleteShader(shaders[shaderNumber]);
					shaders[shaderNumber]=0;
				}
			}
			else{
				System.out.println("RTR: uninitialize => shader count is 0");
			}
			GLES32.glDeleteProgram(shaderProgramObject);
			shaderProgramObject = 0;
			GLES32.glUseProgram(0);
		}

		if(vbo_color_i1[0] != 0)
		{
			System.out.println("RTR: uninitialize => deleting vbo_color_i1");

			GLES32.glDeleteBuffers(1, vbo_color_i1, 0);
			vbo_color_i1[0] = 0;
		}
		if(vbo_position_i1[0] != 0)
		{
			System.out.println("RTR: uninitialize => deleting vbo_position_i1");

			GLES32.glDeleteBuffers(1, vbo_position_i1, 0);
			vbo_position_i1[0] = 0;
		}
		if(vao_i1[0] != 0)
		{
			System.out.println("RTR: uninitialize => deleting vao_i1");

			GLES32.glDeleteVertexArrays(1, vao_i1, 0);
			vao_i1[0]=0;
		}

		if(vbo_color_n[0] != 0)
		{
			System.out.println("RTR: uninitialize => deleting vbo_color_n");

			GLES32.glDeleteBuffers(1, vbo_color_n, 0);
			vbo_color_n[0] = 0;
		}
		if(vbo_position_n[0] != 0)
		{
			System.out.println("RTR: uninitialize => deleting vbo_position_n");

			GLES32.glDeleteBuffers(1, vbo_position_n, 0);
			vbo_position_n[0] = 0;
		}
		if(vao_n[0] != 0)
		{
			System.out.println("RTR: uninitialize => deleting vao_n");

			GLES32.glDeleteVertexArrays(1, vao_n, 0);
			vao_n[0]=0;
		}

		if(vbo_color_d[0] != 0)
		{
			System.out.println("RTR: uninitialize => deleting vbo_color_d");

			GLES32.glDeleteBuffers(1, vbo_color_d, 0);
			vbo_color_d[0] = 0;
		}
		if(vbo_position_d[0] != 0)
		{
			System.out.println("RTR: uninitialize => deleting vbo_position_d");

			GLES32.glDeleteBuffers(1, vbo_position_d, 0);
			vbo_position_d[0] = 0;
		}
		if(vao_d[0] != 0)
		{
			System.out.println("RTR: uninitialize => deleting vao_d");

			GLES32.glDeleteVertexArrays(1, vao_d, 0);
			vao_d[0]=0;
		}

		if(vbo_color_i2[0] != 0)
		{
			System.out.println("RTR: uninitialize => deleting vbo_color_i2");

			GLES32.glDeleteBuffers(1, vbo_color_i2, 0);
			vbo_color_i2[0] = 0;
		}
		if(vbo_position_i2[0] != 0)
		{
			System.out.println("RTR: uninitialize => deleting vbo_position_i2");

			GLES32.glDeleteBuffers(1, vbo_position_i2, 0);
			vbo_position_i2[0] = 0;
		}
		if(vao_i2[0] != 0)
		{
			System.out.println("RTR: uninitialize => deleting vao_i2");

			GLES32.glDeleteVertexArrays(1, vao_i2, 0);
			vao_i2[0]=0;
		}

		if(vbo_color_a[0] != 0)
		{
			System.out.println("RTR: uninitialize => deleting vbo_color_a");

			GLES32.glDeleteBuffers(1, vbo_color_a, 0);
			vbo_color_a[0] = 0;
		}
		if(vbo_position_a[0] != 0)
		{
			System.out.println("RTR: uninitialize => deleting vbo_position_a");

			GLES32.glDeleteBuffers(1, vbo_position_a, 0);
			vbo_position_a[0] = 0;
		}
		if(vao_a[0] != 0)
		{
			System.out.println("RTR: uninitialize => deleting vao_a");

			GLES32.glDeleteVertexArrays(1, vao_a, 0);
			vao_a[0]=0;
		}

		if(vbo_color_flag[0] != 0)
		{
			System.out.println("RTR: uninitialize => deleting vbo_color_flag");

			GLES32.glDeleteBuffers(1, vbo_color_flag, 0);
			vbo_color_flag[0] = 0;
		}
		if(vbo_position_flag[0] != 0)
		{
			System.out.println("RTR: uninitialize => deleting vbo_position_flag");

			GLES32.glDeleteBuffers(1, vbo_position_flag, 0);
			vbo_position_flag[0] = 0;
		}
		if(vao_flag[0] != 0)
		{
			System.out.println("RTR: uninitialize => deleting vao_flag");

			GLES32.glDeleteVertexArrays(1, vao_flag, 0);
			vao_flag[0]=0;
		}
	}

    private void initialize()
	{
		System.out.println("RTR: Called initialize");

		vertexShaderObject = GLES32.glCreateShader(GLES32.GL_VERTEX_SHADER);
		final String vertexShaderSourceCode = String.format
		(
			"	#version 320 es									" +
			"												\n 	" +
			"	in vec4 vPosition;								" +
			"	in vec4 vColor;									" +
			"												\n 	" +
			"	out vec4 out_color;								" +
			"												\n 	" +
			"	uniform mat4 u_mvp_matrix;						" +
			"												\n 	" +
			"	void main(void)									" +
			"	{												" +
			"		gl_Position = u_mvp_matrix * vPosition;		" +
			"		out_color = vColor;							" +
			"	}												"

		);
		GLES32.glShaderSource(vertexShaderObject, vertexShaderSourceCode);
		GLES32.glCompileShader(vertexShaderObject);
		int[] iShaderCompileStatus = new int[1];
		int[] iInfoLogLength = new int[1];
		String szInfoLog = null;

		GLES32.glGetShaderiv(vertexShaderObject, GLES32.GL_COMPILE_STATUS, iShaderCompileStatus, 0);

		if(iShaderCompileStatus[0] == GLES32.GL_FALSE)
		{
			GLES32.glGetShaderiv(vertexShaderObject, GLES32.GL_INFO_LOG_LENGTH, iInfoLogLength, 0);
			if(iInfoLogLength[0] > 0)
			{
				szInfoLog = GLES32.glGetShaderInfoLog(vertexShaderObject);
				System.out.println("RTR: VERTEX SHADER COMPILATION LOG:" + szInfoLog);
				uninitialize();
				System.exit(0);
			}
		}

		//fragment shader
		fragmentShaderObject = GLES32.glCreateShader(GLES32.GL_FRAGMENT_SHADER);
		
		final String fragmentShaderSourceCode = String.format
		(
			"	#version 320 es				" +
			"							\n 	" +
			"	precision highp float;	\n 	" +
			"	in vec4 out_color;			" +
			"							\n 	" +
			"	out vec4 FragColor;			" +
			"							\n 	" +
			"	void main(void)				" +
			"	{							" +
			"		FragColor = out_color;	" +
			"	}							"
		);

		GLES32.glShaderSource(fragmentShaderObject, fragmentShaderSourceCode);
		GLES32.glCompileShader(fragmentShaderObject);
		iShaderCompileStatus[0] = 0;
		iInfoLogLength[0] = 0;
		szInfoLog = null;

		GLES32.glGetShaderiv(fragmentShaderObject, GLES32.GL_COMPILE_STATUS, iShaderCompileStatus, 0);
		
		if(iShaderCompileStatus[0] == GLES32.GL_FALSE)
		{
			GLES32.glGetShaderiv(fragmentShaderObject, GLES32.GL_INFO_LOG_LENGTH, iInfoLogLength, 0);

			if(iInfoLogLength[0] > 0)
			{
				szInfoLog = GLES32.glGetShaderInfoLog(fragmentShaderObject);
				System.out.println("RTR: FRAGMENT SHADER COMPILATION LOG:" + szInfoLog);
				uninitialize();
				System.exit(0);
			}
		}

		int[] shaderCount = new int[1];

		//shader program
		shaderProgramObject = GLES32.glCreateProgram();

		GLES32.glGetProgramiv(shaderProgramObject, GLES32.GL_ATTACHED_SHADERS, shaderCount, 0);
		System.out.println("RTR: initialize => BEFORE ATTACHING SHADERS shader count is :: " + shaderCount[0]);

		GLES32.glAttachShader(shaderProgramObject, vertexShaderObject);
		GLES32.glAttachShader(shaderProgramObject, fragmentShaderObject);

		GLES32.glGetProgramiv(shaderProgramObject, GLES32.GL_ATTACHED_SHADERS, shaderCount, 0);
		System.out.println("RTR: initialize => AFTER ATTACHING SHADERS shader count is :: " + shaderCount[0]);

		//prelinking binding to attributes
		GLES32.glBindAttribLocation(shaderProgramObject,GLESMacros.PS_ATTRIBUTE_POSITION, "vPosition" );
		GLES32.glBindAttribLocation(shaderProgramObject,GLESMacros.PS_ATTRIBUTE_COLOR, "vColor" );

		//link
		GLES32.glLinkProgram(shaderProgramObject);
		//error checking
		int[] iProgramLinkStatus = new int[1];

		iInfoLogLength[0] = 0;
		szInfoLog = null;

		GLES32.glGetProgramiv(shaderProgramObject, GLES32.GL_LINK_STATUS, iProgramLinkStatus, 0);

		if(iProgramLinkStatus[0] == GLES32.GL_FALSE)
		{
			GLES32.glGetProgramiv(shaderProgramObject, GLES32.GL_INFO_LOG_LENGTH, iInfoLogLength, 0);
			if(iInfoLogLength[0] > 0)
			{
				szInfoLog = GLES32.glGetShaderInfoLog(shaderProgramObject);
				System.out.println("RTR SHADER PROGRAM LINKING LOG:" + szInfoLog);
				uninitialize();
				System.exit(0);
			}
		}

		//Get uniform location
		mvpUniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_mvp_matrix");

		ByteBuffer byteBuffer;
		FloatBuffer positionBuffer;
		FloatBuffer colorBuffer;
		
		//-------------------------------- I -------------------------------------------
		// Position vertices array, as it is inline initialization, no need of specifying size of array on rhs 
		final float[] i1Vertices = new float[]
		{
			-1.20f, 0.5f, 0.0f,
			- 0.88f, 0.5f, 0.0f,
			-1.04f, 0.5f, 0.0f,
			-1.04f, -0.5f, 0.0f,
			-1.20f, -0.5f, 0.0f,
			- 0.88f, -0.5f, 0.0f,
		};

		final float[] i1Color = new float[]
		{
			1.0f, 0.6f, 0.2f,
			1.0f, 0.6f, 0.2f,
			1.0f, 0.6f, 0.2f,
			0.070f, 0.533f, 0.027f,
			0.070f, 0.533f, 0.027f,
			0.070f, 0.533f, 0.027f,
		};
		//create and bind with vao
		GLES32.glGenVertexArrays(1, vao_i1, 0);
		GLES32.glBindVertexArray(vao_i1[0]);
		//Create and bind with vbo for position
		GLES32.glGenBuffers(1, vbo_position_i1, 0);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vbo_position_i1[0]);
		//Allocate the buffer directly from native memory
		byteBuffer = ByteBuffer.allocateDirect(i1Vertices.length*4); //4 is size of float in java
		//Arrange the byte order of buffer to the native byte order
		byteBuffer.order(ByteOrder.nativeOrder());
		//create the float type buffer and convert our byte type buffer into float type buffer
		positionBuffer = byteBuffer.asFloatBuffer();
		//Now put your array into the "COOKED" buffer
		positionBuffer.put(i1Vertices);
		//set the array at 0th position of the buffer as our array is not interleaved
		positionBuffer.position(0);
		//data to the buffer
		GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER, i1Vertices.length * 4, positionBuffer,GLES32.GL_STATIC_DRAW);
		GLES32.glVertexAttribPointer(GLESMacros.PS_ATTRIBUTE_POSITION, 
										3,				// break data after x y z, i.e. 3
										GLES32.GL_FLOAT, // type of data
										false, 			// normalize?
										0, 				//stride?-no
										0 );			//offset?-no(windows : NULL, in java null is not #define 0) therefore 0
		GLES32.glEnableVertexAttribArray(GLESMacros.PS_ATTRIBUTE_POSITION);
		
		//bind to buffer
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0);
		
		// for color
		//Create and bind with vbo for position
		GLES32.glGenBuffers(1, vbo_color_i1, 0);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vbo_color_i1[0]);

		//Allocate the buffer directly from native memory
		byteBuffer = ByteBuffer.allocateDirect(i1Color.length*4); //4 is size of float in java
		byteBuffer.order(ByteOrder.nativeOrder());
		colorBuffer = byteBuffer.asFloatBuffer();
		colorBuffer.put(i1Color);
		colorBuffer.position(0);

		//data to the color buffer
		GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER, i1Color.length * 4, colorBuffer,GLES32.GL_STATIC_DRAW);
		GLES32.glVertexAttribPointer(GLESMacros.PS_ATTRIBUTE_COLOR, 
										3,				// break data after x y z, i.e. 3
										GLES32.GL_FLOAT, // type of data
										false, 			// normalize?
										0, 				//stride?-no
										0 );			//offset?-no(windows : NULL, in java null is not #define 0) therefore 0
		GLES32.glEnableVertexAttribArray(GLESMacros.PS_ATTRIBUTE_COLOR);
		
		//bind to buffer
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0);
		
		GLES32.glBindVertexArray(0);
		//------------------------------------------------------------------------------
		
		//------------------------------------- N --------------------------------------
		final float[] nVertices = new float[]
		{
			-0.68f, 0.52f, 0.0f,
			-0.68f, -0.52f, 0.0f,
			-0.68f, 0.52f, 0.0f,
			-0.36f, -0.51f, 0.0f,
			-0.36f, 0.52f, 0.0f,
			-0.36f, -0.52f, 0.0f
		};

		final float[] nColor = new float[]
		{
			1.0f, 0.6f, 0.2f,
			0.070f, 0.533f, 0.027f,
			1.0f, 0.6f, 0.2f,
			0.070f, 0.533f, 0.027f,
			1.0f, 0.6f, 0.2f,
			0.070f, 0.533f, 0.027f
		};

		//create and bind with vao
		GLES32.glGenVertexArrays(1, vao_n, 0);
		GLES32.glBindVertexArray(vao_n[0]);
		//Create and bind with vbo for position
		GLES32.glGenBuffers(1, vbo_position_n, 0);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vbo_position_n[0]);
		//Allocate the buffer directly from native memory
		byteBuffer = ByteBuffer.allocateDirect(nVertices.length*4); //4 is size of float in java
		//Arrange the byte order of buffer to the native byte order
		byteBuffer.order(ByteOrder.nativeOrder());
		//create the float type buffer and convert our byte type buffer into float type buffer
		positionBuffer = byteBuffer.asFloatBuffer();
		//Now put your array into the "COOKED" buffer
		positionBuffer.put(nVertices);
		//set the array at 0th position of the buffer as our array is not interleaved
		positionBuffer.position(0);
		//data to the buffer
		GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER, nVertices.length * 4, positionBuffer,GLES32.GL_STATIC_DRAW);
		GLES32.glVertexAttribPointer(GLESMacros.PS_ATTRIBUTE_POSITION, 
										3,				// break data after x y z, i.e. 3
										GLES32.GL_FLOAT, // type of data
										false, 			// normalize?
										0, 				//stride?-no
										0 );			//offset?-no(windows : NULL, in java null is not #define 0) therefore 0
		GLES32.glEnableVertexAttribArray(GLESMacros.PS_ATTRIBUTE_POSITION);
		//unbind to buffer
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0);
		
		// for color
		//Create and bind with vbo for position
		GLES32.glGenBuffers(1, vbo_color_n, 0);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vbo_color_n[0]);

		//Allocate the buffer directly from native memory
		byteBuffer = ByteBuffer.allocateDirect(nColor.length*4); //4 is size of float in java
		byteBuffer.order(ByteOrder.nativeOrder());
		colorBuffer = byteBuffer.asFloatBuffer();
		colorBuffer.put(nColor);
		colorBuffer.position(0);

		//data to the color buffer
		GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER, nColor.length * 4, colorBuffer,GLES32.GL_STATIC_DRAW);
		GLES32.glVertexAttribPointer(GLESMacros.PS_ATTRIBUTE_COLOR, 
										3,				// break data after x y z, i.e. 3
										GLES32.GL_FLOAT, // type of data
										false, 			// normalize?
										0, 				//stride?-no
										0 );			//offset?-no(windows : NULL, in java null is not #define 0) therefore 0
		GLES32.glEnableVertexAttribArray(GLESMacros.PS_ATTRIBUTE_COLOR);
		
		//unbind to buffer
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0);
		// unbind to vertex array buffer
		GLES32.glBindVertexArray(0);
		//------------------------------------------------------------------------------
		
		// -------------------------------------- D -------------------------------------
		final float[] dVertices = new float[]
		{
			-0.10f,  0.5f, 0.0f,
			-0.10f, -0.5f, 0.0f,
			-0.17f,  0.5f, 0.0f,
			 0.17f,  0.5f, 0.0f,
			 0.17f,  0.5f, 0.0f,
			 0.17f, -0.5f, 0.0f,
			-0.17f, -0.5f, 0.0f,
			 0.17f, -0.5f, 0.0f
		};

		final float[] dColor = new float[]
		{
			1.0f,	0.6f, 	0.2f,
			0.070f, 0.533f, 0.027f,
			1.0f, 	0.6f, 	0.2f,
			1.0f, 	0.6f,	0.2f,
			1.0f, 	0.6f, 	0.2f,
			0.070f, 0.533f, 0.027f,
			0.070f, 0.533f, 0.027f,
			0.070f, 0.533f, 0.027f
		};

		//create and bind with vao
		GLES32.glGenVertexArrays(1, vao_d, 0);
		GLES32.glBindVertexArray(vao_d[0]);
		
		//Create and bind with vbo for position
		GLES32.glGenBuffers(1, vbo_position_d, 0);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vbo_position_d[0]);
		
		//Allocate the buffer directly from native memory
		byteBuffer = ByteBuffer.allocateDirect(dVertices.length*4); //4 is size of float in java
		byteBuffer.order(ByteOrder.nativeOrder());
		positionBuffer = byteBuffer.asFloatBuffer();
		positionBuffer.put(dVertices);
		positionBuffer.position(0);

		//data to the buffer
		GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER, dVertices.length * 4, positionBuffer,GLES32.GL_STATIC_DRAW);
		GLES32.glVertexAttribPointer(GLESMacros.PS_ATTRIBUTE_POSITION, 
										3,				// break data after x y z, i.e. 3
										GLES32.GL_FLOAT, // type of data
										false, 			// normalize?
										0, 				//stride?-no
										0 );			//offset?-no(windows : NULL, in java null is not #define 0) therefore 0
		GLES32.glEnableVertexAttribArray(GLESMacros.PS_ATTRIBUTE_POSITION);
		//unbind to buffer
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0);
		
		// for color
		//Create and bind with vbo for position
		GLES32.glGenBuffers(1, vbo_color_d, 0);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vbo_color_d[0]);

		//Allocate the buffer directly from native memory
		byteBuffer = ByteBuffer.allocateDirect(dColor.length*4); //4 is size of float in java
		byteBuffer.order(ByteOrder.nativeOrder());
		colorBuffer = byteBuffer.asFloatBuffer();
		colorBuffer.put(dColor);
		colorBuffer.position(0);

		//data to the color buffer
		GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER, dColor.length * 4, colorBuffer,GLES32.GL_STATIC_DRAW);
		GLES32.glVertexAttribPointer(GLESMacros.PS_ATTRIBUTE_COLOR, 
										3,				// break data after x y z, i.e. 3
										GLES32.GL_FLOAT, // type of data
										false, 			// normalize?
										0, 				//stride?-no
										0 );			//offset?-no(windows : NULL, in java null is not #define 0) therefore 0
		GLES32.glEnableVertexAttribArray(GLESMacros.PS_ATTRIBUTE_COLOR);
		
		//unbind to buffer
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0);
		
		// unbind to vertex array buffer
		GLES32.glBindVertexArray(0);
		//------------------------------------------------------------------------------
		
		//-------------------------------- I2 -------------------------------------------
		final float[] i2Vertices = new float[]
		{
			0.36f,  0.5f, 0.0f,
			0.68f,  0.5f, 0.0f,
			0.52f,  0.5f, 0.0f,
			0.52f, -0.5f, 0.0f,
			0.36f, -0.5f, 0.0f,
			0.68f, -0.5f, 0.0f
		};

		final float[] i2Color = new float[]
		{
			1.0f, 	0.6f, 	0.2f,
			1.0f, 	0.6f, 	0.2f,
			1.0f, 	0.6f, 	0.2f,
			0.070f, 0.533f, 0.027f,
			0.070f, 0.533f, 0.027f,
			0.070f, 0.533f, 0.027f
		};

		//create and bind with vao
		GLES32.glGenVertexArrays(1, vao_i2, 0);
		GLES32.glBindVertexArray(vao_i2[0]);
		
		//Create and bind with vbo for position
		GLES32.glGenBuffers(1, vbo_position_i2, 0);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vbo_position_i2[0]);
		
		//Allocate the buffer directly from native memory
		byteBuffer = ByteBuffer.allocateDirect(i2Vertices.length*4); //4 is size of float in java
		byteBuffer.order(ByteOrder.nativeOrder());
		positionBuffer = byteBuffer.asFloatBuffer();
		positionBuffer.put(i2Vertices);
		positionBuffer.position(0);

		//data to the buffer
		GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER, i2Vertices.length * 4, positionBuffer,GLES32.GL_STATIC_DRAW);
		GLES32.glVertexAttribPointer(GLESMacros.PS_ATTRIBUTE_POSITION, 
										3,				// break data after x y z, i.e. 3
										GLES32.GL_FLOAT, // type of data
										false, 			// normalize?
										0, 				//stride?-no
										0 );			//offset?-no(windows : NULL, in java null is not #define 0) therefore 0
		GLES32.glEnableVertexAttribArray(GLESMacros.PS_ATTRIBUTE_POSITION);
		//unbind to buffer
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0);
		
		// for color
		//Create and bind with vbo for position
		GLES32.glGenBuffers(1, vbo_color_i2, 0);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vbo_color_i2[0]);

		//Allocate the buffer directly from native memory
		byteBuffer = ByteBuffer.allocateDirect(i2Color.length*4); //4 is size of float in java
		byteBuffer.order(ByteOrder.nativeOrder());
		colorBuffer = byteBuffer.asFloatBuffer();
		colorBuffer.put(i2Color);
		colorBuffer.position(0);

		//data to the color buffer
		GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER, i2Color.length * 4, colorBuffer,GLES32.GL_STATIC_DRAW);
		GLES32.glVertexAttribPointer(GLESMacros.PS_ATTRIBUTE_COLOR, 
										3,				// break data after x y z, i.e. 3
										GLES32.GL_FLOAT, // type of data
										false, 			// normalize?
										0, 				//stride?-no
										0 );			//offset?-no(windows : NULL, in java null is not #define 0) therefore 0
		GLES32.glEnableVertexAttribArray(GLESMacros.PS_ATTRIBUTE_COLOR);
		
		//unbind to buffer
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0);
		
		// unbind to vertex array buffer
		GLES32.glBindVertexArray(0);
		//------------------------------------------------------------------------------
		
		//--------------------------------------- A -----------------------------------------
		final float[] aVertices = new float[]
		{
			0.83f, -0.52f, 0.0f,
			1.04f, 	0.52f, 0.0f,
			1.04f, 	0.52f, 0.0f,
			1.25f, -0.52f, 0.0f
		};

		final float[] aColor = new float[]
		{
			0.070f, 0.533f, 0.027f,
			1.0f, 	0.6f, 	0.2f,
			1.0f, 	0.6f, 	0.2f,
			0.070f, 0.533f, 0.027f
		};

		//create and bind with vao
		GLES32.glGenVertexArrays(1, vao_a, 0);
		GLES32.glBindVertexArray(vao_a[0]);
		
		//Create and bind with vbo for position
		GLES32.glGenBuffers(1, vbo_position_a, 0);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vbo_position_a[0]);
		
		//Allocate the buffer directly from native memory
		byteBuffer = ByteBuffer.allocateDirect(aVertices.length*4); //4 is size of float in java
		byteBuffer.order(ByteOrder.nativeOrder());
		positionBuffer = byteBuffer.asFloatBuffer();
		positionBuffer.put(aVertices);
		positionBuffer.position(0);

		//data to the buffer
		GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER, aVertices.length * 4, positionBuffer,GLES32.GL_STATIC_DRAW);
		GLES32.glVertexAttribPointer(GLESMacros.PS_ATTRIBUTE_POSITION, 
										3,				// break data after x y z, i.e. 3
										GLES32.GL_FLOAT, // type of data
										false, 			// normalize?
										0, 				//stride?-no
										0 );			//offset?-no(windows : NULL, in java null is not #define 0) therefore 0
		GLES32.glEnableVertexAttribArray(GLESMacros.PS_ATTRIBUTE_POSITION);
		//unbind to buffer
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0);
		
		// for color
		//Create and bind with vbo for position
		GLES32.glGenBuffers(1, vbo_color_a, 0);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vbo_color_a[0]);

		//Allocate the buffer directly from native memory
		byteBuffer = ByteBuffer.allocateDirect(aColor.length*4); //4 is size of float in java
		byteBuffer.order(ByteOrder.nativeOrder());
		colorBuffer = byteBuffer.asFloatBuffer();
		colorBuffer.put(aColor);
		colorBuffer.position(0);

		//data to the color buffer
		GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER, aColor.length * 4, colorBuffer,GLES32.GL_STATIC_DRAW);
		GLES32.glVertexAttribPointer(GLESMacros.PS_ATTRIBUTE_COLOR, 
										3,				// break data after x y z, i.e. 3
										GLES32.GL_FLOAT, // type of data
										false, 			// normalize?
										0, 				//stride?-no
										0 );			//offset?-no(windows : NULL, in java null is not #define 0) therefore 0
		GLES32.glEnableVertexAttribArray(GLESMacros.PS_ATTRIBUTE_COLOR);
		
		//unbind to buffer
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0);
		
		// unbind to vertex array buffer
		GLES32.glBindVertexArray(0);
		//------------------------------------------------------------------------------
		
		//--------------------------------------- FLAG -----------------------------------------
		final float[] flagVertices = new float[]
		{
			0.953f,  0.015f, 0.0f,
			1.128f,  0.015f, 0.0f,
			1.122f,  0.045f, 0.0f,
			0.958f,  0.045f, 0.0f,
			0.947f, -0.015f, 0.0f,
			1.135f, -0.015f, 0.0f,
			1.128f,  0.015f, 0.0f,
			0.953f,  0.015f, 0.0f,
			1.135f, -0.015f, 0.0f,
			0.947f, -0.015f, 0.0f,
			0.94f, 	-0.045f, 0.0f,
			1.14f, 	-0.045f, 0.0f
		};

		final float[] flagColor = new float[]
		{
			1.0f, 	0.6f, 	0.2f,
			1.0f, 	0.6f, 	0.2f,
			1.0f, 	0.6f, 	0.2f,
			1.0f, 	0.6f, 	0.2f,
			1.0f, 	1.0f, 	1.0f,
			1.0f, 	1.0f, 	1.0f,
			1.0f, 	1.0f, 	1.0f,
			1.0f, 	1.0f, 	1.0f,
			0.070f, 0.533f, 0.027f,
			0.070f, 0.533f, 0.027f,
			0.070f, 0.533f, 0.027f,
			0.070f, 0.533f, 0.027f
		};

		//create and bind with vao
		GLES32.glGenVertexArrays(1, vao_flag, 0);
		GLES32.glBindVertexArray(vao_flag[0]);
		
		//Create and bind with vbo for position
		GLES32.glGenBuffers(1, vbo_position_flag, 0);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vbo_position_flag[0]);
		
		//Allocate the buffer directly from native memory
		byteBuffer = ByteBuffer.allocateDirect(flagVertices.length*4); //4 is size of float in java
		byteBuffer.order(ByteOrder.nativeOrder());
		positionBuffer = byteBuffer.asFloatBuffer();
		positionBuffer.put(flagVertices);
		positionBuffer.position(0);

		//data to the buffer
		GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER, flagVertices.length * 4, positionBuffer,GLES32.GL_STATIC_DRAW);
		GLES32.glVertexAttribPointer(GLESMacros.PS_ATTRIBUTE_POSITION, 
										3,				// break data after x y z, i.e. 3
										GLES32.GL_FLOAT, // type of data
										false, 			// normalize?
										0, 				//stride?-no
										0 );			//offset?-no(windows : NULL, in java null is not #define 0) therefore 0
		GLES32.glEnableVertexAttribArray(GLESMacros.PS_ATTRIBUTE_POSITION);
		//unbind to buffer
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0);
		
		// for color
		//Create and bind with vbo for position
		GLES32.glGenBuffers(1, vbo_color_flag, 0);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vbo_color_flag[0]);

		//Allocate the buffer directly from native memory
		byteBuffer = ByteBuffer.allocateDirect(flagColor.length*4); //4 is size of float in java
		byteBuffer.order(ByteOrder.nativeOrder());
		colorBuffer = byteBuffer.asFloatBuffer();
		colorBuffer.put(flagColor);
		colorBuffer.position(0);

		//data to the color buffer
		GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER, flagColor.length * 4, colorBuffer,GLES32.GL_STATIC_DRAW);
		GLES32.glVertexAttribPointer(GLESMacros.PS_ATTRIBUTE_COLOR, 
										3,				// break data after x y z, i.e. 3
										GLES32.GL_FLOAT, // type of data
										false, 			// normalize?
										0, 				//stride?-no
										0 );			//offset?-no(windows : NULL, in java null is not #define 0) therefore 0
		GLES32.glEnableVertexAttribArray(GLESMacros.PS_ATTRIBUTE_COLOR);
		
		//unbind to buffer
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0);
		
		// unbind to vertex array buffer
		GLES32.glBindVertexArray(0);
		//------------------------------------------------------------------------------
		
		GLES32.glClearColor(0.0f,0.0f,0.0f,1.0f);
		GLES32.glEnable(GLES32.GL_DEPTH_TEST);
		GLES32.glDepthFunc(GLES32.GL_LEQUAL);

		Matrix.setIdentityM(perspectiveProjectionMatrix,0);
		//no warmup call to resize as we are already in fullscreen. 
		//we dont run multiple app with screen sharing in android unlike windows.
    }

    private void resize(int width, int height)
    {
        if(height<0)
        {
            height =1;
        }
		GLES32.glViewport(0,0,width, height);
		Matrix.perspectiveM(perspectiveProjectionMatrix, 0, 45.0f, (float)width/ (float)height, 0.1f, 100.0f);
    }

	private void display()
	{
		//variable declarations
		float[] modelViewMatrix = new float[16];
		float[] modelViewProjectionMatrix = new float[16];

		//code
		GLES32.glClear(GLES32.GL_COLOR_BUFFER_BIT|GLES32.GL_DEPTH_BUFFER_BIT);
		GLES32.glUseProgram(shaderProgramObject);

		//making martices identity
		Matrix.setIdentityM(modelViewMatrix, 0);
		Matrix.setIdentityM(modelViewProjectionMatrix, 0);

		//matrix transformation
		Matrix.translateM(modelViewMatrix, 0, 0.0f, 0.0f, -2.0f);
		Matrix.multiplyMM(modelViewProjectionMatrix, 0,
						  perspectiveProjectionMatrix, 0,
						  modelViewMatrix, 0);

		//send matrix to shader
		GLES32.glUniformMatrix4fv(mvpUniform, 1, false, modelViewProjectionMatrix, 0);

		GLES32.glLineWidth(15.0f);
		// ******************* I ************************
		GLES32.glBindVertexArray(vao_i1[0]);
		GLES32.glDrawArrays(GLES32.GL_LINES, 0, 6);
		//unbind vao
		GLES32.glBindVertexArray(0);
		// **********************************************

		// ******************* N ************************
		GLES32.glBindVertexArray(vao_n[0]);
		GLES32.glDrawArrays(GLES32.GL_LINES, 0, 6);
		//unbind vao
		GLES32.glBindVertexArray(0);
		// **********************************************

		// ******************* D ************************
		GLES32.glBindVertexArray(vao_d[0]);
		GLES32.glDrawArrays(GLES32.GL_LINES, 0, 8);
		//unbind vao
		GLES32.glBindVertexArray(0);
		// **********************************************

		// ******************* I ************************
		GLES32.glBindVertexArray(vao_i2[0]);
		GLES32.glDrawArrays(GLES32.GL_LINES, 0, 6);
		//unbind vao
		GLES32.glBindVertexArray(0);
		// **********************************************

		// ******************* A ************************
		GLES32.glBindVertexArray(vao_a[0]);
		GLES32.glDrawArrays(GLES32.GL_LINES, 0, 4);
		//unbind vao
		GLES32.glBindVertexArray(0);
		// **********************************************

		// ***************** FLAG ***********************
		GLES32.glBindVertexArray(vao_flag[0]);
		GLES32.glDrawArrays(GLES32.GL_TRIANGLE_FAN, 0, 4);
		GLES32.glDrawArrays(GLES32.GL_TRIANGLE_FAN, 4, 4);
		GLES32.glDrawArrays(GLES32.GL_TRIANGLE_FAN, 8, 4);
		//unbind vao
		GLES32.glBindVertexArray(0);
		// **********************************************

		//unuse program
		GLES32.glUseProgram(0);
        requestRender();
    }

}
