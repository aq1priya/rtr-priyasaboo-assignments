package rtr.example.tessellation;

public class GLESMacros{
    public static final int PS_ATTRIBUTE_POSITION =0;
    public static final int PS_ATTRIBUTE_COLOR = 1;
    public static final int PS_ATTRIBUTE_NORMAL = 2;
    public static final int PS_ATTRIBUTE_TEXCOORD0 = 3;
}
