package rtr.example.tessellation;

//packages added by me
import android.content.Context;
import android.graphics.Color;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.GestureDetector;
import android.view.GestureDetector.OnGestureListener;
import rtr.example.tessellation.GLESMacros;
import android.view.GestureDetector.OnDoubleTapListener;

import android.opengl.GLSurfaceView;
import android.opengl.GLES32;

import javax.microedition.khronos.opengles.GL10;
import javax.microedition.khronos.egl.EGLConfig;

//for opengl buffers
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;

//for matrix mathematics
import android.opengl.Matrix;

public class GLESView extends GLSurfaceView implements GLSurfaceView.Renderer, OnGestureListener, OnDoubleTapListener{
    private GestureDetector gestureDetector;
    private final Context context;

	private int vertexShaderObject;
	private int tessellationControlShaderObject;
	private int tessellationEvaluationShaderObject;
	private int fragmentShaderObject;
	private int shaderProgramObject;

	private int[] vao = new int[1];
	private int[] vbo = new int[1];

	private int mvpUniform;
	private int segmentsUniform;
	private int stripsUniform;
	private int lineColorUniform;
	private int numberOfLineSegments = 1;

	private float[] perspectiveProjectionMatrix = new float[16];		//4x4 matrix
	private boolean isTessLevelIncrementing = false;

    //constructor
    public GLESView(Context drawingContext){
        super(drawingContext);
        context = drawingContext;
        
        setEGLContextClientVersion(3);
        setRenderer(this);
        setRenderMode(GLSurfaceView.RENDERMODE_WHEN_DIRTY);

        gestureDetector =new GestureDetector(drawingContext, this, null, false );
        gestureDetector.setOnDoubleTapListener(this);
    }

    //handling 'onTouchEvent' is the most important,
	//because it triggers all gesture and tap events
	@Override
	public boolean onTouchEvent(MotionEvent event)
	{
		//code
		int eventaction = event.getAction();
		if(!gestureDetector.onTouchEvent(event))
			super.onTouchEvent(event);
		return(true);
	}

    //abstract method from OnDoubleTapListener so must be implemented
	@Override
	public boolean onDoubleTap(MotionEvent e)
	{
		//setText("Double Tap");
		isTessLevelIncrementing = true;
		numberOfLineSegments++;
		if(numberOfLineSegments >10)
			numberOfLineSegments = 10;
		return(true);
    }
    
    //abstract method from OnDoubleTapListener so must be implemented
	@Override
	public boolean onDoubleTapEvent(MotionEvent e)
	{
		//do not write any code here because already written in 'onDoubleTap'
		return(true);
    }
    
    //abstract  method from OnDoubleTapListener so must be implemented
	@Override
	public boolean onSingleTapConfirmed(MotionEvent e)
	{
		isTessLevelIncrementing = false;
		numberOfLineSegments--;
		if(numberOfLineSegments <=0)
			numberOfLineSegments = 1;
		return(true);
    }
    
    //abstract  method from OnGestureListener so must be implemented
	@Override
	public boolean onDown(MotionEvent e)
	{
		//do not write any code here because already written in 'onSingleTapConfirmed'
		return(true);
    }
    
    //abstract  method from OnGestureListener so must be implemented
	@Override
	public boolean onFling(MotionEvent e1,MotionEvent e2, float velocityX, float velocityY)
	{
		return(true);
	}

	//abstract  method from OnGestureListener so must be implemented
	@Override
	public void onLongPress(MotionEvent e)
	{
		//setText("Long Press");
	}

	//abstract  method from OnGestureListener so must be implemented
	@Override
	public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY)
	{
		//setText("Scroll");
		uninitialize();
		System.exit(0);
		return(true);
	}

	//abstract  method from OnGestureListener so must be implemented
	@Override
	public void onShowPress(MotionEvent e)
	{
	
	}

	//abstract  method from OnGestureListener so must be implemented
	@Override
	public boolean onSingleTapUp(MotionEvent e)
	{
		return(true);
    }
    
    //implement GLSurfaceView.Renderer methods
    @Override
    public void onSurfaceCreated(GL10 gl, EGLConfig config){
		System.out.println("RTR: Called onSurfaceCreated");

        String version = gl.glGetString(GL10.GL_VERSION);
		System.out.println("RTR:" + version);
		
		version = gl.glGetString(GLES32.GL_SHADING_LANGUAGE_VERSION);
		System.out.println("RTR:" + version);
        initialize();
    }

    @Override
    public void onSurfaceChanged(GL10 unused, int width, int height)
    {
		System.out.println("RTR: Called onSurfaceChabged");

        resize(width, height);
    }

    @Override
    public void onDrawFrame(GL10 unused){
        display();
    }

    //our custom methods
	private void uninitialize()
	{
		System.out.println("RTR: Called uninitialize");
		
		if(shaderProgramObject != 0)
		{
			int[] shaderCount = new int[1];
			int shaderNumber;

			GLES32.glUseProgram(shaderProgramObject);

			GLES32.glGetProgramiv(shaderProgramObject, GLES32.GL_ATTACHED_SHADERS, shaderCount, 0);
			System.out.println("RTR: uninitialize => " + shaderCount[0]);
		
			int[] shaders =new int[shaderCount[0]];

			if(shaderCount[0] != 0)
			{
				GLES32.glGetAttachedShaders(shaderProgramObject, shaderCount[0], shaderCount, 0, shaders, 0);
				System.out.println("RTR: uninitialize => if shadercount != 0 ::" + shaderCount[0]);

				for(shaderNumber =0;shaderNumber<shaderCount[0]; shaderNumber++)
				{
					GLES32.glDetachShader(shaderProgramObject, shaders[shaderNumber]);
					GLES32.glDeleteShader(shaders[shaderNumber]);
					shaders[shaderNumber]=0;
				}
			}
			else{
				System.out.println("RTR: uninitialize => shader count is 0");
			}
			GLES32.glDeleteProgram(shaderProgramObject);
			shaderProgramObject = 0;
			GLES32.glUseProgram(0);
		}
		if(vbo[0] != 0)
		{
			System.out.println("RTR: uninitialize => deleting vbo");

			GLES32.glDeleteBuffers(1, vbo, 0);
			vbo[0] = 0;
		}
		if(vao[0] != 0)
		{
			System.out.println("RTR: uninitialize => deleting vao");

			GLES32.glDeleteVertexArrays(1, vao, 0);
			vao[0]=0;
		}
	}

    private void initialize()
	{
		System.out.println("RTR: Called initialize");

		// VERTEX SHADER
		vertexShaderObject = GLES32.glCreateShader(GLES32.GL_VERTEX_SHADER);
		final String vertexShaderSourceCode = String.format
		(
			"	#version 320 es									" +
			"                                               \n  " +
			"   in vec2 vPosition;                              " +
			"                                               \n  " +
			"   void main(void)                                 " +
			"   {                                               " +
			"       gl_Position = vec4(vPosition, 0.0, 1.0);    " +
			"	}												"
		);
		GLES32.glShaderSource(vertexShaderObject, vertexShaderSourceCode);
		GLES32.glCompileShader(vertexShaderObject);
		int[] iShaderCompileStatus = new int[1];
		int[] iInfoLogLength = new int[1];
		String szInfoLog = null;

		GLES32.glGetShaderiv(vertexShaderObject, GLES32.GL_COMPILE_STATUS, iShaderCompileStatus, 0);

		if(iShaderCompileStatus[0] == GLES32.GL_FALSE)
		{
			GLES32.glGetShaderiv(vertexShaderObject, GLES32.GL_INFO_LOG_LENGTH, iInfoLogLength, 0);
			if(iInfoLogLength[0] > 0)
			{
				szInfoLog = GLES32.glGetShaderInfoLog(vertexShaderObject);
				System.out.println("RTR: VERTEX SHADER COMPILATION LOG:" + szInfoLog);
				uninitialize();
				System.exit(0);
			}
		}
		
		// TESSELLATION CONTROL SHADER
		tessellationControlShaderObject = GLES32.glCreateShader(GLES32.GL_TESS_CONTROL_SHADER);
		final String tessControlShaderSourceCode = String.format
		(
			"	#version 320 es																	" +
			"                                               								\n  " +
			"   precision highp float;                    								\n  " +
			"                                               								\n  " +
			"   layout(vertices = 4) out;                                                       " +
    		"                                                                               \n  " +
    		"   uniform mediump int u_no_of_segments;                                           " +
    		"   uniform mediump int u_no_of_strips;                                             " +
    		"                                                                               \n  " +
    		"   void main(void)                                                                 " +
    		"   {                                                                               " +
			"		gl_TessLevelOuter[0] = float(u_no_of_strips);                           	" +
    		"		gl_TessLevelOuter[1] = float(u_no_of_segments);                      		    " +
			"       gl_out[gl_InvocationID].gl_Position = gl_in[gl_InvocationID].gl_Position;   " +
			" 	}																				"
		);

		GLES32.glShaderSource(tessellationControlShaderObject, tessControlShaderSourceCode);
		GLES32.glCompileShader(tessellationControlShaderObject);
		
		iShaderCompileStatus[0] = 0;
		iInfoLogLength[0] = 0;
		szInfoLog = null;

		GLES32.glGetShaderiv(tessellationControlShaderObject, GLES32.GL_COMPILE_STATUS, iShaderCompileStatus, 0);

		if(iShaderCompileStatus[0] == GLES32.GL_FALSE)
		{
			GLES32.glGetShaderiv(tessellationControlShaderObject, GLES32.GL_INFO_LOG_LENGTH, iInfoLogLength, 0);
			if(iInfoLogLength[0] > 0)
			{
				szInfoLog = GLES32.glGetShaderInfoLog(tessellationControlShaderObject);
				System.out.println("RTR: TESSELLATION CONTROL SHADER COMPILATION LOG:" + szInfoLog);
				uninitialize();
				System.exit(0);
			}
		}

		// TESSELLATION EVALUATION SHADER
		tessellationEvaluationShaderObject = GLES32.glCreateShader(GLES32.GL_TESS_EVALUATION_SHADER);
		final String tessEvaluationShaderSourceCode = String.format
		(
			"	#version 320 es													" +
			"                                               				\n  " +
			"   precision highp float;                    								\n  " +
			"                                               				\n  " +
			"   layout(isolines) in;                                        \n  " +
    		"                                                               \n  " +
    		"   uniform mat4 u_mvp_matrix;                                  \n  " +
    		"                                                               \n  " +
    		"   void main(void)                                             \n  " +
    		"   {                                                           \n  " +
    		"       float u = gl_TessCoord.x;                               \n  " +
    		"                                                               \n  " +
    		"       vec3 p0 = gl_in[0].gl_Position.xyz;						\n  " +
    		"       vec3 p1 = gl_in[1].gl_Position.xyz;						\n  " +
    		"       vec3 p2 = gl_in[2].gl_Position.xyz;						\n  " +
    		"       vec3 p3 = gl_in[3].gl_Position.xyz;						\n  " +
    		"       float u1 = (1.0 - u);                                   \n  " +
    		"       float u2 = u * u;                                       \n  " +
    		"       float b3 = u2 * u;                                      \n  " +
    		"       float b2 = 3.0 * u2 * u1;                               \n  " +
    		"       float b1 = 3.0 * u * u1 * u1;                           \n  " +
    		"       float b0 = u1 * u1 * u1;                                \n  " +
    		"       vec3 p = (p0 * b0) + (p1 * b1) + (p2 * b2) + (p3 * b3); \n  " +
    		"       gl_Position = u_mvp_matrix * vec4(p, 1.0);                  " +
    		" 	}																"
		);

		GLES32.glShaderSource(tessellationEvaluationShaderObject, tessEvaluationShaderSourceCode);
		GLES32.glCompileShader(tessellationEvaluationShaderObject);
		
		iShaderCompileStatus[0] = 0;
		iInfoLogLength[0] = 0;
		szInfoLog = null;

		GLES32.glGetShaderiv(tessellationEvaluationShaderObject, GLES32.GL_COMPILE_STATUS, iShaderCompileStatus, 0);

		if(iShaderCompileStatus[0] == GLES32.GL_FALSE)
		{
			GLES32.glGetShaderiv(tessellationEvaluationShaderObject, GLES32.GL_INFO_LOG_LENGTH, iInfoLogLength, 0);
			if(iInfoLogLength[0] > 0)
			{
				szInfoLog = GLES32.glGetShaderInfoLog(tessellationEvaluationShaderObject);
				System.out.println("RTR: TESSELLATION EVALUATION SHADER COMPILATION LOG:" + szInfoLog);
				uninitialize();
				System.exit(0);
			}
		}

		// FRAGMENT SHADER
		fragmentShaderObject = GLES32.glCreateShader(GLES32.GL_FRAGMENT_SHADER);
		final String fragmentShaderSourceCode = String.format
		(
			"	#version 320 es								" +
			"                                           \n  " +
			"   precision highp float;                  \n  " +
			"                                           \n  " +
			"   out vec4 FragColor;							" +
			"                                           \n  " +
			"   uniform vec4 u_lineColor;               \n  " +
			"                                           \n  " +
			"   void main(void)                             " +
			"   {                                           " +
			"		FragColor = u_lineColor;                " +
			"	}											"
		);

		GLES32.glShaderSource(fragmentShaderObject, fragmentShaderSourceCode);
		GLES32.glCompileShader(fragmentShaderObject);
		iShaderCompileStatus[0] = 0;
		iInfoLogLength[0] = 0;
		szInfoLog = null;

		GLES32.glGetShaderiv(fragmentShaderObject, GLES32.GL_COMPILE_STATUS, iShaderCompileStatus, 0);
		
		if(iShaderCompileStatus[0] == GLES32.GL_FALSE)
		{
			GLES32.glGetShaderiv(fragmentShaderObject, GLES32.GL_INFO_LOG_LENGTH, iInfoLogLength, 0);

			if(iInfoLogLength[0] > 0)
			{
				szInfoLog = GLES32.glGetShaderInfoLog(fragmentShaderObject);
				System.out.println("RTR: FRAGMENT SHADER COMPILATION LOG:" + szInfoLog);
				uninitialize();
				System.exit(0);
			}
		}

		int[] shaderCount = new int[1];

		//shader program
		shaderProgramObject = GLES32.glCreateProgram();

		GLES32.glGetProgramiv(shaderProgramObject, GLES32.GL_ATTACHED_SHADERS, shaderCount, 0);
		System.out.println("RTR: initialize => BEFORE ATTACHING SHADERS shader count is :: " + shaderCount[0]);

		GLES32.glAttachShader(shaderProgramObject, vertexShaderObject);
		GLES32.glAttachShader(shaderProgramObject, tessellationControlShaderObject);
		GLES32.glAttachShader(shaderProgramObject, tessellationEvaluationShaderObject);
		GLES32.glAttachShader(shaderProgramObject, fragmentShaderObject);

		GLES32.glGetProgramiv(shaderProgramObject, GLES32.GL_ATTACHED_SHADERS, shaderCount, 0);
		System.out.println("RTR: initialize => AFTER ATTACHING SHADERS shader count is :: " + shaderCount[0]);

		//prelinking binding to attributes
		GLES32.glBindAttribLocation(shaderProgramObject,GLESMacros.PS_ATTRIBUTE_POSITION, "vPosition" );

		//link
		GLES32.glLinkProgram(shaderProgramObject);
		//error checking
		int[] iProgramLinkStatus = new int[1];

		iInfoLogLength[0] = 0;
		szInfoLog = null;

		GLES32.glGetProgramiv(shaderProgramObject, GLES32.GL_LINK_STATUS, iProgramLinkStatus, 0);

		if(iProgramLinkStatus[0] == GLES32.GL_FALSE)
		{
			GLES32.glGetProgramiv(shaderProgramObject, GLES32.GL_INFO_LOG_LENGTH, iInfoLogLength, 0);
			if(iInfoLogLength[0] > 0)
			{
				szInfoLog = GLES32.glGetShaderInfoLog(shaderProgramObject);
				System.out.println("RTR SHADER PROGRAM LINKING LOG:" + szInfoLog);
				uninitialize();
				System.exit(0);
			}
		}

		//Get uniform location
		mvpUniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_mvp_matrix");
		stripsUniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_no_of_strips");
		segmentsUniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_no_of_segments");
		lineColorUniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_lineColor");

		// Triangle position vertices array, as it is inline initialization, no need of specifying size of array on rhs 
		final float[] vertices = new float[]
		{
			-1.0f, -1.0f, -0.5f, 1.0f, 0.5f, -1.0f, 1.0f, 1.0f
		};

		ByteBuffer byteBuffer;
		FloatBuffer floatBuffer;

		//create and bind with vao
		GLES32.glGenVertexArrays(1, vao, 0);
		GLES32.glBindVertexArray(vao[0]);

		//Create and bind with vbo
		GLES32.glGenBuffers(1, vbo, 0);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vbo[0]);

		//Allocate the buffer directly from native memory
		byteBuffer = ByteBuffer.allocateDirect(vertices.length*4); //4 is size of float in java
		byteBuffer.order(ByteOrder.nativeOrder());
		floatBuffer = byteBuffer.asFloatBuffer();
		floatBuffer.put(vertices);
		floatBuffer.position(0);

		//data to the buffer
		GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER, vertices.length * 4, floatBuffer, GLES32.GL_STATIC_DRAW);
		GLES32.glVertexAttribPointer(GLESMacros.PS_ATTRIBUTE_POSITION, 
										2,				// break data after x y z, i.e. 3
										GLES32.GL_FLOAT, // type of data
										false, 			// normalize?
										0, 				//stride?-no
										0 );			//offset?-no(windows : NULL, in java null is not #define 0) therefore 0
		GLES32.glEnableVertexAttribArray(GLESMacros.PS_ATTRIBUTE_POSITION);
		
		//bind to buffer
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0);
		GLES32.glBindVertexArray(0);

		GLES32.glClearColor(0.0f,0.0f,0.0f,1.0f);
		GLES32.glEnable(GLES32.GL_DEPTH_TEST);
		GLES32.glDepthFunc(GLES32.GL_LEQUAL);

		Matrix.setIdentityM(perspectiveProjectionMatrix,0);
		//no warmup call to resize as we are already in fullscreen. 
		//we dont run multiple app with screen sharing in android unlike windows.
    }

    private void resize(int width, int height)
    {
        if(height<0)
        {
            height =1;
        }
		GLES32.glViewport(0,0,width, height);
		Matrix.perspectiveM(perspectiveProjectionMatrix, 0, 45.0f, (float)width/ (float)height, 0.1f, 100.0f);
    }

	private void display()
	{
		//variable declarations
		float[] modelViewMatrix = new float[16];
		float[] modelViewProjectionMatrix = new float[16];

		//code
		GLES32.glClear(GLES32.GL_COLOR_BUFFER_BIT|GLES32.GL_DEPTH_BUFFER_BIT);
		GLES32.glUseProgram(shaderProgramObject);

		//making martices identity
		Matrix.setIdentityM(modelViewMatrix, 0);
		Matrix.setIdentityM(modelViewProjectionMatrix, 0);

		//matrix transformation
		Matrix.translateM(modelViewMatrix, 0, 0.5f, 0.5f, -6.0f);
		Matrix.multiplyMM(modelViewProjectionMatrix, 0,
						  perspectiveProjectionMatrix, 0,
						  modelViewMatrix, 0);


		//send matrix to shader
		GLES32.glUniformMatrix4fv(mvpUniform, 1, false, modelViewProjectionMatrix, 0);
		GLES32.glUniform1i(stripsUniform, 1);
		GLES32.glUniform1i(segmentsUniform, numberOfLineSegments);
		if((isTessLevelIncrementing == true) && (numberOfLineSegments != 10))		// color yellow
		{
			GLES32.glLineWidth(5.0f);
			float[] color = new float[] {1.0f, 1.0f, 0.0f, 1.0f};
			GLES32.glUniform4fv(lineColorUniform, 1, color, 0);
		}
		else if((isTessLevelIncrementing == true) && (numberOfLineSegments == 10))		// color red
		{
			GLES32.glLineWidth(10.0f);
			float[] color = new float[] {1.0f, 0.0f, 0.0f, 1.0f};
			GLES32.glUniform4fv(lineColorUniform, 1, color, 0);
		}
		else if ((isTessLevelIncrementing == false) && (numberOfLineSegments == 1))		// color red
		{
			GLES32.glLineWidth(10.0f);
			float[] color = new float[] {1.0f, 0.0f, 0.0f, 1.0f};
			GLES32.glUniform4fv(lineColorUniform, 1, color, 0);
		}
		else if ((isTessLevelIncrementing == false) && (numberOfLineSegments != 1))		// color blue
		{
			GLES32.glLineWidth(5.0f);
			float[] color = new float[] {0.0f, 0.0f, 1.0f, 1.0f};
			GLES32.glUniform4fv(lineColorUniform, 1, color, 0);
		}
		//bind with vao
		GLES32.glBindVertexArray(vao[0]);
		GLES32.glPatchParameteri(GLES32.GL_PATCH_VERTICES, 4);
		GLES32.glDrawArrays(GLES32.GL_PATCHES, 0, 4);
		//unbind with vao
		GLES32.glBindVertexArray(0);

		//unuse program
		GLES32.glUseProgram(0);
        requestRender();
    }

}
