package com.rtr.win_println;

//added by me
import androidx.appcompat.widget.AppCompatTextView;		//this is for AppCompatTextView
import android.content.Context;							//this is for context
import android.graphics.Color;							//this is for color
import android.view.Gravity;							//this is for Gravity
import android.view.MotionEvent;						// for MotionEvent
import android.view.GestureDetector;					// for GestureDetector
import android.view.GestureDetector.OnGestureListener;	// for OnGestureListener
import android.view.GestureDetector.OnDoubleTapListener;//for OnDoubleTapListener

public class MyView extends AppCompatTextView implements OnGestureListener,OnDoubleTapListener
{
	private GestureDetector gestureDetector;	

	public MyView(Context context)
	{
		super(context);
		setTextColor(Color.rgb(255,128,0));
		setTextSize(60);
		setGravity(Gravity.CENTER);
		setText("Hello World !!!");
		gestureDetector = new GestureDetector(context, this, null, false);	//this means handler who is going to handler
		gestureDetector.setOnDoubleTapListener(this);	//this means handler who is going to handle
	}

	//handling 'onTouchEvent' is the most important,
	//because it triggers all gesture and tap events
	@Override
	public boolean onTouchEvent(MotionEvent event)
	{
		//code
		int eventaction = event.getAction();
		if(!gestureDetector.onTouchEvent(event))
			super.onTouchEvent(event);
		return(true);
	}

	//abstract method from OnDoubleTapListener so must be implemented
	@Override
	public boolean onDoubleTap(MotionEvent e)
	{
		setText("Double Tap");
		return(true);
	}

	//abstract method from OnDoubleTapListener so must be implemented
	@Override
	public boolean onDoubleTapEvent(MotionEvent e)
	{
		//do not write any code here because already written in 'onDoubleTap'
		return(true);
	}

	//abstract  method from OnDoubleTapListener so must be implemented
	@Override
	public boolean onSingleTapConfirmed(MotionEvent e)
	{
		setText("Single Tap");
		return(true);
	}

	//abstract  method from OnGestureListener so must be implemented
	@Override
	public boolean onDown(MotionEvent e)
	{
		//do not write any code here because already written in 'onSingleTapConfirmed'
		return(true);
	}

	//abstract  method from OnGestureListener so must be implemented
	@Override
	public boolean onFling(MotionEvent e1,MotionEvent e2, float velocityX, float velocityY)
	{
		return(true);
	}

	//abstract  method from OnGestureListener so must be implemented
	@Override
	public void onLongPress(MotionEvent e)
	{
		setText("Long Press");
	}

	//abstract  method from OnGestureListener so must be implemented
	@Override
	public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY)
	{
		System.exit(0);
		return(true);
	}

	//abstract  method from OnGestureListener so must be implemented
	@Override
	public void onShowPress(MotionEvent e)
	{
	
	}

	//abstract  method from OnGestureListener so must be implemented
	@Override
	public boolean onSingleTapUp(MotionEvent e)
	{
		return(true);
	}
}
