package com.rtr.win_println;

//default given packages
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
//packages added by me
import android.view.Window;
import android.view.WindowManager;
import android.content.pm.ActivityInfo;
import android.graphics.Color;
import android.view.View;

public class MainActivity extends AppCompatActivity {
	
	 private MyView myView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
		System.out.println("RTR: I am in OnCreate");

        //setContentView(R.layout.activity_main);
		//get rid of title bar
		this.supportRequestWindowFeature(Window.FEATURE_NO_TITLE);

		// make fullscreen
		this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

		//get rid of navigation bar on launch and on event occurance
		this.getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_HIDE_NAVIGATION | View.SYSTEM_UI_FLAG_IMMERSIVE);

		//forced landscape orientation
		this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);

		//set background color
		this.getWindow().getDecorView().setBackgroundColor(Color.BLACK);

		//define our own view
		myView = new MyView(this);

		//now set this view as our default view
		setContentView(myView);
    }

	@Override
	protected void onPause(){
		super.onPause();
	}

	@Override
	protected void onResume(){
		super.onResume();
	}
}
