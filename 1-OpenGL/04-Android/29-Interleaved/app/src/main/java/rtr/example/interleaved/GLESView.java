package rtr.example.interleaved;

//packages added by me
import android.content.Context;
import android.graphics.Color;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.GestureDetector;
import android.view.GestureDetector.OnGestureListener;
import android.view.GestureDetector.OnDoubleTapListener;
import rtr.example.interleaved.GLESMacros;

import android.opengl.GLSurfaceView;
import android.opengl.GLES32;

import javax.microedition.khronos.opengles.GL10;
import javax.microedition.khronos.egl.EGLConfig;

//for opengl buffers
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;

//for texture
import android.graphics.BitmapFactory;
import android.graphics.Bitmap;
import android.opengl.GLUtils;

//for matrix mathematics
import android.opengl.Matrix;

public class GLESView extends GLSurfaceView implements GLSurfaceView.Renderer, OnGestureListener, OnDoubleTapListener{
    private GestureDetector gestureDetector;
	private final Context context;
	
	private int vertexShaderObject;
	private int fragmentShaderObject;
	private int shaderProgramObject;

	private int mUniform;
	private int vUniform;
	private int pUniform;
	private int LaUniform, LdUniform, LsUniform;
	private int KaUniform, KdUniform, KsUniform;
	private int materialShininessUniform;
	private int lightPositionUniform;
	private int keyPressedUniform;
	private int samplerUniform;

	private int[] texture_marble = new int[1];

	private int[] vao_cube 	= new int[1];
	private int[] vbo_cube 	= new int[1];

	private float[] perspectiveProjectionMatrix = new float[16];		//4x4 matrix
	private float[] lightAmbient 	= new float[] { 0.25f, 0.25f, 0.25f, 0.25f };
	private float[] lightDiffuse 	= new float[] { 1.0f, 1.0f, 1.0f, 1.0f };
	private float[] lightSpecular	= new float[] { 1.0f, 1.0f, 1.0f, 1.0f };
	private float[] lightPosition 	= new float[] { 10.0f, 10.0f, 10.0f, 1.0f };

	private float[] materialAmbient = new float[] { 0.25f, 0.25f, 0.25f, 0.0f };
	private float[] materialDiffuse = new float[] { 1.0f, 1.0f, 1.0f, 1.0f };
	private float[] materialSpecular= new float[] { 1.0f, 1.0f, 1.0f, 1.0f };
	private float materialShininess = 128.0f;
	
	private float cube_angle 	= 0.0f;
	private int checkAnimationCount = 0;
	private int checkLightingCount = 0;

    private boolean enableLighting 	= false;
	private boolean enableAnimation = false;

	//constructor
    public GLESView(Context drawingContext){
        super(drawingContext);
        context = drawingContext;
        
        setEGLContextClientVersion(3); 
        setRenderer(this);
        setRenderMode(GLSurfaceView.RENDERMODE_WHEN_DIRTY);

        gestureDetector =new GestureDetector(drawingContext, this, null, false );
        gestureDetector.setOnDoubleTapListener(this);
    }

    //handling 'onTouchEvent' is the most important,
	//because it triggers all gesture and tap events
	@Override
	public boolean onTouchEvent(MotionEvent event)
	{
		//code
		int eventaction = event.getAction();
		if(!gestureDetector.onTouchEvent(event))
			super.onTouchEvent(event);
		return(true);
	}

    //abstract method from OnDoubleTapListener so must be implemented
	@Override
	public boolean onDoubleTap(MotionEvent e)
	{
		return(true);
    }
    
    //abstract method from OnDoubleTapListener so must be implemented
	@Override
	public boolean onDoubleTapEvent(MotionEvent e)
	{
		//do not write any code here because already written in 'onDoubleTap'
		checkLightingCount++;
		if((checkLightingCount%2)==0)
			enableLighting = false;
		else
			enableLighting = true;
		return(true);
    }
    
    //abstract  method from OnDoubleTapListener so must be implemented
	@Override
	public boolean onSingleTapConfirmed(MotionEvent e)
	{
	//	setText("Single Tap");
		checkAnimationCount++;
		if((checkAnimationCount%2)==0)
			enableAnimation = false;
		else
			enableAnimation = true;

		return(true);
    }
    
    //abstract  method from OnGestureListener so must be implemented
	@Override
	public boolean onDown(MotionEvent e)
	{
		//do not write any code here because already written in 'onSingleTapConfirmed'
		return(true);
    }
    
    //abstract  method from OnGestureListener so must be implemented
	@Override
	public boolean onFling(MotionEvent e1,MotionEvent e2, float velocityX, float velocityY)
	{
		return(true);
	}

	//abstract  method from OnGestureListener so must be implemented
	@Override
	public void onLongPress(MotionEvent e)
	{
		//setText("Long Press");
	}

	//abstract  method from OnGestureListener so must be implemented
	@Override
	public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY)
	{
		//setText("Scroll");
		uninitialize();
		System.exit(0);
		return(true);
	}

	//abstract  method from OnGestureListener so must be implemented
	@Override
	public void onShowPress(MotionEvent e)
	{
	
	}

	//abstract  method from OnGestureListener so must be implemented
	@Override
	public boolean onSingleTapUp(MotionEvent e)
	{
		return(true);
    }
    
    //implement GLSurfaceView.Renderer methods
    @Override
    public void onSurfaceCreated(GL10 gl, EGLConfig config){
		System.out.println("RTR: Called onSurfaceCreated");

        String version = gl.glGetString(GL10.GL_VERSION);
		System.out.println("RTR:" + version);
		
		version = gl.glGetString(GLES32.GL_SHADING_LANGUAGE_VERSION);
		System.out.println("RTR:" + version);
        initialize();
    }

    @Override
    public void onSurfaceChanged(GL10 unused, int width, int height)
    {
		System.out.println("RTR: Called onSurfaceChanged");

        resize(width, height);
    }

    @Override
    public void onDrawFrame(GL10 unused){
        display();
    }

	//our custom methods
	private void uninitialize()
	{
		System.out.println("RTR: Called uninitialize");
		
		if(shaderProgramObject != 0)
		{
			int[] shaderCount = new int[1];
			int shaderNumber;

			GLES32.glUseProgram(shaderProgramObject);

			GLES32.glGetProgramiv(shaderProgramObject, GLES32.GL_ATTACHED_SHADERS, shaderCount, 0);
			System.out.println("RTR: uninitialize => " + shaderCount[0]);
		
			int[] shaders =new int[shaderCount[0]];

			if(shaderCount[0] != 0)
			{
				GLES32.glGetAttachedShaders(shaderProgramObject, shaderCount[0], shaderCount, 0, shaders, 0);
				System.out.println("RTR: uninitialize => if shadercount != 0 ::" + shaderCount[0]);

				for(shaderNumber =0;shaderNumber<shaderCount[0]; shaderNumber++)
				{
					GLES32.glDetachShader(shaderProgramObject, shaders[shaderNumber]);
					GLES32.glDeleteShader(shaders[shaderNumber]);
					shaders[shaderNumber]=0;
				}
			}
			else{
				System.out.println("RTR: uninitialize => shader count is 0");
			}
			GLES32.glDeleteProgram(shaderProgramObject);
			shaderProgramObject = 0;
			GLES32.glUseProgram(0);
		}
		if(vbo_cube[0] != 0)
		{
			System.out.println("RTR: uninitialize => deleting vbo_cube");

			GLES32.glDeleteBuffers(1, vbo_cube, 0);
			vbo_cube[0] = 0;
		}
		if(vao_cube[0] != 0)
		{
			System.out.println("RTR: uninitialize => deleting vao_cube");

			GLES32.glDeleteVertexArrays(1, vao_cube, 0);
			vao_cube[0]=0;
		}
	}

    private void initialize()
	{
		System.out.println("RTR: Called initialize");

		vertexShaderObject = GLES32.glCreateShader(GLES32.GL_VERTEX_SHADER);
		final String vertexShaderSourceCode = String.format
		(
		"	#version 320 es																		" +
		"	\n																					" +
		"	in vec4 vPosition;																	" +
		"	in vec3 vNormal;																	" +
		"	in vec4 vColor;																		" +
		"	in vec2 vTexCoord;																	" +
		"																					  \n" +
		"	out vec4 out_color;																	" +
		"	out vec2 out_tex;																	" +
		"	out vec4 eyeCoordinates;															" +
		"	out vec3 tNorm;																		" +
		"	out vec3 lightDirection;															" +
		"	out vec3 viewerVector;																" +
		"																					  \n" +
		"	uniform mat4 u_m_matrix;															" +
		"	uniform mat4 u_v_matrix;															" +
		"	uniform mat4 u_p_matrix;															" +
		"	uniform mediump int u_l_key_is_pressed;												" +
		"	uniform vec4 u_light_position;														" +
		"																					  \n" +
		"	void main(void)																		" +
		"	{																					" +
		"		if(u_l_key_is_pressed == 1)														" +
		"		{																				" +
		"			eyeCoordinates = u_v_matrix * u_m_matrix * vPosition;						" +
		"			tNorm = mat3(u_v_matrix * u_m_matrix) * vNormal;							" +
		"			lightDirection = vec3(u_light_position - eyeCoordinates);					" +
		"			viewerVector = vec3(-eyeCoordinates);										" +
		"		}																				" +
		"																					  \n" +
		"	gl_Position = u_v_matrix * u_p_matrix * u_m_matrix * vPosition;						" +
		"	out_tex = vTexCoord;															  \n" +
		"	out_color = vColor;																  \n" +
		"	}																					"
		);

		GLES32.glShaderSource(vertexShaderObject, vertexShaderSourceCode);
		GLES32.glCompileShader(vertexShaderObject);
		int[] iShaderCompileStatus = new int[1];
		int[] iInfoLogLength = new int[1];
		String szInfoLog = null;

		GLES32.glGetShaderiv(vertexShaderObject, GLES32.GL_COMPILE_STATUS, iShaderCompileStatus, 0);

		if(iShaderCompileStatus[0] == GLES32.GL_FALSE)
		{
			GLES32.glGetShaderiv(vertexShaderObject, GLES32.GL_INFO_LOG_LENGTH, iInfoLogLength, 0);
			if(iInfoLogLength[0] > 0)
			{
				szInfoLog = GLES32.glGetShaderInfoLog(vertexShaderObject);
				System.out.println("RTR: VERTEX SHADER COMPILATION LOG:" + szInfoLog);
				uninitialize();
				System.exit(0);
			}
		}

		//fragment shader
		fragmentShaderObject = GLES32.glCreateShader(GLES32.GL_FRAGMENT_SHADER);

		final String fragmentShaderSourceCode = String.format
		(
		"	#version 320 es																					" +
		"	\n																								" +
		"	precision highp float;																				" +
		"	precision mediump int;																				" +
		"	\n																								" +
		"	in vec3 tNorm;																						" +
		"	in vec3 lightDirection;																				" +
		"	in vec3 viewerVector;																				" +
		"	in vec2 out_tex;																	  			\n	" +
		"	in vec4 out_color;  																  			\n	" +
		"																						  			\n	" +
		"	out vec4 fragColor;																					" +
		"																						  			\n	" +
		"	uniform vec3 u_ld;																					" +
		"	uniform vec3 u_kd;																					" +
		"	uniform vec3 u_la;																					" +
		"	uniform vec3 u_ka;																					" +
		"	uniform vec3 u_ls;																					" +
		"	uniform vec3 u_ks;																					" +
		"	uniform float u_material_shininess;																	" +
		"	uniform int u_l_key_is_pressed;																		" +
		"	uniform sampler2D u_sampler;														  			\n	" +
		"																						  			\n	" +
		"	void main(void)																						" +
		"	{																									" +
		"		vec3 phong_ads_light;																			" +
		"		vec4 texture_color;																   			\n	" +
		"																						   			\n	" +
		"		if(u_l_key_is_pressed == 1)																		" +
		"		{																								" +
		"			vec3 normalizedtNorm = normalize(tNorm);													" +
		"			vec3 normalizedlightDirection = normalize(lightDirection);									" +
		"			vec3 normalizedviewerVector = normalize(viewerVector);										" +
		"																						  			\n	" +
		"			vec3 reflectionVector = normalize(reflect(-normalizedlightDirection, normalizedtNorm));		" +
		"			float tnDotld = max(dot(normalizedlightDirection, normalizedtNorm), 0.0);					" +
		"			float rvDotvv = max(dot(reflectionVector, normalizedviewerVector), 0.0);					" +
		"																						  			\n	" +
		"			vec3 ambient = u_la * u_ka;																	" +
		"			vec3 diffuse = u_ld * u_kd * tnDotld;														" +
		"			vec3 specular = u_ls * u_ks * pow(rvDotvv, u_material_shininess);							" +
		"			phong_ads_light = ambient + diffuse + specular;												" +
		"																						  			\n	" +
		"		}																								" +
		"		else																							" +
		"		{																								" +
		"			phong_ads_light = vec3(1.0, 1.0, 1.0);														" +
		"		}																								" +
		"	texture_color = texture(u_sampler, out_tex);										  			\n	" +
		"																						  			\n	" +
		"	fragColor = texture_color * vec4(( vec3(out_color) *phong_ads_light), 1.0);			  			\n	" +
		"	}																									"
		);

		GLES32.glShaderSource(fragmentShaderObject, fragmentShaderSourceCode);
		GLES32.glCompileShader(fragmentShaderObject);
		iShaderCompileStatus[0] = 0;
		iInfoLogLength[0] = 0;
		szInfoLog = null;

		GLES32.glGetShaderiv(fragmentShaderObject, GLES32.GL_COMPILE_STATUS, iShaderCompileStatus, 0);
		
		if(iShaderCompileStatus[0] == GLES32.GL_FALSE)
		{
			GLES32.glGetShaderiv(fragmentShaderObject, GLES32.GL_INFO_LOG_LENGTH, iInfoLogLength, 0);

			if(iInfoLogLength[0] > 0)
			{
				szInfoLog = GLES32.glGetShaderInfoLog(fragmentShaderObject);
				System.out.println("RTR: FRAGMENT SHADER COMPILATION LOG:" + szInfoLog);
				uninitialize();
				System.exit(0);
			}
		}

		//shader program
		shaderProgramObject = GLES32.glCreateProgram();

		GLES32.glAttachShader(shaderProgramObject, vertexShaderObject);
		GLES32.glAttachShader(shaderProgramObject, fragmentShaderObject);

		//prelinking binding to attributes
		GLES32.glBindAttribLocation(shaderProgramObject, GLESMacros.AMC_ATTRIBUTE_POSITION, "vPosition" );
		GLES32.glBindAttribLocation(shaderProgramObject, GLESMacros.AMC_ATTRIBUTE_COLOR, "vColor" );
		GLES32.glBindAttribLocation(shaderProgramObject, GLESMacros.AMC_ATTRIBUTE_NORMAL, "vNormal" );
		GLES32.glBindAttribLocation(shaderProgramObject, GLESMacros.AMC_ATTRIBUTE_TEXCOORD0, "vTexcoord");
		
		//link
		GLES32.glLinkProgram(shaderProgramObject);
		
		//error checking
		int[] iProgramLinkStatus = new int[1];

		iInfoLogLength[0] = 0;
		szInfoLog = null;

		GLES32.glGetProgramiv(shaderProgramObject, GLES32.GL_LINK_STATUS, iProgramLinkStatus, 0);

		if(iProgramLinkStatus[0] == GLES32.GL_FALSE)
		{
			System.out.println("RTR SHADER PROGRAM LINKING FAILED");

			GLES32.glGetProgramiv(shaderProgramObject, GLES32.GL_INFO_LOG_LENGTH, iInfoLogLength, 0);
			if(iInfoLogLength[0] > 0)
			{
				szInfoLog = GLES32.glGetShaderInfoLog(shaderProgramObject);
				System.out.println("RTR SHADER PROGRAM LINKING LOG:" + szInfoLog);
				uninitialize();
				System.exit(0);
			}
		}

		//Get uniform location
		vUniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_v_matrix");
		mUniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_m_matrix");
		pUniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_p_matrix");
		LdUniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_ld");
		KdUniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_kd");
		LaUniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_la");
		KaUniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_ka");
		LsUniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_ls");
		KsUniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_ks");
		materialShininessUniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_material_shininess");
		lightPositionUniform 	 = GLES32.glGetUniformLocation(shaderProgramObject, "u_light_position");
		keyPressedUniform 		 = GLES32.glGetUniformLocation(shaderProgramObject, "u_l_key_is_pressed");
		samplerUniform 			 = GLES32.glGetUniformLocation(shaderProgramObject, "u_sampler");

		//pyramid position vertices
		final float[] cube_vcnt = new float[]
		{
		 	 1.0f,  1.0f,  1.0f, 	1.0f, 0.0f, 0.0f,	 0.0f,  0.0f,  1.0f, 	1.0f, 1.0f, 
			-1.0f,  1.0f,  1.0f, 	1.0f, 0.0f, 0.0f,	 0.0f,  0.0f,  1.0f,	0.0f, 1.0f,
			-1.0f, -1.0f,  1.0f, 	1.0f, 0.0f, 0.0f,	 0.0f,  0.0f,  1.0f,	0.0f, 0.0f,
			 1.0f, -1.0f,  1.0f, 	1.0f, 0.0f, 0.0f,	 0.0f,  0.0f,  1.0f,	1.0f, 0.0f,
			 1.0f,  1.0f, -1.0f, 	0.0f, 1.0f, 0.0f,	 1.0f,  0.0f,  0.0f,	1.0f, 0.0f, 
			 1.0f,  1.0f,  1.0f, 	0.0f, 1.0f, 0.0f,	 1.0f,  0.0f,  0.0f,	1.0f, 1.0f,
			 1.0f, -1.0f,  1.0f, 	0.0f, 1.0f, 0.0f,	 1.0f,  0.0f,  0.0f,	0.0f, 1.0f,
			 1.0f, -1.0f, -1.0f, 	0.0f, 1.0f, 0.0f,	 1.0f,  0.0f,  0.0f,	0.0f, 0.0f,
			 1.0f,  1.0f, -1.0f, 	0.0f, 0.0f, 1.0f,	 0.0f,  0.0f, -1.0f, 	0.0f, 0.0f, 
			-1.0f,  1.0f, -1.0f, 	0.0f, 0.0f, 1.0f,	 0.0f,  0.0f, -1.0f,	0.0f, 1.0f,
			-1.0f, -1.0f, -1.0f, 	0.0f, 0.0f, 1.0f,	 0.0f,  0.0f, -1.0f,	1.0f, 1.0f,
			 1.0f, -1.0f, -1.0f, 	0.0f, 0.0f, 1.0f,	 0.0f,  0.0f, -1.0f,	1.0f, 0.0f,
			-1.0f,  1.0f, -1.0f, 	1.0f, 0.0f, 1.0f,	-1.0f,  0.0f,  0.0f,	1.0f, 0.0f, 
			-1.0f,  1.0f,  1.0f, 	1.0f, 0.0f, 1.0f,	-1.0f,  0.0f,  0.0f,	0.0f, 0.0f,
			-1.0f, -1.0f,  1.0f, 	1.0f, 0.0f, 1.0f,	-1.0f,  0.0f,  0.0f,	0.0f, 1.0f,
			-1.0f, -1.0f, -1.0f, 	1.0f, 0.0f, 1.0f,	-1.0f,  0.0f,  0.0f,	1.0f, 1.0f,
			 1.0f,  1.0f, -1.0f, 	1.0f, 1.0f, 0.0f,	 0.0f,  1.0f,  0.0f, 	0.0f, 1.0f, 
			-1.0f,  1.0f, -1.0f, 	1.0f, 1.0f, 0.0f,	 0.0f,  1.0f,  0.0f,	0.0f, 0.0f,
			-1.0f,  1.0f,  1.0f, 	1.0f, 1.0f, 0.0f,	 0.0f,  1.0f,  0.0f,	1.0f, 0.0f,
			 1.0f,  1.0f,  1.0f, 	1.0f, 1.0f, 0.0f,	 0.0f,  1.0f,  0.0f,	1.0f, 1.0f,
			 1.0f, -1.0f, -1.0f, 	0.0f, 1.0f, 1.0f,	 0.0f, -1.0f,  0.0f,	1.0f, 0.0f, 
			-1.0f, -1.0f, -1.0f, 	0.0f, 1.0f, 1.0f,	 0.0f, -1.0f,  0.0f,	0.0f, 0.0f,
			-1.0f, -1.0f,  1.0f, 	0.0f, 1.0f, 1.0f,	 0.0f, -1.0f,  0.0f,	0.0f, 1.0f,
			 1.0f, -1.0f,  1.0f, 	0.0f, 1.0f, 1.0f,	 0.0f, -1.0f,  0.0f,	1.0f, 1.0f
		};
		

		//************************** CUBE ********************************//
		//create and bind with vao
		GLES32.glGenVertexArrays(1, vao_cube, 0);
		GLES32.glBindVertexArray(vao_cube[0]);

		//Create buffer and bind with vbo
		GLES32.glGenBuffers(1, vbo_cube, 0);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vbo_cube[0]);

		//Allocate the buffer directly from native memory
		ByteBuffer byteBuffer = ByteBuffer.allocateDirect(11 * 24 * 4); //4 is size of float in java
		//Arrange the byte order of buffer to the native byte order
		byteBuffer.order(ByteOrder.nativeOrder());
		//create the float type buffer and convert our byte type buffer into float type buffer
		FloatBuffer floatBuffer = byteBuffer.asFloatBuffer();
		//Now put your array into the "COOKED" buffer
		floatBuffer.put(cube_vcnt);
		//set the array at 0th position of the buffer as our array is not interleaved
		floatBuffer.position(0);

		//data to the buffer 
		GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER, 11 * 24 * 4, floatBuffer, GLES32.GL_STATIC_DRAW);
		
		// attrib pointer for position, color, normal, texcoord
		GLES32.glVertexAttribPointer(GLESMacros.AMC_ATTRIBUTE_POSITION, 
										3,					// break data after x y z, i.e. 3
										GLES32.GL_FLOAT, 	// type of data
										false, 				// normalize?
										11*4, 			//stride?-no
										0);	//offset?-no(windows : NULL, in java null is not #define 0) therefore 0
		GLES32.glEnableVertexAttribArray(GLESMacros.AMC_ATTRIBUTE_POSITION);
		
		GLES32.glVertexAttribPointer(GLESMacros.AMC_ATTRIBUTE_COLOR, 
										3,					// break data after x y z, i.e. 3
										GLES32.GL_FLOAT, 	// type of data
										false, 				// normalize?
										11*4, 			//stride?-no
										3*4);	//offset?-no(windows : NULL, in java null is not #define 0) therefore 0
		GLES32.glEnableVertexAttribArray(GLESMacros.AMC_ATTRIBUTE_COLOR);
		
		GLES32.glVertexAttribPointer(GLESMacros.AMC_ATTRIBUTE_NORMAL, 
										3,					// break data after x y z, i.e. 3
										GLES32.GL_FLOAT, 	// type of data
										false, 				// normalize?
										11*4, 			//stride?-no
										6*4);	//offset?-no(windows : NULL, in java null is not #define 0) therefore 0
		GLES32.glEnableVertexAttribArray(GLESMacros.AMC_ATTRIBUTE_NORMAL);
		
		GLES32.glVertexAttribPointer(GLESMacros.AMC_ATTRIBUTE_TEXCOORD0, 
										2,					// break data after x y z, i.e. 3
										GLES32.GL_FLOAT, 	// type of data
										false, 				// normalize?
										11*4, 			//stride?-no
										9*4);	//offset?-no(windows : NULL, in java null is not #define 0) therefore 0
		GLES32.glEnableVertexAttribArray(GLESMacros.AMC_ATTRIBUTE_TEXCOORD0);
		
		//unbind to buffer
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0);
		//unbind to cube vao
		GLES32.glBindVertexArray(0);

		GLES32.glClearColor(0.0f,0.0f,0.0f,1.0f);
		GLES32.glEnable(GLES32.GL_DEPTH_TEST);
		GLES32.glDepthFunc(GLES32.GL_LEQUAL);

		// load textures		
		texture_marble[0] = loadTexture(R.raw.marble);

		Matrix.setIdentityM(perspectiveProjectionMatrix,0);
		//no warmup call to resize as we are already in fullscreen. 
		//we dont run multiple app with screen sharing in android unlike windows.
    }

    private void resize(int width, int height)
    {
        if(height<0)
        {
            height =1;
        }
		GLES32.glViewport(0,0,width, height);
		Matrix.perspectiveM(perspectiveProjectionMatrix, 0, 45.0f, (float)width/ (float)height, 0.1f, 100.0f);
    }

	private void update()
	{
		if(enableAnimation == true)
		{
			cube_angle = cube_angle + 0.5f;
		}
		if(cube_angle > 360.0f)
		{
			cube_angle = 0.0f;
		}
	}
	private void display()
	{
		
		//variable declarations
		float[] scaleMatrix = new float[16];
		float[] rotationMatrix = new float[16];
		float[] translationMatrix = new float[16];
		float[] modelMatrix = new float[16];
		float[] viewMatrix = new float[16];
		float[] projectionMatrix = new float[16];
		
		ByteBuffer byteBuffer;
		FloatBuffer floatBuffer;

		//code
		GLES32.glClear(GLES32.GL_COLOR_BUFFER_BIT|GLES32.GL_DEPTH_BUFFER_BIT);
		
		GLES32.glUseProgram(shaderProgramObject);

		//making matrices identity
		Matrix.setIdentityM(modelMatrix,0);
		Matrix.setIdentityM(viewMatrix,0);
		Matrix.setIdentityM(projectionMatrix, 0);
		Matrix.setIdentityM(translationMatrix, 0);
		Matrix.setIdentityM(rotationMatrix, 0);
		Matrix.setIdentityM(scaleMatrix, 0);

		//matrix transformation
		Matrix.translateM(translationMatrix, 0, 0.0f, 0.0f, -5.0f);
		Matrix.setRotateM(rotationMatrix, 0, cube_angle, 1.0f, 0.0f, 0.0f);
		Matrix.multiplyMM(modelMatrix, 0,
						  modelMatrix, 0,
						  translationMatrix, 0);
		Matrix.multiplyMM(modelMatrix, 0,
						  modelMatrix, 0,
						  rotationMatrix, 0);
		Matrix.setRotateM(rotationMatrix, 0, cube_angle, 0.0f, 1.0f, 0.0f);
		Matrix.multiplyMM(modelMatrix, 0,
						  modelMatrix, 0,
						  rotationMatrix, 0);
		Matrix.setRotateM(rotationMatrix, 0, cube_angle, 0.0f, 0.0f, 1.0f);
		Matrix.multiplyMM(modelMatrix, 0,
						  modelMatrix, 0,
						  rotationMatrix, 0);

		Matrix.multiplyMM(projectionMatrix, 0,
						  perspectiveProjectionMatrix, 0,
						  projectionMatrix, 0);

		if(enableLighting == true)
		{
			GLES32.glUniform1i(keyPressedUniform, 1);
			GLES32.glUniform3fv(LdUniform, 1, lightDiffuse, 0);
			GLES32.glUniform3fv(LaUniform, 1, lightAmbient, 0);
			GLES32.glUniform3fv(LsUniform, 1, lightSpecular, 0);

			GLES32.glUniform3fv(KdUniform, 1, materialDiffuse, 0);
			GLES32.glUniform3fv(KaUniform, 1, materialAmbient, 0);
			GLES32.glUniform3fv(KsUniform, 1, materialSpecular, 0);

			GLES32.glUniform1f(materialShininessUniform, materialShininess);

			GLES32.glUniform4fv(lightPositionUniform, 1, lightPosition, 0);
		}
		else
		{	
			GLES32.glUniform1i(keyPressedUniform, 0);
		}

		//send necessary matrices to shader in respective uniforms
		GLES32.glUniformMatrix4fv(mUniform, 1, false, modelMatrix, 0);
		GLES32.glUniformMatrix4fv(vUniform, 1, false, viewMatrix, 0);
		GLES32.glUniformMatrix4fv(pUniform, 1, false, projectionMatrix, 0);

		//ABU
		GLES32.glActiveTexture(GLES32.GL_TEXTURE0);
		GLES32.glBindTexture(GLES32.GL_TEXTURE_2D, texture_marble[0]);
		GLES32.glUniform1i(samplerUniform, 0);

		// bind vao
		GLES32.glBindVertexArray(vao_cube[0]);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vbo_cube[0]);

		GLES32.glDrawArrays(GLES32.GL_TRIANGLE_FAN, 0, 4);
		GLES32.glDrawArrays(GLES32.GL_TRIANGLE_FAN, 4, 4);
		GLES32.glDrawArrays(GLES32.GL_TRIANGLE_FAN, 8, 4);
		GLES32.glDrawArrays(GLES32.GL_TRIANGLE_FAN, 12, 4);
		GLES32.glDrawArrays(GLES32.GL_TRIANGLE_FAN, 16, 4);
		GLES32.glDrawArrays(GLES32.GL_TRIANGLE_FAN, 20, 4);

		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0);
		//unbind with vao
		GLES32.glBindVertexArray(0);
		GLES32.glBindTexture(GLES32.GL_TEXTURE_2D, 0);

		//unuse program
		GLES32.glUseProgram(0);
        requestRender();

		update();
	}
	
	private int loadTexture(int imageFileResourceId)
	{
		System.out.println("RTR: In loadTexture");
		int[] texture = new int[1];
		BitmapFactory.Options options = new BitmapFactory.Options();

		options.inScaled = false;

		Bitmap bitmap = BitmapFactory.decodeResource(context.getResources(),imageFileResourceId);
		GLES32.glGenTextures(1, texture, 0);
		GLES32.glBindTexture(GLES32.GL_TEXTURE_2D, texture[0]);
		GLES32.glPixelStorei(GLES32.GL_UNPACK_ALIGNMENT, 1);
		GLES32.glTexParameteri(GLES32.GL_TEXTURE_2D, GLES32.GL_TEXTURE_MAG_FILTER, GLES32.GL_LINEAR);
		GLES32.glTexParameteri(GLES32.GL_TEXTURE_2D, GLES32.GL_TEXTURE_MIN_FILTER, GLES32.GL_LINEAR_MIPMAP_LINEAR);
		GLUtils.texImage2D(GLES32.GL_TEXTURE_2D, //target
			0,		//mipmap levels
			bitmap,	//rest of the parameter of glTexImage2d are present inside bitmap
			0);	//border width
		GLES32.glGenerateMipmap(GLES32.GL_TEXTURE_2D);
		GLES32.glBindTexture(GLES32.GL_TEXTURE_2D, 0);
		return(texture[0]);
	}

}

