package rtr.example.check;

//packages added by me
import android.content.Context;
import android.graphics.Color;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.GestureDetector;
import android.view.GestureDetector.OnGestureListener;
import rtr.example.check.GLESMacros;
import android.view.GestureDetector.OnDoubleTapListener;

import android.opengl.GLSurfaceView;
import android.opengl.GLES32;

import javax.microedition.khronos.opengles.GL10;
import javax.microedition.khronos.egl.EGLConfig;

//for opengl buffers
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;

//for texture
import android.graphics.BitmapFactory;
import android.graphics.Bitmap;
import android.opengl.GLUtils;

//for matrix mathematics
import android.opengl.Matrix;

public class GLESView extends GLSurfaceView implements GLSurfaceView.Renderer, OnGestureListener, OnDoubleTapListener{
    private GestureDetector gestureDetector;
	private final Context context;
	
	private int vertexShaderObject;
	private int fragmentShaderObject;
	private int shaderProgramObject;
	private int mvpUniform;
	private int samplerUniform;
	private int checkImageHeight = 64;
	private int checkImageWidth = 64;

	private int[] texture_smiley = new int[1];
	private int[] vao_square = new int[1];
	private int[] vbo_position_square = new int[1];
	private int[] vbo_texcoord = new int[1];

	private float[] perspectiveProjectionMatrix = new float[16];		//4x4 matrix
	private byte[] checkImageArray = new byte[checkImageHeight * checkImageWidth * 4];

    //constructor
    public GLESView(Context drawingContext){
        super(drawingContext);
        context = drawingContext;
        
        setEGLContextClientVersion(3); 
        setRenderer(this);
        setRenderMode(GLSurfaceView.RENDERMODE_WHEN_DIRTY);

        gestureDetector =new GestureDetector(drawingContext, this, null, false );
        gestureDetector.setOnDoubleTapListener(this);
    }

    //handling 'onTouchEvent' is the most important,
	//because it triggers all gesture and tap events
	@Override
	public boolean onTouchEvent(MotionEvent event)
	{
		//code
		int eventaction = event.getAction();
		if(!gestureDetector.onTouchEvent(event))
			super.onTouchEvent(event);
		return(true);
	}

    //abstract method from OnDoubleTapListener so must be implemented
	@Override
	public boolean onDoubleTap(MotionEvent e)
	{
		//setText("Double Tap");
		return(true);
    }
    
    //abstract method from OnDoubleTapListener so must be implemented
	@Override
	public boolean onDoubleTapEvent(MotionEvent e)
	{
		//do not write any code here because already written in 'onDoubleTap'
		return(true);
    }
    
    //abstract  method from OnDoubleTapListener so must be implemented
	@Override
	public boolean onSingleTapConfirmed(MotionEvent e)
	{
	//	setText("Single Tap");
		return(true);
    }
    
    //abstract  method from OnGestureListener so must be implemented
	@Override
	public boolean onDown(MotionEvent e)
	{
		//do not write any code here because already written in 'onSingleTapConfirmed'
		return(true);
    }
    
    //abstract  method from OnGestureListener so must be implemented
	@Override
	public boolean onFling(MotionEvent e1,MotionEvent e2, float velocityX, float velocityY)
	{
		return(true);
	}

	//abstract  method from OnGestureListener so must be implemented
	@Override
	public void onLongPress(MotionEvent e)
	{
		//setText("Long Press");
	}

	//abstract  method from OnGestureListener so must be implemented
	@Override
	public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY)
	{
		//setText("Scroll");
		uninitialize();
		System.exit(0);
		return(true);
	}

	//abstract  method from OnGestureListener so must be implemented
	@Override
	public void onShowPress(MotionEvent e)
	{
	
	}

	//abstract  method from OnGestureListener so must be implemented
	@Override
	public boolean onSingleTapUp(MotionEvent e)
	{
		return(true);
    }
    
    //implement GLSurfaceView.Renderer methods
    @Override
    public void onSurfaceCreated(GL10 gl, EGLConfig config){
		System.out.println("RTR: Called onSurfaceCreated");

        String version = gl.glGetString(GL10.GL_VERSION);
		System.out.println("RTR:" + version);
		
		version = gl.glGetString(GLES32.GL_SHADING_LANGUAGE_VERSION);
		System.out.println("RTR:" + version);
        initialize();
    }

    @Override
    public void onSurfaceChanged(GL10 unused, int width, int height)
    {
		System.out.println("RTR: Called onSurfaceChanged");

        resize(width, height);
    }

    @Override
    public void onDrawFrame(GL10 unused){
        display();
    }

	//our custom methods
	private void uninitialize()
	{
		System.out.println("RTR: Called uninitialize");
		
		if(shaderProgramObject != 0)
		{
			int[] shaderCount = new int[1];
			int shaderNumber;

			GLES32.glUseProgram(shaderProgramObject);

			GLES32.glGetProgramiv(shaderProgramObject, GLES32.GL_ATTACHED_SHADERS, shaderCount, 0);
			System.out.println("RTR: uninitialize => " + shaderCount[0]);
		
			int[] shaders =new int[shaderCount[0]];

			if(shaderCount[0] != 0)
			{
				GLES32.glGetAttachedShaders(shaderProgramObject, shaderCount[0], shaderCount, 0, shaders, 0);
				System.out.println("RTR: uninitialize => if shadercount != 0 ::" + shaderCount[0]);

				for(shaderNumber =0;shaderNumber<shaderCount[0]; shaderNumber++)
				{
					GLES32.glDetachShader(shaderProgramObject, shaders[shaderNumber]);
					GLES32.glDeleteShader(shaders[shaderNumber]);
					shaders[shaderNumber]=0;
				}
			}
			else{
				System.out.println("RTR: uninitialize => shader count is 0");
			}
			GLES32.glDeleteProgram(shaderProgramObject);
			shaderProgramObject = 0;
			GLES32.glUseProgram(0);
		}
		if(vbo_texcoord[0] != 0)
		{
			System.out.println("RTR: uninitialize => deleting vbo_texcoord");

			GLES32.glDeleteBuffers(1, vbo_texcoord, 0);
			vbo_texcoord[0] = 0;
		}
		if(vbo_position_square[0] != 0)
		{
			System.out.println("RTR: uninitialize => deleting vbo_position_square");

			GLES32.glDeleteBuffers(1, vbo_position_square, 0);
			vbo_position_square[0] = 0;
		}
		if(vao_square[0] != 0)
		{
			System.out.println("RTR: uninitialize => deleting vao_square");

			GLES32.glDeleteVertexArrays(1, vao_square, 0);
			vao_square[0]=0;
		}
	}

    private void initialize()
	{
		System.out.println("RTR: Called initialize");

		vertexShaderObject = GLES32.glCreateShader(GLES32.GL_VERTEX_SHADER);
		final String vertexShaderSourceCode = String.format
		(
			"#version 320 es" +
			"\n" +
			"in vec4 vPosition;" +
			"in vec2 vTexcoord;" +
			"out vec2 out_texcoord;" +
			"uniform mat4 u_mvp_matrix;" +
			"void main(void)" +
			"{" +
			"gl_Position = u_mvp_matrix * vPosition;" +
			"out_texcoord = vTexcoord;" +
			"}"
		);
		GLES32.glShaderSource(vertexShaderObject, vertexShaderSourceCode);
		GLES32.glCompileShader(vertexShaderObject);
		int[] iShaderCompileStatus = new int[1];
		int[] iInfoLogLength = new int[1];
		String szInfoLog = null;

		GLES32.glGetShaderiv(vertexShaderObject, GLES32.GL_COMPILE_STATUS, iShaderCompileStatus, 0);

		if(iShaderCompileStatus[0] == GLES32.GL_FALSE)
		{
			GLES32.glGetShaderiv(vertexShaderObject, GLES32.GL_INFO_LOG_LENGTH, iInfoLogLength, 0);
			if(iInfoLogLength[0] > 0)
			{
				szInfoLog = GLES32.glGetShaderInfoLog(vertexShaderObject);
				System.out.println("RTR: VERTEX SHADER COMPILATION LOG:" + szInfoLog);
				uninitialize();
				System.exit(0);
			}
		}

		//fragment shader
		/*precision :  we don't need to set presion for desktop application. 
		There are 3 hardware unit present in ARM to perform floating point maths calculation; 
		i.e FPU(floating point unit), SIMD(Single Instruction Multiple Data unit), NEON()
		in embedded power and memory is not a luxury. so ypu can not use any precision anywhere.
		eg. low presision and medium precision for int is acceptable but using high precisio for integer calucaltion 
		is very battery consuming which is not acceptable. High precision calucation were completed from NEON
		Vertex shader is by default highp
		int is lowp or mediump
		in fragment shader , user must specify it, as maximum processing happens in fragment shader*/
		fragmentShaderObject = GLES32.glCreateShader(GLES32.GL_FRAGMENT_SHADER);
		final String fragmentShaderSourceCode = String.format
		(
			"#version 320 es" +
			"\n" +
			"precision highp float;" +
			"in vec2 out_texcoord;" +
			"out vec4 FragColor;" +
			"uniform sampler2D u_sampler;" +
			"void main(void)" +
			"{" +
			"FragColor = texture(u_sampler, out_texcoord);" +
			"}"
		);

		GLES32.glShaderSource(fragmentShaderObject, fragmentShaderSourceCode);
		GLES32.glCompileShader(fragmentShaderObject);
		iShaderCompileStatus[0] = 0;
		iInfoLogLength[0] = 0;
		szInfoLog = null;

		GLES32.glGetShaderiv(fragmentShaderObject, GLES32.GL_COMPILE_STATUS, iShaderCompileStatus, 0);
		
		if(iShaderCompileStatus[0] == GLES32.GL_FALSE)
		{
			GLES32.glGetShaderiv(fragmentShaderObject, GLES32.GL_INFO_LOG_LENGTH, iInfoLogLength, 0);

			if(iInfoLogLength[0] > 0)
			{
				szInfoLog = GLES32.glGetShaderInfoLog(fragmentShaderObject);
				System.out.println("RTR: FRAGMENT SHADER COMPILATION LOG:" + szInfoLog);
				uninitialize();
				System.exit(0);
			}
		}

		int[] shaderCount = new int[1];

		//shader program
		shaderProgramObject = GLES32.glCreateProgram();

		GLES32.glGetProgramiv(shaderProgramObject, GLES32.GL_ATTACHED_SHADERS, shaderCount, 0);
		System.out.println("RTR: initialize => BEFORE ATTACHING SHADERS shader count is :: " + shaderCount[0]);

		GLES32.glAttachShader(shaderProgramObject, vertexShaderObject);
		GLES32.glAttachShader(shaderProgramObject, fragmentShaderObject);

		GLES32.glGetProgramiv(shaderProgramObject, GLES32.GL_ATTACHED_SHADERS, shaderCount, 0);
		System.out.println("RTR: initialize => AFTER ATTACHING SHADERS shader count is :: " + shaderCount[0]);

		//prelinking binding to attributes
		GLES32.glBindAttribLocation(shaderProgramObject,GLESMacros.AMC_ATTRIBUTE_POSITION, "vPsition" );
		GLES32.glBindAttribLocation(shaderProgramObject, GLESMacros.AMC_ATTRIBUTE_TEXCOORD0, "vTexcoord");
		
		//link
		GLES32.glLinkProgram(shaderProgramObject);
		
		//error checking
		int[] iProgramLinkStatus = new int[1];

		iInfoLogLength[0] = 0;
		szInfoLog = null;

		GLES32.glGetProgramiv(shaderProgramObject, GLES32.GL_LINK_STATUS, iProgramLinkStatus, 0);

		if(iProgramLinkStatus[0] == GLES32.GL_FALSE)
		{
			GLES32.glGetProgramiv(shaderProgramObject, GLES32.GL_INFO_LOG_LENGTH, iInfoLogLength, 0);
			if(iInfoLogLength[0] > 0)
			{
				szInfoLog = GLES32.glGetShaderInfoLog(shaderProgramObject);
				System.out.println("RTR SHADER PROGRAM LINKING LOG:" + szInfoLog);
				uninitialize();
				System.exit(0);
			}
		}

		//Get uniform location
		mvpUniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_mvp_matrix");
		samplerUniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_sampler");

		
		//square texcoord values
		final float[] squaretexcoord = new float[]
		{
			0.0f, 0.0f,
			0.0f, 1.0f,
			1.0f, 1.0f,
			1.0f, 0.0f
		};

		//*******************************SQUARE************************************//

		//create and bind with vao
		GLES32.glGenVertexArrays(1, vao_square, 0);
		GLES32.glBindVertexArray(vao_square[0]);

		//Create buffer for position and bind with vbo
		GLES32.glGenBuffers(1, vbo_position_square, 0);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vbo_position_square[0]);

		//Allocate the buffer directly from native memory
		//ByteBuffer byteBuffer_ps = ByteBuffer.allocateDirect(squareVertices.length*4); //4 is size of float in java
		//Arrange the byte order of buffer to the native byte order
		//byteBuffer_ps.order(ByteOrder.nativeOrder());
		//create the float type buffer and convert our byte type buffer into float type buffer
		//FloatBuffer squarePositionBuffer = byteBuffer_ps.asFloatBuffer();
		//Now put your array into the "COOKED" buffer
		//squarePositionBuffer.put(squareVertices);
		//set the array at 0th position of the buffer as our array is not interleaved
		//squarePositionBuffer.position(0);

		//data to the buffer
		GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER, 4 * 3 * 4, null ,GLES32.GL_DYNAMIC_DRAW);
		GLES32.glVertexAttribPointer(GLESMacros.AMC_ATTRIBUTE_POSITION, 
										3,				// break data after x y z, i.e. 3
										GLES32.GL_FLOAT, // type of data
										false, 			// normalize?
										0, 				//stride?-no
										0 );			//offset?-no(windows : NULL, in java null is not #define 0) therefore 0
		GLES32.glEnableVertexAttribArray(GLESMacros.AMC_ATTRIBUTE_POSITION);
		
		//unbind to buffer
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0);

		//create buffer or color and bind with vbo
		GLES32.glGenBuffers(1, vbo_texcoord, 0);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vbo_texcoord[0]);

		//allocate the buffer directly from native memory
		ByteBuffer byteBuffer_tc = ByteBuffer.allocateDirect(squaretexcoord.length * 4);
		byteBuffer_tc.order(ByteOrder.nativeOrder());
		FloatBuffer squareTexcoordBuffer = byteBuffer_tc.asFloatBuffer();
		squareTexcoordBuffer.put(squaretexcoord);
		squareTexcoordBuffer.position(0);

		//texcoord data to the buffer
		GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER, squaretexcoord.length * 4, squareTexcoordBuffer, GLES32.GL_STATIC_DRAW);
		GLES32.glVertexAttribPointer(GLESMacros.AMC_ATTRIBUTE_TEXCOORD0,
										2,
										GLES32.GL_FLOAT,
										false,
										0,
										0);
		GLES32.glEnableVertexAttribArray(GLESMacros.AMC_ATTRIBUTE_TEXCOORD0);

		//unbind to texcoord buffer
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER,0);

		//unbind to triangle vao
		GLES32.glBindVertexArray(0);
		GLES32.glClearColor(0.0f,0.0f,0.0f,1.0f);
		GLES32.glEnable(GLES32.GL_DEPTH_TEST);
		GLES32.glDepthFunc(GLES32.GL_LEQUAL);

//		texture_smiley[0] = loadTexture1(R.raw.smiley);
		texture_smiley[0] = loadTexture1();
		Matrix.setIdentityM(perspectiveProjectionMatrix,0);
		//no warmup call to resize as we are already in fullscreen. 
		//we dont run multiple app with screen sharing in android unlike windows.
    }

    private void resize(int width, int height)
    {
        if(height<0)
        {
            height =1;
        }
		GLES32.glViewport(0,0,width, height);
		Matrix.perspectiveM(perspectiveProjectionMatrix, 0, 45.0f, (float)width/ (float)height, 0.1f, 100.0f);
    }

	private void display()
	{
		//variable declarations
		float[] modelViewMatrix = new float[16];
		float[] modelViewProjectionMatrix = new float[16];

		//code
		GLES32.glClear(GLES32.GL_COLOR_BUFFER_BIT|GLES32.GL_DEPTH_BUFFER_BIT);
		GLES32.glUseProgram(shaderProgramObject);

		//********************************QUAD 1 *******************************//
		//square position vertices
		final float[] squareVertices = new float[]
		{
			-2.0f,  -1.0f, 0.0f,
			-2.0f,  1.0f, 0.0f,
			0.0f, 1.0f, 0.0f,
			0.0f, -1.0f, 0.0f
		};

		//making matrices identity
		Matrix.setIdentityM(modelViewMatrix,0);
		Matrix.setIdentityM(modelViewProjectionMatrix, 0);

		//matrix transformation
		Matrix.translateM(modelViewMatrix, 0, -0.5f, 0.0f, -5.0f);
		Matrix.multiplyMM(modelViewProjectionMatrix, 0,
						  perspectiveProjectionMatrix, 0,
						  modelViewMatrix, 0);

		//send matrix to shader
		GLES32.glUniformMatrix4fv(mvpUniform, 1, false, modelViewProjectionMatrix, 0);

		//ABU
		GLES32.glActiveTexture(GLES32.GL_TEXTURE0);
		GLES32.glBindTexture(GLES32.GL_TEXTURE_2D, texture_smiley[0]);
		GLES32.glUniform1i(samplerUniform, 0);
		
		//Allocate the buffer directly from native memory
		ByteBuffer byteBuffer_ps = ByteBuffer.allocateDirect(squareVertices.length*4); //4 is size of float in java
		byteBuffer_ps.order(ByteOrder.nativeOrder());
		FloatBuffer squarePositionBuffer = byteBuffer_ps.asFloatBuffer();
		squarePositionBuffer.put(squareVertices);
		squarePositionBuffer.position(0);
		
		//bind with vao
		GLES32.glBindVertexArray(vao_square[0]);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vbo_position_square[0]);
		GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER, squareVertices.length * 4, squarePositionBuffer, GLES32.GL_DYNAMIC_DRAW);
		GLES32.glDrawArrays(GLES32.GL_TRIANGLE_FAN, 0, 4);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0);
		//unbind with vao
		GLES32.glBindVertexArray(0);

		//*****************************QUAD 2**************************/
		//square position vertices
		final float[] squareVertices2 = new float[]
		{
			1.0f, -1.0f, 0.0f,
			1.0f, 1.0f, 0.0f,
			2.41421f, 1.0f, -1.42421f,
			2.41421f, -1.0f, -1.41421f
		};
		//making matrices identity
		Matrix.setIdentityM(modelViewMatrix,0);
		Matrix.setIdentityM(modelViewProjectionMatrix, 0);

		//matrix transformation
		Matrix.translateM(modelViewMatrix, 0, 0.5f, 0.0f, -5.0f);
		Matrix.multiplyMM(modelViewProjectionMatrix, 0,
						  perspectiveProjectionMatrix, 0,
						  modelViewMatrix, 0);

		//send matrix to shader
		GLES32.glUniformMatrix4fv(mvpUniform, 1, false, modelViewProjectionMatrix, 0);

		//ABU
		GLES32.glActiveTexture(GLES32.GL_TEXTURE0);
		GLES32.glBindTexture(GLES32.GL_TEXTURE_2D, texture_smiley[0]);
		GLES32.glUniform1i(samplerUniform, 0);
		
		//Allocate the buffer directly from native memory
		ByteBuffer byteBuffer_ps2 = ByteBuffer.allocateDirect(squareVertices2.length*4); //4 is size of float in java
		byteBuffer_ps2.order(ByteOrder.nativeOrder());
		FloatBuffer squarePositionBuffer2 = byteBuffer_ps2.asFloatBuffer();
		squarePositionBuffer2.put(squareVertices2);
		squarePositionBuffer2.position(0);
		
		//bind with vao
		GLES32.glBindVertexArray(vao_square[0]);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vbo_position_square[0]);
		GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER, squareVertices2.length * 4, squarePositionBuffer2, GLES32.GL_DYNAMIC_DRAW);
		GLES32.glDrawArrays(GLES32.GL_TRIANGLE_FAN, 0, 4);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0);
		//unbind with vao
		GLES32.glBindVertexArray(0);
		//unuse program
		GLES32.glUseProgram(0);
        requestRender();
	}
	private int loadTexture1()
	{
		int[] texture = new int[1];

		makeCheckImage();

		ByteBuffer byteBuffer = ByteBuffer.allocateDirect(checkImageHeight * checkImageWidth *4); //4 is size of float in java
		byteBuffer.order(ByteOrder.nativeOrder());
		byteBuffer.put(checkImageArray);
		byteBuffer.position(0);

		Bitmap bitmap = Bitmap.createBitmap(checkImageWidth, checkImageHeight, Bitmap.Config.ARGB_8888);
		bitmap.copyPixelsFromBuffer(byteBuffer);

		GLES32.glGenTextures(1, texture, 0);
		GLES32.glBindTexture(GLES32.GL_TEXTURE_2D, texture[0]);
		GLES32.glPixelStorei(GLES32.GL_UNPACK_ALIGNMENT, 1);
		GLES32.glTexParameteri(GLES32.GL_TEXTURE_2D, GLES32.GL_TEXTURE_WRAP_S, GLES32.GL_REPEAT);
		GLES32.glTexParameteri(GLES32.GL_TEXTURE_2D, GLES32.GL_TEXTURE_WRAP_T, GLES32.GL_REPEAT);
		GLES32.glTexParameteri(GLES32.GL_TEXTURE_2D, GLES32.GL_TEXTURE_MAG_FILTER, GLES32.GL_NEAREST);
		GLES32.glTexParameteri(GLES32.GL_TEXTURE_2D, GLES32.GL_TEXTURE_MIN_FILTER, GLES32.GL_NEAREST);

		GLUtils.texImage2D(GLES32.GL_TEXTURE_2D, 	//target
			0,										//mipmap levels
			bitmap,									//rest of the parameter of glTexImage2d are present inside bitmap
			0);										//border width
		
		//GLES32.glBindTexture(GLES32.GL_TEXTURE_2D, 0);
		return(texture[0]);
	}

	private void makeCheckImage()
	{
		int i, j, c;

		for(i =0;i<checkImageHeight;i++)
		{
			for(j =0; j < checkImageWidth;j++)
			{
				c = ( (i & 8) ^ (j & 8) ) * 255;
				checkImageArray[(i*64 + j)*4 + 0] = (byte)c;
				checkImageArray[(i*64 + j)*4 + 1] = (byte)c;
				checkImageArray[(i*64 + j)*4 + 2] = (byte)c;
				checkImageArray[(i*64 + j)*4 + 3] = (byte)255;
			}
		}
	}
}

