package rtr.example.dynamicindia;

//packages added by me
import android.content.Context;
import android.graphics.Color;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.GestureDetector;
import android.view.GestureDetector.OnGestureListener;
import rtr.example.dynamicindia.GLESMacros;
import android.view.GestureDetector.OnDoubleTapListener;

import android.opengl.GLSurfaceView;
import android.opengl.GLES32;
import android.opengl.GLES10;

import javax.microedition.khronos.opengles.GL10;
import javax.microedition.khronos.egl.EGLConfig;

//for opengl buffers
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;

//for matrix mathematics
import android.opengl.Matrix;

public class GLESView extends GLSurfaceView implements GLSurfaceView.Renderer, OnGestureListener, OnDoubleTapListener{
    private GestureDetector gestureDetector;
    private final Context context;

	private int vertexShaderObject;
	private int fragmentShaderObject;
	private int shaderProgramObject;
	private int vertexShaderObject_part2;
	private int fragmentShaderObject_part2;
	private int shaderProgramObject_part2;
	
	// I
	private int[] vao_i1 = new int[1];
	private int[] vbo_color_i1 = new int[1];
	private int[] vbo_position_i1 = new int[1];
	// N
	private int[] vao_n = new int[1];
	private int[] vbo_color_n = new int[1];
	private int[] vbo_position_n = new int[1];
	// D
	private int[] vao_d = new int[1];
	private int[] vbo_color_d = new int[1];
	private int[] vbo_position_d = new int[1];
	// I
	private int[] vao_i2 = new int[1];
	private int[] vbo_color_i2 = new int[1];
	private int[] vbo_position_i2 = new int[1];
	// A
	private int[] vao_a = new int[1];
	private int[] vbo_color_a = new int[1];
	private int[] vbo_position_a = new int[1];
	// Flag
	private int[] vao_flag = new int[1];
	private int[] vbo_color_flag = new int[1];
	private int[] vbo_position_flag = new int[1];
	// Plane
	private int[] vao_plane = new int[1];
	private int[] vbo_position_plane = new int[1];
	private int[] vbo_color_plane = new int[1];
	// Path 1
	private int[] vao_path1 = new int[1];
	private int[] vbo_position_path1 = new int[1];
	// Path 2
	private int[] vao_path2 = new int[1];
	private int[] vbo_position_path2 = new int[1];
	// Path 3
	private int[] vao_path3 = new int[1];
	private int[] vbo_position_path3 = new int[1];

	private int mvpUniform;
	private int alphaUniform;

	private int mvpUniform_part2;
	private int alphaUniform_part2;
	private int colorUniform_r_part2;
	private int colorUniform_g_part2;
	private int colorUniform_b_part2;

	private float trans_i1	= 0.0f;
	private float trans_n	= 0.0f;
	private float trans_d	= 0.0f;
	private float trans_i2	= 0.0f;
	private float trans_a	= 0.0f;
	private float alphaValue = 0.0f;
	private float alphaValue_part2 = 1.0f;
	private float delta = 0.6f;
	private float angle1 = -60.0f;
	private float angle2 = -300.0f;

	private int ip;
	private int pointCount = 0;
	private long anim = 0;

	private float[] path1 	= new float[1503];
	private float[] path2 	= new float[1503];
	private float[] path3 	= new float[1503];
	private float[] saffron = new float[] {.0f, 0.6f, 0.2f};
	private float[] white = new float[] {1.0f, 1.0f, 1.0f};
	private float[] green = new float[] {0.070f, 0.533f, 0.027f};

	private float[] perspectiveProjectionMatrix = new float[16];		//4x4 matrix

	private boolean translateI1	= true;
	private boolean translateN	= false;
	private boolean translateD	= false;
	private boolean translateI2	= false;
	private boolean translateA	= false;
	private boolean translatePlane = false;
	private boolean showFlag	= false;

    //constructor
    public GLESView(Context drawingContext){
        super(drawingContext);
        context = drawingContext;
        
        setEGLContextClientVersion(3);
        setRenderer(this);
        setRenderMode(GLSurfaceView.RENDERMODE_WHEN_DIRTY);

        gestureDetector =new GestureDetector(drawingContext, this, null, false );
        gestureDetector.setOnDoubleTapListener(this);
    }

    //handling 'onTouchEvent' is the most important,
	//because it triggers all gesture and tap events
	@Override
	public boolean onTouchEvent(MotionEvent event)
	{
		//code
		int eventaction = event.getAction();
		if(!gestureDetector.onTouchEvent(event))
			super.onTouchEvent(event);
		return(true);
	}

    //abstract method from OnDoubleTapListener so must be implemented
	@Override
	public boolean onDoubleTap(MotionEvent e)
	{
		//setText("Double Tap");
		return(true);
    }
    
    //abstract method from OnDoubleTapListener so must be implemented
	@Override
	public boolean onDoubleTapEvent(MotionEvent e)
	{
		//do not write any code here because already written in 'onDoubleTap'
		return(true);
    }
    
    //abstract  method from OnDoubleTapListener so must be implemented
	@Override
	public boolean onSingleTapConfirmed(MotionEvent e)
	{
	//	setText("Single Tap");
		return(true);
    }
    
    //abstract  method from OnGestureListener so must be implemented
	@Override
	public boolean onDown(MotionEvent e)
	{
		//do not write any code here because already written in 'onSingleTapConfirmed'
		return(true);
    }
    
    //abstract  method from OnGestureListener so must be implemented
	@Override
	public boolean onFling(MotionEvent e1,MotionEvent e2, float velocityX, float velocityY)
	{
		return(true);
	}

	//abstract  method from OnGestureListener so must be implemented
	@Override
	public void onLongPress(MotionEvent e)
	{
		//setText("Long Press");
	}

	//abstract  method from OnGestureListener so must be implemented
	@Override
	public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY)
	{
		//setText("Scroll");
		uninitialize();
		System.exit(0);
		return(true);
	}

	//abstract  method from OnGestureListener so must be implemented
	@Override
	public void onShowPress(MotionEvent e)
	{
	
	}

	//abstract  method from OnGestureListener so must be implemented
	@Override
	public boolean onSingleTapUp(MotionEvent e)
	{
		return(true);
    }
    
    //implement GLSurfaceView.Renderer methods
    @Override
    public void onSurfaceCreated(GL10 gl, EGLConfig config){
		System.out.println("RTR: Called onSurfaceCreated");

        String version = gl.glGetString(GL10.GL_VERSION);
		System.out.println("RTR:" + version);
		
		version = gl.glGetString(GLES32.GL_SHADING_LANGUAGE_VERSION);
		System.out.println("RTR:" + version);
        initialize();
    }

    @Override
    public void onSurfaceChanged(GL10 unused, int width, int height)
    {
		System.out.println("RTR: Called onSurfaceChabged");

        resize(width, height);
    }

    @Override
    public void onDrawFrame(GL10 unused){
        display();
    }

    //our custom methods
	private void uninitialize()
	{
		System.out.println("RTR: Called uninitialize");
		
		if(shaderProgramObject != 0)
		{
			int[] shaderCount = new int[1];
			int shaderNumber;

			GLES32.glUseProgram(shaderProgramObject);

			GLES32.glGetProgramiv(shaderProgramObject, GLES32.GL_ATTACHED_SHADERS, shaderCount, 0);
			System.out.println("RTR: uninitialize => " + shaderCount[0]);
		
			int[] shaders =new int[shaderCount[0]];

			if(shaderCount[0] != 0)
			{
				GLES32.glGetAttachedShaders(shaderProgramObject, shaderCount[0], shaderCount, 0, shaders, 0);
				System.out.println("RTR: uninitialize => if shadercount != 0 ::" + shaderCount[0]);

				for(shaderNumber =0;shaderNumber<shaderCount[0]; shaderNumber++)
				{
					GLES32.glDetachShader(shaderProgramObject, shaders[shaderNumber]);
					GLES32.glDeleteShader(shaders[shaderNumber]);
					shaders[shaderNumber]=0;
				}
			}
			else{
				System.out.println("RTR: uninitialize => shader count is 0");
			}
			GLES32.glDeleteProgram(shaderProgramObject);
			shaderProgramObject = 0;
			GLES32.glUseProgram(0);
		}

		if(vbo_color_i1[0] != 0)
		{
			System.out.println("RTR: uninitialize => deleting vbo_color_i1");

			GLES32.glDeleteBuffers(1, vbo_color_i1, 0);
			vbo_color_i1[0] = 0;
		}
		if(vbo_position_i1[0] != 0)
		{
			System.out.println("RTR: uninitialize => deleting vbo_position_i1");

			GLES32.glDeleteBuffers(1, vbo_position_i1, 0);
			vbo_position_i1[0] = 0;
		}
		if(vao_i1[0] != 0)
		{
			System.out.println("RTR: uninitialize => deleting vao_i1");

			GLES32.glDeleteVertexArrays(1, vao_i1, 0);
			vao_i1[0]=0;
		}

		if(vbo_color_n[0] != 0)
		{
			System.out.println("RTR: uninitialize => deleting vbo_color_n");

			GLES32.glDeleteBuffers(1, vbo_color_n, 0);
			vbo_color_n[0] = 0;
		}
		if(vbo_position_n[0] != 0)
		{
			System.out.println("RTR: uninitialize => deleting vbo_position_n");

			GLES32.glDeleteBuffers(1, vbo_position_n, 0);
			vbo_position_n[0] = 0;
		}
		if(vao_n[0] != 0)
		{
			System.out.println("RTR: uninitialize => deleting vao_n");

			GLES32.glDeleteVertexArrays(1, vao_n, 0);
			vao_n[0]=0;
		}

		if(vbo_color_d[0] != 0)
		{
			System.out.println("RTR: uninitialize => deleting vbo_color_d");

			GLES32.glDeleteBuffers(1, vbo_color_d, 0);
			vbo_color_d[0] = 0;
		}
		if(vbo_position_d[0] != 0)
		{
			System.out.println("RTR: uninitialize => deleting vbo_position_d");

			GLES32.glDeleteBuffers(1, vbo_position_d, 0);
			vbo_position_d[0] = 0;
		}
		if(vao_d[0] != 0)
		{
			System.out.println("RTR: uninitialize => deleting vao_d");

			GLES32.glDeleteVertexArrays(1, vao_d, 0);
			vao_d[0]=0;
		}

		if(vbo_color_i2[0] != 0)
		{
			System.out.println("RTR: uninitialize => deleting vbo_color_i2");

			GLES32.glDeleteBuffers(1, vbo_color_i2, 0);
			vbo_color_i2[0] = 0;
		}
		if(vbo_position_i2[0] != 0)
		{
			System.out.println("RTR: uninitialize => deleting vbo_position_i2");

			GLES32.glDeleteBuffers(1, vbo_position_i2, 0);
			vbo_position_i2[0] = 0;
		}
		if(vao_i2[0] != 0)
		{
			System.out.println("RTR: uninitialize => deleting vao_i2");

			GLES32.glDeleteVertexArrays(1, vao_i2, 0);
			vao_i2[0]=0;
		}

		if(vbo_color_a[0] != 0)
		{
			System.out.println("RTR: uninitialize => deleting vbo_color_a");

			GLES32.glDeleteBuffers(1, vbo_color_a, 0);
			vbo_color_a[0] = 0;
		}
		if(vbo_position_a[0] != 0)
		{
			System.out.println("RTR: uninitialize => deleting vbo_position_a");

			GLES32.glDeleteBuffers(1, vbo_position_a, 0);
			vbo_position_a[0] = 0;
		}
		if(vao_a[0] != 0)
		{
			System.out.println("RTR: uninitialize => deleting vao_a");

			GLES32.glDeleteVertexArrays(1, vao_a, 0);
			vao_a[0]=0;
		}

		if(vbo_color_flag[0] != 0)
		{
			System.out.println("RTR: uninitialize => deleting vbo_color_flag");

			GLES32.glDeleteBuffers(1, vbo_color_flag, 0);
			vbo_color_flag[0] = 0;
		}
		if(vbo_position_flag[0] != 0)
		{
			System.out.println("RTR: uninitialize => deleting vbo_position_flag");

			GLES32.glDeleteBuffers(1, vbo_position_flag, 0);
			vbo_position_flag[0] = 0;
		}
		if(vao_flag[0] != 0)
		{
			System.out.println("RTR: uninitialize => deleting vao_flag");

			GLES32.glDeleteVertexArrays(1, vao_flag, 0);
			vao_flag[0]=0;
		}
	}

    private void initialize()
	{
		System.out.println("RTR: Called initialize");
	
		// ************************************ Shader for india ******************************

		vertexShaderObject = GLES32.glCreateShader(GLES32.GL_VERTEX_SHADER);
		final String vertexShaderSourceCode = String.format
		(
			"	#version 320 es									" +
			"												\n 	" +
			"	in vec4 vPosition;								" +
			"	in vec3 vColor;									" +
			"												\n 	" +
			"	out vec3 out_color;								" +
			"												\n 	" +
			"	uniform mat4 u_mvp_matrix;						" +
			"												\n 	" +
			"	void main(void)									" +
			"	{												" +
			"		gl_Position = u_mvp_matrix * vPosition;		" +
			"		out_color = vColor;							" +
			"	}												"

		);
		GLES32.glShaderSource(vertexShaderObject, vertexShaderSourceCode);
		GLES32.glCompileShader(vertexShaderObject);
		int[] iShaderCompileStatus = new int[1];
		int[] iInfoLogLength = new int[1];
		String szInfoLog = null;

		GLES32.glGetShaderiv(vertexShaderObject, GLES32.GL_COMPILE_STATUS, iShaderCompileStatus, 0);

		if(iShaderCompileStatus[0] == GLES32.GL_FALSE)
		{
			GLES32.glGetShaderiv(vertexShaderObject, GLES32.GL_INFO_LOG_LENGTH, iInfoLogLength, 0);
			if(iInfoLogLength[0] > 0)
			{
				szInfoLog = GLES32.glGetShaderInfoLog(vertexShaderObject);
				System.out.println("RTR: VERTEX SHADER COMPILATION LOG:" + szInfoLog);
				uninitialize();
				System.exit(0);
			}
		}

		//fragment shader
		fragmentShaderObject = GLES32.glCreateShader(GLES32.GL_FRAGMENT_SHADER);
		
		final String fragmentShaderSourceCode = String.format
		(
			"	#version 320 es										" +
			"													\n 	" +
			"	precision highp float;							\n 	" +
			"	in vec3 out_color;									" +
			"													\n 	" +
			"	out vec4 FragColor;									" +
			"													\n 	" +
			"	uniform float u_alpha_value;					\n 	" +
			"													\n 	" +
			"	void main(void)										" +
			"	{													" +
			"		FragColor = vec4(out_color, u_alpha_value);		" +
			"	}													"
		);

		GLES32.glShaderSource(fragmentShaderObject, fragmentShaderSourceCode);
		GLES32.glCompileShader(fragmentShaderObject);
		iShaderCompileStatus[0] = 0;
		iInfoLogLength[0] = 0;
		szInfoLog = null;

		GLES32.glGetShaderiv(fragmentShaderObject, GLES32.GL_COMPILE_STATUS, iShaderCompileStatus, 0);
		
		if(iShaderCompileStatus[0] == GLES32.GL_FALSE)
		{
			GLES32.glGetShaderiv(fragmentShaderObject, GLES32.GL_INFO_LOG_LENGTH, iInfoLogLength, 0);

			if(iInfoLogLength[0] > 0)
			{
				szInfoLog = GLES32.glGetShaderInfoLog(fragmentShaderObject);
				System.out.println("RTR: FRAGMENT SHADER COMPILATION LOG:" + szInfoLog);
				uninitialize();
				System.exit(0);
			}
		}

		int[] shaderCount = new int[1];

		//shader program
		shaderProgramObject = GLES32.glCreateProgram();

		GLES32.glGetProgramiv(shaderProgramObject, GLES32.GL_ATTACHED_SHADERS, shaderCount, 0);
		System.out.println("RTR: initialize => BEFORE ATTACHING SHADERS shader count is :: " + shaderCount[0]);

		GLES32.glAttachShader(shaderProgramObject, vertexShaderObject);
		GLES32.glAttachShader(shaderProgramObject, fragmentShaderObject);

		GLES32.glGetProgramiv(shaderProgramObject, GLES32.GL_ATTACHED_SHADERS, shaderCount, 0);
		System.out.println("RTR: initialize => AFTER ATTACHING SHADERS shader count is :: " + shaderCount[0]);

		//prelinking binding to attributes
		GLES32.glBindAttribLocation(shaderProgramObject,GLESMacros.PS_ATTRIBUTE_POSITION, "vPosition" );
		GLES32.glBindAttribLocation(shaderProgramObject,GLESMacros.PS_ATTRIBUTE_COLOR, "vColor" );

		//link
		GLES32.glLinkProgram(shaderProgramObject);
		//error checking
		int[] iProgramLinkStatus = new int[1];

		iInfoLogLength[0] = 0;
		szInfoLog = null;

		GLES32.glGetProgramiv(shaderProgramObject, GLES32.GL_LINK_STATUS, iProgramLinkStatus, 0);

		if(iProgramLinkStatus[0] == GLES32.GL_FALSE)
		{
			GLES32.glGetProgramiv(shaderProgramObject, GLES32.GL_INFO_LOG_LENGTH, iInfoLogLength, 0);
			if(iInfoLogLength[0] > 0)
			{
				szInfoLog = GLES32.glGetShaderInfoLog(shaderProgramObject);
				System.out.println("RTR SHADER PROGRAM LINKING LOG:" + szInfoLog);
				uninitialize();
				System.exit(0);
			}
		}

		//Get uniform location
		mvpUniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_mvp_matrix");
		alphaUniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_alpha_value");
		// ***************************************************************************************

		// ************************************ Shader for part 2 ******************************
		vertexShaderObject_part2 = GLES32.glCreateShader(GLES32.GL_VERTEX_SHADER);
		final String vertexShaderSourceCode_part2 = String.format
		(
			"	#version 320 es									" +
			"												\n 	" +
			"	in vec4 vPosition;								" +
			"												\n 	" +
			"	uniform mat4 u_mvp_matrix;						" +
			"												\n 	" +
			"	void main(void)									" +
			"	{												" +
			"		gl_Position = u_mvp_matrix * vPosition;		" +
			"	}												"

		);
		
		GLES32.glShaderSource(vertexShaderObject_part2, vertexShaderSourceCode_part2);
		GLES32.glCompileShader(vertexShaderObject_part2);
		
		iShaderCompileStatus[0] = 0;
		iInfoLogLength[0] = 0;
		szInfoLog = null;

		GLES32.glGetShaderiv(vertexShaderObject_part2, GLES32.GL_COMPILE_STATUS, iShaderCompileStatus, 0);

		if(iShaderCompileStatus[0] == GLES32.GL_FALSE)
		{
			GLES32.glGetShaderiv(vertexShaderObject_part2, GLES32.GL_INFO_LOG_LENGTH, iInfoLogLength, 0);
			if(iInfoLogLength[0] > 0)
			{
				szInfoLog = GLES32.glGetShaderInfoLog(vertexShaderObject_part2);
				System.out.println("RTR: VERTEX SHADER PART 2 COMPILATION LOG:" + szInfoLog);
				uninitialize();
				System.exit(0);
			}
		}

		//fragment shader
		fragmentShaderObject_part2 = GLES32.glCreateShader(GLES32.GL_FRAGMENT_SHADER);
		
		final String fragmentShaderSourceCode_part2 = String.format
		(
			"	#version 320 es										" +
			"													\n 	" +
			"	precision highp float;							\n 	" +
			"													\n 	" +
			"	out vec4 FragColor;									" +
			"													\n 	" +
			"	uniform float u_alpha_value;					\n 	" +
			"	uniform float u_color_r;						\n 	" +
			"	uniform float u_color_g;						\n 	" +
			"	uniform float u_color_b;						\n 	" +
			"													\n 	" +
			"	void main(void)										" +
			"	{													" +
			"		FragColor = vec4(u_color_r,u_color_g,u_color_b, u_alpha_value);	" +
			"	}													"
		);

		GLES32.glShaderSource(fragmentShaderObject_part2, fragmentShaderSourceCode_part2);
		GLES32.glCompileShader(fragmentShaderObject_part2);
		iShaderCompileStatus[0] = 0;
		iInfoLogLength[0] = 0;
		szInfoLog = null;

		GLES32.glGetShaderiv(fragmentShaderObject_part2, GLES32.GL_COMPILE_STATUS, iShaderCompileStatus, 0);
		
		if(iShaderCompileStatus[0] == GLES32.GL_FALSE)
		{
			GLES32.glGetShaderiv(fragmentShaderObject_part2, GLES32.GL_INFO_LOG_LENGTH, iInfoLogLength, 0);

			if(iInfoLogLength[0] > 0)
			{
				szInfoLog = GLES32.glGetShaderInfoLog(fragmentShaderObject_part2);
				System.out.println("RTR: FRAGMENT SHADER PART 2 COMPILATION LOG:" + szInfoLog);
				uninitialize();
				System.exit(0);
			}
		}

		shaderCount[0] = 0;

		//shader program
		shaderProgramObject_part2 = GLES32.glCreateProgram();

		GLES32.glGetProgramiv(shaderProgramObject_part2, GLES32.GL_ATTACHED_SHADERS, shaderCount, 0);
		System.out.println("RTR: initialize => BEFORE ATTACHING SHADERS shader count is :: " + shaderCount[0]);

		GLES32.glAttachShader(shaderProgramObject_part2, vertexShaderObject);
		GLES32.glAttachShader(shaderProgramObject_part2, fragmentShaderObject);

		GLES32.glGetProgramiv(shaderProgramObject_part2, GLES32.GL_ATTACHED_SHADERS, shaderCount, 0);
		System.out.println("RTR: initialize => AFTER ATTACHING SHADERS shader count is :: " + shaderCount[0]);

		//prelinking binding to attributes
		GLES32.glBindAttribLocation(shaderProgramObject_part2,GLESMacros.PS_ATTRIBUTE_POSITION, "vPosition" );

		//link
		GLES32.glLinkProgram(shaderProgramObject_part2);

		//error checking
		iProgramLinkStatus[0] = 0;
		iInfoLogLength[0] = 0;
		szInfoLog = null;

		GLES32.glGetProgramiv(shaderProgramObject_part2, GLES32.GL_LINK_STATUS, iProgramLinkStatus, 0);

		if(iProgramLinkStatus[0] == GLES32.GL_FALSE)
		{
			GLES32.glGetProgramiv(shaderProgramObject_part2, GLES32.GL_INFO_LOG_LENGTH, iInfoLogLength, 0);
			if(iInfoLogLength[0] > 0)
			{
				szInfoLog = GLES32.glGetShaderInfoLog(shaderProgramObject_part2);
				System.out.println("RTR SHADER PROGRAM PART 2 LINKING LOG:" + szInfoLog);
				uninitialize();
				System.exit(0);
			}
		}

		//Get uniform location
		mvpUniform_part2 = GLES32.glGetUniformLocation(shaderProgramObject_part2, "u_mvp_matrix");
		alphaUniform_part2 = GLES32.glGetUniformLocation(shaderProgramObject_part2, "u_alpha_value");
		colorUniform_r_part2 = GLES32.glGetUniformLocation(shaderProgramObject_part2, "u_color_r");
		colorUniform_g_part2 = GLES32.glGetUniformLocation(shaderProgramObject_part2, "u_color_g");
		colorUniform_b_part2 = GLES32.glGetUniformLocation(shaderProgramObject_part2, "u_color_b");
		
		// ***************************************************************************************

		ByteBuffer byteBuffer;
		FloatBuffer positionBuffer;
		FloatBuffer colorBuffer;
		
		//-------------------------------- I -------------------------------------------
		// Position vertices array, as it is inline initialization, no need of specifying size of array on rhs 
		final float[] i1Vertices = new float[]
		{
			-1.20f, 0.5f, 0.0f,
			- 0.88f, 0.5f, 0.0f,
			-1.04f, 0.5f, 0.0f,
			-1.04f, -0.5f, 0.0f,
			-1.20f, -0.5f, 0.0f,
			- 0.88f, -0.5f, 0.0f,
		};

		final float[] i1Color = new float[]
		{
			1.0f, 0.6f, 0.2f,
			1.0f, 0.6f, 0.2f,
			1.0f, 0.6f, 0.2f,
			0.070f, 0.533f, 0.027f,
			0.070f, 0.533f, 0.027f,
			0.070f, 0.533f, 0.027f,
		};
		//create and bind with vao
		GLES32.glGenVertexArrays(1, vao_i1, 0);
		GLES32.glBindVertexArray(vao_i1[0]);
		//Create and bind with vbo for position
		GLES32.glGenBuffers(1, vbo_position_i1, 0);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vbo_position_i1[0]);
		//Allocate the buffer directly from native memory
		byteBuffer = ByteBuffer.allocateDirect(i1Vertices.length*4); //4 is size of float in java
		//Arrange the byte order of buffer to the native byte order
		byteBuffer.order(ByteOrder.nativeOrder());
		//create the float type buffer and convert our byte type buffer into float type buffer
		positionBuffer = byteBuffer.asFloatBuffer();
		//Now put your array into the "COOKED" buffer
		positionBuffer.put(i1Vertices);
		//set the array at 0th position of the buffer as our array is not interleaved
		positionBuffer.position(0);
		//data to the buffer
		GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER, i1Vertices.length * 4, positionBuffer,GLES32.GL_STATIC_DRAW);
		GLES32.glVertexAttribPointer(GLESMacros.PS_ATTRIBUTE_POSITION, 
										3,				// break data after x y z, i.e. 3
										GLES32.GL_FLOAT, // type of data
										false, 			// normalize?
										0, 				//stride?-no
										0 );			//offset?-no(windows : NULL, in java null is not #define 0) therefore 0
		GLES32.glEnableVertexAttribArray(GLESMacros.PS_ATTRIBUTE_POSITION);
		
		//bind to buffer
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0);
		
		// for color
		//Create and bind with vbo for position
		GLES32.glGenBuffers(1, vbo_color_i1, 0);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vbo_color_i1[0]);

		//Allocate the buffer directly from native memory
		byteBuffer = ByteBuffer.allocateDirect(i1Color.length*4); //4 is size of float in java
		byteBuffer.order(ByteOrder.nativeOrder());
		colorBuffer = byteBuffer.asFloatBuffer();
		colorBuffer.put(i1Color);
		colorBuffer.position(0);

		//data to the color buffer
		GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER, i1Color.length * 4, colorBuffer,GLES32.GL_STATIC_DRAW);
		GLES32.glVertexAttribPointer(GLESMacros.PS_ATTRIBUTE_COLOR, 
										3,				// break data after x y z, i.e. 3
										GLES32.GL_FLOAT, // type of data
										false, 			// normalize?
										0, 				//stride?-no
										0 );			//offset?-no(windows : NULL, in java null is not #define 0) therefore 0
		GLES32.glEnableVertexAttribArray(GLESMacros.PS_ATTRIBUTE_COLOR);
		
		//bind to buffer
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0);
		
		GLES32.glBindVertexArray(0);
		//------------------------------------------------------------------------------
		
		//------------------------------------- N --------------------------------------
		final float[] nVertices = new float[]
		{
			-0.68f, 0.52f, 0.0f,
			-0.68f, -0.52f, 0.0f,
			-0.68f, 0.52f, 0.0f,
			-0.36f, -0.51f, 0.0f,
			-0.36f, 0.52f, 0.0f,
			-0.36f, -0.52f, 0.0f
		};

		final float[] nColor = new float[]
		{
			1.0f, 0.6f, 0.2f,
			0.070f, 0.533f, 0.027f,
			1.0f, 0.6f, 0.2f,
			0.070f, 0.533f, 0.027f,
			1.0f, 0.6f, 0.2f,
			0.070f, 0.533f, 0.027f
		};

		//create and bind with vao
		GLES32.glGenVertexArrays(1, vao_n, 0);
		GLES32.glBindVertexArray(vao_n[0]);
		//Create and bind with vbo for position
		GLES32.glGenBuffers(1, vbo_position_n, 0);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vbo_position_n[0]);
		//Allocate the buffer directly from native memory
		byteBuffer = ByteBuffer.allocateDirect(nVertices.length*4); //4 is size of float in java
		//Arrange the byte order of buffer to the native byte order
		byteBuffer.order(ByteOrder.nativeOrder());
		//create the float type buffer and convert our byte type buffer into float type buffer
		positionBuffer = byteBuffer.asFloatBuffer();
		//Now put your array into the "COOKED" buffer
		positionBuffer.put(nVertices);
		//set the array at 0th position of the buffer as our array is not interleaved
		positionBuffer.position(0);
		//data to the buffer
		GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER, nVertices.length * 4, positionBuffer,GLES32.GL_STATIC_DRAW);
		GLES32.glVertexAttribPointer(GLESMacros.PS_ATTRIBUTE_POSITION, 
										3,				// break data after x y z, i.e. 3
										GLES32.GL_FLOAT, // type of data
										false, 			// normalize?
										0, 				//stride?-no
										0 );			//offset?-no(windows : NULL, in java null is not #define 0) therefore 0
		GLES32.glEnableVertexAttribArray(GLESMacros.PS_ATTRIBUTE_POSITION);
		//unbind to buffer
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0);
		
		// for color
		//Create and bind with vbo for position
		GLES32.glGenBuffers(1, vbo_color_n, 0);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vbo_color_n[0]);

		//Allocate the buffer directly from native memory
		byteBuffer = ByteBuffer.allocateDirect(nColor.length*4); //4 is size of float in java
		byteBuffer.order(ByteOrder.nativeOrder());
		colorBuffer = byteBuffer.asFloatBuffer();
		colorBuffer.put(nColor);
		colorBuffer.position(0);

		//data to the color buffer
		GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER, nColor.length * 4, colorBuffer,GLES32.GL_STATIC_DRAW);
		GLES32.glVertexAttribPointer(GLESMacros.PS_ATTRIBUTE_COLOR, 
										3,				// break data after x y z, i.e. 3
										GLES32.GL_FLOAT, // type of data
										false, 			// normalize?
										0, 				//stride?-no
										0 );			//offset?-no(windows : NULL, in java null is not #define 0) therefore 0
		GLES32.glEnableVertexAttribArray(GLESMacros.PS_ATTRIBUTE_COLOR);
		
		//unbind to buffer
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0);
		// unbind to vertex array buffer
		GLES32.glBindVertexArray(0);
		//------------------------------------------------------------------------------
		
		// -------------------------------------- D -------------------------------------
		final float[] dVertices = new float[]
		{
			-0.10f,  0.5f, 0.0f,
			-0.10f, -0.5f, 0.0f,
			-0.17f,  0.5f, 0.0f,
			 0.17f,  0.5f, 0.0f,
			 0.17f,  0.5f, 0.0f,
			 0.17f, -0.5f, 0.0f,
			-0.17f, -0.5f, 0.0f,
			 0.17f, -0.5f, 0.0f
		};

		final float[] dColor = new float[]
		{
			1.0f,	0.6f, 	0.2f,
			0.070f, 0.533f, 0.027f,
			1.0f, 	0.6f, 	0.2f,
			1.0f, 	0.6f,	0.2f,
			1.0f, 	0.6f, 	0.2f,
			0.070f, 0.533f, 0.027f,
			0.070f, 0.533f, 0.027f,
			0.070f, 0.533f, 0.027f
		};

		//create and bind with vao
		GLES32.glGenVertexArrays(1, vao_d, 0);
		GLES32.glBindVertexArray(vao_d[0]);
		
		//Create and bind with vbo for position
		GLES32.glGenBuffers(1, vbo_position_d, 0);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vbo_position_d[0]);
		
		//Allocate the buffer directly from native memory
		byteBuffer = ByteBuffer.allocateDirect(dVertices.length*4); //4 is size of float in java
		byteBuffer.order(ByteOrder.nativeOrder());
		positionBuffer = byteBuffer.asFloatBuffer();
		positionBuffer.put(dVertices);
		positionBuffer.position(0);

		//data to the buffer
		GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER, dVertices.length * 4, positionBuffer,GLES32.GL_STATIC_DRAW);
		GLES32.glVertexAttribPointer(GLESMacros.PS_ATTRIBUTE_POSITION, 
										3,				// break data after x y z, i.e. 3
										GLES32.GL_FLOAT, // type of data
										false, 			// normalize?
										0, 				//stride?-no
										0 );			//offset?-no(windows : NULL, in java null is not #define 0) therefore 0
		GLES32.glEnableVertexAttribArray(GLESMacros.PS_ATTRIBUTE_POSITION);
		//unbind to buffer
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0);
		
		// for color
		//Create and bind with vbo for position
		GLES32.glGenBuffers(1, vbo_color_d, 0);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vbo_color_d[0]);

		//Allocate the buffer directly from native memory
		byteBuffer = ByteBuffer.allocateDirect(dColor.length*4); //4 is size of float in java
		byteBuffer.order(ByteOrder.nativeOrder());
		colorBuffer = byteBuffer.asFloatBuffer();
		colorBuffer.put(dColor);
		colorBuffer.position(0);

		//data to the color buffer
		GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER, dColor.length * 4, colorBuffer,GLES32.GL_STATIC_DRAW);
		GLES32.glVertexAttribPointer(GLESMacros.PS_ATTRIBUTE_COLOR, 
										3,				// break data after x y z, i.e. 3
										GLES32.GL_FLOAT, // type of data
										false, 			// normalize?
										0, 				//stride?-no
										0 );			//offset?-no(windows : NULL, in java null is not #define 0) therefore 0
		GLES32.glEnableVertexAttribArray(GLESMacros.PS_ATTRIBUTE_COLOR);
		
		//unbind to buffer
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0);
		
		// unbind to vertex array buffer
		GLES32.glBindVertexArray(0);
		//------------------------------------------------------------------------------
		
		//-------------------------------- I2 -------------------------------------------
		final float[] i2Vertices = new float[]
		{
			0.36f,  0.5f, 0.0f,
			0.68f,  0.5f, 0.0f,
			0.52f,  0.5f, 0.0f,
			0.52f, -0.5f, 0.0f,
			0.36f, -0.5f, 0.0f,
			0.68f, -0.5f, 0.0f
		};

		final float[] i2Color = new float[]
		{
			1.0f, 	0.6f, 	0.2f,
			1.0f, 	0.6f, 	0.2f,
			1.0f, 	0.6f, 	0.2f,
			0.070f, 0.533f, 0.027f,
			0.070f, 0.533f, 0.027f,
			0.070f, 0.533f, 0.027f
		};

		//create and bind with vao
		GLES32.glGenVertexArrays(1, vao_i2, 0);
		GLES32.glBindVertexArray(vao_i2[0]);
		
		//Create and bind with vbo for position
		GLES32.glGenBuffers(1, vbo_position_i2, 0);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vbo_position_i2[0]);
		
		//Allocate the buffer directly from native memory
		byteBuffer = ByteBuffer.allocateDirect(i2Vertices.length*4); //4 is size of float in java
		byteBuffer.order(ByteOrder.nativeOrder());
		positionBuffer = byteBuffer.asFloatBuffer();
		positionBuffer.put(i2Vertices);
		positionBuffer.position(0);

		//data to the buffer
		GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER, i2Vertices.length * 4, positionBuffer,GLES32.GL_STATIC_DRAW);
		GLES32.glVertexAttribPointer(GLESMacros.PS_ATTRIBUTE_POSITION, 
										3,				// break data after x y z, i.e. 3
										GLES32.GL_FLOAT, // type of data
										false, 			// normalize?
										0, 				//stride?-no
										0 );			//offset?-no(windows : NULL, in java null is not #define 0) therefore 0
		GLES32.glEnableVertexAttribArray(GLESMacros.PS_ATTRIBUTE_POSITION);
		//unbind to buffer
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0);
		
		// for color
		//Create and bind with vbo for position
		GLES32.glGenBuffers(1, vbo_color_i2, 0);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vbo_color_i2[0]);

		//Allocate the buffer directly from native memory
		byteBuffer = ByteBuffer.allocateDirect(i2Color.length*4); //4 is size of float in java
		byteBuffer.order(ByteOrder.nativeOrder());
		colorBuffer = byteBuffer.asFloatBuffer();
		colorBuffer.put(i2Color);
		colorBuffer.position(0);

		//data to the color buffer
		GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER, i2Color.length * 4, colorBuffer,GLES32.GL_STATIC_DRAW);
		GLES32.glVertexAttribPointer(GLESMacros.PS_ATTRIBUTE_COLOR, 
										3,				// break data after x y z, i.e. 3
										GLES32.GL_FLOAT, // type of data
										false, 			// normalize?
										0, 				//stride?-no
										0 );			//offset?-no(windows : NULL, in java null is not #define 0) therefore 0
		GLES32.glEnableVertexAttribArray(GLESMacros.PS_ATTRIBUTE_COLOR);
		
		//unbind to buffer
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0);
		
		// unbind to vertex array buffer
		GLES32.glBindVertexArray(0);
		//------------------------------------------------------------------------------
		
		//--------------------------------------- A -----------------------------------------
		final float[] aVertices = new float[]
		{
			0.83f, -0.52f, 0.0f,
			1.04f, 	0.52f, 0.0f,
			1.04f, 	0.52f, 0.0f,
			1.25f, -0.52f, 0.0f
		};

		final float[] aColor = new float[]
		{
			0.070f, 0.533f, 0.027f,
			1.0f, 	0.6f, 	0.2f,
			1.0f, 	0.6f, 	0.2f,
			0.070f, 0.533f, 0.027f
		};

		//create and bind with vao
		GLES32.glGenVertexArrays(1, vao_a, 0);
		GLES32.glBindVertexArray(vao_a[0]);
		
		//Create and bind with vbo for position
		GLES32.glGenBuffers(1, vbo_position_a, 0);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vbo_position_a[0]);
		
		//Allocate the buffer directly from native memory
		byteBuffer = ByteBuffer.allocateDirect(aVertices.length*4); //4 is size of float in java
		byteBuffer.order(ByteOrder.nativeOrder());
		positionBuffer = byteBuffer.asFloatBuffer();
		positionBuffer.put(aVertices);
		positionBuffer.position(0);

		//data to the buffer
		GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER, aVertices.length * 4, positionBuffer,GLES32.GL_STATIC_DRAW);
		GLES32.glVertexAttribPointer(GLESMacros.PS_ATTRIBUTE_POSITION, 
										3,				// break data after x y z, i.e. 3
										GLES32.GL_FLOAT, // type of data
										false, 			// normalize?
										0, 				//stride?-no
										0 );			//offset?-no(windows : NULL, in java null is not #define 0) therefore 0
		GLES32.glEnableVertexAttribArray(GLESMacros.PS_ATTRIBUTE_POSITION);
		//unbind to buffer
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0);
		
		// for color
		//Create and bind with vbo for position
		GLES32.glGenBuffers(1, vbo_color_a, 0);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vbo_color_a[0]);

		//Allocate the buffer directly from native memory
		byteBuffer = ByteBuffer.allocateDirect(aColor.length*4); //4 is size of float in java
		byteBuffer.order(ByteOrder.nativeOrder());
		colorBuffer = byteBuffer.asFloatBuffer();
		colorBuffer.put(aColor);
		colorBuffer.position(0);

		//data to the color buffer
		GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER, aColor.length * 4, colorBuffer,GLES32.GL_STATIC_DRAW);
		GLES32.glVertexAttribPointer(GLESMacros.PS_ATTRIBUTE_COLOR, 
										3,				// break data after x y z, i.e. 3
										GLES32.GL_FLOAT, // type of data
										false, 			// normalize?
										0, 				//stride?-no
										0 );			//offset?-no(windows : NULL, in java null is not #define 0) therefore 0
		GLES32.glEnableVertexAttribArray(GLESMacros.PS_ATTRIBUTE_COLOR);
		
		//unbind to buffer
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0);
		
		// unbind to vertex array buffer
		GLES32.glBindVertexArray(0);
		//------------------------------------------------------------------------------
		
		//--------------------------------------- FLAG -----------------------------------------
		final float[] flagVertices = new float[]
		{
			0.953f,  0.015f, 0.0f,
			1.128f,  0.015f, 0.0f,
			1.122f,  0.045f, 0.0f,
			0.958f,  0.045f, 0.0f,
			0.947f, -0.015f, 0.0f,
			1.135f, -0.015f, 0.0f,
			1.128f,  0.015f, 0.0f,
			0.953f,  0.015f, 0.0f,
			1.135f, -0.015f, 0.0f,
			0.947f, -0.015f, 0.0f,
			0.94f, 	-0.045f, 0.0f,
			1.14f, 	-0.045f, 0.0f
		};

		final float[] flagColor = new float[]
		{
			1.0f, 	0.6f, 	0.2f,
			1.0f, 	0.6f, 	0.2f,
			1.0f, 	0.6f, 	0.2f,
			1.0f, 	0.6f, 	0.2f,
			1.0f, 	1.0f, 	1.0f,
			1.0f, 	1.0f, 	1.0f,
			1.0f, 	1.0f, 	1.0f,
			1.0f, 	1.0f, 	1.0f,
			0.070f, 0.533f, 0.027f,
			0.070f, 0.533f, 0.027f,
			0.070f, 0.533f, 0.027f,
			0.070f, 0.533f, 0.027f
		};

		//create and bind with vao
		GLES32.glGenVertexArrays(1, vao_flag, 0);
		GLES32.glBindVertexArray(vao_flag[0]);
		
		//Create and bind with vbo for position
		GLES32.glGenBuffers(1, vbo_position_flag, 0);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vbo_position_flag[0]);
		
		//Allocate the buffer directly from native memory
		byteBuffer = ByteBuffer.allocateDirect(flagVertices.length*4); //4 is size of float in java
		byteBuffer.order(ByteOrder.nativeOrder());
		positionBuffer = byteBuffer.asFloatBuffer();
		positionBuffer.put(flagVertices);
		positionBuffer.position(0);

		//data to the buffer
		GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER, flagVertices.length * 4, positionBuffer,GLES32.GL_STATIC_DRAW);
		GLES32.glVertexAttribPointer(GLESMacros.PS_ATTRIBUTE_POSITION, 
										3,				// break data after x y z, i.e. 3
										GLES32.GL_FLOAT, // type of data
										false, 			// normalize?
										0, 				//stride?-no
										0 );			//offset?-no(windows : NULL, in java null is not #define 0) therefore 0
		GLES32.glEnableVertexAttribArray(GLESMacros.PS_ATTRIBUTE_POSITION);
		//unbind to buffer
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0);
		
		// for color
		//Create and bind with vbo for position
		GLES32.glGenBuffers(1, vbo_color_flag, 0);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vbo_color_flag[0]);

		//Allocate the buffer directly from native memory
		byteBuffer = ByteBuffer.allocateDirect(flagColor.length*4); //4 is size of float in java
		byteBuffer.order(ByteOrder.nativeOrder());
		colorBuffer = byteBuffer.asFloatBuffer();
		colorBuffer.put(flagColor);
		colorBuffer.position(0);

		//data to the color buffer
		GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER, flagColor.length * 4, colorBuffer,GLES32.GL_STATIC_DRAW);
		GLES32.glVertexAttribPointer(GLESMacros.PS_ATTRIBUTE_COLOR, 
										3,				// break data after x y z, i.e. 3
										GLES32.GL_FLOAT, // type of data
										false, 			// normalize?
										0, 				//stride?-no
										0 );			//offset?-no(windows : NULL, in java null is not #define 0) therefore 0
		GLES32.glEnableVertexAttribArray(GLESMacros.PS_ATTRIBUTE_COLOR);
		
		//unbind to buffer
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0);
		
		// unbind to vertex array buffer
		GLES32.glBindVertexArray(0);
		//------------------------------------------------------------------------------
		
		//------------------------------------ PLANE ---------------------------------------
		final float[] planeVertices = new float[]
		{
			0.3f,    0.04f,  0.0f,	// 4
			0.3f,	-0.04f,  0.0f,	// 2
			0.4f,	 0.0f,   0.0f,	// 3
			0.0f,	-0.04f,  0.0f,	// 1
			0.3f,	-0.04f,  0.0f,	// 2
			0.3f,    0.04f,  0.0f,	// 4
			0.0f,    0.04f,  0.0f,	// 5
			0.0f,	-0.04f,  0.0f,	// 1
			0.0f,    0.04f,  0.0f,	// 5
			-0.08f,  0.09f,  0.0f,	// 6
			-0.08f, -0.09f,  0.0f,	// 7	BODY
			0.2f,    0.03f,  0.0f,
			0.10f,   0.2f,   0.0f,
			0.0f,    0.2f,   0.0f,
			0.05f,	 0.03f,  0.0f,	// UPPER WING
			0.2f,	-0.03f,  0.0f,
			0.10f,	-0.2f,   0.0f,
			0.0f,	-0.2f,   0.0f,
			0.05f,	-0.03f,  0.0f,	// LOWER WING
			0.03f,   0.02f,  0.0f,
			0.08f,   0.02f,  0.0f,
			0.03f,	-0.02f,  0.0f,
			0.08f,  -0.02f,  0.0f,
			0.055f,  0.02f,  0.0f,
			0.055f, -0.02f,  0.0f,	// I
			0.10f,   0.02f,  0.0f,
			0.10f,	-0.025f, 0.0f,
			0.14f,   0.02f,  0.0f,
			0.14f,	-0.025f, 0.0f,
			0.10f,   0.02f,  0.0f,
			0.14f,   0.02f,  0.0f,
			0.10f,   0.0f,   0.0f,
			0.14f,   0.0f,   0.0f,	// A
			0.16f,   0.02f,  0.0f,
			0.16f,	-0.025f, 0.0f,
			0.16f,   0.02f,  0.0f,
			0.20f,   0.02f,  0.0f,
			0.16f,   0.0f,   0.0f,
			0.19f,   0.0f,   0.0f,	// F
		   -0.08f, 	 0.015f, 0.0f,
		   -0.15f, 	 0.015f, 0.0f,
		   -0.15f, 	 0.045f, 0.0f,
		   -0.08f, 	 0.045f, 0.0f,	// safron trail ends
		   -0.08f,   0.015f, 0.0f,
		   -0.15f,   0.015f, 0.0f,
			-0.15f,  -0.015f, 0.0f,
			-0.08f,  -0.015f, 0.0f,
			-0.08f,  -0.015f, 0.0f,
			-0.15f,  -0.015f, 0.0f,
			-0.15f, 	-0.045f, 0.0f,
			-0.08f, 	-0.045f, 0.0f

		};

		final float[] planeColor = new float[]
		{
			0.72f, 0.886f, 0.933f,
			0.72f, 0.886f, 0.933f,
			0.72f, 0.886f, 0.933f,
			0.72f, 0.886f, 0.933f,
			0.72f, 0.886f, 0.933f,
			0.72f, 0.886f, 0.933f,
			0.72f, 0.886f, 0.933f,
			0.72f, 0.886f, 0.933f,
			0.72f, 0.886f, 0.933f,
			0.72f, 0.886f, 0.933f,
			0.72f, 0.886f, 0.933f,
			0.72f, 0.886f, 0.933f, // body
			0.72f, 0.886f, 0.933f,
			0.72f, 0.886f, 0.933f,
			0.72f, 0.886f, 0.933f,
			0.72f, 0.886f, 0.933f,
			0.72f, 0.886f, 0.933f,
			0.72f, 0.886f, 0.933f,
			0.72f, 0.886f, 0.933f,
			0.72f, 0.886f, 0.933f,
			1.0f, 1.0f, 1.0f,
			1.0f, 1.0f, 1.0f,
			1.0f, 1.0f, 1.0f,
			1.0f, 1.0f, 1.0f,
			1.0f, 1.0f, 1.0f,
			1.0f, 1.0f, 1.0f,
			1.0f, 1.0f, 1.0f,
			1.0f, 1.0f, 1.0f,
			1.0f, 1.0f, 1.0f,
			1.0f, 1.0f, 1.0f,
			1.0f, 1.0f, 1.0f,
			1.0f, 1.0f, 1.0f,
			1.0f, 1.0f, 1.0f,
			1.0f, 1.0f, 1.0f,
			1.0f, 1.0f, 1.0f,
			1.0f, 1.0f, 1.0f,
			1.0f, 1.0f, 1.0f,
			1.0f, 1.0f, 1.0f,
			1.0f, 1.0f, 1.0f,
			1.0f, 	0.6f, 	0.2f,
			1.0f, 	0.6f, 	0.2f,
			1.0f, 	0.6f, 	0.2f,
			1.0f, 	0.6f, 	0.2f, // saffron
			1.0f, 1.0f, 1.0f,
			1.0f, 1.0f, 1.0f,
			1.0f, 1.0f, 1.0f,
			1.0f, 1.0f, 1.0f, 	// white
			0.070f, 0.533f, 0.027f,
			0.070f, 0.533f, 0.027f,
			0.070f, 0.533f, 0.027f,
			0.070f, 0.533f, 0.027f
		};

		//create and bind with vao
		GLES32.glGenVertexArrays(1, vao_plane, 0);
		GLES32.glBindVertexArray(vao_plane[0]);
		
		//Create and bind with vbo for position
		GLES32.glGenBuffers(1, vbo_position_plane, 0);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vbo_position_plane[0]);
		
		//Allocate the buffer directly from native memory
		byteBuffer = ByteBuffer.allocateDirect(planeVertices.length*4); //4 is size of float in java
		byteBuffer.order(ByteOrder.nativeOrder());
		positionBuffer = byteBuffer.asFloatBuffer();
		positionBuffer.put(planeVertices);
		positionBuffer.position(0);

		//data to the buffer
		GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER, planeVertices.length * 4, positionBuffer,GLES32.GL_STATIC_DRAW);
		GLES32.glVertexAttribPointer(GLESMacros.PS_ATTRIBUTE_POSITION, 
										3,				// break data after x y z, i.e. 3
										GLES32.GL_FLOAT, // type of data
										false, 			// normalize?
										0, 				//stride?-no
										0 );			//offset?-no(windows : NULL, in java null is not #define 0) therefore 0
		GLES32.glEnableVertexAttribArray(GLESMacros.PS_ATTRIBUTE_POSITION);
		//unbind to buffer
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0);
		
		// for color
		//Create and bind with vbo for position
		GLES32.glGenBuffers(1, vbo_color_plane, 0);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vbo_color_plane[0]);

		//Allocate the buffer directly from native memory
		byteBuffer = ByteBuffer.allocateDirect(planeColor.length*4); //4 is size of float in java
		byteBuffer.order(ByteOrder.nativeOrder());
		colorBuffer = byteBuffer.asFloatBuffer();
		colorBuffer.put(planeColor);
		colorBuffer.position(0);

		//data to the color buffer
		GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER, planeColor.length * 4, colorBuffer,GLES32.GL_STATIC_DRAW);
		GLES32.glVertexAttribPointer(GLESMacros.PS_ATTRIBUTE_COLOR, 
										3,				// break data after x y z, i.e. 3
										GLES32.GL_FLOAT, // type of data
										false, 			// normalize?
										0, 				//stride?-no
										0 );			//offset?-no(windows : NULL, in java null is not #define 0) therefore 0
		GLES32.glEnableVertexAttribArray(GLESMacros.PS_ATTRIBUTE_COLOR);
		
		//unbind to buffer
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0);
		
		// unbind to vertex array buffer
		GLES32.glBindVertexArray(0);
		//------------------------------------------------------------------------------
		
		// -----------------------------------Plane Path -----------------------------------
		float path2Value = -2.2f;

		for (int i = 0; i < 1500; i = i + 3)
		{
			path2[i] = path2Value;
			path2[i + 1] = 0;
			path2[i + 2] = 0;

			if (path2Value < -1.2f)
				path2Value = path2Value + 0.01f;
			else if (path2Value >= -1.2f && path2Value < 1.2)
				path2Value = path2Value + 0.008f;
			else if (path2Value >= 1.2f && path2Value <= 2.2f)
				path2Value = path2Value + 0.01f;
		}

		float path1StartingValue_x1 = -1.0f;	 // -1 to 0
		float path1StartingValue_x2 = 0.0f;		 // 0 to 1
		float path1Value_y = 0.0f;
		
		for (int i = 0; i < 1500; i = i + 3)
		{
			if (i < 300)
			{
				path1Value_y = 1.2f * path1StartingValue_x1 * path1StartingValue_x1;

				path1[i] = path1StartingValue_x1 - 1.2f;
				path1[i + 1] = path1Value_y;
				path1[i + 2] = 0.0f;

				path1StartingValue_x1 = path1StartingValue_x1 + 0.01f;
			}
			if ((i >= 300) && (i < 1200))
			{
				path1Value_y = 0.0f;

				path1[i] = path1StartingValue_x1 - 1.2f;
				path1[i + 1] = path1Value_y;
				path1[i + 2] = 0.0f;

				path1StartingValue_x1 = path1StartingValue_x1 + 0.008f;

			}
			if ((i >= 1200) && (i < 1500))
			{
				path1Value_y = 1.2f * path1StartingValue_x2 * path1StartingValue_x2;

				path1[i] = path1StartingValue_x2 + 1.2f;
				path1[i + 1] = path1Value_y;
				path1[i + 2] = 0.0f;

				path1StartingValue_x2 = path1StartingValue_x2 + 0.01f;
			}
		}

		float path3StartingValue_x1 = -1.0f;	 // -1 to 0
		float path3StartingValue_x2 = 0.0f;		 // 0 to 1
		float path3Value_y = 0.0f;
		for (int i = 0; i < 1500; i = i + 3)
		{
			if (i < 300)
			{
				path3Value_y = 1.2f * path3StartingValue_x1 * path3StartingValue_x1;

				path3[i] = path3StartingValue_x1 - 1.2f;
				path3[i + 1] = -path3Value_y;
				path3[i + 2] = 0.0f;

				path3StartingValue_x1 = path3StartingValue_x1 + 0.01f;
			}
			if ((i >= 300) && (i < 1200))
			{
				path3Value_y = 0.0f;

				path3[i] = path3StartingValue_x1 - 1.2f;
				path3[i + 1] = -path3Value_y;
				path3[i + 2] = 0.0f;

				path3StartingValue_x1 = path3StartingValue_x1 + 0.008f;

			}
			if ((i >= 1200) && (i < 1500))
			{
				path3Value_y = 1.2f * path3StartingValue_x2 * path3StartingValue_x2;

				path3[i] = path3StartingValue_x2 + 1.2f;
				path3[i + 1] = -path3Value_y;
				path3[i + 2] = 0.0f;

				path3StartingValue_x2 = path3StartingValue_x2 + 0.01f;
			}

		}
		// Path 1
		//create and bind with vao
		GLES32.glGenVertexArrays(1, vao_path1, 0);
		GLES32.glBindVertexArray(vao_path1[0]);
		
		//Create and bind with vbo for position
		GLES32.glGenBuffers(1, vbo_position_path1, 0);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vbo_position_path1[0]);
		
		//Allocate the buffer directly from native memory
		byteBuffer = ByteBuffer.allocateDirect(path1.length*4); //4 is size of float in java
		byteBuffer.order(ByteOrder.nativeOrder());
		positionBuffer = byteBuffer.asFloatBuffer();
		positionBuffer.put(path1);
		positionBuffer.position(0);

		//data to the buffer
		GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER, path1.length * 4, positionBuffer,GLES32.GL_STATIC_DRAW);
		GLES32.glVertexAttribPointer(GLESMacros.PS_ATTRIBUTE_POSITION, 
										3,				// break data after x y z, i.e. 3
										GLES32.GL_FLOAT, // type of data
										false, 			// normalize?
										0, 				//stride?-no
										0 );			//offset?-no(windows : NULL, in java null is not #define 0) therefore 0
		GLES32.glEnableVertexAttribArray(GLESMacros.PS_ATTRIBUTE_POSITION);
		//unbind to buffer
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0);
		// unbind to vertex array buffer
		GLES32.glBindVertexArray(0);

		// Path 2
		//create and bind with vao
		GLES32.glGenVertexArrays(1, vao_path2, 0);
		GLES32.glBindVertexArray(vao_path2[0]);
		
		//Create and bind with vbo for position
		GLES32.glGenBuffers(1, vbo_position_path2, 0);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vbo_position_path2[0]);
		
		//Allocate the buffer directly from native memory
		byteBuffer = ByteBuffer.allocateDirect(path2.length*4); //4 is size of float in java
		byteBuffer.order(ByteOrder.nativeOrder());
		positionBuffer = byteBuffer.asFloatBuffer();
		positionBuffer.put(path2);
		positionBuffer.position(0);

		//data to the buffer
		GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER, path2.length * 4, positionBuffer,GLES32.GL_STATIC_DRAW);
		GLES32.glVertexAttribPointer(GLESMacros.PS_ATTRIBUTE_POSITION, 
										3,				// break data after x y z, i.e. 3
										GLES32.GL_FLOAT, // type of data
										false, 			// normalize?
										0, 				//stride?-no
										0 );			//offset?-no(windows : NULL, in java null is not #define 0) therefore 0
		GLES32.glEnableVertexAttribArray(GLESMacros.PS_ATTRIBUTE_POSITION);
		//unbind to buffer
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0);
		// unbind to vertex array buffer
		GLES32.glBindVertexArray(0);

		// path 3
		//create and bind with vao
		GLES32.glGenVertexArrays(1, vao_path3, 0);
		GLES32.glBindVertexArray(vao_path3[0]);
		
		//Create and bind with vbo for position
		GLES32.glGenBuffers(1, vbo_position_path3, 0);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vbo_position_path3[0]);
		
		//Allocate the buffer directly from native memory
		byteBuffer = ByteBuffer.allocateDirect(path3.length*4); //4 is size of float in java
		byteBuffer.order(ByteOrder.nativeOrder());
		positionBuffer = byteBuffer.asFloatBuffer();
		positionBuffer.put(path3);
		positionBuffer.position(0);

		//data to the buffer
		GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER, path3.length * 4, positionBuffer,GLES32.GL_STATIC_DRAW);
		GLES32.glVertexAttribPointer(GLESMacros.PS_ATTRIBUTE_POSITION, 
										3,				// break data after x y z, i.e. 3
										GLES32.GL_FLOAT, // type of data
										false, 			// normalize?
										0, 				//stride?-no
										0 );			//offset?-no(windows : NULL, in java null is not #define 0) therefore 0
		GLES32.glEnableVertexAttribArray(GLESMacros.PS_ATTRIBUTE_POSITION);
		//unbind to buffer
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0);
		// unbind to vertex array buffer
		GLES32.glBindVertexArray(0);
		//----------------------------------------------------------------------------------

		trans_i1 = -2.0f;
		trans_n = 2.0f;
		trans_i2 = -2.0f;
		trans_a = 2.0f;



		GLES32.glClearColor(0.0f,0.0f,0.0f,1.0f);
		GLES32.glEnable(GLES32.GL_DEPTH_TEST);
		GLES32.glDepthFunc(GLES32.GL_LEQUAL);
		//GLES32.glEnable( GLES32.GL_PROGRAM_POINT_SIZE );
		Matrix.setIdentityM(perspectiveProjectionMatrix,0);
		//no warmup call to resize as we are already in fullscreen. 
		//we dont run multiple app with screen sharing in android unlike windows.
	
		//System.out.println("RTR:display :: max point size " + GLES32.GL_POINT_SIZE);
    
	}

    private void resize(int width, int height)
    {
        if(height<0)
        {
            height =1;
        }
		GLES32.glViewport(0,0,width, height);
		Matrix.perspectiveM(perspectiveProjectionMatrix, 0, 45.0f, (float)width/ (float)height, 0.1f, 100.0f);
    }

	private void update()
	{
		if (translateI1 == true)
		{
			if (trans_i1 >= 0.0f)
			{
				trans_i1 = 0.0f;
				translateA = true;
			}
			else
			{
				trans_i1 += 0.005f;
			}
		}
		if (translateA == true)
		{
			if (trans_a <= 0.0f)
			{
				trans_a = 0.0f;
				translateN = true;
			}
			else
			{
				trans_a -= 0.005f;
			}
		}
		if (translateN == true)
		{
			if (trans_n <= 0.0f)
			{
				trans_n = 0.0f;
				translateI2 = true;
			}
			else
			{
				trans_n -= 0.005f;
			}
		}
		if (translateI2 == true)
		{
			if (trans_i2 >= 0.0f)
			{
				trans_i2 = 0.0f;
				translateD = true;
			}
			else
			{
				trans_i2 += 0.005f;
			}
		}

		if (translateD == true && alphaValue < 1.0f)
		{
			alphaValue = alphaValue + 0.005f;
			if (alphaValue >= 1.0f)
			{
				translatePlane = true;
				System.out.println("RTR: update => translate plane condition is true now ");
			}
		}

		if (translatePlane == true)
		{
			if ((anim % 4) == 0)
			{	
				if (ip < 1500)
					ip = ip + 3;
				pointCount++;

				if (pointCount >= 350)
					showFlag = true;

				if (pointCount < 100)
				{
					angle1 = angle1 + delta;
					angle2 = angle2 - delta;
				}
				else if (pointCount < 400)
				{
					angle1 = 0.0f;
					angle2 = 0.0f;
				}
				else if (pointCount < 490)
				{
					angle1 = angle1 + delta;
					angle2 = angle2 - delta;
				}
				if(pointCount >= 490)
					pointCount = 490;

			}
		}


		if (pointCount == 490)
		{
			alphaValue_part2 = alphaValue_part2 - 0.005f;
			System.out.println("RTR: update => pointcount > 500, alpha value change part2 :: " + alphaValue_part2);
		}
		System.out.println("RTR: update => pointcount :: " + pointCount);
		System.out.println("RTR: update => ip :: " + ip);
		anim++;

	}

	private void display()
	{
		//variable declarations
		float[] modelViewMatrix = new float[16];
		float[] modelViewProjectionMatrix = new float[16];
		float[] translationMatrix = new float[16];
		float[] rotationMatrix = new float[16];

		//code
		GLES32.glClear(GLES32.GL_COLOR_BUFFER_BIT|GLES32.GL_DEPTH_BUFFER_BIT);
		
		//making martices identity
		Matrix.setIdentityM(modelViewMatrix, 0);
		Matrix.setIdentityM(modelViewProjectionMatrix, 0);
		Matrix.setIdentityM(translationMatrix, 0);
		Matrix.setIdentityM(rotationMatrix, 0);

		if (translateI1 == true)
		{
			GLES32.glUseProgram(shaderProgramObject);

		
			//matrix transformation
			Matrix.translateM(translationMatrix, 0,trans_i1, 0.0f, -3.0f);
			Matrix.multiplyMM(modelViewMatrix, 0,
							  translationMatrix, 0,
							  modelViewMatrix, 0);
			Matrix.multiplyMM(modelViewProjectionMatrix, 0,
							  perspectiveProjectionMatrix, 0,
							  modelViewMatrix, 0);

			//send matrix to shader
			GLES32.glUniformMatrix4fv(mvpUniform, 1, false, modelViewProjectionMatrix, 0);
			GLES32.glUniform1f(alphaUniform, 1.0f);

			GLES32.glLineWidth(50.0f);
			// ******************* I ************************
			GLES32.glBindVertexArray(vao_i1[0]);
			GLES32.glDrawArrays(GLES32.GL_LINES, 0, 6);
			//unbind vao
			GLES32.glBindVertexArray(0);
			// **********************************************
			
			GLES32.glUseProgram(0);
		}
		if (translateN == true)
		{
			GLES32.glUseProgram(shaderProgramObject);

			//making martices identity
			Matrix.setIdentityM(modelViewMatrix, 0);
			Matrix.setIdentityM(modelViewProjectionMatrix, 0);
			Matrix.setIdentityM(translationMatrix, 0);
			Matrix.setIdentityM(rotationMatrix, 0);
			
			//matrix transformation
			Matrix.translateM(translationMatrix, 0, 0.0f, trans_n, -3.0f);
			Matrix.multiplyMM(modelViewMatrix, 0,
							  translationMatrix, 0,
							  modelViewMatrix, 0);
			Matrix.multiplyMM(modelViewProjectionMatrix, 0,
							  perspectiveProjectionMatrix, 0,
							  modelViewMatrix, 0);

			//send matrix to shader
			GLES32.glUniformMatrix4fv(mvpUniform, 1, false, modelViewProjectionMatrix, 0);
			GLES32.glUniform1f(alphaUniform, 1.0f);

			// ******************* N ************************
			GLES32.glBindVertexArray(vao_n[0]);
			GLES32.glDrawArrays(GLES32.GL_LINES, 0, 6);
			//unbind vao
			GLES32.glBindVertexArray(0);
			// **********************************************
			GLES32.glUseProgram(0);
		}
		if(translateI2)
		{
			GLES32.glUseProgram(shaderProgramObject);

			//making martices identity
			Matrix.setIdentityM(modelViewMatrix, 0);
			Matrix.setIdentityM(modelViewProjectionMatrix, 0);
			Matrix.setIdentityM(translationMatrix, 0);
			Matrix.setIdentityM(rotationMatrix, 0);
			
			//matrix transformation
			Matrix.translateM(translationMatrix, 0, 0.0f, trans_i2, -3.0f);
			Matrix.multiplyMM(modelViewMatrix, 0,
							  translationMatrix, 0,
							  modelViewMatrix, 0);
			Matrix.multiplyMM(modelViewProjectionMatrix, 0,
							  perspectiveProjectionMatrix, 0,
							  modelViewMatrix, 0);

			//send matrix to shader
			GLES32.glUniformMatrix4fv(mvpUniform, 1, false, modelViewProjectionMatrix, 0);
			GLES32.glUniform1f(alphaUniform, 1.0f);
			
			// ******************* I ************************
			GLES32.glBindVertexArray(vao_i2[0]);
			GLES32.glDrawArrays(GLES32.GL_LINES, 0, 6);
			//unbind vao
			GLES32.glBindVertexArray(0);
			// **********************************************

			
			GLES32.glUseProgram(0);
		}
		if(translateA)
		{
			GLES32.glUseProgram(shaderProgramObject);

			//making martices identity
			Matrix.setIdentityM(modelViewMatrix, 0);
			Matrix.setIdentityM(modelViewProjectionMatrix, 0);
			Matrix.setIdentityM(translationMatrix, 0);
			Matrix.setIdentityM(rotationMatrix, 0);
			
			//matrix transformation
			Matrix.translateM(translationMatrix, 0, trans_a, 0.0f, -3.0f);
			Matrix.multiplyMM(modelViewMatrix, 0,
							  translationMatrix, 0,
							  modelViewMatrix, 0);
			Matrix.multiplyMM(modelViewProjectionMatrix, 0,
							  perspectiveProjectionMatrix, 0,
							  modelViewMatrix, 0);

			//send matrix to shader
			GLES32.glUniformMatrix4fv(mvpUniform, 1, false, modelViewProjectionMatrix, 0);
			GLES32.glUniform1f(alphaUniform, 1.0f);

			// ******************* A ************************
			GLES32.glBindVertexArray(vao_a[0]);
			GLES32.glDrawArrays(GLES32.GL_LINES, 0, 4);
			//unbind vao
			GLES32.glBindVertexArray(0);
			// **********************************************

			GLES32.glUseProgram(0);			
		}
		if(translateD)
		{
			GLES32.glUseProgram(shaderProgramObject);

			//making martices identity
			Matrix.setIdentityM(modelViewMatrix, 0);
			Matrix.setIdentityM(modelViewProjectionMatrix, 0);
			Matrix.setIdentityM(translationMatrix, 0);
			Matrix.setIdentityM(rotationMatrix, 0);
			
			//matrix transformation
			Matrix.translateM(translationMatrix, 0, 0.0f, 0.0f, -3.0f);
			Matrix.multiplyMM(modelViewMatrix, 0,
							  translationMatrix, 0,
							  modelViewMatrix, 0);
			Matrix.multiplyMM(modelViewProjectionMatrix, 0,
							  perspectiveProjectionMatrix, 0,
							  modelViewMatrix, 0);

			//send matrix to shader
			GLES32.glUniformMatrix4fv(mvpUniform, 1, false, modelViewProjectionMatrix, 0);
			GLES32.glUniform1f(alphaUniform, alphaValue);

			// ******************* D ************************
			GLES32.glEnable(GLES32.GL_BLEND);
			GLES32.glBlendFunc(GLES32.GL_SRC_ALPHA, GLES32.GL_ONE_MINUS_SRC_ALPHA);
			GLES32.glBindVertexArray(vao_d[0]);
			GLES32.glDrawArrays(GLES32.GL_LINES, 0, 8);
			//unbind vao
			GLES32.glBindVertexArray(0);
			GLES32.glDisable(GLES32.GL_BLEND);
			// **********************************************
			
			GLES32.glUseProgram(0);
		}
		if (translatePlane == true)
		{

			// // ***************** DRAW PATHS ******************

			// // --------------------- PATH 1 ---------------------
			// // PATH 1 - SAFFRON
			// GLES32.glUseProgram(shaderProgramObject_part2);

			// //making martices identity
			// Matrix.setIdentityM(modelViewMatrix, 0);
			// Matrix.setIdentityM(modelViewProjectionMatrix, 0);
			// Matrix.setIdentityM(translationMatrix, 0);
			// Matrix.setIdentityM(rotationMatrix, 0);
			
			// //matrix transformation
			// Matrix.translateM(translationMatrix, 0, 0.0f, 0.03f, -2.5f);
			// Matrix.multiplyMM(modelViewMatrix, 0,
			// 				  translationMatrix, 0,
			// 				  modelViewMatrix, 0);
			// Matrix.multiplyMM(modelViewProjectionMatrix, 0,
			// 				  perspectiveProjectionMatrix, 0,
			// 				  modelViewMatrix, 0);

			// //send matrix to shader
			// GLES32.glUniformMatrix4fv(mvpUniform_part2, 1, false, modelViewProjectionMatrix, 0);
			// if (pointCount < 490)
			// 	GLES32.glUniform1f(alphaUniform_part2, 1.0f);
			// else
			// 	GLES32.glUniform1f(alphaUniform_part2, alphaValue_part2);
			// GLES32.glUniform1f(colorUniform_r_part2, 1.0f);
			// GLES32.glUniform1f(colorUniform_g_part2, 0.6f);
			// GLES32.glUniform1f(colorUniform_b_part2, 0.2f);

			// GLES32.glEnable(GLES32.GL_BLEND);
			// GLES32.glBlendFunc(GLES32.GL_SRC_ALPHA, GLES32.GL_ONE_MINUS_SRC_ALPHA);
			// GLES32.glBindVertexArray(vao_path1[0]);
			// GLES32.glDrawArrays(GLES32.GL_POINTS, 0, pointCount);
			// GLES32.glBindVertexArray(0);
			// GLES32.glDisable(GLES32.GL_BLEND);

			// // Path 1 - WHITE
			// //making martices identity
			// Matrix.setIdentityM(modelViewMatrix, 0);
			// Matrix.setIdentityM(modelViewProjectionMatrix, 0);
			// Matrix.setIdentityM(translationMatrix, 0);
			// Matrix.setIdentityM(rotationMatrix, 0);
			
			// //matrix transformation
			// Matrix.translateM(translationMatrix, 0, 0.0f, 0.0f, -2.5f);
			// Matrix.multiplyMM(modelViewMatrix, 0,
			// 				  translationMatrix, 0,
			// 				  modelViewMatrix, 0);
			// Matrix.multiplyMM(modelViewProjectionMatrix, 0,
			// 				  perspectiveProjectionMatrix, 0,
			// 				  modelViewMatrix, 0);

			// //send matrix to shader
			// GLES32.glUniformMatrix4fv(mvpUniform_part2, 1, false, modelViewProjectionMatrix, 0);
			// if (pointCount < 490)
			// 	GLES32.glUniform1f(alphaUniform_part2, 1.0f);
			// else
			// 	GLES32.glUniform1f(alphaUniform_part2, alphaValue_part2);
			// GLES32.glUniform1f(colorUniform_r_part2, 1.0f);
			// GLES32.glUniform1f(colorUniform_g_part2, 1.0f);
			// GLES32.glUniform1f(colorUniform_b_part2, 1.0f);

			// GLES10.glPointSize(10.0f);
			// GLES32.glEnable(GLES32.GL_BLEND);
			// GLES32.glBlendFunc(GLES32.GL_SRC_ALPHA, GLES32.GL_ONE_MINUS_SRC_ALPHA);
			// GLES32.glBindVertexArray(vao_path1[0]);
			// GLES32.glDrawArrays(GLES32.GL_POINTS, 0, pointCount);
			// GLES32.glBindVertexArray(0);
			// GLES32.glDisable(GLES32.GL_BLEND);

			// // Path 1 - GREEN
			// //making martices identity
			// Matrix.setIdentityM(modelViewMatrix, 0);
			// Matrix.setIdentityM(modelViewProjectionMatrix, 0);
			// Matrix.setIdentityM(translationMatrix, 0);
			// Matrix.setIdentityM(rotationMatrix, 0);
			
			// //matrix transformation
			// Matrix.translateM(translationMatrix, 0, 0.0f, -0.03f, -2.5f);
			// Matrix.multiplyMM(modelViewMatrix, 0,
			// 				  translationMatrix, 0,
			// 				  modelViewMatrix, 0);
			// Matrix.multiplyMM(modelViewProjectionMatrix, 0,
			// 				  perspectiveProjectionMatrix, 0,
			// 				  modelViewMatrix, 0);

			// //send matrix to shader
			// GLES32.glUniformMatrix4fv(mvpUniform_part2, 1, false, modelViewProjectionMatrix, 0);
			// if (pointCount < 490)
			// 	GLES32.glUniform1f(alphaUniform_part2, 1.0f);
			// else
			// 	GLES32.glUniform1f(alphaUniform_part2, alphaValue_part2);
			// GLES32.glUniform1f(colorUniform_r_part2, 0.07f);
			// GLES32.glUniform1f(colorUniform_g_part2, 0.533f);
			// GLES32.glUniform1f(colorUniform_b_part2, 0.027f);

			// GLES10.glPointSize(10.0f);
			// GLES32.glEnable(GLES32.GL_BLEND);
			// GLES32.glBlendFunc(GLES32.GL_SRC_ALPHA, GLES32.GL_ONE_MINUS_SRC_ALPHA);
			// GLES32.glBindVertexArray(vao_path1[0]);
			// GLES32.glDrawArrays(GLES32.GL_POINTS, 0, pointCount);
			// GLES32.glBindVertexArray(0);
			// GLES32.glDisable(GLES32.GL_BLEND);

			// GLES32.glUseProgram(0);
			// // --------------------- PATH 1 ENDS ---------------------

			// // -------------------- PATH 3 STARTS --------------------
			// // PATH 3 - SAFFRON
			// GLES32.glUseProgram(shaderProgramObject_part2);

			// //making martices identity
			// Matrix.setIdentityM(modelViewMatrix, 0);
			// Matrix.setIdentityM(modelViewProjectionMatrix, 0);
			// Matrix.setIdentityM(translationMatrix, 0);
			// Matrix.setIdentityM(rotationMatrix, 0);
			
			// //matrix transformation
			// Matrix.translateM(translationMatrix, 0, 0.0f, 0.03f, -2.5f);
			// Matrix.multiplyMM(modelViewMatrix, 0,
			// 				  translationMatrix, 0,
			// 				  modelViewMatrix, 0);
			// Matrix.multiplyMM(modelViewProjectionMatrix, 0,
			// 				  perspectiveProjectionMatrix, 0,
			// 				  modelViewMatrix, 0);

			// //send matrix to shader
			// GLES32.glUniformMatrix4fv(mvpUniform_part2, 1, false, modelViewProjectionMatrix, 0);
			// if (pointCount < 490)
			// 	GLES32.glUniform1f(alphaUniform_part2, 1.0f);
			// else
			// 	GLES32.glUniform1f(alphaUniform_part2, alphaValue_part2);
			// GLES32.glUniform1f(colorUniform_r_part2, 1.0f);
			// GLES32.glUniform1f(colorUniform_g_part2, 0.6f);
			// GLES32.glUniform1f(colorUniform_b_part2, 0.2f);

			// GLES32.glEnable(GLES10.GL_POINT_SMOOTH);
			// GLES10.glPointSize(10.0f);
			// GLES32.glEnable(GLES32.GL_BLEND);
			// GLES32.glBlendFunc(GLES32.GL_SRC_ALPHA, GLES32.GL_ONE_MINUS_SRC_ALPHA);
			// GLES32.glBindVertexArray(vao_path3[0]);
			// GLES32.glDrawArrays(GLES32.GL_POINTS, 0, pointCount);
			// GLES32.glBindVertexArray(0);
			// GLES32.glDisable(GLES32.GL_BLEND);
			// GLES32.glDisable(GLES10.GL_POINT_SMOOTH);

			// // Path 3 - WHITE
			// //making martices identity
			// Matrix.setIdentityM(modelViewMatrix, 0);
			// Matrix.setIdentityM(modelViewProjectionMatrix, 0);
			// Matrix.setIdentityM(translationMatrix, 0);
			// Matrix.setIdentityM(rotationMatrix, 0);
			
			// //matrix transformation
			// Matrix.translateM(translationMatrix, 0, 0.0f, 0.0f, -2.5f);
			// Matrix.multiplyMM(modelViewMatrix, 0,
			// 				  translationMatrix, 0,
			// 				  modelViewMatrix, 0);
			// Matrix.multiplyMM(modelViewProjectionMatrix, 0,
			// 				  perspectiveProjectionMatrix, 0,
			// 				  modelViewMatrix, 0);

			// //send matrix to shader
			// GLES32.glUniformMatrix4fv(mvpUniform_part2, 1, false, modelViewProjectionMatrix, 0);
			// if (pointCount < 490)
			// 	GLES32.glUniform1f(alphaUniform_part2, 1.0f);
			// else
			// 	GLES32.glUniform1f(alphaUniform_part2, alphaValue_part2);
			// GLES32.glUniform1f(colorUniform_r_part2, 1.0f);
			// GLES32.glUniform1f(colorUniform_g_part2, 1.0f);
			// GLES32.glUniform1f(colorUniform_b_part2, 1.0f);

			// GLES32.glEnable(GLES10.GL_POINT_SMOOTH);
			// GLES10.glPointSize(10.0f);
			// GLES32.glEnable(GLES32.GL_BLEND);
			// GLES32.glBlendFunc(GLES32.GL_SRC_ALPHA, GLES32.GL_ONE_MINUS_SRC_ALPHA);
			// GLES32.glBindVertexArray(vao_path3[0]);
			// GLES32.glDrawArrays(GLES32.GL_POINTS, 0, pointCount);
			// GLES32.glBindVertexArray(0);
			// GLES32.glDisable(GLES32.GL_BLEND);
			// GLES32.glDisable(GLES10.GL_POINT_SMOOTH);

			// // Path 3 - GREEN
			// //making martices identity
			// Matrix.setIdentityM(modelViewMatrix, 0);
			// Matrix.setIdentityM(modelViewProjectionMatrix, 0);
			// Matrix.setIdentityM(translationMatrix, 0);
			// Matrix.setIdentityM(rotationMatrix, 0);
			
			// //matrix transformation
			// Matrix.translateM(translationMatrix, 0, 0.0f, -0.03f, -2.5f);
			// Matrix.multiplyMM(modelViewMatrix, 0,
			// 				  translationMatrix, 0,
			// 				  modelViewMatrix, 0);
			// Matrix.multiplyMM(modelViewProjectionMatrix, 0,
			// 				  perspectiveProjectionMatrix, 0,
			// 				  modelViewMatrix, 0);

			// //send matrix to shader
			// GLES32.glUniformMatrix4fv(mvpUniform_part2, 1, false, modelViewProjectionMatrix, 0);
			// if (pointCount < 490)
			// 	GLES32.glUniform1f(alphaUniform_part2, 1.0f);
			// else
			// 	GLES32.glUniform1f(alphaUniform_part2, alphaValue_part2);
			// GLES32.glUniform1f(colorUniform_r_part2, 0.07f);
			// GLES32.glUniform1f(colorUniform_g_part2, 0.533f);
			// GLES32.glUniform1f(colorUniform_b_part2, 0.027f);

			// GLES32.glEnable(GLES10.GL_POINT_SMOOTH);
			// GLES10.glPointSize(10.0f);
			// GLES32.glEnable(GLES32.GL_BLEND);
			// GLES32.glBlendFunc(GLES32.GL_SRC_ALPHA, GLES32.GL_ONE_MINUS_SRC_ALPHA);
			// GLES32.glBindVertexArray(vao_path3[0]);
			// GLES32.glDrawArrays(GLES32.GL_POINTS, 0, pointCount);
			// GLES32.glBindVertexArray(0);
			// GLES32.glDisable(GLES32.GL_BLEND);
			// GLES32.glDisable(GLES10.GL_POINT_SMOOTH);

			// GLES32.glUseProgram(0);
			// // --------------------- PATH 3 ENDS ---------------------

			// // -------------------- PATH 2 STARTS --------------------
			// // PATH 2 - SAFFRON
			// GLES32.glUseProgram(shaderProgramObject_part2);

			// //making martices identity
			// Matrix.setIdentityM(modelViewMatrix, 0);
			// Matrix.setIdentityM(modelViewProjectionMatrix, 0);
			// Matrix.setIdentityM(translationMatrix, 0);
			// Matrix.setIdentityM(rotationMatrix, 0);
			
			// //matrix transformation
			// Matrix.translateM(translationMatrix, 0, 0.0f, 0.03f, -2.5f);
			// Matrix.multiplyMM(modelViewMatrix, 0,
			// 				  translationMatrix, 0,
			// 				  modelViewMatrix, 0);
			// Matrix.multiplyMM(modelViewProjectionMatrix, 0,
			// 				  perspectiveProjectionMatrix, 0,
			// 				  modelViewMatrix, 0);

			// //send matrix to shader
			// GLES32.glUniformMatrix4fv(mvpUniform_part2, 1, false, modelViewProjectionMatrix, 0);
			// if (pointCount < 490)
			// 	GLES32.glUniform1f(alphaUniform_part2, 1.0f);
			// else
			// 	GLES32.glUniform1f(alphaUniform_part2, alphaValue_part2);
			// GLES32.glUniform1f(colorUniform_r_part2, 1.0f);
			// GLES32.glUniform1f(colorUniform_g_part2, 0.6f);
			// GLES32.glUniform1f(colorUniform_b_part2, 0.2f);

			// GLES32.glEnable(GLES10.GL_POINT_SMOOTH);
			// GLES10.glPointSize(5.0f);
			// GLES32.glEnable(GLES32.GL_BLEND);
			// GLES32.glBlendFunc(GLES32.GL_SRC_ALPHA, GLES32.GL_ONE_MINUS_SRC_ALPHA);
			// GLES32.glBindVertexArray(vao_path2[0]);
			// GLES32.glDrawArrays(GLES32.GL_POINTS, 0, pointCount);
			// GLES32.glBindVertexArray(0);
			// GLES32.glDisable(GLES32.GL_BLEND);
			// GLES32.glDisable(GLES10.GL_POINT_SMOOTH);

			// // Path 3 - WHITE
			// //making martices identity
			// Matrix.setIdentityM(modelViewMatrix, 0);
			// Matrix.setIdentityM(modelViewProjectionMatrix, 0);
			// Matrix.setIdentityM(translationMatrix, 0);
			// Matrix.setIdentityM(rotationMatrix, 0);
			
			// //matrix transformation
			// Matrix.translateM(translationMatrix, 0, 0.0f, 0.0f, -2.5f);
			// Matrix.multiplyMM(modelViewMatrix, 0,
			// 				  translationMatrix, 0,
			// 				  modelViewMatrix, 0);
			// Matrix.multiplyMM(modelViewProjectionMatrix, 0,
			// 				  perspectiveProjectionMatrix, 0,
			// 				  modelViewMatrix, 0);

			// //send matrix to shader
			// GLES32.glUniformMatrix4fv(mvpUniform_part2, 1, false, modelViewProjectionMatrix, 0);
			// if (pointCount < 490)
			// 	GLES32.glUniform1f(alphaUniform_part2, 1.0f);
			// else
			// 	GLES32.glUniform1f(alphaUniform_part2, alphaValue_part2);
			// GLES32.glUniform1f(colorUniform_r_part2, 1.0f);
			// GLES32.glUniform1f(colorUniform_g_part2, 1.0f);
			// GLES32.glUniform1f(colorUniform_b_part2, 1.0f);
			
			// GLES32.glEnable(GLES10.GL_POINT_SMOOTH);
			// GLES10.glPointSize(10.0f);
			// GLES32.glEnable(GLES32.GL_BLEND);
			// GLES32.glBlendFunc(GLES32.GL_SRC_ALPHA, GLES32.GL_ONE_MINUS_SRC_ALPHA);
			// GLES32.glBindVertexArray(vao_path2[0]);
			// GLES32.glDrawArrays(GLES32.GL_POINTS, 0, pointCount);
			// GLES32.glBindVertexArray(0);
			// GLES32.glDisable(GLES32.GL_BLEND);
			// GLES32.glDisable(GLES10.GL_POINT_SMOOTH);

			// // Path 3 - GREEN
			// //making martices identity
			// Matrix.setIdentityM(modelViewMatrix, 0);
			// Matrix.setIdentityM(modelViewProjectionMatrix, 0);
			// Matrix.setIdentityM(translationMatrix, 0);
			// Matrix.setIdentityM(rotationMatrix, 0);
			
			// //matrix transformation
			// Matrix.translateM(translationMatrix, 0, 0.0f, -0.03f, -2.5f);
			// Matrix.multiplyMM(modelViewMatrix, 0,
			// 				  translationMatrix, 0,
			// 				  modelViewMatrix, 0);
			// Matrix.multiplyMM(modelViewProjectionMatrix, 0,
			// 				  perspectiveProjectionMatrix, 0,
			// 				  modelViewMatrix, 0);

			// //send matrix to shader
			// GLES32.glUniformMatrix4fv(mvpUniform_part2, 1, false, modelViewProjectionMatrix, 0);
			// if (pointCount < 490)
			// 	GLES32.glUniform1f(alphaUniform_part2, 1.0f);
			// else
			// 	GLES32.glUniform1f(alphaUniform_part2, alphaValue_part2);
			// GLES32.glUniform1f(colorUniform_r_part2, 0.07f);
			// GLES32.glUniform1f(colorUniform_g_part2, 0.533f);
			// GLES32.glUniform1f(colorUniform_b_part2, 0.027f);
			
			// GLES32.glEnable(GLES10.GL_POINT_SMOOTH);
			// GLES10.glPointSize(10.0f);
			// GLES32.glEnable(GLES32.GL_BLEND);
			// GLES32.glBlendFunc(GLES32.GL_SRC_ALPHA, GLES32.GL_ONE_MINUS_SRC_ALPHA);
			// GLES32.glBindVertexArray(vao_path2[0]);
			// GLES32.glDrawArrays(GLES32.GL_POINTS, 0, pointCount);
			// GLES32.glBindVertexArray(0);
			// GLES32.glDisable(GLES32.GL_BLEND);
			// GLES32.glDisable(GLES10.GL_POINT_SMOOTH);

			// GLES32.glUseProgram(0);
			// // --------------------- PATH 2 ENDS ---------------------

			// ******************* PLANE 1 *********************
			GLES32.glUseProgram(shaderProgramObject);
			
			//making martices identity
			Matrix.setIdentityM(modelViewMatrix, 0);
			Matrix.setIdentityM(modelViewProjectionMatrix, 0);
			Matrix.setIdentityM(translationMatrix, 0);
			Matrix.setIdentityM(rotationMatrix, 0);
			
			//matrix transformation
			Matrix.translateM(translationMatrix, 0, path1[ip], path1[ip + 1], -2.5f);
			if(pointCount < 100)
				Matrix.rotateM(rotationMatrix, 0, angle1, 0.0f, 0.0f, 1.0f);
			else if(pointCount < 400)
				Matrix.setIdentityM(rotationMatrix, 0);
			else if(pointCount < 490)
				Matrix.rotateM(rotationMatrix, 0, angle1, 0.0f, 0.0f, 1.0f);

			Matrix.multiplyMM(modelViewMatrix, 0,
							  rotationMatrix, 0,
							  modelViewMatrix, 0);
			
			Matrix.multiplyMM(modelViewMatrix, 0,
							  translationMatrix, 0,
							  modelViewMatrix, 0);

			Matrix.multiplyMM(modelViewProjectionMatrix, 0,
							  perspectiveProjectionMatrix, 0,
							  modelViewMatrix, 0);
			
			//send matrix to shader
			GLES32.glUniformMatrix4fv(mvpUniform, 1, false, modelViewProjectionMatrix, 0);
			if (pointCount < 490)
				GLES32.glUniform1f(alphaUniform, 1.0f);
			else
				GLES32.glUniform1f(alphaUniform, 0.0f);

			// ******************* PLANE 1 *********************
			GLES32.glLineWidth(2.0f);
			GLES32.glEnable(GLES32.GL_BLEND);
			GLES32.glBlendFunc(GLES32.GL_SRC_ALPHA, GLES32.GL_ONE_MINUS_SRC_ALPHA);
			GLES32.glBindVertexArray(vao_plane[0]);
			GLES32.glDrawArrays(GLES32.GL_TRIANGLES, 0, 3);
			GLES32.glDrawArrays(GLES32.GL_TRIANGLE_FAN, 3, 4);
			GLES32.glDrawArrays(GLES32.GL_TRIANGLE_FAN, 7, 4);
			GLES32.glDrawArrays(GLES32.GL_TRIANGLE_FAN, 11, 4);
			GLES32.glDrawArrays(GLES32.GL_TRIANGLE_FAN, 15, 4);
			GLES32.glDrawArrays(GLES32.GL_LINES, 19, 20);
			GLES32.glDrawArrays(GLES32.GL_TRIANGLE_FAN, 39, 4);
			GLES32.glDrawArrays(GLES32.GL_TRIANGLE_FAN, 43, 4);
			GLES32.glDrawArrays(GLES32.GL_TRIANGLE_FAN, 47, 4);

			GLES32.glBindVertexArray(0);
			GLES32.glDisable(GLES32.GL_BLEND);
			// ******************* PLANE 1 ends ******************

			// ******************* PLANE 2 *********************
			
			//making martices identity
			Matrix.setIdentityM(modelViewMatrix, 0);
			Matrix.setIdentityM(modelViewProjectionMatrix, 0);
			Matrix.setIdentityM(translationMatrix, 0);
			Matrix.setIdentityM(rotationMatrix, 0);
			
			//matrix transformation
			Matrix.translateM(translationMatrix, 0, path2[ip], path2[ip + 1], -2.50f);

			Matrix.multiplyMM(modelViewMatrix, 0,
							  translationMatrix, 0,
							  modelViewMatrix, 0);

			Matrix.multiplyMM(modelViewProjectionMatrix, 0,
							  perspectiveProjectionMatrix, 0,
							  modelViewMatrix, 0);
			
			//send matrix to shader
			GLES32.glUniformMatrix4fv(mvpUniform, 1, false, modelViewProjectionMatrix, 0);
			if (pointCount < 490)
				GLES32.glUniform1f(alphaUniform, 1.0f);
			else
				GLES32.glUniform1f(alphaUniform, 0.0f);

			// ******************* PLANE 2 *********************
			GLES32.glLineWidth(2.0f);
			GLES32.glEnable(GLES32.GL_BLEND);
			GLES32.glBlendFunc(GLES32.GL_SRC_ALPHA, GLES32.GL_ONE_MINUS_SRC_ALPHA);
			GLES32.glBindVertexArray(vao_plane[0]);
			GLES32.glDrawArrays(GLES32.GL_TRIANGLES, 0, 3);
			GLES32.glDrawArrays(GLES32.GL_TRIANGLE_FAN, 3, 4);
			GLES32.glDrawArrays(GLES32.GL_TRIANGLE_FAN, 7, 4);
			GLES32.glDrawArrays(GLES32.GL_TRIANGLE_FAN, 11, 4);
			GLES32.glDrawArrays(GLES32.GL_TRIANGLE_FAN, 15, 4);
			GLES32.glDrawArrays(GLES32.GL_LINES, 19, 20);
			GLES32.glDrawArrays(GLES32.GL_TRIANGLE_FAN, 39, 4);
			GLES32.glDrawArrays(GLES32.GL_TRIANGLE_FAN, 43, 4);
			GLES32.glDrawArrays(GLES32.GL_TRIANGLE_FAN, 47, 4);

			GLES32.glBindVertexArray(0);
			GLES32.glDisable(GLES32.GL_BLEND);
			// ******************* PLANE 2 ends ******************

			// ******************* PLANE 3 *********************
			
			//making martices identity
			Matrix.setIdentityM(modelViewMatrix, 0);
			Matrix.setIdentityM(modelViewProjectionMatrix, 0);
			Matrix.setIdentityM(translationMatrix, 0);
			Matrix.setIdentityM(rotationMatrix, 0);
			
			//matrix transformation
			Matrix.translateM(translationMatrix, 0, path3[ip], path3[ip + 1], -2.5f);
			if(pointCount < 100)
				Matrix.rotateM(rotationMatrix, 0, angle2, 0.0f, 0.0f, 1.0f);
			else if(pointCount < 400)
				Matrix.setIdentityM(rotationMatrix, 0);
			else if(pointCount < 490)
				Matrix.rotateM(rotationMatrix, 0, angle2, 0.0f, 0.0f, 1.0f);

			Matrix.multiplyMM(modelViewMatrix, 0,
							  rotationMatrix, 0,
							  modelViewMatrix, 0);

			Matrix.multiplyMM(modelViewMatrix, 0,
							  translationMatrix, 0,
							  modelViewMatrix, 0);
							  
			Matrix.multiplyMM(modelViewProjectionMatrix, 0,
							  perspectiveProjectionMatrix, 0,
							  modelViewMatrix, 0);
			
			//send matrix to shader
			GLES32.glUniformMatrix4fv(mvpUniform, 1, false, modelViewProjectionMatrix, 0);
			if (pointCount < 490)
				GLES32.glUniform1f(alphaUniform, 1.0f);
			else
				GLES32.glUniform1f(alphaUniform, 0.0f);

			// ******************* PLANE 3 *********************
			GLES32.glLineWidth(2.0f);
			GLES32.glEnable(GLES32.GL_BLEND);
			GLES32.glBlendFunc(GLES32.GL_SRC_ALPHA, GLES32.GL_ONE_MINUS_SRC_ALPHA);
			GLES32.glBindVertexArray(vao_plane[0]);
			GLES32.glDrawArrays(GLES32.GL_TRIANGLES, 0, 3);
			GLES32.glDrawArrays(GLES32.GL_TRIANGLE_FAN, 3, 4);
			GLES32.glDrawArrays(GLES32.GL_TRIANGLE_FAN, 7, 4);
			GLES32.glDrawArrays(GLES32.GL_TRIANGLE_FAN, 11, 4);
			GLES32.glDrawArrays(GLES32.GL_TRIANGLE_FAN, 15, 4);
			GLES32.glDrawArrays(GLES32.GL_LINES, 19, 20);
			GLES32.glDrawArrays(GLES32.GL_TRIANGLE_FAN, 39, 4);
			GLES32.glDrawArrays(GLES32.GL_TRIANGLE_FAN, 43, 4);
			GLES32.glDrawArrays(GLES32.GL_TRIANGLE_FAN, 47, 4);

			GLES32.glBindVertexArray(0);
			GLES32.glDisable(GLES32.GL_BLEND);
			// ******************* PLANE 3 ends ******************

			GLES32.glUseProgram(0);
		}

		if(showFlag)
		{
			GLES32.glUseProgram(shaderProgramObject);

			//making martices identity
			Matrix.setIdentityM(modelViewMatrix, 0);
			Matrix.setIdentityM(modelViewProjectionMatrix, 0);
			Matrix.setIdentityM(translationMatrix, 0);
			Matrix.setIdentityM(rotationMatrix, 0);
			
			//do necessary transformation
			Matrix.translateM(translationMatrix, 0,0.0f, 0.0f, -3.0f);

			Matrix.multiplyMM(modelViewMatrix, 0,
							  translationMatrix, 0,
							  modelViewMatrix, 0);
			
			Matrix.multiplyMM(modelViewProjectionMatrix, 0,
							  perspectiveProjectionMatrix, 0,
							  modelViewMatrix, 0);
			
			// send necessary matrices to shaders as uniforms
			GLES32.glUniformMatrix4fv(mvpUniform, 1, false, modelViewProjectionMatrix, 0);
			GLES32.glUniform1f(alphaUniform, 1.0f);

			// ***************** FLAG ***********************
			GLES32.glBindVertexArray(vao_flag[0]);
			GLES32.glDrawArrays(GLES32.GL_TRIANGLE_FAN, 0, 4);
			GLES32.glDrawArrays(GLES32.GL_TRIANGLE_FAN, 4, 4);
			GLES32.glDrawArrays(GLES32.GL_TRIANGLE_FAN, 8, 4);
			//unbind vao
			GLES32.glBindVertexArray(0);
			// **********************************************

			//unuse program
			GLES32.glUseProgram(0);
		}
        requestRender();
		update();
    }

}
