package rtr.example.deathlyhallows;

//packages added by me
import android.content.Context;
import android.graphics.Color;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.GestureDetector;
import android.view.GestureDetector.OnGestureListener;
import android.view.GestureDetector.OnDoubleTapListener;
import rtr.example.deathlyhallows.GLESMacros;

import android.opengl.GLSurfaceView;
import android.opengl.GLES32;

import javax.microedition.khronos.opengles.GL10;
import javax.microedition.khronos.egl.EGLConfig;

//for opengl buffers
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;
import java.lang.Math;

//for matrix mathematics
import android.opengl.Matrix;

public class GLESView extends GLSurfaceView implements GLSurfaceView.Renderer, OnGestureListener, OnDoubleTapListener{
    private GestureDetector gestureDetector;
    private final Context context;
	private int vertexShaderObject;
	private int fragmentShaderObject;
	private int shaderProgramObject;
	private int mvpUniform;

	private float angleTriangle = 0.0f;
	private float angleCircle = 0.0f;
	private float angleline = 0.0f;
	private float trans_x = 2.0f;
	private float trans_y = -2.0f;
	private float tline_y = 2.0f;
	private float tcirc_x = -2.0f;
	private float tcirc_y = -2.0f;

	private float[] InCenter= new float[3];
	private float[] perspectiveProjectionMatrix = new float[16];		//4x4 matrix
	private float[] circleVertices = new float[1100];
	private float[] lineVertices = new float[6];

	private int[] vao_triangle = new int[1];
	private int[] vbo_position_triangle = new int[1];
	private int[] vao_circle = new int[1];
	private int[] vbo_position_circle = new int[1];
	private int[] vao_line = new int[1];
	private int[] vbo_position_line = new int[1];

	boolean triangle = true;
	boolean line = false;
	boolean circle = false;
    
	//constructor
    public GLESView(Context drawingContext){
        super(drawingContext);
        context = drawingContext;
        
        setEGLContextClientVersion(3);
        setRenderer(this);
        setRenderMode(GLSurfaceView.RENDERMODE_WHEN_DIRTY);

        gestureDetector =new GestureDetector(drawingContext, this, null, false );
        gestureDetector.setOnDoubleTapListener(this);
    }

    //handling 'onTouchEvent' is the most important,
	//because it triggers all gesture and tap events
	@Override
	public boolean onTouchEvent(MotionEvent event)
	{
		//code
		int eventaction = event.getAction();
		if(!gestureDetector.onTouchEvent(event))
			super.onTouchEvent(event);
		return(true);
	}

    //abstract method from OnDoubleTapListener so must be implemented
	@Override
	public boolean onDoubleTap(MotionEvent e)
	{
		//setText("Double Tap");
		return(true);
    }
    
    //abstract method from OnDoubleTapListener so must be implemented
	@Override
	public boolean onDoubleTapEvent(MotionEvent e)
	{
		//do not write any code here because already written in 'onDoubleTap'
		return(true);
    }
    
    //abstract  method from OnDoubleTapListener so must be implemented
	@Override
	public boolean onSingleTapConfirmed(MotionEvent e)
	{
	//	setText("Single Tap");
		return(true);
    }
    
    //abstract  method from OnGestureListener so must be implemented
	@Override
	public boolean onDown(MotionEvent e)
	{
		//do not write any code here because already written in 'onSingleTapConfirmed'
		return(true);
    }
    
    //abstract  method from OnGestureListener so must be implemented
	@Override
	public boolean onFling(MotionEvent e1,MotionEvent e2, float velocityX, float velocityY)
	{
		return(true);
	}

	//abstract  method from OnGestureListener so must be implemented
	@Override
	public void onLongPress(MotionEvent e)
	{
		//setText("Long Press");
	}

	//abstract  method from OnGestureListener so must be implemented
	@Override
	public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY)
	{
		//setText("Scroll");
		uninitialize();
		System.exit(0);
		return(true);
	}

	//abstract  method from OnGestureListener so must be implemented
	@Override
	public void onShowPress(MotionEvent e)
	{
	
	}

	//abstract  method from OnGestureListener so must be implemented
	@Override
	public boolean onSingleTapUp(MotionEvent e)
	{
		return(true);
    }
    
    //implement GLSurfaceView.Renderer methods
    @Override
    public void onSurfaceCreated(GL10 gl, EGLConfig config){
		System.out.println("RTR: Called onSurfaceCreated");

        String version = gl.glGetString(GL10.GL_VERSION);
		System.out.println("RTR:" + version);
		
		version = gl.glGetString(GLES32.GL_SHADING_LANGUAGE_VERSION);
		System.out.println("RTR:" + version);
        initialize();
    }

    @Override
    public void onSurfaceChanged(GL10 unused, int width, int height)
    {
		System.out.println("RTR: Called onSurfaceChabged");

        resize(width, height);
    }

    @Override
    public void onDrawFrame(GL10 unused){
        display();
    }

    //our custom methods
	private void uninitialize()
	{
		System.out.println("RTR: Called uninitialize");
		
		if(shaderProgramObject != 0)
		{
			int[] shaderCount = new int[1];
			int shaderNumber;

			GLES32.glUseProgram(shaderProgramObject);

			GLES32.glGetProgramiv(shaderProgramObject, GLES32.GL_ATTACHED_SHADERS, shaderCount, 0);
			System.out.println("RTR: uninitialize => " + shaderCount[0]);
		
			int[] shaders =new int[shaderCount[0]];

			if(shaderCount[0] != 0)
			{
				GLES32.glGetAttachedShaders(shaderProgramObject, shaderCount[0], shaderCount, 0, shaders, 0);
				System.out.println("RTR: uninitialize => if shadercount != 0 ::" + shaderCount[0]);

				for(shaderNumber =0;shaderNumber<shaderCount[0]; shaderNumber++)
				{
					GLES32.glDetachShader(shaderProgramObject, shaders[shaderNumber]);
					GLES32.glDeleteShader(shaders[shaderNumber]);
					shaders[shaderNumber]=0;
				}
			}
			else{
				System.out.println("RTR: uninitialize => shader count is 0");
			}
			GLES32.glDeleteProgram(shaderProgramObject);
			shaderProgramObject = 0;
			GLES32.glUseProgram(0);
		}
		if(vbo_position_line[0] != 0)
		{
			System.out.println("RTR: uninitialize => deleting vbo_position_line");

			GLES32.glDeleteBuffers(1, vbo_position_line, 0);
			vbo_position_line[0] = 0;
		}
		if(vao_line[0] != 0)
		{
			System.out.println("RTR: uninitialize => deleting vao_line");

			GLES32.glDeleteVertexArrays(1, vao_line, 0);
			vao_line[0]=0;
		}
		if(vbo_position_circle[0] != 0)
		{
			System.out.println("RTR: uninitialize => deleting vbo_position_circle");

			GLES32.glDeleteBuffers(1, vbo_position_circle, 0);
			vbo_position_circle[0] = 0;
		}
		if(vao_circle[0] != 0)
		{
			System.out.println("RTR: uninitialize => deleting vao_circle");

			GLES32.glDeleteVertexArrays(1, vao_circle, 0);
			vao_circle[0]=0;
		}

		if(vbo_position_triangle[0] != 0)
		{
			System.out.println("RTR: uninitialize => deleting vbo_position_triangle");

			GLES32.glDeleteBuffers(1, vbo_position_triangle, 0);
			vbo_position_triangle[0] = 0;
		}
		if(vao_triangle[0] != 0)
		{
			System.out.println("RTR: uninitialize => deleting vao_triangle");

			GLES32.glDeleteVertexArrays(1, vao_triangle, 0);
			vao_triangle[0]=0;
		}
	}

    private void initialize()
	{
		System.out.println("RTR: Called initialize");

		vertexShaderObject = GLES32.glCreateShader(GLES32.GL_VERTEX_SHADER);
		final String vertexShaderSourceCode = String.format
		(
			"#version 320 es" +
			"\n" +
			"in vec4 vPosition;" +
			"uniform mat4 u_mvp_matrix;" +
			"void main(void)" +
			"{" +
			"gl_Position = u_mvp_matrix * vPosition;" +
			"}"
		);
		GLES32.glShaderSource(vertexShaderObject, vertexShaderSourceCode);
		GLES32.glCompileShader(vertexShaderObject);
		int[] iShaderCompileStatus = new int[1];
		int[] iInfoLogLength = new int[1];
		String szInfoLog = null;

		GLES32.glGetShaderiv(vertexShaderObject, GLES32.GL_COMPILE_STATUS, iShaderCompileStatus, 0);

		if(iShaderCompileStatus[0] == GLES32.GL_FALSE)
		{
			GLES32.glGetShaderiv(vertexShaderObject, GLES32.GL_INFO_LOG_LENGTH, iInfoLogLength, 0);
			if(iInfoLogLength[0] > 0)
			{
				szInfoLog = GLES32.glGetShaderInfoLog(vertexShaderObject);
				System.out.println("RTR: VERTEX SHADER COMPILATION LOG:" + szInfoLog);
				uninitialize();
				System.exit(0);
			}
		}

		//fragment shader
		/*precision :  we don't need to set presion for desktop application. 
		There are 3 hardware unit present in ARM to perform floating point maths calculation; 
		i.e FPU(floating point unit), SIMD(Single Instruction Multiple Data unit), NEON()
		in embedded power and memory is not a luxury. so ypu can not use any precision anywhere.
		eg. low presision and medium precision for int is acceptable but using high precisio for integer calucaltion 
		is very battery consuming which is not acceptable. High precision calucation were completed from NEON
		Vertex shader is by default highp
		int is lowp or mediump
		in fragment shader , user must specify it, as maximum processing happens in fragment shader*/
		fragmentShaderObject = GLES32.glCreateShader(GLES32.GL_FRAGMENT_SHADER);
		final String fragmentShaderSourceCode = String.format
		(
			"#version 320 es" +
			"\n" +
			"precision highp float;" +
			"out vec4 FragColor;" +
			"void main(void)" +
			"{" +
			"FragColor = vec4(1.0, 1.0, 1.0, 1.0);" +
			"}"
		);

		GLES32.glShaderSource(fragmentShaderObject, fragmentShaderSourceCode);
		GLES32.glCompileShader(fragmentShaderObject);
		iShaderCompileStatus[0] = 0;
		iInfoLogLength[0] = 0;
		szInfoLog = null;

		GLES32.glGetShaderiv(fragmentShaderObject, GLES32.GL_COMPILE_STATUS, iShaderCompileStatus, 0);
		
		if(iShaderCompileStatus[0] == GLES32.GL_FALSE)
		{
			GLES32.glGetShaderiv(fragmentShaderObject, GLES32.GL_INFO_LOG_LENGTH, iInfoLogLength, 0);

			if(iInfoLogLength[0] > 0)
			{
				szInfoLog = GLES32.glGetShaderInfoLog(fragmentShaderObject);
				System.out.println("RTR: FRAGMENT SHADER COMPILATION LOG:" + szInfoLog);
				uninitialize();
				System.exit(0);
			}
		}

		int[] shaderCount = new int[1];

		//shader program
		shaderProgramObject = GLES32.glCreateProgram();

		GLES32.glGetProgramiv(shaderProgramObject, GLES32.GL_ATTACHED_SHADERS, shaderCount, 0);
		System.out.println("RTR: initialize => BEFORE ATTACHING SHADERS shader count is :: " + shaderCount[0]);

		GLES32.glAttachShader(shaderProgramObject, vertexShaderObject);
		GLES32.glAttachShader(shaderProgramObject, fragmentShaderObject);

		GLES32.glGetProgramiv(shaderProgramObject, GLES32.GL_ATTACHED_SHADERS, shaderCount, 0);
		System.out.println("RTR: initialize => AFTER ATTACHING SHADERS shader count is :: " + shaderCount[0]);

		//prelinking binding to attributes
		GLES32.glBindAttribLocation(shaderProgramObject,GLESMacros.AMC_ATTRIBUTE_POSITION, "vPsition" );

		//link
		GLES32.glLinkProgram(shaderProgramObject);
		//error checking
		int[] iProgramLinkStatus = new int[1];

		iInfoLogLength[0] = 0;
		szInfoLog = null;

		GLES32.glGetProgramiv(shaderProgramObject, GLES32.GL_LINK_STATUS, iProgramLinkStatus, 0);

		if(iProgramLinkStatus[0] == GLES32.GL_FALSE)
		{
			GLES32.glGetProgramiv(shaderProgramObject, GLES32.GL_INFO_LOG_LENGTH, iInfoLogLength, 0);
			if(iInfoLogLength[0] > 0)
			{
				szInfoLog = GLES32.glGetShaderInfoLog(shaderProgramObject);
				System.out.println("RTR SHADER PROGRAM LINKING LOG:" + szInfoLog);
				uninitialize();
				System.exit(0);
			}
		}

		//Get uniform location
		mvpUniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_mvp_matrix");

		// Triangle position vertices array, as it is inline initialization, no need of specifying size of array on rhs 
		final float[] triangleVertices = new float[]
		{
			 0.0f, 1.0f, 0.0f,
			-1.0f, -1.0f, 0.0f,
			 1.0f, -1.0f, 0.0f
		};

		// calculation of incenter of circle
		float[] A = new float[] { 0.0f, 1.0f, 0.0f };	//apex
		float[] B = new float[] { -1.0f, -1.0f, 0.0f };	//left bottom
		float[] C = new float[] { 1.0f, -1.0f, 0.0f };	//right bottom
		float P;										//perimeter
		float S;										//Semiperimeter = perimeter/2
		float[] Ta = new float[3];						//Point where incircle touches Triangle ABC on side AB, i.e. opposite to Vertex A
		float a, b, c;									//length of sides opposite to respective vertex
		float Radius;									//Radius of incircle
		float theta = 0.0f;

		//	Length of Sides of triangle using distance formula
		a = (float)Math.sqrt((float)Math.pow((C[0] - B[0]), 2.0f) + (float)Math.pow((C[1] - B[1]), 2.0f));		//length of side opposite to A i.e BC 
		b = (float)Math.sqrt((float)Math.pow((C[0] - A[0]), 2.0f) + (float)Math.pow((C[1] - A[1]), 2.0f));		//length of side opposite to B i.e AC
		c = (float)Math.sqrt((float)Math.pow((B[0] - A[0]), 2.0f) + (float)Math.pow((B[1] - A[1]), 2.0f));		//length of side opposite to C i.e AB

		//Triangle Perimeter and Semi-Perimeter
		P = a + b + c;
		S = P / 2.0f;

		//	Incenter of Triangle
		InCenter[0] = ((a*A[0]) + (b*B[0]) + (c*C[0])) / P;					//	Cx = (a*Ax + b*Bx + c*Cx)/S
		InCenter[1] = ((a*A[1]) + (b*B[1]) + (c*C[1])) / P;					//	Cy = (a*Ay + b*By + c*Cy)/S

		//	Radius of incircle
		Radius = (float)Math.sqrt(S*(S - a)*(S - b)*(S - c)) / S;						//	radius = sqrt(S(S-a)(S-b)(S-c))/S

		//	Calculation of Ta[]	midpoint of BC
		Ta[0] = C[0] - a / 2;
		Ta[1] = C[1];
		Ta[2] = 0.0f;

		for (int i = 0; i < 300; i = i + 3)
		{
			theta = 2 * 3.1415f * i / 300;
			circleVertices[i] = Radius * (float)Math.cos(theta);
			circleVertices[i + 1] = Radius * (float)Math.sin(theta);
			circleVertices[i + 2] = 0.0f;
		}

		lineVertices[0] = 0.0f;
		lineVertices[1] = 1.0f;
		lineVertices[2] = 0.0f;
		lineVertices[3] = Ta[0];
		lineVertices[4] = Ta[1];
		lineVertices[5] = Ta[2];

		ByteBuffer byteBuffer;
		FloatBuffer positionBuffer;

	// VAO FOR TRIANGLE
		//create and bind with vao
		GLES32.glGenVertexArrays(1, vao_triangle, 0);
		GLES32.glBindVertexArray(vao_triangle[0]);
		//Create and bind with vbo
		GLES32.glGenBuffers(1, vbo_position_triangle, 0);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vbo_position_triangle[0]);
		//Allocate the buffer directly from native memory
		byteBuffer = ByteBuffer.allocateDirect(triangleVertices.length*4); //4 is size of float in java
		//Arrange the byte order of buffer to the native byte order
		byteBuffer.order(ByteOrder.nativeOrder());
		//create the float type buffer and convert our byte type buffer into float type buffer
		positionBuffer = byteBuffer.asFloatBuffer();
		//Now put your array into the "COOKED" buffer
		positionBuffer.put(triangleVertices);
		//set the array at 0th position of the buffer as our array is not interleaved
		positionBuffer.position(0);
		//data to the buffer
		GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER, triangleVertices.length * 4, positionBuffer,GLES32.GL_STATIC_DRAW);
		GLES32.glVertexAttribPointer(GLESMacros.AMC_ATTRIBUTE_POSITION, 
										3,				// break data after x y z, i.e. 3
										GLES32.GL_FLOAT, // type of data
										false, 			// normalize?
										0, 				//stride?-no
										0 );			//offset?-no(windows : NULL, in java null is not #define 0) therefore 0
		GLES32.glEnableVertexAttribArray(GLESMacros.AMC_ATTRIBUTE_POSITION);
		
		//bind to buffer
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0);
		GLES32.glBindVertexArray(0);

	//VAO FOR CIRCLE
		//create and bind with vao
		GLES32.glGenVertexArrays(1, vao_circle, 0);
		GLES32.glBindVertexArray(vao_circle[0]);
		//Create and bind with vbo
		GLES32.glGenBuffers(1, vbo_position_circle, 0);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vbo_position_circle[0]);
		//Allocate the buffer directly from native memory
		byteBuffer = ByteBuffer.allocateDirect(circleVertices.length*4); //4 is size of float in java
		//Arrange the byte order of buffer to the native byte order
		byteBuffer.order(ByteOrder.nativeOrder());
		//create the float type buffer and convert our byte type buffer into float type buffer
		positionBuffer = byteBuffer.asFloatBuffer();
		//Now put your array into the "COOKED" buffer
		positionBuffer.put(circleVertices);
		//set the array at 0th position of the buffer as our array is not interleaved
		positionBuffer.position(0);
		//data to the buffer
		GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER, circleVertices.length * 4, positionBuffer,GLES32.GL_STATIC_DRAW);
		GLES32.glVertexAttribPointer(GLESMacros.AMC_ATTRIBUTE_POSITION, 
										3,				// break data after x y z, i.e. 3
										GLES32.GL_FLOAT, // type of data
										false, 			// normalize?
										0, 				//stride?-no
										0 );			//offset?-no(windows : NULL, in java null is not #define 0) therefore 0
		GLES32.glEnableVertexAttribArray(GLESMacros.AMC_ATTRIBUTE_POSITION);
		//bind to buffer
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0);
		GLES32.glBindVertexArray(0);

	//VAO FOR LINE
		//create and bind with vao
		GLES32.glGenVertexArrays(1, vao_line, 0);
		GLES32.glBindVertexArray(vao_line[0]);
		//Create and bind with vbo
		GLES32.glGenBuffers(1, vbo_position_line, 0);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vbo_position_line[0]);
		//Allocate the buffer directly from native memory
		byteBuffer = ByteBuffer.allocateDirect(lineVertices.length*4); //4 is size of float in java
		//Arrange the byte order of buffer to the native byte order
		byteBuffer.order(ByteOrder.nativeOrder());
		//create the float type buffer and convert our byte type buffer into float type buffer
		positionBuffer = byteBuffer.asFloatBuffer();
		//Now put your array into the "COOKED" buffer
		positionBuffer.put(lineVertices);
		//set the array at 0th position of the buffer as our array is not interleaved
		positionBuffer.position(0);
		//data to the buffer
		GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER, lineVertices.length * 4, positionBuffer,GLES32.GL_STATIC_DRAW);
		GLES32.glVertexAttribPointer(GLESMacros.AMC_ATTRIBUTE_POSITION, 
										3,				// break data after x y z, i.e. 3
										GLES32.GL_FLOAT, // type of data
										false, 			// normalize?
										0, 				//stride?-no
										0 );			//offset?-no(windows : NULL, in java null is not #define 0) therefore 0
		GLES32.glEnableVertexAttribArray(GLESMacros.AMC_ATTRIBUTE_POSITION);
		//bind to buffer
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0);
		GLES32.glBindVertexArray(0);
		
		GLES32.glClearColor(0.0f,0.0f,0.0f,1.0f);
		GLES32.glEnable(GLES32.GL_DEPTH_TEST);
		GLES32.glDepthFunc(GLES32.GL_LEQUAL);

		Matrix.setIdentityM(perspectiveProjectionMatrix,0);
		//no warmup call to resize as we are already in fullscreen. 
		//we dont run multiple app with screen sharing in android unlike windows.
    }

	private void update()
	{
		if (angleTriangle < 360.0f)
			angleTriangle = angleTriangle + 0.05f;
		else
			angleTriangle = 0.0f;

		if (triangle == true)
		{
			if (trans_x > 0.0f)
				trans_x = trans_x - 0.0002f;
			else
				line = true;

			if (trans_y < 0.0f)
				trans_y = trans_y + 0.0002f;
			else
				line = true;
		}
	
		if (line == true)
		{
			if (tline_y > 0)
				tline_y = tline_y - 0.0002f;
			else
				circle = true;
		}
	
		if (circle == true)
		{
			if (tcirc_x + InCenter[0] < 0)
				tcirc_x = tcirc_x + 0.0002f;

			if (tcirc_y - InCenter[1] < 0)
				tcirc_y = tcirc_y + 0.0002f;
		}
	}

    private void resize(int width, int height)
    {
        if(height<0)
        {
            height =1;
        }
		GLES32.glViewport(0,0,width, height);
		Matrix.perspectiveM(perspectiveProjectionMatrix, 0, 45.0f, (float)width/ (float)height, 0.1f, 100.0f);
    }

	private void display()
	{
		//variable declarations
		float[] modelViewMatrix = new float[16];
		float[] modelViewProjectionMatrix = new float[16];
		float[] translationMatrix = new float[16];
		float[] rotationMatrix = new float[16];

		//code
		GLES32.glClear(GLES32.GL_COLOR_BUFFER_BIT|GLES32.GL_DEPTH_BUFFER_BIT);
		GLES32.glUseProgram(shaderProgramObject);

		//making martices identity
		Matrix.setIdentityM(modelViewMatrix, 0);
		Matrix.setIdentityM(modelViewProjectionMatrix, 0);
		Matrix.setIdentityM(translationMatrix, 0);
		Matrix.setIdentityM(rotationMatrix, 0);

		//**************************************TRIANGLE ************************************
		//if(triangle == true)
		//{
			//matrix transformation
			Matrix.translateM(translationMatrix, 0, trans_x, trans_y, -6.0f);
			Matrix.rotateM(rotationMatrix, 0, angleTriangle, 0.0f, 1.0f, 0.0f);

			Matrix.multiplyMM(modelViewMatrix, 0, modelViewMatrix, 0, translationMatrix, 0 );
			Matrix.multiplyMM(modelViewMatrix, 0, modelViewMatrix, 0, rotationMatrix, 0 );
			Matrix.multiplyMM(modelViewProjectionMatrix, 0, perspectiveProjectionMatrix, 0, modelViewMatrix, 0);

			//send matrix to shader
			GLES32.glUniformMatrix4fv(mvpUniform, 1, false, modelViewProjectionMatrix, 0);

			//bind with vao
			GLES32.glBindVertexArray(vao_triangle[0]);
			GLES32.glDrawArrays(GLES32.GL_LINE_LOOP, 0, 3);
			//unbind with vao
			GLES32.glBindVertexArray(0);
		//}
		//***********************************************************************************
		
		//making martices identity
		Matrix.setIdentityM(modelViewMatrix, 0);
		Matrix.setIdentityM(modelViewProjectionMatrix, 0);
		Matrix.setIdentityM(translationMatrix, 0);
		Matrix.setIdentityM(rotationMatrix, 0);
	
		//***************************************LINE*****************************************
		if (line == true)
		{
			//matrix transformation
			Matrix.translateM(translationMatrix, 0, 0.0f, tline_y, -6.0f);

			Matrix.multiplyMM(modelViewMatrix, 0, modelViewMatrix, 0, translationMatrix, 0 );
			Matrix.multiplyMM(modelViewMatrix, 0, modelViewMatrix, 0, rotationMatrix, 0 );
			Matrix.multiplyMM(modelViewProjectionMatrix, 0, perspectiveProjectionMatrix, 0, modelViewMatrix, 0);

			//send matrix to shader
			GLES32.glUniformMatrix4fv(mvpUniform, 1, false, modelViewProjectionMatrix, 0);

			//bind with vao
			GLES32.glBindVertexArray(vao_line[0]);
			GLES32.glDrawArrays(GLES32.GL_LINES, 0, 2);
			//unbind with vao
			GLES32.glBindVertexArray(0);
		}
		//***********************************************************************************
		
			//making martices identity
		Matrix.setIdentityM(modelViewMatrix, 0);
		Matrix.setIdentityM(modelViewProjectionMatrix, 0);
		Matrix.setIdentityM(translationMatrix, 0);
		Matrix.setIdentityM(rotationMatrix, 0);
	
		// **************************************CIRCLE****************************************
		if (circle == true)
		{
			//matrix transformation
			Matrix.translateM(translationMatrix, 0, tcirc_x, tcirc_y, -6.0f);
			Matrix.rotateM(rotationMatrix, 0, angleTriangle, 0.0f, 1.0f, 0.0f);

			Matrix.multiplyMM(modelViewMatrix, 0, modelViewMatrix, 0, translationMatrix, 0 );
			Matrix.multiplyMM(modelViewMatrix, 0, modelViewMatrix, 0, rotationMatrix, 0 );
			Matrix.multiplyMM(modelViewProjectionMatrix, 0, perspectiveProjectionMatrix, 0, modelViewMatrix, 0);

			//send matrix to shader
			GLES32.glUniformMatrix4fv(mvpUniform, 1, false, modelViewProjectionMatrix, 0);

			//bind with vao
			GLES32.glBindVertexArray(vao_circle[0]);
			GLES32.glDrawArrays(GLES32.GL_LINE_LOOP, 0, 100);
			//unbind with vao
			GLES32.glBindVertexArray(0);
		}
	
		//unuse program
		GLES32.glUseProgram(0);
		 requestRender();
		if (angleTriangle < 360.0f)
			angleTriangle = angleTriangle + 0.5f;
		else
			angleTriangle = 0.0f;

		if (triangle == true)
		{
			if (trans_x > 0.0f)
				trans_x = trans_x - 0.002f;
			else
				line = true;

			if (trans_y < 0.0f)
				trans_y = trans_y + 0.002f;
			else
				line = true;
		}
	
		if (line == true)
		{
			if (tline_y > 0)
				tline_y = tline_y - 0.002f;
			else
				circle = true;
		}
	
		if (circle == true)
		{
			if (tcirc_x + InCenter[0] < 0)
				tcirc_x = tcirc_x + 0.002f;

			if (tcirc_y - InCenter[1] < 0)
				tcirc_y = tcirc_y + 0.002f;
		}
       
    }

}
