package rtr.example.adssphere_pv;

//packages added by me
import android.content.Context;
import android.graphics.Color;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.GestureDetector;
import android.view.GestureDetector.OnGestureListener;
import rtr.example.adssphere_pv.GLESMacros;
import rtr.example.adssphere_pv.Sphere;
import android.view.GestureDetector.OnDoubleTapListener;

import android.opengl.GLSurfaceView;
import android.opengl.GLES32;

import javax.microedition.khronos.opengles.GL10;
import javax.microedition.khronos.egl.EGLConfig;

//for opengl buffers
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;
import java.nio.ShortBuffer;

//for texture
import android.graphics.BitmapFactory;
import android.graphics.Bitmap;
import android.opengl.GLUtils;

//for matrix mathematics
import android.opengl.Matrix;

public class GLESView extends GLSurfaceView implements GLSurfaceView.Renderer, OnGestureListener, OnDoubleTapListener{
    private GestureDetector gestureDetector;
	private final Context context;
	
	private int vertexShaderObject;
	private int fragmentShaderObject;
	private int shaderProgramObject;

	private int mUniform;
	private int vUniform;
	private int pUniform;
	private int ldUniform;
	private int laUniform;
	private int lsUniform;
	private int lightPositionUniform;

	private int kdUniform;
	private int kaUniform;
	private int ksUniform;
	private int ShininessUniform;

	private int enableLightsUniform;

	private int[] vao_sphere = new int[1];
	private int[] vbo_sphere_position = new int[1];
	private int[] vbo_sphere_normal = new int[1];
	private int[] vbo_sphere_element = new int[1];

	private int numVertices;
	private int numElements;

	private float[] perspectiveProjectionMatrix = new float[16];		//4x4 matrix
	
	private float[] lightDiffuse = new float[] {1.0f, 1.0f, 1.0f, 1.0f};
	private float[] lightAmbient = new float[] {0.0f, 0.0f, 0.0f, 0.0f};
	private float[] lightSpecular = new float[] {1.0f, 1.0f, 1.0f, 1.0f};
	private float[] lightPosition = new float[] {100.0f, 100.0f, 100.0f, 1.0f};

	private float[] materialDiffuse = new float[] {1.0f, 1.0f, 1.0f, 1.0f};
	private float[] materialAmbient = new float[] {0.0f, 0.0f, 0.0f, 0.0f};
	private float[] materialSpecular = new float[] {1.0f, 1.0f, 1.0f, 1.0f};
	private float  materialShininess = 128.0f;

	private int enableLighting = 0;

    //constructor
    public GLESView(Context drawingContext){
        super(drawingContext);
        context = drawingContext;
        
        setEGLContextClientVersion(3); 
        setRenderer(this);
        setRenderMode(GLSurfaceView.RENDERMODE_WHEN_DIRTY);

        gestureDetector =new GestureDetector(drawingContext, this, null, false );
        gestureDetector.setOnDoubleTapListener(this);
    }

    //handling 'onTouchEvent' is the most important,
	//because it triggers all gesture and tap events
	@Override
	public boolean onTouchEvent(MotionEvent event)
	{
		//code
		int eventaction = event.getAction();
		if(!gestureDetector.onTouchEvent(event))
			super.onTouchEvent(event);
		return(true);
	}

    //abstract method from OnDoubleTapListener so must be implemented
	@Override
	public boolean onDoubleTap(MotionEvent e)
	{
		if(enableLighting == 0)
		{
			enableLighting = 1;
		}
		else
		{
			enableLighting = 0;
		}

		System.out.println("RTR: Double Tap Event Occured");
		return(true);
    }
    
    //abstract method from OnDoubleTapListener so must be implemented
	@Override
	public boolean onDoubleTapEvent(MotionEvent e)
	{
		//do not write any code here because already written in 'onDoubleTap'
		return(true);
    }
    
    //abstract  method from OnDoubleTapListener so must be implemented
	@Override
	public boolean onSingleTapConfirmed(MotionEvent e)
	{
	//	setText("Single Tap");
		
		return(true);
    }
    
    //abstract  method from OnGestureListener so must be implemented
	@Override
	public boolean onDown(MotionEvent e)
	{
		//do not write any code here because already written in 'onSingleTapConfirmed'
		return(true);
    }
    
    //abstract  method from OnGestureListener so must be implemented
	@Override
	public boolean onFling(MotionEvent e1,MotionEvent e2, float velocityX, float velocityY)
	{
		return(true);
	}

	//abstract  method from OnGestureListener so must be implemented
	@Override
	public void onLongPress(MotionEvent e)
	{
		//setText("Long Press");
	}

	//abstract  method from OnGestureListener so must be implemented
	@Override
	public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY)
	{
		//setText("Scroll");
		uninitialize();
		System.exit(0);
		return(true);
	}

	//abstract  method from OnGestureListener so must be implemented
	@Override
	public void onShowPress(MotionEvent e)
	{
	
	}

	//abstract  method from OnGestureListener so must be implemented
	@Override
	public boolean onSingleTapUp(MotionEvent e)
	{
		return(true);
    }
    
    //implement GLSurfaceView.Renderer methods
    @Override
    public void onSurfaceCreated(GL10 gl, EGLConfig config){
		System.out.println("RTR: Called onSurfaceCreated");

        String version = gl.glGetString(GL10.GL_VERSION);
		System.out.println("RTR:" + version);
		
		version = gl.glGetString(GLES32.GL_SHADING_LANGUAGE_VERSION);
		System.out.println("RTR:" + version);
        initialize();
    }

    @Override
    public void onSurfaceChanged(GL10 unused, int width, int height)
    {
		System.out.println("RTR: Called onSurfaceChanged");

        resize(width, height);
    }

    @Override
    public void onDrawFrame(GL10 unused){
        display();
    }

	//our custom methods
	private void uninitialize()
	{
		System.out.println("RTR: Called uninitialize");
		
		if(shaderProgramObject != 0)
		{
			int[] shaderCount = new int[1];
			int shaderNumber;

			GLES32.glUseProgram(shaderProgramObject);

			GLES32.glGetProgramiv(shaderProgramObject, GLES32.GL_ATTACHED_SHADERS, shaderCount, 0);
			System.out.println("RTR: uninitialize => " + shaderCount[0]);
		
			int[] shaders =new int[shaderCount[0]];

			if(shaderCount[0] != 0)
			{
				GLES32.glGetAttachedShaders(shaderProgramObject, shaderCount[0], shaderCount, 0, shaders, 0);

				for(shaderNumber =0;shaderNumber<shaderCount[0]; shaderNumber++)
				{
					GLES32.glDetachShader(shaderProgramObject, shaders[shaderNumber]);
					GLES32.glDeleteShader(shaders[shaderNumber]);
					shaders[shaderNumber]=0;
				}
			}
			else{
				System.out.println("RTR: uninitialize => shader count is 0");
			}
			GLES32.glDeleteProgram(shaderProgramObject);
			shaderProgramObject = 0;
			GLES32.glUseProgram(0);
		}
		if(vbo_sphere_position[0] != 0)
		{
			System.out.println("RTR: uninitialize => deleting vbo_sphere_position");

			GLES32.glDeleteBuffers(1, vbo_sphere_position, 0);
			vbo_sphere_position[0] = 0;
		}
		if(vbo_sphere_normal[0] != 0)
		{
			System.out.println("RTR: uninitialize => deleting vbo_sphere_normal");

			GLES32.glDeleteBuffers(1, vbo_sphere_normal, 0);
			vbo_sphere_normal[0] = 0;
		}
		if(vbo_sphere_element[0] != 0)
		{
			System.out.println("RTR: uninitialize => deleting vbo_sphere_element");

			GLES32.glDeleteBuffers(1, vbo_sphere_element, 0);
			vbo_sphere_element[0] = 0;
		}
		if(vao_sphere[0] != 0)
		{
			System.out.println("RTR: uninitialize => deleting vao_sphere");

			GLES32.glDeleteVertexArrays(1, vao_sphere, 0);
			vao_sphere[0]=0;
		}
	}

    private void initialize()
	{
		System.out.println("RTR: Called initialize");

		vertexShaderObject = GLES32.glCreateShader(GLES32.GL_VERTEX_SHADER);
		final String vertexShaderSourceCode = String.format
		(
			"	#version 320 es																					" +
			"																								\n	" +
			"	in vec4 vPosition;																				" +
			"	in vec3 vNormal;																				" +
			"																								\n	" +
			"	uniform mat4 u_m_matrix;																		" +
			"	uniform mat4 u_v_matrix;																		" +
			"	uniform mat4 u_p_matrix;																		" +
			"	uniform int u_enable_lights;																	" +
			"	uniform vec3 u_ld;																			" +
			"	uniform vec3 u_la;																			" +
			"	uniform vec3 u_ls;																		\n	" +
			"	uniform vec3 u_ka;																			\n	" +
			"	uniform vec3 u_kd;																			\n	" +
			"	uniform vec3 u_ks;																			\n	" +
			"	uniform vec4 u_light_position;																" +
			"	uniform float u_material_shininess;															\n	" +
			"																								\n	" +
			"	out vec3 phong_ads_light;																		" +
			"																								\n	" +
			"	void main(void)																					" +
			"	{																								" +
			"		if(u_enable_lights == 1)																	" +
			"		{																							" +
			"			vec4 eyeCoordinates = u_v_matrix * u_m_matrix * vPosition;								" +
			"			mat3 normalMatrix   = mat3(u_v_matrix * u_m_matrix);									" +
			"			vec3 tNorm 			= normalize(normalMatrix * vNormal);								" +
			"			vec3 viewerVector	= normalize(vec3(-eyeCoordinates));									" +
			"																								\n	" +
			"			vec3 lightDirection = normalize(vec3(u_light_position - eyeCoordinates));	\n	" +
			"			vec3 lightReflectionVector = reflect(-lightDirection, tNorm);				\n	" +
			"			float tnDotld = max(dot(tNorm, lightDirection), 0.0);						\n	" +
			"			float rvDotvv = max(dot(lightReflectionVector, viewerVector),0.0);	 		\n	" +
			"																								\n	" +
			"			vec3 Ambient = u_la * u_ka;												\n	" +
			"			vec3 Diffuse = u_ld * u_kd * tnDotld;									\n	" +
			"			vec3 Specular = u_ls * u_ks * pow(rvDotvv, u_material_shininess);		\n	" +
			"																								\n	" +
			"			phong_ads_light = Ambient + Diffuse + Specular;										\n	" +
			"		}																							" +
			"		else																						" +
			"		{																							" +
			"			phong_ads_light = vec3(1.0, 1.0, 1.0);													" +
			"		}																							" +
			"		gl_Position = u_p_matrix * u_v_matrix *u_m_matrix * vPosition;								" +
			"	}																								"
		);

		GLES32.glShaderSource(vertexShaderObject, vertexShaderSourceCode);
		
		GLES32.glCompileShader(vertexShaderObject);
		int[] iShaderCompileStatus = new int[1];
		int[] iInfoLogLength = new int[1];
		String szInfoLog = null;

		GLES32.glGetShaderiv(vertexShaderObject, GLES32.GL_COMPILE_STATUS, iShaderCompileStatus, 0);

		if(iShaderCompileStatus[0] == GLES32.GL_FALSE)
		{
			GLES32.glGetShaderiv(vertexShaderObject, GLES32.GL_INFO_LOG_LENGTH, iInfoLogLength, 0);
			if(iInfoLogLength[0] > 0)
			{
				szInfoLog = GLES32.glGetShaderInfoLog(vertexShaderObject);
				System.out.println("RTR: VERTEX SHADER COMPILATION LOG:" + szInfoLog);
				uninitialize();
				System.exit(0);
			}
		}

		//fragment shader
		fragmentShaderObject = GLES32.glCreateShader(GLES32.GL_FRAGMENT_SHADER);
		final String fragmentShaderSourceCode = String.format
		(
			"	#version 320 es											" +
			"														\n	" +
			"	precision highp float;									" +
			"	out vec4 FragColor;										" +
			"	in vec3 phong_ads_light;								" +
			"														\n	" +
			"	void main(void)											" +
			"	{														" +
			"			FragColor = vec4(phong_ads_light, 1.0);			" +
			"	}														"
		);

		GLES32.glShaderSource(fragmentShaderObject, fragmentShaderSourceCode);
		GLES32.glCompileShader(fragmentShaderObject);
		
		iShaderCompileStatus[0] = 0;
		iInfoLogLength[0] = 0;
		szInfoLog = null;

		GLES32.glGetShaderiv(fragmentShaderObject, GLES32.GL_COMPILE_STATUS, iShaderCompileStatus, 0);
		
		if(iShaderCompileStatus[0] == GLES32.GL_FALSE)
		{
			GLES32.glGetShaderiv(fragmentShaderObject, GLES32.GL_INFO_LOG_LENGTH, iInfoLogLength, 0);

			if(iInfoLogLength[0] > 0)
			{
				szInfoLog = GLES32.glGetShaderInfoLog(fragmentShaderObject);
				System.out.println("RTR: FRAGMENT SHADER COMPILATION LOG:" + szInfoLog);
				uninitialize();
				System.exit(0);
			}
		}

		int[] shaderCount = new int[1];

		//shader program
		shaderProgramObject = GLES32.glCreateProgram();

		GLES32.glGetProgramiv(shaderProgramObject, GLES32.GL_ATTACHED_SHADERS, shaderCount, 0);

		GLES32.glAttachShader(shaderProgramObject, vertexShaderObject);
		GLES32.glAttachShader(shaderProgramObject, fragmentShaderObject);

		GLES32.glGetProgramiv(shaderProgramObject, GLES32.GL_ATTACHED_SHADERS, shaderCount, 0);

		//prelinking binding to attributes
		GLES32.glBindAttribLocation(shaderProgramObject,GLESMacros.AMC_ATTRIBUTE_POSITION, "vPosition" );
		GLES32.glBindAttribLocation(shaderProgramObject,GLESMacros.AMC_ATTRIBUTE_NORMAL, "vNormal" );
		
		//link
		GLES32.glLinkProgram(shaderProgramObject);
		
		//error checking
		int[] iProgramLinkStatus = new int[1];
		iProgramLinkStatus[0] = 0;
		iInfoLogLength[0] = 0;
		szInfoLog = null;

		GLES32.glGetProgramiv(shaderProgramObject, GLES32.GL_LINK_STATUS, iProgramLinkStatus, 0);

		if(iProgramLinkStatus[0] == GLES32.GL_FALSE)
		{
			GLES32.glGetProgramiv(shaderProgramObject, GLES32.GL_INFO_LOG_LENGTH, iInfoLogLength, 0);
			if(iInfoLogLength[0] > 0)
			{
				szInfoLog = GLES32.glGetShaderInfoLog(shaderProgramObject);
				System.out.println("RTR SHADER PROGRAM LINKING LOG:" + szInfoLog);
				uninitialize();
				System.exit(0);
			}
		}

		//post linking retrieving uniform location
		// model, view, projection matrices uniform location
		mUniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_m_matrix");
		vUniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_v_matrix");
		pUniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_p_matrix");
		// light uniform location
		ldUniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_ld");
		laUniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_la");
		lsUniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_ls");
		lightPositionUniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_light_position" );
		// material uniform location
		kaUniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_ka");
		kdUniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_kd");
		ksUniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_ks");
		ShininessUniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_material_shininess");
		// enable lighting
		enableLightsUniform  = GLES32.glGetUniformLocation(shaderProgramObject, "u_enable_lights");

		//******************************* CREATE SPHERE ************************************//
		Sphere sphere = new Sphere();
		float sphere_vertices[] = new float[1146];
		float sphere_normals[] = new float[1146];
		float sphere_textures[] = new float[764];
		short sphere_elements[] = new short[2280];
		sphere.getSphereVertexData(sphere_vertices, sphere_normals, sphere_textures, sphere_elements);
		numVertices = sphere.getNumberOfSphereVertices();
		numElements = sphere.getNumberOfSphereElements();

		//create and bind with vao
		GLES32.glGenVertexArrays(1, vao_sphere, 0);
		GLES32.glBindVertexArray(vao_sphere[0]);

		//Create buffer for position and bind with vbo
		GLES32.glGenBuffers(1, vbo_sphere_position, 0);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vbo_sphere_position[0]);

		//Create the buffer 
		ByteBuffer byteBuffer_pos = ByteBuffer.allocateDirect(sphere_vertices.length * 4); //4 is size of float in java
		byteBuffer_pos.order(ByteOrder.nativeOrder());
		FloatBuffer sphereVerticesBuffer = byteBuffer_pos.asFloatBuffer();
		sphereVerticesBuffer.put(sphere_vertices);
		sphereVerticesBuffer.position(0);

		//data to the buffer
		GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER, sphere_vertices.length * 4, sphereVerticesBuffer, GLES32.GL_STATIC_DRAW);
		GLES32.glVertexAttribPointer(GLESMacros.AMC_ATTRIBUTE_POSITION, 
										3,				// break data after x y z, i.e. 3
										GLES32.GL_FLOAT, // type of data
										false, 			// normalize?
										0, 				//stride?-no
										0 );			//offset?-no(windows : NULL, in java null is not #define 0) therefore 0
		GLES32.glEnableVertexAttribArray(GLESMacros.AMC_ATTRIBUTE_POSITION);
		
		//unbind to buffer
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0);

		//Create buffer for normal and bind with vbo
		GLES32.glGenBuffers(1, vbo_sphere_normal, 0);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vbo_sphere_normal[0]);

		//Allocate the buffer directly from native memory
		ByteBuffer byteBuffer_nor = ByteBuffer.allocateDirect(sphere_normals.length*4);
		byteBuffer_nor.order(ByteOrder.nativeOrder());
		FloatBuffer sphereNormalBuffer = byteBuffer_nor.asFloatBuffer();
		sphereNormalBuffer.put(sphere_normals);
		sphereNormalBuffer.position(0);
		
		//data to the buffer
		GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER, sphere_normals.length *4, sphereNormalBuffer, GLES32.GL_STATIC_DRAW);
		GLES32.glVertexAttribPointer(GLESMacros.AMC_ATTRIBUTE_NORMAL, 
										3,				// break data after x y z, i.e. 3
										GLES32.GL_FLOAT, // type of data
										false, 			// normalize?
										0, 				//stride?-no
										0 );			//offset?-no(windows : NULL, in java null is not #define 0) therefore 0
		GLES32.glEnableVertexAttribArray(GLESMacros.AMC_ATTRIBUTE_NORMAL);
		
		//unbind to buffer
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0);
		
		//Create buffer for element and bind with vbo
		GLES32.glGenBuffers(1, vbo_sphere_element, 0);
		GLES32.glBindBuffer(GLES32.GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_element[0]);

		//Allocate the buffer directly from native memory
		ByteBuffer byteBuffer_ele = ByteBuffer.allocateDirect(sphere_elements.length*2);
		byteBuffer_ele.order(ByteOrder.nativeOrder());
		ShortBuffer sphereElementsBuffer = byteBuffer_ele.asShortBuffer();
		sphereElementsBuffer.put(sphere_elements);
		sphereElementsBuffer.position(0);

		//data to the buffer
		GLES32.glBufferData(GLES32.GL_ELEMENT_ARRAY_BUFFER, sphere_elements.length * 2, sphereElementsBuffer, GLES32.GL_STATIC_DRAW);
		GLES32.glBindBuffer(GLES32.GL_ELEMENT_ARRAY_BUFFER, 0);
		GLES32.glBindVertexArray(0);

		GLES32.glClearColor(0.0f,0.0f,0.0f,1.0f);
		GLES32.glEnable(GLES32.GL_DEPTH_TEST);
		GLES32.glDepthFunc(GLES32.GL_LEQUAL);

		Matrix.setIdentityM(perspectiveProjectionMatrix,0);
		//no warmup call to resize as we are already in fullscreen. 
		//we dont run multiple app with screen sharing in android unlike windows.
    }

    private void resize(int width, int height)
    {
        if(height < 0)
        {
            height = 1;
        }
		GLES32.glViewport(0,0,width, height);
		Matrix.perspectiveM(perspectiveProjectionMatrix, 0, 45.0f, (float)width/ (float)height, 0.1f, 100.0f);
    }

	private void display()
	{
		//variable declarations
		float[] modelMatrix = new float[16];
		float[] viewMatrix = new float[16];
		float[] projectionMatrix = new float[16];
		float[] translationMatrix = new float[16];
		float[] scaleMatrix = new float[16];
		float[] rotationMatrix = new float[16];

		//making matrices identity
		Matrix.setIdentityM(modelMatrix,0);
		Matrix.setIdentityM(viewMatrix,0);
		Matrix.setIdentityM(projectionMatrix,0);
		Matrix.setIdentityM(translationMatrix,0);
		Matrix.setIdentityM(scaleMatrix,0);
		Matrix.setIdentityM(rotationMatrix, 0);
		
		//code
		GLES32.glClear(GLES32.GL_COLOR_BUFFER_BIT|GLES32.GL_DEPTH_BUFFER_BIT);
		GLES32.glUseProgram(shaderProgramObject);

		//matrix transformation
		Matrix.translateM(translationMatrix, 0, 0.0f, 0.0f, -2.0f);

		Matrix.multiplyMM(modelMatrix, 0,
						  modelMatrix, 0,
						  translationMatrix, 0);
		Matrix.multiplyMM(modelMatrix, 0,
						  modelMatrix, 0,
						  rotationMatrix, 0);
		Matrix.multiplyMM(projectionMatrix, 0,
						  projectionMatrix, 0,
						  perspectiveProjectionMatrix, 0);

		if(enableLighting == 1)
		{
			GLES32.glUniform1i(enableLightsUniform, 1);
			GLES32.glUniform3fv(ldUniform, 1, lightDiffuse, 0);
			GLES32.glUniform3fv(laUniform, 1, lightAmbient, 0);
			GLES32.glUniform3fv(lsUniform, 1, lightSpecular, 0);			
			
			GLES32.glUniform3fv(kdUniform, 1, materialDiffuse, 0);
			GLES32.glUniform3fv(kaUniform, 1, materialAmbient, 0);
			GLES32.glUniform3fv(ksUniform, 1, materialSpecular, 0);			
		
			GLES32.glUniform1f(ShininessUniform, materialShininess);			

			GLES32.glUniform3fv(lightPositionUniform, 1, lightPosition, 0);			
		}
		else
		{
			GLES32.glUniform1i(enableLightsUniform, 0);
		}
		//send matrix to shader
		GLES32.glUniformMatrix4fv(mUniform, 1, false, modelMatrix, 0);
		GLES32.glUniformMatrix4fv(vUniform, 1, false, viewMatrix, 0);
		GLES32.glUniformMatrix4fv(pUniform, 1, false, projectionMatrix, 0);

		System.out.println("RTR: in Display ");

		//bind with vao
		GLES32.glBindVertexArray(vao_sphere[0]);

		GLES32.glBindBuffer(GLES32.GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_element[0]);
		GLES32.glDrawElements(GLES32.GL_TRIANGLES, numElements,GLES32.GL_UNSIGNED_SHORT, 0);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0);

		//unbind with vao
		GLES32.glBindVertexArray(0);
		
		//unuse program
		GLES32.glUseProgram(0);
        requestRender();

	}
	
}

