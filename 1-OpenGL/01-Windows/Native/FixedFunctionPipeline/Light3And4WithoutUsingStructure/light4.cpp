//	Headers
#include<windows.h>
#include<stdio.h>
#include<gl/gl.h>
#include<gl/glu.h>

#define _USE_MATH_DEFINES 1
#include<math.h>

#pragma comment (lib, "glu32.lib")
#pragma comment (lib, "opengl32.lib")

//	Macros
#define WIN_WIDTH 800
#define WIN_HEIGHT 600

//	Global function declaration
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

//	Global variables
FILE *gpfile;
HWND ghwnd = NULL;
bool gbActiveWindow = false;
bool gbFullScreen = false;
bool bLight = false;
DWORD dwStyle;
WINDOWPLACEMENT wpPrev = { sizeof(WINDOWPLACEMENT) };
HDC ghdc = NULL;
HGLRC ghrc = NULL;

//LIGHT0
GLfloat LightAmbientZero[] = { 0.0f,0.0f,0.0f,1.0f };
GLfloat LightDiffuseZero[] = { 1.0f,0.0f,0.0f,1.0f };	//Red
GLfloat LightSpecularZero[] = { 1.0f,0.0f,0.0f,1.0f };	//Red
GLfloat LightPositionZero[] = { 0.0f,0.0f,0.0f,1.0f };

//LIGHT1
GLfloat LightAmbientOne[] = { 0.0f,0.0f,0.0f,1.0f };
GLfloat LightDiffuseOne[] = { 0.0f,1.0f,0.0f,1.0f };	//Green
GLfloat LightSpecularOne[] = { 0.0f,1.0f,0.0f,1.0f };	//Green
GLfloat LightPositionOne[] = { 0.0f,0.0f,0.0f,1.0f };

//LIGHT2
GLfloat LightAmbientTwo[] = { 0.0f,0.0f,0.0f,1.0f };
GLfloat LightDiffuseTwo[] = { 0.0f,0.0f,1.0f,1.0f };	//Blue
GLfloat LightSpecularTwo[] = { 0.0f,0.0f,1.0f,1.0f };	//Blue
GLfloat LightPositionTwo[] = { 0.0f,0.0f,0.0f,0.0f };

//Material
GLfloat MaterialAmbient[] = { 0.0f,0.0f,0.0f,1.0f };
GLfloat MaterialDiffuse[] = { 1.0f,1.0f,1.0f,1.0f };
GLfloat MaterialSpecular[] = { 1.0f,1.0f,1.0f,1.0f };
GLfloat MaterialShininess[] = { 128.0f };//128.0f

GLfloat LightAngleZero = 0.0f, LightAngleOne = 0.0f, LightAngleTwo = 0.0f;
GLUquadric *quadric_sphere = NULL;

//WinMain()
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{
	//function declarations
	int initialize(void);
	void display(void);
	void update(void);

	//variable declarations
	WNDCLASSEX wndclass;
	HWND hwnd;
	MSG msg;
	TCHAR szAppName[] = TEXT("Light-4");
	bool bDone = false;
	int iRet = 0;

	//code
	if (fopen_s(&gpfile, "log.txt", "w") != 0)
	{
		MessageBox(NULL, TEXT("Log file cannot be created"), TEXT("Erroe"), MB_OK);
		exit(0);
	}

	//initialization of wndclass structure
	wndclass.cbSize = sizeof(WNDCLASSEX);
	wndclass.cbWndExtra = 0;
	wndclass.cbClsExtra = 0;
	wndclass.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
	wndclass.lpfnWndProc = WndProc;
	wndclass.hInstance = hInstance;
	wndclass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	wndclass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndclass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndclass.lpszClassName = szAppName;
	wndclass.lpszMenuName = NULL;
	wndclass.hIconSm = LoadIcon(hInstance, IDI_APPLICATION);

	//register above class 
	RegisterClassEx(&wndclass);

	//create window
	hwnd = CreateWindowEx(WS_EX_APPWINDOW,
		szAppName,
		TEXT("steady sphere lit by 3 rotating light - PriyaSaboo"),
		WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN | WS_CLIPSIBLINGS | WS_VISIBLE,
		100,
		100,
		WIN_WIDTH,
		WIN_HEIGHT,
		NULL,
		NULL,
		hInstance,
		NULL);

	ghwnd = hwnd;
	iRet = initialize();
	if (iRet == -1)
	{
		fprintf(gpfile, "ChoosePixelFormat() Failed\n");
		DestroyWindow(hwnd);
	}
	else if (iRet == -2)
	{
		fprintf(gpfile, "SetPixelFormat() Failed\n");
		DestroyWindow(hwnd);
	}
	else if (iRet == -3)
	{
		fprintf(gpfile, "wglCreateContext() Failed\n");
		DestroyWindow(hwnd);
	}
	else if (iRet == -4)
	{
		fprintf(gpfile, "wglMakeCurrent() Failed\n");
		DestroyWindow(hwnd);
	}
	else
		fprintf(gpfile, "Initialize() Succeded\n");

	//show window
	ShowWindow(hwnd, iCmdShow);
	SetForegroundWindow(hwnd);
	SetFocus(hwnd);

	//gameloop
	while (bDone == false)
	{
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			if (msg.message == WM_QUIT)
			{
				bDone = true;
			}
			else
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}
		else
		{
			if (gbActiveWindow == true)
			{
				//update should be called here
				update();
			}
			display();
		}
	}
	return((int)msg.wParam);
}

//callback function WndProc()
LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	//function declaration
	void resize(int, int);
	void ToggleFullScreen(void);
	void uninitialize(void);

	//variables
	static GLfloat width, height;

	//code
	switch (iMsg)
	{
	case WM_SETFOCUS:
		gbActiveWindow = true;
		break;

	case WM_KILLFOCUS:
		gbActiveWindow = false;
		break;

	case WM_ERASEBKGND:
		return(0);

	case WM_SIZE:
		width = LOWORD(lParam);
		height = HIWORD(lParam);
		resize(LOWORD(lParam), HIWORD(lParam));
		break;

	case WM_CLOSE:
		DestroyWindow(hwnd);
		break;

	case WM_KEYDOWN:
		switch (LOWORD(wParam))
		{
		case VK_ESCAPE:
			DestroyWindow(hwnd);
			break;

		case 0x46:
			ToggleFullScreen();
			break;
		}
		break;

	case WM_CHAR:
		switch (LOWORD(wParam))
		{
		case 'l':
		case 'L':
			if (!bLight)
			{
				bLight = true;
				glEnable(GL_LIGHTING);
			}
			else
			{
				bLight = false;
				glDisable(GL_LIGHTING);
			}
			break;
		}
		break;

	case WM_DESTROY:
		uninitialize();
		PostQuitMessage(0);
		break;
	}
	return(DefWindowProc(hwnd, iMsg, wParam, lParam));
}

//ToggleFullScreen() function
void ToggleFullScreen(void)
{
	MONITORINFO mi;
	if (gbFullScreen == false)
	{
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);
		if (dwStyle & WS_OVERLAPPEDWINDOW)
		{
			mi = { sizeof(MONITORINFO) };
			if (GetWindowPlacement(ghwnd, &wpPrev) && GetMonitorInfo(MonitorFromWindow(ghwnd, MONITORINFOF_PRIMARY), &mi))
			{
				SetWindowLong(ghwnd, GWL_STYLE, dwStyle & ~WS_OVERLAPPEDWINDOW);
				SetWindowPos(ghwnd,
					HWND_TOP,
					mi.rcMonitor.left,
					mi.rcMonitor.top,
					mi.rcMonitor.right - mi.rcMonitor.left,
					mi.rcMonitor.bottom - mi.rcMonitor.top,
					SWP_NOZORDER | SWP_FRAMECHANGED);
			}
		}
		ShowCursor(FALSE);
		gbFullScreen = true;
	}
	else
	{
		fprintf(gpfile, "Full Screen to normal\n");
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPED);
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd,
			HWND_TOP,
			0,
			0,
			0,
			0,
			SWP_NOZORDER | SWP_FRAMECHANGED | SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER);
		ShowCursor(TRUE);
		gbFullScreen = false;
	}
}

//initialize() function
int initialize(void)
{
	//function declarations
	void resize(int, int);

	//vaiable declaraions
	PIXELFORMATDESCRIPTOR pfd;
	int iPixelFormatIndex;

	//code
	//initialize pfd structure
	ZeroMemory(&pfd, sizeof(PIXELFORMATDESCRIPTOR));
	pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
	pfd.nVersion = 1;
	pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
	pfd.iPixelType = PFD_TYPE_RGBA;
	pfd.cColorBits = 32;
	pfd.cDepthBits = 32;
	pfd.cRedBits = 8;
	pfd.cGreenBits = 8;
	pfd.cBlueBits = 8;
	pfd.cAlphaBits = 8;

	ghdc = GetDC(ghwnd);
	iPixelFormatIndex = ChoosePixelFormat(ghdc, &pfd);
	if (iPixelFormatIndex == 0)
	{
		return(-1);
	}
	if (SetPixelFormat(ghdc, iPixelFormatIndex, &pfd) == false)
	{
		return(-2);
	}
	ghrc = wglCreateContext(ghdc);
	if (ghrc == NULL)
	{
		return(-3);
	}
	if (wglMakeCurrent(ghdc, ghrc) == false)
	{
		return(-4);
	}
	glShadeModel(GL_SMOOTH);

	glClearDepth(1.0f);						//giving existance to depth buffer
	glEnable(GL_DEPTH_TEST);				// enableing depth test
	glDepthFunc(GL_LEQUAL);
	glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);

	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);	//r,g,b,a	//clear the screen by opengl color

	//setting light position and light properties LIGHT0
	glLightfv(GL_LIGHT0, GL_AMBIENT, LightAmbientZero);
	glLightfv(GL_LIGHT0, GL_DIFFUSE, LightDiffuseZero);
	glLightfv(GL_LIGHT0, GL_SPECULAR, LightSpecularZero);
	//enabling light 
	glEnable(GL_LIGHT0);

	//setting light position and light properties LIGHT1
	glLightfv(GL_LIGHT1, GL_AMBIENT, LightAmbientOne);
	glLightfv(GL_LIGHT1, GL_DIFFUSE, LightDiffuseOne);
	glLightfv(GL_LIGHT1, GL_SPECULAR, LightSpecularOne);
	//enabling light 
	glEnable(GL_LIGHT1);

	//setting light position and light properties LIGHT2
	glLightfv(GL_LIGHT2, GL_AMBIENT, LightAmbientTwo);
	glLightfv(GL_LIGHT2, GL_DIFFUSE, LightDiffuseTwo);
	glLightfv(GL_LIGHT2, GL_SPECULAR, LightSpecularTwo);
	//enabling light 
	glEnable(GL_LIGHT2);

	//setting material properties
	glMaterialfv(GL_FRONT, GL_AMBIENT, MaterialAmbient);
	glMaterialfv(GL_FRONT, GL_DIFFUSE, MaterialDiffuse);
	glMaterialfv(GL_FRONT, GL_SHININESS, MaterialShininess);
	glMaterialfv(GL_FRONT, GL_SPECULAR, MaterialSpecular);

	resize(WIN_WIDTH, WIN_HEIGHT);			//wramup call to resize
	return(0);
}

//update function
void update(void)
{
	if (LightAngleZero == 360.0f)
		LightAngleZero = 0.0f;
	LightAngleZero = LightAngleZero + 0.05f;
	if (LightAngleOne == 360.0f)
		LightAngleOne = 0.0f;
	LightAngleOne = LightAngleOne + 0.05f;
	if (LightAngleTwo == 360.0f)
		LightAngleTwo = 0.0f;
	LightAngleTwo = LightAngleTwo + 0.05f;
}

//display function
void display(void)
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glPushMatrix();
	gluLookAt(0, 0, 3, 0, 0, 0, 0, 1, 0);
	//LIGHT0
	glPushMatrix();
	glRotatef(LightAngleZero, 1.0f, 0.0f, 0.0f);	// rotation along x axis y and z will change
	LightPositionZero[1] = LightAngleZero;
	glLightfv(GL_LIGHT0, GL_POSITION, LightPositionZero);
	glPopMatrix();

	//LIGHT1
	glPushMatrix();
	glRotatef(LightAngleOne, 0.0f, 1.0f, 0.0f);	// rotation along y axis
	LightPositionOne[0] = LightAngleOne;
	glLightfv(GL_LIGHT1, GL_POSITION, LightPositionOne);
	glPopMatrix();

	//LIGHT0
	glPushMatrix();
	glRotatef(LightAngleTwo, 0.0f, 0.0f, 1.0f);	// rotation along z axis
	LightPositionTwo[0] = LightAngleTwo;
	glLightfv(GL_LIGHT2, GL_POSITION, LightPositionTwo);
	glPopMatrix();

	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	quadric_sphere = gluNewQuadric();
	gluSphere(quadric_sphere, 0.7f, 30, 30);

	glPopMatrix();

	SwapBuffers(ghdc);
}

//reize() function
void resize(int win_width, int win_height)
{
	if (win_height == 0)
		win_height = 1;
	glViewport(0, 0, (GLsizei)win_width, (GLsizei)win_height);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluPerspective(45.0f, (GLfloat)win_width / (GLfloat)win_height, 0.1f, 100.0f);
}

//uninitialize() function
void uninitialize(void)
{
	if (quadric_sphere)
	{
		gluDeleteQuadric(quadric_sphere);
		quadric_sphere = NULL;
	}
	if (gbFullScreen == true)
	{
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPED);
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd,
			HWND_TOP,
			0,
			0,
			0,
			0,
			SWP_NOZORDER | SWP_FRAMECHANGED | SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER);
		ShowCursor(TRUE);
	}

	//break the current context
	if (wglGetCurrentContext() == ghrc)
	{
		wglMakeCurrent(NULL, NULL);
	}

	if (ghrc)
	{
		wglDeleteContext(ghrc);
		ghrc = NULL;
	}

	if (ghdc)
	{
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	if (gpfile)
	{
		fprintf(gpfile, "log file closed successfully\n");
		fclose(gpfile);
		gpfile = NULL;
	}
}