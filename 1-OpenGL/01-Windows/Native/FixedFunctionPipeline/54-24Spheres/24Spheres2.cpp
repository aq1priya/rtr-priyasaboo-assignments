//	Headers
#include<windows.h>
#include<stdio.h>
#include<gl/gl.h>
#include<gl/glu.h>

#define _USE_MATH_DEFINES 1
#include<math.h>

#pragma comment (lib, "glu32.lib")
#pragma comment (lib, "opengl32.lib")

//	Macros
#define WIN_WIDTH 800
#define WIN_HEIGHT 600

//	Global function declaration
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

//	Global variables
FILE *gpfile;
HWND ghwnd = NULL;
bool gbActiveWindow = false;
bool gbFullScreen = false;
bool bLight = false;
DWORD dwStyle;
WINDOWPLACEMENT wpPrev = { sizeof(WINDOWPLACEMENT) };
HDC ghdc = NULL;
HGLRC ghrc = NULL;
GLfloat lightAmbient[4] = { 0.0f,0.0f,0.0f,1.0f };
GLfloat lightDiffuse[4] = { 1.0f,1.0f,1.0f,1.0f };
GLfloat lightSpecular[4] = { 1.0f,1.0f,1.0f,1.0f };
GLfloat lightPosition[4] = { 0.0f,0.0f,0.0f,1.0f };	//center(positional)
GLfloat Light_Model_Ambient[] = { 0.2f,0.2f,0.2f,1.0f };
GLfloat Light_Model_Local_Viewer[] = { 0.0f };
GLUquadric *quadric[24];
GLfloat angleOfXRotation = 0.0f;
GLfloat angleOfYRotation = 0.0f;
GLfloat angleOfZRotation = 0.0f;
GLint keyPress = 0;
GLUquadric *quadricSphere;

//WinMain()
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{
	//function declarations
	int initialize(void);
	void display(void);
	void update(void);

	//variable declarations
	WNDCLASSEX wndclass;
	HWND hwnd;
	MSG msg;
	TCHAR szAppName[] = TEXT("Light-2");
	bool bDone = false;
	int iRet = 0;

	//code
	if (fopen_s(&gpfile, "log.txt", "w") != 0)
	{
		MessageBox(NULL, TEXT("Log file cannot be created"), TEXT("Erroe"), MB_OK);
		exit(0);
	}

	//initialization of wndclass structure
	wndclass.cbSize = sizeof(WNDCLASSEX);
	wndclass.cbWndExtra = 0;
	wndclass.cbClsExtra = 0;
	wndclass.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
	wndclass.lpfnWndProc = WndProc;
	wndclass.hInstance = hInstance;
	wndclass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	wndclass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndclass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndclass.lpszClassName = szAppName;
	wndclass.lpszMenuName = NULL;
	wndclass.hIconSm = LoadIcon(hInstance, IDI_APPLICATION);

	//register above class 
	RegisterClassEx(&wndclass);

	//create window
	hwnd = CreateWindowEx(WS_EX_APPWINDOW,
		szAppName,
		TEXT("sphere lit by single light - PriyaSaboo"),
		WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN | WS_CLIPSIBLINGS | WS_VISIBLE,
		100,
		100,
		WIN_WIDTH,
		WIN_HEIGHT,
		NULL,
		NULL,
		hInstance,
		NULL);

	ghwnd = hwnd;
	iRet = initialize();
	if (iRet == -1)
	{
		fprintf(gpfile, "ChoosePixelFormat() Failed\n");
		DestroyWindow(hwnd);
	}
	else if (iRet == -2)
	{
		fprintf(gpfile, "SetPixelFormat() Failed\n");
		DestroyWindow(hwnd);
	}
	else if (iRet == -3)
	{
		fprintf(gpfile, "wglCreateContext() Failed\n");
		DestroyWindow(hwnd);
	}
	else if (iRet == -4)
	{
		fprintf(gpfile, "wglMakeCurrent() Failed\n");
		DestroyWindow(hwnd);
	}
	else
		fprintf(gpfile, "Initialize() Succeded\n");

	//show window
	ShowWindow(hwnd, iCmdShow);
	SetForegroundWindow(hwnd);
	SetFocus(hwnd);

	//gameloop
	while (bDone == false)
	{
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			if (msg.message == WM_QUIT)
			{
				bDone = true;
			}
			else
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}
		else
		{
			if (gbActiveWindow == true)
			{
				//update should be called here
				update();
			}
			display();
		}
	}
	return((int)msg.wParam);
}

//callback function WndProc()
LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	//function declaration
	void resize(int, int);
	void ToggleFullScreen(void);
	void uninitialize(void);

	//variables
	static GLfloat width, height;

	//code
	switch (iMsg)
	{
	case WM_SETFOCUS:
		gbActiveWindow = true;
		break;

	case WM_KILLFOCUS:
		gbActiveWindow = false;
		break;

	case WM_ERASEBKGND:
		return(0);

	case WM_SIZE:
		width = LOWORD(lParam);
		height = HIWORD(lParam);
		resize(LOWORD(lParam), HIWORD(lParam));
		break;

	case WM_CLOSE:
		DestroyWindow(hwnd);
		break;

	case WM_KEYDOWN:
		switch (LOWORD(wParam))
		{
		case VK_ESCAPE:
			DestroyWindow(hwnd);
			break;

		case 0x46:
			ToggleFullScreen();
			break;
		}
		break;
		
	case WM_CHAR:
		switch (LOWORD(wParam))
		{
		case 'l':
		case 'L':
			if (!bLight)
			{
				bLight = true;
				glEnable(GL_LIGHTING);
			}
			else
			{
				bLight = false;
				glDisable(GL_LIGHTING);
			}
			break;

		case 'x':
		case 'X':
			keyPress = 1;
			angleOfXRotation = 0.0f;
			break;

		case 'y':
		case 'Y':
			keyPress = 2;
			angleOfYRotation = 0.0f;
			break;

		case 'z':
		case 'Z':
			keyPress = 3;
			angleOfZRotation = 0.0f;
			break;
		}
		break;

	case WM_DESTROY:
		uninitialize();
		PostQuitMessage(0);
		break;
	}
	return(DefWindowProc(hwnd, iMsg, wParam, lParam));
}

//ToggleFullScreen() function
void ToggleFullScreen(void)
{
	MONITORINFO mi;
	if (gbFullScreen == false)
	{
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);
		if (dwStyle & WS_OVERLAPPEDWINDOW)
		{
			mi = { sizeof(MONITORINFO) };
			if (GetWindowPlacement(ghwnd, &wpPrev) && GetMonitorInfo(MonitorFromWindow(ghwnd, MONITORINFOF_PRIMARY), &mi))
			{
				SetWindowLong(ghwnd, GWL_STYLE, dwStyle & ~WS_OVERLAPPEDWINDOW);
				SetWindowPos(ghwnd,
					HWND_TOP,
					mi.rcMonitor.left,
					mi.rcMonitor.top,
					mi.rcMonitor.right - mi.rcMonitor.left,
					mi.rcMonitor.bottom - mi.rcMonitor.top,
					SWP_NOZORDER | SWP_FRAMECHANGED);
			}
		}
		ShowCursor(FALSE);
		gbFullScreen = true;
	}
	else
	{
		fprintf(gpfile, "Full Screen to normal\n");
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPED);
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd,
			HWND_TOP,
			0,
			0,
			0,
			0,
			SWP_NOZORDER | SWP_FRAMECHANGED | SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER);
		ShowCursor(TRUE);
		gbFullScreen = false;
	}
}

//initialize() function
int initialize(void)
{
	//function declarations
	void resize(int, int);

	//vaiable declaraions
	PIXELFORMATDESCRIPTOR pfd;
	int iPixelFormatIndex;

	//code
	//initialize pfd structure
	ZeroMemory(&pfd, sizeof(PIXELFORMATDESCRIPTOR));
	pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
	pfd.nVersion = 1;
	pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
	pfd.iPixelType = PFD_TYPE_RGBA;
	pfd.cColorBits = 32;
	pfd.cDepthBits = 32;
	pfd.cRedBits = 8;
	pfd.cGreenBits = 8;
	pfd.cBlueBits = 8;
	pfd.cAlphaBits = 8;

	ghdc = GetDC(ghwnd);
	iPixelFormatIndex = ChoosePixelFormat(ghdc, &pfd);
	if (iPixelFormatIndex == 0)
	{
		return(-1);
	}
	if (SetPixelFormat(ghdc, iPixelFormatIndex, &pfd) == false)
	{
		return(-2);
	}
	ghrc = wglCreateContext(ghdc);
	if (ghrc == NULL)
	{
		return(-3);
	}
	if (wglMakeCurrent(ghdc, ghrc) == false)
	{
		return(-4);
	}
	glShadeModel(GL_SMOOTH);

	glClearDepth(1.0f);						//giving existance to depth buffer
	glEnable(GL_DEPTH_TEST);				// enableing depth test
	glDepthFunc(GL_LEQUAL);
	glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);

	glClearColor(0.25f, 0.25f, 0.25f, 1.0f);	//r,g,b,a	//clear the screen by opengl color

	glEnable(GL_AUTO_NORMAL);
	glEnable(GL_NORMALIZE);
	glLightfv(GL_LIGHT0, GL_AMBIENT, lightAmbient);
	glLightfv(GL_LIGHT0, GL_DIFFUSE, lightDiffuse);
	glLightfv(GL_LIGHT0, GL_SPECULAR, lightSpecular);
	glLightModelfv(GL_LIGHT_MODEL_AMBIENT, Light_Model_Ambient);
	glLightModelfv(GL_LIGHT_MODEL_LOCAL_VIEWER, Light_Model_Local_Viewer);

	for (int i = 0; i < 24; i++)
	{
		quadric[i] = gluNewQuadric();
	}
	//enabling light 
	glEnable(GL_LIGHT0);

	resize(WIN_WIDTH, WIN_HEIGHT);			//wramup call to resize
	return(0);
}

//update function
void update(void)
{
	if (angleOfXRotation == 360)
		angleOfXRotation = 0.0f;
	angleOfXRotation = angleOfXRotation + 0.2f;

	if (angleOfYRotation == 360)
		angleOfYRotation = 0.0f;
	angleOfYRotation = angleOfYRotation + 0.2f;

	if (angleOfZRotation == 360)
		angleOfZRotation = 0.0f;
	angleOfZRotation = angleOfZRotation + 0.2f;
}

//display function
void display(void)
{
	//function declaration
	void draw24Spheres(void);

	//code
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(1.0f, 1.0f, 0.0f);
	if (keyPress == 1)
	{
		glRotatef(angleOfXRotation, 1.0f, 0.0f, 0.0f);
		lightPosition[1] = angleOfXRotation;
		lightPosition[0] = 0.0f;
		lightPosition[2] = 0.0f;
	}
	else if (keyPress == 2)
	{
		glRotatef(angleOfYRotation, 0.0f, 1.0f, 0.0f);
		lightPosition[2] = angleOfYRotation;
		lightPosition[0] = 0.0f;
		lightPosition[1] = 0.0f;
	}
	else if (keyPress == 3)
	{
		glRotatef(angleOfZRotation, 0.0f, 0.0f, 1.0f);
		lightPosition[0] = angleOfZRotation;
		lightPosition[1] = 0.0f;
		lightPosition[2] = 0.0f;
	}
	glLightfv(GL_LIGHT0, GL_POSITION, lightPosition);

	draw24Spheres();
	SwapBuffers(ghdc);
}

//resize() function
void resize(int width, int height)
{
	if (height == 0)
	{
		height = 1;
	}
	glViewport(0, 0, (GLsizei)width, (GLsizei)height);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	if (width <= height)
	{
		glOrtho(0.0f, 15.5f, 0.0f, 15.5f*((GLfloat)height / (GLfloat)width), -10.0f, 10.0f);
	}
	else
	{
		glOrtho(0.0f, 15.5f*((GLfloat)width / (GLfloat)height), 0.0f, 15.5f, -10.0f, 10.0f);
	}
}

//uninitialize() function
void uninitialize(void)
{
	for (int i = 0; i < 24; i++)
	{
		if (quadric[i])
		gluDeleteQuadric(quadric[i]);
		quadric[i] = NULL;
	}
	if (gbFullScreen == true)
	{
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPED);
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd,
			HWND_TOP,
			0,
			0,
			0,
			0,
			SWP_NOZORDER | SWP_FRAMECHANGED | SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER);
		ShowCursor(TRUE);
	}

	//break the current context
	if (wglGetCurrentContext() == ghrc)
	{
		wglMakeCurrent(NULL, NULL);
	}

	if (ghrc)
	{
		wglDeleteContext(ghrc);
		ghrc = NULL;
	}

	if (ghdc)
	{
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	if (gpfile)
	{
		fprintf(gpfile, "log file closed successfully\n");
		fclose(gpfile);
		gpfile = NULL;
	}
}


//draw24Spheres() function
void draw24Spheres(void)
{
	//variables
	GLfloat materialAmbient[4];
	GLfloat materialDiffuse[4];
	GLfloat materialSpecular[4];
	GLfloat materialShininess[1];

	//code
	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);

	//***** 1st Column : Gems *****
	//	1st Sphere : emarald
	//material ambient
	materialAmbient[0] = 0.0215f;
	materialAmbient[1] = 0.1745f;
	materialAmbient[2] = 0.0215f;
	materialAmbient[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_AMBIENT, materialAmbient);
	//diffuse material
	materialDiffuse[0] = 0.07568f;
	materialDiffuse[1] = 0.61424f;
	materialDiffuse[2] = 0.07568f;
	materialDiffuse[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_DIFFUSE, materialDiffuse);
	//specular material
	materialSpecular[0] = 0.633f;
	materialSpecular[1] = 0.727811f;
	materialSpecular[2] = 0.633f;
	materialSpecular[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_SPECULAR, materialSpecular);
	//shininess material
	materialShininess[0] = 0.6f*128.0f;
	glMaterialfv(GL_FRONT, GL_SHININESS, materialShininess);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(9.5f, 14.0f, 0.0f);
	gluSphere(quadric[0], 1.0f, 30, 30);

	//2nd Sphere : Jade
	//material ambient
	materialAmbient[0] = 0.135f;
	materialAmbient[1] = 0.2225f;
	materialAmbient[2] = 0.1575f;
	materialAmbient[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_AMBIENT, materialAmbient);
	//diffuse material
	materialDiffuse[0] = 0.54f;
	materialDiffuse[1] = 0.89f;
	materialDiffuse[2] = 0.63f;
	materialDiffuse[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_DIFFUSE, materialDiffuse);
	//specular material
	materialSpecular[0] = 0.316228f;
	materialSpecular[1] = 0.316228f;
	materialSpecular[2] = 0.316228f;
	materialSpecular[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_SPECULAR, materialSpecular);
	//shininess material
	materialShininess[0] = 0.1f*128.0f;
	glMaterialfv(GL_FRONT, GL_SHININESS, materialShininess);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(9.5f, 11.5f, 0.0f);
	gluSphere(quadric[1], 1.0f, 30, 30);

	// 3rd Sphere : Obsidian
	//ambient material
	materialAmbient[0] = 0.05375f;
	materialAmbient[1] = 0.05f;
	materialAmbient[2] = 0.06625f;
	materialAmbient[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_AMBIENT, materialAmbient);
	//diffuse material
	materialDiffuse[0] = 0.18275f;
	materialDiffuse[1] = 0.17f;
	materialDiffuse[2] = 0.22525f;
	materialDiffuse[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_DIFFUSE, materialDiffuse);
	//specular material
	materialSpecular[0] = 0.332741f;
	materialSpecular[1] = 0.328634f;
	materialSpecular[2] = 0.346435f;
	materialSpecular[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_SPECULAR, materialSpecular);
	//shininess
	materialShininess[0] = 0.3f*128.0f;
	glMaterialfv(GL_FRONT, GL_SHININESS, materialShininess);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(9.5f, 9.0f, 0.0f);
	gluSphere(quadric[2], 1.0f, 30, 30);

	//4th Sphere : Pearl
	//ambient material
	materialAmbient[0] = 0.25f;
	materialAmbient[1] = 0.20725f;
	materialAmbient[2] = 0.20725f;
	materialAmbient[3] = 1.0f;
	//material diffuse
	materialDiffuse[0] = 1.0f;
	materialDiffuse[1] = 0.829f;
	materialDiffuse[2] = 0.829f;
	materialDiffuse[3] = 1.0f;
	//material specular
	materialSpecular[0] = 0.296648f;
	materialSpecular[0] = 0.296648f;
	materialSpecular[0] = 0.296648f;
	materialSpecular[0] = 1.0f;
	//shininess
	materialShininess[0] = 0.088f*128.0f;
	glMaterialfv(GL_FRONT, GL_AMBIENT, materialAmbient);
	glMaterialfv(GL_FRONT, GL_DIFFUSE, materialDiffuse);
	glMaterialfv(GL_FRONT, GL_SPECULAR, materialSpecular);
	glMaterialfv(GL_FRONT, GL_SHININESS, materialShininess);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(9.5f, 6.5f, 0.0f);
	gluSphere(quadric[3], 1.0f, 30, 30);

	//5th Sphere : Ruby
	//ambient material
	materialAmbient[0] = 0.1745f;
	materialAmbient[1] = 0.01175f;
	materialAmbient[2] = 0.01175f;
	materialAmbient[3] = 1.0f;
	//material diffuse
	materialDiffuse[0] = 0.61424f;
	materialDiffuse[1] = 0.04136f;
	materialDiffuse[2] = 0.04136f;
	materialDiffuse[3] = 1.0f;
	//material specular
	materialSpecular[0] = 0.727811f;
	materialSpecular[0] = 0.626959f;
	materialSpecular[0] = 0.626959f;
	materialSpecular[0] = 1.0f;
	//shininess
	materialShininess[0] = 0.6f*128.0f;
	glMaterialfv(GL_FRONT, GL_AMBIENT, materialAmbient);
	glMaterialfv(GL_FRONT, GL_DIFFUSE, materialDiffuse);
	glMaterialfv(GL_FRONT, GL_SPECULAR, materialSpecular);
	glMaterialfv(GL_FRONT, GL_SHININESS, materialShininess);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(9.5f, 4.0f, 0.0f);
	gluSphere(quadric[4], 1.0f, 30, 30);

	//6th Sphere : turquoise
	//ambient material
	materialAmbient[0] = 0.1f;
	materialAmbient[1] = 0.18725f;
	materialAmbient[2] = 0.1745f;
	materialAmbient[3] = 1.0f;
	//material diffuse
	materialDiffuse[0] = 0.396f;
	materialDiffuse[1] = 0.74151f;
	materialDiffuse[2] = 0.69102f;
	materialDiffuse[3] = 1.0f;
	//material specular
	materialSpecular[0] = 0.297254f;
	materialSpecular[0] = 0.30829f;
	materialSpecular[0] = 0.306678f;
	materialSpecular[0] = 1.0f;
	//shininess
	materialShininess[0] = 0.1f*128.0f;
	glMaterialfv(GL_FRONT, GL_AMBIENT, materialAmbient);
	glMaterialfv(GL_FRONT, GL_DIFFUSE, materialDiffuse);
	glMaterialfv(GL_FRONT, GL_SPECULAR, materialSpecular);
	glMaterialfv(GL_FRONT, GL_SHININESS, materialShininess);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(9.5f, 1.5f, 0.0f);
	gluSphere(quadric[5], 1.0f, 30, 30);

	// ***** 2nd column : Metal *****
	//	1st Sphere : brass
	materialAmbient[0] = 0.329412f;
	materialAmbient[1] = 0.223529f;
	materialAmbient[2] = 0.027451f;
	materialAmbient[3] = 1.0f;
	//material diffuse
	materialDiffuse[0] = 0.780392f;
	materialDiffuse[1] = 0.568627f;
	materialDiffuse[2] = 0.113725f;
	materialDiffuse[3] = 1.0f;
	//material specular
	materialSpecular[0] = 0.992157f;
	materialSpecular[0] = 0.941176f;
	materialSpecular[0] = 0.807843f;
	materialSpecular[0] = 1.0f;
	//shininess
	materialShininess[0] = 0.21794872f*128.0f;
	glMaterialfv(GL_FRONT, GL_AMBIENT, materialAmbient);
	glMaterialfv(GL_FRONT, GL_DIFFUSE, materialDiffuse);
	glMaterialfv(GL_FRONT, GL_SPECULAR, materialSpecular);
	glMaterialfv(GL_FRONT, GL_SHININESS, materialShininess);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(12.5f, 14.0f, 0.0f);
	gluSphere(quadric[6], 1.0f, 30, 30);

	//	2nd Sphere : Bronze
	//ambient material
	materialAmbient[0] = 0.2125f;
	materialAmbient[1] = 0.1275f;
	materialAmbient[2] = 0.054f;
	materialAmbient[3] = 1.0f;
	//material diffuse
	materialDiffuse[0] = 0.714f;
	materialDiffuse[1] = 0.4284f;
	materialDiffuse[2] = 0.18144f;
	materialDiffuse[3] = 1.0f;
	//material specular
	materialSpecular[0] = 0.393548f;
	materialSpecular[0] = 0.271906f;
	materialSpecular[0] = 0.166721f;
	materialSpecular[0] = 1.0f;
	//shininess
	materialShininess[0] = 0.2f*128.0f;
	glMaterialfv(GL_FRONT, GL_AMBIENT, materialAmbient);
	glMaterialfv(GL_FRONT, GL_DIFFUSE, materialDiffuse);
	glMaterialfv(GL_FRONT, GL_SPECULAR, materialSpecular);
	glMaterialfv(GL_FRONT, GL_SHININESS, materialShininess);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(12.5f, 11.5f, 0.0f);
	gluSphere(quadric[7], 1.0f, 30, 30);

	//	3rd Sphere : Chrome
	//ambient material
	materialAmbient[0] = 0.25f;
	materialAmbient[1] = 0.25f;
	materialAmbient[2] = 0.25f;
	materialAmbient[3] = 1.0f;
	//material diffuse
	materialDiffuse[0] = 0.4f;
	materialDiffuse[1] = 0.4f;
	materialDiffuse[2] = 0.4f;
	materialDiffuse[3] = 1.0f;
	//material specular
	materialSpecular[0] = 0.774597f;
	materialSpecular[0] = 0.774597f;
	materialSpecular[0] = 0.774597f;
	materialSpecular[0] = 1.0f;
	//shininess
	materialShininess[0] = 0.6f*128.0f;
	glMaterialfv(GL_FRONT, GL_AMBIENT, materialAmbient);
	glMaterialfv(GL_FRONT, GL_DIFFUSE, materialDiffuse);
	glMaterialfv(GL_FRONT, GL_SPECULAR, materialSpecular);
	glMaterialfv(GL_FRONT, GL_SHININESS, materialShininess);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(12.5f, 9.0f, 0.0f);
	gluSphere(quadric[8], 1.0f, 30, 30);

	//	4th Sphere : Copper
	//ambient material
	materialAmbient[0] = 0.19125f;
	materialAmbient[1] = 0.0735f;
	materialAmbient[2] = 0.0225f;
	materialAmbient[3] = 1.0f;
	//material diffuse
	materialDiffuse[0] = 0.7038f;
	materialDiffuse[1] = 0.27048f;
	materialDiffuse[2] = 0.0828f;
	materialDiffuse[3] = 1.0f;
	//material specular
	materialSpecular[0] = 0.256777f;
	materialSpecular[0] = 0.137622f;
	materialSpecular[0] = 0.086014f;
	materialSpecular[0] = 1.0f;
	//shininess
	materialShininess[0] = 0.1f*128.0f;
	glMaterialfv(GL_FRONT, GL_AMBIENT, materialAmbient);
	glMaterialfv(GL_FRONT, GL_DIFFUSE, materialDiffuse);
	glMaterialfv(GL_FRONT, GL_SPECULAR, materialSpecular);
	glMaterialfv(GL_FRONT, GL_SHININESS, materialShininess);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(12.5f, 6.5f, 0.0f);
	gluSphere(quadric[9], 1.0f, 30, 30);

	// 5th Sphere : Gold
	//ambient material
	materialAmbient[0] = 0.24725f;
	materialAmbient[1] = 0.1995f;
	materialAmbient[2] = 0.0745f;
	materialAmbient[3] = 1.0f;
	//material diffuse
	materialDiffuse[0] = 0.75164f;
	materialDiffuse[1] = 0.60648f;
	materialDiffuse[2] = 0.22648f;
	materialDiffuse[3] = 1.0f;
	//material specular
	materialSpecular[0] = 0.628281f;
	materialSpecular[0] = 0.555802f;
	materialSpecular[0] = 0.366065f;
	materialSpecular[0] = 1.0f;
	//shininess
	materialShininess[0] = 0.4f*128.0f;
	glMaterialfv(GL_FRONT, GL_AMBIENT, materialAmbient);
	glMaterialfv(GL_FRONT, GL_DIFFUSE, materialDiffuse);
	glMaterialfv(GL_FRONT, GL_SPECULAR, materialSpecular);
	glMaterialfv(GL_FRONT, GL_SHININESS, materialShininess);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(12.5f, 4.0f, 0.0f);
	gluSphere(quadric[10], 1.0f, 30, 30);

	//	6th Sphere : Silver
	//ambient material
	materialAmbient[0] = 0.19225f;
	materialAmbient[1] = 0.19225f;
	materialAmbient[2] = 0.19225f;
	materialAmbient[3] = 1.0f;
	//material diffuse
	materialDiffuse[0] = 0.50754f;
	materialDiffuse[1] = 0.50754f;
	materialDiffuse[2] = 0.50754f;
	materialDiffuse[3] = 1.0f;
	//material specular
	materialSpecular[0] = 0.508273f;
	materialSpecular[0] = 0.508273f;
	materialSpecular[0] = 0.508273f;
	materialSpecular[0] = 1.0f;
	//shininess
	materialShininess[0] = 0.4f*128.0f;
	glMaterialfv(GL_FRONT, GL_AMBIENT, materialAmbient);
	glMaterialfv(GL_FRONT, GL_DIFFUSE, materialDiffuse);
	glMaterialfv(GL_FRONT, GL_SPECULAR, materialSpecular);
	glMaterialfv(GL_FRONT, GL_SHININESS, materialShininess);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(12.5f, 1.5f, 0.0f);
	gluSphere(quadric[11], 1.0f, 30, 30);

	// ***** 3rd column : Plastic *****
	//	1st Sphere : Black Plastic
	materialAmbient[0] = 0.0f;
	materialAmbient[1] = 0.0f;
	materialAmbient[2] = 0.0f;
	materialAmbient[3] = 1.0f;
	//material diffuse
	materialDiffuse[0] = 0.01f;
	materialDiffuse[1] = 0.01f;
	materialDiffuse[2] = 0.01f;
	materialDiffuse[3] = 1.0f;
	//material specular
	materialSpecular[0] = 0.50f;
	materialSpecular[0] = 0.50f;
	materialSpecular[0] = 0.50f;
	materialSpecular[0] = 1.0f;
	//shininess
	materialShininess[0] = 0.25f*128.0f;
	glMaterialfv(GL_FRONT, GL_AMBIENT, materialAmbient);
	glMaterialfv(GL_FRONT, GL_DIFFUSE, materialDiffuse);
	glMaterialfv(GL_FRONT, GL_SPECULAR, materialSpecular);
	glMaterialfv(GL_FRONT, GL_SHININESS, materialShininess);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(15.5f, 14.0f, 0.0f);
	gluSphere(quadric[12], 1.0f, 30, 30);

	//	2nd Sphere : Cyan Plastic
	//ambient material
	materialAmbient[0] = 0.0f;
	materialAmbient[1] = 0.1f;
	materialAmbient[2] = 0.06f;
	materialAmbient[3] = 1.0f;
	//material diffuse
	materialDiffuse[0] = 0.0f;
	materialDiffuse[1] = 0.50980392f;
	materialDiffuse[2] = 0.50980392f;
	materialDiffuse[3] = 1.0f;
	//material specular
	materialSpecular[0] = 0.50196078f;
	materialSpecular[0] = 0.50196078f;
	materialSpecular[0] = 0.50196078f;
	materialSpecular[0] = 1.0f;
	//shininess
	materialShininess[0] = 0.25f*128.0f;
	glMaterialfv(GL_FRONT, GL_AMBIENT, materialAmbient);
	glMaterialfv(GL_FRONT, GL_DIFFUSE, materialDiffuse);
	glMaterialfv(GL_FRONT, GL_SPECULAR, materialSpecular);
	glMaterialfv(GL_FRONT, GL_SHININESS, materialShininess);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(15.5f, 11.5f, 0.0f);
	gluSphere(quadric[13], 1.0f, 30, 30);

	//	3rd Sphere : Green Plastic
	//ambient material
	materialAmbient[0] = 0.0f;
	materialAmbient[1] = 0.0f;
	materialAmbient[2] = 0.0f;
	materialAmbient[3] = 1.0f;
	//material diffuse
	materialDiffuse[0] = 0.1f;
	materialDiffuse[1] = 0.35f;
	materialDiffuse[2] = 0.1f;
	materialDiffuse[3] = 1.0f;
	//material specular
	materialSpecular[0] = 0.45f;
	materialSpecular[0] = 0.55f;
	materialSpecular[0] = 0.45f;
	materialSpecular[0] = 1.0f;
	//shininess
	materialShininess[0] = 0.25f*128.0f;
	glMaterialfv(GL_FRONT, GL_AMBIENT, materialAmbient);
	glMaterialfv(GL_FRONT, GL_DIFFUSE, materialDiffuse);
	glMaterialfv(GL_FRONT, GL_SPECULAR, materialSpecular);
	glMaterialfv(GL_FRONT, GL_SHININESS, materialShininess);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(15.5f, 9.0f, 0.0f);
	gluSphere(quadric[14], 1.0f, 30, 30);

	//	4th Sphere : Red Plastic
	//ambient material
	materialAmbient[0] = 0.0f;
	materialAmbient[1] = 0.0f;
	materialAmbient[2] = 0.0f;
	materialAmbient[3] = 1.0f;
	//material diffuse
	materialDiffuse[0] = 0.5f;
	materialDiffuse[1] = 0.0f;
	materialDiffuse[2] = 0.0f;
	materialDiffuse[3] = 1.0f;
	//material specular
	materialSpecular[0] = 0.7f;
	materialSpecular[0] = 0.6f;
	materialSpecular[0] = 0.6f;
	materialSpecular[0] = 1.0f;
	//shininess
	materialShininess[0] = 0.25f*128.0f;
	glMaterialfv(GL_FRONT, GL_AMBIENT, materialAmbient);
	glMaterialfv(GL_FRONT, GL_DIFFUSE, materialDiffuse);
	glMaterialfv(GL_FRONT, GL_SPECULAR, materialSpecular);
	glMaterialfv(GL_FRONT, GL_SHININESS, materialShininess);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(15.5f, 6.5f, 0.0f);
	gluSphere(quadric[15], 1.0f, 30, 30);

	// 5th Sphere : White Plastic
	//ambient material
	materialAmbient[0] = 0.0f;
	materialAmbient[1] = 0.0f;
	materialAmbient[2] = 0.0f;
	materialAmbient[3] = 1.0f;
	//material diffuse
	materialDiffuse[0] = 0.55f;
	materialDiffuse[1] = 0.55f;
	materialDiffuse[2] = 0.55f;
	materialDiffuse[3] = 1.0f;
	//material specular
	materialSpecular[0] = 0.70f;
	materialSpecular[0] = 0.70f;
	materialSpecular[0] = 0.70f;
	materialSpecular[0] = 1.0f;
	//shininess
	materialShininess[0] = 0.25f*128.0f;
	glMaterialfv(GL_FRONT, GL_AMBIENT, materialAmbient);
	glMaterialfv(GL_FRONT, GL_DIFFUSE, materialDiffuse);
	glMaterialfv(GL_FRONT, GL_SPECULAR, materialSpecular);
	glMaterialfv(GL_FRONT, GL_SHININESS, materialShininess);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(15.5f, 4.0f, 0.0f);
	gluSphere(quadric[16], 1.0f, 30, 30);

	//	6th Sphere : Yellow Plastic
	//ambient material
	materialAmbient[0] = 0.0f;
	materialAmbient[1] = 0.0f;
	materialAmbient[2] = 0.0f;
	materialAmbient[3] = 1.0f;
	//material diffuse
	materialDiffuse[0] = 0.5f;
	materialDiffuse[1] = 0.5f;
	materialDiffuse[2] = 0.0f;
	materialDiffuse[3] = 1.0f;
	//material specular
	materialSpecular[0] = 0.60f;
	materialSpecular[0] = 0.60f;
	materialSpecular[0] = 0.50f;
	materialSpecular[0] = 1.0f;
	//shininess
	materialShininess[0] = 0.25f*128.0f;
	glMaterialfv(GL_FRONT, GL_AMBIENT, materialAmbient);
	glMaterialfv(GL_FRONT, GL_DIFFUSE, materialDiffuse);
	glMaterialfv(GL_FRONT, GL_SPECULAR, materialSpecular);
	glMaterialfv(GL_FRONT, GL_SHININESS, materialShininess);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(15.5f, 1.5f, 0.0f);
	gluSphere(quadric[17], 1.0f, 30, 30);

	// ***** 4th column : Rubber *****
	//	1st Sphere : Black Rubber
	materialAmbient[0] = 0.02f;
	materialAmbient[1] = 0.02f;
	materialAmbient[2] = 0.02f;
	materialAmbient[3] = 1.0f;
	//material diffuse
	materialDiffuse[0] = 0.01f;
	materialDiffuse[1] = 0.01f;
	materialDiffuse[2] = 0.01f;
	materialDiffuse[3] = 1.0f;
	//material specular
	materialSpecular[0] = 0.4f;
	materialSpecular[0] = 0.4f;
	materialSpecular[0] = 0.4f;
	materialSpecular[0] = 1.0f;
	//shininess
	materialShininess[0] = 0.078125f*128.0f;
	glMaterialfv(GL_FRONT, GL_AMBIENT, materialAmbient);
	glMaterialfv(GL_FRONT, GL_DIFFUSE, materialDiffuse);
	glMaterialfv(GL_FRONT, GL_SPECULAR, materialSpecular);
	glMaterialfv(GL_FRONT, GL_SHININESS, materialShininess);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(18.5f, 14.0f, 0.0f);
	gluSphere(quadric[18], 1.0f, 30, 30);

	//	2nd Sphere : Cyan Rubber
	//ambient material
	materialAmbient[0] = 0.0f;
	materialAmbient[1] = 0.05f;
	materialAmbient[2] = 0.05f;
	materialAmbient[3] = 1.0f;
	//material diffuse
	materialDiffuse[0] = 0.4f;
	materialDiffuse[1] = 0.5f;
	materialDiffuse[2] = 0.5f;
	materialDiffuse[3] = 1.0f;
	//material specular
	materialSpecular[0] = 0.04f;
	materialSpecular[0] = 0.7f;
	materialSpecular[0] = 0.7f;
	materialSpecular[0] = 1.0f;
	//shininess
	materialShininess[0] = 0.078125f*128.0f;
	glMaterialfv(GL_FRONT, GL_AMBIENT, materialAmbient);
	glMaterialfv(GL_FRONT, GL_DIFFUSE, materialDiffuse);
	glMaterialfv(GL_FRONT, GL_SPECULAR, materialSpecular);
	glMaterialfv(GL_FRONT, GL_SHININESS, materialShininess);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(18.5f, 11.5f, 0.0f);
	gluSphere(quadric[19], 1.0f, 30, 30);

	//	3rd Sphere : Green Rubber
	//ambient material
	materialAmbient[0] = 0.0f;
	materialAmbient[1] = 0.05f;
	materialAmbient[2] = 0.0f;
	materialAmbient[3] = 1.0f;
	//material diffuse
	materialDiffuse[0] = 0.4f;
	materialDiffuse[1] = 0.5f;
	materialDiffuse[2] = 0.4f;
	materialDiffuse[3] = 1.0f;
	//material specular
	materialSpecular[0] = 0.04f;
	materialSpecular[0] = 0.7f;
	materialSpecular[0] = 0.04f;
	materialSpecular[0] = 1.0f;
	//shininess
	materialShininess[0] = 0.078125f*128.0f;
	glMaterialfv(GL_FRONT, GL_AMBIENT, materialAmbient);
	glMaterialfv(GL_FRONT, GL_DIFFUSE, materialDiffuse);
	glMaterialfv(GL_FRONT, GL_SPECULAR, materialSpecular);
	glMaterialfv(GL_FRONT, GL_SHININESS, materialShininess);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(18.5f, 9.0f, 0.0f);
	gluSphere(quadric[20], 1.0f, 30, 30);

	//	4th Sphere : Red Rubber
	//ambient material
	materialAmbient[0] = 0.05f;
	materialAmbient[1] = 0.0f;
	materialAmbient[2] = 0.0f;
	materialAmbient[3] = 1.0f;
	//material diffuse
	materialDiffuse[0] = 0.5f;
	materialDiffuse[1] = 0.4f;
	materialDiffuse[2] = 0.4f;
	materialDiffuse[3] = 1.0f;
	//material specular
	materialSpecular[0] = 0.7f;
	materialSpecular[0] = 0.04f;
	materialSpecular[0] = 0.04f;
	materialSpecular[0] = 1.0f;
	//shininess
	materialShininess[0] = 0.078125f*128.0f;
	glMaterialfv(GL_FRONT, GL_AMBIENT, materialAmbient);
	glMaterialfv(GL_FRONT, GL_DIFFUSE, materialDiffuse);
	glMaterialfv(GL_FRONT, GL_SPECULAR, materialSpecular);
	glMaterialfv(GL_FRONT, GL_SHININESS, materialShininess);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(18.5f, 6.5f, 0.0f);
	gluSphere(quadric[21], 1.0f, 30, 30);

	// 5th Sphere : White Rubber
	//ambient material
	materialAmbient[0] = 0.05f;
	materialAmbient[1] = 0.05f;
	materialAmbient[2] = 0.05f;
	materialAmbient[3] = 1.0f;
	//material diffuse
	materialDiffuse[0] = 0.5f;
	materialDiffuse[1] = 0.5f;
	materialDiffuse[2] = 0.5f;
	materialDiffuse[3] = 1.0f;
	//material specular
	materialSpecular[0] = 0.70f;
	materialSpecular[0] = 0.70f;
	materialSpecular[0] = 0.70f;
	materialSpecular[0] = 1.0f;
	//shininess
	materialShininess[0] = 0.078125f*128.0f;
	glMaterialfv(GL_FRONT, GL_AMBIENT, materialAmbient);
	glMaterialfv(GL_FRONT, GL_DIFFUSE, materialDiffuse);
	glMaterialfv(GL_FRONT, GL_SPECULAR, materialSpecular);
	glMaterialfv(GL_FRONT, GL_SHININESS, materialShininess);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(18.5f, 4.0f, 0.0f);
	gluSphere(quadric[22], 1.0f, 30, 30);

	//	6th Sphere : Yellow Rubber
	//ambient material
	materialAmbient[0] = 0.05f;
	materialAmbient[1] = 0.05f;
	materialAmbient[2] = 0.0f;
	materialAmbient[3] = 1.0f;
	//material diffuse
	materialDiffuse[0] = 0.5f;
	materialDiffuse[1] = 0.5f;
	materialDiffuse[2] = 0.4f;
	materialDiffuse[3] = 1.0f;
	//material specular
	materialSpecular[0] = 0.70f;
	materialSpecular[0] = 0.70f;
	materialSpecular[0] = 0.04f;
	materialSpecular[0] = 1.0f;
	//shininess
	materialShininess[0] = 0.078125f*128.0f;
	glMaterialfv(GL_FRONT, GL_AMBIENT, materialAmbient);
	glMaterialfv(GL_FRONT, GL_DIFFUSE, materialDiffuse);
	glMaterialfv(GL_FRONT, GL_SPECULAR, materialSpecular);
	glMaterialfv(GL_FRONT, GL_SHININESS, materialShininess);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(18.5f, 1.5f, 0.0f);
	gluSphere(quadric[23], 1.0f, 30, 30);
}
