//	Headers
#include<windows.h>
#include<stdio.h>
#include<gl/gl.h>
#include<gl/glu.h>
#include"AudioResource.h"
#define _USE_MATH_DEFINES 1
#include<math.h>

#pragma comment (lib, "glu32.lib")
#pragma comment (lib, "opengl32.lib")
#pragma comment(lib, "Winmm.lib")

//	Macros
#define WIN_WIDTH 800
#define WIN_HEIGHT 600

//	Global function declaration
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

//	Global variables
FILE *gpfile;
HWND ghwnd = NULL;
bool gbActiveWindow = false;
bool gbFullScreen = true;
DWORD dwStyle;
WINDOWPLACEMENT wpPrev = { sizeof(WINDOWPLACEMENT) };
HDC ghdc = NULL;
HGLRC ghrc = NULL;
bool bDrawI1 = true;

//	WinMain()
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{
	//function declarations
	int initialize(void);
	void display(void);
	void FullScreen(void);

	//variable declaration
	int iRet;
	WNDCLASSEX wndclass;
	MSG msg;
	HWND hwnd;
	TCHAR szAppName[] = TEXT("StaticIndia");
	BOOL bDone = false;

	//code
	if (fopen_s(&gpfile, "log.txt", "w") != 0)
	{
		MessageBox(NULL, TEXT("Log file cannot be created"), TEXT("Erroe"), MB_OK);
		exit(0);
	}

	//	Initialization of wndclass
	wndclass.cbSize = sizeof(WNDCLASSEX);
	wndclass.cbWndExtra = 0;
	wndclass.cbClsExtra = 0;
	wndclass.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
	wndclass.lpfnWndProc = WndProc;
	wndclass.hInstance = hInstance;
	wndclass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	wndclass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndclass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndclass.lpszClassName = szAppName;
	wndclass.lpszMenuName = NULL;
	wndclass.hIconSm = LoadIcon(hInstance, IDI_APPLICATION);

	//register above class 
	RegisterClassEx(&wndclass);

	//create window
	hwnd = CreateWindowEx(WS_EX_APPWINDOW,
		szAppName,
		TEXT("Dynamic India - Priya Saboo"),
		WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN | WS_CLIPSIBLINGS | WS_VISIBLE,
		100,
		100,
		WIN_WIDTH,
		WIN_HEIGHT,
		NULL,
		NULL,
		hInstance,
		NULL);

	ghwnd = hwnd;
	iRet = initialize();
	if (iRet == -1)
	{
		fprintf(gpfile, "ChoosePixelFormat() Failed\n");
		DestroyWindow(hwnd);
	}
	else if (iRet == -2)
	{
		fprintf(gpfile, "SetPixelFormat() Failed\n");
		DestroyWindow(hwnd);
	}
	else if (iRet == -3)
	{
		fprintf(gpfile, "wglCreateContext() Failed\n");
		DestroyWindow(hwnd);
	}
	else if (iRet == -4)
	{
		fprintf(gpfile, "wglMakeCurrent() Failed\n");
		DestroyWindow(hwnd);
	}
	else
		fprintf(gpfile, "Initialize() Succeded\n");

	//show window
	ShowWindow(hwnd, iCmdShow);
	SetForegroundWindow(hwnd);
	SetFocus(hwnd);
	FullScreen();
	//pLAY SOUND
	if (!PlaySound(MAKEINTRESOURCE(MY_AUDIO), hInstance, SND_RESOURCE | SND_ASYNC | SND_MEMORY | SND_NODEFAULT))
		fprintf(gpfile, "Cannot play the audio file, PlaySound() failed");

	//gameloop
	while (bDone == false)
	{
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			if (msg.message == WM_QUIT)
			{
				bDone = true;
			}
			else
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}
		else
		{
			if (gbActiveWindow == true)
			{
				//update should be called here
			}
			display();
		}
	}
	return((int)msg.wParam);
}

//callback function WndProc()
LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	//function declaration
	void resize(int, int);
	void FullScreen(void);
	void uninitialize(void);

	//variables
	static GLfloat width, height;

	//code
	HINSTANCE hInst = (HINSTANCE)GetWindowLong(hwnd, GWL_HINSTANCE);
	switch (iMsg)
	{
	case WM_SETFOCUS:
		gbActiveWindow = true;
		break;

	case WM_KILLFOCUS:
		gbActiveWindow = false;
		break;

	case WM_ERASEBKGND:
		return(0);

	case WM_SIZE:
		width = LOWORD(lParam);
		height = HIWORD(lParam);
		resize(LOWORD(lParam), HIWORD(lParam));
		break;

	case WM_CLOSE:
		DestroyWindow(hwnd);
		break;

	case WM_KEYDOWN:
		switch (LOWORD(wParam))
		{
		case VK_ESCAPE:
			DestroyWindow(hwnd);
			break;

		case 0x46:
			FullScreen();
			break;
		}
		break;

	case WM_DESTROY:
		uninitialize();
		PostQuitMessage(0);
		break;
	}
	return(DefWindowProc(hwnd, iMsg, wParam, lParam));
}

//FullScreen() function
void FullScreen(void)
{
	MONITORINFO mi;
	if (gbFullScreen)
	{
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);
		if (dwStyle & WS_OVERLAPPEDWINDOW)
		{
			mi = { sizeof(MONITORINFO) };
			if (GetWindowPlacement(ghwnd, &wpPrev) && GetMonitorInfo(MonitorFromWindow(ghwnd, MONITORINFOF_PRIMARY), &mi))
			{
				SetWindowLong(ghwnd, GWL_STYLE, dwStyle & ~WS_OVERLAPPEDWINDOW);
				SetWindowPos(ghwnd,
					HWND_TOP,
					mi.rcMonitor.left,
					mi.rcMonitor.top,
					mi.rcMonitor.right - mi.rcMonitor.left,
					mi.rcMonitor.bottom - mi.rcMonitor.top,
					SWP_NOZORDER | SWP_FRAMECHANGED);
			}
		}
		ShowCursor(FALSE);
	}
}

//initialize() function
int initialize(void)
{
	//function declarations
	void resize(int, int);

	//vaiable declaraions
	PIXELFORMATDESCRIPTOR pfd;
	int iPixelFormatIndex;

	//code
	//initialize pfd structure
	ZeroMemory(&pfd, sizeof(PIXELFORMATDESCRIPTOR));
	pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
	pfd.nVersion = 1;
	pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
	pfd.iPixelType = PFD_TYPE_RGBA;
	pfd.cColorBits = 32;
	pfd.cRedBits = 8;
	pfd.cGreenBits = 8;
	pfd.cBlueBits = 8;
	pfd.cAlphaBits = 8;

	ghdc = GetDC(ghwnd);
	iPixelFormatIndex = ChoosePixelFormat(ghdc, &pfd);
	if (iPixelFormatIndex == 0)
	{
		return(-1);
	}
	if (SetPixelFormat(ghdc, iPixelFormatIndex, &pfd) == false)
	{
		return(-2);
	}
	ghrc = wglCreateContext(ghdc);
	if (ghrc == NULL)
	{
		return(-3);
	}
	if (wglMakeCurrent(ghdc, ghrc) == false)
	{
		return(-4);
	}

	//clear the screen by opengl color
	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);	//r,g,b,a

	//wramup call to resize
	resize(WIN_WIDTH, WIN_HEIGHT);
	return(0);
}

//Display()
void display(void)
{
	//function declarations
	void Draw_I(void);
	void Draw_N(void);
	void Draw_D(GLfloat);
	void Draw_I2(void);
	void Draw_A(void);
	void FighterPlane(void);
	void TrailOfMiddlePlane(GLfloat, GLfloat, GLfloat);
	void TrailOfConvergingPlane(GLfloat, GLfloat, GLfloat);
	void TrailOfDivergingPlane(GLfloat, GLfloat, GLfloat);
	void Draw_Tricolor(void);

	//variable declarations
	bool bDrawN = false;
	bool bDrawD = false;
	bool bDrawI2 = false;
	bool bDrawA = false;
	
	static bool bDrawPlane = false;
	static bool bConverge = true;
	static bool bStraight = false;
	static bool bDiverge = false;
	static bool bStartFadeOut = false;
	static GLfloat Translation_Of_I = -1.2f;
	static GLfloat Translation_Of_A = 1.2f;
	static GLfloat Translation_Of_N = -1.1f;
	static GLfloat Translation_Of_I2 = 1.1f;
	static GLfloat Color_Brightness = 0.0f;
	static GLfloat Color_Brightness1 = 1.0f;
	static GLfloat M_x = -3.0f;	//transition of middle plane
	static GLfloat Para_x = -1.5f, Para_y = 0.0f;
	static GLfloat Planerotation = -60.0f;
	static GLfloat Planerotation1 = -300.0f;
	static GLfloat AlphaValue = 1.0f;
	GLfloat temp = 0.0f;

	glClear(GL_COLOR_BUFFER_BIT);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(0.0f, 0.0f, -4.0f);
	
	glLineWidth(10.0f);
	if (bDrawI1)
	{
		glLoadIdentity();
		glTranslatef(Translation_Of_I, 0.0f, -3.0f);
		Draw_I();
		if (Translation_Of_I <= 0.0f)
			Translation_Of_I += 0.008f;
		else
			bDrawA = true;
	}
	if (bDrawA)
	{
		glLoadIdentity();
		glTranslatef(Translation_Of_A, 0.0f, -3.0f);
		Draw_A();
		if (Translation_Of_A >= 0.0f)
			Translation_Of_A -= 0.008f;
		else
			bDrawN = true;
	}
	if (bDrawN)
	{
		glLoadIdentity();
		glTranslatef(0.0f, Translation_Of_N, -3.0f);
		Draw_N();
		if (Translation_Of_N <= 0.0f)
			Translation_Of_N += 0.008f;
		else
			bDrawI2 = true;
	}

	if (bDrawI2)
	{
		glLoadIdentity();
		glTranslatef(0.0f, Translation_Of_I2, -3.0f);
		Draw_I2();
		if (Translation_Of_I2 >= 0.0f)
			Translation_Of_I2 -= 0.008f;
		else
			bDrawD = true;
	}
	if (bDrawD)
	{
		glLoadIdentity();
		glTranslatef(0.0f, 0.0f, -3.0f);
		Draw_D(Color_Brightness);
		if (Color_Brightness < 1.0f)
			Color_Brightness += 0.005f;
		else
		{
			bDrawPlane = true;
		}

	}
	//bDrawPlane = true;
	if (bDrawPlane)
	{
		glLoadIdentity();
		glTranslatef(0.0f, 0.0f, -3.5f);
		if (bConverge)
		{
			if (Para_x < 0.0f)
			{

				Para_y = 1.2f * (GLfloat)pow(Para_x, 2);
				Para_x = Para_x + 0.005f;
				TrailOfConvergingPlane(Para_x, 1.0f, 1.0f);
			}
			if (Para_x > 0.000000f)
			{
				bConverge = false;
				bDiverge = false;
				bStraight = true;
				temp = Para_x;
			}
		}

		if (bStraight)
		{
			Para_y = 0.000000f;
			Para_x = Para_x + 0.005f;
			TrailOfConvergingPlane(0.0f, 1.0f, 1.0f);
			if (Para_x > 3.0f)
			{
				bDiverge = true;
				bConverge = false;
				bStraight = false;
				Para_x = temp;
			}
		}
		if (bDiverge)
		{
			Para_y = 1.2f * (GLfloat)pow(Para_x, 2);
			Para_x = Para_x + 0.005f;
			TrailOfDivergingPlane(Para_x, 1.0f, 1.0f);
			TrailOfConvergingPlane(0.0f, 1.0f, 1.0f);
			if (Para_x > 1.5f)
			{
				bDiverge = false;
				bConverge = false;
				bStraight = false;
			}
		}

		//	Upper Plane
		glLoadIdentity();
		if (bConverge == true)
		{
			glTranslatef(0.0f, 0.0f, -3.5f);
			glTranslatef(-1.5f, 0.0f, 0.0f);
			glTranslatef(Para_x, Para_y, 0.0f);
			glRotatef(Planerotation, 0.0f, 0.0f, 1.0f);
			FighterPlane();

		}
		if (bStraight == true)
		{
			glTranslatef(-1.5f, 0.0f, 0.0f);
			glTranslatef(Para_x, Para_y, -3.5f);
			glRotatef(Planerotation1, 0.0f, 0.0f, 1.0f);
			FighterPlane();
		}
		if (bDiverge == true)
		{
			glTranslatef(1.5f, 0.0f, 0.0f);
			glTranslatef(Para_x, Para_y, -3.5f);
			glRotatef(Planerotation1, 0.0f, 0.0f, 1.0f);
			FighterPlane();
		}

		//	Middle plane
		glLoadIdentity();
		glTranslatef(0.0f, 0.0f, -3.5f);
		if (M_x <= 2.8f)
		{
			TrailOfMiddlePlane(M_x, 1.0f, 1.0f);
		}
		else if (M_x > 2.8f)
		{
			Draw_Tricolor();
			glLoadIdentity();
			glTranslatef(0.0f, 0.0f, -3.5f);
			TrailOfConvergingPlane(0.0f, Color_Brightness1, AlphaValue);
			TrailOfDivergingPlane(1.5f, Color_Brightness1, AlphaValue);
			TrailOfMiddlePlane(M_x, Color_Brightness1, AlphaValue);
			if (AlphaValue > 0)
				AlphaValue = AlphaValue - 0.005f;
		}
		glTranslatef(M_x, 0.0f, 0.0f);
		FighterPlane();

		//	Lower plane
		glLoadIdentity();
		if (bConverge == true)
		{
			glTranslatef(-1.5f, 0.0f, -3.5f);
			glTranslatef(Para_x, -Para_y, 0.0f);
			glRotatef(Planerotation1, 0.0f, 0.0f, 1.0f);
			FighterPlane();
		}
		if (bStraight == true)
		{
			glTranslatef(-1.5f, 0.0f, 0.0f);
			glTranslatef(Para_x, Para_y, -3.5f);
			FighterPlane();
		}
		if (bDiverge == true)
		{
			glTranslatef(1.5f, 0.0f, 0.0f);
			glTranslatef(Para_x, -Para_y, -3.5f);
			glRotatef(Planerotation, 0.0f, 0.0f, 1.0f);
			FighterPlane();
		}

		if (M_x < 3.0f)		//translation of middle plane
			M_x = M_x + 0.005f;

		if ((bConverge == true) && (Planerotation < 0.0f) && (Planerotation > -360.0f))
		{
			Planerotation = Planerotation + 0.2f;
			Planerotation1 = Planerotation1 - 0.2f;
			if (Planerotation > 0.0f)
				Planerotation = 0.00f;
			if (Planerotation1 <= -360.0f)
				Planerotation1 = 0.0f;
		}
		if ((bDiverge == true))
		{
			Planerotation = Planerotation - 0.4f;
			Planerotation1 = Planerotation1 + 0.4f;
		}
	}
	SwapBuffers(ghdc);
}


//reize() function
void resize(int win_width, int win_height)
{
	if (win_height == 0)
		win_height = 1;
	glViewport(0, 0, (GLsizei)win_width, (GLsizei)win_height);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluPerspective(45.0f, 1280 / 720.0f, 0.1f, 100.0f);
}

//uninitialize() function
void uninitialize(void)
{
	if (gbFullScreen == true)
	{
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPED);
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd,
			HWND_TOP,
			0,
			0,
			0,
			0,
			SWP_NOZORDER | SWP_FRAMECHANGED | SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER);
		ShowCursor(TRUE);
	}

	//break the current context
	if (wglGetCurrentContext() == ghrc)
	{
		wglMakeCurrent(NULL, NULL);
	}

	if (ghrc)
	{
		wglDeleteContext(ghrc);
		ghrc = NULL;
	}

	if (ghdc)
	{
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	if (gpfile)
	{
		fprintf(gpfile, "log file closed successfully\n");
		fclose(gpfile);
		gpfile = NULL;
	}
}

void Draw_I(void)							//for I
{
	glBegin(GL_LINES);						
	glColor3f(1.0f, 0.6f, 0.2f);			//saffron
	glVertex3f(-1.20f, 0.5f, 0.0f);
	glVertex3f(-0.88f, 0.5f, 0.0f);

	glColor3f(0.070f, 0.533f, 0.027f);		//green
	glVertex3f(-1.20f, -0.5f, 0.0f);
	glVertex3f(-0.88f, -0.5f, 0.0f);

	glColor3f(1.0f, 0.6f, 0.2f);
	glVertex3f(-1.04f, 0.5f, 0.0f);
	glColor3f(0.070f, 0.533f, 0.027f);
	glVertex3f(-1.04f, -0.5f, 0.0f);
	glEnd();
}

void Draw_N(void)							// N
{
	glBegin(GL_LINES);	
	glColor3f(1.0f, 0.6f, 0.2f);
	glVertex3f(-0.68f, 0.52f, 0.0f);
	glColor3f(0.070f, 0.533f, 0.027f);
	glVertex3f(-0.68f, -0.52f, 0.0f);

	glColor3f(1.0f, 0.6f, 0.2f);
	glVertex3f(-0.68f, 0.52f, 0.0f);
	glColor3f(0.070f, 0.533f, 0.027f);
	glVertex3f(-0.36f, -0.51f, 0.0f);

	glColor3f(1.0f, 0.6f, 0.2f);
	glVertex3f(-0.36f, 0.52f, 0.0f);
	glColor3f(0.070f, 0.533f, 0.027f);
	glVertex3f(-0.36f, -0.52f, 0.0f);
	glEnd();
}

void Draw_D(GLfloat b)						// D
{
	glBegin(GL_LINES);	
	glColor3f(1.0f * b, 0.6f* b, 0.2f*b);
	glVertex3f(-0.10f, 0.5f, 0.0f);
	glColor3f(0.070f * b, 0.533f * b, 0.027f * b);
	glVertex3f(-0.10f, -0.5f, 0.0f);

	glColor3f(1.0f * b, 0.6f * b, 0.2f * b);
	glVertex3f(-0.17f, 0.5f, 0.0f);
	glColor3f(1.0f * b, 0.6f * b, 0.2f * b);
	glVertex3f(0.17f, 0.5f, 0.0f);

	glColor3f(1.0f * b, 0.6f * b, 0.2f * b);
	glVertex3f(0.17f, 0.5f, 0.0f);
	glColor3f(0.070f * b, 0.533f * b, 0.027f * b);
	glVertex3f(0.17f, -0.5f, 0.0f);

	glColor3f(0.070f * b, 0.533f * b, 0.027f * b);
	glVertex3f(-0.17f, -0.5f, 0.0f);
	glColor3f(0.070f * b, 0.533f * b, 0.027f * b);
	glVertex3f(0.17f, -0.5f, 0.0f);
	glEnd();
}

void Draw_I2(void)						// I2
{
	glBegin(GL_LINES);	 
	glColor3f(1.0f, 0.6f, 0.2f);//saffron
	glVertex3f(0.36f, 0.5f, 0.0f);
	glVertex3f(0.68f, 0.5f, 0.0f);

	glColor3f(0.070f, 0.533f, 0.027f);//green
	glVertex3f(0.36f, -0.5f, 0.0f);
	glVertex3f(0.68f, -0.5f, 0.0f);

	glColor3f(1.0f, 0.6f, 0.2f);
	glVertex3f(0.52f, 0.5f, 0.0f);
	glColor3f(0.070f, 0.533f, 0.027f);
	glVertex3f(0.52f, -0.5f, 0.0f);
	glEnd();
}

void Draw_A(void)						 // A
{
	glBegin(GL_LINES);	
	glColor3f(0.070f, 0.533f, 0.027f);
	glVertex3f(0.83f, -0.52f, 0.0f);
	glColor3f(1.0f, 0.6f, 0.2f);
	glVertex3f(1.04f, 0.52f, 0.0f);
	glColor3f(1.0f, 0.6f, 0.2f);
	glVertex3f(1.04f, 0.52f, 0.0f);
	glColor3f(0.070f, 0.533f, 0.027f);
	glVertex3f(1.25f, -0.52f, 0.0f);
	glEnd();
}

void Draw_Tricolor(void)
{
	glLoadIdentity();
	glTranslatef(0.0f, 0.0f, -3.0f);
	glBegin(GL_QUADS);					//flag tricolor
	glColor3f(1.0f, 0.6f, 0.2f);		//saffron
	glVertex3f(0.953f, 0.015f, 0.0f);
	glVertex3f(1.128f, 0.015f, 0.0f);
	glVertex3f(1.122f, 0.045f, 0.0f);
	glVertex3f(0.958f, 0.045f, 0.0f);

	glColor3f(1.0f, 1.0f, 1.0f);		//white
	glVertex3f(0.947f, -0.015f, 0.0f);
	glVertex3f(1.135f, -0.015f, 0.0f);
	glVertex3f(1.128f, 0.015f, 0.0f);
	glVertex3f(0.953f, 0.015f, 0.0f);

	glColor3f(0.070f, 0.533f, 0.027f);	//green
	glVertex3f(1.135f, -0.015f, 0.0f);
	glVertex3f(0.947f, -0.015f, 0.0f);
	glVertex3f(0.94f, -0.045f, 0.0f);
	glVertex3f(1.14f, -0.045f, 0.0f);
	glEnd();
}

void PlaneTrail(GLfloat b)
{
	glBegin(GL_QUAD_STRIP);
	//saffron
	glColor3f(1.0f, 0.6f, 0.2f);
	glVertex3f(-0.14f, 0.045f, 0.0f);
	glVertex3f(-0.14f, 0.015f, 0.0f);
	glVertex3f(-0.11f, 0.045f, 0.0f);
	glVertex3f(-0.11f, 0.015f, 0.0f);
	glVertex3f(-0.08f, 0.045f, 0.0f);
	glVertex3f(-0.08f, 0.015f, 0.0f);
	glEnd();

	//white
	glBegin(GL_QUAD_STRIP);
	glColor3f(1.0f, 1.0f, 1.0f);
	glVertex3f(-0.14f, 0.015f, 0.0f);
	glVertex3f(-0.14f, -0.015f, 0.0f);
	glVertex3f(-0.11f, 0.015f, 0.0f);
	glVertex3f(-0.11f, -0.015f, 0.0f);
	glVertex3f(-0.08f, 0.015f, 0.0f);
	glVertex3f(-0.08f, -0.015f, 0.0f);
	glEnd();

	//green
	glBegin(GL_QUAD_STRIP);
	glColor3f(0.070f, 0.533f, 0.027f);
	glVertex3f(-0.14f, -0.015f, 0.0f);
	glVertex3f(-0.14f, -0.045f, 0.0f);
	glVertex3f(-0.11f, -0.015f, 0.0f);
	glVertex3f(-0.11f, -0.045f, 0.0f);
	glVertex3f(-0.08f, -0.015f, 0.0f);
	glVertex3f(-0.08f, -0.045f, 0.0f);
	glEnd();
}
void TrailOfMiddlePlane(GLfloat trail_end, GLfloat b, GLfloat alpha)
{
	
	glLoadIdentity();
	glTranslatef(0.0f, 0.0f, -3.5f);
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	for (GLfloat start = -3.001f; start < trail_end - 0.1f; start = start + 0.008)
	{
		glPointSize(10.0f);
		glBegin(GL_POINTS);
		glColor4f(1.0f * b, 1.0f * b, 1.0f * b, alpha);//white
		glVertex3f(start, 0.0f, 0.0f);
		glColor4f(1.0f * b, 0.6f * b, 0.2f * b, alpha);//saffron
		glVertex3f(start, 0.035f, 0.0f);
		glColor4f(0.070f * b, 0.533f * b, 0.027f*b, alpha);//green
		glVertex3f(start, -0.035f, 0.0f);
		glEnd();
	}
	glDisable(GL_BLEND);
}

void TrailOfConvergingPlane(GLfloat trail_end, GLfloat b, GLfloat alpha)
{
	//variable declaration
	GLfloat ParabolicY = 0.0f;
	
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	for (GLfloat StartParabola = -1.505f; StartParabola < trail_end; StartParabola = StartParabola + 0.005f)
	{
		glLoadIdentity();
		glTranslatef(0.0f, 0.0f, -3.5f);
		glPointSize(9.0f);
		glBegin(GL_POINTS);
		glColor4f(1.0f * b, 0.6f * b, 0.2f * b, alpha);//saffron
		ParabolicY = 1.2f * (GLfloat)pow(StartParabola, 2);
		glVertex3f(StartParabola - 1.5f, ParabolicY, 0.0f);
		glVertex3f(StartParabola - 0.06f - 1.5f, -ParabolicY, 0.0f);
		
		glColor4f(1.0f * b, 1.0f * b, 1.0f * b, alpha);//white
		glVertex3f(StartParabola - 0.03f - 1.5f, ParabolicY, 0.0f);
		glVertex3f(StartParabola - 0.03f - 1.5f, -ParabolicY, 0.0f);
		
		glColor4f(0.070f * b, 0.533f * b, 0.027f * b, alpha);//green
		glVertex3f(StartParabola - 0.06f - 1.5f, ParabolicY, 0.0f);
		glVertex3f(StartParabola - 1.5f, -ParabolicY, 0.0f);
		glEnd();
	}
	glDisable(GL_BLEND);
}

void TrailOfDivergingPlane(GLfloat trail_end, GLfloat b, GLfloat alpha)
{
	//variable declaration
	GLfloat ParabolicY = 0.0f;

	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	for (GLfloat StartParabola = 0.0f; StartParabola < trail_end; StartParabola = StartParabola + 0.004f)
	{
		glLoadIdentity();
		glTranslatef(1.52f, 0.0f, -3.5f);
		glPointSize(9.0f);
		glBegin(GL_POINTS);
		ParabolicY = 1.2f * (GLfloat)pow(StartParabola, 2);

		glColor4f(1.0f * b, 0.6f * b, 0.2f * b, alpha);//saffron
		glVertex3f(StartParabola - 0.06f, ParabolicY, 0.0f);
		glVertex3f(StartParabola, -ParabolicY, 0.0f);

		glColor4f(1.0f * b, 1.0f * b, 1.0f * b, alpha);//white
		glVertex3f(StartParabola - 0.03f, ParabolicY, 0.0f);
		glVertex3f(StartParabola - 0.03f, -ParabolicY, 0.0f);

		glColor4f(0.070f * b, 0.533f * b, 0.027f * b, alpha);//green
		glVertex3f(StartParabola, ParabolicY, 0.0f);
		glVertex3f(StartParabola - 0.06f, -ParabolicY, 0.0f);
		glEnd();
	}
	glDisable(GL_BLEND);
}
void FighterPlane(void)
{
	glLineWidth(1.0f);
	//at origin
	glBegin(GL_POLYGON);
	glColor3f(0.72f, 0.886f, 0.933f);
	glVertex3f(0.0f, -0.04f, 0.0f);
	glVertex3f(0.3f, -0.04f, 0.0f);
	glVertex3f(0.4f, 0.0f, 0.0f);//center point
	glVertex3f(0.3f, 0.04f, 0.0f);
	glVertex3f(0.0f, 0.04f, 0.0f);
	glVertex3f(-0.08f, 0.09f, 0.0f);
	glVertex3f(-0.08f, -0.09f, 0.0f);
	glEnd();

	glBegin(GL_QUADS);
	glColor3f(0.72f, 0.886f, 0.933f);
	glVertex3f(0.2f, 0.03f, 0.0f);
	glVertex3f(0.10f, 0.2f, 0.0f);
	glVertex3f(0.0f, 0.2f, 0.0f);
	glVertex3f(0.05f, 0.03f, 0.0f);

	glVertex3f(0.2f, -0.03f, 0.0f);
	glVertex3f(0.10f, -0.2f, 0.0f);
	glVertex3f(0.0f, -0.2f, 0.0f);
	glVertex3f(0.05f, -0.03f, 0.0f);
	glEnd();

	glLineWidth(2.0f);
	glBegin(GL_LINES);
	glColor3f(1.0f, 1.0f, 1.0f);
	//I
	glVertex3f(0.03f, 0.02f, 0.0f);
	glVertex3f(0.08f, 0.02f, 0.0f);
	glVertex3f(0.03f, -0.02f, 0.0f);
	glVertex3f(0.08f, -0.02f, 0.0f);
	glVertex3f(0.055f, 0.02f, 0.0f);
	glVertex3f(0.055f, -0.02f, 0.0f);
	//A
	glVertex3f(0.10f, 0.02f, 0.0f);
	glVertex3f(0.10f, -0.025f, 0.0f);
	glVertex3f(0.14f, 0.02f, 0.0f);
	glVertex3f(0.14f, -0.025f, 0.0f);
	glVertex3f(0.10f, 0.02f, 0.0f);
	glVertex3f(0.14f, 0.02f, 0.0f);
	glVertex3f(0.10f, 0.0f, 0.0f);
	glVertex3f(0.14f, 0.0f, 0.0f);
	//F
	glVertex3f(0.16f, 0.02f, 0.0f);
	glVertex3f(0.16f, -0.025f, 0.0f);
	glVertex3f(0.16f, 0.02f, 0.0f);
	glVertex3f(0.20f, 0.02f, 0.0f);
	glVertex3f(0.16f, 0.0f, 0.0f);
	glVertex3f(0.19f, 0.0f, 0.0f);
	glEnd();
}

