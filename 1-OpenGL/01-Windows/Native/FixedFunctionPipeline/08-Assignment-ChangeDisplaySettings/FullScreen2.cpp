//Headers
#include<windows.h>

//global function declarations
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

//Global variable declaration
bool bFullScreen = false;
DWORD dwStyle;
HWND ghwnd = NULL;

//WinMain()
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{
	//variable declarations
	WNDCLASSEX wndclass;
	HWND hwnd;
	MSG msg;
	TCHAR szAppName[] = TEXT("MyWindow");

	//code
	//initialization of WNDCLASSEX
	wndclass.cbSize = sizeof(WNDCLASSEX);
	wndclass.style = CS_HREDRAW | CS_VREDRAW;
	wndclass.cbClsExtra = 0;
	wndclass.cbWndExtra = 0;
	wndclass.lpfnWndProc = WndProc;
	wndclass.hInstance = hInstance;
	wndclass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	wndclass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndclass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndclass.lpszClassName = szAppName;
	wndclass.lpszMenuName = NULL;
	wndclass.hIconSm = LoadIcon(NULL, IDI_APPLICATION);

	//register above class
	RegisterClassEx(&wndclass);

	//create window
	hwnd = CreateWindow(szAppName,
		TEXT("My Window(FULLSCREEN)- Priya Saboo"),
		WS_OVERLAPPEDWINDOW,
		100,
		100,
		800,
		600,
		NULL,
		NULL,
		hInstance,
		NULL);

	ghwnd = hwnd;
	ShowWindow(hwnd, iCmdShow);
	UpdateWindow(hwnd);

	//message loop
	while (GetMessage(&msg, NULL, 0, 0))
	{
		TranslateMessage(&msg);
		DispatchMessage(&msg);
	}
	return((int)msg.wParam);
}

LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	//function declaration
	void ToggleFullScreen(void);

	//code
	switch (iMsg)
	{
	case WM_CREATE:
		//MessageBox(hwnd, TEXT("This is WM_CREATE"), TEXT("Message"), MB_OK);
		break;

	case WM_KEYDOWN:
		switch (wParam)
		{
		case VK_ESCAPE:
			MessageBox(hwnd, TEXT("ESCAPE button is pressed"), TEXT("Message"), MB_OK);
			DestroyWindow(hwnd);
			break;

		case 0x46:
			ToggleFullScreen();
			break;
		}
		break;

	case WM_LBUTTONDOWN:
		MessageBox(hwnd, TEXT("Left mouse button is clicked"), TEXT("Message"), MB_OK);
		break;

	case WM_RBUTTONDOWN:
		MessageBox(hwnd, TEXT("Right button is clicked"), TEXT("Message"), MB_OK);
		break;

	case WM_DESTROY:
		//MessageBox(hwnd, TEXT("This is WM_DESTROY"), TEXT("Message"), MB_OK);
		PostQuitMessage(0);
		break;
	}
	return(DefWindowProc(hwnd, iMsg, wParam, lParam));
}

//toggle fullscreen function
void ToggleFullScreen(void)
{
	DEVMODE dm;
	dwStyle = GetWindowLong(ghwnd, GWL_STYLE);

	if (bFullScreen == false)
	{
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);
		dm.dmSize = sizeof(DEVMODE);
		dm.dmDriverExtra = 0;
		ZeroMemory(&dm,sizeof(DEVMODE));
		EnumDisplaySettings(NULL, ENUM_CURRENT_SETTINGS, &dm);
		long lret = ChangeDisplaySettings(&dm, CDS_RESET | CDS_FULLSCREEN);
		if (lret != DISP_CHANGE_SUCCESSFUL)
		{
			TCHAR str[10];
			wsprintf(str, TEXT("%ld"), lret);
			MessageBox(ghwnd, str, TEXT("message"), MB_OK);
		}
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle & ~WS_OVERLAPPEDWINDOW);
		ShowWindow(ghwnd, SW_SHOWMAXIMIZED);
		ShowCursor(FALSE);
		bFullScreen = true;
	}
	else
	{
		dm.dmSize = sizeof(DEVMODE);
		dm.dmDriverExtra = 0;
		ChangeDisplaySettings(&dm, CDS_RESET);
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		ShowWindow(ghwnd, SW_SHOWNORMAL);
		ShowCursor(TRUE);
		bFullScreen = false;
	}
}