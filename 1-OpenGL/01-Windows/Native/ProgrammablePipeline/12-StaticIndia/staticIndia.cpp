//	Headers
#include<windows.h>
#include<stdio.h>
#include<gl/glew.h>
#include<gl/gl.h>
#include "vmath.h"	

#pragma comment (lib, "glew32.lib")
#pragma comment (lib, "opengl32.lib")

//	Macros
#define WIN_WIDTH 800
#define WIN_HEIGHT 600

//namespaces
using namespace vmath;

enum
{
	AMC_ATTRIBUTE_POSITION = 0,
	AMC_ATTRIBUTE_COLOR,
	AMC_ATTRIBUTE_NORMAL,
	AMC_ATTRIBUTE_TEXCOORD0
};

//	Global function declaration
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

//	Global variables
FILE *gpfile;
HWND ghwnd = NULL;
bool gbActiveWindow = false;
bool gbFullScreen = false;
DWORD dwStyle;
WINDOWPLACEMENT wpPrev = { sizeof(WINDOWPLACEMENT) };
HDC ghdc = NULL;
HGLRC ghrc = NULL;

GLuint gShaderProgramObject;
GLuint mvpUniform;
GLuint vao_i1;			// I
GLuint vbo_color_i1;
GLuint vbo_position_i1;
GLuint vao_n;			// N
GLuint vbo_color_n;
GLuint vbo_position_n;
GLuint vao_d;			// D
GLuint vbo_color_d;
GLuint vbo_position_d;
GLuint vao_i2;			// I
GLuint vbo_color_i2;
GLuint vbo_position_i2;
GLuint vao_a;			// A
GLuint vbo_color_a;
GLuint vbo_position_a;
GLuint vao_flag;
GLuint vbo_position_flag;
GLuint vbo_color_flag;

mat4 perspectiveProjectionMatrix;

//WinMain()
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{
	//function declarations
	int initialize(void);
	void display(void);

	//variable declarations
	WNDCLASSEX wndclass;
	HWND hwnd;
	MSG msg;
	TCHAR szAppName[] = TEXT("staticindia");
	bool bDone = false;
	int iRet = 0;

	//code
	if (fopen_s(&gpfile, "log.txt", "w") != 0)
	{
		MessageBox(NULL, TEXT("Log file cannot be created"), TEXT("Error"), MB_OK);
		exit(0);
	}

	//initialization of wndclass structure
	wndclass.cbSize = sizeof(WNDCLASSEX);
	wndclass.cbWndExtra = 0;
	wndclass.cbClsExtra = 0;
	wndclass.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
	wndclass.lpfnWndProc = WndProc;
	wndclass.hInstance = hInstance;
	wndclass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	wndclass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndclass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndclass.lpszClassName = szAppName;
	wndclass.lpszMenuName = NULL;
	wndclass.hIconSm = LoadIcon(hInstance, IDI_APPLICATION);

	//register above class 
	RegisterClassEx(&wndclass);

	//create window
	hwnd = CreateWindowEx(WS_EX_APPWINDOW,
		szAppName,
		TEXT(" Static India - PriyaSaboo"),
		WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN | WS_CLIPSIBLINGS | WS_VISIBLE,
		100,
		100,
		WIN_WIDTH,
		WIN_HEIGHT,
		NULL,
		NULL,
		hInstance,
		NULL);

	ghwnd = hwnd;
	iRet = initialize();
	if (iRet == -1)
	{
		fprintf(gpfile, "ChoosePixelFormat() Failed\n");
		DestroyWindow(hwnd);
	}
	else if (iRet == -2)
	{
		fprintf(gpfile, "SetPixelFormat() Failed\n");
		DestroyWindow(hwnd);
	}
	else if (iRet == -3)
	{
		fprintf(gpfile, "wglCreateContext() Failed\n");
		DestroyWindow(hwnd);
	}
	else if (iRet == -4)
	{
		fprintf(gpfile, "wglMakeCurrent() Failed\n");
		DestroyWindow(hwnd);
	}
	else
		fprintf(gpfile, "Initialize() Succeded\n");

	//show window
	ShowWindow(hwnd, iCmdShow);
	SetForegroundWindow(hwnd);
	SetFocus(hwnd);

	//gameloop
	while (bDone == false)
	{
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			if (msg.message == WM_QUIT)
			{
				bDone = true;
			}
			else
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}
		else
		{
			if (gbActiveWindow == true)
			{
				//update should be called here
			}
			display();
		}
	}
	return((int)msg.wParam);
}

//callback function WndProc()
LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	//function declaration
	void resize(int, int);
	void ToggleFullScreen(void);
	void uninitialize(void);

	//variables
	static GLfloat width, height;

	//code
	switch (iMsg)
	{
	case WM_SETFOCUS:
		gbActiveWindow = true;
		break;

	case WM_KILLFOCUS:
		gbActiveWindow = false;
		break;

	case WM_ERASEBKGND:
		return(0);

	case WM_SIZE:
		width = LOWORD(lParam);
		height = HIWORD(lParam);
		resize(LOWORD(lParam), HIWORD(lParam));
		break;

	case WM_CLOSE:
		DestroyWindow(hwnd);
		break;

	case WM_KEYDOWN:
		switch (LOWORD(wParam))
		{
		case VK_ESCAPE:
			DestroyWindow(hwnd);
			break;

		case 0x46:
			ToggleFullScreen();
			break;
		}
		break;

	case WM_DESTROY:
		uninitialize();	
		PostQuitMessage(0);
		break;
	}
	return(DefWindowProc(hwnd, iMsg, wParam, lParam));
}

//ToggleFullScreen() function
void ToggleFullScreen(void)
{
	MONITORINFO mi;
	if (gbFullScreen == false)
	{
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);
		if (dwStyle & WS_OVERLAPPEDWINDOW)
		{
			mi = { sizeof(MONITORINFO) };
			if (GetWindowPlacement(ghwnd, &wpPrev) && GetMonitorInfo(MonitorFromWindow(ghwnd, MONITORINFOF_PRIMARY), &mi))
			{
				SetWindowLong(ghwnd, GWL_STYLE, dwStyle & ~WS_OVERLAPPEDWINDOW);
				SetWindowPos(ghwnd,
					HWND_TOP,
					mi.rcMonitor.left,
					mi.rcMonitor.top,
					mi.rcMonitor.right - mi.rcMonitor.left,
					mi.rcMonitor.bottom - mi.rcMonitor.top,
					SWP_NOZORDER | SWP_FRAMECHANGED);
			}
		}
		ShowCursor(FALSE);
		gbFullScreen = true;
	}
	else
	{
		fprintf(gpfile, "Full Screen to normal\n");
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPED);
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd,
			HWND_TOP,
			0,
			0,
			0,
			0,
			SWP_NOZORDER | SWP_FRAMECHANGED | SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER);
		ShowCursor(TRUE);
		gbFullScreen = false;
	}
}

//initialize() function
int initialize(void)
{
	//function declarations
	void resize(int, int);
	void uninitialize(void);

	//vaiable declaraions
	PIXELFORMATDESCRIPTOR pfd;
	int iPixelFormatIndex;
	GLenum result;
	GLint iShaderCompileStatus = 0;
	GLint iInfoLogLength = 0;
	GLchar *szInfoLog = NULL;
	GLint iProgramLinkStatus = 0;
	GLuint gVertexShaderObject;
	GLuint gFragmentShaderObject;

	//code
	//initialize pfd structure
	ZeroMemory(&pfd, sizeof(PIXELFORMATDESCRIPTOR));
	pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
	pfd.nVersion = 1;
	pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
	pfd.iPixelType = PFD_TYPE_RGBA;
	pfd.cColorBits = 32;
	pfd.cDepthBits = 32;
	pfd.cRedBits = 8;
	pfd.cGreenBits = 8;
	pfd.cBlueBits = 8;
	pfd.cAlphaBits = 8;

	ghdc = GetDC(ghwnd);
	iPixelFormatIndex = ChoosePixelFormat(ghdc, &pfd);
	if (iPixelFormatIndex == 0)
	{
		return(-1);
	}
	if (SetPixelFormat(ghdc, iPixelFormatIndex, &pfd) == false)
	{
		return(-2);
	}
	ghrc = wglCreateContext(ghdc);
	if (ghrc == NULL)
	{
		return(-3);
	}
	if (wglMakeCurrent(ghdc, ghrc) == false)
	{
		return(-4);
	}
	result = glewInit();
	if (result != GLEW_OK)
	{
		fprintf(gpfile, "glewInit() failed\n");
		uninitialize();
		DestroyWindow(ghwnd);
	}

	//define vertex shader object
	gVertexShaderObject = glCreateShader(GL_VERTEX_SHADER);
	
	//write vertex shader code
	const GLchar *VertexShaderSourceCode =
		"#version 430 core" \
		"\n" \
		"in vec4 vPosition;" \
		"in vec4 vColor;" \
		"out vec4 out_color;" \
		"uniform mat4 u_mvp_matrix;"
		"void main(void)" \
		"{" \
		"gl_Position = u_mvp_matrix * vPosition;" \
		"out_color = vColor;" \
		"}";

	//specify above source code to the vertex shader object
	glShaderSource(gVertexShaderObject, 1, (const GLchar **)&VertexShaderSourceCode, NULL);
	
	//compile the vertex shader
	glCompileShader(gVertexShaderObject);
	glGetShaderiv(gVertexShaderObject, GL_COMPILE_STATUS, &iShaderCompileStatus);
	if (iShaderCompileStatus == GL_FALSE)
	{
		glGetShaderiv(gVertexShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (GLchar *)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(gVertexShaderObject, iInfoLogLength, &written, szInfoLog);
				fprintf(gpfile, "VERTEX SHADER COMPILATION LOG : %s\n", szInfoLog);
				free(szInfoLog);
				uninitialize();
				DestroyWindow(ghwnd);
				exit(0);
			}
		}
	}

	//define fragment shader object
	gFragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);
	//write fragment shader code
	const GLchar *FragmentShaderSourceCode =
		"#version 430 core" \
		"\n" \
		"in vec4 out_color;" \
		"out vec4 FragColor;" \
		"void main(void)" \
		"{" \
		"FragColor = out_color;" \
		"}";

	//specify above source code to fragmnet shader object
	glShaderSource(gFragmentShaderObject, 1, (const GLchar **)&FragmentShaderSourceCode, NULL);
	
	//compile fragment shader
	iShaderCompileStatus = 0;
	iInfoLogLength = 0;
	szInfoLog = NULL;
	glCompileShader(gFragmentShaderObject);
	glGetShaderiv(gFragmentShaderObject, GL_COMPILE_STATUS, &iShaderCompileStatus);
	if (iShaderCompileStatus == GL_FALSE)
	{
		glGetShaderiv(gFragmentShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (GLchar *)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(gFragmentShaderObject, iInfoLogLength, &written, szInfoLog);
				fprintf(gpfile, "FRAGMENT SHADER COMPILATION LOG : %s\n", szInfoLog);
				free(szInfoLog);
				uninitialize();
				DestroyWindow(ghwnd);
				exit(0);
			}
		}
	}

	//create shader program object
	gShaderProgramObject = glCreateProgram();
	
	//attach vertex shader to the shader program 
	glAttachShader(gShaderProgramObject, gVertexShaderObject);
	//attach fragment shader 
	glAttachShader(gShaderProgramObject, gFragmentShaderObject);
	
	//prelinking binding to vertex attribute
	glBindAttribLocation(gShaderProgramObject, AMC_ATTRIBUTE_POSITION, "vPosition");
	glBindAttribLocation(gShaderProgramObject, AMC_ATTRIBUTE_COLOR, "vColor");

	//link shader program
	glLinkProgram(gShaderProgramObject);
	//error checking
	iProgramLinkStatus = 0;
	iInfoLogLength = 0;
	szInfoLog = NULL;
	glGetProgramiv(gShaderProgramObject, GL_LINK_STATUS, &iProgramLinkStatus);
	if (iProgramLinkStatus == GL_FALSE)
	{
		glGetProgramiv(gShaderProgramObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (GLchar *)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLint written;
				glGetProgramInfoLog(gShaderProgramObject, iInfoLogLength, &written, szInfoLog);
				fprintf(gpfile, "SHADER PROGRAM LINK LOG : %s\n", szInfoLog);
				free(szInfoLog);
				uninitialize();
				DestroyWindow(ghwnd);
				exit(0);
			}
		}
	}

	//postlinking retieving uniform location
	mvpUniform = glGetUniformLocation(gShaderProgramObject, "u_mvp_matrix");

	const GLfloat i1Vertices[] = {
		-1.20f, 0.5f, 0.0f,
		- 0.88f, 0.5f, 0.0f,
		-1.04f, 0.5f, 0.0f,
		-1.04f, -0.5f, 0.0f,
		-1.20f, -0.5f, 0.0f,
		- 0.88f, -0.5f, 0.0f,

	};
	
	const GLfloat i1Color[] = {
		1.0f, 0.6f, 0.2f,
		1.0f, 0.6f, 0.2f,
		1.0f, 0.6f, 0.2f,
		0.070f, 0.533f, 0.027f,
		0.070f, 0.533f, 0.027f,
		0.070f, 0.533f, 0.027f,
	};

	//-------------------------------- I -------------------------------------------
	glGenVertexArrays(1, &vao_i1);
	glBindVertexArray(vao_i1);
	// For position buffer
	glGenBuffers(1, &vbo_position_i1);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_position_i1);
	glBufferData(GL_ARRAY_BUFFER, sizeof(i1Vertices), i1Vertices, GL_STATIC_DRAW);
	glVertexAttribPointer(AMC_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	// For color buffer
	glGenBuffers(1, &vbo_color_i1);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_color_i1);
	glBufferData(GL_ARRAY_BUFFER, sizeof(i1Color), i1Color, GL_STATIC_DRAW);
	glVertexAttribPointer(AMC_ATTRIBUTE_COLOR, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AMC_ATTRIBUTE_COLOR);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	// Unbind with vao of I
	glBindVertexArray(0);
	//-----------------------------------------------------------------------------

	const GLfloat nVertices[] = {
		-0.68f, 0.52f, 0.0f,
		-0.68f, -0.52f, 0.0f,
		-0.68f, 0.52f, 0.0f,
		-0.36f, -0.51f, 0.0f,
		-0.36f, 0.52f, 0.0f,
		-0.36f, -0.52f, 0.0f
	};

	const GLfloat nColor[] = {
		1.0f, 0.6f, 0.2f,
		0.070f, 0.533f, 0.027f,
		1.0f, 0.6f, 0.2f,
		0.070f, 0.533f, 0.027f,
		1.0f, 0.6f, 0.2f,
		0.070f, 0.533f, 0.027f
	};

	//------------------------------------- N --------------------------------------
	glGenVertexArrays(1, &vao_n);
	glBindVertexArray(vao_n);
	// For position buffer
	glGenBuffers(1, &vbo_position_n);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_position_n);
	glBufferData(GL_ARRAY_BUFFER, sizeof(nVertices), nVertices, GL_STATIC_DRAW);
	glVertexAttribPointer(AMC_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	// For color buffer
	glGenBuffers(1, &vbo_color_n);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_color_n);
	glBufferData(GL_ARRAY_BUFFER, sizeof(nColor), nColor, GL_STATIC_DRAW);
	glVertexAttribPointer(AMC_ATTRIBUTE_COLOR, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AMC_ATTRIBUTE_COLOR);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	// Unbind with vao of N
	glBindVertexArray(0);
	//------------------------------------------------------------------------------
	
	const GLfloat dVertices[] = {
		-0.10f, 0.5f, 0.0f,
		-0.10f, -0.5f, 0.0f,
		-0.17f, 0.5f, 0.0f,
		0.17f, 0.5f, 0.0f,
		0.17f, 0.5f, 0.0f,
		0.17f, -0.5f, 0.0f,
		-0.17f, -0.5f, 0.0f,
		0.17f, -0.5f, 0.0f
	};

	const GLfloat dColor[] = {
		1.0f, 0.6f, 0.2f,
		0.070f, 0.533f, 0.027f,
		1.0f, 0.6f, 0.2f,
		1.0f, 0.6f, 0.2f,
		1.0f, 0.6f, 0.2f,
		0.070f, 0.533f, 0.027f,
		0.070f, 0.533f, 0.027f,
		0.070f, 0.533f, 0.027f
	};
	
	// -------------------------------------- D -------------------------------------
	glGenVertexArrays(1, &vao_d);
	glBindVertexArray(vao_d);
	// For position buffer
	glGenBuffers(1, &vbo_position_d);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_position_d);
	glBufferData(GL_ARRAY_BUFFER, sizeof(dVertices), dVertices, GL_STATIC_DRAW);
	glVertexAttribPointer(AMC_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	// For color buffer
	glGenBuffers(1, &vbo_color_d);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_color_d);
	glBufferData(GL_ARRAY_BUFFER, sizeof(dColor), dColor, GL_STATIC_DRAW);
	glVertexAttribPointer(AMC_ATTRIBUTE_COLOR, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AMC_ATTRIBUTE_COLOR);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	// Unbind with vao of D
	glBindVertexArray(0);
	//-------------------------------------------------------------------------------
	
	const GLfloat i2Vertices[] = {
		0.36f, 0.5f, 0.0f,
		0.68f, 0.5f, 0.0f,
		0.52f, 0.5f, 0.0f,
		0.52f, -0.5f, 0.0f,
		0.36f, -0.5f, 0.0f,
		0.68f, -0.5f, 0.0f
	};
	
	const GLfloat i2Color[] = {
		1.0f, 0.6f, 0.2f,
		1.0f, 0.6f, 0.2f,
		1.0f, 0.6f, 0.2f,
		0.070f, 0.533f, 0.027f,
		0.070f, 0.533f, 0.027f,
		0.070f, 0.533f, 0.027f
	};

	//-------------------------------- I2 -------------------------------------------
	glGenVertexArrays(1, &vao_i2);
	glBindVertexArray(vao_i2);
	// For position buffer
	glGenBuffers(1, &vbo_position_i2);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_position_i2);
	glBufferData(GL_ARRAY_BUFFER, sizeof(i2Vertices), i2Vertices, GL_STATIC_DRAW);
	glVertexAttribPointer(AMC_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	// For color buffer
	glGenBuffers(1, &vbo_color_i2);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_color_i2);
	glBufferData(GL_ARRAY_BUFFER, sizeof(i2Color), i2Color, GL_STATIC_DRAW);
	glVertexAttribPointer(AMC_ATTRIBUTE_COLOR, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AMC_ATTRIBUTE_COLOR);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	// Unbind with vao of I2
	glBindVertexArray(0);
	//-----------------------------------------------------------------------------

	const GLfloat aVertices[] = {
		0.83f, -0.52f, 0.0f,
		1.04f, 0.52f, 0.0f,
		1.04f, 0.52f, 0.0f,
		1.25f, -0.52f, 0.0f
	};

	const GLfloat aColor[] = {
		0.070f, 0.533f, 0.027f,
		1.0f, 0.6f, 0.2f,
		1.0f, 0.6f, 0.2f,
		0.070f, 0.533f, 0.027f
	};

	//--------------------------------------- A -----------------------------------------
	glGenVertexArrays(1, &vao_a);
	glBindVertexArray(vao_a);
	// For position buffer
	glGenBuffers(1, &vbo_position_a);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_position_a);
	glBufferData(GL_ARRAY_BUFFER, sizeof(aVertices), aVertices, GL_STATIC_DRAW);
	glVertexAttribPointer(AMC_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	// For color buffer
	glGenBuffers(1, &vbo_color_a);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_color_a);
	glBufferData(GL_ARRAY_BUFFER, sizeof(aColor), aColor, GL_STATIC_DRAW);
	glVertexAttribPointer(AMC_ATTRIBUTE_COLOR, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AMC_ATTRIBUTE_COLOR);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	// Unbind with vao of A
	glBindVertexArray(0);
	//----------------------------------------------------------------------------------
	
	const GLfloat flagVertices[] = {
		0.953f, 0.015f, 0.0f,
		1.128f, 0.015f, 0.0f,
		1.122f, 0.045f, 0.0f,
		0.958f, 0.045f, 0.0f,
		0.947f, -0.015f, 0.0f,
		1.135f, -0.015f, 0.0f,
		1.128f, 0.015f, 0.0f,
		0.953f, 0.015f, 0.0f,
		1.135f, -0.015f, 0.0f,
		0.947f, -0.015f, 0.0f,
		0.94f, -0.045f, 0.0f,
		1.14f, -0.045f, 0.0f

	}; // 4 saffron quad - 4 white quad - 4 green quad

	const GLfloat flagColor[] = {
		1.0f, 0.6f, 0.2f,
		1.0f, 0.6f, 0.2f,
		1.0f, 0.6f, 0.2f,
		1.0f, 0.6f, 0.2f,
		1.0f, 1.0f, 1.0f,
		1.0f, 1.0f, 1.0f,
		1.0f, 1.0f, 1.0f,
		1.0f, 1.0f, 1.0f,
		0.070f, 0.533f, 0.027f,
		0.070f, 0.533f, 0.027f,
		0.070f, 0.533f, 0.027f,
		0.070f, 0.533f, 0.027f
	};

	//--------------------------------------- FLAG -----------------------------------------
	glGenVertexArrays(1, &vao_flag);
	glBindVertexArray(vao_flag);
	// For position buffer
	glGenBuffers(1, &vbo_position_flag);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_position_flag);
	glBufferData(GL_ARRAY_BUFFER, sizeof(flagVertices), flagVertices, GL_STATIC_DRAW);
	glVertexAttribPointer(AMC_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	// For color buffer
	glGenBuffers(1, &vbo_color_flag);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_color_flag);
	glBufferData(GL_ARRAY_BUFFER, sizeof(flagColor), flagColor, GL_STATIC_DRAW);
	glVertexAttribPointer(AMC_ATTRIBUTE_COLOR, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AMC_ATTRIBUTE_COLOR);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	// Unbind with vao of flag
	glBindVertexArray(0);
	//----------------------------------------------------------------------------------

	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);	//r,g,b,a	//clear the screen by opengl color
	
	glClearDepth(1.0f);						//giving existance to depth buffer
	glEnable(GL_DEPTH_TEST);				// enableing depth test
	glDepthFunc(GL_LEQUAL);

	perspectiveProjectionMatrix = mat4::identity();

	//wramup call to resize
	resize(WIN_WIDTH, WIN_HEIGHT);

	return(0);
}


//display function
void display(void)
{
	//declaration of matrices
	mat4 modelViewMatrix;
	mat4 modelViewProjectionMatrix;

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glUseProgram(gShaderProgramObject);
	//initialize above matrices to identity
	modelViewMatrix = mat4::identity();
	modelViewProjectionMatrix = mat4::identity();
	
	//do necessary transformation
	modelViewMatrix = translate(0.0f, 0.0f, -3.0f);
	modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;

	//send necessary matrices to shader in respective uniforms
	glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);
	
	glLineWidth(15.0f);
	// ******************* I ************************
	glBindVertexArray(vao_i1);
	glDrawArrays(GL_LINES, 0, 2);
	glDrawArrays(GL_LINES, 2, 2);
	glDrawArrays(GL_LINES, 4, 2);
	//unbind vao
	glBindVertexArray(0);
	// **********************************************

	// ******************* N ************************
	glBindVertexArray(vao_n);
	glDrawArrays(GL_LINES, 0, 2);
	glDrawArrays(GL_LINES, 2, 2);
	glDrawArrays(GL_LINES, 4, 2);
	//unbind vao
	glBindVertexArray(0);
	// **********************************************

	// ******************* D ************************
	glBindVertexArray(vao_d);
	glDrawArrays(GL_LINES, 0, 2);
	glDrawArrays(GL_LINES, 2, 2);
	glDrawArrays(GL_LINES, 4, 2);
	glDrawArrays(GL_LINES, 6, 2);
	//unbind vao
	glBindVertexArray(0);
	// **********************************************

	// ******************* I ************************
	glBindVertexArray(vao_i2);
	glDrawArrays(GL_LINES, 0, 2);
	glDrawArrays(GL_LINES, 2, 2);
	glDrawArrays(GL_LINES, 4, 2);
	//unbind vao
	glBindVertexArray(0);
	// **********************************************

	// ******************* A ************************
	glBindVertexArray(vao_a);
	glDrawArrays(GL_LINES, 0, 2);
	glDrawArrays(GL_LINES, 2, 2);
	//unbind vao
	glBindVertexArray(0);
	// **********************************************

	// ***************** FLAG ***********************
	glBindVertexArray(vao_flag);
	glDrawArrays(GL_TRIANGLE_FAN, 0, 4);
	glDrawArrays(GL_TRIANGLE_FAN, 4, 4);
	glDrawArrays(GL_TRIANGLE_FAN, 8, 4);
	//unbind vao
	glBindVertexArray(0);
	// **********************************************

	//unuse program
	glUseProgram(0);
	SwapBuffers(ghdc);
}

//reize() function
void resize(int win_width, int win_height)
{
	if (win_height == 0)
		win_height = 1;
	glViewport(0, 0, (GLsizei)win_width, (GLsizei)win_height);
	perspectiveProjectionMatrix = perspective(45.0f, (GLfloat)win_width / (GLfloat)win_height, 0.1f, 100.0f);
}

//uninitialize() function
void uninitialize(void)
{
	if (vbo_color_i1)
	{
		glDeleteBuffers(1, &vbo_color_i1);
		vbo_color_i1 = 0;
	}
	if (vbo_position_i1)
	{
		glDeleteBuffers(1, &vbo_position_i1);
		vbo_position_i1 = 0;
	}
	if (vao_i1)
	{
		glDeleteVertexArrays(1, &vao_i1);
		vao_i1 = 0;
	}
	if (vbo_color_n)
	{
		glDeleteBuffers(1, &vbo_color_n);
		vbo_color_n = 0;
	}
	if (vbo_position_n)
	{
		glDeleteBuffers(1, &vbo_position_n);
		vbo_position_n = 0;
	}
	if (vao_n)
	{
		glDeleteVertexArrays(1, &vao_n);
		vao_n = 0;
	}

	if (vbo_color_d)
	{
		glDeleteBuffers(1, &vbo_color_d);
		vbo_color_d = 0;
	}
	if (vbo_position_d)
	{
		glDeleteBuffers(1, &vbo_position_d);
		vbo_position_d = 0;
	}
	if (vao_d)
	{
		glDeleteVertexArrays(1, &vao_d);
		vao_d = 0;
	}

	if (vbo_color_i2)
	{
		glDeleteBuffers(1, &vbo_color_i2);
		vbo_color_i2 = 0;
	}
	if (vbo_position_i2)
	{
		glDeleteBuffers(1, &vbo_position_i2);
		vbo_position_i2 = 0;
	}
	if (vao_i2)
	{
		glDeleteVertexArrays(1, &vao_i2);
		vao_i2 = 0;
	}

	if (vbo_color_a)
	{
		glDeleteBuffers(1, &vbo_color_a);
		vbo_color_a = 0;
	}
	if (vbo_position_a)
	{
		glDeleteBuffers(1, &vbo_position_a);
		vbo_position_a = 0;
	}
	if (vao_a)
	{
		glDeleteVertexArrays(1, &vao_a);
		vao_a = 0;
	}

	if (vbo_color_flag)
	{
		glDeleteBuffers(1, &vbo_color_flag);
		vbo_color_flag = 0;
	}
	if (vbo_position_flag)
	{
		glDeleteBuffers(1, &vbo_position_flag);
		vbo_position_flag = 0;
	}
	if (vao_flag)
	{
		glDeleteVertexArrays(1, &vao_flag);
		vao_flag = 0;
	}
	
	if (gShaderProgramObject)
	{
		GLsizei shaderCount;
		GLsizei shaderNumber;

		glUseProgram(gShaderProgramObject);
		glGetProgramiv(gShaderProgramObject, GL_ATTACHED_SHADERS, &shaderCount);
		GLuint *pShaders = (GLuint *)malloc(sizeof(GLuint) * shaderCount);

		if (pShaders)
		{
			glGetAttachedShaders(gShaderProgramObject, shaderCount, &shaderCount, pShaders);
			for (shaderNumber = 0; shaderNumber < shaderCount; shaderNumber++)
			{
				//detach all the shader one by one
				glDetachShader(gShaderProgramObject, pShaders[shaderNumber]);
				//delete detached shader
				glDeleteShader(pShaders[shaderNumber]);
				pShaders[shaderNumber] = 0;
			}
			free(pShaders);
		}
		glDeleteProgram(gShaderProgramObject);
		gShaderProgramObject = 0;
		glUseProgram(0);
	}

	if (gbFullScreen == true)
	{
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPED);
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd,
			HWND_TOP,
			0,
			0,
			0,
			0,
			SWP_NOZORDER | SWP_FRAMECHANGED | SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER);
		ShowCursor(TRUE);
	}

	//break the current context
	if (wglGetCurrentContext() == ghrc)
	{
		wglMakeCurrent(NULL, NULL);
	}

	if (ghrc)
	{
		wglDeleteContext(ghrc);
		ghrc = NULL;
	}

	if (ghdc)
	{
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	if (gpfile)
	{
		fprintf(gpfile, "log file closed successfully\n");
		fclose(gpfile);
		gpfile = NULL;
	}
}
