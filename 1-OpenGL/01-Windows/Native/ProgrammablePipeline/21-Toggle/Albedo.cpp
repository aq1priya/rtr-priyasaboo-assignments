//	Headers
#include<windows.h>
#include<stdio.h>
#include<gl/glew.h>
#include<gl/gl.h>

#include "vmath.h"	
#include "Sphere.h"

#pragma comment (lib, "glew32.lib")
#pragma comment (lib, "opengl32.lib")
#pragma comment (lib, "user32.lib")
#pragma comment (lib, "gdi32.lib")
#pragma comment (lib, "Sphere.lib")

//	Macros
#define WIN_WIDTH 800
#define WIN_HEIGHT 600

//namespaces
using namespace vmath;

enum
{
	AMC_ATTRIBUTE_POSITION = 0,
	AMC_ATTRIBUTE_COLOR,
	AMC_ATTRIBUTE_NORMAL,
	AMC_ATTRIBUTE_TEXCOORD0
};

//	Global function declaration
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

// Global variables for sphere
float sphere_vertices[1146];
float sphere_normals[1146];
float sphere_textures[764];
unsigned short sphere_elements[2280];
int gNumVertices;
int gNumElements;

//	Global variables
FILE *gpfile;
HWND ghwnd = NULL;
bool gbActiveWindow = false;
bool gbFullScreen = false;
DWORD dwStyle;
WINDOWPLACEMENT wpPrev = { sizeof(WINDOWPLACEMENT) };
HDC ghdc = NULL;
HGLRC ghrc = NULL;
TCHAR str[256];

GLuint spoPerVertex;
GLuint spoPerFragment;

GLuint vao_sphere;
GLuint vbo_normal_sphere;
GLuint vbo_position_sphere;
GLuint vbo_element_sphere;

GLuint mUniformv, vUniformv, pUniformv;	// ModelView Matrix Uniform and Projection Matrix Uniform
GLuint LdUniformv, KdUniformv;			// Diffuse property of Light and diffuse property of material
GLuint LaUniformv, KaUniformv;			// Ambient property of Light and ambient property of material
GLuint LsUniformv, KsUniformv;			// Specular property of Light and specular property of material
GLuint materialShininessUniformv;		// material shininess
GLuint lightPositionUniformv;			// light position
GLuint keyPressedUniformv;				// is L key Pressed, to enable lighting effect

GLuint mUniformf, vUniformf, pUniformf;	// ModelView Matrix Uniform and Projection Matrix Uniform
GLuint LdUniformf, KdUniformf;			// Diffuse property of Light and diffuse property of material
GLuint LaUniformf, KaUniformf;			// Ambient property of Light and ambient property of material
GLuint LsUniformf, KsUniformf;			// Specular property of Light and specular property of material
GLuint materialShininessUniformf;		// material shininess
GLuint lightPositionUniformf;			// light position
GLuint keyPressedUniformf;				// is L key Pressed, to enable lighting effect

GLfloat angle_sphere = 0.0f;

mat4 perspectiveProjectionMatrix;

bool enableLighting = false;
bool toggle = false;

float lightAmbient[4] = { 0.0f, 0.0f, 0.0f, 0.0f };
float lightDiffuse[4] = { 1.0f, 1.0f, 1.0f, 1.0f };
float lightSpecular[4] = { 1.0f, 1.0f, 1.0f, 1.0f };
float lightPosition[4] = { 100.0f, 100.0f, 100.0f, 1.0f };

float materialAmbient[4] = { 0.0f, 0.0f, 0.0f, 0.0f };
float materialDiffuse[4] = { 1.0f, 1.0f, 1.0f, 1.0f };
float materialSpecular[4] = { 1.0f, 1.0f, 1.0f, 1.0f };
float materialShininess = 128.0f;


//WinMain()
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{
	//function declarations
	int initialize(void);
	void display(void);
	void update(void);

	//variable declarations
	WNDCLASSEX wndclass;
	HWND hwnd;
	MSG msg;
	TCHAR szAppName[] = TEXT("lit cube");
	bool bDone = false;
	int iRet = 0;

	//code
	if (fopen_s(&gpfile, "log.txt", "w") != 0)
	{
		MessageBox(NULL, TEXT("Log file cannot be created"), TEXT("Error"), MB_OK);
		exit(0);
	}

	//initialization of wndclass structure
	wndclass.cbSize = sizeof(WNDCLASSEX);
	wndclass.cbWndExtra = 0;
	wndclass.cbClsExtra = 0;
	wndclass.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
	wndclass.lpfnWndProc = WndProc;
	wndclass.hInstance = hInstance;
	wndclass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	wndclass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndclass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndclass.lpszClassName = szAppName;
	wndclass.lpszMenuName = NULL;
	wndclass.hIconSm = LoadIcon(hInstance, IDI_APPLICATION);

	//register above class 
	RegisterClassEx(&wndclass);

	//create window
	hwnd = CreateWindowEx(WS_EX_APPWINDOW,
		szAppName,
		TEXT("Toggle between per Vertex diffuse lighting and per fragment diffuse lighting - PriyaSaboo"),
		WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN | WS_CLIPSIBLINGS | WS_VISIBLE,
		100,
		100,
		WIN_WIDTH,
		WIN_HEIGHT,
		NULL,
		NULL,
		hInstance,
		NULL);

	ghwnd = hwnd;
	iRet = initialize();
	if (iRet == -1)
	{
		fprintf(gpfile, "ChoosePixelFormat() Failed\n");
		DestroyWindow(hwnd);
	}
	else if (iRet == -2)
	{
		fprintf(gpfile, "SetPixelFormat() Failed\n");
		DestroyWindow(hwnd);
	}
	else if (iRet == -3)
	{
		fprintf(gpfile, "wglCreateContext() Failed\n");
		DestroyWindow(hwnd);
	}
	else if (iRet == -4)
	{
		fprintf(gpfile, "wglMakeCurrent() Failed\n");
		DestroyWindow(hwnd);
	}
	else
		fprintf(gpfile, "Initialize() Succeded\n");

	//show window
	ShowWindow(hwnd, iCmdShow);
	SetForegroundWindow(hwnd);
	SetFocus(hwnd);

	//gameloop
	while (bDone == false)
	{
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			if (msg.message == WM_QUIT)
			{
				bDone = true;
			}
			else
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}
		else
		{
			if (gbActiveWindow == true)
			{
				//update should be called here
				update();
			}
			display();
		}
	}
	return((int)msg.wParam);
}

//callback function WndProc()
LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	//function declaration
	void resize(int, int);
	void ToggleFullScreen(void);
	void uninitialize(void);

	//variables
	static GLfloat width, height;

	//code
	switch (iMsg)
	{
	case WM_SETFOCUS:
		gbActiveWindow = true;
		break;

	case WM_KILLFOCUS:
		gbActiveWindow = false;
		break;

	case WM_ERASEBKGND:
		return(0);

	case WM_SIZE:
		width = LOWORD(lParam);
		height = HIWORD(lParam);
		resize(LOWORD(lParam), HIWORD(lParam));
		break;

	case WM_CLOSE:
		DestroyWindow(hwnd);
		break;

	case WM_KEYDOWN:
		switch (LOWORD(wParam))
		{
		case VK_ESCAPE:
			DestroyWindow(hwnd);
			break;

		case 0x46:
			ToggleFullScreen();
			break;
		}
		break;
	case WM_CHAR:
		switch (LOWORD(wParam))
		{
		case 'l':
		case 'L':
			if (enableLighting == true)
				enableLighting = false;
			else
				enableLighting = true;
			break;
		case 't':
		case 'T':
			if (toggle == true)
			{
				toggle = false;
				enableLighting = false;
			}
			else
			{
				toggle = true;
				enableLighting = false;
			}
		}
		break;
	case WM_DESTROY:
		uninitialize();	
		PostQuitMessage(0);
		break;
	}
	return(DefWindowProc(hwnd, iMsg, wParam, lParam));
}

//ToggleFullScreen() function
void ToggleFullScreen(void)
{
	MONITORINFO mi;
	if (gbFullScreen == false)
	{
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);
		if (dwStyle & WS_OVERLAPPEDWINDOW)
		{
			mi = { sizeof(MONITORINFO) };
			if (GetWindowPlacement(ghwnd, &wpPrev) && GetMonitorInfo(MonitorFromWindow(ghwnd, MONITORINFOF_PRIMARY), &mi))
			{
				SetWindowLong(ghwnd, GWL_STYLE, dwStyle & ~WS_OVERLAPPEDWINDOW);
				SetWindowPos(ghwnd,
					HWND_TOP,
					mi.rcMonitor.left,
					mi.rcMonitor.top,
					mi.rcMonitor.right - mi.rcMonitor.left,
					mi.rcMonitor.bottom - mi.rcMonitor.top,
					SWP_NOZORDER | SWP_FRAMECHANGED);
			}
		}
		ShowCursor(FALSE);
		gbFullScreen = true;
	}
	else
	{
		fprintf(gpfile, "Full Screen to normal\n");
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPED);
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd,
			HWND_TOP,
			0,
			0,
			0,
			0,
			SWP_NOZORDER | SWP_FRAMECHANGED | SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER);
		ShowCursor(TRUE);
		gbFullScreen = false;
	}
}

//initialize() function
int initialize(void)
{
	//function declarations
	void resize(int, int);
	void uninitialize(void);

	//vaiable declaraions
	PIXELFORMATDESCRIPTOR pfd;
	int iPixelFormatIndex;
	GLenum result;

	GLint iShaderCompileStatus = 0;
	GLint iProgramLinkStatus = 0;
	GLint iInfoLogLength = 0;

	GLchar *szInfoLog = NULL;

	GLuint vsoPerVertex;
	GLuint fsoPerVertex;
	GLuint vsoPerFragment;
	GLuint fsoPerFragment;

	//code
	//initialize pfd structure
	ZeroMemory(&pfd, sizeof(PIXELFORMATDESCRIPTOR));
	pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
	pfd.nVersion = 1;
	pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
	pfd.iPixelType = PFD_TYPE_RGBA;
	pfd.cColorBits = 32;
	pfd.cDepthBits = 32;
	pfd.cRedBits = 8;
	pfd.cGreenBits = 8;
	pfd.cBlueBits = 8;
	pfd.cAlphaBits = 8;

	ghdc = GetDC(ghwnd);
	iPixelFormatIndex = ChoosePixelFormat(ghdc, &pfd);
	if (iPixelFormatIndex == 0)
	{
		return(-1);
	}
	if (SetPixelFormat(ghdc, iPixelFormatIndex, &pfd) == false)
	{
		return(-2);
	}
	ghrc = wglCreateContext(ghdc);
	if (ghrc == NULL)
	{
		return(-3);
	}
	if (wglMakeCurrent(ghdc, ghrc) == false)
	{
		return(-4);
	}

	// Initialize Extensions
	result = glewInit();
	if (result != GLEW_OK)
	{
		fprintf(gpfile, "glewInit() failed\n");
		uninitialize();
		DestroyWindow(ghwnd);
	}

	// ***************************************************************************************************
	//-----------------------------------ADS PER VERTEX LIGHTING SHADERS----------------------------------
	//****************************************************************************************************
	//define vertex shader object
	vsoPerVertex = glCreateShader(GL_VERTEX_SHADER);
	
	//write vertex shader code
	const GLchar *vsoPerVertexSourceCode =
		"	#version 430 core																	" \
		"	\n																					" \
		"	in vec4 vPosition;																	" \
		"	in vec3 vNormal;																	" \
		"																					  \n" \
		"	uniform mat4 u_m_matrix;															" \
		"	uniform mat4 u_v_matrix;															" \
		"	uniform mat4 u_p_matrix;															" \
		"	uniform int u_l_key_is_pressed;														" \
		"	uniform vec3 u_ld;																	" \
		"	uniform vec3 u_kd;																	" \
		"	uniform vec3 u_la;																	" \
		"	uniform vec3 u_ka;																	" \
		"	uniform vec3 u_ls;																	" \
		"	uniform vec3 u_ks;																	" \
		"	uniform vec4 u_light_position;														" \
		"	uniform float u_material_shininess;													" \
		"																					  \n" \
		"	out vec3 phong_ads_light;															" \
		"																					  \n" \
		"	void main(void)																		" \
		"	{																					" \
		"		if(u_l_key_is_pressed == 1)														" \
		"		{																				" \
		"			vec4 eyeCoordinates = u_v_matrix * u_m_matrix * vPosition;					" \
		"			vec3 tNorm = normalize(mat3(u_v_matrix * u_m_matrix) * vNormal);			" \
		"			vec3 lightDirection = normalize(vec3(u_light_position - eyeCoordinates));	" \
		"			float tnDotld = max(dot(lightDirection, tNorm), 0.0);						" \
		"			vec3 reflectionVector = reflect(-lightDirection, tNorm);					" \
		"			vec3 viewerVector = normalize(vec3(-eyeCoordinates));						" \
		"			float rvDotvv = max(dot(reflectionVector, viewerVector), 0.0);				" \
		"																					  \n" \
		"			vec3 ambient = u_la * u_ka;													" \
		"			vec3 diffuse = u_ld * u_kd * tnDotld;										" \
		"			vec3 specular = u_ls * u_ks * pow(rvDotvv, u_material_shininess);			" \
		"																					  \n" \
		"			phong_ads_light = ambient + diffuse + specular;								" \
		"		}																				" \
		"		else																			" \
		"		{																				" \
		"			phong_ads_light = vec3(1.0, 1.0, 1.0);										" \
		"		}																				" \
		"																					  \n" \
		"	gl_Position = u_v_matrix * u_p_matrix * u_m_matrix * vPosition;						" \
		"	}																					";

	//specify above source code to the vertex shader object
	glShaderSource(vsoPerVertex, 1, (const GLchar **)&vsoPerVertexSourceCode, NULL);
	
	//compile the vertex shader
	glCompileShader(vsoPerVertex);
	glGetShaderiv(vsoPerVertex, GL_COMPILE_STATUS, &iShaderCompileStatus);
	if (iShaderCompileStatus == GL_FALSE)
	{
		glGetShaderiv(vsoPerVertex, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (GLchar *)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(vsoPerVertex, iInfoLogLength, &written, szInfoLog);
				fprintf(gpfile, "VERTEX SHADER COMPILATION LOG : %s\n", szInfoLog);
				free(szInfoLog);
				uninitialize();
				DestroyWindow(ghwnd);
				exit(0);
			}
		}
	}

	//define fragment shader object
	fsoPerVertex = glCreateShader(GL_FRAGMENT_SHADER);
	//write fragment shader code
	const GLchar *fsoPerVertexSourceCode =
		"	#version 430 core																		" \
		"	\n																						" \
		"	in vec3 phong_ads_light;																" \
		"	out vec4 fragColor;																		" \
		"																						  \n" \
		"	void main(void)																			" \
		"	{																						" \
		"		fragColor = vec4(phong_ads_light, 1.0);											" \
		"	}																						";

	//specify above source code to fragmnet shader object
	glShaderSource(fsoPerVertex, 1, (const GLchar **)&fsoPerVertexSourceCode, NULL);
	
	//compile fragment shader
	iShaderCompileStatus = 0;
	iInfoLogLength = 0;
	szInfoLog = NULL;
	glCompileShader(fsoPerVertex);
	glGetShaderiv(fsoPerVertex, GL_COMPILE_STATUS, &iShaderCompileStatus);
	if (iShaderCompileStatus == GL_FALSE)
	{
		glGetShaderiv(fsoPerVertex, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (GLchar *)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(fsoPerVertex, iInfoLogLength, &written, szInfoLog);
				fprintf(gpfile, "FRAGMENT SHADER COMPILATION LOG : %s\n", szInfoLog);
				free(szInfoLog);
				uninitialize();
				DestroyWindow(ghwnd);
				exit(0);
			}
		}
	}

	//create shader program object
	spoPerVertex = glCreateProgram();
	
	//attach vertex shader to the shader program 
	glAttachShader(spoPerVertex, vsoPerVertex);
	//attach fragment shader 
	glAttachShader(spoPerVertex, fsoPerVertex);
	
	//prelinking binding to vertex attribute
	glBindAttribLocation(spoPerVertex, AMC_ATTRIBUTE_POSITION, "vPosition");
	glBindAttribLocation(spoPerVertex, AMC_ATTRIBUTE_NORMAL, "vNormal");

	//link shader program
	glLinkProgram(spoPerVertex);
	//error checking
	iProgramLinkStatus = 0;
	iInfoLogLength = 0;
	szInfoLog = NULL;
	glGetProgramiv(spoPerVertex, GL_LINK_STATUS, &iProgramLinkStatus);
	if (iProgramLinkStatus == GL_FALSE)
	{
		glGetProgramiv(spoPerVertex, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (GLchar *)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLint written;
				glGetProgramInfoLog(spoPerVertex, iInfoLogLength, &written, szInfoLog);
				fprintf(gpfile, "SHADER PROGRAM LINK LOG : %s\n", szInfoLog);
				free(szInfoLog);
				uninitialize();
				DestroyWindow(ghwnd);
				exit(0);
			}
		}
	}

	//postlinking retieving uniform location
	vUniformv = glGetUniformLocation(spoPerVertex, "u_v_matrix");
	mUniformv = glGetUniformLocation(spoPerVertex, "u_m_matrix");
	pUniformv = glGetUniformLocation(spoPerVertex, "u_p_matrix");
	LdUniformv = glGetUniformLocation(spoPerVertex, "u_ld");
	KdUniformv = glGetUniformLocation(spoPerVertex, "u_kd");
	LaUniformv = glGetUniformLocation(spoPerVertex, "u_la");
	KaUniformv = glGetUniformLocation(spoPerVertex, "u_ka");
	LsUniformv = glGetUniformLocation(spoPerVertex, "u_ls");
	KsUniformv = glGetUniformLocation(spoPerVertex, "u_ks");
	materialShininessUniformv = glGetUniformLocation(spoPerVertex, "u_material_shininess");
	lightPositionUniformv = glGetUniformLocation(spoPerVertex, "u_light_position");
	keyPressedUniformv = glGetUniformLocation(spoPerVertex, "u_l_key_is_pressed");


	// ***************************************************************************************************
	//-----------------------------------ADS PER FRAGMENT LIGHTING SHADERS--------------------------------
	//****************************************************************************************************
	// reintitailize local variable
	iShaderCompileStatus = 0;
	iInfoLogLength = 0;
	szInfoLog = NULL;

	// create vertex shader object
	vsoPerFragment = glCreateShader(GL_VERTEX_SHADER);

	// vertex shader source code
	GLchar *vsoPerFragmentSourceCode =
		"	#version 430 core																				" \
		"																								\n	" \
		"	in vec4 vPosition;																				" \
		"	in vec3 vNormal;																				" \
		"																								\n	" \
		"	out vec4 eye_coordinates;																		" \
		"	out vec3 t_norm;																				" \
		"	out vec3 light_direction;																		" \
		"	out vec3 viewer_vector;																			" \
		"																								\n	" \
		"	uniform mat4 u_m_matrix;																		" \
		"	uniform mat4 u_v_matrix;																		" \
		"	uniform mat4 u_p_matrix;																		" \
		" 	uniform int u_l_key_is_pressed;																	" \
		"	uniform vec4 u_light_position;																	" \
		"																								\n	" \
		"	void main(void)																					" \
		"	{																								" \
		"		if(u_l_key_is_pressed == 1)																	" \
		"		{																							" \
		"			eye_coordinates = u_v_matrix * u_m_matrix * vPosition;									" \
		"			t_norm = mat3(u_v_matrix * u_m_matrix) * vNormal;										" \
		"			light_direction = vec3(u_light_position - eye_coordinates);								" \
		"			viewer_vector = vec3(-eye_coordinates);													" \
		"		}																							" \
		"		gl_Position = u_v_matrix * u_p_matrix * u_m_matrix *vPosition; 								" \
		"	}																								";

	// give source code to shader
	glShaderSource(vsoPerFragment, 1, (const GLchar**)&vsoPerFragmentSourceCode, NULL);

	//compile the vertex shader
	glCompileShader(vsoPerFragment);
	glGetShaderiv(vsoPerFragment, GL_COMPILE_STATUS, &iShaderCompileStatus);
	if (iShaderCompileStatus == GL_FALSE)
	{
		glGetShaderiv(vsoPerFragment, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (GLchar *)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(vsoPerFragment, iInfoLogLength, &written, szInfoLog);
				fprintf(gpfile, "VERTEX SHADER COMPILATION LOG 2 : %s\n", szInfoLog);
				free(szInfoLog);
				uninitialize();
				DestroyWindow(ghwnd);
				exit(0);
			}
		}
	}

	// define fragment shader object
	fsoPerFragment = glCreateShader(GL_FRAGMENT_SHADER);

	// source code
	const GLchar *fsoPerFragmentSourceCode =
		"	#version 430 core																		" \
		"	\n																						" \
		"	in vec3 t_norm;																			" \
		"	in vec3 light_direction;																	" \
		"	in vec3 viewer_vector;																	" \
		"																						  \n" \
		"	out vec4 fragColor;																		" \
		"																						  \n" \
		"	uniform vec3 u_ld;																		" \
		"	uniform vec3 u_kd;																		" \
		"	uniform vec3 u_la;																		" \
		"	uniform vec3 u_ka;																		" \
		"	uniform vec3 u_ls;																		" \
		"	uniform vec3 u_ks;																		" \
		"	uniform float u_material_shininess;														" \
		"	uniform int u_l_key_is_pressed;															" \
		"																						  \n" \
		"	void main(void)																			" \
		"	{																						" \
		"		vec3 phong_ads_light;																" \
		"		if(u_l_key_is_pressed == 1)															" \
		"		{																					" \
		"			vec3 normalizedtNorm = normalize(t_norm);										" \
		"			vec3 normalizedlightDirection = normalize(light_direction);						" \
		"			vec3 normalizedviewerVector = normalize(viewer_vector);							" \
		"																						  \n" \
		"			vec3 reflectionVector = normalize(reflect(-normalizedlightDirection, normalizedtNorm));" \
		"			float tnDotld = max(dot(normalizedlightDirection, normalizedtNorm), 0.0);		" \
		"			float rvDotvv = max(dot(reflectionVector, normalizedviewerVector), 0.0);		" \
		"																						  \n" \
		"			vec3 ambient = u_la * u_ka;														" \
		"			vec3 diffuse = u_ld * u_kd * tnDotld;											" \
		"			vec3 specular = u_ls * u_ks * pow(rvDotvv, u_material_shininess);				" \
		"			phong_ads_light = ambient + diffuse + specular;									" \
		"																						  \n" \
		"			fragColor = vec4(phong_ads_light, 1.0);											" \
		"		}																					" \
		"		else																				" \
		"		{																					" \
		"			phong_ads_light = vec3(1.0, 1.0, 1.0);											" \
		"			fragColor = vec4(phong_ads_light, 1.0);											" \
		"		}																					" \
		"	}																						";
	
		// specify source code to shader ibject
		glShaderSource(fsoPerFragment, 1, (const GLchar **)&fsoPerFragmentSourceCode, NULL);

		// compile fragment  shader
		iShaderCompileStatus = 0;
		iInfoLogLength = 0;
		szInfoLog = NULL;

	glCompileShader(fsoPerFragment);
	glGetShaderiv(fsoPerFragment, GL_COMPILE_STATUS, &iShaderCompileStatus);
	if(iShaderCompileStatus == GL_FALSE)
	{
		glGetShaderiv(fsoPerFragment, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if(iInfoLogLength > 0)
		{
			szInfoLog =	(GLchar *)malloc(iInfoLogLength);
			if(szInfoLog != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(fsoPerFragment, iInfoLogLength, &written, szInfoLog);
				fprintf(gpfile, "FRAGMENT SHADER COMPILATION LOG 2 : %s\n", szInfoLog);
				free(szInfoLog);
				uninitialize();
				DestroyWindow(ghwnd);
				exit(0);
			}
		}
	}

	spoPerFragment = glCreateProgram();

	// attach vertex shader and fragment shader to program object
	glAttachShader(spoPerFragment, vsoPerFragment);
	glAttachShader(spoPerFragment, fsoPerFragment);

	glBindAttribLocation(spoPerFragment, AMC_ATTRIBUTE_POSITION, "vPosition");
	glBindAttribLocation(spoPerFragment, AMC_ATTRIBUTE_NORMAL, "vNormal");

	// link shader program
	glLinkProgram(spoPerFragment);

	//error checking
	iProgramLinkStatus = 0;
	iInfoLogLength = 0;
	szInfoLog = NULL;
	glGetProgramiv(spoPerFragment, GL_LINK_STATUS, &iProgramLinkStatus);
	if (iProgramLinkStatus == GL_FALSE)
	{
		glGetProgramiv(spoPerFragment, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (GLchar *)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLint written;
				glGetProgramInfoLog(spoPerFragment, iInfoLogLength, &written, szInfoLog);
				fprintf(gpfile, "SHADER PROGRAM LINK LOG : %s\n", szInfoLog);
				free(szInfoLog);
				uninitialize();
				DestroyWindow(ghwnd);
				exit(0);
			}
		}
	}

	//postlinking retieving uniform location
	vUniformf = glGetUniformLocation(spoPerFragment, "u_v_matrix");
	mUniformf = glGetUniformLocation(spoPerFragment, "u_m_matrix");
	pUniformf = glGetUniformLocation(spoPerFragment, "u_p_matrix");
	LdUniformf = glGetUniformLocation(spoPerFragment, "u_ld");
	KdUniformf = glGetUniformLocation(spoPerFragment, "u_kd");
	LaUniformf = glGetUniformLocation(spoPerFragment, "u_la");
	KaUniformf = glGetUniformLocation(spoPerFragment, "u_ka");
	LsUniformf = glGetUniformLocation(spoPerFragment, "u_ls");
	KsUniformf = glGetUniformLocation(spoPerFragment, "u_ks");
	materialShininessUniformf = glGetUniformLocation(spoPerFragment, "u_material_shininess");
	lightPositionUniformf = glGetUniformLocation(spoPerFragment, "u_light_position");
	keyPressedUniformf = glGetUniformLocation(spoPerFragment, "u_l_key_is_pressed");

	getSphereVertexData(sphere_vertices, sphere_normals, sphere_textures, sphere_elements);
	gNumVertices = getNumberOfSphereVertices();
	gNumElements = getNumberOfSphereElements();

	//SPHERE
	glGenVertexArrays(1, &vao_sphere);
	glBindVertexArray(vao_sphere);
	
	//buffer for position
	glGenBuffers(1, &vbo_position_sphere);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_position_sphere);
	glBufferData(GL_ARRAY_BUFFER, sizeof(sphere_vertices), sphere_vertices, GL_STATIC_DRAW);
	glVertexAttribPointer(AMC_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	//buffer for normal
	glGenBuffers(1, &vbo_normal_sphere);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_normal_sphere);
	glBufferData(GL_ARRAY_BUFFER, sizeof(sphere_normals), sphere_normals, GL_STATIC_DRAW);
	glVertexAttribPointer(AMC_ATTRIBUTE_NORMAL, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AMC_ATTRIBUTE_NORMAL);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	//buffer for elements
	glGenBuffers(1, &vbo_element_sphere);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_element_sphere);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(sphere_elements), sphere_elements, GL_STATIC_DRAW);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

	glBindVertexArray(0);

	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);	//r,g,b,a	//clear the screen by opengl color
	
	glClearDepth(1.0f);						//giving existance to depth buffer
	glEnable(GL_DEPTH_TEST);				// enableing depth test
	glDepthFunc(GL_LEQUAL);
	//glEnable(GL_CULL_FACE);
	
	perspectiveProjectionMatrix = mat4::identity();

	//wramup call to resize
	resize(WIN_WIDTH, WIN_HEIGHT);

	return(0);
}

//update function
void update(void)
{

}
	

//display function
void display(void)
{
	//declaration of matrices
	mat4 modelMatrix;
	mat4 ViewMatrix;
	mat4 projectionMatrix;
	mat4 translationMatrix;
	mat4 rotationMatrix;
	mat4 scaleMatrix;

	//initialize above matrices to identity
	modelMatrix = mat4::identity();
	ViewMatrix = mat4::identity();
	projectionMatrix = mat4::identity();
	translationMatrix = mat4::identity();
	rotationMatrix = mat4::identity();
	scaleMatrix = mat4::identity();

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	
	//do necessary matrix multiplication
	translationMatrix = translate(0.0f, 0.0f, -3.0f);
	rotationMatrix = rotate(angle_sphere, angle_sphere, angle_sphere);
	modelMatrix = translationMatrix * scaleMatrix * rotationMatrix;
	projectionMatrix = projectionMatrix * perspectiveProjectionMatrix;

	if (toggle == true)
	{
		glUseProgram(spoPerFragment);
		if (enableLighting == true)
		{
			wsprintf(str, L"Diffuse Sphere :: ADS Per Fragment lighting - Lighting enabled");
			SetWindowText(ghwnd, str);

			glUniform1i(keyPressedUniformf, 1);
			glUniform3fv(LdUniformf, 1, lightDiffuse);
			glUniform3fv(LaUniformf, 1, lightAmbient);
			glUniform3fv(LsUniformf, 1, lightSpecular);

			glUniform3fv(KdUniformf, 1, materialDiffuse);
			glUniform3fv(KaUniformf, 1, materialAmbient);
			glUniform3fv(KsUniformf, 1, materialSpecular);

			glUniform1f(materialShininessUniformf, materialShininess);

			glUniform4fv(lightPositionUniformf, 1, lightPosition);
		}
		else
		{
			wsprintf(str, L"Diffuse Sphere :: ADS Per Fragment lighting - Lighting Disabled");
			SetWindowText(ghwnd, str);

			glUniform1i(keyPressedUniformf, 0);
		}

		//send necessary matrices to shader in respective uniforms
		glUniformMatrix4fv(mUniformf, 1, GL_FALSE, modelMatrix);
		glUniformMatrix4fv(vUniformf, 1, GL_FALSE, ViewMatrix);
		glUniformMatrix4fv(pUniformf, 1, GL_FALSE, projectionMatrix);

		//bind to vao_sphere
		glBindVertexArray(vao_sphere);
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_element_sphere);
		glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);
		//unbind vao
		glBindVertexArray(0);

		//unuse program
		glUseProgram(0);
	}
	else
	{
		glUseProgram(spoPerVertex);
		if (enableLighting == true)
		{
			wsprintf(str, L"Diffuse Sphere :: ADS Per Vertex lighting - Lighting Enabled");
			SetWindowText(ghwnd, str);

			glUniform1i(keyPressedUniformv, 1);
			glUniform3fv(LdUniformv, 1, lightDiffuse);
			glUniform3fv(LaUniformv, 1, lightAmbient);
			glUniform3fv(LsUniformv, 1, lightSpecular);

			glUniform3fv(KdUniformv, 1, materialDiffuse);
			glUniform3fv(KaUniformv, 1, materialAmbient);
			glUniform3fv(KsUniformv, 1, materialSpecular);

			glUniform1f(materialShininessUniformv, materialShininess);

			glUniform4fv(lightPositionUniformv, 1, lightPosition);
		}
		else
		{
			wsprintf(str, L"Diffuse Sphere :: ADS Per Vertex lighting - Lighting Disabled");
			SetWindowText(ghwnd, str);

			glUniform1i(keyPressedUniformv, 0);
		}

		//send necessary matrices to shader in respective uniforms
		glUniformMatrix4fv(mUniformv, 1, GL_FALSE, modelMatrix);
		glUniformMatrix4fv(vUniformv, 1, GL_FALSE, ViewMatrix);
		glUniformMatrix4fv(pUniformv, 1, GL_FALSE, projectionMatrix);

		//bind to vao_sphere
		glBindVertexArray(vao_sphere);
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_element_sphere);
		glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);
		//unbind vao
		glBindVertexArray(0);

		//unuse program
		glUseProgram(0);
	}

	SwapBuffers(ghdc);
}

//reize() function
void resize(int win_width, int win_height)
{
	if (win_height == 0)
		win_height = 1;
	glViewport(0, 0, (GLsizei)win_width, (GLsizei)win_height);
	perspectiveProjectionMatrix = perspective(45.0f, (GLfloat)win_width / (GLfloat)win_height, 0.1f, 100.0f);
}

//uninitialize() function
void uninitialize(void)
{
	if (vbo_element_sphere)
	{
		glDeleteBuffers(1, &vbo_element_sphere);
		vbo_element_sphere = 0;
	}

	if (vbo_normal_sphere)
	{
		glDeleteBuffers(1, &vbo_normal_sphere);
		vbo_normal_sphere = 0;
	}
	if (vbo_position_sphere)
	{
		glDeleteBuffers(1, &vbo_position_sphere);
		vbo_position_sphere = 0;
	}
	if (vao_sphere)
	{
		glDeleteVertexArrays(1, &vao_sphere);
		vao_sphere = 0;
	}
	
	if (spoPerVertex)
	{
		GLsizei shaderCount;
		GLsizei shaderNumber;

		glUseProgram(spoPerVertex);
		glGetProgramiv(spoPerVertex, GL_ATTACHED_SHADERS, &shaderCount);
		GLuint *pShaders = (GLuint *)malloc(sizeof(GLuint) * shaderCount);

		if (pShaders)
		{
			glGetAttachedShaders(spoPerVertex, shaderCount, &shaderCount, pShaders);
			for (shaderNumber = 0; shaderNumber < shaderCount; shaderNumber++)
			{
				//detach all the shader one by one
				glDetachShader(spoPerVertex, pShaders[shaderNumber]);
				//delete detached shader
				glDeleteShader(pShaders[shaderNumber]);
				pShaders[shaderNumber] = 0;
			}
			free(pShaders);
		}
		glDeleteProgram(spoPerVertex);
		spoPerVertex = 0;
		glUseProgram(0);
	}

	if (gbFullScreen == true)
	{
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPED);
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd,
			HWND_TOP,
			0,
			0,
			0,
			0,
			SWP_NOZORDER | SWP_FRAMECHANGED | SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER);
		ShowCursor(TRUE);
	}

	//break the current context
	if (wglGetCurrentContext() == ghrc)
	{
		wglMakeCurrent(NULL, NULL);
	}

	if (ghrc)
	{
		wglDeleteContext(ghrc);
		ghrc = NULL;
	}

	if (ghdc)
	{
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	if (gpfile)
	{
		fprintf(gpfile, "log file closed successfully\n");
		fclose(gpfile);
		gpfile = NULL;
	}
}
