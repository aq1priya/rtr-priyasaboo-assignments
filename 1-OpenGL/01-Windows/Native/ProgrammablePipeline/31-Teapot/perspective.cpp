//	Headers
#include<windows.h>
#include<stdio.h>
#include<gl/glew.h>
#include<gl/gl.h>
#include "vmath.h"	
#include "resource.h"

#pragma warning(disable : 4996)
#pragma comment (lib, "glew32.lib")
#pragma comment (lib, "opengl32.lib")

//	Macros
#define WIN_WIDTH 800
#define WIN_HEIGHT 600
#define BUFFER_SIZE 1024

//namespaces
using namespace vmath;

enum
{
	AMC_ATTRIBUTE_POSITION = 0,
	AMC_ATTRIBUTE_COLOR,
	AMC_ATTRIBUTE_NORMAL,
	AMC_ATTRIBUTE_TEXCOORD0
};

struct vec_int
{
	int *p;
	int size;
};

struct vec_float
{
	float *pf;
	int size;
};

//	Global function declaration
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

//	Global variables
FILE *gpfile;
HWND ghwnd = NULL;
bool gbActiveWindow = false;
bool gbFullScreen = false;
DWORD dwStyle;
WINDOWPLACEMENT wpPrev = { sizeof(WINDOWPLACEMENT) };
HDC ghdc = NULL;
HGLRC ghrc = NULL;

GLuint gShaderProgramObject;
GLuint vao_model;
GLuint vbo_model;
GLuint vbo_model_normal;
GLuint vbo_model_texture;
GLuint vbo_element_model;
GLuint marbleTexture;

GLuint mUniform, vUniform, pUniform;	// ModelView Matrix Uniform and Projection Matrix Uniform
GLuint LdUniform, KdUniform;			// Diffuse property of Light and diffuse property of material
GLuint LaUniform, KaUniform;			// Ambient property of Light and ambient property of material
GLuint LsUniform, KsUniform;			// Specular property of Light and specular property of material
GLuint materialShininessUniform;		// material shininess
GLuint lightPositionUniform;			// light position
GLuint keyPressedUniform;				// is L key Pressed, to enable lighting effect
GLuint samplerUniform;

bool enableLighting = false;

float rotationAngle = 0.0f;

float lightAmbient[4] = { 0.5f, 0.5f, 0.5f, 0.0f };
float lightDiffuse[4] = { 1.0, 1.0f, 1.0f, 1.0f };
float lightSpecular[4] = { 1.0f, 1.0f, 1.0f, 1.0f };
float lightPosition[4] = { 10.0f, 10.0f, 10.0f, 1.0f };

float materialAmbient[4] = { 0.5f, 0.5f, 0.5f, 0.0f };
float materialDiffuse[4] = { 1.0f, 1.0f, 1.0f, 1.0f };
float materialSpecular[4] = { 1.0f, 1.0f, 1.0f, 1.0f };
float materialShininess = 128.0f;

FILE *gp_mesh_file;
char buffer[BUFFER_SIZE];
struct vec_float *gp_vertex, *gp_texture, *gp_normal;
struct vec_float *gp_vertex_sorted, *gp_texture_sorted, *gp_normal_sorted;
struct vec_int *gp_vertex_indices, *gp_texture_indices, *gp_normal_indices;


mat4 perspectiveProjectionMatrix;

//WinMain()
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{
	//function declarations
	int initialize(void);
	void display(void);

	//variable declarations
	WNDCLASSEX wndclass;
	HWND hwnd;
	MSG msg;
	TCHAR szAppName[] = TEXT("perspective");
	bool bDone = false;
	int iRet = 0;

	//code
	if (fopen_s(&gpfile, "log.txt", "w") != 0)
	{
		MessageBox(NULL, TEXT("Log file cannot be created"), TEXT("Error"), MB_OK);
		exit(0);
	}

	//initialization of wndclass structure
	wndclass.cbSize = sizeof(WNDCLASSEX);
	wndclass.cbWndExtra = 0;
	wndclass.cbClsExtra = 0;
	wndclass.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
	wndclass.lpfnWndProc = WndProc;
	wndclass.hInstance = hInstance;
	wndclass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	wndclass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndclass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndclass.lpszClassName = szAppName;
	wndclass.lpszMenuName = NULL;
	wndclass.hIconSm = LoadIcon(hInstance, IDI_APPLICATION);

	//register above class 
	RegisterClassEx(&wndclass);

	//create window
	hwnd = CreateWindowEx(WS_EX_APPWINDOW,
		szAppName,
		TEXT("programmable model loading - PriyaSaboo"),
		WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN | WS_CLIPSIBLINGS | WS_VISIBLE,
		100,
		100,
		WIN_WIDTH,
		WIN_HEIGHT,
		NULL,
		NULL,
		hInstance,
		NULL);

	ghwnd = hwnd;
	iRet = initialize();
	if (iRet == -1)
	{
		fprintf(gpfile, "ChoosePixelFormat() Failed\n");
		DestroyWindow(hwnd);
	}
	else if (iRet == -2)
	{
		fprintf(gpfile, "SetPixelFormat() Failed\n");
		DestroyWindow(hwnd);
	}
	else if (iRet == -3)
	{
		fprintf(gpfile, "wglCreateContext() Failed\n");
		DestroyWindow(hwnd);
	}
	else if (iRet == -4)
	{
		fprintf(gpfile, "wglMakeCurrent() Failed\n");
		DestroyWindow(hwnd);
	}
	else
		fprintf(gpfile, "Initialize() Succeded\n");

	//show window
	ShowWindow(hwnd, iCmdShow);
	SetForegroundWindow(hwnd);
	SetFocus(hwnd);

	//gameloop
	while (bDone == false)
	{
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			if (msg.message == WM_QUIT)
			{
				bDone = true;
			}
			else
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}
		else
		{
			if (gbActiveWindow == true)
			{
				//update should be called here
			}
			display();
		}
	}
	return((int)msg.wParam);
}

//callback function WndProc()
LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	//function declaration
	void resize(int, int);
	void ToggleFullScreen(void);
	void uninitialize(void);

	//variables
	static GLfloat width, height;

	//code
	switch (iMsg)
	{
	case WM_SETFOCUS:
		gbActiveWindow = true;
		break;

	case WM_KILLFOCUS:
		gbActiveWindow = false;
		break;

	case WM_ERASEBKGND:
		return(0);

	case WM_SIZE:
		width = LOWORD(lParam);
		height = HIWORD(lParam);
		resize(LOWORD(lParam), HIWORD(lParam));
		break;

	case WM_CLOSE:
		DestroyWindow(hwnd);
		break;

	case WM_KEYDOWN:
		switch (LOWORD(wParam))
		{
		case VK_ESCAPE:
			DestroyWindow(hwnd);
			break;

		case 0x46:
			ToggleFullScreen();
			break;
		}
		break;

	case WM_CHAR:
		switch (LOWORD(wParam))
		{
		case 'l':
		case 'L':
			if (enableLighting == true)
				enableLighting = false;
			else
				enableLighting = true;
			break;
		}
		break;

	case WM_DESTROY:
		uninitialize();	
		PostQuitMessage(0);
		break;
	}
	return(DefWindowProc(hwnd, iMsg, wParam, lParam));
}

//ToggleFullScreen() function
void ToggleFullScreen(void)
{
	MONITORINFO mi;
	if (gbFullScreen == false)
	{
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);
		if (dwStyle & WS_OVERLAPPEDWINDOW)
		{
			mi = { sizeof(MONITORINFO) };
			if (GetWindowPlacement(ghwnd, &wpPrev) && GetMonitorInfo(MonitorFromWindow(ghwnd, MONITORINFOF_PRIMARY), &mi))
			{
				SetWindowLong(ghwnd, GWL_STYLE, dwStyle & ~WS_OVERLAPPEDWINDOW);
				SetWindowPos(ghwnd,
					HWND_TOP,
					mi.rcMonitor.left,
					mi.rcMonitor.top,
					mi.rcMonitor.right - mi.rcMonitor.left,
					mi.rcMonitor.bottom - mi.rcMonitor.top,
					SWP_NOZORDER | SWP_FRAMECHANGED);
			}
		}
		ShowCursor(FALSE);
		gbFullScreen = true;
	}
	else
	{
		fprintf(gpfile, "Full Screen to normal\n");
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPED);
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd,
			HWND_TOP,
			0,
			0,
			0,
			0,
			SWP_NOZORDER | SWP_FRAMECHANGED | SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER);
		ShowCursor(TRUE);
		gbFullScreen = false;
	}
}

//initialize() function
int initialize(void)
{
	//function declarations
	void resize(int, int);
	void uninitialize(void);
	void load_mesh(void);
	BOOL loadTexture(GLuint *Textures, TCHAR ImageResourceID[]);

	//vaiable declaraions
	PIXELFORMATDESCRIPTOR pfd;
	int iPixelFormatIndex;
	GLenum result;
	GLint iShaderCompileStatus = 0;
	GLint iInfoLogLength = 0;
	GLchar *szInfoLog = NULL;
	GLint iProgramLinkStatus = 0;
	GLuint gVertexShaderObject;
	GLuint gFragmentShaderObject;

	//code
	//initialize pfd structure
	ZeroMemory(&pfd, sizeof(PIXELFORMATDESCRIPTOR));
	pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
	pfd.nVersion = 1;
	pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
	pfd.iPixelType = PFD_TYPE_RGBA;
	pfd.cColorBits = 32;
	pfd.cDepthBits = 32;
	pfd.cRedBits = 8;
	pfd.cGreenBits = 8;
	pfd.cBlueBits = 8;
	pfd.cAlphaBits = 8;

	ghdc = GetDC(ghwnd);
	iPixelFormatIndex = ChoosePixelFormat(ghdc, &pfd);
	if (iPixelFormatIndex == 0)
	{
		return(-1);
	}
	if (SetPixelFormat(ghdc, iPixelFormatIndex, &pfd) == false)
	{
		return(-2);
	}
	ghrc = wglCreateContext(ghdc);
	if (ghrc == NULL)
	{
		return(-3);
	}
	if (wglMakeCurrent(ghdc, ghrc) == false)
	{
		return(-4);
	}
	result = glewInit();
	if (result != GLEW_OK)
	{
		fprintf(gpfile, "glewInit() failed\n");
		uninitialize();
		DestroyWindow(ghwnd);
	}

	//define vertex shader object
	gVertexShaderObject = glCreateShader(GL_VERTEX_SHADER);
	
	//write vertex shader code
	const GLchar *VertexShaderSourceCode =
		"	#version 430 core																	" \
		"	\n																					" \
		"	in vec4 vPosition;																	" \
		"	in vec3 vNormal;																	" \
		"	in vec2 vTexcoord;																	" \
		"																					  \n" \
		"	out vec4 eyeCoordinates;															" \
		"	out vec3 tNorm;																		" \
		"	out vec3 lightDirection;															" \
		"	out vec3 viewerVector;																" \
		"	out vec2 out_texcoord;																" \
		"																					  \n" \
		"	uniform mat4 u_m_matrix;															" \
		"	uniform mat4 u_v_matrix;															" \
		"	uniform mat4 u_p_matrix;															" \
		"	uniform int u_l_key_is_pressed;														" \
		"	uniform vec4 u_light_position;														" \
		"																					  \n" \
		"	void main(void)																		" \
		"	{																					" \
		"		if(u_l_key_is_pressed == 1)														" \
		"		{																				" \
		"			eyeCoordinates = u_v_matrix * u_m_matrix * vPosition;						" \
		"			tNorm = mat3(u_v_matrix * u_m_matrix) * vNormal;							" \
		"			lightDirection = vec3(u_light_position - eyeCoordinates);					" \
		"			viewerVector = vec3(-eyeCoordinates);										" \
		"		}																				" \
		"	out_texcoord = vTexcoord;														  \n" \
		"	gl_Position = u_v_matrix * u_p_matrix * u_m_matrix * vPosition;						" \
		"	}																					";


	//specify above source code to the vertex shader object
	glShaderSource(gVertexShaderObject, 1, (const GLchar **)&VertexShaderSourceCode, NULL);
	
	//compile the vertex shader
	glCompileShader(gVertexShaderObject);
	glGetShaderiv(gVertexShaderObject, GL_COMPILE_STATUS, &iShaderCompileStatus);
	if (iShaderCompileStatus == GL_FALSE)
	{
		glGetShaderiv(gVertexShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (GLchar *)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(gVertexShaderObject, iInfoLogLength, &written, szInfoLog);
				fprintf(gpfile, "VERTEX SHADER COMPILATION LOG : %s\n", szInfoLog);
				free(szInfoLog);
				uninitialize();
				DestroyWindow(ghwnd);
				exit(0);
			}
		}
	}

	//define fragment shader object
	gFragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);
	//write fragment shader code
	const GLchar *FragmentShaderSourceCode =
		"	#version 430 core																		" \
		"	\n																						" \
		"	in vec3 tNorm;																			" \
		"	in vec3 lightDirection;																	" \
		"	in vec3 viewerVector;																	" \
		"	in vec2 out_texcoord;																	" \
		"																						  \n" \
		"	out vec4 fragColor;																		" \
		"																						  \n" \
		"	uniform vec3 u_ld;																		" \
		"	uniform vec3 u_kd;																		" \
		"	uniform vec3 u_la;																		" \
		"	uniform vec3 u_ka;																		" \
		"	uniform vec3 u_ls;																		" \
		"	uniform vec3 u_ks;																		" \
		"	uniform float u_material_shininess;														" \
		"	uniform int u_l_key_is_pressed;															" \
		"	uniform sampler2D u_sampler;														  \n" \
		"																						  \n" \
		"	void main(void)																			" \
		"	{																						" \
		"		vec3 phong_ads_light;																" \
		"		vec4 texColor;																" \
		"		if(u_l_key_is_pressed == 1)															" \
		"		{																					" \
		"			vec3 normalizedtNorm = normalize(tNorm);										" \
		"			vec3 normalizedlightDirection = normalize(lightDirection);						" \
		"			vec3 normalizedviewerVector = normalize(viewerVector);							" \
		"																						  \n" \
		"			vec3 reflectionVector = normalize(reflect(-normalizedlightDirection, normalizedtNorm));" \
		"			float tnDotld = max(dot(normalizedlightDirection, normalizedtNorm), 0.0);		" \
		"			float rvDotvv = max(dot(reflectionVector, normalizedviewerVector), 0.0);		" \
		"																						  \n" \
		"			vec3 ambient = u_la * u_ka;														" \
		"			vec3 diffuse = u_ld * u_kd * tnDotld;											" \
		"			vec3 specular = u_ls * u_ks * pow(rvDotvv, u_material_shininess);				" \
		"			phong_ads_light = ambient + diffuse + specular;									" \
		"																						  \n" \
		"			texColor = texture(u_sampler, out_texcoord);								  \n" \
		"			fragColor = texColor * vec4(phong_ads_light, 1.0);								" \
		"		}																					" \
		"		else																				" \
		"		{																					" \
		"			texColor = texture(u_sampler, out_texcoord);								  \n" \
		"			phong_ads_light = vec3(1.0, 1.0, 1.0);											" \
		"			fragColor = texColor * vec4(phong_ads_light, 1.0);								" \
		"		}																					" \
		"	}																						";


	//specify above source code to fragmnet shader object
	glShaderSource(gFragmentShaderObject, 1, (const GLchar **)&FragmentShaderSourceCode, NULL);
	
	//compile fragment shader
	iShaderCompileStatus = 0;
	iInfoLogLength = 0;
	szInfoLog = NULL;
	glCompileShader(gFragmentShaderObject);
	glGetShaderiv(gFragmentShaderObject, GL_COMPILE_STATUS, &iShaderCompileStatus);
	if (iShaderCompileStatus == GL_FALSE)
	{
		glGetShaderiv(gFragmentShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (GLchar *)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(gFragmentShaderObject, iInfoLogLength, &written, szInfoLog);
				fprintf(gpfile, "FRAGMENT SHADER COMPILATION LOG : %s\n", szInfoLog);
				free(szInfoLog);
				uninitialize();
				DestroyWindow(ghwnd);
				exit(0);
			}
		}
	}

	//create shader program object
	gShaderProgramObject = glCreateProgram();
	
	//attach vertex shader to the shader program 
	glAttachShader(gShaderProgramObject, gVertexShaderObject);
	//attach fragment shader 
	glAttachShader(gShaderProgramObject, gFragmentShaderObject);
	
	//prelinking binding to vertex attribute
	glBindAttribLocation(gShaderProgramObject, AMC_ATTRIBUTE_POSITION, "vPosition");
	glBindAttribLocation(gShaderProgramObject, AMC_ATTRIBUTE_NORMAL, "vNormal");
	glBindAttribLocation(gShaderProgramObject, AMC_ATTRIBUTE_TEXCOORD0, "vTexCoord");

	//link shader program
	glLinkProgram(gShaderProgramObject);
	//error checking
	iProgramLinkStatus = 0;
	iInfoLogLength = 0;
	szInfoLog = NULL;
	glGetProgramiv(gShaderProgramObject, GL_LINK_STATUS, &iProgramLinkStatus);
	if (iProgramLinkStatus == GL_FALSE)
	{
		glGetProgramiv(gShaderProgramObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (GLchar *)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLint written;
				glGetProgramInfoLog(gShaderProgramObject, iInfoLogLength, &written, szInfoLog);
				fprintf(gpfile, "SHADER PROGRAM LINK LOG : %s\n", szInfoLog);
				free(szInfoLog);
				uninitialize();
				DestroyWindow(ghwnd);
				exit(0);
			}
		}
	}

	//postlinking retieving uniform location
	//postlinking retieving uniform location
	vUniform = glGetUniformLocation(gShaderProgramObject, "u_v_matrix");
	mUniform = glGetUniformLocation(gShaderProgramObject, "u_m_matrix");
	pUniform = glGetUniformLocation(gShaderProgramObject, "u_p_matrix");
	LdUniform = glGetUniformLocation(gShaderProgramObject, "u_ld");
	KdUniform = glGetUniformLocation(gShaderProgramObject, "u_kd");
	LaUniform = glGetUniformLocation(gShaderProgramObject, "u_la");
	KaUniform = glGetUniformLocation(gShaderProgramObject, "u_ka");
	LsUniform = glGetUniformLocation(gShaderProgramObject, "u_ls");
	KsUniform = glGetUniformLocation(gShaderProgramObject, "u_ks");
	materialShininessUniform = glGetUniformLocation(gShaderProgramObject, "u_material_shininess");
	lightPositionUniform = glGetUniformLocation(gShaderProgramObject, "u_light_position");
	keyPressedUniform = glGetUniformLocation(gShaderProgramObject, "u_l_key_is_pressed");
	samplerUniform = glGetUniformLocation(gShaderProgramObject, "u_Sampler");

	// load model data
	load_mesh();

	glGenVertexArrays(1, &vao_model);
	glBindVertexArray(vao_model);

	// buffer for position
	glGenBuffers(1, &vbo_model);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_model);
	glBufferData(GL_ARRAY_BUFFER, (gp_vertex_sorted->size)*sizeof(GLfloat), gp_vertex_sorted->pf, GL_STATIC_DRAW);
	glVertexAttribPointer(AMC_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	// buffer for normal
	glGenBuffers(1, &vbo_model_normal);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_model_normal);
	glBufferData(GL_ARRAY_BUFFER, (gp_normal_sorted->size) * sizeof(GLfloat), gp_normal_sorted->pf, GL_STATIC_DRAW);
	glVertexAttribPointer(AMC_ATTRIBUTE_NORMAL, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AMC_ATTRIBUTE_NORMAL);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	// buffer for texture
	glGenBuffers(1, &vbo_model_texture);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_model_texture);
	glBufferData(GL_ARRAY_BUFFER, (gp_texture_sorted->size) * sizeof(GLfloat), gp_texture_sorted->pf, GL_STATIC_DRAW);
	glVertexAttribPointer(AMC_ATTRIBUTE_TEXCOORD0, 2, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AMC_ATTRIBUTE_TEXCOORD0);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	// buffer for elements
	glGenBuffers(1, &vbo_element_model);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_element_model);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, gp_vertex_indices->size * sizeof(int), gp_vertex_indices->p, GL_STATIC_DRAW);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

	glBindVertexArray(0);

	bool bstatus = loadTexture(&marbleTexture, MAKEINTRESOURCE(ID_MARBLE));
	if (bstatus != true)
		fprintf(gpfile, "texture loading failed\n");
	glClearColor(0.25f, 0.25f, 0.25f, 1.0f);	//r,g,b,a	//clear the screen by opengl color
	
	glClearDepth(1.0f);						//giving existance to depth buffer
	glEnable(GL_DEPTH_TEST);				// enableing depth test
	glDepthFunc(GL_LEQUAL);

	perspectiveProjectionMatrix = mat4::identity();

	//wramup call to resize
	resize(WIN_WIDTH, WIN_HEIGHT);

	return(0);
}

//function to load texture
BOOL loadTexture(GLuint *Textures, TCHAR ImageResourceID[])
{
	//variable declarations
	HBITMAP hBitmap = NULL;
	BITMAP bmp;
	BOOL bStatus = FALSE;

	//code
	hBitmap = (HBITMAP)LoadImage(GetModuleHandle(NULL),
		ImageResourceID,
		IMAGE_BITMAP,
		0,
		0,
		LR_CREATEDIBSECTION);
	if (hBitmap)
	{
		bStatus = TRUE;
		GetObject(hBitmap, sizeof(BITMAP), &bmp);
		glPixelStorei(GL_UNPACK_ALIGNMENT, 4);
		glGenTextures(1, Textures);
		glBindTexture(GL_TEXTURE_2D, *Textures);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
		glTexImage2D(GL_TEXTURE_2D,
			0,
			GL_RGB,
			bmp.bmWidth,
			bmp.bmHeight,
			0,
			GL_BGR,
			GL_UNSIGNED_BYTE,
			bmp.bmBits);
		glGenerateMipmap(GL_TEXTURE_2D);
		DeleteObject(hBitmap);
	}
	return(bStatus);
}

//display function
void display(void)
{
	//declaration of matrices
	mat4 modelMatrix;
	mat4 ViewMatrix;
	mat4 projectionMatrix;
	mat4 translationMatrix;
	mat4 rotationMatrix;
	mat4 scaleMatrix;

	//initialize above matrices to identity
	modelMatrix = mat4::identity();
	ViewMatrix = mat4::identity();
	projectionMatrix = mat4::identity();
	translationMatrix = mat4::identity();
	rotationMatrix = mat4::identity();
	scaleMatrix = mat4::identity();

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	glUseProgram(gShaderProgramObject);
	
	//do necessary matrix multiplication
	translationMatrix = translate(0.0f, 0.0f, -20.0f);
	scaleMatrix = scale(0.3f, 0.3f, 0.3f);
	rotationMatrix = rotate(rotationAngle, 0.0f, 1.0f, 0.0f);
	modelMatrix = translationMatrix * scaleMatrix * rotationMatrix;
	projectionMatrix = projectionMatrix * perspectiveProjectionMatrix;

	if (enableLighting == true)
	{
		glUniform1i(keyPressedUniform, 1);
		glUniform3fv(LdUniform, 1, lightDiffuse);
		glUniform3fv(LaUniform, 1, lightAmbient);
		glUniform3fv(LsUniform, 1, lightSpecular);

		glUniform3fv(KdUniform, 1, materialDiffuse);
		glUniform3fv(KaUniform, 1, materialAmbient);
		glUniform3fv(KsUniform, 1, materialSpecular);

		glUniform1f(materialShininessUniform, materialShininess);

		glUniform4fv(lightPositionUniform, 1, lightPosition);
	}
	else
	{
		glUniform1i(keyPressedUniform, 0);
	}
	//send necessary matrices to shader in respective uniforms
	glUniformMatrix4fv(mUniform, 1, GL_FALSE, modelMatrix);
	glUniformMatrix4fv(vUniform, 1, GL_FALSE, ViewMatrix);
	glUniformMatrix4fv(pUniform, 1, GL_FALSE, projectionMatrix);

	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, marbleTexture);
	glUniform1i(samplerUniform, 0);

	//render geometry
	//glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
	//bind vao
	glBindVertexArray(vao_model);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_element_model);
	glDrawElements(GL_TRIANGLES, (gp_vertex_indices->size), GL_UNSIGNED_INT, NULL);
	//unbind vao
	glBindVertexArray(0);
	
	//unuse program
	glUseProgram(0);
	SwapBuffers(ghdc);

	rotationAngle = rotationAngle + 0.05f;
	if (rotationAngle > 360.0f)
		rotationAngle = 0.0f;
}

//reize() function
void resize(int win_width, int win_height)
{
	if (win_height == 0)
		win_height = 1;
	glViewport(0, 0, (GLsizei)win_width, (GLsizei)win_height);
	perspectiveProjectionMatrix = perspective(45.0f, (GLfloat)win_width / (GLfloat)win_height, 0.1f, 100.0f);
}

// load mesh function
void load_mesh(void)
{
	// function declarations
	struct vec_int *create_vec_int();
	struct vec_float *create_vec_float();
	int push_back_vec_int(struct vec_int *p_vec_int, int data);
	int push_back_vec_float(struct vec_float *p_vec_float, float data);
	void show_vec_float(struct vec_float *p_vec_float);
	void show_vec_int(struct vec_int *p_vec_int);
	int destroy_vec_float(struct vec_float *p_vec_float);

	// variable declarations
	const char *space = " ", *slash = "/";
	char *first_token = NULL, *token;
	char *f_entries[3] = { NULL, NULL, NULL };
	int nr_pos_cords = 0;
	int nr_tex_cords = 0;
	int nr_normal_cords = 0;
	int nr_faces = 0;
	int i, vi;

	fopen_s(&gp_mesh_file, "teapot1.obj", "r");
	if (gp_mesh_file == NULL)
	{
		fprintf(gpfile, "Mesh file %s can not be opened.\n", "untitled.obj");
	}
	else
		fprintf(gpfile, "Mesh file %s loaded successfully.\n", "untitled.obj");

	gp_vertex = create_vec_float();
	gp_texture = create_vec_float();
	gp_normal = create_vec_float();

	gp_vertex_indices = create_vec_int();
	gp_texture_indices = create_vec_int();
	gp_normal_indices = create_vec_int();

	while (fgets(buffer, BUFFER_SIZE, gp_mesh_file) != NULL)
	{
		first_token = strtok(buffer, space);

		if (strcmp(first_token, "v") == 0)
		{
			nr_pos_cords++;
			while ((token = strtok(NULL, space)) != NULL)
				push_back_vec_float(gp_vertex, atof(token));

		}
		else if (strcmp(first_token, "vt") == 0)
		{
			nr_tex_cords++;
			while ((token = strtok(NULL, space)) != NULL)
				push_back_vec_float(gp_texture, atof(token));

		}
		else if (strcmp(first_token, "vn") == 0)
		{
			nr_normal_cords++;
			while ((token = strtok(NULL, space)) != NULL)
				push_back_vec_float(gp_normal, atof(token));

		}
		else if (strcmp(first_token, "f") == 0)
		{
			nr_faces++;
			for (i = 0; i < 3; i++)
			{
				f_entries[i] = strtok(NULL, space);
			}

			for (i = 0; i < 3; i++)
			{
				token = strtok(f_entries[i], slash);
				push_back_vec_int(gp_vertex_indices, atoi(token) - 1);
				token = strtok(NULL, slash);
				push_back_vec_int(gp_texture_indices, atoi(token) - 1);
				token = strtok(NULL, slash);
				push_back_vec_int(gp_normal_indices, atoi(token) - 1);
			}
		}
	}

	gp_vertex_sorted = create_vec_float();
	for (i = 0; i < gp_vertex_indices->size; i++)
		push_back_vec_float(gp_vertex_sorted, gp_vertex->pf[i]);

	gp_texture_sorted = create_vec_float();
	for (i = 0; i < gp_texture_indices->size; i++)
		push_back_vec_float(gp_texture_sorted, gp_texture->pf[i]);

	gp_normal_sorted = create_vec_float();
	for (i = 0; i < gp_normal_indices->size; i++)
		push_back_vec_float(gp_normal_sorted, gp_normal->pf[i]);
	
	fclose(gp_mesh_file);
	gp_mesh_file = NULL;

	/*fprintf(gpFile, "vertices:%d texture:%d normal:%d faces:%d\n", nr_pos_cords, nr_tex_cords, nr_normal_cords, nr_faces);
	fprintf(gpFile, "vert size:%d texture size:%d normal size:%d\n", gp_vertex->size, gp_texture->size, gp_normal->size);
	fprintf(gpFile, "vert indices :%d texture indices :%d normal indices:%d\n", gp_vertex_indices->size, gp_texture_indices->size,
		gp_normal_indices->size);

	fprintf(gpFile, "Vertex Array\n");
	show_vec_float(gp_vertex);

	fprintf(gpFile, "Normal Array\n");
	show_vec_float(gp_normal);

	fprintf(gpFile, "Vertex Array Indices\n");
	show_vec_int(gp_vertex_indices);

	fprintf(gpFile, "Normal Array Indices\n");
	show_vec_int(gp_normal_indices);
	*/
}

struct vec_int *create_vec_int()
{
	struct vec_int *p = (struct vec_int *)malloc(sizeof(struct vec_int));
	memset(p, 0, sizeof(struct vec_int));
	return(p);
}

struct vec_float *create_vec_float()
{
	struct vec_float *p = (struct vec_float *)malloc(sizeof(struct vec_float));
	memset(p, 0, sizeof(struct vec_float));
	return(p);
}

int push_back_vec_int(struct vec_int *p_vec_int, int data)
{
	p_vec_int->p = (int*)realloc(p_vec_int->p, (p_vec_int->size + 1) * sizeof(int));
	p_vec_int->size = p_vec_int->size + 1;
	p_vec_int->p[p_vec_int->size - 1] = data;
	return(0);
}

int push_back_vec_float(struct vec_float *p_vec_float, float data)
{
	p_vec_float->pf = (float*)realloc(p_vec_float->pf, (p_vec_float->size + 1) * sizeof(float));
	p_vec_float->size = p_vec_float->size + 1;
	p_vec_float->pf[p_vec_float->size - 1] = data;
	return(0);
}

int destroy_vec_int(struct vec_int *p_vec_int)
{
	free(p_vec_int->p);
	free(p_vec_int);
	return(0);
}

int destroy_vec_float(struct vec_float *p_vec_float)
{
	free(p_vec_float->pf);
	free(p_vec_float);
	return (0);
}

void show_vec_float(struct vec_float *p_vec_float)
{
	int i;
	for (i = 0; i < p_vec_float->size; i++)
		fprintf(gpfile, "%f\n", p_vec_float->pf[i]);
}

void show_vec_int(struct vec_int *p_vec_int)
{
	int i;
	for (i = 0; i < p_vec_int->size; i++)
		fprintf(gpfile, "%d\n", p_vec_int->p[i]);
}

//uninitialize() function
void uninitialize(void)
{
	int destroy_vec_int(struct vec_int *p_vec_int);
	int destroy_vec_float(struct vec_float *p_vec_float);

	if (vbo_model)
	{
		glDeleteBuffers(1, &vbo_model);
		vbo_model = 0;
	}
	if (vbo_element_model)
	{
		glDeleteBuffers(1, &vbo_element_model);
		vbo_element_model = 0;
	}
	if (vao_model)
	{
		glDeleteVertexArrays(1, &vao_model);
		vao_model = 0;
	}

	destroy_vec_float(gp_vertex);
	gp_vertex = NULL;
	destroy_vec_float(gp_texture);
	gp_texture = NULL;
	destroy_vec_float(gp_normal);
	gp_normal = NULL;

	destroy_vec_float(gp_vertex_sorted);
	gp_vertex_sorted = NULL;
	destroy_vec_float(gp_texture_sorted);
	gp_texture_sorted = NULL;
	destroy_vec_float(gp_normal_sorted);
	gp_normal_sorted = NULL;

	destroy_vec_int(gp_vertex_indices);
	gp_vertex_indices = NULL;
	destroy_vec_int(gp_texture_indices);
	gp_texture_indices = NULL;
	destroy_vec_int(gp_normal_indices);
	gp_normal_indices = NULL;


	if (gShaderProgramObject)
	{
		GLsizei shaderCount;
		GLsizei shaderNumber;

		glUseProgram(gShaderProgramObject);
		glGetProgramiv(gShaderProgramObject, GL_ATTACHED_SHADERS, &shaderCount);
		GLuint *pShaders = (GLuint *)malloc(sizeof(GLuint) * shaderCount);

		if (pShaders)
		{
			glGetAttachedShaders(gShaderProgramObject, shaderCount, &shaderCount, pShaders);
			for (shaderNumber = 0; shaderNumber < shaderCount; shaderNumber++)
			{
				//detach all the shader one by one
				glDetachShader(gShaderProgramObject, pShaders[shaderNumber]);
				//delete detached shader
				glDeleteShader(pShaders[shaderNumber]);
				pShaders[shaderNumber] = 0;
			}
			free(pShaders);
		}
		glDeleteProgram(gShaderProgramObject);
		gShaderProgramObject = 0;
		glUseProgram(0);
	}

	if (gbFullScreen == true)
	{
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPED);
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd,
			HWND_TOP,
			0,
			0,
			0,
			0,
			SWP_NOZORDER | SWP_FRAMECHANGED | SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER);
		ShowCursor(TRUE);
	}

	//break the current context
	if (wglGetCurrentContext() == ghrc)
	{
		wglMakeCurrent(NULL, NULL);
	}

	if (ghrc)
	{
		wglDeleteContext(ghrc);
		ghrc = NULL;
	}

	if (ghdc)
	{
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	if (gpfile)
	{
		fprintf(gpfile, "log file closed successfully\n");
		fclose(gpfile);
		gpfile = NULL;
	}
}
