//	Headers
#include<windows.h>
#include<stdio.h>
#include<gl/glew.h>
#include<gl/gl.h>

#include "vmath.h"	
#include "Sphere.h"

#pragma comment (lib, "glew32.lib")
#pragma comment (lib, "opengl32.lib")
#pragma comment (lib, "Sphere.lib")

//	Macros
#define WIN_WIDTH 800
#define WIN_HEIGHT 600

//namespaces
using namespace vmath;

enum
{
	AMC_ATTRIBUTE_POSITION = 0,
	AMC_ATTRIBUTE_COLOR,
	AMC_ATTRIBUTE_NORMAL,
	AMC_ATTRIBUTE_TEXCOORD0
};

//	Global function declaration
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

// Sphere characteristics
struct SphereProperty
{
	vec3 spherePosition;
	vec4 MDiffuse;
	vec4 MAmbient;
	vec4 MSpecular;
	int SViewport[4];
	float sphereShininess;
};

// global variables for sphere
float sphere_vertices[1146];
float sphere_normals[1146];
float sphere_textures[764];
unsigned short sphere_elements[2280];
int gNumVertices;
int gNumElements;
SphereProperty spheres[24];

//	Global variables
FILE *gpfile;
HWND ghwnd = NULL;
bool gbActiveWindow = false;
bool gbFullScreen = false;
DWORD dwStyle;
WINDOWPLACEMENT wpPrev = { sizeof(WINDOWPLACEMENT) };
HDC ghdc = NULL;
HGLRC ghrc = NULL;
TCHAR str[256];
int gWidth;
int gHeight;

GLuint gShaderProgramObject_v;	// shader program object for per vertex lighting
GLuint gShaderProgramObject_f;	// shader program object for per fragment lighting

GLuint vao_sphere;
GLuint vbo_normal_sphere;
GLuint vbo_position_sphere;
GLuint vbo_element_sphere;

// uniform variables for per vertex lighting shader object
GLuint modelUniform_v, viewUniform_v, projectionUniform_v;
GLuint ldUniform_v;
GLuint laUniform_v;
GLuint lsUniform_v;
GLuint lightPositionUniform_v;

GLuint material_kdUniform_v;
GLuint material_kaUniform_v;
GLuint material_ksUniform_v;
GLuint material_shininessUniform_v;
GLuint keyPressedUniform_v;

// uniform variables for per fragment lighting shader object
GLuint modelUniform_f, viewUniform_f, projectionUniform_f;
GLuint ldUniform_f;
GLuint laUniform_f;
GLuint lsUniform_f;
GLuint lightPositionUniform_f;

GLuint material_kdUniform_f;
GLuint material_kaUniform_f;
GLuint material_ksUniform_f;
GLuint material_shininessUniform_f;
GLuint keyPressedUniform_f;

// light and material properties values
float lightDiffuse[4]	= {1.0f, 1.0f, 1.0f, 1.0f};
float lightAmbient[4]	= {0.0f, 0.0f, 0.0f, 0.0f};
float lightSpecular[4]	= {1.0f, 1.0f, 1.0f, 1.0f};
float lightPosition[4] = {0.0f, 0.0f, 0.0f, 1.0f};

float theta;
GLint axis = 0;
bool enableLighting = false;		// key to enable and disable lighting
bool toggleLightingType = false;	// key to toggle between per vertex lighting and per fragment lighting

mat4 perspectiveProjectionMatrix;	// orthographic projection matrix

//WinMain()
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{
	//function declarations
	int initialize(void);
	void display(void);
	void update(void);

	//variable declarations
	WNDCLASSEX wndclass;
	HWND hwnd;
	MSG msg;
	TCHAR szAppName[] = TEXT("3d shapes");
	bool bDone = false;
	int iRet = 0;

	//code
	if (fopen_s(&gpfile, "log.txt", "w") != 0)
	{
		MessageBox(NULL, TEXT("Log file cannot be created"), TEXT("Error"), MB_OK);
		exit(0);
	}

	//initialization of wndclass structure
	wndclass.cbSize = sizeof(WNDCLASSEX);
	wndclass.cbWndExtra = 0;
	wndclass.cbClsExtra = 0;
	wndclass.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
	wndclass.lpfnWndProc = WndProc;
	wndclass.hInstance = hInstance;
	wndclass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	wndclass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndclass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndclass.lpszClassName = szAppName;
	wndclass.lpszMenuName = NULL;
	wndclass.hIconSm = LoadIcon(hInstance, IDI_APPLICATION);

	//register above class 
	RegisterClassEx(&wndclass);

	//create window
	hwnd = CreateWindowEx(WS_EX_APPWINDOW,
		szAppName,
		TEXT("3d shape rotation - PriyaSaboo"),
		WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN | WS_CLIPSIBLINGS | WS_VISIBLE,
		100,
		100,
		WIN_WIDTH,
		WIN_HEIGHT,
		NULL,
		NULL,
		hInstance,
		NULL);

	ghwnd = hwnd;
	iRet = initialize();
	if (iRet == -1)
	{
		fprintf(gpfile, "ChoosePixelFormat() Failed\n");
		DestroyWindow(hwnd);
	}
	else if (iRet == -2)
	{
		fprintf(gpfile, "SetPixelFormat() Failed\n");
		DestroyWindow(hwnd);
	}
	else if (iRet == -3)
	{
		fprintf(gpfile, "wglCreateContext() Failed\n");
		DestroyWindow(hwnd);
	}
	else if (iRet == -4)
	{
		fprintf(gpfile, "wglMakeCurrent() Failed\n");
		DestroyWindow(hwnd);
	}
	else
		fprintf(gpfile, "Initialize() Succeded\n");

	//show window
	ShowWindow(hwnd, iCmdShow);
	SetForegroundWindow(hwnd);
	SetFocus(hwnd);

	//gameloop
	while (bDone == false)
	{
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			if (msg.message == WM_QUIT)
			{
				bDone = true;
			}
			else
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}
		else
		{
			if (gbActiveWindow == true)
			{
				//update should be called here
				update();
			}
			display();
		}
	}
	return((int)msg.wParam);
}

//callback function WndProc()
LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	//function declaration
	void resize(int, int);
	void ToggleFullScreen(void);
	void uninitialize(void);

	//variables
	static GLfloat width, height;

	//code
	switch (iMsg)
	{
	case WM_SETFOCUS:
		gbActiveWindow = true;
		break;

	case WM_KILLFOCUS:
		gbActiveWindow = false;
		break;

	case WM_ERASEBKGND:
		return(0);

	case WM_SIZE:
		width = LOWORD(lParam);
		height = HIWORD(lParam);
		resize(LOWORD(lParam), HIWORD(lParam));
		break;

	case WM_CLOSE:
		DestroyWindow(hwnd);
		break;

	case WM_KEYDOWN:
		switch (LOWORD(wParam))
		{
		case VK_ESCAPE:
			ToggleFullScreen();
			break;
		}
		break;
	case WM_CHAR:
	switch(LOWORD(wParam))
	{
		case 'e':
		case 'E':
			DestroyWindow(hwnd);
			break;
		case 't':
		case 'T':
			if (toggleLightingType == true)
			{
				toggleLightingType = false;
				enableLighting = false;
			}
			else
			{
				toggleLightingType = true;
				enableLighting = false;
			}
			fprintf(gpfile, "t key is pressed\n");
			break;
		case 'l':
		case 'L':
			if(enableLighting == true)
				enableLighting = false;
			else
				enableLighting = true;
			fprintf(gpfile, "l key is pressed\n");
			break;
		case 'x':
		case 'X':
			axis = 1;
			theta = 0.0f;
			lightPosition[0] = 0.0f;
			lightPosition[1] = 0.0f;
			lightPosition[2] = 0.0f;
			break;
		case 'y':
		case 'Y':
			axis = 2;
			theta = 0.0f;
			lightPosition[0] = 0.0f;
			lightPosition[1] = 0.0f;
			lightPosition[2] = 0.0f;
			break;
		case 'z':
		case 'Z':
			axis = 3;
			theta = 0.0f;
			lightPosition[0] = 0.0f;
			lightPosition[1] = 0.0f;
			lightPosition[2] = 0.0f;
			break;
	}
	break;

	case WM_DESTROY:
		uninitialize();	
		PostQuitMessage(0);
		break;
	}
	return(DefWindowProc(hwnd, iMsg, wParam, lParam));
}

//ToggleFullScreen() function
void ToggleFullScreen(void)
{
	MONITORINFO mi;
	if (gbFullScreen == false)
	{
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);
		if (dwStyle & WS_OVERLAPPEDWINDOW)
		{
			mi = { sizeof(MONITORINFO) };
			if (GetWindowPlacement(ghwnd, &wpPrev) && GetMonitorInfo(MonitorFromWindow(ghwnd, MONITORINFOF_PRIMARY), &mi))
			{
				SetWindowLong(ghwnd, GWL_STYLE, dwStyle & ~WS_OVERLAPPEDWINDOW);
				SetWindowPos(ghwnd,
					HWND_TOP,
					mi.rcMonitor.left,
					mi.rcMonitor.top,
					mi.rcMonitor.right - mi.rcMonitor.left,
					mi.rcMonitor.bottom - mi.rcMonitor.top,
					SWP_NOZORDER | SWP_FRAMECHANGED);
			}
		}
		ShowCursor(FALSE);
		gbFullScreen = true;
	}
	else
	{
		fprintf(gpfile, "Full Screen to normal\n");
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPED);
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd,
			HWND_TOP,
			0,
			0,
			0,
			0,
			SWP_NOZORDER | SWP_FRAMECHANGED | SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER);
		ShowCursor(TRUE);
		gbFullScreen = false;
	}
}

//initialize() function
int initialize(void)
{
	//function declarations
	void resize(int, int);
	void uninitialize(void);
	void initSpheres(SphereProperty spheres[24]);

	//vaiable declaraions

	PIXELFORMATDESCRIPTOR pfd;

	int iPixelFormatIndex;

	GLenum result;

	GLint iShaderCompileStatus = 0;
	GLint iInfoLogLength = 0;
	GLint iProgramLinkStatus = 0;

	GLchar *szInfoLog = NULL;

	// shader objects for per vertex lighting
	GLuint VertexShaderObject_v;
	GLuint FragmentShaderObject_v;

	GLuint VertexShaderObject_f;
	GLuint FragmentShaderObject_f;

	//code
	//initialize pfd structure
	ZeroMemory(&pfd, sizeof(PIXELFORMATDESCRIPTOR));
	pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
	pfd.nVersion = 1;
	pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
	pfd.iPixelType = PFD_TYPE_RGBA;
	pfd.cColorBits = 32;
	pfd.cDepthBits = 32;
	pfd.cRedBits = 8;
	pfd.cGreenBits = 8;
	pfd.cBlueBits = 8;
	pfd.cAlphaBits = 8;

	ghdc = GetDC(ghwnd);
	iPixelFormatIndex = ChoosePixelFormat(ghdc, &pfd);
	if (iPixelFormatIndex == 0)
	{
		return(-1);
	}
	if (SetPixelFormat(ghdc, iPixelFormatIndex, &pfd) == false)
	{
		return(-2);
	}
	ghrc = wglCreateContext(ghdc);
	if (ghrc == NULL)
	{
		return(-3);
	}
	if (wglMakeCurrent(ghdc, ghrc) == false)
	{
		return(-4);
	}

	result = glewInit();
	if (result != GLEW_OK)
	{
		fprintf(gpfile, "glewInit() failed\n");
		uninitialize();
		DestroyWindow(ghwnd);
	}

	// **********************************************************************************
	//-----------------------------Per Vertex lighting Shaders---------------------------
	// **********************************************************************************
	//define vertex shader object
	VertexShaderObject_v = glCreateShader(GL_VERTEX_SHADER);
	
	//write vertex shader code
	const GLchar *VertexShaderSourceCode_v =
		"	#version 430 core																			" \
		"																							\n	" \
		"	in vec4 vPosition;																			" \
		"	in vec3 vNormal;																			" \
		"																							\n	" \
		"	out vec3 phong_ads_light;																	" \
		"																							\n	" \
		"	uniform mat4 u_m_matrix;																	" \
		"	uniform mat4 u_v_matrix;																	" \
		"	uniform mat4 u_p_matrix;																	" \
		"	uniform int u_l_key_is_pressed;																" \
		"	uniform vec3 u_ld;																			" \
		"	uniform vec3 u_la;																			" \
		"	uniform vec3 u_ls;																			" \
		"	uniform vec3 u_ka;																			" \
		"	uniform vec3 u_kd;																			" \
		"	uniform vec3 u_ks;																			" \
		"	uniform vec4 u_light_position;																" \
		"	uniform float u_material_shininess;															" \
		"																							\n	" \
		"	void main(void)																				" \
		"	{																							" \
		"		if(u_l_key_is_pressed == 1)																" \
		"		{																						" \
		"			vec4 eyeCoordinates = u_v_matrix * u_m_matrix * vPosition;							" \
		"			vec3 tNorm			= normalize(mat3(u_v_matrix * u_m_matrix) * vNormal);			" \
		"			vec3 viewerVector   = normalize(vec3(-eyeCoordinates));								" \
		"																							\n	" \
		"			vec3 LightDirection = normalize(vec3(u_light_position - eyeCoordinates));			" \
		"			vec3 LightReflectionVector = reflect(-LightDirection, tNorm);						" \
		"			float tnDotld = max(dot(tNorm, LightDirection), 0.0);								" \
		"			float rvDotvv = max(dot(viewerVector, LightReflectionVector), 0.0);					" \
		"																							\n	" \
		"			vec3 Ambient  = u_la * u_ka;														" \
		"			vec3 Diffuse  = u_ld * u_kd * tnDotld;												" \
		"			vec3 Specular = u_ls * u_ks * pow(rvDotvv, u_material_shininess);					" \
		"																							\n	" \
		"			phong_ads_light = Ambient + Diffuse + Specular;										" \
		"		}																						" \
		"		else																					" \
		"		{																						" \
		"			phong_ads_light = vec3(1.0, 1.0, 1.0);												" \
		"		}																						" \
		"		gl_Position = u_v_matrix * u_p_matrix * u_m_matrix * vPosition;							" \
		"	}																							";

	//specify above source code to the vertex shader object
	glShaderSource(VertexShaderObject_v, 1, (const GLchar **)&VertexShaderSourceCode_v, NULL);
	
	//compile the vertex shader
	glCompileShader(VertexShaderObject_v);
	glGetShaderiv(VertexShaderObject_v, GL_COMPILE_STATUS, &iShaderCompileStatus);
	if (iShaderCompileStatus == GL_FALSE)
	{
		glGetShaderiv(VertexShaderObject_v, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (GLchar *)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(VertexShaderObject_v, iInfoLogLength, &written, szInfoLog);
				fprintf(gpfile, "VERTEX SHADER COMPILATION LOG 1 : %s\n", szInfoLog);
				free(szInfoLog);
				uninitialize();
				DestroyWindow(ghwnd);
				exit(0);
			}
		}
	}

	//define fragment shader object
	FragmentShaderObject_v = glCreateShader(GL_FRAGMENT_SHADER);

	//write fragment shader code
	const GLchar *FragmentShaderSourceCode_v =
		"	#version 430 core																			" \
		"																							\n	" \
		"	in vec3 phong_ads_light;																	" \
		"																							\n	" \
		"	out vec4 FragColor;																			" \
		"																							\n	" \
		"	void main(void)																				" \
		"	{																							" \
		"		FragColor = vec4(phong_ads_light,1.0);													" \
		"	}																							";

	//specify above source code to fragmnet shader object
	glShaderSource(FragmentShaderObject_v, 1, (const GLchar **)&FragmentShaderSourceCode_v, NULL);
	
	//compile fragment shader
	iShaderCompileStatus = 0;
	iInfoLogLength = 0;
	szInfoLog = NULL;
	
	//error checking
	glCompileShader(FragmentShaderObject_v);
	glGetShaderiv(FragmentShaderObject_v, GL_COMPILE_STATUS, &iShaderCompileStatus);
	if (iShaderCompileStatus == GL_FALSE)
	{
		glGetShaderiv(FragmentShaderObject_v, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (GLchar *)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(FragmentShaderObject_v, iInfoLogLength, &written, szInfoLog);
				fprintf(gpfile, "FRAGMENT SHADER COMPILATION LOG 1 : %s\n", szInfoLog);
				free(szInfoLog);
				uninitialize();
				DestroyWindow(ghwnd);
				exit(0);
			}
		}
	}

	//create shader program object
	gShaderProgramObject_v = glCreateProgram();
	
	//attach vertex shader to the shader program 
	glAttachShader(gShaderProgramObject_v, VertexShaderObject_v);
	//attach fragment shader 
	glAttachShader(gShaderProgramObject_v, FragmentShaderObject_v);
	
	//prelinking binding to vertex attribute
	glBindAttribLocation(gShaderProgramObject_v, AMC_ATTRIBUTE_POSITION, "vPosition");
	glBindAttribLocation(gShaderProgramObject_v, AMC_ATTRIBUTE_NORMAL, "vNormal");

	//link shader program
	glLinkProgram(gShaderProgramObject_v);

	//error checking
	iProgramLinkStatus = 0;
	iInfoLogLength = 0;
	szInfoLog = NULL;
	
	glGetProgramiv(gShaderProgramObject_v, GL_LINK_STATUS, &iProgramLinkStatus);
	if (iProgramLinkStatus == GL_FALSE)
	{
		glGetProgramiv(gShaderProgramObject_v, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (GLchar *)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLint written;
				glGetProgramInfoLog(gShaderProgramObject_v, iInfoLogLength, &written, szInfoLog);
				fprintf(gpfile, "SHADER PROGRAM LINK LOG 1 : %s\n", szInfoLog);
				free(szInfoLog);
				uninitialize();
				DestroyWindow(ghwnd);
				exit(0);
			}
		}
	}

	//postlinking retieving uniform location
	// model, view and projection matrices
	viewUniform_v = glGetUniformLocation(gShaderProgramObject_v, "u_v_matrix");
	modelUniform_v = glGetUniformLocation(gShaderProgramObject_v, "u_m_matrix");
	projectionUniform_v = glGetUniformLocation(gShaderProgramObject_v, "u_p_matrix");
	// blue light uniforms
	ldUniform_v = glGetUniformLocation(gShaderProgramObject_v, "u_ld");
	laUniform_v = glGetUniformLocation(gShaderProgramObject_v, "u_la");
	lsUniform_v = glGetUniformLocation(gShaderProgramObject_v, "u_ls");
	lightPositionUniform_v = glGetUniformLocation(gShaderProgramObject_v, "u_light_position");
	// material uniform
	material_kdUniform_v = glGetUniformLocation(gShaderProgramObject_v, "u_kd");
	material_kaUniform_v = glGetUniformLocation(gShaderProgramObject_v, "u_ka");
	material_ksUniform_v = glGetUniformLocation(gShaderProgramObject_v, "u_ks");
	material_shininessUniform_v = glGetUniformLocation(gShaderProgramObject_v, "u_material_shininess");
	// key pressed uniform
	keyPressedUniform_v = glGetUniformLocation(gShaderProgramObject_v, "u_l_key_is_pressed");

	//**********************************************************************************************************
	//-----------------------------------Shaders For Per Fragment Lighting--------------------------------------
	//**********************************************************************************************************
	//define vertex shader object
	VertexShaderObject_f = glCreateShader(GL_VERTEX_SHADER);
	
	//write vertex shader code
	const GLchar *VertexShaderSourceCode_f =
		"	#version 430 core																			" \
		"																							\n	" \
		"	in vec4 vPosition;																			" \
		"	in vec3 vNormal;																			" \
		"																							\n	" \
		"	out vec4 eyeCoordinates;																	" \
		"	out vec3 tNorm;																				" \
		"	out vec3 viewerVector;																		" \
		"	out vec3 LightDirection;																	" \
		"																							\n	" \
		"	uniform mat4 u_m_matrix;																	" \
		"	uniform mat4 u_v_matrix;																	" \
		"	uniform mat4 u_p_matrix;																	" \
		"	uniform int u_l_key_is_pressed;																" \
		"	uniform vec4 u_light_position;																" \
		"																							\n	" \
		"	void main(void)																				" \
		"	{																							" \
		"		if(u_l_key_is_pressed == 1)																" \
		"		{																						" \
		"			eyeCoordinates 	= u_v_matrix * u_m_matrix * vPosition;								" \
		"			tNorm			= mat3(u_v_matrix * u_m_matrix) * vNormal;							" \
		"			viewerVector   	= vec3(-eyeCoordinates);											" \
		"																							\n	" \
		"			LightDirection = vec3(u_light_position - eyeCoordinates);							" \
		"		}																						" \
		"		gl_Position = u_v_matrix * u_p_matrix * u_m_matrix * vPosition;							" \
		"	}																							";

	//specify above source code to the vertex shader object
	glShaderSource(VertexShaderObject_f, 1, (const GLchar **)&VertexShaderSourceCode_f, NULL);
	
	//compile the vertex shader
	glCompileShader(VertexShaderObject_f);
	glGetShaderiv(VertexShaderObject_f, GL_COMPILE_STATUS, &iShaderCompileStatus);
	if (iShaderCompileStatus == GL_FALSE)
	{
		glGetShaderiv(VertexShaderObject_f, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (GLchar *)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(VertexShaderObject_f, iInfoLogLength, &written, szInfoLog);
				fprintf(gpfile, "VERTEX SHADER COMPILATION LOG 2 : %s\n", szInfoLog);
				free(szInfoLog);
				uninitialize();
				DestroyWindow(ghwnd);
				exit(0);
			}
		}
	}

	//define fragment shader object
	FragmentShaderObject_f = glCreateShader(GL_FRAGMENT_SHADER);

	//write fragment shader code
	const GLchar *FragmentShaderSourceCode_f =
		"	#version 430 core																					" \
		"																									\n	" \
		"	in vec3 tNorm;																						" \
		"	in vec3 viewerVector;																				" \
		"	in vec3 LightDirection;																				" \
		"																									\n	" \
		"	uniform vec3 u_ld;																					" \
		"	uniform vec3 u_la;																					" \
		"	uniform vec3 u_ls;																					" \
		"	uniform vec3 u_ka;																					" \
		"	uniform vec3 u_kd;																					" \
		"	uniform vec3 u_ks;																					" \
		"	uniform float u_material_shininess;																	" \
		"	uniform int u_l_key_is_pressed;																		" \
		"																									\n	" \
		"	out vec4 FragColor;																					" \
		"																									\n	" \
		"	void main(void)																						" \
		"	{																									" \
		"		vec3 phong_ads_light;																			" \
		"		if(u_l_key_is_pressed == 1)																		" \
		"		{																								" \
		"			vec3 normalizedtNorm = normalize(tNorm);													" \
		"			vec3 normalizedViewerVector		  = normalize(viewerVector);								" \
		"			vec3 normalizedLightDirection = normalize(LightDirection);									" \
		"																									\n	" \
		"			vec3 LightReflectionVector = reflect(-normalizedLightDirection, normalizedtNorm);			" \
		"																									\n	" \
		"			float tnDotld = max(dot(normalizedtNorm, normalizedLightDirection), 0.0);					" \
		"			float rvDotvv = max(dot(normalizedViewerVector, LightReflectionVector), 0.0);				" \
		"																									\n	" \
		"			vec3 Ambient  = u_la * u_ka;																" \
		"			vec3 Diffuse  = u_ld * u_kd * tnDotld;														" \
		"			vec3 Specular = u_ls * u_ks * pow(rvDotvv, u_material_shininess);							" \
		"																									\n	" \
		"			phong_ads_light = Ambient + Diffuse + Specular;												" \
		"			FragColor = vec4(phong_ads_light,1.0);														" \
		"		}																								" \
		"		else																							" \
		"		{																								" \
		"			phong_ads_light = vec3(1.0, 1.0, 1.0);														" \
		"			FragColor = vec4(phong_ads_light,1.0);														" \
		"		}																								" \
		"																									\n	" \
		"	}																									";

	//specify above source code to fragmnet shader object
	glShaderSource(FragmentShaderObject_f, 1, (const GLchar **)&FragmentShaderSourceCode_f, NULL);
	
	//compile fragment shader
	iShaderCompileStatus = 0;
	iInfoLogLength = 0;
	szInfoLog = NULL;
	
	//error checking
	glCompileShader(FragmentShaderObject_f);
	glGetShaderiv(FragmentShaderObject_f, GL_COMPILE_STATUS, &iShaderCompileStatus);
	if (iShaderCompileStatus == GL_FALSE)
	{
		glGetShaderiv(FragmentShaderObject_f, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (GLchar *)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(FragmentShaderObject_f, iInfoLogLength, &written, szInfoLog);
				fprintf(gpfile, "FRAGMENT SHADER COMPILATION LOG 2 : %s\n", szInfoLog);
				free(szInfoLog);
				uninitialize();
				DestroyWindow(ghwnd);
				exit(0);
			}
		}
	}

	//create shader program object
	gShaderProgramObject_f = glCreateProgram();
	
	//attach vertex shader to the shader program 
	glAttachShader(gShaderProgramObject_f, VertexShaderObject_f);
	//attach fragment shader 
	glAttachShader(gShaderProgramObject_f, FragmentShaderObject_f);
	
	//prelinking binding to vertex attribute
	glBindAttribLocation(gShaderProgramObject_f, AMC_ATTRIBUTE_POSITION, "vPosition");
	glBindAttribLocation(gShaderProgramObject_f, AMC_ATTRIBUTE_NORMAL, "vNormal");

	//link shader program
	glLinkProgram(gShaderProgramObject_f);

	//error checking
	iProgramLinkStatus = 0;
	iInfoLogLength = 0;
	szInfoLog = NULL;
	
	glGetProgramiv(gShaderProgramObject_f, GL_LINK_STATUS, &iProgramLinkStatus);
	if (iProgramLinkStatus == GL_FALSE)
	{
		glGetProgramiv(gShaderProgramObject_f, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (GLchar *)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLint written;
				glGetProgramInfoLog(gShaderProgramObject_f, iInfoLogLength, &written, szInfoLog);
				fprintf(gpfile, "SHADER PROGRAM LINK LOG 2 : %s\n", szInfoLog);
				free(szInfoLog);
				uninitialize();
				DestroyWindow(ghwnd);
				exit(0);
			}
		}
	}

	//postlinking retieving uniform location
	// model, view and projection matrices
	viewUniform_f  		= glGetUniformLocation(gShaderProgramObject_f, "u_v_matrix");
	modelUniform_f 		= glGetUniformLocation(gShaderProgramObject_f, "u_m_matrix");
	projectionUniform_f = glGetUniformLocation(gShaderProgramObject_f, "u_p_matrix");
	// light uniforms
	ldUniform_f = glGetUniformLocation(gShaderProgramObject_f, "u_ld");
	laUniform_f = glGetUniformLocation(gShaderProgramObject_f, "u_la");
	lsUniform_f = glGetUniformLocation(gShaderProgramObject_f, "u_ls");
	lightPositionUniform_f = glGetUniformLocation(gShaderProgramObject_f, "u_light_position");
	// material uniform
	material_kdUniform_f = glGetUniformLocation(gShaderProgramObject_f, "u_kd");
	material_kaUniform_f = glGetUniformLocation(gShaderProgramObject_f, "u_ka");
	material_ksUniform_f = glGetUniformLocation(gShaderProgramObject_f, "u_ks");
	material_shininessUniform_f = glGetUniformLocation(gShaderProgramObject_f, "u_material_shininess");
	// key pressed uniform
	keyPressedUniform_f = glGetUniformLocation(gShaderProgramObject_f, "u_l_key_is_pressed");

	// Obtained sphere vertices, normals, and texture coordinates
	getSphereVertexData(sphere_vertices, sphere_normals, sphere_textures, sphere_elements);
	gNumVertices = getNumberOfSphereVertices();
	gNumElements = getNumberOfSphereElements();

	// SPHERE
	glGenVertexArrays(1, &vao_sphere);
	glBindVertexArray(vao_sphere);
	//buffer for position
	glGenBuffers(1, &vbo_position_sphere);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_position_sphere);
	glBufferData(GL_ARRAY_BUFFER, sizeof(sphere_vertices), sphere_vertices, GL_STATIC_DRAW);
	glVertexAttribPointer(AMC_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	// buffer for normals
	glGenBuffers(1, &vbo_normal_sphere);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_normal_sphere);
	glBufferData(GL_ARRAY_BUFFER, sizeof(sphere_normals), sphere_normals, GL_STATIC_DRAW);
	glVertexAttribPointer(AMC_ATTRIBUTE_NORMAL, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AMC_ATTRIBUTE_NORMAL);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindVertexArray(0); // unbinding with trianle vao
	// buffer for elements
	glGenBuffers(1, &vbo_element_sphere);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_element_sphere);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(sphere_elements), sphere_elements, GL_STATIC_DRAW);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);	//r,g,b,a	//clear the screen by opengl color
	
	glClearDepth(1.0f);						//giving existance to depth buffer
	glEnable(GL_DEPTH_TEST);				// enableing depth test
	glDepthFunc(GL_LEQUAL);
	//glEnable(GL_CULL_FACE);
	
	perspectiveProjectionMatrix = mat4::identity();

	
	//wramup call to resize
	resize(WIN_WIDTH, WIN_HEIGHT);

	return(0);
}

//update function
void update(void)
{
	theta = theta + 0.005f;
	if (theta > 360)
	{
		theta = 0.0f;
	}
}

//display function
void display(void)
{
	//declaration of matrices
	mat4 modelMatrix;
	mat4 viewMatrix;
	mat4 projectionMatrix;
	mat4 translationMatrix;
	mat4 rotationMatrix;
	mat4 scaleMatrix;

	//initialize above matrices to identity
	modelMatrix = mat4::identity();
	viewMatrix = mat4::identity();
	projectionMatrix = mat4::identity();
	translationMatrix = mat4::identity();
	rotationMatrix = mat4::identity();
	scaleMatrix = mat4::identity();

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	//do necessary matrix multiplication
	for (int i = 0; i < 24; i++)
	{
		//initialize above matrices to identity
		modelMatrix = mat4::identity();
		viewMatrix = mat4::identity();
		projectionMatrix = mat4::identity();
		translationMatrix = mat4::identity();
		rotationMatrix = mat4::identity();
		scaleMatrix = mat4::identity();

		scaleMatrix = scale(0.5f, 0.5f, 0.5f);
		translationMatrix = translate(spheres[i].spherePosition);
		modelMatrix = translationMatrix * scaleMatrix * rotationMatrix;
		projectionMatrix = projectionMatrix * perspectiveProjectionMatrix;
	
		if(toggleLightingType == false)
		{

			glUseProgram(gShaderProgramObject_v);
			if(enableLighting == true)
			{
				wsprintf(str, L"Three Rotating Lights On Steady Sphere :: ADS Per Vertex lighting - Lighting enabled");
				SetWindowText(ghwnd, str);

				// calculate position of rotating lights
				if (axis == 3)
				{
					lightPosition[0] = 15 *cosf(theta);
					lightPosition[1] = 15 * sinf(theta);
					lightPosition[2] = 0.0f;
				}
				if (axis == 2)
				{
					lightPosition[0] = 15 * cosf(theta);
					lightPosition[1] = 0.0f;
					lightPosition[2] = 15 * sinf(theta);
				}
				if (axis == 1)
				{
					lightPosition[0] = 0.0f;
					lightPosition[1] = 15 * cosf(theta);
					lightPosition[2] = 15 * sinf(theta);
				}

				//send necessary matrices to shader in respective uniforms
				glUniform1i(keyPressedUniform_v, 1);
				glUniform3fv(ldUniform_v, 1, lightDiffuse);
				glUniform3fv(laUniform_v, 1, lightAmbient);
				glUniform3fv(lsUniform_v, 1, lightSpecular);

				glUniform3fv(material_kdUniform_v, 1, spheres[i].MDiffuse);
				glUniform3fv(material_kaUniform_v, 1, spheres[i].MAmbient);
				glUniform3fv(material_ksUniform_v, 1, spheres[i].MSpecular);

				glUniform1f(material_shininessUniform_v, spheres[i].sphereShininess);

				glUniform4fv(lightPositionUniform_v, 1, lightPosition);
			}
			else
			{
				wsprintf(str, L"Three Rotating Lights On Steady Sphere :: ADS Per Vertex lighting - Lighting disabled");
				SetWindowText(ghwnd, str);

				glUniform1i(keyPressedUniform_v, 0);
			}
			glUniformMatrix4fv(modelUniform_v, 1, GL_FALSE, modelMatrix);
			glUniformMatrix4fv(viewUniform_v, 1, GL_FALSE, viewMatrix);
			glUniformMatrix4fv(projectionUniform_v, 1, GL_FALSE, projectionMatrix);
		
			glViewport((GLint)spheres[i].SViewport[0], (GLint)spheres[i].SViewport[1], (GLsizei)spheres[i].SViewport[2], (GLsizei)spheres[i].SViewport[3]);
			//bind to vao_sphere
			glBindVertexArray(vao_sphere);
			glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_element_sphere);
			glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);
			//unbind vao
			glBindVertexArray(0);

			//unuse program
			glUseProgram(0);
		}
		else
		{
			glUseProgram(gShaderProgramObject_f);
			if (enableLighting == true)
			{
				wsprintf(str, L" Three Rotating Lights On Steady Sphere :: ADS Per Fragment lighting - Lighting enabled ");
				SetWindowText(ghwnd, str);

				// calculate position of rotating lights
				if (axis == 3)	// z
				{
					lightPosition[0] = 15 * cosf(theta);
					lightPosition[1] = 15 * sinf(theta);
					lightPosition[2] = 0.0f;
				}
				if (axis == 2)	// y
				{
					lightPosition[0] = 15 * cosf(theta);
					lightPosition[1] = 0.0f;
					lightPosition[2] = 15 * sinf(theta);
				}
				if (axis == 1)	// x
				{
					lightPosition[0] = 0.0f;
					lightPosition[1] = 15 * cosf(theta);
					lightPosition[2] = 15 * sinf(theta);
				}

				//send necessary matrices to shader in respective uniforms
				glUniform1i(keyPressedUniform_f, 1);
				glUniform3fv(ldUniform_f, 1, lightDiffuse);
				glUniform3fv(laUniform_f, 1, lightAmbient);
				glUniform3fv(lsUniform_f, 1, lightSpecular);

				glUniform3fv(material_kdUniform_f, 1,spheres[i].MDiffuse);
				glUniform3fv(material_kaUniform_f, 1, spheres[i].MAmbient);
				glUniform3fv(material_ksUniform_f, 1, spheres[i].MSpecular);

				glUniform1f(material_shininessUniform_f, spheres[i].sphereShininess);

				glUniform4fv(lightPositionUniform_f, 1, lightPosition);
			}
			else
			{
				wsprintf(str, L"Three Rotating Lights On Steady Sphere :: ADS Per Fragment lighting - Lighting disabled");
				SetWindowText(ghwnd, str);

				glUniform1i(keyPressedUniform_f, 0);
			}
			glUniformMatrix4fv(modelUniform_f, 1, GL_FALSE, modelMatrix);
			glUniformMatrix4fv(viewUniform_f, 1, GL_FALSE, viewMatrix);
			glUniformMatrix4fv(projectionUniform_f, 1, GL_FALSE, projectionMatrix);

			glViewport((GLint)spheres[i].SViewport[0], (GLint)spheres[i].SViewport[1], (GLsizei)spheres[i].SViewport[2], (GLsizei)spheres[i].SViewport[3]);

			//bind to vao_sphere
			glBindVertexArray(vao_sphere);
			glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_element_sphere);
			glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);
			//unbind vao
			glBindVertexArray(0);
			//unuse program
			glUseProgram(0);
		}	// enf of if_else
		
	}	// end of for
	
	SwapBuffers(ghdc);
}

//reize() function
void resize(int win_width, int win_height)
{
	void initSpheres(SphereProperty Spheres[24]);

	gWidth = win_width;
	gHeight = win_height;

	if (win_height == 0)
		win_height = 1;
	//glViewport(0, 0, (GLsizei)win_width, (GLsizei)win_height);
	perspectiveProjectionMatrix = perspective(45.0f, (GLfloat)win_width / (GLfloat)win_height, 0.1, 100.0f);

	initSpheres(spheres);
}

// initiliaze sphere properties
void initSpheres(SphereProperty Spheres[24])
{
	//****************** 1st column GEMS***************
	// EMARALD
	Spheres[0].MAmbient = { 0.0215f, 0.1745f, 0.0215f, 1.0f };
	Spheres[0].MDiffuse = { 0.07568f, 0.61424f, 0.07568f, 1.0f };
	Spheres[0].MSpecular = { 0.633f, 0.727811f, 0.633f, 1.0f };
	Spheres[0].spherePosition = { 0.0f, 0.0f, -3.0f };
	Spheres[0].SViewport[0] = 0;
	Spheres[0].SViewport[1] = 4 * gHeight / 6;
	Spheres[0].SViewport[2] = gWidth / 2;
	Spheres[0].SViewport[3] = gHeight / 2;
	Spheres[0].sphereShininess = 0.6f * 128.0f;
	// JADE
	Spheres[1].MAmbient = { 0.135f, 0.2225f, 0.1575f, 1.0f };
	Spheres[1].MDiffuse = { 0.54f, 0.89f, 0.63f, 1.0f };
	Spheres[1].MSpecular = { 0.316228f, 0.316228f, 0.316228f, 1.0f };
	Spheres[1].spherePosition = { 0.0f, 0.0f, -3.0f };
	Spheres[1].SViewport[0] = 0;
	Spheres[1].SViewport[1] = 3* gHeight / 6;
	Spheres[1].SViewport[2] = gWidth / 2;
	Spheres[1].SViewport[3] = gHeight / 2;
	Spheres[1].sphereShininess = 0.1f * 128.0f;
	// OBSIDIAN
	Spheres[2].MAmbient = { 0.05375f, 0.05f, 0.06625f, 1.0f };
	Spheres[2].MDiffuse = { 0.18275f, 0.17f, 0.22525f, 1.0f };
	Spheres[2].MSpecular = { 0.332741f, 0.328634f, 0.346435f, 1.0f };
	Spheres[2].spherePosition = { 0.0f, 0.0f, -3.0f };
	Spheres[2].SViewport[0] = 0;
	Spheres[2].SViewport[1] = gHeight / 3;
	Spheres[2].SViewport[2] = gWidth / 2;
	Spheres[2].SViewport[3] = gHeight / 2;
	Spheres[2].sphereShininess = 0.3f * 128.0f;
	// PEARL
	Spheres[3].MAmbient = { 0.25f, 0.20725f, 0.20725f, 1.0f };
	Spheres[3].MDiffuse = { 1.0f, 0.829f, 0.829f, 1.0f };
	Spheres[3].MSpecular = { 0.296648f, 0.296648f, 0.296648f, 1.0f }; 
	Spheres[3].spherePosition = { 0.0f, 0.0f, -3.0f };
	Spheres[3].SViewport[0] = 0;
	Spheres[3].SViewport[1] = gHeight / 6;
	Spheres[3].SViewport[2] = gWidth / 2;
	Spheres[3].SViewport[3] = gHeight / 2;
	Spheres[3].sphereShininess = 0.88f * 128.0f;
	// RUBY
	Spheres[4].MAmbient = { 0.1745f, 0.01175f, 0.01175f, 1.0f };
	Spheres[4].MDiffuse = { 0.61424f, 0.04136f, 0.04136f, 1.0f };
	Spheres[4].MSpecular = { 0.727811f, 0.626959f, 0.626959f, 1.0f };
	Spheres[4].spherePosition = { 0.0f, 0.0f, -3.0f };
	Spheres[4].SViewport[0] = 0;
	Spheres[4].SViewport[1] = 0;
	Spheres[4].SViewport[2] = gWidth / 2;
	Spheres[4].SViewport[3] = gHeight / 2;
	Spheres[4].sphereShininess = 0.6f * 128.0f;
	// TURQUOISE
	Spheres[5].MAmbient = { 0.1f, 0.18725f, 0.1745f, 1.0f };
	Spheres[5].MDiffuse = { 0.396f, 0.74151f, 0.69102f, 1.0f };
	Spheres[5].MSpecular = { 0.297254f, 0.30829f, 0.306678f, 1.0f };
	Spheres[5].spherePosition = { 0.0f, 0.0f, -3.0f };
	Spheres[5].SViewport[0] = 0;
	Spheres[5].SViewport[1] = -gHeight / 6;
	Spheres[5].SViewport[2] = gWidth / 2;
	Spheres[5].SViewport[3] = gHeight / 2;
	Spheres[5].sphereShininess = 0.1f * 128.0f;

	// **************** 2nd Column : Metal **************************
	// BRASS
	Spheres[6].MAmbient = { 0.329412f, 0.223529f, 0.027451f, 1.0f };
	Spheres[6].MDiffuse = { 0.780392f, 0.568627f, 0.113725f, 1.0f };
	Spheres[6].MSpecular = { 0.992157f, 0.941176f, 0.807843f, 1.0f };
	Spheres[6].spherePosition  = { 0.0f, 0.0f, -3.0f };
	Spheres[6].SViewport[0] = gWidth / 6;
	Spheres[6].SViewport[1] = 4 * gHeight / 6;
	Spheres[6].SViewport[2] = gWidth / 2;
	Spheres[6].SViewport[3] = gHeight / 2;
	Spheres[6].sphereShininess = 0.21794872f * 128.0f;
	// BRONZE
	Spheres[7].MAmbient = { 0.2125f, 0.1275f, 0.054f, 1.0f };
	Spheres[7].MDiffuse = { 0.714f, 0.4284f, 0.18144f, 1.0f };
	Spheres[7].MSpecular = { 0.393548f, 0.271906f, 0.166721f, 1.0f };
	Spheres[7].spherePosition = { 0.0f, 0.0f, -3.0f };
	Spheres[7].SViewport[0] = gWidth / 6;
	Spheres[7].SViewport[1] = 3 * gHeight / 6;
	Spheres[7].SViewport[2] = gWidth / 2;
	Spheres[7].SViewport[3] = gHeight / 2;
	Spheres[7].sphereShininess = 0.2f * 128.0f;
	// CHROME
	Spheres[8].MAmbient = { 0.25f, 0.25f, 0.25f, 1.0f };
	Spheres[8].MDiffuse = { 0.4f, 0.4f, 0.4f, 1.0f };
	Spheres[8].MSpecular = { 0.774597f, 0.774597f, 0.774597f, 1.0f };
	Spheres[8].spherePosition = { 0.0f, 0.0f, -3.0f };
	Spheres[8].SViewport[0] = gWidth / 6;
	Spheres[8].SViewport[1] = 2 * gHeight / 6;
	Spheres[8].SViewport[2] = gWidth / 2;
	Spheres[8].SViewport[3] = gHeight / 2;
	Spheres[8].sphereShininess = 0.6f * 128.0f;
	// COPPER
	Spheres[9].MAmbient = { 0.19125f, 0.0735f, 0.0225f, 1.0f };
	Spheres[9].MDiffuse = { 0.7038f, 0.27048f, 0.0828f, 1.0f };
	Spheres[9].MSpecular = { 0.256777f, 0.137622f, 0.086014f, 1.0f };
	Spheres[9].spherePosition = { 0.0f, 0.0f, -3.0f };
	Spheres[9].SViewport[0] = gWidth / 6;
	Spheres[9].SViewport[1] = gHeight / 6;
	Spheres[9].SViewport[2] = gWidth / 2;
	Spheres[9].SViewport[3] = gHeight / 2;
	Spheres[9].sphereShininess = 0.1f * 128.0f;
	// GOLD
	Spheres[10].MAmbient = { 0.24725f, 0.1995f, 0.0745f, 1.0f };
	Spheres[10].MDiffuse = { 0.75164f, 0.555802f, 0.366065f, 1.0f };
	Spheres[10].MSpecular = { 0.628281f, 0.555802f, 0.366065f, 1.0f };
	Spheres[10].spherePosition = { 0.0f, 0.0f, -3.0f };
	Spheres[10].SViewport[0] = gWidth / 6;
	Spheres[10].SViewport[1] = 0;
	Spheres[10].SViewport[2] = gWidth / 2;
	Spheres[10].SViewport[3] = gHeight / 2;
	Spheres[10].sphereShininess = 0.4f * 128.0f;
	// SILVER
	Spheres[11].MAmbient = { 0.19225f, 0.19225f, 0.19225f, 1.0f };
	Spheres[11].MDiffuse = { 0.50754f, 0.50754f, 0.50754f, 1.0f };
	Spheres[11].MSpecular = { 0.508273f, 0.508273f, 0.508273f, 1.0f };
	Spheres[11].spherePosition = { 0.0f, 0.0f, -3.0f };
	Spheres[11].SViewport[0] = gWidth / 6;
	Spheres[11].SViewport[1] = -gHeight / 6;
	Spheres[11].SViewport[2] = gWidth / 2;
	Spheres[11].SViewport[3] = gHeight / 2;
	Spheres[11].sphereShininess = 0.4f * 128.0f;
	
	// **************** 3rd Column : Plastic **************************
	// BLACK PLASTIC
	Spheres[12].MAmbient = { 0.0f, 0.0f, 0.0f, 1.0f };
	Spheres[12].MDiffuse = { 0.01f, 0.01f, 0.01f, 1.0f };
	Spheres[12].MSpecular = { 0.50f, 0.50f, 0.50f, 1.0f };
	Spheres[12].spherePosition = { 0.0f, 0.0f, -3.0f };
	Spheres[12].SViewport[0] = gWidth / 3;
	Spheres[12].SViewport[1] = 4 * gHeight / 6;
	Spheres[12].SViewport[2] = gWidth / 2;
	Spheres[12].SViewport[3] = gHeight / 2;
	Spheres[12].sphereShininess = 0.25f * 128.0f;
	// CYAN PLASTIC
	Spheres[13].MAmbient = { 0.0f, 0.1f, 0.06f, 1.0f };
	Spheres[13].MDiffuse = { 0.0f, 0.50980392f, 0.50980392f, 1.0f };
	Spheres[13].MSpecular = { 0.50196078f, 0.50196078f, 0.50196078f, 1.0f };
	Spheres[13].spherePosition = { 0.0f, 0.0f, -3.0f };
	Spheres[13].SViewport[0] = gWidth / 3;
	Spheres[13].SViewport[1] = 3 * gHeight / 6;
	Spheres[13].SViewport[2] = gWidth / 2;
	Spheres[13].SViewport[3] = gHeight / 2;
	Spheres[13].sphereShininess = 0.25f * 128.0f;
	// GREEN PLASTIC
	Spheres[14].MAmbient = { 0.0f, 0.0f, 0.0f, 1.0f };
	Spheres[14].MDiffuse = { 0.1f, 0.35f, 0.1f, 1.0f };
	Spheres[14].MSpecular = { 0.45f, 0.55f, 0.45f, 1.0f };
	Spheres[14].spherePosition  = { 0.0f, 0.0f, -3.0f };
	Spheres[14].SViewport[0] = gWidth / 3;
	Spheres[14].SViewport[1] = 2 * gHeight / 6;
	Spheres[14].SViewport[2] = gWidth / 2;
	Spheres[14].SViewport[3] = gHeight / 2;
	Spheres[14].sphereShininess = 0.25f * 128.0f;
	// RED PLASTIC
	Spheres[15].MAmbient = { 0.0f, 0.0f, 0.0f, 1.0f };
	Spheres[15].MDiffuse = { 0.5f, 0.0f, 0.0f, 1.0f };
	Spheres[15].MSpecular = { 0.7f, 0.6f, 0.6f, 1.0f };
	Spheres[15].spherePosition = { 0.0f, 0.0f, -3.0f };
	Spheres[15].SViewport[0] = gWidth / 3;
	Spheres[15].SViewport[1] = gHeight / 6;
	Spheres[15].SViewport[2] = gWidth / 2;
	Spheres[15].SViewport[3] = gHeight / 2;
	Spheres[15].sphereShininess = 0.25f * 128.0f;
	// WHITE PLASTIC
	Spheres[16].MAmbient = { 0.0f, 0.0f, 0.0f, 1.0f };
	Spheres[16].MDiffuse = { 0.55f, 0.55f, 0.55f, 1.0f };
	Spheres[16].MSpecular = { 0.70f, 0.70f, 0.70f, 1.0f };
	Spheres[16].spherePosition = { 0.0f, 0.0f, -3.0f };
	Spheres[16].SViewport[0] = gWidth / 3;
	Spheres[16].SViewport[1] = 0;
	Spheres[16].SViewport[2] = gWidth / 2;
	Spheres[16].SViewport[3] = gHeight / 2;
	Spheres[16].sphereShininess = 0.25f * 128.0f;
	// YELLOW PLASTIC
	Spheres[17].MAmbient = { 0.0f, 0.0f, 0.0f, 1.0f };
	Spheres[17].MDiffuse = { 0.5f, 0.5f, 0.0f, 1.0f };
	Spheres[17].MSpecular = { 0.60f, 0.60f, 0.50f, 1.0f };
	Spheres[17].spherePosition = { 0.0f, 0.0f, -3.0f };
	Spheres[17].SViewport[0] = gWidth / 3;
	Spheres[17].SViewport[1] = -gHeight/6;
	Spheres[17].SViewport[2] = gWidth / 2;
	Spheres[17].SViewport[3] = gHeight / 2;
	Spheres[17].sphereShininess = 0.25f * 128.0f;
	
	// **************** 4th Column : Rubber **************************
	// BLACK RUBBER
	Spheres[18].MAmbient = { 0.02f, 0.02f, 0.02f, 1.0f };
	Spheres[18].MDiffuse = { 0.01f, 0.01f, 0.01f, 1.0f };
	Spheres[18].MSpecular = { 0.4f, 0.40f, 0.40f, 1.0f };
	Spheres[18].spherePosition = { 0.0f, 0.0f, -3.0f };
	Spheres[18].SViewport[0] = gWidth / 2;
	Spheres[18].SViewport[1] = 4 * gHeight / 6;
	Spheres[18].SViewport[2] = gWidth / 2;
	Spheres[18].SViewport[3] = gHeight / 2;
	Spheres[18].sphereShininess = 0.078125f * 128.0f;
	// CYAN RUBBER
	Spheres[19].MAmbient = { 0.0f, 0.05f, 0.05f, 1.0f };
	Spheres[19].MDiffuse = { 0.40f, 0.50f, 0.50f, 1.0f };
	Spheres[19].MSpecular = { 0.04f, 0.7f, 0.7f, 1.0f };
	Spheres[19].spherePosition = { 0.0f, 0.0f, -3.0f };
	Spheres[19].SViewport[0] = gWidth / 2;
	Spheres[19].SViewport[1] = 3 * gHeight / 6;
	Spheres[19].SViewport[2] = gWidth / 2;
	Spheres[19].SViewport[3] = gHeight / 2;
	Spheres[19].sphereShininess = 0.078125f * 128.0f;
	// GREEN RUBBER
	Spheres[20].MAmbient = { 0.0f, 0.05f, 0.0f, 1.0f };
	Spheres[20].MDiffuse = { 0.4f, 0.5f, 0.4f, 1.0f };
	Spheres[20].MSpecular = { 0.04f, 0.7f, 0.04f, 1.0f };
	Spheres[20].spherePosition = { 0.0f, 0.0f, -3.0f };
	Spheres[20].SViewport[0] = gWidth / 2;
	Spheres[20].SViewport[1] = 2 * gHeight / 6;
	Spheres[20].SViewport[2] = gWidth / 2;
	Spheres[20].SViewport[3] = gHeight / 2;
	Spheres[20].sphereShininess = 0.078125f * 128.0f;
	// RED RUBBER
	Spheres[21].MAmbient = { 0.05f, 0.0f, 0.0f, 1.0f };
	Spheres[21].MDiffuse = { 0.5f, 0.4f, 0.4f, 1.0f };
	Spheres[21].MSpecular = { 0.7f, 0.04f, 0.04f, 1.0f };
	Spheres[21].spherePosition = { 0.0f, 0.0f, -3.0f };
	Spheres[21].SViewport[0] = gWidth / 2;
	Spheres[21].SViewport[1] = gHeight / 6;
	Spheres[21].SViewport[2] = gWidth / 2;
	Spheres[21].SViewport[3] = gHeight / 2;
	Spheres[21].sphereShininess = 0.078125f * 128.0f;
	// WHITE RUBBER
	Spheres[22].MAmbient = { 0.05f, 0.05f, 0.05f, 1.0f };
	Spheres[22].MDiffuse = { 0.5f, 0.5f, 0.5f, 1.0f };
	Spheres[22].MSpecular = { 0.70f, 0.70f, 0.70f, 1.0f };
	Spheres[22].spherePosition = { 0.0f, 0.0f, -3.0f };
	Spheres[22].SViewport[0] = gWidth / 2;
	Spheres[22].SViewport[1] = 0;
	Spheres[22].SViewport[2] = gWidth / 2;
	Spheres[22].SViewport[3] = gHeight / 2;
	Spheres[22].sphereShininess = 0.078125f * 128.0f;
	// YELLOW RUBBER
	Spheres[23].MAmbient = { 0.05f, 0.05f, 0.0f, 1.0f };
	Spheres[23].MDiffuse = { 0.50f, 0.50f, 0.40f, 1.0f };
	Spheres[23].MSpecular = { 0.70f, 0.70f, 0.04f, 1.0f };
	Spheres[23].spherePosition = { 0.0f, 0.0f, -3.0f };
	Spheres[23].SViewport[0] = gWidth / 2;
	Spheres[23].SViewport[1] = -gHeight / 6;
	Spheres[23].SViewport[2] = gWidth / 2;
	Spheres[23].SViewport[3] = gHeight / 2;
	Spheres[23].sphereShininess = 0.078125f * 128.0f;
}

//uninitialize() function
void uninitialize(void)
{
	if (vbo_element_sphere)
	{
		glDeleteBuffers(1, &vbo_element_sphere);
		vbo_element_sphere = 0;
	}
	if (vbo_normal_sphere)
	{
		glDeleteBuffers(1, &vbo_normal_sphere);
		vbo_normal_sphere = 0;
	}
	if (vbo_position_sphere)
	{
		glDeleteBuffers(1, &vbo_position_sphere);
		vbo_position_sphere = 0;
	}
	if (vao_sphere)
	{
		glDeleteVertexArrays(1, &vao_sphere);
		vao_sphere = 0;
	}
	if (gShaderProgramObject_v)
	{
		GLsizei shaderCount;
		GLsizei shaderNumber;

		glUseProgram(gShaderProgramObject_v);
		glGetProgramiv(gShaderProgramObject_v, GL_ATTACHED_SHADERS, &shaderCount);
		GLuint *pShaders = (GLuint *)malloc(sizeof(GLuint) * shaderCount);

		if (pShaders)
		{
			glGetAttachedShaders(gShaderProgramObject_v, shaderCount, &shaderCount, pShaders);
			for (shaderNumber = 0; shaderNumber < shaderCount; shaderNumber++)
			{
				//detach all the shader one by one
				glDetachShader(gShaderProgramObject_v, pShaders[shaderNumber]);
				//delete detached shader
				glDeleteShader(pShaders[shaderNumber]);
				pShaders[shaderNumber] = 0;
			}
			free(pShaders);
		}
		glDeleteProgram(gShaderProgramObject_v);
		gShaderProgramObject_v = 0;
		glUseProgram(0);
	}
	if (gShaderProgramObject_f)
	{
		GLsizei shaderCount;
		GLsizei shaderNumber;

		glUseProgram(gShaderProgramObject_f);
		glGetProgramiv(gShaderProgramObject_f, GL_ATTACHED_SHADERS, &shaderCount);
		GLuint *pShaders = (GLuint *)malloc(sizeof(GLuint) * shaderCount);

		if (pShaders)
		{
			glGetAttachedShaders(gShaderProgramObject_f, shaderCount, &shaderCount, pShaders);
			for (shaderNumber = 0; shaderNumber < shaderCount; shaderNumber++)
			{
				//detach all the shader one by one
				glDetachShader(gShaderProgramObject_f, pShaders[shaderNumber]);
				//delete detached shader
				glDeleteShader(pShaders[shaderNumber]);
				pShaders[shaderNumber] = 0;
			}
			free(pShaders);
		}
		glDeleteProgram(gShaderProgramObject_f);
		gShaderProgramObject_f = 0;
		glUseProgram(0);
	}
	if (gbFullScreen == true)
	{
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPED);
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd,
			HWND_TOP,
			0,
			0,
			0,
			0,
			SWP_NOZORDER | SWP_FRAMECHANGED | SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER);
		ShowCursor(TRUE);
	}

	//break the current context
	if (wglGetCurrentContext() == ghrc)
	{
		wglMakeCurrent(NULL, NULL);
	}

	if (ghrc)
	{
		wglDeleteContext(ghrc);
		ghrc = NULL;
	}

	if (ghdc)
	{
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	if (gpfile)
	{
		fprintf(gpfile, "log file closed successfully\n");
		fclose(gpfile);
		gpfile = NULL;
	}
}
