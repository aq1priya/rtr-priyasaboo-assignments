//	Headers
#include<windows.h>
#include<stdio.h>
#include<gl/glew.h>
#include<gl/gl.h>
#include "vmath.h"	

#pragma comment (lib, "glew32.lib")
#pragma comment (lib, "opengl32.lib")

//	Macros
#define WIN_WIDTH 800
#define WIN_HEIGHT 600

//namespaces
using namespace vmath;

enum
{
	AMC_ATTRIBUTE_POSITION = 0,
	AMC_ATTRIBUTE_COLOR,
	AMC_ATTRIBUTE_NORMAL,
	AMC_ATTRIBUTE_TEXCOORD0
};

//	Global function declaration
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

//	Global variables
FILE *gpfile;
HWND ghwnd = NULL;
bool gbActiveWindow = false;
bool gbFullScreen = false;
DWORD dwStyle;
WINDOWPLACEMENT wpPrev = { sizeof(WINDOWPLACEMENT) };
HDC ghdc = NULL;
HGLRC ghrc = NULL;
TCHAR str[256];

GLuint gShaderProgramObject_v;	// shader program object for per vertex lighting
GLuint gShaderProgramObject_f;	// shader program object for per fragment lighting

GLuint vao_pyramid;
GLuint vbo_position_pyramid;
GLuint vbo_normal_pyramid;
GLfloat angle_pyramid = 0.0f;

// uniform variables for per vertex lighting shader object
GLuint modelUniform_v, viewUniform_v, projectionUniform_v;
GLuint blue_ldUniform_v;
GLuint blue_laUniform_v;
GLuint blue_lsUniform_v;
GLuint blue_lightPositionUniform_v;

GLuint red_ldUniform_v;
GLuint red_laUniform_v;
GLuint red_lsUniform_v;
GLuint red_lightPositionUniform_v;

GLuint material_kdUniform_v;
GLuint material_kaUniform_v;
GLuint material_ksUniform_v;
GLuint material_shininessUniform_v;
GLuint keyPressedUniform_v;

// uniform variables for per fragment lighting shader object
GLuint modelUniform_f, viewUniform_f, projectionUniform_f;
GLuint blue_ldUniform_f;
GLuint blue_laUniform_f;
GLuint blue_lsUniform_f;
GLuint blue_lightPositionUniform_f;

GLuint red_ldUniform_f;
GLuint red_laUniform_f;
GLuint red_lsUniform_f;
GLuint red_lightPositionUniform_f;

GLuint material_kdUniform_f;
GLuint material_kaUniform_f;
GLuint material_ksUniform_f;
GLuint material_shininessUniform_f;
GLuint keyPressedUniform_f;

// light and material properties values
float lightDiffuse_blue[4]	= {0.0f, 0.0f, 1.0f, 1.0f};
float lightAmbient_blue[4]	= {0.0f, 0.0f, 0.0f, 0.0f};
float lightSpecular_blue[4]	= {1.0f, 1.0f, 1.0f, 1.0f};
float lightPosition_blue[4] = {-2.0f, 0.0f, 0.0f, 1.0f};

float lightDiffuse_red[4]	= {1.0f, 0.0f, 0.0f, 1.0f};
float lightAmbient_red[4]	= {0.0f, 0.0f, 0.0f, 0.0f};
float lightSpecular_red[4] 	= {1.0f, 1.0f, 1.0f, 1.0f};
float lightPosition_red[4] 	= {2.0f, 0.0f, 0.0f, 1.0f};

float MaterialDiffuse[4] 	= {1.0f, 1.0f, 1.0f, 1.0f};
float MaterialAmbient[4]	= {0.0f, 0.0f, 0.0f, 0.0f};
float MaterialSpecular[4]	= {1.0f, 1.0f, 1.0f, 1.0f};
float materialShininess = 128.0f;

bool enableLighting = true;		// key to enable and disable lighting
bool toggleLightingType = false;	// key to toggle between per vertex lighting and per fragment lighting

int key1 = 1;
int key2 = 0;
mat4 perspectiveProjectionMatrix;	// perspective projection matrix

//WinMain()
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{
	//function declarations
	int initialize(void);
	void display(void);
	void update(void);

	//variable declarations
	WNDCLASSEX wndclass;
	HWND hwnd;
	MSG msg;
	TCHAR szAppName[] = TEXT("3d shapes");
	bool bDone = false;
	int iRet = 0;

	//code
	if (fopen_s(&gpfile, "log.txt", "w") != 0)
	{
		MessageBox(NULL, TEXT("Log file cannot be created"), TEXT("Error"), MB_OK);
		exit(0);
	}

	//initialization of wndclass structure
	wndclass.cbSize = sizeof(WNDCLASSEX);
	wndclass.cbWndExtra = 0;
	wndclass.cbClsExtra = 0;
	wndclass.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
	wndclass.lpfnWndProc = WndProc;
	wndclass.hInstance = hInstance;
	wndclass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	wndclass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndclass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndclass.lpszClassName = szAppName;
	wndclass.lpszMenuName = NULL;
	wndclass.hIconSm = LoadIcon(hInstance, IDI_APPLICATION);

	//register above class 
	RegisterClassEx(&wndclass);

	//create window
	hwnd = CreateWindowEx(WS_EX_APPWINDOW,
		szAppName,
		TEXT("3d shape rotation - PriyaSaboo"),
		WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN | WS_CLIPSIBLINGS | WS_VISIBLE,
		100,
		100,
		WIN_WIDTH,
		WIN_HEIGHT,
		NULL,
		NULL,
		hInstance,
		NULL);

	ghwnd = hwnd;
	iRet = initialize();
	if (iRet == -1)
	{
		fprintf(gpfile, "ChoosePixelFormat() Failed\n");
		DestroyWindow(hwnd);
	}
	else if (iRet == -2)
	{
		fprintf(gpfile, "SetPixelFormat() Failed\n");
		DestroyWindow(hwnd);
	}
	else if (iRet == -3)
	{
		fprintf(gpfile, "wglCreateContext() Failed\n");
		DestroyWindow(hwnd);
	}
	else if (iRet == -4)
	{
		fprintf(gpfile, "wglMakeCurrent() Failed\n");
		DestroyWindow(hwnd);
	}
	else
		fprintf(gpfile, "Initialize() Succeded\n");

	//show window
	ShowWindow(hwnd, iCmdShow);
	SetForegroundWindow(hwnd);
	SetFocus(hwnd);

	//gameloop
	while (bDone == false)
	{
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			if (msg.message == WM_QUIT)
			{
				bDone = true;
			}
			else
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}
		else
		{
			if (gbActiveWindow == true)
			{
				//update should be called here
				update();
			}
			display();
		}
	}
	return((int)msg.wParam);
}

//callback function WndProc()
LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	//function declaration
	void resize(int, int);
	void ToggleFullScreen(void);
	void uninitialize(void);

	//variables
	static GLfloat width, height;

	//code
	switch (iMsg)
	{
	case WM_SETFOCUS:
		gbActiveWindow = true;
		break;

	case WM_KILLFOCUS:
		gbActiveWindow = false;
		break;

	case WM_ERASEBKGND:
		return(0);

	case WM_SIZE:
		width = LOWORD(lParam);
		height = HIWORD(lParam);
		resize(LOWORD(lParam), HIWORD(lParam));
		break;

	case WM_CLOSE:
		DestroyWindow(hwnd);
		break;

	case WM_KEYDOWN:
		switch (LOWORD(wParam))
		{
		case VK_ESCAPE:
			ToggleFullScreen();
			break;
		}
		break;
	case WM_CHAR:
	switch(LOWORD(wParam))
	{
		case 'e':
		case 'E':
			DestroyWindow(hwnd);
			break;
		case 't':
		case 'T':
			if (toggleLightingType == true)
			{
				toggleLightingType = false;
				enableLighting = false;
			}
			else
			{
				toggleLightingType = true;
				enableLighting = false;
			}
			fprintf(gpfile, "t key is pressed\n");
			break;
		case 'l':
		case 'L':
			if(enableLighting == true)
				enableLighting = false;
			else
				enableLighting = true;
			fprintf(gpfile, "l key is pressed\n");
			break;
	}
	break;

	case WM_DESTROY:
		uninitialize();	
		PostQuitMessage(0);
		break;
	}
	return(DefWindowProc(hwnd, iMsg, wParam, lParam));
}

//ToggleFullScreen() function
void ToggleFullScreen(void)
{
	MONITORINFO mi;
	if (gbFullScreen == false)
	{
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);
		if (dwStyle & WS_OVERLAPPEDWINDOW)
		{
			mi = { sizeof(MONITORINFO) };
			if (GetWindowPlacement(ghwnd, &wpPrev) && GetMonitorInfo(MonitorFromWindow(ghwnd, MONITORINFOF_PRIMARY), &mi))
			{
				SetWindowLong(ghwnd, GWL_STYLE, dwStyle & ~WS_OVERLAPPEDWINDOW);
				SetWindowPos(ghwnd,
					HWND_TOP,
					mi.rcMonitor.left,
					mi.rcMonitor.top,
					mi.rcMonitor.right - mi.rcMonitor.left,
					mi.rcMonitor.bottom - mi.rcMonitor.top,
					SWP_NOZORDER | SWP_FRAMECHANGED);
			}
		}
		ShowCursor(FALSE);
		gbFullScreen = true;
	}
	else
	{
		fprintf(gpfile, "Full Screen to normal\n");
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPED);
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd,
			HWND_TOP,
			0,
			0,
			0,
			0,
			SWP_NOZORDER | SWP_FRAMECHANGED | SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER);
		ShowCursor(TRUE);
		gbFullScreen = false;
	}
}

//initialize() function
int initialize(void)
{
	//function declarations
	void resize(int, int);
	void uninitialize(void);

	//vaiable declaraions
	PIXELFORMATDESCRIPTOR pfd;

	int iPixelFormatIndex;

	GLenum result;

	GLint iShaderCompileStatus = 0;
	GLint iInfoLogLength = 0;
	GLint iProgramLinkStatus = 0;

	GLchar *szInfoLog = NULL;

	// shader objects for per vertex lighting
	GLuint VertexShaderObject_v;
	GLuint FragmentShaderObject_v;

	GLuint VertexShaderObject_f;
	GLuint FragmentShaderObject_f;

	//code
	//initialize pfd structure
	ZeroMemory(&pfd, sizeof(PIXELFORMATDESCRIPTOR));
	pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
	pfd.nVersion = 1;
	pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
	pfd.iPixelType = PFD_TYPE_RGBA;
	pfd.cColorBits = 32;
	pfd.cDepthBits = 32;
	pfd.cRedBits = 8;
	pfd.cGreenBits = 8;
	pfd.cBlueBits = 8;
	pfd.cAlphaBits = 8;

	ghdc = GetDC(ghwnd);
	iPixelFormatIndex = ChoosePixelFormat(ghdc, &pfd);
	if (iPixelFormatIndex == 0)
	{
		return(-1);
	}
	if (SetPixelFormat(ghdc, iPixelFormatIndex, &pfd) == false)
	{
		return(-2);
	}
	ghrc = wglCreateContext(ghdc);
	if (ghrc == NULL)
	{
		return(-3);
	}
	if (wglMakeCurrent(ghdc, ghrc) == false)
	{
		return(-4);
	}

	result = glewInit();
	if (result != GLEW_OK)
	{
		fprintf(gpfile, "glewInit() failed\n");
		uninitialize();
		DestroyWindow(ghwnd);
	}

	// **********************************************************************************
	//-----------------------------Per Vertex lighting Shaders---------------------------
	// **********************************************************************************
	//define vertex shader object
	VertexShaderObject_v = glCreateShader(GL_VERTEX_SHADER);
	
	//write vertex shader code
	const GLchar *VertexShaderSourceCode_v =
		"	#version 430 core																			" \
		"																							\n	" \
		"	in vec4 vPosition;																			" \
		"	in vec3 vNormal;																			" \
		"																							\n	" \
		"	out vec3 phong_ads_light;																	" \
		"																							\n	" \
		"	uniform mat4 u_m_matrix;																	" \
		"	uniform mat4 u_v_matrix;																	" \
		"	uniform mat4 u_p_matrix;																	" \
		"	uniform int u_l_key_is_pressed;																" \
		"	uniform vec3 u_blue_ld;																		" \
		"	uniform vec3 u_blue_la;																		" \
		"	uniform vec3 u_blue_ls;																		" \
		"	uniform vec3 u_red_ld;																		" \
		"	uniform vec3 u_red_la;																		" \
		"	uniform vec3 u_red_ls;																		" \
		"	uniform vec3 u_ka;																			" \
		"	uniform vec3 u_kd;																			" \
		"	uniform vec3 u_ks;																			" \
		"	uniform vec4 u_blue_light_position;															" \
		"	uniform vec4 u_red_light_position;															" \
		"	uniform float u_material_shininess;															" \
		"																							\n	" \
		"	void main(void)																				" \
		"	{																							" \
		"		if(u_l_key_is_pressed == 1)																" \
		"		{																						" \
		"			vec4 eyeCoordinates = u_v_matrix * u_m_matrix * vPosition;							" \
		"			vec3 tNorm			= normalize(mat3(u_v_matrix * u_m_matrix) * vNormal);			" \
		"			vec3 viewerVector   = normalize(vec3(-eyeCoordinates));								" \
		"																							\n	" \
		"			vec3 blueLightDirection = normalize(vec3(u_blue_light_position - eyeCoordinates));	" \
		"			vec3 blueLightReflectionVector = reflect(-blueLightDirection, tNorm);	" \
		"			float bluetnDotld = max(dot(tNorm, blueLightDirection), 0.0);						" \
		"			float bluervDotvv = max(dot(viewerVector, blueLightReflectionVector), 0.0);			" \
		"																							\n	" \
		"			vec3 redLightDirection  = normalize(vec3(u_red_light_position - eyeCoordinates));	" \
		"			vec3 redLightReflectionVector = reflect(-redLightDirection, tNorm);					" \
		"			float redtnDotld = max(dot(tNorm, redLightDirection), 0.0);							" \
		"			float redrvDotvv = max(dot(viewerVector, redLightReflectionVector), 0.0);			" \
		"																							\n	" \
		"			vec3 blueAmbient  = u_blue_la * u_ka;												" \
		"			vec3 blueDiffuse  = u_blue_ld * u_kd * bluetnDotld;									" \
		"			vec3 blueSpecular = u_blue_ls * u_ks * pow(bluervDotvv, u_material_shininess);		" \
		"																							\n	" \
		"			vec3 redAmbient  = u_red_la * u_ka;													" \
		"			vec3 redDiffuse  = u_red_ld * u_kd * redtnDotld;									" \
		"			vec3 redSpecular = u_red_ls * u_ks * pow(redrvDotvv, u_material_shininess);			" \
		"																							\n	" \
		"			vec3 Ambient  = blueAmbient + redAmbient;											" \
		"			vec3 Diffuse  = blueDiffuse + redDiffuse;											" \
		"			vec3 Specular = blueSpecular + redSpecular;											" \
		"			phong_ads_light = Ambient + Diffuse + Specular;										" \
		"		}																						" \
		"		else																					" \
		"		{																						" \
		"			phong_ads_light = vec3(1.0, 1.0, 1.0);												" \
		"		}																						" \
		"		gl_Position = u_v_matrix * u_p_matrix * u_m_matrix * vPosition;							" \
		"	}																							";

	//specify above source code to the vertex shader object
	glShaderSource(VertexShaderObject_v, 1, (const GLchar **)&VertexShaderSourceCode_v, NULL);
	
	//compile the vertex shader
	glCompileShader(VertexShaderObject_v);
	glGetShaderiv(VertexShaderObject_v, GL_COMPILE_STATUS, &iShaderCompileStatus);
	if (iShaderCompileStatus == GL_FALSE)
	{
		glGetShaderiv(VertexShaderObject_v, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (GLchar *)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(VertexShaderObject_v, iInfoLogLength, &written, szInfoLog);
				fprintf(gpfile, "VERTEX SHADER COMPILATION LOG 1 : %s\n", szInfoLog);
				free(szInfoLog);
				uninitialize();
				DestroyWindow(ghwnd);
				exit(0);
			}
		}
	}

	//define fragment shader object
	FragmentShaderObject_v = glCreateShader(GL_FRAGMENT_SHADER);

	//write fragment shader code
	const GLchar *FragmentShaderSourceCode_v =
		"	#version 430 core																			" \
		"																							\n	" \
		"	in vec3 phong_ads_light;																	" \
		"																							\n	" \
		"	out vec4 FragColor;																			" \
		"																							\n	" \
		"	void main(void)																				" \
		"	{																							" \
		"		FragColor = vec4(phong_ads_light,1.0);													" \
		"	}																							";

	//specify above source code to fragmnet shader object
	glShaderSource(FragmentShaderObject_v, 1, (const GLchar **)&FragmentShaderSourceCode_v, NULL);
	
	//compile fragment shader
	iShaderCompileStatus = 0;
	iInfoLogLength = 0;
	szInfoLog = NULL;
	
	//error checking
	glCompileShader(FragmentShaderObject_v);
	glGetShaderiv(FragmentShaderObject_v, GL_COMPILE_STATUS, &iShaderCompileStatus);
	if (iShaderCompileStatus == GL_FALSE)
	{
		glGetShaderiv(FragmentShaderObject_v, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (GLchar *)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(FragmentShaderObject_v, iInfoLogLength, &written, szInfoLog);
				fprintf(gpfile, "FRAGMENT SHADER COMPILATION LOG 1 : %s\n", szInfoLog);
				free(szInfoLog);
				uninitialize();
				DestroyWindow(ghwnd);
				exit(0);
			}
		}
	}

	//create shader program object
	gShaderProgramObject_v = glCreateProgram();
	
	//attach vertex shader to the shader program 
	glAttachShader(gShaderProgramObject_v, VertexShaderObject_v);
	//attach fragment shader 
	glAttachShader(gShaderProgramObject_v, FragmentShaderObject_v);
	
	//prelinking binding to vertex attribute
	glBindAttribLocation(gShaderProgramObject_v, AMC_ATTRIBUTE_POSITION, "vPosition");
	glBindAttribLocation(gShaderProgramObject_v, AMC_ATTRIBUTE_NORMAL, "vNormal");

	//link shader program
	glLinkProgram(gShaderProgramObject_v);

	//error checking
	iProgramLinkStatus = 0;
	iInfoLogLength = 0;
	szInfoLog = NULL;
	
	glGetProgramiv(gShaderProgramObject_v, GL_LINK_STATUS, &iProgramLinkStatus);
	if (iProgramLinkStatus == GL_FALSE)
	{
		glGetProgramiv(gShaderProgramObject_v, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (GLchar *)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLint written;
				glGetProgramInfoLog(gShaderProgramObject_v, iInfoLogLength, &written, szInfoLog);
				fprintf(gpfile, "SHADER PROGRAM LINK LOG 1 : %s\n", szInfoLog);
				free(szInfoLog);
				uninitialize();
				DestroyWindow(ghwnd);
				exit(0);
			}
		}
	}

	//postlinking retieving uniform location
	// model, view and projection matrices
	viewUniform_v = glGetUniformLocation(gShaderProgramObject_v, "u_v_matrix");
	modelUniform_v = glGetUniformLocation(gShaderProgramObject_v, "u_m_matrix");
	projectionUniform_v = glGetUniformLocation(gShaderProgramObject_v, "u_p_matrix");
	// blue light uniforms
	blue_ldUniform_v = glGetUniformLocation(gShaderProgramObject_v, "u_blue_ld");
	blue_laUniform_v = glGetUniformLocation(gShaderProgramObject_v, "u_blue_la");
	blue_lsUniform_v = glGetUniformLocation(gShaderProgramObject_v, "u_blue_ls");
	blue_lightPositionUniform_v = glGetUniformLocation(gShaderProgramObject_v, "u_blue_light_position");
	// red light uniforms
	red_ldUniform_v = glGetUniformLocation(gShaderProgramObject_v, "u_red_ld");
	red_laUniform_v = glGetUniformLocation(gShaderProgramObject_v, "u_red_la");
	red_lsUniform_v = glGetUniformLocation(gShaderProgramObject_v, "u_red_ls");
	red_lightPositionUniform_v = glGetUniformLocation(gShaderProgramObject_v, "u_red_light_position");
	// material uniform
	material_kdUniform_v = glGetUniformLocation(gShaderProgramObject_v, "u_kd");
	material_kaUniform_v = glGetUniformLocation(gShaderProgramObject_v, "u_ka");
	material_ksUniform_v = glGetUniformLocation(gShaderProgramObject_v, "u_ks");
	material_shininessUniform_v = glGetUniformLocation(gShaderProgramObject_v, "u_material_shininess");
	// key pressed uniform
	keyPressedUniform_v = glGetUniformLocation(gShaderProgramObject_v, "u_l_key_is_pressed");

	//**********************************************************************************************************
	//-----------------------------------Shaders For Per Fragment Lighting--------------------------------------
	//**********************************************************************************************************
	//define vertex shader object
	VertexShaderObject_f = glCreateShader(GL_VERTEX_SHADER);
	
	//write vertex shader code
	const GLchar *VertexShaderSourceCode_f =
		"	#version 430 core																			" \
		"																							\n	" \
		"	in vec4 vPosition;																			" \
		"	in vec3 vNormal;																			" \
		"																							\n	" \
		"	out vec4 eyeCoordinates;																		" \
		"	out vec3 tNorm;																				" \
		"	out vec3 viewerVector;																		" \
		"	out vec3 blueLightDirection;																" \
		"	out vec3 redLightDirection;																	" \
		"																							\n	" \
		"	uniform mat4 u_m_matrix;																	" \
		"	uniform mat4 u_v_matrix;																	" \
		"	uniform mat4 u_p_matrix;																	" \
		"	uniform int u_l_key_is_pressed;																" \
		"	uniform vec4 u_blue_light_position;															" \
		"	uniform vec4 u_red_light_position;															" \
		"																							\n	" \
		"	void main(void)																				" \
		"	{																							" \
		"		if(u_l_key_is_pressed == 1)																" \
		"		{																						" \
		"			eyeCoordinates = u_v_matrix * u_m_matrix * vPosition;								" \
		"			tNorm			= mat3(u_v_matrix * u_m_matrix) * vNormal;							" \
		"			viewerVector   = vec3(-eyeCoordinates);												" \
		"																							\n	" \
		"			blueLightDirection = vec3(u_blue_light_position - eyeCoordinates);					" \
		"			redLightDirection  = vec3(u_red_light_position - eyeCoordinates);					" \
		"		}																						" \
		"		gl_Position = u_v_matrix * u_p_matrix * u_m_matrix * vPosition;							" \
		"	}																							";

	//specify above source code to the vertex shader object
	glShaderSource(VertexShaderObject_f, 1, (const GLchar **)&VertexShaderSourceCode_f, NULL);
	
	//compile the vertex shader
	glCompileShader(VertexShaderObject_f);
	glGetShaderiv(VertexShaderObject_f, GL_COMPILE_STATUS, &iShaderCompileStatus);
	if (iShaderCompileStatus == GL_FALSE)
	{
		glGetShaderiv(VertexShaderObject_f, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (GLchar *)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(VertexShaderObject_f, iInfoLogLength, &written, szInfoLog);
				fprintf(gpfile, "VERTEX SHADER COMPILATION LOG 2 : %s\n", szInfoLog);
				free(szInfoLog);
				uninitialize();
				DestroyWindow(ghwnd);
				exit(0);
			}
		}
	}

	//define fragment shader object
	FragmentShaderObject_f = glCreateShader(GL_FRAGMENT_SHADER);

	//write fragment shader code
	const GLchar *FragmentShaderSourceCode_f =
		"	#version 430 core																					" \
		"																									\n	" \
		"	in vec3 tNorm;																						" \
		"	in vec3 viewerVector;																				" \
		"	in vec3 blueLightDirection;																			" \
		"	in vec3 redLightDirection;																			" \
		"																									\n	" \
		"	uniform vec3 u_blue_ld;																				" \
		"	uniform vec3 u_blue_la;																				" \
		"	uniform vec3 u_blue_ls;																				" \
		"	uniform vec3 u_red_ld;																				" \
		"	uniform vec3 u_red_la;																				" \
		"	uniform vec3 u_red_ls;																				" \
		"	uniform vec3 u_ka;																					" \
		"	uniform vec3 u_kd;																					" \
		"	uniform vec3 u_ks;																					" \
		"	uniform float u_material_shininess;																	" \
		"	uniform int u_l_key_is_pressed;																		" \
		"																									\n	" \
		"	out vec4 FragColor;																					" \
		"																									\n	" \
		"	void main(void)																						" \
		"	{																									" \
		"		vec3 phong_ads_light;																			" \
		"		if(u_l_key_is_pressed == 1)																		" \
		"		{																								" \
		"			vec3 normalizedtNorm = normalize(tNorm);													" \
		"			vec3 normalizedBlueLightDirection = normalize(blueLightDirection);							" \
		"			vec3 normalizedRedLightDirection  = normalize(redLightDirection);							" \
		"			vec3 normalizedViewerVector		  = normalize(viewerVector);								" \
		"																									\n	" \
		"			vec3 blueLightReflectionVector = normalize(reflect(-normalizedBlueLightDirection, normalizedtNorm));	" \
		"			vec3 redLightReflectionVector  = normalize(reflect(-normalizedRedLightDirection, normalizedtNorm));	" \
		"																									\n	" \
		"			float bluetnDotld = max(dot(normalizedtNorm, normalizedBlueLightDirection), 0.0);						" \
		"			float bluervDotvv = max(dot(normalizedViewerVector, blueLightReflectionVector), 0.0);		" \
		"			float redtnDotld = max(dot(normalizedtNorm, normalizedRedLightDirection), 0.0);						" \
		"			float redrvDotvv = max(dot(normalizedViewerVector, redLightReflectionVector), 0.0);			" \
		"																									\n	" \
		"			vec3 blueAmbient  = u_blue_la * u_ka;														" \
		"			vec3 blueDiffuse  = u_blue_ld * u_kd * bluetnDotld;											" \
		"			vec3 blueSpecular = u_blue_ls * u_ks * pow(bluervDotvv, u_material_shininess);				" \
		"																									\n	" \
		"			vec3 redAmbient  = u_red_la * u_ka;															" \
		"			vec3 redDiffuse  = u_red_ld * u_kd * redtnDotld;											" \
		"			vec3 redSpecular = u_red_ls * u_ks * pow(redrvDotvv, u_material_shininess);					" \
		"																									\n	" \
		"			vec3 Ambient  = blueAmbient + redAmbient;													" \
		"			vec3 Diffuse  = blueDiffuse + redDiffuse;													" \
		"			vec3 Specular = blueSpecular + redSpecular;													" \
		"			phong_ads_light = Ambient + Diffuse + Specular;												" \
		"			FragColor = vec4(phong_ads_light,1.0);															" \
		"		}																								" \
		"		else																							" \
		"		{																								" \
		"			phong_ads_light = vec3(1.0, 1.0, 1.0);														" \
		"			FragColor = vec4(phong_ads_light,1.0);															" \
		"		}																								" \
		"																									\n	" \
		"	}																									";

	//specify above source code to fragmnet shader object
	glShaderSource(FragmentShaderObject_f, 1, (const GLchar **)&FragmentShaderSourceCode_f, NULL);
	
	//compile fragment shader
	iShaderCompileStatus = 0;
	iInfoLogLength = 0;
	szInfoLog = NULL;
	
	//error checking
	glCompileShader(FragmentShaderObject_f);
	glGetShaderiv(FragmentShaderObject_f, GL_COMPILE_STATUS, &iShaderCompileStatus);
	if (iShaderCompileStatus == GL_FALSE)
	{
		glGetShaderiv(FragmentShaderObject_f, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (GLchar *)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(FragmentShaderObject_f, iInfoLogLength, &written, szInfoLog);
				fprintf(gpfile, "FRAGMENT SHADER COMPILATION LOG 2 : %s\n", szInfoLog);
				free(szInfoLog);
				uninitialize();
				DestroyWindow(ghwnd);
				exit(0);
			}
		}
	}

	//create shader program object
	gShaderProgramObject_f = glCreateProgram();
	
	//attach vertex shader to the shader program 
	glAttachShader(gShaderProgramObject_f, VertexShaderObject_f);
	//attach fragment shader 
	glAttachShader(gShaderProgramObject_f, FragmentShaderObject_f);
	
	//prelinking binding to vertex attribute
	glBindAttribLocation(gShaderProgramObject_f, AMC_ATTRIBUTE_POSITION, "vPosition");
	glBindAttribLocation(gShaderProgramObject_f, AMC_ATTRIBUTE_NORMAL, "vNormal");

	//link shader program
	glLinkProgram(gShaderProgramObject_f);

	//error checking
	iProgramLinkStatus = 0;
	iInfoLogLength = 0;
	szInfoLog = NULL;
	
	glGetProgramiv(gShaderProgramObject_f, GL_LINK_STATUS, &iProgramLinkStatus);
	if (iProgramLinkStatus == GL_FALSE)
	{
		glGetProgramiv(gShaderProgramObject_f, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (GLchar *)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLint written;
				glGetProgramInfoLog(gShaderProgramObject_f, iInfoLogLength, &written, szInfoLog);
				fprintf(gpfile, "SHADER PROGRAM LINK LOG 2 : %s\n", szInfoLog);
				free(szInfoLog);
				uninitialize();
				DestroyWindow(ghwnd);
				exit(0);
			}
		}
	}

	//postlinking retieving uniform location
	// model, view and projection matrices
	viewUniform_f  		= glGetUniformLocation(gShaderProgramObject_f, "u_v_matrix");
	modelUniform_f 		= glGetUniformLocation(gShaderProgramObject_f, "u_m_matrix");
	projectionUniform_f = glGetUniformLocation(gShaderProgramObject_f, "u_p_matrix");
	// blue light uniforms
	blue_ldUniform_f = glGetUniformLocation(gShaderProgramObject_f, "u_blue_ld");
	blue_laUniform_f = glGetUniformLocation(gShaderProgramObject_f, "u_blue_la");
	blue_lsUniform_f = glGetUniformLocation(gShaderProgramObject_f, "u_blue_ls");
	blue_lightPositionUniform_f = glGetUniformLocation(gShaderProgramObject_f, "u_blue_light_position");
	// red light uniforms
	red_ldUniform_f = glGetUniformLocation(gShaderProgramObject_f, "u_red_ld");
	red_laUniform_f = glGetUniformLocation(gShaderProgramObject_f, "u_red_la");
	red_lsUniform_f = glGetUniformLocation(gShaderProgramObject_f, "u_red_ls");
	red_lightPositionUniform_f = glGetUniformLocation(gShaderProgramObject_f, "u_red_light_position");
	// material uniform
	material_kdUniform_f = glGetUniformLocation(gShaderProgramObject_f, "u_kd");
	material_kaUniform_f = glGetUniformLocation(gShaderProgramObject_f, "u_ka");
	material_ksUniform_f = glGetUniformLocation(gShaderProgramObject_f, "u_ks");
	material_shininessUniform_f = glGetUniformLocation(gShaderProgramObject_f, "u_material_shininess");
	// key pressed uniform
	keyPressedUniform_f = glGetUniformLocation(gShaderProgramObject_f, "u_l_key_is_pressed");

	// Pyramid vertics and normals
	const GLfloat pyramidVertices[] = {
		0.0f, 1.0f, 0.0f, -1.0f, -1.0f, 1.0f, 1.0f, -1.0f, 1.0f,
		0.0f, 1.0f, 0.0f, 1.0f, -1.0f, 1.0f, 1.0f, -1.0f, -1.0f,
		0.0f, 1.0f, 0.0f, 1.0f, -1.0f, -1.0f, -1.0f, -1.0f, -1.0f,
		0.0f, 1.0f, 0.0f, -1.0f, -1.0f, -1.0f, -1.0f, -1.0f, 1.0f
	};//front-right-back-left

	const GLfloat pyramidNormal[] = {
		0.0f, 0.447214f, 0.894427f, 0.0f, 0.447214f, 0.894427f, 0.0f, 0.447214f, 0.894427f,
		0.894427f, 0.447214f, 0.0f, 0.894427f, 0.447214f, 0.0f, 0.894427f, 0.447214f, 0.0f,
		0.0f, 0.447214f, -0.894427f, 0.0f, 0.447214f, -0.894427f, 0.0f, 0.447214f, -0.894427f,
		-0.894427f, 0.447214f, 0.0f, -0.894427f, 0.447214f, 0.0f, -0.894427f, 0.447214f, 0.0f
	};

	//TRIANGLE
	glGenVertexArrays(1, &vao_pyramid);
	glBindVertexArray(vao_pyramid);
	//buffer for position
	glGenBuffers(1, &vbo_position_pyramid);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_position_pyramid);
	glBufferData(GL_ARRAY_BUFFER, sizeof(pyramidVertices), pyramidVertices, GL_STATIC_DRAW);
	glVertexAttribPointer(AMC_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	// buffer for normals
	glGenBuffers(1, &vbo_normal_pyramid);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_normal_pyramid);
	glBufferData(GL_ARRAY_BUFFER, sizeof(pyramidNormal), pyramidNormal, GL_STATIC_DRAW);
	glVertexAttribPointer(AMC_ATTRIBUTE_NORMAL, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AMC_ATTRIBUTE_NORMAL);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindVertexArray(0); // unbinding with trianle vao

	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);	//r,g,b,a	//clear the screen by opengl color
	
	glClearDepth(1.0f);						//giving existance to depth buffer
	glEnable(GL_DEPTH_TEST);				// enableing depth test
	glDepthFunc(GL_LEQUAL);
	//glEnable(GL_CULL_FACE);
	
	perspectiveProjectionMatrix = mat4::identity();

	//wramup call to resize
	resize(WIN_WIDTH, WIN_HEIGHT);

	return(0);
}

//update function
void update(void)
{
	if (angle_pyramid < 360.0f)
		angle_pyramid = angle_pyramid + 0.05f;
	else
		angle_pyramid = 0.0f;
}

//display function
void display(void)
{
	//declaration of matrices
	mat4 modelMatrix;
	mat4 viewMatrix;
	mat4 projectionMatrix;
	mat4 translationMatrix;
	mat4 rotationMatrix;
	mat4 scaleMatrix;

	//initialize above matrices to identity
	modelMatrix = mat4::identity();
	viewMatrix = mat4::identity();
	projectionMatrix = mat4::identity();
	translationMatrix = mat4::identity();
	rotationMatrix = mat4::identity();
	scaleMatrix = mat4::identity();

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	//do necessary matrix multiplication
	translationMatrix = translate(0.0f, 0.0f, -6.0f);
	rotationMatrix = rotate(angle_pyramid, 0.0f, 1.0f, 0.0f);
	modelMatrix = translationMatrix * rotationMatrix;
	projectionMatrix = projectionMatrix * perspectiveProjectionMatrix;
	
	if(toggleLightingType == false)
	{

		glUseProgram(gShaderProgramObject_v);
		if(enableLighting == true)
		{
			wsprintf(str, L"Two Steady Lights On Rotating Pyramid :: ADS Per Vertex lighting - Lighting enabled");
			SetWindowText(ghwnd, str);

			//send necessary matrices to shader in respective uniforms
			glUniform1i(keyPressedUniform_v, 1);
			glUniform3fv(blue_ldUniform_v, 1, lightDiffuse_blue);
			glUniform3fv(blue_laUniform_v, 1, lightAmbient_blue);
			glUniform3fv(blue_lsUniform_v, 1, lightSpecular_blue);

			glUniform3fv(red_ldUniform_v, 1, lightDiffuse_red);
			glUniform3fv(red_laUniform_v, 1, lightAmbient_red);
			glUniform3fv(red_lsUniform_v, 1, lightSpecular_red);

			glUniform3fv(material_kdUniform_v, 1, MaterialDiffuse);
			glUniform3fv(material_kaUniform_v, 1, MaterialAmbient);
			glUniform3fv(material_ksUniform_v, 1, MaterialSpecular);

			glUniform1f(material_shininessUniform_v, materialShininess);

			glUniform4fv(blue_lightPositionUniform_v, 1, lightPosition_blue);
			glUniform4fv(red_lightPositionUniform_v, 1, lightPosition_red);
		}
		else
		{
			wsprintf(str, L"Two Steady Lights On Rotating Pyramid :: ADS Per Vertex lighting - Lighting disabled");
			SetWindowText(ghwnd, str);

			glUniform1i(keyPressedUniform_v, 0);
		}
		glUniformMatrix4fv(modelUniform_v, 1, GL_FALSE, modelMatrix);
		glUniformMatrix4fv(viewUniform_v, 1, GL_FALSE, viewMatrix);
		glUniformMatrix4fv(projectionUniform_v, 1, GL_FALSE, projectionMatrix);
		
		//bind to vao_pyrmid
		glBindVertexArray(vao_pyramid);
		glDrawArrays(GL_TRIANGLES, 0, 12);
		//unbind vao
		glBindVertexArray(0);

		//unuse program
		glUseProgram(0);
	}
	else
	{
		glUseProgram(gShaderProgramObject_f);
		if(enableLighting == true)
		{
			wsprintf(str, L" Two Steady Lights On Rotating Pyramid :: ADS Per Fragment lighting - Lighting enabled ");
			SetWindowText(ghwnd, str);

			//send necessary matrices to shader in respective uniforms
			glUniform1i(keyPressedUniform_f, 1);
			glUniform3fv(blue_ldUniform_f, 1, lightDiffuse_blue);
			glUniform3fv(blue_laUniform_f, 1, lightAmbient_blue);
			glUniform3fv(blue_lsUniform_f, 1, lightSpecular_blue);

			glUniform3fv(red_ldUniform_f, 1, lightDiffuse_red);
			glUniform3fv(red_laUniform_f, 1, lightAmbient_red);
			glUniform3fv(red_lsUniform_f, 1, lightSpecular_red);

			glUniform3fv(material_kdUniform_f, 1, MaterialDiffuse);
			glUniform3fv(material_kaUniform_f, 1, MaterialAmbient);
			glUniform3fv(material_ksUniform_f, 1, MaterialSpecular);

			glUniform1f(material_shininessUniform_f, materialShininess);

			glUniform4fv(blue_lightPositionUniform_f, 1, lightPosition_blue);
			glUniform4fv(red_lightPositionUniform_f, 1, lightPosition_red);
		}
		else
		{
			wsprintf(str, L"Two Steady Lights On Rotating Pyramid :: ADS Per Fragment lighting - Lighting disabled");
			SetWindowText(ghwnd, str);

			glUniform1i(keyPressedUniform_f, 0);
		}
		glUniformMatrix4fv(modelUniform_f, 1, GL_FALSE, modelMatrix);
		glUniformMatrix4fv(viewUniform_f, 1, GL_FALSE, viewMatrix);
		glUniformMatrix4fv(projectionUniform_f, 1, GL_FALSE, projectionMatrix);
		
		//bind to vao_pyrmid
		glBindVertexArray(vao_pyramid);
		glDrawArrays(GL_TRIANGLES, 0, 12);
		//unbind vao
		glBindVertexArray(0);

		//unuse program
		glUseProgram(0);
	}
	
	SwapBuffers(ghdc);
}

//reize() function
void resize(int win_width, int win_height)
{
	if (win_height == 0)
		win_height = 1;
	glViewport(0, 0, (GLsizei)win_width, (GLsizei)win_height);
	perspectiveProjectionMatrix = perspective(45.0f, (GLfloat)win_width / (GLfloat)win_height, 0.1f, 100.0f);
}

//uninitialize() function
void uninitialize(void)
{
	if (vbo_normal_pyramid)
	{
		glDeleteBuffers(1, &vbo_normal_pyramid);
		vbo_normal_pyramid = 0;
	}
	if (vbo_position_pyramid)
	{
		glDeleteBuffers(1, &vbo_position_pyramid);
		vbo_position_pyramid = 0;
	}
	if (vao_pyramid)
	{
		glDeleteVertexArrays(1, &vao_pyramid);
		vao_pyramid = 0;
	}
	if (gShaderProgramObject_v)
	{
		GLsizei shaderCount;
		GLsizei shaderNumber;

		glUseProgram(gShaderProgramObject_v);
		glGetProgramiv(gShaderProgramObject_v, GL_ATTACHED_SHADERS, &shaderCount);
		GLuint *pShaders = (GLuint *)malloc(sizeof(GLuint) * shaderCount);

		if (pShaders)
		{
			glGetAttachedShaders(gShaderProgramObject_v, shaderCount, &shaderCount, pShaders);
			for (shaderNumber = 0; shaderNumber < shaderCount; shaderNumber++)
			{
				//detach all the shader one by one
				glDetachShader(gShaderProgramObject_v, pShaders[shaderNumber]);
				//delete detached shader
				glDeleteShader(pShaders[shaderNumber]);
				pShaders[shaderNumber] = 0;
			}
			free(pShaders);
		}
		glDeleteProgram(gShaderProgramObject_v);
		gShaderProgramObject_v = 0;
		glUseProgram(0);
	}
	if (gShaderProgramObject_f)
	{
		GLsizei shaderCount;
		GLsizei shaderNumber;

		glUseProgram(gShaderProgramObject_f);
		glGetProgramiv(gShaderProgramObject_f, GL_ATTACHED_SHADERS, &shaderCount);
		GLuint *pShaders = (GLuint *)malloc(sizeof(GLuint) * shaderCount);

		if (pShaders)
		{
			glGetAttachedShaders(gShaderProgramObject_f, shaderCount, &shaderCount, pShaders);
			for (shaderNumber = 0; shaderNumber < shaderCount; shaderNumber++)
			{
				//detach all the shader one by one
				glDetachShader(gShaderProgramObject_f, pShaders[shaderNumber]);
				//delete detached shader
				glDeleteShader(pShaders[shaderNumber]);
				pShaders[shaderNumber] = 0;
			}
			free(pShaders);
		}
		glDeleteProgram(gShaderProgramObject_f);
		gShaderProgramObject_f = 0;
		glUseProgram(0);
	}
	if (gbFullScreen == true)
	{
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPED);
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd,
			HWND_TOP,
			0,
			0,
			0,
			0,
			SWP_NOZORDER | SWP_FRAMECHANGED | SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER);
		ShowCursor(TRUE);
	}

	//break the current context
	if (wglGetCurrentContext() == ghrc)
	{
		wglMakeCurrent(NULL, NULL);
	}

	if (ghrc)
	{
		wglDeleteContext(ghrc);
		ghrc = NULL;
	}

	if (ghdc)
	{
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	if (gpfile)
	{
		fprintf(gpfile, "log file closed successfully\n");
		fclose(gpfile);
		gpfile = NULL;
	}
}
