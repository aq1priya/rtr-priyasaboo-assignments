//  Headers
#import <Foundation/Foundation.h>
#import <Cocoa/Cocoa.h>
#import <QuartzCore/CVDisplayLink.h>
#import <OpenGL/gl3.h>
#import <OpenGL/gl3ext.h>

#import "vmath.h"

#define CHECK_IMAGE_HEIGHT 64
#define CHECK_IMAGE_WIDTH 64

enum
{
    PS_ATTRIBUTE_POSITION = 0,
    PS_ATTRIBUTE_COLOR,
    PS_ATTRIBUTE_NORMAL,
    PS_ATTRIBUTE_TEXTURE0,
};

//  C Style Global Function Declarations
CVReturn MyDisplayLinkCallback(CVDisplayLinkRef, const CVTimeStamp *, const CVTimeStamp *, CVOptionFlags, CVOptionFlags *, void *);

// Global Variables
FILE *gpFile = NULL;

GLubyte CheckImage[CHECK_IMAGE_WIDTH][CHECK_IMAGE_HEIGHT][4];

//  Interface Declarations
@interface AppDelegate:NSObject <NSApplicationDelegate, NSWindowDelegate>
@end

@interface GLView:NSOpenGLView
@end

//  Entry Point Function
int main(int argc, const char *argv[])
{
    // code
    NSAutoreleasePool *pPool = [[NSAutoreleasePool alloc]init];

    NSApp =[NSApplication sharedApplication];

    [NSApp setDelegate:[[AppDelegate alloc]init]];

    [NSApp run];

    [pPool release];

    return(0);
}

//  Interface Implementations
@implementation AppDelegate
{
    @private
        NSWindow *window;
        GLView *glView;
}

-(void)applicationDidFinishLaunching:(NSNotification *)aNotification
{
    // code
    // logFile
    NSBundle *mainBundle=[NSBundle mainBundle];
    NSString *appDirName=[mainBundle bundlePath];
    NSString *parentDirPath=[appDirName stringByDeletingLastPathComponent];
    NSString *logFileNameWithPath=[NSString stringWithFormat:@"%@/Log.txt", parentDirPath];
    const char *pszLogFileNameWithPath=[logFileNameWithPath cStringUsingEncoding:NSASCIIStringEncoding];

    gpFile = fopen(pszLogFileNameWithPath, "w");
    if(gpFile == NULL)
    {
        printf("Can not create log file.\nExitting...\n");
        [self release];
        [NSApp terminate:self];
    }
    fprintf(gpFile, "Program Is Started Successfully\n");
    
    //window
    NSRect win_rect;
    win_rect=NSMakeRect(0.0,0.0,800.0,600.0);

    //create simple window
    window=[[NSWindow alloc]initWithContentRect:win_rect
    styleMask:NSWindowStyleMaskTitled | NSWindowStyleMaskClosable | NSWindowStyleMaskMiniaturizable | NSWindowStyleMaskResizable
      backing:NSBackingStoreBuffered
        defer:NO];
    
    [window setTitle:@"MacOS OpenGL Window"];
    [window center];

    glView=[[GLView alloc]initWithFrame:win_rect];

    [window setContentView:glView];
    [window setDelegate:self];
    [window makeKeyAndOrderFront:self];
}

-(void)applicationWillTerminate:(NSNotification *)notification
{
    //code
    fprintf(gpFile, "Program Is Terminated Successfully\n");

    if(gpFile)
    {
        fclose(gpFile);
        gpFile = NULL;
    }
}

-(void)windowWillClose:(NSNotification *)notification
{
    // code
    [NSApp terminate:self];
}

-(void)dealloc
{
    //code
    [glView release];

    [window release];

    [super dealloc];
}
@end

@implementation GLView
{
   
    @private
        CVDisplayLinkRef displayLink;

        GLuint vertexShaderObject;                
        GLuint fragmentShaderObject;
        GLuint shaderProgramObject;

        GLuint vao;
        GLuint vbo_rectangle_position;
        GLuint vbo_rectangle_texcoord;
        
        GLuint mvpUniform;        
        GLuint samplerUniform;
        
        GLuint texImage;
        
        vmath::mat4 perspectiveProjectionMatrix;
}

-(id)initWithFrame:(NSRect)frame;
{
    // code
    self=[super initWithFrame:frame];

    if(self)
    {
        [[self window]setContentView:self];

        NSOpenGLPixelFormatAttribute attrs[] =
        {
            // must specify the 4.1 core Profile to use OpenGL 4.1
            NSOpenGLPFAOpenGLProfile, NSOpenGLProfileVersion4_1Core,
            //specify the display ID to associate the GL Context with(main display for now)
            NSOpenGLPFAScreenMask, CGDisplayIDToOpenGLDisplayMask(kCGDirectMainDisplay),
            NSOpenGLPFANoRecovery,
            NSOpenGLPFAAccelerated,
            NSOpenGLPFAColorSize, 24,
            NSOpenGLPFADepthSize, 24,
            NSOpenGLPFAAlphaSize, 8,
            NSOpenGLPFADoubleBuffer,
            0};

        NSOpenGLPixelFormat *pixelFormat=[[[NSOpenGLPixelFormat alloc]initWithAttributes:attrs] autorelease];

        if(pixelFormat == nil)
        {
            fprintf(gpFile, "No Valid OpenGLPixel Format Is Available.\nExitting...\n");
            [self release];
            [NSApp terminate:self];
        }

        NSOpenGLContext *glContext=[[[NSOpenGLContext alloc]initWithFormat:pixelFormat shareContext:nil]autorelease];

        [self setPixelFormat:pixelFormat];

        [self setOpenGLContext:glContext];  // it automatically releases the older context, if present, and sets the newer one
    }
    return(self);
}

-(CVReturn)getFrameForTime:(const CVTimeStamp *)pOutputTime
{
    // code
    NSAutoreleasePool *pool = [[NSAutoreleasePool alloc]init];

    [self drawView];

    [pool release];
    return(kCVReturnSuccess);
}

-(void)prepareOpenGL
{
    // code
    // OpenGL Info
    fprintf(gpFile, "OpenGL Version :   %s\n", glGetString(GL_VERSION));
    fprintf(gpFile, "GLSL Version   :   %s\n", glGetString(GL_SHADING_LANGUAGE_VERSION));

    [[self openGLContext]makeCurrentContext];

    GLint swapInt = 1; // matching with OS's swaping interval default is 1, frame gap
    [[self openGLContext]setValues:&swapInt forParameter:NSOpenGLCPSwapInterval];

    /*      ***** VERTEX SHADER *****       */
    //create shader
    vertexShaderObject = glCreateShader(GL_VERTEX_SHADER);

    // source code for vertex shader
    const GLchar *vertexShaderSourceCode =
    "   #version 410                                        " \
    "                                                   \n  " \
    "   in vec4 vPosition;                                  " \
    "   in vec2 vTexCoord;                                  " \
    "                                                   \n  " \
    "   uniform mat4 u_mvp_matrix;                          " \
    "                                                   \n  " \
    "   out vec2 out_texcoord;                              " \
    "                                                   \n  " \
    "   void main(void)                                     " \
    "   {                                                   " \
    "       gl_Position = u_mvp_matrix * vPosition;         " \
    "       out_texcoord = vTexCoord;                       " \
    "   }                                                   " ;

    glShaderSource(vertexShaderObject, 1, (const GLchar**)&vertexShaderSourceCode, NULL);

    // compile shader
    GLint iInfoLogLength =0;
    GLint iShaderCompileStatus = 0;
    char *szInfoLog = NULL;

    glCompileShader(vertexShaderObject);
	glGetShaderiv(vertexShaderObject, GL_COMPILE_STATUS, &iShaderCompileStatus);
	if (iShaderCompileStatus == GL_FALSE)
	{
		glGetShaderiv(vertexShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (GLchar *)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(vertexShaderObject, iInfoLogLength, &written, szInfoLog);
				fprintf(gpFile, "VERTEX SHADER COMPILATION LOG : %s\n", szInfoLog);
				free(szInfoLog);
                [self release];
                [NSApp terminate:self];
            }
		}
	}

    /*      ***** FRAGMENT SHADER *****     */
    //create shader
    fragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);

    // source code for fragment shader
    const GLchar* fragmentShaderSourceCode =
    "   #version 410                                                " \
    "                                                           \n  " \
    "   in vec2 out_texcoord;                                       " \
    "                                                           \n  " \
    "   out vec4 FragColor;                                         " \
    "                                                           \n  " \
    "   uniform sampler2D u_sampler;                                " \
    "                                                           \n  " \
    "   void main(void)                                             " \
    "   {                                                           " \
    "       vec3 tex = vec3(texture(u_sampler, out_texcoord));      " \
    "       FragColor = vec4(tex, 1.0);                             " \
    "   }                                                           " ;

     glShaderSource(fragmentShaderObject, 1, (const GLchar**)&fragmentShaderSourceCode, NULL);

    // compile shader
    iInfoLogLength =0;
    iShaderCompileStatus = 0;
    szInfoLog = NULL;

    glCompileShader(fragmentShaderObject);
	glGetShaderiv(fragmentShaderObject, GL_COMPILE_STATUS, &iShaderCompileStatus);
	if (iShaderCompileStatus == GL_FALSE)
	{
		glGetShaderiv(fragmentShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (GLchar *)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(fragmentShaderObject, iInfoLogLength, &written, szInfoLog);
				fprintf(gpFile, "FRAGMENT SHADER COMPILATION LOG : %s\n", szInfoLog);
				free(szInfoLog);
                [self release];
                [NSApp terminate:self];
            }
		}
	}

    /*      ***** SHADER PROGRAM *****      */
    //create program object
    shaderProgramObject = glCreateProgram();

    //attach shaders to it
    glAttachShader(shaderProgramObject, vertexShaderObject);
    glAttachShader(shaderProgramObject, fragmentShaderObject);

    // pre-linking binding of shader program object with vertex shader position attribute
    glBindAttribLocation(shaderProgramObject, PS_ATTRIBUTE_POSITION, "vPosition");
    glBindAttribLocation(shaderProgramObject, PS_ATTRIBUTE_TEXTURE0, "vTexCoord");

    // link shader program object
	glLinkProgram(shaderProgramObject);

	//error checking
	GLint iProgramLinkStatus = 0;
	iInfoLogLength = 0;
	szInfoLog = NULL;

	glGetProgramiv(shaderProgramObject, GL_LINK_STATUS, &iProgramLinkStatus);
	if (iProgramLinkStatus == GL_FALSE)
	{
		glGetProgramiv(shaderProgramObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (GLchar *)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLint written;
				glGetProgramInfoLog(shaderProgramObject, iInfoLogLength, &written, szInfoLog);
				fprintf(gpFile, "SHADER PROGRAM LINK LOG : %s\n", szInfoLog);
				free(szInfoLog);
				[self release];
                [NSApp terminate:self];
			}
		}
	}

    // get MVP uniform location
	mvpUniform = glGetUniformLocation(shaderProgramObject, "u_mvp_matrix");
	samplerUniform = glGetUniformLocation(shaderProgramObject, "u_sampler");

    // load Texture
    //textureSmiley = [self loadTextureFromBMPFile:"Smiley.bmp"];

    // vertices, color, shader attribs, vbo, vao initialization
  
   	const GLfloat rectangleTexcoord[] = {
		1.0f, 1.0f,
		0.0f, 1.0f,
		0.0f, 0.0f,
		1.0f, 0.0f
	};

    
    // rectangle
     glGenVertexArrays(1, &vao);
	glBindVertexArray(vao);
	
	//buffer for rectangle position
	glGenBuffers(1, &vbo_rectangle_position);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_rectangle_position);
	glBufferData(GL_ARRAY_BUFFER, 4 * 3 * sizeof(GLfloat), NULL, GL_DYNAMIC_DRAW);
	glVertexAttribPointer(PS_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(PS_ATTRIBUTE_POSITION);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	//buffer for rectangle texcoord
	glGenBuffers(1, &vbo_rectangle_texcoord);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_rectangle_texcoord);
	glBufferData(GL_ARRAY_BUFFER, sizeof(rectangleTexcoord), rectangleTexcoord, GL_STATIC_DRAW);
	glVertexAttribPointer(PS_ATTRIBUTE_TEXTURE0, 2, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(PS_ATTRIBUTE_TEXTURE0);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	glBindVertexArray(0);
    

	// load texture
	[self loadTexture:&texImage];

    glClearDepth(1.0f);
    glEnable(GL_DEPTH_TEST);
    glDepthFunc(GL_LEQUAL);

    glEnable(GL_TEXTURE_2D);

    // set background color
    glClearColor(0.0f, 0.0f, 0.0f, 0.0f);   //black

    // set projection matrix to identity
    perspectiveProjectionMatrix = vmath::mat4::identity();

    CVDisplayLinkCreateWithActiveCGDisplays(&displayLink);
    CVDisplayLinkSetOutputCallback(displayLink, &MyDisplayLinkCallback, self);
    CGLContextObj cglContext = (CGLContextObj)[[self openGLContext]CGLContextObj];
    CGLPixelFormatObj cglPixelFormat = (CGLPixelFormatObj)[[self pixelFormat]CGLPixelFormatObj];
    CVDisplayLinkSetCurrentCGDisplayFromOpenGLContext(displayLink, cglContext, cglPixelFormat);
    CVDisplayLinkStart(displayLink);
    
    [super prepareOpenGL];
}

-(void)reshape
{
    // code
    CGLLockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);

    NSRect rect = [self bounds];

    GLfloat width  = rect.size.width;
    GLfloat height = rect.size.height;

    if(height == 0)
        height =1;

    glViewport(0,0,(GLsizei)width, (GLsizei)height);

    // perspective projection matrix : perspective
    perspectiveProjectionMatrix = vmath::perspective(45.0f, width/height, 0.1f, 100.0f);

    CGLUnlockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);
    
    [super reshape];
}

-(void)drawRect:(NSRect)dirtyRect
{
    //code
    [self drawView];
}

-(void)loadTexture:(GLuint *)texture
{
    fprintf(gpFile, "In loadTexture().\n");
    	[self makeCheckImage];

     glPixelStorei(GL_UNPACK_ALIGNMENT, 4);
	glGenTextures(1, texture);
	glBindTexture(GL_TEXTURE_2D, *texture);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexImage2D(GL_TEXTURE_2D,
		0,
		GL_RGBA,
		CHECK_IMAGE_WIDTH,
		CHECK_IMAGE_HEIGHT,
		0,
		GL_RGBA,
		GL_UNSIGNED_BYTE,
		CheckImage);
}

-(void)makeCheckImage
{
    //variable declarations
	int i, j, c;

    fprintf(gpFile, "In make ckeck image().\n");
	for (i = 0; i < CHECK_IMAGE_HEIGHT; i++)
	{
		for (j = 0; j < CHECK_IMAGE_WIDTH; j++)
		{
			c = ((((i & 0x8) == 0) ^ ((j & 0x8) == 0)) * 255);
			CheckImage[i][j][0] = (GLubyte)c;
			CheckImage[i][j][1] = (GLubyte)c;
			CheckImage[i][j][2] = (GLubyte)c;
			CheckImage[i][j][3] = (GLubyte)255;
            fprintf(gpFile, "CheckImage : %d, %d %d %d\n", c, c, c, 255);
		}
	}
}

-(void)drawView
{
    // code
    [[self openGLContext]makeCurrentContext];

    CGLLockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);

    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    vmath::mat4 modelViewMatrix = vmath::mat4::identity();
    vmath::mat4 rotationMatrix = vmath::mat4::identity();
    vmath::mat4 modelViewProjectionMatrix = vmath::mat4::identity();

    modelViewMatrix = vmath::translate(-1.0f, 0.0f, -5.0f);

    modelViewMatrix = modelViewMatrix * rotationMatrix;

    modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;

    glUseProgram(shaderProgramObject);

    glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);
	glUniform1i(samplerUniform, 0);
   
    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, texImage);

    const GLfloat rectangleVertices[] = {
        1.0f, 1.0f, 0.0f,
        -1.0f, 1.0f, 0.0f,
        -1.0f, -1.0f, 0.0f,
        1.0f, -1.0f, 0.0f
    };
    
    //bind with vao
	glBindVertexArray(vao);
    glBindBuffer(GL_ARRAY_BUFFER, vbo_rectangle_position);
    glBufferData(GL_ARRAY_BUFFER, sizeof(rectangleVertices), rectangleVertices, GL_DYNAMIC_DRAW);
	glDrawArrays(GL_TRIANGLE_FAN, 0, 4);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    //unbind vao
	glBindVertexArray(0);
    
    
    modelViewMatrix = vmath::mat4::identity();
    rotationMatrix = vmath::mat4::identity();
    modelViewProjectionMatrix = vmath::mat4::identity();
    
    modelViewMatrix = vmath::translate(0.5f, 0.0f, -5.0f);
    
    modelViewMatrix = modelViewMatrix * rotationMatrix;
    
    modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;

    glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);
    glUniform1i(samplerUniform, 0);
    
    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, texImage);
    
    const GLfloat rectangleVertices2[] = {
        1.0f, -1.0f, 0.0f,
        1.0f, 1.0f, 0.0f,
        2.41421f, 1.0f, -1.42421f,
        2.41421f, -1.0f, -1.41421f
    };
    
    //bind with vao
    glBindVertexArray(vao);
    glBindBuffer(GL_ARRAY_BUFFER, vbo_rectangle_position);
    glBufferData(GL_ARRAY_BUFFER, sizeof(rectangleVertices2), rectangleVertices2, GL_DYNAMIC_DRAW);
    glDrawArrays(GL_TRIANGLE_FAN, 0, 4);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    //unbind vao
    glBindVertexArray(0);
    
    glUseProgram(0);

    CGLFlushDrawable((CGLContextObj)[[self openGLContext]CGLContextObj]);
    CGLUnlockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);

}

-(BOOL)acceptsFirstResponder
{
    //code
    [[self window]makeFirstResponder:self];
    return(YES);
}

-(void)keyDown:(NSEvent *)theEvent
{
    //code
    int key = (int)[[theEvent characters]characterAtIndex:0];
    switch(key)
    {
        case 27:
            [self release];
            [NSApp terminate:self];
            break;
        case 'F':
        case 'f':
            [[self window]toggleFullScreen:self];   //repainting occurs automatically
            break;
        default:
            break;
    }
}

-(void)mouseDown:(NSEvent *)theEvent
{
    // code
}

-(void)mouseDragged:(NSEvent *)theEvent
{
    // code
}

-(void)rightMouseDown:(NSEvent *)theEvent
{
    // code
}

-(void)dealloc
{
    // code
    // destroy vao
    

     if(vao)
    {
        glDeleteVertexArrays(1, &vao);
        vao = 0;
    }

    //destroy vbo
    if(vbo_rectangle_position)
    {
        glDeleteBuffers(1, & vbo_rectangle_position);
        vbo_rectangle_position = 0;
    }

    if(vbo_rectangle_texcoord)
    {
        glDeleteBuffers(1, & vbo_rectangle_texcoord);
        vbo_rectangle_texcoord = 0;
    }

    

    //detach shaders
    glDetachShader(shaderProgramObject, vertexShaderObject);
    glDetachShader(shaderProgramObject, fragmentShaderObject);

    //delete shader objects
    glDeleteShader(vertexShaderObject);
    vertexShaderObject=0;
    glDeleteShader(fragmentShaderObject);
    fragmentShaderObject = 0;

    // delete program object
    glDeleteProgram(shaderProgramObject);
    shaderProgramObject = 0;

    CVDisplayLinkStop(displayLink);
    CVDisplayLinkRelease(displayLink);

    [super dealloc];
}
@end

CVReturn MyDisplayLinkCallback(CVDisplayLinkRef displayLink, 
                                const CVTimeStamp *pNow,
                                const CVTimeStamp *pOutputTime,
                                CVOptionFlags flagsIn,
                                CVOptionFlags *pFlagsOut,
                                void *pDisplayLinkcontext)
{
    CVReturn result = [(GLView *)pDisplayLinkcontext getFrameForTime:pOutputTime];
    return(result);    
}
