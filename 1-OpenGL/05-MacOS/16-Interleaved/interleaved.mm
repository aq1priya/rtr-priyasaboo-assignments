//  Headers
#import <Foundation/Foundation.h>
#import <Cocoa/Cocoa.h>
#import <QuartzCore/CVDisplayLink.h>
#import <OpenGL/gl3.h>
#import <OpenGL/gl3ext.h>

#import "vmath.h"

enum
{
    PS_ATTRIBUTE_POSITION = 0,
    PS_ATTRIBUTE_COLOR,
    PS_ATTRIBUTE_NORMAL,
    PS_ATTRIBUTE_TEXTURE0,
};

//  C Style Global Function Declarations
CVReturn MyDisplayLinkCallback(CVDisplayLinkRef, const CVTimeStamp *, const CVTimeStamp *, CVOptionFlags, CVOptionFlags *, void *);

// Global Variables
FILE *gpFile_ps = NULL;

float lightAmbient[4] = { 0.25f, 0.25f, 0.25f, 0.25f };
float lightDiffuse[4] = { 1.0f, 1.0f, 1.0f, 1.0f };
float lightSpecular[4] = { 1.0f, 1.0f, 1.0f, 1.0f };
float lightPosition[4] = { 2.0f, 2.0f, 2.0f, 1.0f };

float materialAmbient[4] = { 0.25f, 0.25f, 0.25f, 0.0f };
float materialDiffuse[4] = { 1.0f, 1.0f, 1.0f, 1.0f };
float materialSpecular[4] = { 1.0f, 1.0f, 1.0f, 1.0f };
float materialShininess = 128.0f;

//  Interface Declarations
@interface AppDelegate:NSObject <NSApplicationDelegate, NSWindowDelegate>
@end

@interface GLView:NSOpenGLView
@end

//  Entry Point Function
int main(int argc, const char *argv[])
{
    // code
    NSAutoreleasePool *pPool = [[NSAutoreleasePool alloc]init];

    NSApp =[NSApplication sharedApplication];

    [NSApp setDelegate:[[AppDelegate alloc]init]];

    [NSApp run];

    [pPool release];

    return(0);
}

//  Interface Implementations
@implementation AppDelegate
{
    @private
        NSWindow *window;
        GLView *glView;
}

-(void)applicationDidFinishLaunching:(NSNotification *)aNotification
{
    // code
    // logFile
    NSBundle *mainBundle=[NSBundle mainBundle];
    NSString *appDirName=[mainBundle bundlePath];
    NSString *parentDirPath=[appDirName stringByDeletingLastPathComponent];
    NSString *logFileNameWithPath=[NSString stringWithFormat:@"%@/Log.txt", parentDirPath];
    const char *pszLogFileNameWithPath=[logFileNameWithPath cStringUsingEncoding:NSASCIIStringEncoding];

    gpFile_ps = fopen(pszLogFileNameWithPath, "w");
    if(gpFile_ps == NULL)
    {
        printf("Can not create log file.\nExitting...\n");
        [self release];
        [NSApp terminate:self];
    }
    fprintf(gpFile_ps, "Program Is Started Successfully\n");
    
    //window
    NSRect win_rect;
    win_rect=NSMakeRect(0.0,0.0,800.0,600.0);

    //create simple window
    window=[[NSWindow alloc]initWithContentRect:win_rect
    styleMask:NSWindowStyleMaskTitled | NSWindowStyleMaskClosable | NSWindowStyleMaskMiniaturizable | NSWindowStyleMaskResizable
      backing:NSBackingStoreBuffered
        defer:NO];
    
    [window setTitle:@"MacOS OpenGL Window"];
    [window center];

    glView=[[GLView alloc]initWithFrame:win_rect];

    [window setContentView:glView];
    [window setDelegate:self];
    [window makeKeyAndOrderFront:self];
}

-(void)applicationWillTerminate:(NSNotification *)notification
{
    //code
    fprintf(gpFile_ps, "Program Is Terminated Successfully\n");

    if(gpFile_ps)
    {
        fclose(gpFile_ps);
        gpFile_ps = NULL;
    }
}

-(void)windowWillClose:(NSNotification *)notification
{
    // code
    [NSApp terminate:self];
}

-(void)dealloc
{
    //code
    [glView release];

    [window release];

    [super dealloc];
}
@end

@implementation GLView
{
     @private
        CVDisplayLinkRef displayLink;

        GLuint vertexShaderObject;                
        GLuint fragmentShaderObject;
        GLuint shaderProgramObject;

        GLuint vao_cube;
        GLuint vbo_data_cube;

        GLfloat angle_cube;

        GLuint textureMarble;
    
        GLuint samplerUniform;
        GLuint mUniform, vUniform, pUniform;				// ModelView Matrix Uniform and Projection Matrix Uniform
        GLuint LdUniform, KdUniform;			// Diffuse property of Light and diffuse property of material
        GLuint LaUniform, KaUniform;            // Ambient property of light and material
        GLuint LsUniform, KsUniform;            // Specular property of light and material
        GLuint lightPositionUniform;			// light position
        GLuint materialShininessUniform;
        GLuint keyPressedUniform;				// is L key Pressed, to enable lighting effect

        BOOL startAnimation;
        BOOL enableLighting;

        vmath::mat4 perspectiveProjectionMatrix;
}

-(id)initWithFrame:(NSRect)frame;
{
    // code
    self=[super initWithFrame:frame];

    if(self)
    {
        [[self window]setContentView:self];

        NSOpenGLPixelFormatAttribute attrs[] =
        {
            // must specify the 4.1 core Profile to use OpenGL 4.1
            NSOpenGLPFAOpenGLProfile, NSOpenGLProfileVersion4_1Core,
            //specify the display ID to associate the GL Context with(main display for now)
            NSOpenGLPFAScreenMask, CGDisplayIDToOpenGLDisplayMask(kCGDirectMainDisplay),
            NSOpenGLPFANoRecovery,
            NSOpenGLPFAAccelerated,
            NSOpenGLPFAColorSize, 24,
            NSOpenGLPFADepthSize, 24,
            NSOpenGLPFAAlphaSize, 8,
            NSOpenGLPFADoubleBuffer,
            0};

        NSOpenGLPixelFormat *pixelFormat=[[[NSOpenGLPixelFormat alloc]initWithAttributes:attrs] autorelease];

        if(pixelFormat == nil)
        {
            fprintf(gpFile_ps, "No Valid OpenGLPixel Format Is Available.\nExitting...\n");
            [self release];
            [NSApp terminate:self];
        }

        NSOpenGLContext *glContext=[[[NSOpenGLContext alloc]initWithFormat:pixelFormat shareContext:nil]autorelease];

        [self setPixelFormat:pixelFormat];

        [self setOpenGLContext:glContext];  // it automatically releases the older context, if present, and sets the newer one
    }
    return(self);
}

-(CVReturn)getFrameForTime:(const CVTimeStamp *)pOutputTime
{
    // code
    NSAutoreleasePool *pool = [[NSAutoreleasePool alloc]init];

    [self drawView];

    [pool release];
    return(kCVReturnSuccess);
}

-(void)prepareOpenGL
{
    // code
    // OpenGL Info
    fprintf(gpFile_ps, "OpenGL Version :   %s\n", glGetString(GL_VERSION));
    fprintf(gpFile_ps, "GLSL Version   :   %s\n", glGetString(GL_SHADING_LANGUAGE_VERSION));

    [[self openGLContext]makeCurrentContext];

    GLint swapInt = 1; // matching with OS's swaping interval default is 1, frame gap
    [[self openGLContext]setValues:&swapInt forParameter:NSOpenGLCPSwapInterval];

    /*      ***** VERTEX SHADER *****       */
    //create shader
    vertexShaderObject = glCreateShader(GL_VERTEX_SHADER);

    // source code for vertex shader
    const GLchar *vertexShaderSourceCode =
		"	#version 410    																	" \
		"																					  \n" \
		"	in vec4 vPosition;							                        				" \
		"	in vec3 vNormal;							                        				" \
        "   in vec4 vColor;                                                                     " \
        "   in vec2 vTexCoord;                                                                  " \
		"																					  \n" \
		"	uniform mat4 u_m_matrix;					                        				" \
        "   uniform mat4 u_v_matrix;                                                            " \
		"	uniform mat4 u_p_matrix;					                        				" \
		"	uniform int u_l_key_is_pressed;					                            		" \
		"	uniform vec4 u_light_position;								                        " \
		"																					  \n" \
		"	out vec4 out_color;									                              	" \
        "   out vec2 out_tex;                                                                 \n" \
        "   out vec4 eyeCoordinates;                                                           \n" \
        "   out vec3 tNorm;                                                                     " \
        "   out vec3 lightDirection;                                                            " \
        "   out vec3 viewerVector;                                                              " \
        "                                                                                     \n" \
		"	void main(void)																		" \
		"	{																					" \
        "       if(u_l_key_is_pressed == 1)                                                     " \
        "       {                                                                               " \
        "			eyeCoordinates = u_v_matrix * u_m_matrix * vPosition;				    	" \
		"			tNorm = mat3(u_v_matrix * u_m_matrix) * vNormal;		    				" \
		"			lightDirection = vec3(u_light_position - eyeCoordinates);           		" \
        "           viewerVector = vec3(-eyeCoordinates);                                       " \
        "       }                                                                               " \
        "	gl_Position = u_p_matrix * u_v_matrix * u_m_matrix * vPosition;				    	" \
        "   out_tex = vTexCoord;                                                                " \
        "   out_color = vColor;                                                                 " \
		"	}																					";
// u_ld * u_kd * max(dot(s, tNorm), 0.0f);
    glShaderSource(vertexShaderObject, 1, (const GLchar**)&vertexShaderSourceCode, NULL);

    // compile shader
    GLint iInfoLogLength =0;
    GLint iShaderCompileStatus = 0;
    char *szInfoLog = NULL;

    glCompileShader(vertexShaderObject);
	glGetShaderiv(vertexShaderObject, GL_COMPILE_STATUS, &iShaderCompileStatus);
	if (iShaderCompileStatus == GL_FALSE)
	{
		glGetShaderiv(vertexShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (GLchar *)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(vertexShaderObject, iInfoLogLength, &written, szInfoLog);
				fprintf(gpFile_ps, "VERTEX SHADER COMPILATION LOG : %s\n", szInfoLog);
				free(szInfoLog);
                [self release];
                [NSApp terminate:self];
            }
		}
	}

    /*      ***** FRAGMENT SHADER *****     */
    //create shader
    fragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);

    // source code for fragment shader
    const GLchar* fragmentShaderSourceCode =
    "	#version 410 	    																	" \
    "	\n																						" \
    "    in vec3 tNorm;                                                                         " \
    "    in vec3 lightDirection;                                                                " \
    "    in vec3 viewerVector;                                                                  " \
    "    in vec2 out_tex;                                                                      \n" \
    "    in vec4 out_color;                                                                    \n" \
    "                                                                                          \n" \
    "    out vec4 fragColor;                                                                     " \
    "                                                                                          \n" \
    "    uniform vec3 u_ld;                                                                      " \
    "    uniform vec3 u_kd;                                                                      " \
    "    uniform vec3 u_la;                                                                      " \
    "    uniform vec3 u_ka;                                                                      " \
    "    uniform vec3 u_ls;                                                                      " \
    "    uniform vec3 u_ks;                                                                      " \
    "    uniform float u_material_shininess;                                                     " \
    "    uniform int u_l_key_is_pressed;                                                         " \
    "    uniform sampler2D u_sampler;                                                          \n" \
    "                                                                                          \n" \
    "	void main(void)																			 " \
    "    {                                                                                       " \
    "        vec3 phong_ads_light;                                                               " \
    "        vec4 texture_color;                                                               \n" \
    "                                                                                          \n" \
    "        if(u_l_key_is_pressed == 1)                                                         " \
    "        {                                                                                   " \
    "            vec3 normalizedtNorm = normalize(tNorm);                                        " \
    "            vec3 normalizedlightDirection = normalize(lightDirection);                      " \
    "            vec3 normalizedviewerVector = normalize(viewerVector);                          " \
    "                                                                                          \n" \
    "            vec3 reflectionVector = normalize(reflect(-normalizedlightDirection, normalizedtNorm));" \
    "            float tnDotld = max(dot(normalizedlightDirection, normalizedtNorm), 0.0);       " \
    "            float rvDotvv = max(dot(reflectionVector, normalizedviewerVector), 0.0);        " \
    "                                                                                          \n" \
    "            vec3 ambient = u_la * u_ka;                                                     " \
    "            vec3 diffuse = u_ld * u_kd * tnDotld;                                           " \
    "            vec3 specular = u_ls * u_ks * pow(rvDotvv, u_material_shininess);               " \
    "            phong_ads_light = diffuse + ambient + specular;                                 " \
    "                                                                                          \n" \
    "        }                                                                                   " \
    "        else                                                                                " \
    "        {                                                                                   " \
    "            phong_ads_light = vec3(1.0, 0.0, 1.0);                                          " \
    "        }                                                                                   " \
    "    texture_color = texture(u_sampler, out_tex);                                          \n" \
    "                                                                                          \n" \
    "    fragColor =  texture_color * vec4(( vec3(out_color) *phong_ads_light), 1.0);          \n" \
    "    }                                                                                       ";
//ambient + diffuse + specular, texture_color * vec4(( vec3(out_color) *phong_ads_light), 1.0);
     glShaderSource(fragmentShaderObject, 1, (const GLchar**)&fragmentShaderSourceCode, NULL);

    // compile shader
    iInfoLogLength =0;
    iShaderCompileStatus = 0;
    szInfoLog = NULL;

    glCompileShader(fragmentShaderObject);
	glGetShaderiv(fragmentShaderObject, GL_COMPILE_STATUS, &iShaderCompileStatus);
	if (iShaderCompileStatus == GL_FALSE)
	{
		glGetShaderiv(fragmentShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (GLchar *)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(fragmentShaderObject, iInfoLogLength, &written, szInfoLog);
				fprintf(gpFile_ps, "FRAGMENT SHADER COMPILATION LOG : %s\n", szInfoLog);
				free(szInfoLog);
                [self release];
                [NSApp terminate:self];
            }
		}
	}

    /*      ***** SHADER PROGRAM *****      */
    //create program object
    shaderProgramObject = glCreateProgram();

    //attach shaders to it
    glAttachShader(shaderProgramObject, vertexShaderObject);
    glAttachShader(shaderProgramObject, fragmentShaderObject);

    // pre-linking binding of shader program object with vertex shader position attribute
    //(not required when used layout qualifier with location in shader, writing here for legacy of assignments)
    glBindAttribLocation(shaderProgramObject, PS_ATTRIBUTE_POSITION, "vPosition");
    glBindAttribLocation(shaderProgramObject, PS_ATTRIBUTE_TEXTURE0, "vTexCoord");
    glBindAttribLocation(shaderProgramObject, PS_ATTRIBUTE_COLOR, "vColor");
    glBindAttribLocation(shaderProgramObject, PS_ATTRIBUTE_NORMAL, "vNormal");

    // link shader program object
	glLinkProgram(shaderProgramObject);

	//error checking
	GLint iProgramLinkStatus = 0;
	iInfoLogLength = 0;
	szInfoLog = NULL;

	glGetProgramiv(shaderProgramObject, GL_LINK_STATUS, &iProgramLinkStatus);
	if (iProgramLinkStatus == GL_FALSE)
	{
		glGetProgramiv(shaderProgramObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (GLchar *)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLint written;
				glGetProgramInfoLog(shaderProgramObject, iInfoLogLength, &written, szInfoLog);
				fprintf(gpFile_ps, "SHADER PROGRAM LINK LOG : %s\n", szInfoLog);
				free(szInfoLog);
				[self release];
                [NSApp terminate:self];
			}
		}
	}

    // get MVP uniform location
    vUniform = glGetUniformLocation(shaderProgramObject, "u_v_matrix");
    mUniform = glGetUniformLocation(shaderProgramObject, "u_m_matrix");
    pUniform = glGetUniformLocation(shaderProgramObject, "u_p_matrix");
    LdUniform = glGetUniformLocation(shaderProgramObject, "u_ld");
    KdUniform = glGetUniformLocation(shaderProgramObject, "u_kd");
    LaUniform = glGetUniformLocation(shaderProgramObject, "u_la");
    KaUniform = glGetUniformLocation(shaderProgramObject, "u_ka");
    LsUniform = glGetUniformLocation(shaderProgramObject, "u_ls");
    KsUniform = glGetUniformLocation(shaderProgramObject, "u_ks");
    materialShininessUniform = glGetUniformLocation(shaderProgramObject, "u_material_shininess");
    lightPositionUniform = glGetUniformLocation(shaderProgramObject, "u_light_position");
    keyPressedUniform = glGetUniformLocation(shaderProgramObject, "u_l_key_is_pressed");
    samplerUniform = glGetUniformLocation(shaderProgramObject, "u_sampler");
    
    // vertices, color, shader attribs, vbo, vao initialization
   
	const GLfloat cube_vcnt[] = {
        1.0f,  1.0f,  1.0f,     1.0f, 0.0f, 0.0f,     0.0f,  0.0f,  1.0f,     1.0f, 1.0f,
        -1.0f,  1.0f,  1.0f,     1.0f, 0.0f, 0.0f,     0.0f,  0.0f,  1.0f,    0.0f, 1.0f,
        -1.0f, -1.0f,  1.0f,     1.0f, 0.0f, 0.0f,     0.0f,  0.0f,  1.0f,    0.0f, 0.0f,
        1.0f, -1.0f,  1.0f,     1.0f, 0.0f, 0.0f,     0.0f,  0.0f,  1.0f,    1.0f, 0.0f,
        1.0f,  1.0f, -1.0f,     0.0f, 1.0f, 0.0f,     1.0f,  0.0f,  0.0f,    1.0f, 0.0f,
        1.0f,  1.0f,  1.0f,     0.0f, 1.0f, 0.0f,     1.0f,  0.0f,  0.0f,    1.0f, 1.0f,
        1.0f, -1.0f,  1.0f,     0.0f, 1.0f, 0.0f,     1.0f,  0.0f,  0.0f,    0.0f, 1.0f,
        1.0f, -1.0f, -1.0f,     0.0f, 1.0f, 0.0f,     1.0f,  0.0f,  0.0f,    0.0f, 0.0f,
        1.0f,  1.0f, -1.0f,     0.0f, 0.0f, 1.0f,     0.0f,  0.0f, -1.0f,     0.0f, 0.0f,
        -1.0f,  1.0f, -1.0f,     0.0f, 0.0f, 1.0f,     0.0f,  0.0f, -1.0f,    0.0f, 1.0f,
        -1.0f, -1.0f, -1.0f,     0.0f, 0.0f, 1.0f,     0.0f,  0.0f, -1.0f,    1.0f, 1.0f,
        1.0f, -1.0f, -1.0f,     0.0f, 0.0f, 1.0f,     0.0f,  0.0f, -1.0f,    1.0f, 0.0f,
        -1.0f,  1.0f, -1.0f,     1.0f, 0.0f, 1.0f,    -1.0f,  0.0f,  0.0f,    1.0f, 0.0f,
        -1.0f,  1.0f,  1.0f,     1.0f, 0.0f, 1.0f,    -1.0f,  0.0f,  0.0f,    0.0f, 0.0f,
        -1.0f, -1.0f,  1.0f,     1.0f, 0.0f, 1.0f,    -1.0f,  0.0f,  0.0f,    0.0f, 1.0f,
        -1.0f, -1.0f, -1.0f,     1.0f, 0.0f, 1.0f,    -1.0f,  0.0f,  0.0f,    1.0f, 1.0f,
        1.0f,  1.0f, -1.0f,     1.0f, 1.0f, 0.0f,     0.0f,  1.0f,  0.0f,     0.0f, 1.0f,
        -1.0f,  1.0f, -1.0f,     1.0f, 1.0f, 0.0f,     0.0f,  1.0f,  0.0f,    0.0f, 0.0f,
        -1.0f,  1.0f,  1.0f,     1.0f, 1.0f, 0.0f,     0.0f,  1.0f,  0.0f,    1.0f, 0.0f,
        1.0f,  1.0f,  1.0f,     1.0f, 1.0f, 0.0f,     0.0f,  1.0f,  0.0f,    1.0f, 1.0f,
        1.0f, -1.0f, -1.0f,     0.0f, 1.0f, 1.0f,     0.0f, -1.0f,  0.0f,    1.0f, 0.0f,
        -1.0f, -1.0f, -1.0f,     0.0f, 1.0f, 1.0f,     0.0f, -1.0f,  0.0f,    0.0f, 0.0f,
        -1.0f, -1.0f,  1.0f,     0.0f, 1.0f, 1.0f,     0.0f, -1.0f,  0.0f,    0.0f, 1.0f,
        1.0f, -1.0f,  1.0f,     0.0f, 1.0f, 1.0f,     0.0f, -1.0f,  0.0f,    1.0f, 1.0f
    };//front:4-right:4-back:4-left:4-top:4-bottom:4

    //	*** CUBE ***
	glGenVertexArrays(1, &vao_cube);
	glBindVertexArray(vao_cube);
	//buffer for all  data
	glGenBuffers(1, &vbo_data_cube);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_data_cube);
	glBufferData(GL_ARRAY_BUFFER, sizeof(cube_vcnt), cube_vcnt, GL_STATIC_DRAW);
    
	glVertexAttribPointer(PS_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 11 * sizeof(GLfloat), (void *)(0 * sizeof(GLfloat)));
	glEnableVertexAttribArray(PS_ATTRIBUTE_POSITION);
    
    glVertexAttribPointer(PS_ATTRIBUTE_COLOR, 3, GL_FLOAT, GL_FALSE, 11 * sizeof(GLfloat), (void *)(3 * sizeof(GLfloat)));
    glEnableVertexAttribArray(PS_ATTRIBUTE_COLOR);
    
    glVertexAttribPointer(PS_ATTRIBUTE_NORMAL, 3, GL_FLOAT, GL_FALSE, 11 * sizeof(GLfloat), (void *)(6 * sizeof(GLfloat)));
    glEnableVertexAttribArray(PS_ATTRIBUTE_NORMAL);
    
    glVertexAttribPointer(PS_ATTRIBUTE_TEXTURE0, 2, GL_FLOAT, GL_FALSE, 11 * sizeof(GLfloat), (void *)(9 * sizeof(GLfloat)));
    glEnableVertexAttribArray(PS_ATTRIBUTE_TEXTURE0);
    
	glBindBuffer(GL_ARRAY_BUFFER, 0);
    glBindVertexArray(0);
    
    // load texture
    textureMarble = [self loadTextureFromBMPFile:"Marble.bmp"];
    
    startAnimation = NO;
    enableLighting = NO;
    
    glClearDepth(1.0f);
    glEnable(GL_DEPTH_TEST);
    glDepthFunc(GL_LEQUAL);
    //glEnable(GL_CULL_FACE);
    
    // set background color
    glClearColor(0.0f, 0.0f, 0.0f, 1.0f);

    // set projection matrix to identity
    perspectiveProjectionMatrix = vmath::mat4::identity();

    CVDisplayLinkCreateWithActiveCGDisplays(&displayLink);
    CVDisplayLinkSetOutputCallback(displayLink, &MyDisplayLinkCallback, self);
    CGLContextObj cglContext = (CGLContextObj)[[self openGLContext]CGLContextObj];
    CGLPixelFormatObj cglPixelFormat = (CGLPixelFormatObj)[[self pixelFormat]CGLPixelFormatObj];
    CVDisplayLinkSetCurrentCGDisplayFromOpenGLContext(displayLink, cglContext, cglPixelFormat);
    CVDisplayLinkStart(displayLink);
    
    [super prepareOpenGL];
}

-(GLuint)loadTextureFromBMPFile:(const char *)texFileName
{
    NSBundle *mainBundle = [NSBundle mainBundle];
    NSString *appDirName = [mainBundle bundlePath];
    NSString *parentDirPath = [appDirName stringByDeletingLastPathComponent];
    NSString *textureFileNameWithPath = [NSString stringWithFormat:@"%@/%s", parentDirPath, texFileName];
    
    NSImage *bmpImage = [[NSImage alloc]initWithContentsOfFile:textureFileNameWithPath];
    if(!bmpImage)
    {
        NSLog(@"can't find %@", textureFileNameWithPath);
        return(0);
    }
    
    CGImageRef cgImage = [bmpImage CGImageForProposedRect:nil context:nil hints:nil];
    
    int w = (int)CGImageGetWidth(cgImage);
    int h = (int)CGImageGetHeight(cgImage);
    
    CFDataRef imageData = CGDataProviderCopyData(CGImageGetDataProvider(cgImage));
    void *pixels = (void *)CFDataGetBytePtr(imageData);
    
    GLuint bmpTexture;
    glGenTextures(1, &bmpTexture);
    
    glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
    glBindTexture(GL_TEXTURE_2D, bmpTexture);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
    glTexImage2D(GL_TEXTURE_2D,
                 0,
                 GL_RGBA,
                 w,
                 h,
                 0,
                 GL_RGBA,
                 GL_UNSIGNED_BYTE,
                 pixels);
    glGenerateMipmap(GL_TEXTURE_2D);
    
    CFRelease(imageData);
    return(bmpTexture);
}

-(void)reshape
{
    // code
    CGLLockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);

    NSRect rect = [self bounds];

    GLfloat width  = rect.size.width;
    GLfloat height = rect.size.height;

    if(height == 0)
        height =1;

    glViewport(0,0,(GLsizei)width, (GLsizei)height);

    // perspective projection matrix
    perspectiveProjectionMatrix = vmath::perspective(45.0f, width/height, 0.1f, 100.0f);


    CGLUnlockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);
    
    [super reshape];
}

-(void)drawRect:(NSRect)dirtyRect
{
    //code
    [self drawView];
}

-(void)drawView
{
    // code
    [[self openGLContext]makeCurrentContext];

    CGLLockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);

    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    vmath::mat4 modelMatrix = vmath::mat4::identity();
    vmath::mat4 viewMatrix = vmath::mat4::identity();
    vmath::mat4 projectionMatrix = vmath::mat4::identity();
    vmath::mat4 translationMatrix = vmath::mat4::identity();
	vmath::mat4 rotationMatrix = vmath::mat4::identity();
	vmath::mat4 scaleMatrix = vmath::mat4::identity();

    glUseProgram(shaderProgramObject);

    // **** CUBE ****

    //do necessary matrix multiplication
	translationMatrix =  vmath::translate(0.0f, 0.0f, -6.0f);
	rotationMatrix =  vmath::rotate(angle_cube, angle_cube, angle_cube);
	scaleMatrix = vmath::scale(0.8f, 0.8f, 0.8f);
	modelMatrix = translationMatrix * rotationMatrix;
	projectionMatrix = perspectiveProjectionMatrix;
	
	if (enableLighting == YES)
	{
        fprintf(gpFile_ps, "enable lighting uniform\n");
		glUniform1i(keyPressedUniform, 1);
        glUniform3fv(LdUniform, 1, lightDiffuse);
        glUniform3fv(LaUniform, 1, lightAmbient);
        glUniform3fv(LsUniform, 1, lightSpecular);
        
        glUniform3fv(KdUniform, 1, materialDiffuse);
        glUniform3fv(KaUniform, 1, materialAmbient);
        glUniform3fv(KsUniform, 1, materialSpecular);
        
        glUniform1f(materialShininessUniform, materialShininess);
        
        glUniform4f(lightPositionUniform, 0.0f, 0.0f, 2.0f, 1.0f);
	}
	else
	{
		glUniform1i(keyPressedUniform, 0);
	}
	//send necessary matrices to shader in respective uniforms
    glUniform1i(samplerUniform, 0);
	glUniformMatrix4fv(mUniform, 1, GL_FALSE, modelMatrix);
    glUniformMatrix4fv(vUniform, 1, GL_FALSE, viewMatrix);
	glUniformMatrix4fv(pUniform, 1, GL_FALSE, projectionMatrix);

    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, textureMarble);
    
   	//bind to vao_cube
	glBindVertexArray(vao_cube);
	glDrawArrays(GL_TRIANGLE_FAN, 0, 4);
	glDrawArrays(GL_TRIANGLE_FAN, 4, 4);
	glDrawArrays(GL_TRIANGLE_FAN, 8, 4);
	glDrawArrays(GL_TRIANGLE_FAN, 12, 4);
	glDrawArrays(GL_TRIANGLE_FAN, 16, 4);
	glDrawArrays(GL_TRIANGLE_FAN, 20, 4);
	//unbind vao
	glBindVertexArray(0);

    glUseProgram(0);

    if (startAnimation == YES)
    {
        if (angle_cube < 360.0f)
            angle_cube = angle_cube + 0.5f;
        else
            angle_cube = 0.0f;
    }

    
    CGLFlushDrawable((CGLContextObj)[[self openGLContext]CGLContextObj]);
    CGLUnlockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);
    
   // [self update];
}

-(BOOL)acceptsFirstResponder
{
    //code
    [[self window]makeFirstResponder:self];
    return(YES);
}

-(void)keyDown:(NSEvent *)theEvent
{
    //code
    int key = (int)[[theEvent characters]characterAtIndex:0];
    switch(key)
    {
        case 27:
            [self release];
            [NSApp terminate:self];
            break;
        case 'F':
        case 'f':
            [[self window]toggleFullScreen:self];   //repainting occurs automatically
            break;
        
        case 'L':
        case 'l':
            if(enableLighting == NO)
                enableLighting = YES;
            else
                enableLighting = NO;
            break;
        case 'A':
        case 'a':
            if(startAnimation == NO)
                startAnimation = YES;
            else
                startAnimation = NO;
            break;
        default:
            break;
    }
}

-(void)mouseDown:(NSEvent *)theEvent
{
    // code
}

-(void)mouseDragged:(NSEvent *)theEvent
{
    // code
}

-(void)rightMouseDown:(NSEvent *)theEvent
{
    // code
}

-(void)dealloc
{
    // code
    // destroy vao and vbo
	if (vbo_data_cube)
	{
		glDeleteBuffers(1, &vbo_data_cube);
		vbo_data_cube = 0;
	}
	if (vao_cube)
	{
		glDeleteVertexArrays(1, &vao_cube);
		vao_cube = 0;
	}

    //detach shaders
    glDetachShader(shaderProgramObject, vertexShaderObject);
    glDetachShader(shaderProgramObject, fragmentShaderObject);

    //delete shader objects
    glDeleteShader(vertexShaderObject);
    vertexShaderObject=0;
    glDeleteShader(fragmentShaderObject);
    fragmentShaderObject = 0;

    // delete program object
    glDeleteProgram(shaderProgramObject);
    shaderProgramObject = 0;

    CVDisplayLinkStop(displayLink);
    CVDisplayLinkRelease(displayLink);

    [super dealloc];
}
@end

CVReturn MyDisplayLinkCallback(CVDisplayLinkRef displayLink, 
                                const CVTimeStamp *pNow,
                                const CVTimeStamp *pOutputTime,
                                CVOptionFlags flagsIn,
                                CVOptionFlags *pFlagsOut,
                                void *pDisplayLinkcontext)
{
    CVReturn result = [(GLView *)pDisplayLinkcontext getFrameForTime:pOutputTime];
    return(result);    
}
