//  Headers
#import <Foundation/Foundation.h>
#import <Cocoa/Cocoa.h>
#import <QuartzCore/CVDisplayLink.h>
#import <OpenGL/gl3.h>
#import <OpenGL/gl3ext.h>

#import "vmath.h"
#import "ps_sphere.h"

enum
{
    PS_ATTRIBUTE_POSITION = 0,
    PS_ATTRIBUTE_COLOR,
    PS_ATTRIBUTE_NORMAL,
    PS_ATTRIBUTE_TEXTURE0,
};

//  C Style Global Function Declarations
CVReturn MyDisplayLinkCallback(CVDisplayLinkRef, const CVTimeStamp *, const CVTimeStamp *, CVOptionFlags, CVOptionFlags *, void *);

// Global Variables
FILE *gpFile_ps = NULL;
Sphere *mySphere;

//  Interface Declarations
@interface AppDelegate:NSObject <NSApplicationDelegate, NSWindowDelegate>
@end

@interface GLView:NSOpenGLView
@end

//  Entry Point Function
int main(int argc, const char *argv[])
{
    // code
    NSAutoreleasePool *pPool = [[NSAutoreleasePool alloc]init];

    NSApp =[NSApplication sharedApplication];

    [NSApp setDelegate:[[AppDelegate alloc]init]];

    [NSApp run];

    [pPool release];

    return(0);
}

//  Interface Implementations
@implementation AppDelegate
{
    @private
        NSWindow *window;
        GLView *glView;
}

-(void)applicationDidFinishLaunching:(NSNotification *)aNotification
{
    // code
    // logFile
    NSBundle *mainBundle=[NSBundle mainBundle];
    NSString *appDirName=[mainBundle bundlePath];
    NSString *parentDirPath=[appDirName stringByDeletingLastPathComponent];
    NSString *logFileNameWithPath=[NSString stringWithFormat:@"%@/Log.txt", parentDirPath];
    const char *pszLogFileNameWithPath=[logFileNameWithPath cStringUsingEncoding:NSASCIIStringEncoding];

    gpFile_ps = fopen(pszLogFileNameWithPath, "w");
    if(gpFile_ps == NULL)
    {
        printf("Can not create log file.\nExitting...\n");
        [self release];
        [NSApp terminate:self];
    }
    fprintf(gpFile_ps, "Program Is Started Successfully\n");
    
    //window
    NSRect win_rect;
    win_rect=NSMakeRect(0.0,0.0,800.0,600.0);

    //create simple window
    window=[[NSWindow alloc]initWithContentRect:win_rect
    styleMask:NSWindowStyleMaskTitled | NSWindowStyleMaskClosable | NSWindowStyleMaskMiniaturizable | NSWindowStyleMaskResizable
      backing:NSBackingStoreBuffered
        defer:NO];
    
    [window setTitle:@"MacOS OpenGL Window"];
    [window center];

    glView=[[GLView alloc]initWithFrame:win_rect];

    [window setContentView:glView];
    [window setDelegate:self];
    [window makeKeyAndOrderFront:self];
}

-(void)applicationWillTerminate:(NSNotification *)notification
{
    //code
    fprintf(gpFile_ps, "Program Is Terminated Successfully\n");

    if(gpFile_ps)
    {
        fclose(gpFile_ps);
        gpFile_ps = NULL;
    }
}

-(void)windowWillClose:(NSNotification *)notification
{
    // code
    [NSApp terminate:self];
}

-(void)dealloc
{
    //code
    [glView release];

    [window release];

    [super dealloc];
}
@end

@implementation GLView
{
     @private
    CVDisplayLinkRef displayLink;

    GLuint vertexShaderObject_v;
    GLuint fragmentShaderObject_v;
    GLuint shaderProgramObject_v;
    
    GLuint vertexShaderObject_f;
    GLuint fragmentShaderObject_f;
    GLuint shaderProgramObject_f;

    GLuint vao_sphere;
    GLuint vbo_sphere_elements;
    GLuint vbo_sphere_position;
    GLuint vbo_sphere_normals;
    
    float sphere_vertices[1146];
    float sphere_normals[1146];
    float sphere_textures[764];
    unsigned short sphere_elements[2280];
    
    int iNumVertices;
    int iNumElements;
    
    GLfloat angle_sphere;
    
    GLuint mUniform_v, vUniform_v, pUniform_v;	// Model Matrix Uniform, view Matrix Uniform and Projection Matrix Uniform
    GLuint LdUniform_v, KdUniform_v;			// Diffuse property of Light and diffuse property of material
    GLuint LaUniform_v, KaUniform_v;            // Ambient property of light and material
    GLuint LsUniform_v, KsUniform_v;            // Specular property of light and material
    GLuint lightPositionUniform_v;			// light position
    GLuint keyPressedUniform_v;				// is L key Pressed, to enable lighting effect
    GLuint materialShininessUniform_v;
    
    GLuint mUniform_f, vUniform_f, pUniform_f;    // Model Matrix Uniform, view Matrix Uniform and Projection Matrix Uniform
    GLuint LdUniform_f, KdUniform_f;            // Diffuse property of Light and diffuse property of material
    GLuint LaUniform_f, KaUniform_f;            // Ambient property of light and material
    GLuint LsUniform_f, KsUniform_f;            // Specular property of light and material
    GLuint lightPositionUniform_f;            // light position
    GLuint keyPressedUniform_f;                // is L key Pressed, to enable lighting effect
    GLuint materialShininessUniform_f;

    GLfloat lightDiffuse[4];
    GLfloat lightAmbient[4];
    GLfloat lightSpecular[4];
    
    GLfloat lightPosition[4];
    
    float materialAmbient[4];
    float materialDiffuse[4];
    float materialSpecular[4];
    float materialShininess;
    
    BOOL startAnimation;
    BOOL enableLighting;
    BOOL toggleLightingType;
    
    vmath::mat4 perspectiveProjectionMatrix;
}

-(id)initWithFrame:(NSRect)frame;
{
    // code
    self=[super initWithFrame:frame];

    if(self)
    {
        [[self window]setContentView:self];

        NSOpenGLPixelFormatAttribute attrs[] =
        {
            // must specify the 4.1 core Profile to use OpenGL 4.1
            NSOpenGLPFAOpenGLProfile, NSOpenGLProfileVersion4_1Core,
            //specify the display ID to associate the GL Context with(main display for now)
            NSOpenGLPFAScreenMask, CGDisplayIDToOpenGLDisplayMask(kCGDirectMainDisplay),
            NSOpenGLPFANoRecovery,
            NSOpenGLPFAAccelerated,
            NSOpenGLPFAColorSize, 24,
            NSOpenGLPFADepthSize, 24,
            NSOpenGLPFAAlphaSize, 8,
            NSOpenGLPFADoubleBuffer,
            0};

        NSOpenGLPixelFormat *pixelFormat=[[[NSOpenGLPixelFormat alloc]initWithAttributes:attrs] autorelease];

        if(pixelFormat == nil)
        {
            fprintf(gpFile_ps, "No Valid OpenGLPixel Format Is Available.\nExitting...\n");
            [self release];
            [NSApp terminate:self];
        }

        NSOpenGLContext *glContext=[[[NSOpenGLContext alloc]initWithFormat:pixelFormat shareContext:nil]autorelease];

        [self setPixelFormat:pixelFormat];

        [self setOpenGLContext:glContext];  // it automatically releases the older context, if present, and sets the newer one
    }
    return(self);
}

-(CVReturn)getFrameForTime:(const CVTimeStamp *)pOutputTime
{
    // code
    NSAutoreleasePool *pool = [[NSAutoreleasePool alloc]init];

    [self drawView];

    [pool release];
    return(kCVReturnSuccess);
}

-(void)prepareOpenGL
{
    // code
    // OpenGL Info
    fprintf(gpFile_ps, "OpenGL Version :   %s\n", glGetString(GL_VERSION));
    fprintf(gpFile_ps, "GLSL Version   :   %s\n", glGetString(GL_SHADING_LANGUAGE_VERSION));

    [[self openGLContext]makeCurrentContext];

    GLint swapInt = 1; // matching with OS's swaping interval default is 1, frame gap
    [[self openGLContext]setValues:&swapInt forParameter:NSOpenGLCPSwapInterval];

    /*      ***** VERTEX SHADER *****       */
    //create shader
    vertexShaderObject_v = glCreateShader(GL_VERTEX_SHADER);

    // source code for vertex shader
    const GLchar *vertexShaderSourceCode_v =
    "	#version 410    																     " \
    "    \n                                                                                  " \
    "    in vec4 vPosition;                                                                  " \
    "    in vec3 vNormal;                                                                    " \
    "                                                                                      \n" \
    "    uniform mat4 u_m_matrix;                                                            " \
    "    uniform mat4 u_v_matrix;                                                            " \
    "    uniform mat4 u_p_matrix;                                                            " \
    "    uniform int u_l_key_is_pressed;                                                        " \
    "    uniform vec3 u_ld;                                                                    " \
    "    uniform vec3 u_kd;                                                                    " \
    "    uniform vec3 u_la;                                                                    " \
    "    uniform vec3 u_ka;                                                                    " \
    "    uniform vec3 u_ls;                                                                    " \
    "    uniform vec3 u_ks;                                                                    " \
    "    uniform vec4 u_light_position;                                                        " \
    "    uniform float u_material_shininess;                                                    " \
    "                                                                                      \n" \
    "    out vec3 phong_ads_light;                                                            " \
    "                                                                                      \n" \
    "    void main(void)                                                                        " \
    "    {                                                                                    " \
    "        if(u_l_key_is_pressed == 1)                                                        " \
    "        {                                                                                " \
    "            vec4 eyeCoordinates = u_v_matrix * u_m_matrix * vPosition;                    " \
    "            vec3 tNorm = normalize(mat3(u_v_matrix * u_m_matrix) * vNormal);            " \
    "            vec3 lightDirection = normalize(vec3(u_light_position - eyeCoordinates));    " \
    "            float tnDotld = max(dot(lightDirection, tNorm), 0.0);                        " \
    "            vec3 reflectionVector = reflect(-lightDirection, tNorm);                    " \
    "            vec3 viewerVector = normalize(vec3(-eyeCoordinates));                        " \
    "            float rvDotvv = max(dot(reflectionVector, viewerVector), 0.0);                " \
    "                                                                                      \n" \
    "            vec3 ambient = u_la * u_ka;                                                    " \
    "            vec3 diffuse = u_ld * u_kd * tnDotld;                                        " \
    "            vec3 specular = u_ls * u_ks * pow(rvDotvv, u_material_shininess);            " \
    "                                                                                      \n" \
    "            phong_ads_light = ambient + diffuse + specular;                                " \
    "        }                                                                                " \
    "        else                                                                            " \
    "        {                                                                                " \
    "            phong_ads_light = vec3(1.0, 1.0, 1.0);                                        " \
    "        }                                                                                " \
    "                                                                                      \n" \
    "    gl_Position = u_v_matrix * u_p_matrix * u_m_matrix * vPosition;                        " \
    "    }                                                                                    ";

    glShaderSource(vertexShaderObject_v, 1, (const GLchar**)&vertexShaderSourceCode_v, NULL);

    // compile shader
    GLint iInfoLogLength =0;
    GLint iShaderCompileStatus = 0;
    char *szInfoLog = NULL;

    glCompileShader(vertexShaderObject_v);
	glGetShaderiv(vertexShaderObject_v, GL_COMPILE_STATUS, &iShaderCompileStatus);
	if (iShaderCompileStatus == GL_FALSE)
	{
		glGetShaderiv(vertexShaderObject_v, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (GLchar *)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(vertexShaderObject_v, iInfoLogLength, &written, szInfoLog);
				fprintf(gpFile_ps, "VERTEX SHADER COMPILATION LOG : %s\n", szInfoLog);
				free(szInfoLog);
                [self release];
                [NSApp terminate:self];
            }
		}
	}

    /*      ***** FRAGMENT SHADER *****     */
    //create shader
    fragmentShaderObject_v = glCreateShader(GL_FRAGMENT_SHADER);

    // source code for fragment shader
    const GLchar* fragmentShaderSourceCode_v =
    "	 #version 410 	    																	" \
    "	 \n																						" \
    "    in vec3 phong_ads_light;                                                                " \
    "    out vec4 fragColor;                                                                        " \
    "                                                                                        \n    " \
    "    void main(void)                                                                            " \
    "    {                                                                                        " \
    "        fragColor = vec4(phong_ads_light, 1.0);                                                " \
    "    }                                                                                        ";

     glShaderSource(fragmentShaderObject_v, 1, (const GLchar**)&fragmentShaderSourceCode_v, NULL);

    // compile shader
    iInfoLogLength =0;
    iShaderCompileStatus = 0;
    szInfoLog = NULL;

    glCompileShader(fragmentShaderObject_v);
	glGetShaderiv(fragmentShaderObject_v, GL_COMPILE_STATUS, &iShaderCompileStatus);
	if (iShaderCompileStatus == GL_FALSE)
	{
		glGetShaderiv(fragmentShaderObject_v, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (GLchar *)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(fragmentShaderObject_v, iInfoLogLength, &written, szInfoLog);
				fprintf(gpFile_ps, "FRAGMENT SHADER COMPILATION LOG : %s\n", szInfoLog);
				free(szInfoLog);
                [self release];
                [NSApp terminate:self];
            }
		}
	}

    /*      ***** SHADER PROGRAM *****      */
    //create program object
    shaderProgramObject_v = glCreateProgram();

    //attach shaders to it
    glAttachShader(shaderProgramObject_v, vertexShaderObject_v);
    glAttachShader(shaderProgramObject_v, fragmentShaderObject_v);

    // pre-linking binding of shader program object with vertex shader position attribute
    //(not required when used layout qualifier with location in shader, writing here for legacy of assignments)
    glBindAttribLocation(shaderProgramObject_v, PS_ATTRIBUTE_POSITION, "vPosition");
    glBindAttribLocation(shaderProgramObject_v, PS_ATTRIBUTE_NORMAL, "vNormal");

    // link shader program object
	glLinkProgram(shaderProgramObject_v);

	//error checking
	GLint iProgramLinkStatus = 0;
	iInfoLogLength = 0;
	szInfoLog = NULL;

	glGetProgramiv(shaderProgramObject_v, GL_LINK_STATUS, &iProgramLinkStatus);
	if (iProgramLinkStatus == GL_FALSE)
	{
		glGetProgramiv(shaderProgramObject_v, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (GLchar *)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLint written;
				glGetProgramInfoLog(shaderProgramObject_v, iInfoLogLength, &written, szInfoLog);
				fprintf(gpFile_ps, "SHADER PROGRAM LINK LOG : %s\n", szInfoLog);
				free(szInfoLog);
				[self release];
                [NSApp terminate:self];
			}
		}
	}

    // get MVP uniform location
    //postlinking retieving uniform location
    vUniform_v = glGetUniformLocation(shaderProgramObject_v, "u_v_matrix");
    mUniform_v = glGetUniformLocation(shaderProgramObject_v, "u_m_matrix");
    pUniform_v = glGetUniformLocation(shaderProgramObject_v, "u_p_matrix");
    LdUniform_v = glGetUniformLocation(shaderProgramObject_v, "u_ld");
    KdUniform_v = glGetUniformLocation(shaderProgramObject_v, "u_kd");
    LaUniform_v = glGetUniformLocation(shaderProgramObject_v, "u_la");
    KaUniform_v = glGetUniformLocation(shaderProgramObject_v, "u_ka");
    LsUniform_v = glGetUniformLocation(shaderProgramObject_v, "u_ls");
    KsUniform_v = glGetUniformLocation(shaderProgramObject_v, "u_ks");
    materialShininessUniform_v = glGetUniformLocation(shaderProgramObject_v, "u_material_shininess");
    lightPositionUniform_v = glGetUniformLocation(shaderProgramObject_v, "u_light_position");
    keyPressedUniform_v = glGetUniformLocation(shaderProgramObject_v, "u_l_key_is_pressed");
    
    
    //create shader
    vertexShaderObject_f = glCreateShader(GL_VERTEX_SHADER);
    
    // source code for vertex shader
    const GLchar *vertexShaderSourceCode_f =
    "    #version 410                                                                         " \
    "    \n                                                                                  " \
    "    in vec4 vPosition;                                                                  " \
    "    in vec3 vNormal;                                                                    " \
    "                                                                                      \n" \
    "    out vec3 tNorm;                                                                        " \
    "    out vec3 lightDirection;                                                            " \
    "    out vec3 viewerVector;                                                                " \
    "                                                                                      \n" \
    "    uniform mat4 u_m_matrix;                                                            " \
    "    uniform mat4 u_v_matrix;                                                            " \
    "    uniform mat4 u_p_matrix;                                                            " \
    "    uniform int u_l_key_is_pressed;                                                        " \
    "    uniform vec4 u_light_position;                                                        " \
    "                                                                                      \n" \
    "    void main(void)                                                                        " \
    "    {                                                                                    " \
    "        if(u_l_key_is_pressed == 1)                                                        " \
    "        {                                                                                " \
    "            vec4 eyeCoordinates = u_v_matrix * u_m_matrix * vPosition;                          " \
    "            tNorm = mat3(u_v_matrix * u_m_matrix) * vNormal;                               " \
    "            lightDirection = vec3(u_light_position - eyeCoordinates);                      " \
    "            viewerVector = vec3(-eyeCoordinates);                                     " \
    "        }                                                                                " \
    "                                                                                      \n" \
    "    gl_Position = u_v_matrix * u_p_matrix * u_m_matrix * vPosition;                        " \
    "    }                                                                                    ";
    
    glShaderSource(vertexShaderObject_f, 1, (const GLchar**)&vertexShaderSourceCode_f, NULL);
    
    // compile shader
    iInfoLogLength =0;
    iShaderCompileStatus = 0;
    szInfoLog = NULL;
    
    glCompileShader(vertexShaderObject_f);
    glGetShaderiv(vertexShaderObject_f, GL_COMPILE_STATUS, &iShaderCompileStatus);
    if (iShaderCompileStatus == GL_FALSE)
    {
        glGetShaderiv(vertexShaderObject_f, GL_INFO_LOG_LENGTH, &iInfoLogLength);
        if (iInfoLogLength > 0)
        {
            szInfoLog = (GLchar *)malloc(iInfoLogLength);
            if (szInfoLog != NULL)
            {
                GLsizei written;
                glGetShaderInfoLog(vertexShaderObject_f, iInfoLogLength, &written, szInfoLog);
                fprintf(gpFile_ps, "VERTEX SHADER COMPILATION LOG : %s\n", szInfoLog);
                free(szInfoLog);
                [self release];
                [NSApp terminate:self];
            }
        }
    }
    
    /*      ***** FRAGMENT SHADER *****     */
    //create shader
    fragmentShaderObject_f = glCreateShader(GL_FRAGMENT_SHADER);
    
    // source code for fragment shader
    const GLchar* fragmentShaderSourceCode_f =
    "     #version 410                                                                             " \
    "     \n                                                                                        " \
    "    in vec3 tNorm;                                                                            " \
    "    in vec3 lightDirection;                                                                    " \
    "    in vec3 viewerVector;                                                                    " \
    "                                                                                          \n" \
    "    out vec4 fragColor;                                                                        " \
    "                                                                                          \n" \
    "    uniform vec3 u_ld;                                                                        " \
    "    uniform vec3 u_kd;                                                                        " \
    "    uniform vec3 u_la;                                                                        " \
    "    uniform vec3 u_ka;                                                                        " \
    "    uniform vec3 u_ls;                                                                        " \
    "    uniform vec3 u_ks;                                                                        " \
    "    uniform float u_material_shininess;                                                        " \
    "    uniform int u_l_key_is_pressed;                                                            " \
    "                                                                                          \n" \
    "    void main(void)                                                                            " \
    "    {                                                                                        " \
    "        vec3 phong_ads_light;                                                                " \
    "        if(u_l_key_is_pressed == 1)                                                            " \
    "        {                                                                                    " \
    "            vec3 normalizedtNorm = normalize(tNorm);                                        " \
    "            vec3 normalizedlightDirection = normalize(lightDirection);                        " \
    "            vec3 normalizedviewerVector = normalize(viewerVector);                            " \
    "                                                                                          \n" \
    "            vec3 reflectionVector = normalize(reflect(-normalizedlightDirection, normalizedtNorm));" \
    "            float tnDotld = max(dot(normalizedlightDirection, normalizedtNorm), 0.0);        " \
    "            float rvDotvv = max(dot(reflectionVector, normalizedviewerVector), 0.0);        " \
    "                                                                                          \n" \
    "            vec3 ambient = u_la * u_ka;                                                        " \
    "            vec3 diffuse = u_ld * u_kd * tnDotld;                                            " \
    "            vec3 specular = u_ls * u_ks * pow(rvDotvv, u_material_shininess);                " \
    "            phong_ads_light = ambient + diffuse + specular;                                    " \
    "                                                                                          \n" \
    "            fragColor = vec4(phong_ads_light, 1.0);                                            " \
    "        }                                                                                    " \
    "        else                                                                                " \
    "        {                                                                                    " \
    "            phong_ads_light = vec3(1.0, 1.0, 1.0);                                            " \
    "            fragColor = vec4(phong_ads_light, 1.0);                                            " \
    "        }                                                                                    " \
    "    }                                                                                        ";
    
    glShaderSource(fragmentShaderObject_f, 1, (const GLchar**)&fragmentShaderSourceCode_f, NULL);
    
    // compile shader
    iInfoLogLength =0;
    iShaderCompileStatus = 0;
    szInfoLog = NULL;
    
    glCompileShader(fragmentShaderObject_f);
    glGetShaderiv(fragmentShaderObject_f, GL_COMPILE_STATUS, &iShaderCompileStatus);
    if (iShaderCompileStatus == GL_FALSE)
    {
        glGetShaderiv(fragmentShaderObject_f, GL_INFO_LOG_LENGTH, &iInfoLogLength);
        if (iInfoLogLength > 0)
        {
            szInfoLog = (GLchar *)malloc(iInfoLogLength);
            if (szInfoLog != NULL)
            {
                GLsizei written;
                glGetShaderInfoLog(fragmentShaderObject_f, iInfoLogLength, &written, szInfoLog);
                fprintf(gpFile_ps, "FRAGMENT SHADER COMPILATION LOG : %s\n", szInfoLog);
                free(szInfoLog);
                [self release];
                [NSApp terminate:self];
            }
        }
    }
    
    /*      ***** SHADER PROGRAM *****      */
    //create program object
    shaderProgramObject_f = glCreateProgram();
    
    //attach shaders to it
    glAttachShader(shaderProgramObject_f, vertexShaderObject_f);
    glAttachShader(shaderProgramObject_f, fragmentShaderObject_f);
    
    // pre-linking binding of shader program object with vertex shader position attribute
    //(not required when used layout qualifier with location in shader, writing here for legacy of assignments)
    glBindAttribLocation(shaderProgramObject_f, PS_ATTRIBUTE_POSITION, "vPosition");
    glBindAttribLocation(shaderProgramObject_f, PS_ATTRIBUTE_NORMAL, "vNormal");
    
    // link shader program object
    glLinkProgram(shaderProgramObject_f);
    
    //error checking
    iProgramLinkStatus = 0;
    iInfoLogLength = 0;
    szInfoLog = NULL;
    
    glGetProgramiv(shaderProgramObject_f, GL_LINK_STATUS, &iProgramLinkStatus);
    if (iProgramLinkStatus == GL_FALSE)
    {
        glGetProgramiv(shaderProgramObject_f, GL_INFO_LOG_LENGTH, &iInfoLogLength);
        if (iInfoLogLength > 0)
        {
            szInfoLog = (GLchar *)malloc(iInfoLogLength);
            if (szInfoLog != NULL)
            {
                GLint written;
                glGetProgramInfoLog(shaderProgramObject_f, iInfoLogLength, &written, szInfoLog);
                fprintf(gpFile_ps, "SHADER PROGRAM LINK LOG : %s\n", szInfoLog);
                free(szInfoLog);
                [self release];
                [NSApp terminate:self];
            }
        }
    }
    
    // get MVP uniform location
    //postlinking retieving uniform location
    vUniform_f = glGetUniformLocation(shaderProgramObject_f, "u_v_matrix");
    mUniform_f = glGetUniformLocation(shaderProgramObject_f, "u_m_matrix");
    pUniform_f = glGetUniformLocation(shaderProgramObject_f, "u_p_matrix");
    LdUniform_f = glGetUniformLocation(shaderProgramObject_f, "u_ld");
    KdUniform_f = glGetUniformLocation(shaderProgramObject_f, "u_kd");
    LaUniform_f = glGetUniformLocation(shaderProgramObject_f, "u_la");
    KaUniform_f = glGetUniformLocation(shaderProgramObject_f, "u_ka");
    LsUniform_f = glGetUniformLocation(shaderProgramObject_f, "u_ls");
    KsUniform_f = glGetUniformLocation(shaderProgramObject_f, "u_ks");
    materialShininessUniform_f = glGetUniformLocation(shaderProgramObject_f, "u_material_shininess");
    lightPositionUniform_f = glGetUniformLocation(shaderProgramObject_f, "u_light_position");
    keyPressedUniform_f = glGetUniformLocation(shaderProgramObject_f, "u_l_key_is_pressed");
    
    
    // sphere data
    mySphere = [[Sphere alloc]init];
    fprintf(gpFile_ps, "sphere alloc init done\n");
    
    [mySphere getSphereVertexDataWithPosition:sphere_vertices withNormals:sphere_normals withTexCoords:sphere_textures andElements:sphere_elements];
    fprintf(gpFile_ps, "sphere get data done\n");
    
    iNumVertices = [mySphere getNumberOfSphereVertices];
    iNumElements = [mySphere getNumberOfSphereElements];
    fprintf(gpFile_ps, "sphere get number of vertices and elements done\n");
    
    glGenVertexArrays(1,&vao_sphere);
    glBindVertexArray(vao_sphere);
    
    // position vbo
    glGenBuffers(1, &vbo_sphere_position);
    glBindBuffer(GL_ARRAY_BUFFER, vbo_sphere_position);
    glBufferData(GL_ARRAY_BUFFER, sizeof(sphere_vertices), sphere_vertices, GL_STATIC_DRAW);
    glVertexAttribPointer(PS_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
    glEnableVertexAttribArray(PS_ATTRIBUTE_POSITION);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    
    // normal vbo
    glGenBuffers(1, &vbo_sphere_normals);
    glBindBuffer(GL_ARRAY_BUFFER, vbo_sphere_normals);
    glBufferData(GL_ARRAY_BUFFER, sizeof(sphere_normals), sphere_normals, GL_STATIC_DRAW);
    glVertexAttribPointer(PS_ATTRIBUTE_NORMAL, 3, GL_FLOAT, GL_FALSE, 0, NULL);
    glEnableVertexAttribArray(PS_ATTRIBUTE_NORMAL);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    
    // elements vbo
    glGenBuffers(1, &vbo_sphere_elements);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_elements);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(sphere_elements), sphere_elements, GL_STATIC_DRAW);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
    
    glBindVertexArray(0);
    
    // light and material properties
    //light ambient
    lightAmbient[0] = 0.0f;
    lightAmbient[1] = 0.0f;
    lightAmbient[2] = 0.0f;
    lightAmbient[3] = 0.0f;
    
    // light diffuse
    lightDiffuse[0] = 1.0f;
    lightDiffuse[1] = 1.0f;
    lightDiffuse[2] = 1.0f;
    lightDiffuse[3] = 1.0f;
    
    //light specular
    lightSpecular[0] = 1.0f;
    lightSpecular[1] = 1.0f;
    lightSpecular[2] = 1.0f;
    lightSpecular[3] = 1.0f;
    
    // light position
    lightPosition[0] = 100.0f;
    lightPosition[1] = 100.0f;
    lightPosition[2] = 100.0f;
    lightPosition[3] = 1.0f;
    
    // material ambient
    materialAmbient[0] = 0.0f;
    materialAmbient[1] = 0.0f;
    materialAmbient[2] = 0.0f;
    materialAmbient[3] = 0.0f;
    
    // material diffuse
    materialDiffuse[0] = 1.0f;
    materialDiffuse[1] = 1.0f;
    materialDiffuse[2] = 1.0f;
    materialDiffuse[3] = 1.0f;
    
    // material specular
    materialSpecular[0] = 1.0f;
    materialSpecular[1] = 1.0f;
    materialSpecular[2] = 1.0f;
    materialSpecular[3] = 1.0f;
    
    // material shininess
    materialShininess = 128.0f;
    
    startAnimation = NO;
    enableLighting = NO;
    toggleLightingType = YES;
    
    glClearDepth(1.0f);
    glEnable(GL_DEPTH_TEST);
    glDepthFunc(GL_LEQUAL);

    //glEnable(GL_CULL_FACE);

    // set background color
    glClearColor(0.0f, 0.0f, 0.0f, 1.0f);

    // set projection matrix to identity
    perspectiveProjectionMatrix = vmath::mat4::identity();

    CVDisplayLinkCreateWithActiveCGDisplays(&displayLink);
    CVDisplayLinkSetOutputCallback(displayLink, &MyDisplayLinkCallback, self);
    CGLContextObj cglContext = (CGLContextObj)[[self openGLContext]CGLContextObj];
    CGLPixelFormatObj cglPixelFormat = (CGLPixelFormatObj)[[self pixelFormat]CGLPixelFormatObj];
    CVDisplayLinkSetCurrentCGDisplayFromOpenGLContext(displayLink, cglContext, cglPixelFormat);
    CVDisplayLinkStart(displayLink);
    
    //[super prepareOpenGL];
}

-(void)reshape
{
    // code
    CGLLockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);

    NSRect rect = [self bounds];

    GLfloat width  = rect.size.width;
    GLfloat height = rect.size.height;

    if(height == 0)
        height =1;

    glViewport(0,0,(GLsizei)width, (GLsizei)height);

    // perspective projection matrix
    perspectiveProjectionMatrix = vmath::perspective(45.0f, width/height, 0.1f, 100.0f);

    CGLUnlockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);
    
    [super reshape];
}

-(void)drawRect:(NSRect)dirtyRect
{
    //code
    [self drawView];
}

-(void)drawView
{
    // code
    [[self openGLContext]makeCurrentContext];

    CGLLockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);

    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    vmath::mat4 modelMatrix = vmath::mat4::identity();
    vmath::mat4 viewMatrix = vmath::mat4::identity();
    vmath::mat4 projectionMatrix = vmath::mat4::identity();
    vmath::mat4 translationMatrix = vmath::mat4::identity();
	vmath::mat4 rotationMatrix = vmath::mat4::identity();
	vmath::mat4 scaleMatrix = vmath::mat4::identity();

    
    
    
    if(toggleLightingType == NO)
    {
        glUseProgram(shaderProgramObject_v);
        
        // Sphere
        translationMatrix =  vmath::translate(0.0f, 0.0f, -3.0f);
        rotationMatrix =  vmath::rotate(angle_sphere, angle_sphere, angle_sphere);
        modelMatrix = translationMatrix * rotationMatrix;
        projectionMatrix = perspectiveProjectionMatrix;
        
        if (enableLighting == YES)
        {
            glUniform1i(keyPressedUniform_v, 1);
            glUniform3fv(LaUniform_v, 1, lightAmbient);
            glUniform3fv(LdUniform_v, 1, lightDiffuse);
            glUniform3fv(LsUniform_v, 1, lightSpecular);
        
            glUniform3fv(KaUniform_v, 1, materialAmbient);
            glUniform3fv(KdUniform_v, 1, materialDiffuse);
            glUniform3fv(KsUniform_v, 1, materialSpecular);
        
            glUniform1f(materialShininessUniform_v, materialShininess);
        
            glUniform4fv(lightPositionUniform_v, 1, lightPosition);
        }
        else
        {
            glUniform1i(keyPressedUniform_v, 0);
        }
        //send necessary matrices to shader in respective uniforms
        glUniformMatrix4fv(mUniform_v, 1, GL_FALSE, modelMatrix);
        glUniformMatrix4fv(vUniform_v, 1, GL_FALSE, viewMatrix);
        glUniformMatrix4fv(pUniform_v, 1, GL_FALSE, projectionMatrix);

        // draw sphere
        glBindVertexArray(vao_sphere);
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_elements);
        glDrawElements(GL_TRIANGLES, iNumElements, GL_UNSIGNED_SHORT, 0);
        glBindVertexArray(0);
    
        glUseProgram(0);
    }
    else
    {
        glUseProgram(shaderProgramObject_f);
        
        // Sphere
        translationMatrix =  vmath::translate(0.0f, 0.0f, -3.0f);
        rotationMatrix =  vmath::rotate(angle_sphere, angle_sphere, angle_sphere);
        modelMatrix = translationMatrix * rotationMatrix;
        projectionMatrix = perspectiveProjectionMatrix;
        
        if (enableLighting == YES)
        {
            glUniform1i(keyPressedUniform_f, 1);
            glUniform3fv(LaUniform_f, 1, lightAmbient);
            glUniform3fv(LdUniform_f, 1, lightDiffuse);
            glUniform3fv(LsUniform_f, 1, lightSpecular);
            
            glUniform3fv(KaUniform_f, 1, materialAmbient);
            glUniform3fv(KdUniform_f, 1, materialDiffuse);
            glUniform3fv(KsUniform_f, 1, materialSpecular);
            
            glUniform1f(materialShininessUniform_f, materialShininess);
            
            glUniform4fv(lightPositionUniform_f, 1, lightPosition);
        }
        else
        {
            glUniform1i(keyPressedUniform_f, 0);
        }
        //send necessary matrices to shader in respective uniforms
        glUniformMatrix4fv(mUniform_f, 1, GL_FALSE, modelMatrix);
        glUniformMatrix4fv(vUniform_f, 1, GL_FALSE, viewMatrix);
        glUniformMatrix4fv(pUniform_f, 1, GL_FALSE, projectionMatrix);
        
        // draw sphere
        glBindVertexArray(vao_sphere);
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_elements);
        glDrawElements(GL_TRIANGLES, iNumElements, GL_UNSIGNED_SHORT, 0);
        glBindVertexArray(0);
        
        glUseProgram(0);
    }
    if (startAnimation == YES)
    {
        if (angle_sphere < 360.0f)
            angle_sphere = angle_sphere + 0.5f;
        else
            angle_sphere = 0.0f;
    }
    else
    {
        angle_sphere = 0.0f;
    }
    
    CGLFlushDrawable((CGLContextObj)[[self openGLContext]CGLContextObj]);
    CGLUnlockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);
    
   // [self update];
}

-(BOOL)acceptsFirstResponder
{
    //code
    [[self window]makeFirstResponder:self];
    return(YES);
}

-(void)keyDown:(NSEvent *)theEvent
{
    //code
    int key = (int)[[theEvent characters]characterAtIndex:0];
    switch(key)
    {
        case 27:
            [self release];
            [NSApp terminate:self];
            break;
        case 'F':
        case 'f':
            [[self window]toggleFullScreen:self];   //repainting occurs automatically
            break;
        
        case 'L':
        case 'l':
            if(enableLighting == NO)
                enableLighting = YES;
            else
                enableLighting = NO;
            fprintf(gpFile_ps, "l is pressed\n");
            break;
        case 'A':
        case 'a':
            fprintf(gpFile_ps, "a is pressed\n");
            if(startAnimation == NO)
                startAnimation = YES;
            else
                startAnimation = NO;
            break;
        case 't':
        case 'T':
            if(toggleLightingType == NO)
                toggleLightingType = YES;
            else
                toggleLightingType = NO;
            break;
        default:
            break;
    }
}

-(void)mouseDown:(NSEvent *)theEvent
{
    // code
}

-(void)mouseDragged:(NSEvent *)theEvent
{
    // code
}

-(void)rightMouseDown:(NSEvent *)theEvent
{
    // code
}

-(void)dealloc
{
    // code
    // destroy vao
   	if (vbo_sphere_elements)
	{
		glDeleteBuffers(1, &vbo_sphere_elements);
		vbo_sphere_elements = 0;
	}
	if (vbo_sphere_normals)
	{
		glDeleteBuffers(1, &vbo_sphere_normals);
		vbo_sphere_normals = 0;
	}
    if (vbo_sphere_position)
    {
        glDeleteBuffers(1, &vbo_sphere_position);
        vbo_sphere_position = 0;
    }
	if (vao_sphere)
	{
		glDeleteVertexArrays(1, &vao_sphere);
		vao_sphere = 0;
	}

    //detach shaders
    glDetachShader(shaderProgramObject_v, vertexShaderObject_v);
    glDetachShader(shaderProgramObject_v, fragmentShaderObject_v);

    //delete shader objects
    glDeleteShader(vertexShaderObject_v);
    vertexShaderObject_v=0;
    glDeleteShader(fragmentShaderObject_v);
    fragmentShaderObject_v = 0;

    // delete program object
    glDeleteProgram(shaderProgramObject_v);
    shaderProgramObject_v = 0;

    CVDisplayLinkStop(displayLink);
    CVDisplayLinkRelease(displayLink);

    [super dealloc];
}
@end

CVReturn MyDisplayLinkCallback(CVDisplayLinkRef displayLink, 
                                const CVTimeStamp *pNow,
                                const CVTimeStamp *pOutputTime,
                                CVOptionFlags flagsIn,
                                CVOptionFlags *pFlagsOut,
                                void *pDisplayLinkcontext)
{
    CVReturn result = [(GLView *)pDisplayLinkcontext getFrameForTime:pOutputTime];
    return(result);    
}
