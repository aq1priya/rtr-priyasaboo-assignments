//  Headers
#import <Foundation/Foundation.h>
#import <Cocoa/Cocoa.h>
#import <QuartzCore/CVDisplayLink.h>
#import <OpenGL/gl3.h>
#import <OpenGL/gl3ext.h>

#import "vmath.h"
#import "ps_sphere.h"

enum
{
    PS_ATTRIBUTE_POSITION = 0,
    PS_ATTRIBUTE_COLOR,
    PS_ATTRIBUTE_NORMAL,
    PS_ATTRIBUTE_TEXTURE0,
};

//  C Style Global Function Declarations
CVReturn MyDisplayLinkCallback(CVDisplayLinkRef, const CVTimeStamp *, const CVTimeStamp *, CVOptionFlags, CVOptionFlags *, void *);

// Global Variables
FILE *gpFile_ps = NULL;
Sphere *mySphere;

//  Interface Declarations
@interface AppDelegate:NSObject <NSApplicationDelegate, NSWindowDelegate>
@end

@interface GLView:NSOpenGLView
@end

//  Entry Point Function
int main(int argc, const char *argv[])
{
    // code
    NSAutoreleasePool *pPool = [[NSAutoreleasePool alloc]init];

    NSApp =[NSApplication sharedApplication];

    [NSApp setDelegate:[[AppDelegate alloc]init]];

    [NSApp run];

    [pPool release];

    return(0);
}

//  Interface Implementations
@implementation AppDelegate
{
    @private
        NSWindow *window;
        GLView *glView;
}

-(void)applicationDidFinishLaunching:(NSNotification *)aNotification
{
    // code
    // logFile
    NSBundle *mainBundle=[NSBundle mainBundle];
    NSString *appDirName=[mainBundle bundlePath];
    NSString *parentDirPath=[appDirName stringByDeletingLastPathComponent];
    NSString *logFileNameWithPath=[NSString stringWithFormat:@"%@/Log.txt", parentDirPath];
    const char *pszLogFileNameWithPath=[logFileNameWithPath cStringUsingEncoding:NSASCIIStringEncoding];

    gpFile_ps = fopen(pszLogFileNameWithPath, "w");
    if(gpFile_ps == NULL)
    {
        printf("Can not create log file.\nExitting...\n");
        [self release];
        [NSApp terminate:self];
    }
    fprintf(gpFile_ps, "Program Is Started Successfully\n");
    
    //window
    NSRect win_rect;
    win_rect=NSMakeRect(0.0,0.0,800.0,600.0);

    //create simple window
    window=[[NSWindow alloc]initWithContentRect:win_rect
    styleMask:NSWindowStyleMaskTitled | NSWindowStyleMaskClosable | NSWindowStyleMaskMiniaturizable | NSWindowStyleMaskResizable
      backing:NSBackingStoreBuffered
        defer:NO];
    
    [window setTitle:@"MacOS OpenGL Window"];
    [window center];

    glView=[[GLView alloc]initWithFrame:win_rect];

    [window setContentView:glView];
    [window setDelegate:self];
    [window makeKeyAndOrderFront:self];
}

-(void)applicationWillTerminate:(NSNotification *)notification
{
    //code
    fprintf(gpFile_ps, "Program Is Terminated Successfully\n");

    if(gpFile_ps)
    {
        fclose(gpFile_ps);
        gpFile_ps = NULL;
    }
}

-(void)windowWillClose:(NSNotification *)notification
{
    // code
    [NSApp terminate:self];
}

-(void)dealloc
{
    //code
    [glView release];

    [window release];

    [super dealloc];
}
@end

@implementation GLView
{
     @private
    CVDisplayLinkRef displayLink;

    GLuint vertexShaderObject_v;
    GLuint fragmentShaderObject_v;
    GLuint shaderProgramObject_v;
    
    GLuint vertexShaderObject_f;
    GLuint fragmentShaderObject_f;
    GLuint shaderProgramObject_f;

    GLuint vao_sphere;
    GLuint vbo_sphere_elements;
    GLuint vbo_sphere_position;
    GLuint vbo_sphere_normals;
    
    float sphere_vertices[1146];
    float sphere_normals[1146];
    float sphere_textures[764];
    unsigned short sphere_elements[2280];
    
    int iNumVertices;
    int iNumElements;
    
    GLfloat angle_sphere;
    
    GLuint mUniform_v, vUniform_v, pUniform_v;	// Model Matrix Uniform, view Matrix Uniform and Projection Matrix Uniform
    GLuint LdUniform_v, KdUniform_v;			// Diffuse property of Light and diffuse property of material
    GLuint LaUniform_v, KaUniform_v;            // Ambient property of light and material
    GLuint LsUniform_v, KsUniform_v;            // Specular property of light and material
    GLuint lightPositionUniform_v;			// light position
    GLuint keyPressedUniform_v;				// is L key Pressed, to enable lighting effect
    GLuint materialShininessUniform_v;
    
    GLuint mUniform_f, vUniform_f, pUniform_f;    // Model Matrix Uniform, view Matrix Uniform and Projection Matrix Uniform
    GLuint LdUniform_f, KdUniform_f;            // Diffuse property of Light and diffuse property of material
    GLuint LaUniform_f, KaUniform_f;            // Ambient property of light and material
    GLuint LsUniform_f, KsUniform_f;            // Specular property of light and material
    GLuint lightPositionUniform_f;            // light position
    GLuint keyPressedUniform_f;                // is L key Pressed, to enable lighting effect
    GLuint materialShininessUniform_f;

    GLfloat lightDiffuse[4];
    GLfloat lightAmbient[4];
    GLfloat lightSpecular[4];
    GLfloat lightPosition[4];
    
    int axis;
    float theta;
    float gWidth;
    float gHeight;
    
    // Sphere characteristics
    struct SphereProperty
    {
        vmath::vec3 spherePosition;
        vmath::vec4 MDiffuse;
        vmath::vec4 MAmbient;
        vmath::vec4 MSpecular;
        int SViewport[4];
        float sphereShininess;
    }Spheres[24];

    BOOL startAnimation;
    BOOL enableLighting;
    BOOL toggleLightingType;
    
    vmath::mat4 perspectiveProjectionMatrix;
}

-(id)initWithFrame:(NSRect)frame;
{
    // code
    self=[super initWithFrame:frame];

    if(self)
    {
        [[self window]setContentView:self];

        NSOpenGLPixelFormatAttribute attrs[] =
        {
            // must specify the 4.1 core Profile to use OpenGL 4.1
            NSOpenGLPFAOpenGLProfile, NSOpenGLProfileVersion4_1Core,
            //specify the display ID to associate the GL Context with(main display for now)
            NSOpenGLPFAScreenMask, CGDisplayIDToOpenGLDisplayMask(kCGDirectMainDisplay),
            NSOpenGLPFANoRecovery,
            NSOpenGLPFAAccelerated,
            NSOpenGLPFAColorSize, 24,
            NSOpenGLPFADepthSize, 24,
            NSOpenGLPFAAlphaSize, 8,
            NSOpenGLPFADoubleBuffer,
            0};

        NSOpenGLPixelFormat *pixelFormat=[[[NSOpenGLPixelFormat alloc]initWithAttributes:attrs] autorelease];

        if(pixelFormat == nil)
        {
            fprintf(gpFile_ps, "No Valid OpenGLPixel Format Is Available.\nExitting...\n");
            [self release];
            [NSApp terminate:self];
        }

        NSOpenGLContext *glContext=[[[NSOpenGLContext alloc]initWithFormat:pixelFormat shareContext:nil]autorelease];

        [self setPixelFormat:pixelFormat];

        [self setOpenGLContext:glContext];  // it automatically releases the older context, if present, and sets the newer one
    }
    return(self);
}

-(CVReturn)getFrameForTime:(const CVTimeStamp *)pOutputTime
{
    // code
    NSAutoreleasePool *pool = [[NSAutoreleasePool alloc]init];

    [self drawView];

    [pool release];
    return(kCVReturnSuccess);
}

-(void)initSphereProperties
{
    //****************** 1st column GEMS***************
    // EMARALD
    Spheres[0].MAmbient = vmath::vec4( 0.0215f, 0.1745f, 0.0215f, 1.0f );
    Spheres[0].MDiffuse = vmath::vec4( 0.07568f, 0.61424f, 0.07568f, 1.0f );
    Spheres[0].MSpecular = vmath::vec4( 0.633f, 0.727811f, 0.633f, 1.0f );
    Spheres[0].spherePosition = vmath::vec3( 0.0f, 0.0f, -3.0f );
    Spheres[0].SViewport[0] = 0;
    Spheres[0].SViewport[1] = 4 * gHeight / 6;
    Spheres[0].SViewport[2] = gWidth / 2;
    Spheres[0].SViewport[3] = gHeight / 2;
    Spheres[0].sphereShininess = 0.6f * 128.0f;
    // JADE
    Spheres[1].MAmbient = vmath::vec4(  0.135f, 0.2225f, 0.1575f, 1.0f );
    Spheres[1].MDiffuse = vmath::vec4(  0.54f, 0.89f, 0.63f, 1.0f );
    Spheres[1].MSpecular = vmath::vec4( 0.316228f, 0.316228f, 0.316228f, 1.0f );
    Spheres[1].spherePosition = vmath::vec3( 0.0f, 0.0f, -3.0f );
    Spheres[1].SViewport[0] = 0;
    Spheres[1].SViewport[1] = 3* gHeight / 6;
    Spheres[1].SViewport[2] = gWidth / 2;
    Spheres[1].SViewport[3] = gHeight / 2;
    Spheres[1].sphereShininess = 0.1f * 128.0f;
    // OBSIDIAN
    Spheres[2].MAmbient = vmath::vec4( 0.05375f, 0.05f, 0.06625f, 1.0f );
    Spheres[2].MDiffuse = vmath::vec4( 0.18275f, 0.17f, 0.22525f, 1.0f );
    Spheres[2].MSpecular = vmath::vec4( 0.332741f, 0.328634f, 0.346435f, 1.0f );
    Spheres[2].spherePosition = vmath::vec3( 0.0f, 0.0f, -3.0f );
    Spheres[2].SViewport[0] = 0;
    Spheres[2].SViewport[1] = gHeight / 3;
    Spheres[2].SViewport[2] = gWidth / 2;
    Spheres[2].SViewport[3] = gHeight / 2;
    Spheres[2].sphereShininess = 0.3f * 128.0f;
    // PEARL
    Spheres[3].MAmbient = vmath::vec4( 0.25f, 0.20725f, 0.20725f, 1.0f );
    Spheres[3].MDiffuse = vmath::vec4( 1.0f, 0.829f, 0.829f, 1.0f );
    Spheres[3].MSpecular = vmath::vec4( 0.296648f, 0.296648f, 0.296648f, 1.0f );
    Spheres[3].spherePosition = vmath::vec3( 0.0f, 0.0f, -3.0f );
    Spheres[3].SViewport[0] = 0;
    Spheres[3].SViewport[1] = gHeight / 6;
    Spheres[3].SViewport[2] = gWidth / 2;
    Spheres[3].SViewport[3] = gHeight / 2;
    Spheres[3].sphereShininess = 0.88f * 128.0f;
    // RUBY
    Spheres[4].MAmbient = vmath::vec4( 0.1745f, 0.01175f, 0.01175f, 1.0f );
    Spheres[4].MDiffuse = vmath::vec4( 0.61424f, 0.04136f, 0.04136f, 1.0f );
    Spheres[4].MSpecular = vmath::vec4( 0.727811f, 0.626959f, 0.626959f, 1.0f );
    Spheres[4].spherePosition = vmath::vec3( 0.0f, 0.0f, -3.0f );
    Spheres[4].SViewport[0] = 0;
    Spheres[4].SViewport[1] = 0;
    Spheres[4].SViewport[2] = gWidth / 2;
    Spheres[4].SViewport[3] = gHeight / 2;
    Spheres[4].sphereShininess = 0.6f * 128.0f;
    // TURQUOISE
    Spheres[5].MAmbient = vmath::vec4( 0.1f, 0.18725f, 0.1745f, 1.0f );
    Spheres[5].MDiffuse = vmath::vec4( 0.396f, 0.74151f, 0.69102f, 1.0f );
    Spheres[5].MSpecular = vmath::vec4( 0.297254f, 0.30829f, 0.306678f, 1.0f );
    Spheres[5].spherePosition = vmath::vec3( 0.0f, 0.0f, -3.0f );
    Spheres[5].SViewport[0] = 0;
    Spheres[5].SViewport[1] = -gHeight / 6;
    Spheres[5].SViewport[2] = gWidth / 2;
    Spheres[5].SViewport[3] = gHeight / 2;
    Spheres[5].sphereShininess = 0.1f * 128.0f;
    
    // **************** 2nd Column : Metal **************************
    // BRASS
    Spheres[6].MAmbient = vmath::vec4(  0.329412f, 0.223529f, 0.027451f, 1.0f );
    Spheres[6].MDiffuse = vmath::vec4( 0.780392f, 0.568627f, 0.113725f, 1.0f );
    Spheres[6].MSpecular = vmath::vec4(  0.992157f, 0.941176f, 0.807843f, 1.0f );
    Spheres[6].spherePosition  = vmath::vec3(  0.0f, 0.0f, -3.0f );
    Spheres[6].SViewport[0] = gWidth / 6;
    Spheres[6].SViewport[1] = 4 * gHeight / 6;
    Spheres[6].SViewport[2] = gWidth / 2;
    Spheres[6].SViewport[3] = gHeight / 2;
    Spheres[6].sphereShininess = 0.21794872f * 128.0f;
    // BRONZE
    Spheres[7].MAmbient = vmath::vec4( 0.2125f, 0.1275f, 0.054f, 1.0f );
    Spheres[7].MDiffuse = vmath::vec4( 0.714f, 0.4284f, 0.18144f, 1.0f );
    Spheres[7].MSpecular = vmath::vec4( 0.393548f, 0.271906f, 0.166721f, 1.0f );
    Spheres[7].spherePosition = vmath::vec3( 0.0f, 0.0f, -3.0f );
    Spheres[7].SViewport[0] = gWidth / 6;
    Spheres[7].SViewport[1] = 3 * gHeight / 6;
    Spheres[7].SViewport[2] = gWidth / 2;
    Spheres[7].SViewport[3] = gHeight / 2;
    Spheres[7].sphereShininess = 0.2f * 128.0f;
    // CHROME
    Spheres[8].MAmbient = vmath::vec4( 0.25f, 0.25f, 0.25f, 1.0f );
    Spheres[8].MDiffuse = vmath::vec4( 0.4f, 0.4f, 0.4f, 1.0f );
    Spheres[8].MSpecular = vmath::vec4( 0.774597f, 0.774597f, 0.774597f, 1.0f );
    Spheres[8].spherePosition = vmath::vec3( 0.0f, 0.0f, -3.0f );
    Spheres[8].SViewport[0] = gWidth / 6;
    Spheres[8].SViewport[1] = 2 * gHeight / 6;
    Spheres[8].SViewport[2] = gWidth / 2;
    Spheres[8].SViewport[3] = gHeight / 2;
    Spheres[8].sphereShininess = 0.6f * 128.0f;
    // COPPER
    Spheres[9].MAmbient = vmath::vec4( 0.19125f, 0.0735f, 0.0225f, 1.0f );
    Spheres[9].MDiffuse = vmath::vec4( 0.7038f, 0.27048f, 0.0828f, 1.0f );
    Spheres[9].MSpecular = vmath::vec4( 0.256777f, 0.137622f, 0.086014f, 1.0f );
    Spheres[9].spherePosition = vmath::vec3( 0.0f, 0.0f, -3.0f );
    Spheres[9].SViewport[0] = gWidth / 6;
    Spheres[9].SViewport[1] = gHeight / 6;
    Spheres[9].SViewport[2] = gWidth / 2;
    Spheres[9].SViewport[3] = gHeight / 2;
    Spheres[9].sphereShininess = 0.1f * 128.0f;
    // GOLD
    Spheres[10].MAmbient = vmath::vec4( 0.24725f, 0.1995f, 0.0745f, 1.0f );
    Spheres[10].MDiffuse = vmath::vec4(  0.75164f, 0.555802f, 0.366065f, 1.0f );
    Spheres[10].MSpecular = vmath::vec4( 0.628281f, 0.555802f, 0.366065f, 1.0f );
    Spheres[10].spherePosition = vmath::vec3( 0.0f, 0.0f, -3.0f );
    Spheres[10].SViewport[0] = gWidth / 6;
    Spheres[10].SViewport[1] = 0;
    Spheres[10].SViewport[2] = gWidth / 2;
    Spheres[10].SViewport[3] = gHeight / 2;
    Spheres[10].sphereShininess = 0.4f * 128.0f;
    // SILVER
    Spheres[11].MAmbient = vmath::vec4(  0.19225f, 0.19225f, 0.19225f, 1.0f );
    Spheres[11].MDiffuse = vmath::vec4(  0.50754f, 0.50754f, 0.50754f, 1.0f );
    Spheres[11].MSpecular = vmath::vec4( 0.508273f, 0.508273f, 0.508273f, 1.0f );
    Spheres[11].spherePosition = vmath::vec3( 0.0f, 0.0f, -3.0f );
    Spheres[11].SViewport[0] = gWidth / 6;
    Spheres[11].SViewport[1] = -gHeight / 6;
    Spheres[11].SViewport[2] = gWidth / 2;
    Spheres[11].SViewport[3] = gHeight / 2;
    Spheres[11].sphereShininess = 0.4f * 128.0f;
    
    // **************** 3rd Column : Plastic **************************
    // BLACK PLASTIC
    Spheres[12].MAmbient = vmath::vec4( 0.0f, 0.0f, 0.0f, 1.0f );
    Spheres[12].MDiffuse = vmath::vec4( 0.01f, 0.01f, 0.01f, 1.0f );
    Spheres[12].MSpecular = vmath::vec4( 0.50f, 0.50f, 0.50f, 1.0f );
    Spheres[12].spherePosition = vmath::vec3( 0.0f, 0.0f, -3.0f );
    Spheres[12].SViewport[0] = gWidth / 3;
    Spheres[12].SViewport[1] = 4 * gHeight / 6;
    Spheres[12].SViewport[2] = gWidth / 2;
    Spheres[12].SViewport[3] = gHeight / 2;
    Spheres[12].sphereShininess = 0.25f * 128.0f;
    // CYAN PLASTIC
    Spheres[13].MAmbient = vmath::vec4( 0.0f, 0.1f, 0.06f, 1.0f );
    Spheres[13].MDiffuse = vmath::vec4( 0.0f, 0.50980392f, 0.50980392f, 1.0f );
    Spheres[13].MSpecular = vmath::vec4( 0.50196078f, 0.50196078f, 0.50196078f, 1.0f );
    Spheres[13].spherePosition = vmath::vec3( 0.0f, 0.0f, -3.0f );
    Spheres[13].SViewport[0] = gWidth / 3;
    Spheres[13].SViewport[1] = 3 * gHeight / 6;
    Spheres[13].SViewport[2] = gWidth / 2;
    Spheres[13].SViewport[3] = gHeight / 2;
    Spheres[13].sphereShininess = 0.25f * 128.0f;
    // GREEN PLASTIC
    Spheres[14].MAmbient = vmath::vec4( 0.0f, 0.0f, 0.0f, 1.0f );
    Spheres[14].MDiffuse = vmath::vec4( 0.1f, 0.35f, 0.1f, 1.0f );
    Spheres[14].MSpecular = vmath::vec4( 0.45f, 0.55f, 0.45f, 1.0f );
    Spheres[14].spherePosition  = vmath::vec3( 0.0f, 0.0f, -3.0f );
    Spheres[14].SViewport[0] = gWidth / 3;
    Spheres[14].SViewport[1] = 2 * gHeight / 6;
    Spheres[14].SViewport[2] = gWidth / 2;
    Spheres[14].SViewport[3] = gHeight / 2;
    Spheres[14].sphereShininess = 0.25f * 128.0f;
    // RED PLASTIC
    Spheres[15].MAmbient = vmath::vec4( 0.0f, 0.0f, 0.0f, 1.0f );
    Spheres[15].MDiffuse = vmath::vec4( 0.5f, 0.0f, 0.0f, 1.0f );
    Spheres[15].MSpecular = vmath::vec4( 0.7f, 0.6f, 0.6f, 1.0f );
    Spheres[15].spherePosition = vmath::vec3( 0.0f, 0.0f, -3.0f );
    Spheres[15].SViewport[0] = gWidth / 3;
    Spheres[15].SViewport[1] = gHeight / 6;
    Spheres[15].SViewport[2] = gWidth / 2;
    Spheres[15].SViewport[3] = gHeight / 2;
    Spheres[15].sphereShininess = 0.25f * 128.0f;
    // WHITE PLASTIC
    Spheres[16].MAmbient = vmath::vec4( 0.0f, 0.0f, 0.0f, 1.0f );
    Spheres[16].MDiffuse = vmath::vec4( 0.55f, 0.55f, 0.55f, 1.0f );
    Spheres[16].MSpecular = vmath::vec4( 0.70f, 0.70f, 0.70f, 1.0f );
    Spheres[16].spherePosition = vmath::vec3( 0.0f, 0.0f, -3.0f);
    Spheres[16].SViewport[0] = gWidth / 3;
    Spheres[16].SViewport[1] = 0;
    Spheres[16].SViewport[2] = gWidth / 2;
    Spheres[16].SViewport[3] = gHeight / 2;
    Spheres[16].sphereShininess = 0.25f * 128.0f;
    // YELLOW PLASTIC
    Spheres[17].MAmbient = vmath::vec4( 0.0f, 0.0f, 0.0f, 1.0f );
    Spheres[17].MDiffuse = vmath::vec4( 0.5f, 0.5f, 0.0f, 1.0f );
    Spheres[17].MSpecular = vmath::vec4( 0.60f, 0.60f, 0.50f, 1.0f );
    Spheres[17].spherePosition = vmath::vec3(0.0f, 0.0f, -3.0f );
    Spheres[17].SViewport[0] = gWidth / 3;
    Spheres[17].SViewport[1] = -gHeight/6;
    Spheres[17].SViewport[2] = gWidth / 2;
    Spheres[17].SViewport[3] = gHeight / 2;
    Spheres[17].sphereShininess = 0.25f * 128.0f;
    
    // **************** 4th Column : Rubber **************************
    // BLACK RUBBER
    Spheres[18].MAmbient = vmath::vec4( 0.02f, 0.02f, 0.02f, 1.0f );
    Spheres[18].MDiffuse = vmath::vec4( 0.01f, 0.01f, 0.01f, 1.0f );
    Spheres[18].MSpecular = vmath::vec4( 0.4f, 0.40f, 0.40f, 1.0f );
    Spheres[18].spherePosition = vmath::vec3( 0.0f, 0.0f, -3.0f );
    Spheres[18].SViewport[0] = gWidth / 2;
    Spheres[18].SViewport[1] = 4 * gHeight / 6;
    Spheres[18].SViewport[2] = gWidth / 2;
    Spheres[18].SViewport[3] = gHeight / 2;
    Spheres[18].sphereShininess = 0.078125f * 128.0f;
    // CYAN RUBBER
    Spheres[19].MAmbient = vmath::vec4( 0.0f, 0.05f, 0.05f, 1.0f );
    Spheres[19].MDiffuse = vmath::vec4( 0.40f, 0.50f, 0.50f, 1.0f );
    Spheres[19].MSpecular = vmath::vec4( 0.04f, 0.7f, 0.7f, 1.0f );
    Spheres[19].spherePosition = vmath::vec3( 0.0f, 0.0f, -3.0f );
    Spheres[19].SViewport[0] = gWidth / 2;
    Spheres[19].SViewport[1] = 3 * gHeight / 6;
    Spheres[19].SViewport[2] = gWidth / 2;
    Spheres[19].SViewport[3] = gHeight / 2;
    Spheres[19].sphereShininess = 0.078125f * 128.0f;
    // GREEN RUBBER
    Spheres[20].MAmbient = vmath::vec4( 0.0f, 0.05f, 0.0f, 1.0f );
    Spheres[20].MDiffuse = vmath::vec4( 0.4f, 0.5f, 0.4f, 1.0f );
    Spheres[20].MSpecular = vmath::vec4( 0.04f, 0.7f, 0.04f, 1.0f );
    Spheres[20].spherePosition = vmath::vec3( 0.0f, 0.0f, -3.0f );
    Spheres[20].SViewport[0] = gWidth / 2;
    Spheres[20].SViewport[1] = 2 * gHeight / 6;
    Spheres[20].SViewport[2] = gWidth / 2;
    Spheres[20].SViewport[3] = gHeight / 2;
    Spheres[20].sphereShininess = 0.078125f * 128.0f;
    // RED RUBBER
    Spheres[21].MAmbient = vmath::vec4( 0.05f, 0.0f, 0.0f, 1.0f );
    Spheres[21].MDiffuse = vmath::vec4( 0.5f, 0.4f, 0.4f, 1.0f );
    Spheres[21].MSpecular = vmath::vec4( 0.7f, 0.04f, 0.04f, 1.0f );
    Spheres[21].spherePosition = vmath::vec3( 0.0f, 0.0f, -3.0f );
    Spheres[21].SViewport[0] = gWidth / 2;
    Spheres[21].SViewport[1] = gHeight / 6;
    Spheres[21].SViewport[2] = gWidth / 2;
    Spheres[21].SViewport[3] = gHeight / 2;
    Spheres[21].sphereShininess = 0.078125f * 128.0f;
    // WHITE RUBBER
    Spheres[22].MAmbient = vmath::vec4( 0.05f, 0.05f, 0.05f, 1.0f );
    Spheres[22].MDiffuse = vmath::vec4( 0.5f, 0.5f, 0.5f, 1.0f );
    Spheres[22].MSpecular = vmath::vec4( 0.70f, 0.70f, 0.70f, 1.0f );
    Spheres[22].spherePosition = vmath::vec3( 0.0f, 0.0f, -3.0f );
    Spheres[22].SViewport[0] = gWidth / 2;
    Spheres[22].SViewport[1] = 0;
    Spheres[22].SViewport[2] = gWidth / 2;
    Spheres[22].SViewport[3] = gHeight / 2;
    Spheres[22].sphereShininess = 0.078125f * 128.0f;
    // YELLOW RUBBER
    Spheres[23].MAmbient = vmath::vec4( 0.05f, 0.05f, 0.0f, 1.0f );
    Spheres[23].MDiffuse = vmath::vec4( 0.50f, 0.50f, 0.40f, 1.0f );
    Spheres[23].MSpecular = vmath::vec4( 0.70f, 0.70f, 0.04f, 1.0f );
    Spheres[23].spherePosition = vmath::vec3( 0.0f, 0.0f, -3.0f );
    Spheres[23].SViewport[0] = gWidth / 2;
    Spheres[23].SViewport[1] = -gHeight / 6;
    Spheres[23].SViewport[2] = gWidth / 2;
    Spheres[23].SViewport[3] = gHeight / 2;
    Spheres[23].sphereShininess = 0.078125f * 128.0f;
}

-(void)prepareOpenGL
{
    // code
    // OpenGL Info
    fprintf(gpFile_ps, "OpenGL Version :   %s\n", glGetString(GL_VERSION));
    fprintf(gpFile_ps, "GLSL Version   :   %s\n", glGetString(GL_SHADING_LANGUAGE_VERSION));

    [[self openGLContext]makeCurrentContext];

    GLint swapInt = 1; // matching with OS's swaping interval default is 1, frame gap
    [[self openGLContext]setValues:&swapInt forParameter:NSOpenGLCPSwapInterval];

    /*      ***** VERTEX SHADER *****       */
    //create shader
    vertexShaderObject_v = glCreateShader(GL_VERTEX_SHADER);

    // source code for vertex shader
    const GLchar *vertexShaderSourceCode_v =
    "	#version 410    																     " \
    "    \n                                                                                  " \
    "    in vec4 vPosition;                                                                  " \
    "    in vec3 vNormal;                                                                    " \
    "                                                                                      \n" \
    "    uniform mat4 u_m_matrix;                                                            " \
    "    uniform mat4 u_v_matrix;                                                            " \
    "    uniform mat4 u_p_matrix;                                                            " \
    "    uniform int u_l_key_is_pressed;                                                        " \
    "    uniform vec3 u_ld;                                                                    " \
    "    uniform vec3 u_kd;                                                                    " \
    "    uniform vec3 u_la;                                                                    " \
    "    uniform vec3 u_ka;                                                                    " \
    "    uniform vec3 u_ls;                                                                    " \
    "    uniform vec3 u_ks;                                                                    " \
    "    uniform vec4 u_light_position;                                                        " \
    "    uniform float u_material_shininess;                                                    " \
    "                                                                                      \n" \
    "    out vec3 phong_ads_light;                                                            " \
    "                                                                                      \n" \
    "    void main(void)                                                                        " \
    "    {                                                                                    " \
    "        if(u_l_key_is_pressed == 1)                                                        " \
    "        {                                                                                " \
    "            vec4 eyeCoordinates = u_v_matrix * u_m_matrix * vPosition;                    " \
    "            vec3 tNorm = normalize(mat3(u_v_matrix * u_m_matrix) * vNormal);            " \
    "            vec3 lightDirection = normalize(vec3(u_light_position - eyeCoordinates));    " \
    "            float tnDotld = max(dot(lightDirection, tNorm), 0.0);                        " \
    "            vec3 reflectionVector = reflect(-lightDirection, tNorm);                    " \
    "            vec3 viewerVector = normalize(vec3(-eyeCoordinates));                        " \
    "            float rvDotvv = max(dot(reflectionVector, viewerVector), 0.0);                " \
    "                                                                                      \n" \
    "            vec3 ambient = u_la * u_ka;                                                    " \
    "            vec3 diffuse = u_ld * u_kd * tnDotld;                                        " \
    "            vec3 specular = u_ls * u_ks * pow(rvDotvv, u_material_shininess);            " \
    "                                                                                      \n" \
    "            phong_ads_light = ambient + diffuse + specular;                                " \
    "        }                                                                                " \
    "        else                                                                            " \
    "        {                                                                                " \
    "            phong_ads_light = vec3(1.0, 1.0, 1.0);                                        " \
    "        }                                                                                " \
    "                                                                                      \n" \
    "    gl_Position = u_v_matrix * u_p_matrix * u_m_matrix * vPosition;                        " \
    "    }                                                                                    ";

    glShaderSource(vertexShaderObject_v, 1, (const GLchar**)&vertexShaderSourceCode_v, NULL);

    // compile shader
    GLint iInfoLogLength =0;
    GLint iShaderCompileStatus = 0;
    char *szInfoLog = NULL;

    glCompileShader(vertexShaderObject_v);
	glGetShaderiv(vertexShaderObject_v, GL_COMPILE_STATUS, &iShaderCompileStatus);
	if (iShaderCompileStatus == GL_FALSE)
	{
		glGetShaderiv(vertexShaderObject_v, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (GLchar *)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(vertexShaderObject_v, iInfoLogLength, &written, szInfoLog);
				fprintf(gpFile_ps, "VERTEX SHADER COMPILATION LOG : %s\n", szInfoLog);
				free(szInfoLog);
                [self release];
                [NSApp terminate:self];
            }
		}
	}

    /*      ***** FRAGMENT SHADER *****     */
    //create shader
    fragmentShaderObject_v = glCreateShader(GL_FRAGMENT_SHADER);

    // source code for fragment shader
    const GLchar* fragmentShaderSourceCode_v =
    "	 #version 410 	    																	" \
    "	 \n																						" \
    "    in vec3 phong_ads_light;                                                                " \
    "    out vec4 fragColor;                                                                        " \
    "                                                                                        \n    " \
    "    void main(void)                                                                            " \
    "    {                                                                                        " \
    "        fragColor = vec4(phong_ads_light, 1.0);                                                " \
    "    }                                                                                        ";

     glShaderSource(fragmentShaderObject_v, 1, (const GLchar**)&fragmentShaderSourceCode_v, NULL);

    // compile shader
    iInfoLogLength =0;
    iShaderCompileStatus = 0;
    szInfoLog = NULL;

    glCompileShader(fragmentShaderObject_v);
	glGetShaderiv(fragmentShaderObject_v, GL_COMPILE_STATUS, &iShaderCompileStatus);
	if (iShaderCompileStatus == GL_FALSE)
	{
		glGetShaderiv(fragmentShaderObject_v, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (GLchar *)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(fragmentShaderObject_v, iInfoLogLength, &written, szInfoLog);
				fprintf(gpFile_ps, "FRAGMENT SHADER COMPILATION LOG : %s\n", szInfoLog);
				free(szInfoLog);
                [self release];
                [NSApp terminate:self];
            }
		}
	}

    /*      ***** SHADER PROGRAM *****      */
    //create program object
    shaderProgramObject_v = glCreateProgram();

    //attach shaders to it
    glAttachShader(shaderProgramObject_v, vertexShaderObject_v);
    glAttachShader(shaderProgramObject_v, fragmentShaderObject_v);

    // pre-linking binding of shader program object with vertex shader position attribute
    //(not required when used layout qualifier with location in shader, writing here for legacy of assignments)
    glBindAttribLocation(shaderProgramObject_v, PS_ATTRIBUTE_POSITION, "vPosition");
    glBindAttribLocation(shaderProgramObject_v, PS_ATTRIBUTE_NORMAL, "vNormal");

    // link shader program object
	glLinkProgram(shaderProgramObject_v);

	//error checking
	GLint iProgramLinkStatus = 0;
	iInfoLogLength = 0;
	szInfoLog = NULL;

	glGetProgramiv(shaderProgramObject_v, GL_LINK_STATUS, &iProgramLinkStatus);
	if (iProgramLinkStatus == GL_FALSE)
	{
		glGetProgramiv(shaderProgramObject_v, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (GLchar *)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLint written;
				glGetProgramInfoLog(shaderProgramObject_v, iInfoLogLength, &written, szInfoLog);
				fprintf(gpFile_ps, "SHADER PROGRAM LINK LOG : %s\n", szInfoLog);
				free(szInfoLog);
				[self release];
                [NSApp terminate:self];
			}
		}
	}

    // get MVP uniform location
    //postlinking retieving uniform location
    vUniform_v = glGetUniformLocation(shaderProgramObject_v, "u_v_matrix");
    mUniform_v = glGetUniformLocation(shaderProgramObject_v, "u_m_matrix");
    pUniform_v = glGetUniformLocation(shaderProgramObject_v, "u_p_matrix");
    LdUniform_v = glGetUniformLocation(shaderProgramObject_v, "u_ld");
    KdUniform_v = glGetUniformLocation(shaderProgramObject_v, "u_kd");
    LaUniform_v = glGetUniformLocation(shaderProgramObject_v, "u_la");
    KaUniform_v = glGetUniformLocation(shaderProgramObject_v, "u_ka");
    LsUniform_v = glGetUniformLocation(shaderProgramObject_v, "u_ls");
    KsUniform_v = glGetUniformLocation(shaderProgramObject_v, "u_ks");
    materialShininessUniform_v = glGetUniformLocation(shaderProgramObject_v, "u_material_shininess");
    lightPositionUniform_v = glGetUniformLocation(shaderProgramObject_v, "u_light_position");
    keyPressedUniform_v = glGetUniformLocation(shaderProgramObject_v, "u_l_key_is_pressed");
    
    
    //create shader
    vertexShaderObject_f = glCreateShader(GL_VERTEX_SHADER);
    
    // source code for vertex shader
    const GLchar *vertexShaderSourceCode_f =
    "    #version 410                                                                         " \
    "    \n                                                                                  " \
    "    in vec4 vPosition;                                                                  " \
    "    in vec3 vNormal;                                                                    " \
    "                                                                                      \n" \
    "    out vec3 tNorm;                                                                        " \
    "    out vec3 lightDirection;                                                            " \
    "    out vec3 viewerVector;                                                                " \
    "                                                                                      \n" \
    "    uniform mat4 u_m_matrix;                                                            " \
    "    uniform mat4 u_v_matrix;                                                            " \
    "    uniform mat4 u_p_matrix;                                                            " \
    "    uniform int u_l_key_is_pressed;                                                        " \
    "    uniform vec4 u_light_position;                                                        " \
    "                                                                                      \n" \
    "    void main(void)                                                                        " \
    "    {                                                                                    " \
    "        if(u_l_key_is_pressed == 1)                                                        " \
    "        {                                                                                " \
    "            vec4 eyeCoordinates = u_v_matrix * u_m_matrix * vPosition;                          " \
    "            tNorm = mat3(u_v_matrix * u_m_matrix) * vNormal;                               " \
    "            lightDirection = vec3(u_light_position - eyeCoordinates);                      " \
    "            viewerVector = vec3(-eyeCoordinates);                                     " \
    "        }                                                                                " \
    "                                                                                      \n" \
    "    gl_Position = u_v_matrix * u_p_matrix * u_m_matrix * vPosition;                        " \
    "    }                                                                                    ";
    
    glShaderSource(vertexShaderObject_f, 1, (const GLchar**)&vertexShaderSourceCode_f, NULL);
    
    // compile shader
    iInfoLogLength =0;
    iShaderCompileStatus = 0;
    szInfoLog = NULL;
    
    glCompileShader(vertexShaderObject_f);
    glGetShaderiv(vertexShaderObject_f, GL_COMPILE_STATUS, &iShaderCompileStatus);
    if (iShaderCompileStatus == GL_FALSE)
    {
        glGetShaderiv(vertexShaderObject_f, GL_INFO_LOG_LENGTH, &iInfoLogLength);
        if (iInfoLogLength > 0)
        {
            szInfoLog = (GLchar *)malloc(iInfoLogLength);
            if (szInfoLog != NULL)
            {
                GLsizei written;
                glGetShaderInfoLog(vertexShaderObject_f, iInfoLogLength, &written, szInfoLog);
                fprintf(gpFile_ps, "VERTEX SHADER COMPILATION LOG : %s\n", szInfoLog);
                free(szInfoLog);
                [self release];
                [NSApp terminate:self];
            }
        }
    }
    
    /*      ***** FRAGMENT SHADER *****     */
    //create shader
    fragmentShaderObject_f = glCreateShader(GL_FRAGMENT_SHADER);
    
    // source code for fragment shader
    const GLchar* fragmentShaderSourceCode_f =
    "     #version 410                                                                             " \
    "     \n                                                                                        " \
    "    in vec3 tNorm;                                                                            " \
    "    in vec3 lightDirection;                                                                    " \
    "    in vec3 viewerVector;                                                                    " \
    "                                                                                          \n" \
    "    out vec4 fragColor;                                                                        " \
    "                                                                                          \n" \
    "    uniform vec3 u_ld;                                                                        " \
    "    uniform vec3 u_kd;                                                                        " \
    "    uniform vec3 u_la;                                                                        " \
    "    uniform vec3 u_ka;                                                                        " \
    "    uniform vec3 u_ls;                                                                        " \
    "    uniform vec3 u_ks;                                                                        " \
    "    uniform float u_material_shininess;                                                        " \
    "    uniform int u_l_key_is_pressed;                                                            " \
    "                                                                                          \n" \
    "    void main(void)                                                                            " \
    "    {                                                                                        " \
    "        vec3 phong_ads_light;                                                                " \
    "        if(u_l_key_is_pressed == 1)                                                            " \
    "        {                                                                                    " \
    "            vec3 normalizedtNorm = normalize(tNorm);                                        " \
    "            vec3 normalizedlightDirection = normalize(lightDirection);                        " \
    "            vec3 normalizedviewerVector = normalize(viewerVector);                            " \
    "                                                                                          \n" \
    "            vec3 reflectionVector = normalize(reflect(-normalizedlightDirection, normalizedtNorm));" \
    "            float tnDotld = max(dot(normalizedlightDirection, normalizedtNorm), 0.0);        " \
    "            float rvDotvv = max(dot(reflectionVector, normalizedviewerVector), 0.0);        " \
    "                                                                                          \n" \
    "            vec3 ambient = u_la * u_ka;                                                        " \
    "            vec3 diffuse = u_ld * u_kd * tnDotld;                                            " \
    "            vec3 specular = u_ls * u_ks * pow(rvDotvv, u_material_shininess);                " \
    "            phong_ads_light = ambient + diffuse + specular;                                    " \
    "                                                                                          \n" \
    "            fragColor = vec4(phong_ads_light, 1.0);                                            " \
    "        }                                                                                    " \
    "        else                                                                                " \
    "        {                                                                                    " \
    "            phong_ads_light = vec3(1.0, 1.0, 1.0);                                            " \
    "            fragColor = vec4(phong_ads_light, 1.0);                                            " \
    "        }                                                                                    " \
    "    }                                                                                        ";
    
    glShaderSource(fragmentShaderObject_f, 1, (const GLchar**)&fragmentShaderSourceCode_f, NULL);
    
    // compile shader
    iInfoLogLength =0;
    iShaderCompileStatus = 0;
    szInfoLog = NULL;
    
    glCompileShader(fragmentShaderObject_f);
    glGetShaderiv(fragmentShaderObject_f, GL_COMPILE_STATUS, &iShaderCompileStatus);
    if (iShaderCompileStatus == GL_FALSE)
    {
        glGetShaderiv(fragmentShaderObject_f, GL_INFO_LOG_LENGTH, &iInfoLogLength);
        if (iInfoLogLength > 0)
        {
            szInfoLog = (GLchar *)malloc(iInfoLogLength);
            if (szInfoLog != NULL)
            {
                GLsizei written;
                glGetShaderInfoLog(fragmentShaderObject_f, iInfoLogLength, &written, szInfoLog);
                fprintf(gpFile_ps, "FRAGMENT SHADER COMPILATION LOG : %s\n", szInfoLog);
                free(szInfoLog);
                [self release];
                [NSApp terminate:self];
            }
        }
    }
    
    /*      ***** SHADER PROGRAM *****      */
    //create program object
    shaderProgramObject_f = glCreateProgram();
    
    //attach shaders to it
    glAttachShader(shaderProgramObject_f, vertexShaderObject_f);
    glAttachShader(shaderProgramObject_f, fragmentShaderObject_f);
    
    // pre-linking binding of shader program object with vertex shader position attribute
    //(not required when used layout qualifier with location in shader, writing here for legacy of assignments)
    glBindAttribLocation(shaderProgramObject_f, PS_ATTRIBUTE_POSITION, "vPosition");
    glBindAttribLocation(shaderProgramObject_f, PS_ATTRIBUTE_NORMAL, "vNormal");
    
    // link shader program object
    glLinkProgram(shaderProgramObject_f);
    
    //error checking
    iProgramLinkStatus = 0;
    iInfoLogLength = 0;
    szInfoLog = NULL;
    
    glGetProgramiv(shaderProgramObject_f, GL_LINK_STATUS, &iProgramLinkStatus);
    if (iProgramLinkStatus == GL_FALSE)
    {
        glGetProgramiv(shaderProgramObject_f, GL_INFO_LOG_LENGTH, &iInfoLogLength);
        if (iInfoLogLength > 0)
        {
            szInfoLog = (GLchar *)malloc(iInfoLogLength);
            if (szInfoLog != NULL)
            {
                GLint written;
                glGetProgramInfoLog(shaderProgramObject_f, iInfoLogLength, &written, szInfoLog);
                fprintf(gpFile_ps, "SHADER PROGRAM LINK LOG : %s\n", szInfoLog);
                free(szInfoLog);
                [self release];
                [NSApp terminate:self];
            }
        }
    }
    
    // get MVP uniform location
    //postlinking retieving uniform location
    vUniform_f = glGetUniformLocation(shaderProgramObject_f, "u_v_matrix");
    mUniform_f = glGetUniformLocation(shaderProgramObject_f, "u_m_matrix");
    pUniform_f = glGetUniformLocation(shaderProgramObject_f, "u_p_matrix");
    LdUniform_f = glGetUniformLocation(shaderProgramObject_f, "u_ld");
    KdUniform_f = glGetUniformLocation(shaderProgramObject_f, "u_kd");
    LaUniform_f = glGetUniformLocation(shaderProgramObject_f, "u_la");
    KaUniform_f = glGetUniformLocation(shaderProgramObject_f, "u_ka");
    LsUniform_f = glGetUniformLocation(shaderProgramObject_f, "u_ls");
    KsUniform_f = glGetUniformLocation(shaderProgramObject_f, "u_ks");
    materialShininessUniform_f = glGetUniformLocation(shaderProgramObject_f, "u_material_shininess");
    lightPositionUniform_f = glGetUniformLocation(shaderProgramObject_f, "u_light_position");
    keyPressedUniform_f = glGetUniformLocation(shaderProgramObject_f, "u_l_key_is_pressed");
    
    
    // sphere data
    mySphere = [[Sphere alloc]init];
    fprintf(gpFile_ps, "sphere alloc init done\n");
    
    [mySphere getSphereVertexDataWithPosition:sphere_vertices withNormals:sphere_normals withTexCoords:sphere_textures andElements:sphere_elements];
    fprintf(gpFile_ps, "sphere get data done\n");
    
    iNumVertices = [mySphere getNumberOfSphereVertices];
    iNumElements = [mySphere getNumberOfSphereElements];
    fprintf(gpFile_ps, "sphere get number of vertices and elements done\n");
    
    glGenVertexArrays(1,&vao_sphere);
    glBindVertexArray(vao_sphere);
    
    // position vbo
    glGenBuffers(1, &vbo_sphere_position);
    glBindBuffer(GL_ARRAY_BUFFER, vbo_sphere_position);
    glBufferData(GL_ARRAY_BUFFER, sizeof(sphere_vertices), sphere_vertices, GL_STATIC_DRAW);
    glVertexAttribPointer(PS_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
    glEnableVertexAttribArray(PS_ATTRIBUTE_POSITION);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    
    // normal vbo
    glGenBuffers(1, &vbo_sphere_normals);
    glBindBuffer(GL_ARRAY_BUFFER, vbo_sphere_normals);
    glBufferData(GL_ARRAY_BUFFER, sizeof(sphere_normals), sphere_normals, GL_STATIC_DRAW);
    glVertexAttribPointer(PS_ATTRIBUTE_NORMAL, 3, GL_FLOAT, GL_FALSE, 0, NULL);
    glEnableVertexAttribArray(PS_ATTRIBUTE_NORMAL);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    
    // elements vbo
    glGenBuffers(1, &vbo_sphere_elements);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_elements);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(sphere_elements), sphere_elements, GL_STATIC_DRAW);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
    
    glBindVertexArray(0);
    
    // light and material properties
    //light ambient
    lightAmbient[0] = 0.0f;
    lightAmbient[1] = 0.0f;
    lightAmbient[2] = 0.0f;
    lightAmbient[3] = 0.0f;
    
    // light diffuse
    lightDiffuse[0] = 1.0f;
    lightDiffuse[1] = 1.0f;
    lightDiffuse[2] = 1.0f;
    lightDiffuse[3] = 1.0f;
    
    //light specular
    lightSpecular[0] = 1.0f;
    lightSpecular[1] = 1.0f;
    lightSpecular[2] = 1.0f;
    lightSpecular[3] = 1.0f;
    
    // light position
    lightPosition[0] = 100.0f;
    lightPosition[1] = 100.0f;
    lightPosition[2] = 100.0f;
    lightPosition[3] = 1.0f;
    
    axis = 0;
    theta = 0.0f;
    
    startAnimation = NO;
    enableLighting = NO;
    toggleLightingType = YES;
    
    glClearDepth(1.0f);
    glEnable(GL_DEPTH_TEST);
    glDepthFunc(GL_LEQUAL);

    //glEnable(GL_CULL_FACE);

    // set background color
    glClearColor(0.0f, 0.0f, 0.0f, 1.0f);

    // set projection matrix to identity
    perspectiveProjectionMatrix = vmath::mat4::identity();

    CVDisplayLinkCreateWithActiveCGDisplays(&displayLink);
    CVDisplayLinkSetOutputCallback(displayLink, &MyDisplayLinkCallback, self);
    CGLContextObj cglContext = (CGLContextObj)[[self openGLContext]CGLContextObj];
    CGLPixelFormatObj cglPixelFormat = (CGLPixelFormatObj)[[self pixelFormat]CGLPixelFormatObj];
    CVDisplayLinkSetCurrentCGDisplayFromOpenGLContext(displayLink, cglContext, cglPixelFormat);
    CVDisplayLinkStart(displayLink);
    
    //[super prepareOpenGL];
}

-(void)reshape
{
    // code
    CGLLockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);

    NSRect rect = [self bounds];

    GLfloat width  = rect.size.width;
    GLfloat height = rect.size.height;

    if(height == 0)
        height =1;
    
    gWidth = width;
    gHeight = height;
    
    //****************** 1st column GEMS***************
    // EMARALD
    Spheres[0].MAmbient = vmath::vec4( 0.0215f, 0.1745f, 0.0215f, 1.0f );
    Spheres[0].MDiffuse = vmath::vec4( 0.07568f, 0.61424f, 0.07568f, 1.0f );
    Spheres[0].MSpecular = vmath::vec4( 0.633f, 0.727811f, 0.633f, 1.0f );
    Spheres[0].spherePosition = vmath::vec3( 0.0f, 0.0f, -3.0f );
    Spheres[0].SViewport[0] = 0;
    Spheres[0].SViewport[1] = 4 * gHeight / 6;
    Spheres[0].SViewport[2] = gWidth / 2;
    Spheres[0].SViewport[3] = gHeight / 2;
    Spheres[0].sphereShininess = 0.6f * 128.0f;
    // JADE
    Spheres[1].MAmbient = vmath::vec4(  0.135f, 0.2225f, 0.1575f, 1.0f );
    Spheres[1].MDiffuse = vmath::vec4(  0.54f, 0.89f, 0.63f, 1.0f );
    Spheres[1].MSpecular = vmath::vec4( 0.316228f, 0.316228f, 0.316228f, 1.0f );
    Spheres[1].spherePosition = vmath::vec3( 0.0f, 0.0f, -3.0f );
    Spheres[1].SViewport[0] = 0;
    Spheres[1].SViewport[1] = 3* gHeight / 6;
    Spheres[1].SViewport[2] = gWidth / 2;
    Spheres[1].SViewport[3] = gHeight / 2;
    Spheres[1].sphereShininess = 0.1f * 128.0f;
    // OBSIDIAN
    Spheres[2].MAmbient = vmath::vec4( 0.05375f, 0.05f, 0.06625f, 1.0f );
    Spheres[2].MDiffuse = vmath::vec4( 0.18275f, 0.17f, 0.22525f, 1.0f );
    Spheres[2].MSpecular = vmath::vec4( 0.332741f, 0.328634f, 0.346435f, 1.0f );
    Spheres[2].spherePosition = vmath::vec3( 0.0f, 0.0f, -3.0f );
    Spheres[2].SViewport[0] = 0;
    Spheres[2].SViewport[1] = gHeight / 3;
    Spheres[2].SViewport[2] = gWidth / 2;
    Spheres[2].SViewport[3] = gHeight / 2;
    Spheres[2].sphereShininess = 0.3f * 128.0f;
    // PEARL
    Spheres[3].MAmbient = vmath::vec4( 0.25f, 0.20725f, 0.20725f, 1.0f );
    Spheres[3].MDiffuse = vmath::vec4( 1.0f, 0.829f, 0.829f, 1.0f );
    Spheres[3].MSpecular = vmath::vec4( 0.296648f, 0.296648f, 0.296648f, 1.0f );
    Spheres[3].spherePosition = vmath::vec3( 0.0f, 0.0f, -3.0f );
    Spheres[3].SViewport[0] = 0;
    Spheres[3].SViewport[1] = gHeight / 6;
    Spheres[3].SViewport[2] = gWidth / 2;
    Spheres[3].SViewport[3] = gHeight / 2;
    Spheres[3].sphereShininess = 0.88f * 128.0f;
    // RUBY
    Spheres[4].MAmbient = vmath::vec4( 0.1745f, 0.01175f, 0.01175f, 1.0f );
    Spheres[4].MDiffuse = vmath::vec4( 0.61424f, 0.04136f, 0.04136f, 1.0f );
    Spheres[4].MSpecular = vmath::vec4( 0.727811f, 0.626959f, 0.626959f, 1.0f );
    Spheres[4].spherePosition = vmath::vec3( 0.0f, 0.0f, -3.0f );
    Spheres[4].SViewport[0] = 0;
    Spheres[4].SViewport[1] = 0;
    Spheres[4].SViewport[2] = gWidth / 2;
    Spheres[4].SViewport[3] = gHeight / 2;
    Spheres[4].sphereShininess = 0.6f * 128.0f;
    // TURQUOISE
    Spheres[5].MAmbient = vmath::vec4( 0.1f, 0.18725f, 0.1745f, 1.0f );
    Spheres[5].MDiffuse = vmath::vec4( 0.396f, 0.74151f, 0.69102f, 1.0f );
    Spheres[5].MSpecular = vmath::vec4( 0.297254f, 0.30829f, 0.306678f, 1.0f );
    Spheres[5].spherePosition = vmath::vec3( 0.0f, 0.0f, -3.0f );
    Spheres[5].SViewport[0] = 0;
    Spheres[5].SViewport[1] = -gHeight / 6;
    Spheres[5].SViewport[2] = gWidth / 2;
    Spheres[5].SViewport[3] = gHeight / 2;
    Spheres[5].sphereShininess = 0.1f * 128.0f;
    
    // **************** 2nd Column : Metal **************************
    // BRASS
    Spheres[6].MAmbient = vmath::vec4(  0.329412f, 0.223529f, 0.027451f, 1.0f );
    Spheres[6].MDiffuse = vmath::vec4( 0.780392f, 0.568627f, 0.113725f, 1.0f );
    Spheres[6].MSpecular = vmath::vec4(  0.992157f, 0.941176f, 0.807843f, 1.0f );
    Spheres[6].spherePosition  = vmath::vec3(  0.0f, 0.0f, -3.0f );
    Spheres[6].SViewport[0] = gWidth / 6;
    Spheres[6].SViewport[1] = 4 * gHeight / 6;
    Spheres[6].SViewport[2] = gWidth / 2;
    Spheres[6].SViewport[3] = gHeight / 2;
    Spheres[6].sphereShininess = 0.21794872f * 128.0f;
    // BRONZE
    Spheres[7].MAmbient = vmath::vec4( 0.2125f, 0.1275f, 0.054f, 1.0f );
    Spheres[7].MDiffuse = vmath::vec4( 0.714f, 0.4284f, 0.18144f, 1.0f );
    Spheres[7].MSpecular = vmath::vec4( 0.393548f, 0.271906f, 0.166721f, 1.0f );
    Spheres[7].spherePosition = vmath::vec3( 0.0f, 0.0f, -3.0f );
    Spheres[7].SViewport[0] = gWidth / 6;
    Spheres[7].SViewport[1] = 3 * gHeight / 6;
    Spheres[7].SViewport[2] = gWidth / 2;
    Spheres[7].SViewport[3] = gHeight / 2;
    Spheres[7].sphereShininess = 0.2f * 128.0f;
    // CHROME
    Spheres[8].MAmbient = vmath::vec4( 0.25f, 0.25f, 0.25f, 1.0f );
    Spheres[8].MDiffuse = vmath::vec4( 0.4f, 0.4f, 0.4f, 1.0f );
    Spheres[8].MSpecular = vmath::vec4( 0.774597f, 0.774597f, 0.774597f, 1.0f );
    Spheres[8].spherePosition = vmath::vec3( 0.0f, 0.0f, -3.0f );
    Spheres[8].SViewport[0] = gWidth / 6;
    Spheres[8].SViewport[1] = 2 * gHeight / 6;
    Spheres[8].SViewport[2] = gWidth / 2;
    Spheres[8].SViewport[3] = gHeight / 2;
    Spheres[8].sphereShininess = 0.6f * 128.0f;
    // COPPER
    Spheres[9].MAmbient = vmath::vec4( 0.19125f, 0.0735f, 0.0225f, 1.0f );
    Spheres[9].MDiffuse = vmath::vec4( 0.7038f, 0.27048f, 0.0828f, 1.0f );
    Spheres[9].MSpecular = vmath::vec4( 0.256777f, 0.137622f, 0.086014f, 1.0f );
    Spheres[9].spherePosition = vmath::vec3( 0.0f, 0.0f, -3.0f );
    Spheres[9].SViewport[0] = gWidth / 6;
    Spheres[9].SViewport[1] = gHeight / 6;
    Spheres[9].SViewport[2] = gWidth / 2;
    Spheres[9].SViewport[3] = gHeight / 2;
    Spheres[9].sphereShininess = 0.1f * 128.0f;
    // GOLD
    Spheres[10].MAmbient = vmath::vec4( 0.24725f, 0.1995f, 0.0745f, 1.0f );
    Spheres[10].MDiffuse = vmath::vec4(  0.75164f, 0.555802f, 0.366065f, 1.0f );
    Spheres[10].MSpecular = vmath::vec4( 0.628281f, 0.555802f, 0.366065f, 1.0f );
    Spheres[10].spherePosition = vmath::vec3( 0.0f, 0.0f, -3.0f );
    Spheres[10].SViewport[0] = gWidth / 6;
    Spheres[10].SViewport[1] = 0;
    Spheres[10].SViewport[2] = gWidth / 2;
    Spheres[10].SViewport[3] = gHeight / 2;
    Spheres[10].sphereShininess = 0.4f * 128.0f;
    // SILVER
    Spheres[11].MAmbient = vmath::vec4(  0.19225f, 0.19225f, 0.19225f, 1.0f );
    Spheres[11].MDiffuse = vmath::vec4(  0.50754f, 0.50754f, 0.50754f, 1.0f );
    Spheres[11].MSpecular = vmath::vec4( 0.508273f, 0.508273f, 0.508273f, 1.0f );
    Spheres[11].spherePosition = vmath::vec3( 0.0f, 0.0f, -3.0f );
    Spheres[11].SViewport[0] = gWidth / 6;
    Spheres[11].SViewport[1] = -gHeight / 6;
    Spheres[11].SViewport[2] = gWidth / 2;
    Spheres[11].SViewport[3] = gHeight / 2;
    Spheres[11].sphereShininess = 0.4f * 128.0f;
    
    // **************** 3rd Column : Plastic **************************
    // BLACK PLASTIC
    Spheres[12].MAmbient = vmath::vec4( 0.0f, 0.0f, 0.0f, 1.0f );
    Spheres[12].MDiffuse = vmath::vec4( 0.01f, 0.01f, 0.01f, 1.0f );
    Spheres[12].MSpecular = vmath::vec4( 0.50f, 0.50f, 0.50f, 1.0f );
    Spheres[12].spherePosition = vmath::vec3( 0.0f, 0.0f, -3.0f );
    Spheres[12].SViewport[0] = gWidth / 3;
    Spheres[12].SViewport[1] = 4 * gHeight / 6;
    Spheres[12].SViewport[2] = gWidth / 2;
    Spheres[12].SViewport[3] = gHeight / 2;
    Spheres[12].sphereShininess = 0.25f * 128.0f;
    // CYAN PLASTIC
    Spheres[13].MAmbient = vmath::vec4( 0.0f, 0.1f, 0.06f, 1.0f );
    Spheres[13].MDiffuse = vmath::vec4( 0.0f, 0.50980392f, 0.50980392f, 1.0f );
    Spheres[13].MSpecular = vmath::vec4( 0.50196078f, 0.50196078f, 0.50196078f, 1.0f );
    Spheres[13].spherePosition = vmath::vec3( 0.0f, 0.0f, -3.0f );
    Spheres[13].SViewport[0] = gWidth / 3;
    Spheres[13].SViewport[1] = 3 * gHeight / 6;
    Spheres[13].SViewport[2] = gWidth / 2;
    Spheres[13].SViewport[3] = gHeight / 2;
    Spheres[13].sphereShininess = 0.25f * 128.0f;
    // GREEN PLASTIC
    Spheres[14].MAmbient = vmath::vec4( 0.0f, 0.0f, 0.0f, 1.0f );
    Spheres[14].MDiffuse = vmath::vec4( 0.1f, 0.35f, 0.1f, 1.0f );
    Spheres[14].MSpecular = vmath::vec4( 0.45f, 0.55f, 0.45f, 1.0f );
    Spheres[14].spherePosition  = vmath::vec3( 0.0f, 0.0f, -3.0f );
    Spheres[14].SViewport[0] = gWidth / 3;
    Spheres[14].SViewport[1] = 2 * gHeight / 6;
    Spheres[14].SViewport[2] = gWidth / 2;
    Spheres[14].SViewport[3] = gHeight / 2;
    Spheres[14].sphereShininess = 0.25f * 128.0f;
    // RED PLASTIC
    Spheres[15].MAmbient = vmath::vec4( 0.0f, 0.0f, 0.0f, 1.0f );
    Spheres[15].MDiffuse = vmath::vec4( 0.5f, 0.0f, 0.0f, 1.0f );
    Spheres[15].MSpecular = vmath::vec4( 0.7f, 0.6f, 0.6f, 1.0f );
    Spheres[15].spherePosition = vmath::vec3( 0.0f, 0.0f, -3.0f );
    Spheres[15].SViewport[0] = gWidth / 3;
    Spheres[15].SViewport[1] = gHeight / 6;
    Spheres[15].SViewport[2] = gWidth / 2;
    Spheres[15].SViewport[3] = gHeight / 2;
    Spheres[15].sphereShininess = 0.25f * 128.0f;
    // WHITE PLASTIC
    Spheres[16].MAmbient = vmath::vec4( 0.0f, 0.0f, 0.0f, 1.0f );
    Spheres[16].MDiffuse = vmath::vec4( 0.55f, 0.55f, 0.55f, 1.0f );
    Spheres[16].MSpecular = vmath::vec4( 0.70f, 0.70f, 0.70f, 1.0f );
    Spheres[16].spherePosition = vmath::vec3( 0.0f, 0.0f, -3.0f);
    Spheres[16].SViewport[0] = gWidth / 3;
    Spheres[16].SViewport[1] = 0;
    Spheres[16].SViewport[2] = gWidth / 2;
    Spheres[16].SViewport[3] = gHeight / 2;
    Spheres[16].sphereShininess = 0.25f * 128.0f;
    // YELLOW PLASTIC
    Spheres[17].MAmbient = vmath::vec4( 0.0f, 0.0f, 0.0f, 1.0f );
    Spheres[17].MDiffuse = vmath::vec4( 0.5f, 0.5f, 0.0f, 1.0f );
    Spheres[17].MSpecular = vmath::vec4( 0.60f, 0.60f, 0.50f, 1.0f );
    Spheres[17].spherePosition = vmath::vec3(0.0f, 0.0f, -3.0f );
    Spheres[17].SViewport[0] = gWidth / 3;
    Spheres[17].SViewport[1] = -gHeight/6;
    Spheres[17].SViewport[2] = gWidth / 2;
    Spheres[17].SViewport[3] = gHeight / 2;
    Spheres[17].sphereShininess = 0.25f * 128.0f;
    
    // **************** 4th Column : Rubber **************************
    // BLACK RUBBER
    Spheres[18].MAmbient = vmath::vec4( 0.02f, 0.02f, 0.02f, 1.0f );
    Spheres[18].MDiffuse = vmath::vec4( 0.01f, 0.01f, 0.01f, 1.0f );
    Spheres[18].MSpecular = vmath::vec4( 0.4f, 0.40f, 0.40f, 1.0f );
    Spheres[18].spherePosition = vmath::vec3( 0.0f, 0.0f, -3.0f );
    Spheres[18].SViewport[0] = gWidth / 2;
    Spheres[18].SViewport[1] = 4 * gHeight / 6;
    Spheres[18].SViewport[2] = gWidth / 2;
    Spheres[18].SViewport[3] = gHeight / 2;
    Spheres[18].sphereShininess = 0.078125f * 128.0f;
    // CYAN RUBBER
    Spheres[19].MAmbient = vmath::vec4( 0.0f, 0.05f, 0.05f, 1.0f );
    Spheres[19].MDiffuse = vmath::vec4( 0.40f, 0.50f, 0.50f, 1.0f );
    Spheres[19].MSpecular = vmath::vec4( 0.04f, 0.7f, 0.7f, 1.0f );
    Spheres[19].spherePosition = vmath::vec3( 0.0f, 0.0f, -3.0f );
    Spheres[19].SViewport[0] = gWidth / 2;
    Spheres[19].SViewport[1] = 3 * gHeight / 6;
    Spheres[19].SViewport[2] = gWidth / 2;
    Spheres[19].SViewport[3] = gHeight / 2;
    Spheres[19].sphereShininess = 0.078125f * 128.0f;
    // GREEN RUBBER
    Spheres[20].MAmbient = vmath::vec4( 0.0f, 0.05f, 0.0f, 1.0f );
    Spheres[20].MDiffuse = vmath::vec4( 0.4f, 0.5f, 0.4f, 1.0f );
    Spheres[20].MSpecular = vmath::vec4( 0.04f, 0.7f, 0.04f, 1.0f );
    Spheres[20].spherePosition = vmath::vec3( 0.0f, 0.0f, -3.0f );
    Spheres[20].SViewport[0] = gWidth / 2;
    Spheres[20].SViewport[1] = 2 * gHeight / 6;
    Spheres[20].SViewport[2] = gWidth / 2;
    Spheres[20].SViewport[3] = gHeight / 2;
    Spheres[20].sphereShininess = 0.078125f * 128.0f;
    // RED RUBBER
    Spheres[21].MAmbient = vmath::vec4( 0.05f, 0.0f, 0.0f, 1.0f );
    Spheres[21].MDiffuse = vmath::vec4( 0.5f, 0.4f, 0.4f, 1.0f );
    Spheres[21].MSpecular = vmath::vec4( 0.7f, 0.04f, 0.04f, 1.0f );
    Spheres[21].spherePosition = vmath::vec3( 0.0f, 0.0f, -3.0f );
    Spheres[21].SViewport[0] = gWidth / 2;
    Spheres[21].SViewport[1] = gHeight / 6;
    Spheres[21].SViewport[2] = gWidth / 2;
    Spheres[21].SViewport[3] = gHeight / 2;
    Spheres[21].sphereShininess = 0.078125f * 128.0f;
    // WHITE RUBBER
    Spheres[22].MAmbient = vmath::vec4( 0.05f, 0.05f, 0.05f, 1.0f );
    Spheres[22].MDiffuse = vmath::vec4( 0.5f, 0.5f, 0.5f, 1.0f );
    Spheres[22].MSpecular = vmath::vec4( 0.70f, 0.70f, 0.70f, 1.0f );
    Spheres[22].spherePosition = vmath::vec3( 0.0f, 0.0f, -3.0f );
    Spheres[22].SViewport[0] = gWidth / 2;
    Spheres[22].SViewport[1] = 0;
    Spheres[22].SViewport[2] = gWidth / 2;
    Spheres[22].SViewport[3] = gHeight / 2;
    Spheres[22].sphereShininess = 0.078125f * 128.0f;
    // YELLOW RUBBER
    Spheres[23].MAmbient = vmath::vec4( 0.05f, 0.05f, 0.0f, 1.0f );
    Spheres[23].MDiffuse = vmath::vec4( 0.50f, 0.50f, 0.40f, 1.0f );
    Spheres[23].MSpecular = vmath::vec4( 0.70f, 0.70f, 0.04f, 1.0f );
    Spheres[23].spherePosition = vmath::vec3( 0.0f, 0.0f, -3.0f );
    Spheres[23].SViewport[0] = gWidth / 2;
    Spheres[23].SViewport[1] = -gHeight / 6;
    Spheres[23].SViewport[2] = gWidth / 2;
    Spheres[23].SViewport[3] = gHeight / 2;
    Spheres[23].sphereShininess = 0.078125f * 128.0f;

    // perspective projection matrix
    perspectiveProjectionMatrix = vmath::perspective(45.0f, width/height, 0.1f, 100.0f);

    CGLUnlockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);
    
    [super reshape];
}

-(void)drawRect:(NSRect)dirtyRect
{
    //code
    [self drawView];
}

-(void)drawView
{
    // code
    [[self openGLContext]makeCurrentContext];

    CGLLockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);

    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    vmath::mat4 modelMatrix = vmath::mat4::identity();
    vmath::mat4 viewMatrix = vmath::mat4::identity();
    vmath::mat4 projectionMatrix = vmath::mat4::identity();
    vmath::mat4 translationMatrix = vmath::mat4::identity();
	vmath::mat4 rotationMatrix = vmath::mat4::identity();
	vmath::mat4 scaleMatrix = vmath::mat4::identity();

    
    for(int i = 0; i< 24; i++)
    {
        vmath::mat4 modelMatrix = vmath::mat4::identity();
        vmath::mat4 viewMatrix = vmath::mat4::identity();
        vmath::mat4 projectionMatrix = vmath::mat4::identity();
        vmath::mat4 translationMatrix = vmath::mat4::identity();
        vmath::mat4 rotationMatrix = vmath::mat4::identity();
        vmath::mat4 scaleMatrix = vmath::mat4::identity();
        
        scaleMatrix = vmath::scale(0.5f,0.5f,0.5f);
        translationMatrix =  vmath::translate(Spheres[i].spherePosition);
        modelMatrix = translationMatrix * scaleMatrix * rotationMatrix;
        projectionMatrix = projectionMatrix * perspectiveProjectionMatrix;
        
    if(toggleLightingType == NO)
    {
        glUseProgram(shaderProgramObject_v);
        
        if (enableLighting == YES)
        {
            if (axis == 3)
            {
                lightPosition[0] = 15 *cosf(theta);
                lightPosition[1] = 15 * sinf(theta);
                lightPosition[2] = 0.0f;
            }
            if (axis == 2)
            {
                lightPosition[0] = 15 * cosf(theta);
                lightPosition[1] = 0.0f;
                lightPosition[2] = 15 * sinf(theta);
            }
            if (axis == 1)
            {
                lightPosition[0] = 0.0f;
                lightPosition[1] = 15 * cosf(theta);
                lightPosition[2] = 15 * sinf(theta);
            }
            
            glUniform1i(keyPressedUniform_v, 1);
            glUniform3fv(LaUniform_v, 1, lightAmbient);
            glUniform3fv(LdUniform_v, 1, lightDiffuse);
            glUniform3fv(LsUniform_v, 1, lightSpecular);
        
            glUniform3fv(KaUniform_v, 1, Spheres[i].MAmbient);
            glUniform3fv(KdUniform_v, 1, Spheres[i].MDiffuse);
            glUniform3fv(KsUniform_v, 1, Spheres[i].MSpecular);
        
            glUniform1f(materialShininessUniform_v, Spheres[i].sphereShininess);
        
            glUniform4fv(lightPositionUniform_v, 1, lightPosition);
        }
        else
        {
            glUniform1i(keyPressedUniform_v, 0);
        }
        //send necessary matrices to shader in respective uniforms
        glUniformMatrix4fv(mUniform_v, 1, GL_FALSE, modelMatrix);
        glUniformMatrix4fv(vUniform_v, 1, GL_FALSE, viewMatrix);
        glUniformMatrix4fv(pUniform_v, 1, GL_FALSE, projectionMatrix);

        //set viewport
         glViewport((GLint)Spheres[i].SViewport[0], (GLint)Spheres[i].SViewport[1], (GLsizei)Spheres[i].SViewport[2], (GLsizei)Spheres[i].SViewport[3]);
        
        // draw sphere
        glBindVertexArray(vao_sphere);
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_elements);
        glDrawElements(GL_TRIANGLES, iNumElements, GL_UNSIGNED_SHORT, 0);
        glBindVertexArray(0);
    
        glUseProgram(0);
    }
    else
    {
        glUseProgram(shaderProgramObject_f);
        
        if (enableLighting == YES)
        {
            // calculate position of rotating lights
            if (axis == 3)    // z
            {
                lightPosition[0] = 15 * cosf(theta);
                lightPosition[1] = 15 * sinf(theta);
                lightPosition[2] = 0.0f;
            }
            if (axis == 2)    // y
            {
                lightPosition[0] = 15 * cosf(theta);
                lightPosition[1] = 0.0f;
                lightPosition[2] = 15 * sinf(theta);
            }
            if (axis == 1)    // x
            {
                lightPosition[0] = 0.0f;
                lightPosition[1] = 15 * cosf(theta);
                lightPosition[2] = 15 * sinf(theta);
            }
            
            glUniform1i(keyPressedUniform_f, 1);
            glUniform3fv(LaUniform_f, 1, lightAmbient);
            glUniform3fv(LdUniform_f, 1, lightDiffuse);
            glUniform3fv(LsUniform_f, 1, lightSpecular);
            
            glUniform3fv(KaUniform_f, 1, Spheres[i].MAmbient);
            glUniform3fv(KdUniform_f, 1, Spheres[i].MDiffuse);
            glUniform3fv(KsUniform_f, 1, Spheres[i].MSpecular);
            
            glUniform1f(materialShininessUniform_f, Spheres[i].sphereShininess);
            
            glUniform4fv(lightPositionUniform_f, 1, lightPosition);
        }
        else
        {
            glUniform1i(keyPressedUniform_f, 0);
        }
        //send necessary matrices to shader in respective uniforms
        glUniformMatrix4fv(mUniform_f, 1, GL_FALSE, modelMatrix);
        glUniformMatrix4fv(vUniform_f, 1, GL_FALSE, viewMatrix);
        glUniformMatrix4fv(pUniform_f, 1, GL_FALSE, projectionMatrix);
        
        //set viewport
        glViewport((GLint)Spheres[i].SViewport[0], (GLint)Spheres[i].SViewport[1], (GLsizei)Spheres[i].SViewport[2], (GLsizei)Spheres[i].SViewport[3]);
        
        // draw sphere
        glBindVertexArray(vao_sphere);
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_elements);
        glDrawElements(GL_TRIANGLES, iNumElements, GL_UNSIGNED_SHORT, 0);
        glBindVertexArray(0);
        
        glUseProgram(0);
    }
    }
    
    theta = theta + 0.05;
    if(theta > 360.0f)
        theta = 0.0f;
    
    CGLFlushDrawable((CGLContextObj)[[self openGLContext]CGLContextObj]);
    CGLUnlockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);
    
   // [self update];
}

-(BOOL)acceptsFirstResponder
{
    //code
    [[self window]makeFirstResponder:self];
    return(YES);
}

-(void)keyDown:(NSEvent *)theEvent
{
    //code
    int key = (int)[[theEvent characters]characterAtIndex:0];
    switch(key)
    {
        case 27:
            [self release];
            [NSApp terminate:self];
            break;
        case 'F':
        case 'f':
            [[self window]toggleFullScreen:self];   //repainting occurs automatically
            break;
        
        case 'L':
        case 'l':
            if(enableLighting == NO)
                enableLighting = YES;
            else
                enableLighting = NO;
            fprintf(gpFile_ps, "l is pressed\n");
            break;
        case 'A':
        case 'a':
            fprintf(gpFile_ps, "a is pressed\n");
            if(startAnimation == NO)
                startAnimation = YES;
            else
                startAnimation = NO;
            break;
        case 't':
        case 'T':
            if(toggleLightingType == NO)
                toggleLightingType = YES;
            else
                toggleLightingType = NO;
            break;
            
        case 'x':
        case 'X':
            axis = 1;
            theta = 0.0f;
            lightPosition[0] = 0.0f;
            lightPosition[1] = 0.0f;
            lightPosition[2] = 0.0f;
            break;
        case 'y':
        case 'Y':
            axis = 2;
            theta = 0.0f;
            lightPosition[0] = 0.0f;
            lightPosition[1] = 0.0f;
            lightPosition[2] = 0.0f;
            break;
        case 'z':
        case 'Z':
            axis = 3;
            theta = 0.0f;
            lightPosition[0] = 0.0f;
            lightPosition[1] = 0.0f;
            lightPosition[2] = 0.0f;
            break;
            
        default:
            break;
    }
}

-(void)mouseDown:(NSEvent *)theEvent
{
    // code
}

-(void)mouseDragged:(NSEvent *)theEvent
{
    // code
}

-(void)rightMouseDown:(NSEvent *)theEvent
{
    // code
}

-(void)dealloc
{
    // code
    // destroy vao
   	if (vbo_sphere_elements)
	{
		glDeleteBuffers(1, &vbo_sphere_elements);
		vbo_sphere_elements = 0;
	}
	if (vbo_sphere_normals)
	{
		glDeleteBuffers(1, &vbo_sphere_normals);
		vbo_sphere_normals = 0;
	}
    if (vbo_sphere_position)
    {
        glDeleteBuffers(1, &vbo_sphere_position);
        vbo_sphere_position = 0;
    }
	if (vao_sphere)
	{
		glDeleteVertexArrays(1, &vao_sphere);
		vao_sphere = 0;
	}

    //detach shaders
    glDetachShader(shaderProgramObject_v, vertexShaderObject_v);
    glDetachShader(shaderProgramObject_v, fragmentShaderObject_v);

    //delete shader objects
    glDeleteShader(vertexShaderObject_v);
    vertexShaderObject_v=0;
    glDeleteShader(fragmentShaderObject_v);
    fragmentShaderObject_v = 0;

    // delete program object
    glDeleteProgram(shaderProgramObject_v);
    shaderProgramObject_v = 0;

    CVDisplayLinkStop(displayLink);
    CVDisplayLinkRelease(displayLink);

    [super dealloc];
}
@end

CVReturn MyDisplayLinkCallback(CVDisplayLinkRef displayLink, 
                                const CVTimeStamp *pNow,
                                const CVTimeStamp *pOutputTime,
                                CVOptionFlags flagsIn,
                                CVOptionFlags *pFlagsOut,
                                void *pDisplayLinkcontext)
{
    CVReturn result = [(GLView *)pDisplayLinkcontext getFrameForTime:pOutputTime];
    return(result);    
}
