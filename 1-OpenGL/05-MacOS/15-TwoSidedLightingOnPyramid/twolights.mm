//  Headers
#import <Foundation/Foundation.h>
#import <Cocoa/Cocoa.h>
#import <QuartzCore/CVDisplayLink.h>
#import <OpenGL/gl3.h>
#import <OpenGL/gl3ext.h>

#import "vmath.h"

enum
{
    PS_ATTRIBUTE_POSITION = 0,
    PS_ATTRIBUTE_COLOR,
    PS_ATTRIBUTE_NORMAL,
    PS_ATTRIBUTE_TEXTURE0,
};

//  C Style Global Function Declarations
CVReturn MyDisplayLinkCallback(CVDisplayLinkRef, const CVTimeStamp *, const CVTimeStamp *, CVOptionFlags, CVOptionFlags *, void *);

// Global Variables
FILE *gpFile_ps = NULL;
// light and material properties values
float lightDiffuse_blue[4]    = {0.0f, 0.0f, 1.0f, 1.0f};
float lightAmbient_blue[4]    = {0.0f, 0.0f, 0.0f, 0.0f};
float lightSpecular_blue[4]    = {1.0f, 1.0f, 1.0f, 1.0f};
float lightPosition_blue[4] = {-2.0f, 0.0f, 0.0f, 1.0f};

float lightDiffuse_red[4]    = {1.0f, 0.0f, 0.0f, 1.0f};
float lightAmbient_red[4]    = {0.0f, 0.0f, 0.0f, 0.0f};
float lightSpecular_red[4]     = {1.0f, 1.0f, 1.0f, 1.0f};
float lightPosition_red[4]     = {2.0f, 0.0f, 0.0f, 1.0f};

float MaterialDiffuse[4]     = {1.0f, 1.0f, 1.0f, 1.0f};
float MaterialAmbient[4]    = {0.0f, 0.0f, 0.0f, 0.0f};
float MaterialSpecular[4]    = {1.0f, 1.0f, 1.0f, 1.0f};
float materialShininess = 128.0f;

BOOL startAnimation = NO;
BOOL enableLighting = NO;

//  Interface Declarations
@interface AppDelegate:NSObject <NSApplicationDelegate, NSWindowDelegate>
@end

@interface GLView:NSOpenGLView
@end

//  Entry Point Function
int main(int argc, const char *argv[])
{
    // code
    NSAutoreleasePool *pPool = [[NSAutoreleasePool alloc]init];

    NSApp =[NSApplication sharedApplication];

    [NSApp setDelegate:[[AppDelegate alloc]init]];

    [NSApp run];

    [pPool release];

    return(0);
}

//  Interface Implementations
@implementation AppDelegate
{
    @private
        NSWindow *window;
        GLView *glView;
}

-(void)applicationDidFinishLaunching:(NSNotification *)aNotification
{
    // code
    // logFile
    NSBundle *mainBundle=[NSBundle mainBundle];
    NSString *appDirName=[mainBundle bundlePath];
    NSString *parentDirPath=[appDirName stringByDeletingLastPathComponent];
    NSString *logFileNameWithPath=[NSString stringWithFormat:@"%@/Log.txt", parentDirPath];
    const char *pszLogFileNameWithPath=[logFileNameWithPath cStringUsingEncoding:NSASCIIStringEncoding];

    gpFile_ps = fopen(pszLogFileNameWithPath, "w");
    if(gpFile_ps == NULL)
    {
        printf("Can not create log file.\nExitting...\n");
        [self release];
        [NSApp terminate:self];
    }
    fprintf(gpFile_ps, "Program Is Started Successfully\n");
    
    //window
    NSRect win_rect;
    win_rect=NSMakeRect(0.0,0.0,800.0,600.0);

    //create simple window
    window=[[NSWindow alloc]initWithContentRect:win_rect
    styleMask:NSWindowStyleMaskTitled | NSWindowStyleMaskClosable | NSWindowStyleMaskMiniaturizable | NSWindowStyleMaskResizable
      backing:NSBackingStoreBuffered
        defer:NO];
    
    [window setTitle:@"MacOS OpenGL Window"];
    [window center];

    glView=[[GLView alloc]initWithFrame:win_rect];

    [window setContentView:glView];
    [window setDelegate:self];
    [window makeKeyAndOrderFront:self];
}

-(void)applicationWillTerminate:(NSNotification *)notification
{
    //code
    fprintf(gpFile_ps, "Program Is Terminated Successfully\n");

    if(gpFile_ps)
    {
        fclose(gpFile_ps);
        gpFile_ps = NULL;
    }
}

-(void)windowWillClose:(NSNotification *)notification
{
    // code
    [NSApp terminate:self];
}

-(void)dealloc
{
    //code
    [glView release];

    [window release];

    [super dealloc];
}
@end

@implementation GLView
{
     @private
        CVDisplayLinkRef displayLink;

        GLuint vertexShaderObject;                
        GLuint fragmentShaderObject;
        GLuint shaderProgramObject;

        GLuint vao_pyramid;
        GLuint vbo_normal_pyramid;
        GLuint vbo_position_pyramid;

        GLuint modelUniform_v, viewUniform_v, projectionUniform_v;
        GLuint blue_ldUniform_v;
        GLuint blue_laUniform_v;
        GLuint blue_lsUniform_v;
        GLuint blue_lightPositionUniform_v;
    
        GLuint red_ldUniform_v;
        GLuint red_laUniform_v;
        GLuint red_lsUniform_v;
        GLuint red_lightPositionUniform_v;
    
        GLuint material_kdUniform_v;
        GLuint material_kaUniform_v;
        GLuint material_ksUniform_v;
        GLuint material_shininessUniform_v;
        GLuint keyPressedUniform_v;

    
        vmath::mat4 perspectiveProjectionMatrix;
}

-(id)initWithFrame:(NSRect)frame;
{
    // code
    self=[super initWithFrame:frame];

    if(self)
    {
        [[self window]setContentView:self];

        NSOpenGLPixelFormatAttribute attrs[] =
        {
            // must specify the 4.1 core Profile to use OpenGL 4.1
            NSOpenGLPFAOpenGLProfile, NSOpenGLProfileVersion4_1Core,
            //specify the display ID to associate the GL Context with(main display for now)
            NSOpenGLPFAScreenMask, CGDisplayIDToOpenGLDisplayMask(kCGDirectMainDisplay),
            NSOpenGLPFANoRecovery,
            NSOpenGLPFAAccelerated,
            NSOpenGLPFAColorSize, 24,
            NSOpenGLPFADepthSize, 24,
            NSOpenGLPFAAlphaSize, 8,
            NSOpenGLPFADoubleBuffer,
            0};

        NSOpenGLPixelFormat *pixelFormat=[[[NSOpenGLPixelFormat alloc]initWithAttributes:attrs] autorelease];

        if(pixelFormat == nil)
        {
            fprintf(gpFile_ps, "No Valid OpenGLPixel Format Is Available.\nExitting...\n");
            [self release];
            [NSApp terminate:self];
        }

        NSOpenGLContext *glContext=[[[NSOpenGLContext alloc]initWithFormat:pixelFormat shareContext:nil]autorelease];

        [self setPixelFormat:pixelFormat];

        [self setOpenGLContext:glContext];  // it automatically releases the older context, if present, and sets the newer one
    }
    return(self);
}

-(CVReturn)getFrameForTime:(const CVTimeStamp *)pOutputTime
{
    // code
    NSAutoreleasePool *pool = [[NSAutoreleasePool alloc]init];

    [self drawView];

    [pool release];
    return(kCVReturnSuccess);
}

-(void)prepareOpenGL
{
    // code
    // OpenGL Info
    fprintf(gpFile_ps, "OpenGL Version :   %s\n", glGetString(GL_VERSION));
    fprintf(gpFile_ps, "GLSL Version   :   %s\n", glGetString(GL_SHADING_LANGUAGE_VERSION));

    [[self openGLContext]makeCurrentContext];

    GLint swapInt = 1; // matching with OS's swaping interval default is 1, frame gap
    [[self openGLContext]setValues:&swapInt forParameter:NSOpenGLCPSwapInterval];

    /*      ***** VERTEX SHADER *****       */
    //create shader
    vertexShaderObject = glCreateShader(GL_VERTEX_SHADER);

    // source code for vertex shader
    const GLchar *vertexShaderSourceCode =
		"   #version 410                                    " \
		"                                               \n  " \
    "    in vec4 vPosition;                                                                         " \
    "    in vec3 vNormal;                                                                           " \
    "                                                                                            \n " \
    "    out vec3 phong_ads_light;                                                                  " \
    "                                                                                            \n " \
    "    uniform mat4 u_m_matrix;                                                                   " \
    "    uniform mat4 u_v_matrix;                                                                   " \
    "    uniform mat4 u_p_matrix;                                                                   " \
    "    uniform int u_l_key_is_pressed;                                                            " \
    "    uniform vec3 u_blue_ld;                                                                    " \
    "    uniform vec3 u_blue_la;                                                                    " \
    "    uniform vec3 u_blue_ls;                                                                    " \
    "    uniform vec3 u_red_ld;                                                                     " \
    "    uniform vec3 u_red_la;                                                                     " \
    "    uniform vec3 u_red_ls;                                                                     " \
    "    uniform vec3 u_ka;                                                                         " \
    "    uniform vec3 u_kd;                                                                         " \
    "    uniform vec3 u_ks;                                                                         " \
    "    uniform vec4 u_blue_light_position;                                                        " \
    "    uniform vec4 u_red_light_position;                                                         " \
    "    uniform float u_material_shininess;                                                        " \
    "                                                                                            \n " \
    "    void main(void)                                                                            " \
    "    {                                                                                          " \
    "        if(u_l_key_is_pressed == 1)                                                            " \
    "        {                                                                                      " \
    "            vec4 eyeCoordinates = u_v_matrix * u_m_matrix * vPosition;                         " \
    "            vec3 tNorm            = normalize(mat3(u_v_matrix * u_m_matrix) * vNormal);        " \
    "            vec3 viewerVector   = normalize(vec3(-eyeCoordinates));                            " \
    "                                                                                            \n " \
    "            vec3 blueLightDirection = normalize(vec3(u_blue_light_position - eyeCoordinates)); " \
    "            vec3 blueLightReflectionVector = reflect(-blueLightDirection, tNorm);              " \
    "            float bluetnDotld = max(dot(tNorm, blueLightDirection), 0.0);                      " \
    "            float bluervDotvv = max(dot(viewerVector, blueLightReflectionVector), 0.0);        " \
    "                                                                                            \n " \
    "            vec3 redLightDirection  = normalize(vec3(u_red_light_position - eyeCoordinates));  " \
    "            vec3 redLightReflectionVector = reflect(-redLightDirection, tNorm);                " \
    "            float redtnDotld = max(dot(tNorm, redLightDirection), 0.0);                        " \
    "            float redrvDotvv = max(dot(viewerVector, redLightReflectionVector), 0.0);          " \
    "                                                                                            \n " \
    "            vec3 blueAmbient  = u_blue_la * u_ka;                                              " \
    "            vec3 blueDiffuse  = u_blue_ld * u_kd * bluetnDotld;                                " \
    "            vec3 blueSpecular = u_blue_ls * u_ks * pow(bluervDotvv, u_material_shininess);     " \
    "                                                                                            \n " \
    "            vec3 redAmbient  = u_red_la * u_ka;                                                " \
    "            vec3 redDiffuse  = u_red_ld * u_kd * redtnDotld;                                   " \
    "            vec3 redSpecular = u_red_ls * u_ks * pow(redrvDotvv, u_material_shininess);        " \
    "                                                                                            \n " \
    "            vec3 Ambient  = blueAmbient + redAmbient;                                          " \
    "            vec3 Diffuse  = blueDiffuse + redDiffuse;                                          " \
    "            vec3 Specular = blueSpecular + redSpecular;                                        " \
    "            phong_ads_light = Ambient + Diffuse + Specular;                                    " \
    "        }                                                                                      " \
    "        else                                                                                   " \
    "        {                                                                                      " \
    "            phong_ads_light = vec3(1.0, 1.0, 1.0);                                             " \
    "        }                                                                                      " \
    "        gl_Position = u_v_matrix * u_p_matrix * u_m_matrix * vPosition;                        " \
    "    }                                                                                          ";
    
    glShaderSource(vertexShaderObject, 1, (const GLchar**)&vertexShaderSourceCode, NULL);

    // compile shader
    GLint iInfoLogLength =0;
    GLint iShaderCompileStatus = 0;
    char *szInfoLog = NULL;

    glCompileShader(vertexShaderObject);
	glGetShaderiv(vertexShaderObject, GL_COMPILE_STATUS, &iShaderCompileStatus);
	if (iShaderCompileStatus == GL_FALSE)
	{
		glGetShaderiv(vertexShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (GLchar *)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(vertexShaderObject, iInfoLogLength, &written, szInfoLog);
				fprintf(gpFile_ps, "VERTEX SHADER COMPILATION LOG : %s\n", szInfoLog);
				free(szInfoLog);
                [self release];
                [NSApp terminate:self];
            }
		}
	}

    /*      ***** FRAGMENT SHADER *****     */
    //create shader
    fragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);

    // source code for fragment shader
    const GLchar* fragmentShaderSourceCode =
    "   #version 410                                            " \
    "                                                       \n  " \
    "    in vec3 phong_ads_light;                               " \
    "                                                       \n  " \
    "    out vec4 FragColor;                                    " \
    "                                                       \n  " \
    "    void main(void)                                        " \
    "    {                                                      " \
    "        FragColor = vec4(phong_ads_light,1.0);             " \
    "    }                                                      ";

     glShaderSource(fragmentShaderObject, 1, (const GLchar**)&fragmentShaderSourceCode, NULL);

    // compile shader
    iInfoLogLength =0;
    iShaderCompileStatus = 0;
    szInfoLog = NULL;

    glCompileShader(fragmentShaderObject);
	glGetShaderiv(fragmentShaderObject, GL_COMPILE_STATUS, &iShaderCompileStatus);
	if (iShaderCompileStatus == GL_FALSE)
	{
		glGetShaderiv(fragmentShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (GLchar *)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(fragmentShaderObject, iInfoLogLength, &written, szInfoLog);
				fprintf(gpFile_ps, "FRAGMENT SHADER COMPILATION LOG : %s\n", szInfoLog);
				free(szInfoLog);
                [self release];
                [NSApp terminate:self];
            }
		}
	}

    /*      ***** SHADER PROGRAM *****      */
    //create program object
    shaderProgramObject = glCreateProgram();

    //attach shaders to it
    glAttachShader(shaderProgramObject, vertexShaderObject);
    glAttachShader(shaderProgramObject, fragmentShaderObject);

    // pre-linking binding of shader program object with vertex shader position attribute
    //(not required when used layout qualifier with location in shader, writing here for legacy of assignments)
    glBindAttribLocation(shaderProgramObject, PS_ATTRIBUTE_POSITION, "vPosition");
    glBindAttribLocation(shaderProgramObject, PS_ATTRIBUTE_NORMAL, "vNormal");

    // link shader program object
	glLinkProgram(shaderProgramObject);

	//error checking
	GLint iProgramLinkStatus = 0;
	iInfoLogLength = 0;
	szInfoLog = NULL;

	glGetProgramiv(shaderProgramObject, GL_LINK_STATUS, &iProgramLinkStatus);
	if (iProgramLinkStatus == GL_FALSE)
	{
		glGetProgramiv(shaderProgramObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (GLchar *)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLint written;
				glGetProgramInfoLog(shaderProgramObject, iInfoLogLength, &written, szInfoLog);
				fprintf(gpFile_ps, "SHADER PROGRAM LINK LOG : %s\n", szInfoLog);
				free(szInfoLog);
				[self release];
                [NSApp terminate:self];
			}
		}
	}

    // get uniform location
    // model, view and projection matrices
    viewUniform_v = glGetUniformLocation(shaderProgramObject, "u_v_matrix");
    modelUniform_v = glGetUniformLocation(shaderProgramObject, "u_m_matrix");
    projectionUniform_v = glGetUniformLocation(shaderProgramObject, "u_p_matrix");
    // blue light uniforms
    blue_ldUniform_v = glGetUniformLocation(shaderProgramObject, "u_blue_ld");
    blue_laUniform_v = glGetUniformLocation(shaderProgramObject, "u_blue_la");
    blue_lsUniform_v = glGetUniformLocation(shaderProgramObject, "u_blue_ls");
    blue_lightPositionUniform_v = glGetUniformLocation(shaderProgramObject, "u_blue_light_position");
    // red light uniforms
    red_ldUniform_v = glGetUniformLocation(shaderProgramObject, "u_red_ld");
    red_laUniform_v = glGetUniformLocation(shaderProgramObject, "u_red_la");
    red_lsUniform_v = glGetUniformLocation(shaderProgramObject, "u_red_ls");
    red_lightPositionUniform_v = glGetUniformLocation(shaderProgramObject, "u_red_light_position");
    // material uniform
    material_kdUniform_v = glGetUniformLocation(shaderProgramObject, "u_kd");
    material_kaUniform_v = glGetUniformLocation(shaderProgramObject, "u_ka");
    material_ksUniform_v = glGetUniformLocation(shaderProgramObject, "u_ks");
    material_shininessUniform_v = glGetUniformLocation(shaderProgramObject, "u_material_shininess");
    // key pressed uniform
    keyPressedUniform_v = glGetUniformLocation(shaderProgramObject, "u_l_key_is_pressed");
    

    // vertices, color, shader attribs, vbo, vao initialization
    const GLfloat pyramidVertices[] = {
		0.0f, 1.0f, 0.0f, -1.0f, -1.0f, 1.0f, 1.0f, -1.0f, 1.0f,
		0.0f, 1.0f, 0.0f, 1.0f, -1.0f, 1.0f, 1.0f, -1.0f, -1.0f,
		0.0f, 1.0f, 0.0f, 1.0f, -1.0f, -1.0f, -1.0f, -1.0f, -1.0f,
		0.0f, 1.0f, 0.0f, -1.0f, -1.0f, -1.0f, -1.0f, -1.0f, 1.0f
	};//front-right-back-left

	const GLfloat pyramidNormal[] = {
        0.0f, 0.447214f, 0.894427f, 0.0f, 0.447214f, 0.894427f, 0.0f, 0.447214f, 0.894427f,
        0.894427f, 0.447214f, 0.0f, 0.894427f, 0.447214f, 0.0f, 0.894427f, 0.447214f, 0.0f,
        0.0f, 0.447214f, -0.894427f, 0.0f, 0.447214f, -0.894427f, 0.0f, 0.447214f, -0.894427f,
        -0.894427f, 0.447214f, 0.0f, -0.894427f, 0.447214f, 0.0f, -0.894427f, 0.447214f, 0.0f
    };

   //	*** PYRAMID ***
	glGenVertexArrays(1, &vao_pyramid);
	glBindVertexArray(vao_pyramid);
	//buffer for position
	glGenBuffers(1, &vbo_position_pyramid);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_position_pyramid);
	glBufferData(GL_ARRAY_BUFFER, sizeof(pyramidVertices), pyramidVertices, GL_STATIC_DRAW);
	glVertexAttribPointer(PS_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(PS_ATTRIBUTE_POSITION);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	//buffer for color
	glGenBuffers(1, &vbo_normal_pyramid);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_normal_pyramid);
	glBufferData(GL_ARRAY_BUFFER, sizeof(pyramidNormal), pyramidNormal, GL_STATIC_DRAW);
	glVertexAttribPointer(PS_ATTRIBUTE_NORMAL, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(PS_ATTRIBUTE_NORMAL);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	glBindVertexArray(0); // unbinding with trianle vao
    
    glClearDepth(1.0f);
    glEnable(GL_DEPTH_TEST);
    glDepthFunc(GL_LEQUAL);

    glEnable(GL_CULL_FACE);

    // set background color
    glClearColor(0.0f, 0.0f, 0.0f, 1.0f);   

    // set projection matrix to identity
    perspectiveProjectionMatrix = vmath::mat4::identity();

    CVDisplayLinkCreateWithActiveCGDisplays(&displayLink);
    CVDisplayLinkSetOutputCallback(displayLink, &MyDisplayLinkCallback, self);
    CGLContextObj cglContext = (CGLContextObj)[[self openGLContext]CGLContextObj];
    CGLPixelFormatObj cglPixelFormat = (CGLPixelFormatObj)[[self pixelFormat]CGLPixelFormatObj];
    CVDisplayLinkSetCurrentCGDisplayFromOpenGLContext(displayLink, cglContext, cglPixelFormat);
    CVDisplayLinkStart(displayLink);
    
    [super prepareOpenGL];
}

-(void)reshape
{
    // code
    CGLLockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);

    NSRect rect = [self bounds];

    GLfloat width  = rect.size.width;
    GLfloat height = rect.size.height;

    if(height == 0)
        height =1;

    glViewport(0,0,(GLsizei)width, (GLsizei)height);

    // perspective projection matrix
    perspectiveProjectionMatrix = vmath::perspective(45.0f, width/height, 0.1f, 100.0f);


    CGLUnlockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);
    
    [super reshape];
}

-(void)drawRect:(NSRect)dirtyRect
{
    //code
    [self drawView];
}

-(void)drawView
{
    // code
    static GLfloat angle_pyramid = 0.0f;
    
    [[self openGLContext]makeCurrentContext];

    CGLLockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);

    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    vmath::mat4 modelMatrix = vmath::mat4::identity();
    vmath::mat4 viewMatrix = vmath::mat4::identity();
    vmath::mat4 projectionMatrix = vmath::mat4::identity();
    vmath::mat4 translationMatrix = vmath::mat4::identity();
	vmath::mat4 rotationMatrix = vmath::mat4::identity();
	vmath::mat4 scaleMatrix = vmath::mat4::identity();

    glUseProgram(shaderProgramObject);

    // **** TRIANGLE ****
    translationMatrix = vmath::translate(0.0f, 0.0f, -6.0f);
	rotationMatrix = vmath::rotate(angle_pyramid, 0.0f, 1.0f, 0.0f);
	modelMatrix = translationMatrix * rotationMatrix;
	projectionMatrix = perspectiveProjectionMatrix;

    if(enableLighting == YES)
    {
        //send necessary matrices to shader in respective uniforms
        glUniform1i(keyPressedUniform_v, 1);
        glUniform3fv(blue_ldUniform_v, 1, lightDiffuse_blue);
        glUniform3fv(blue_laUniform_v, 1, lightAmbient_blue);
        glUniform3fv(blue_lsUniform_v, 1, lightSpecular_blue);
        glUniform3fv(red_ldUniform_v, 1, lightDiffuse_red);
        glUniform3fv(red_laUniform_v, 1, lightAmbient_red);
        glUniform3fv(red_lsUniform_v, 1, lightSpecular_red);
        glUniform3fv(material_kdUniform_v, 1, MaterialDiffuse);
        glUniform3fv(material_kaUniform_v, 1, MaterialAmbient);
        glUniform3fv(material_ksUniform_v, 1, MaterialSpecular);
        glUniform1f(material_shininessUniform_v, materialShininess);
        glUniform4fv(blue_lightPositionUniform_v, 1, lightPosition_blue);
        glUniform4fv(red_lightPositionUniform_v, 1, lightPosition_red);
    }
    else
    {
        glUniform1i(keyPressedUniform_v, 0);
    }
    glUniformMatrix4fv(modelUniform_v, 1, GL_FALSE, modelMatrix);
    glUniformMatrix4fv(viewUniform_v, 1, GL_FALSE, viewMatrix);
    glUniformMatrix4fv(projectionUniform_v, 1, GL_FALSE, projectionMatrix);
    

    //bind to vao_pyrmid
	glBindVertexArray(vao_pyramid);
	glDrawArrays(GL_TRIANGLES, 0, 12);
	//unbind vao
	glBindVertexArray(0);
    glUseProgram(0);

    if(startAnimation == YES)
    {
        angle_pyramid = angle_pyramid + 0.5f;
        if (angle_pyramid > 360.0f)
        {
            angle_pyramid = 0.0;
        }
    }
    
    CGLFlushDrawable((CGLContextObj)[[self openGLContext]CGLContextObj]);
    CGLUnlockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);
}

-(BOOL)acceptsFirstResponder
{
    //code
    [[self window]makeFirstResponder:self];
    return(YES);
}

-(void)keyDown:(NSEvent *)theEvent
{
    //code
    int key = (int)[[theEvent characters]characterAtIndex:0];
    switch(key)
    {
        case 27:
            [self release];
            [NSApp terminate:self];
            break;
        case 'F':
        case 'f':
            [[self window]toggleFullScreen:self];   //repainting occurs automatically
            break;
        case 'l':
        case 'L':
            if(enableLighting == NO)
                enableLighting = YES;
            else
                enableLighting = NO;
            break;
        case 'a':
        case 'A':
            if(startAnimation == NO)
                startAnimation = YES;
            else
                startAnimation = NO;
            break;
        default:
            break;
    }
}

-(void)mouseDown:(NSEvent *)theEvent
{
    // code
}

-(void)mouseDragged:(NSEvent *)theEvent
{
    // code
}

-(void)rightMouseDown:(NSEvent *)theEvent
{
    // code
}

-(void)dealloc
{
	if (vbo_normal_pyramid)
	{
		glDeleteBuffers(1, &vbo_normal_pyramid);
		vbo_normal_pyramid = 0;
	}
	if (vbo_position_pyramid)
	{
		glDeleteBuffers(1, &vbo_position_pyramid);
		vbo_position_pyramid = 0;
	}
	if (vao_pyramid)
	{
		glDeleteVertexArrays(1, &vao_pyramid);
		vao_pyramid = 0;
	}

    //detach shaders
    glDetachShader(shaderProgramObject, vertexShaderObject);
    glDetachShader(shaderProgramObject, fragmentShaderObject);

    //delete shader objects
    glDeleteShader(vertexShaderObject);
    vertexShaderObject=0;
    glDeleteShader(fragmentShaderObject);
    fragmentShaderObject = 0;

    // delete program object
    glDeleteProgram(shaderProgramObject);
    shaderProgramObject = 0;

    CVDisplayLinkStop(displayLink);
    CVDisplayLinkRelease(displayLink);

    [super dealloc];
}
@end

CVReturn MyDisplayLinkCallback(CVDisplayLinkRef displayLink, 
                                const CVTimeStamp *pNow,
                                const CVTimeStamp *pOutputTime,
                                CVOptionFlags flagsIn,
                                CVOptionFlags *pFlagsOut,
                                void *pDisplayLinkcontext)
{
    CVReturn result = [(GLView *)pDisplayLinkcontext getFrameForTime:pOutputTime];
    return(result);    
}
