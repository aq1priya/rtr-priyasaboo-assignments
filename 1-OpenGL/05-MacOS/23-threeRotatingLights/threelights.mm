//  Headers
#import <Foundation/Foundation.h>
#import <Cocoa/Cocoa.h>
#import <QuartzCore/CVDisplayLink.h>
#import <OpenGL/gl3.h>
#import <OpenGL/gl3ext.h>

#import "vmath.h"
#import "ps_sphere.h"

enum
{
    PS_ATTRIBUTE_POSITION = 0,
    PS_ATTRIBUTE_COLOR,
    PS_ATTRIBUTE_NORMAL,
    PS_ATTRIBUTE_TEXTURE0,
};

//  C Style Global Function Declarations
CVReturn MyDisplayLinkCallback(CVDisplayLinkRef, const CVTimeStamp *, const CVTimeStamp *, CVOptionFlags, CVOptionFlags *, void *);

// Global Variables
FILE *gpFile_ps = NULL;
Sphere *mySphere;

//  Interface Declarations
@interface AppDelegate:NSObject <NSApplicationDelegate, NSWindowDelegate>
@end

@interface GLView:NSOpenGLView
@end

//  Entry Point Function
int main(int argc, const char *argv[])
{
    // code
    NSAutoreleasePool *pPool = [[NSAutoreleasePool alloc]init];

    NSApp =[NSApplication sharedApplication];

    [NSApp setDelegate:[[AppDelegate alloc]init]];

    [NSApp run];

    [pPool release];

    return(0);
}

//  Interface Implementations
@implementation AppDelegate
{
    @private
        NSWindow *window;
        GLView *glView;
}

-(void)applicationDidFinishLaunching:(NSNotification *)aNotification
{
    // code
    // logFile
    NSBundle *mainBundle=[NSBundle mainBundle];
    NSString *appDirName=[mainBundle bundlePath];
    NSString *parentDirPath=[appDirName stringByDeletingLastPathComponent];
    NSString *logFileNameWithPath=[NSString stringWithFormat:@"%@/Log.txt", parentDirPath];
    const char *pszLogFileNameWithPath=[logFileNameWithPath cStringUsingEncoding:NSASCIIStringEncoding];

    gpFile_ps = fopen(pszLogFileNameWithPath, "w");
    if(gpFile_ps == NULL)
    {
        printf("Can not create log file.\nExitting...\n");
        [self release];
        [NSApp terminate:self];
    }
    fprintf(gpFile_ps, "Program Is Started Successfully\n");
    
    //window
    NSRect win_rect;
    win_rect=NSMakeRect(0.0,0.0,800.0,600.0);

    //create simple window
    window=[[NSWindow alloc]initWithContentRect:win_rect
    styleMask:NSWindowStyleMaskTitled | NSWindowStyleMaskClosable | NSWindowStyleMaskMiniaturizable | NSWindowStyleMaskResizable
      backing:NSBackingStoreBuffered
        defer:NO];
    
    [window setTitle:@"MacOS OpenGL Window"];
    [window center];

    glView=[[GLView alloc]initWithFrame:win_rect];

    [window setContentView:glView];
    [window setDelegate:self];
    [window makeKeyAndOrderFront:self];
}

-(void)applicationWillTerminate:(NSNotification *)notification
{
    //code
    fprintf(gpFile_ps, "Program Is Terminated Successfully\n");

    if(gpFile_ps)
    {
        fclose(gpFile_ps);
        gpFile_ps = NULL;
    }
}

-(void)windowWillClose:(NSNotification *)notification
{
    // code
    [NSApp terminate:self];
}

-(void)dealloc
{
    //code
    [glView release];

    [window release];

    [super dealloc];
}
@end

@implementation GLView
{
     @private
    CVDisplayLinkRef displayLink;

    GLuint vertexShaderObject_v;
    GLuint fragmentShaderObject_v;
    GLuint shaderProgramObject_v;
    
    GLuint vertexShaderObject_f;
    GLuint fragmentShaderObject_f;
    GLuint shaderProgramObject_f;

    GLuint vao_sphere;
    GLuint vbo_sphere_elements;
    GLuint vbo_sphere_position;
    GLuint vbo_sphere_normals;
    
    float sphere_vertices[1146];
    float sphere_normals[1146];
    float sphere_textures[764];
    unsigned short sphere_elements[2280];
    
    int iNumVertices;
    int iNumElements;
    
    GLfloat angle_sphere;
    
    // uniform variables for per vertex lighting shader object
    GLuint modelUniform_v, viewUniform_v, projectionUniform_v;
    GLuint blue_ldUniform_v;
    GLuint blue_laUniform_v;
    GLuint blue_lsUniform_v;
    GLuint blue_lightPositionUniform_v;
    
    GLuint red_ldUniform_v;
    GLuint red_laUniform_v;
    GLuint red_lsUniform_v;
    GLuint red_lightPositionUniform_v;
    
    GLuint green_laUniform_v;
    GLuint green_ldUniform_v;
    GLuint green_lsUniform_v;
    GLuint green_lightPositionUniform_v;
    
    GLuint material_kdUniform_v;
    GLuint material_kaUniform_v;
    GLuint material_ksUniform_v;
    GLuint material_shininessUniform_v;
    GLuint keyPressedUniform_v;
    
    // uniform variables for per fragment lighting shader object
    GLuint modelUniform_f, viewUniform_f, projectionUniform_f;
    GLuint blue_ldUniform_f;
    GLuint blue_laUniform_f;
    GLuint blue_lsUniform_f;
    GLuint blue_lightPositionUniform_f;
    
    GLuint red_ldUniform_f;
    GLuint red_laUniform_f;
    GLuint red_lsUniform_f;
    GLuint red_lightPositionUniform_f;
    
    GLuint green_laUniform_f;
    GLuint green_ldUniform_f;
    GLuint green_lsUniform_f;
    GLuint green_lightPositionUniform_f;
    
    GLuint material_kdUniform_f;
    GLuint material_kaUniform_f;
    GLuint material_ksUniform_f;
    GLuint material_shininessUniform_f;
    GLuint keyPressedUniform_f;

    GLfloat lightDiffuse_blue[4];
    GLfloat lightAmbient_blue[4];
    GLfloat lightSpecular_blue[4];
    GLfloat lightPosition_blue[4];
    
    GLfloat lightDiffuse_red[4];
    GLfloat lightAmbient_red[4];
    GLfloat lightSpecular_red[4];
    GLfloat lightPosition_red[4];
    
    GLfloat lightDiffuse_green[4];
    GLfloat lightAmbient_green[4];
    GLfloat lightSpecular_green[4];
    GLfloat lightPosition_green[4];
    
    float materialAmbient[4];
    float materialDiffuse[4];
    float materialSpecular[4];
    float materialShininess;
    
    float theta;
    
    BOOL startAnimation;
    BOOL enableLighting;
    BOOL toggleLightingType;
    
    vmath::mat4 perspectiveProjectionMatrix;
}

-(id)initWithFrame:(NSRect)frame;
{
    // code
    self=[super initWithFrame:frame];

    if(self)
    {
        [[self window]setContentView:self];

        NSOpenGLPixelFormatAttribute attrs[] =
        {
            // must specify the 4.1 core Profile to use OpenGL 4.1
            NSOpenGLPFAOpenGLProfile, NSOpenGLProfileVersion4_1Core,
            //specify the display ID to associate the GL Context with(main display for now)
            NSOpenGLPFAScreenMask, CGDisplayIDToOpenGLDisplayMask(kCGDirectMainDisplay),
            NSOpenGLPFANoRecovery,
            NSOpenGLPFAAccelerated,
            NSOpenGLPFAColorSize, 24,
            NSOpenGLPFADepthSize, 24,
            NSOpenGLPFAAlphaSize, 8,
            NSOpenGLPFADoubleBuffer,
            0};

        NSOpenGLPixelFormat *pixelFormat=[[[NSOpenGLPixelFormat alloc]initWithAttributes:attrs] autorelease];

        if(pixelFormat == nil)
        {
            fprintf(gpFile_ps, "No Valid OpenGLPixel Format Is Available.\nExitting...\n");
            [self release];
            [NSApp terminate:self];
        }

        NSOpenGLContext *glContext=[[[NSOpenGLContext alloc]initWithFormat:pixelFormat shareContext:nil]autorelease];

        [self setPixelFormat:pixelFormat];

        [self setOpenGLContext:glContext];  // it automatically releases the older context, if present, and sets the newer one
    }
    return(self);
}

-(CVReturn)getFrameForTime:(const CVTimeStamp *)pOutputTime
{
    // code
    NSAutoreleasePool *pool = [[NSAutoreleasePool alloc]init];

    [self drawView];

    [pool release];
    return(kCVReturnSuccess);
}

-(void)prepareOpenGL
{
    // code
    // OpenGL Info
    fprintf(gpFile_ps, "OpenGL Version :   %s\n", glGetString(GL_VERSION));
    fprintf(gpFile_ps, "GLSL Version   :   %s\n", glGetString(GL_SHADING_LANGUAGE_VERSION));

    [[self openGLContext]makeCurrentContext];

    GLint swapInt = 1; // matching with OS's swaping interval default is 1, frame gap
    [[self openGLContext]setValues:&swapInt forParameter:NSOpenGLCPSwapInterval];

    /*      ***** VERTEX SHADER *****       */
    //create shader
    vertexShaderObject_v = glCreateShader(GL_VERTEX_SHADER);

    // source code for vertex shader
    const GLchar *vertexShaderSourceCode_v =
    "	#version 410    																     " \
    "    \n                                                                                  " \
    "    in vec4 vPosition;                                                                            " \
    "    in vec3 vNormal;                                                                            " \
    "                                                                                            \n    " \
    "    out vec3 phong_ads_light;                                                                    " \
    "                                                                                            \n    " \
    "    uniform mat4 u_m_matrix;                                                                    " \
    "    uniform mat4 u_v_matrix;                                                                    " \
    "    uniform mat4 u_p_matrix;                                                                    " \
    "    uniform int u_l_key_is_pressed;                                                                " \
    "    uniform vec3 u_blue_ld;                                                                        " \
    "    uniform vec3 u_blue_la;                                                                        " \
    "    uniform vec3 u_blue_ls;                                                                        " \
    "    uniform vec3 u_red_ld;                                                                        " \
    "    uniform vec3 u_red_la;                                                                        " \
    "    uniform vec3 u_red_ls;                                                                        " \
    "    uniform vec3 u_green_ld;                                                                    " \
    "    uniform vec3 u_green_la;                                                                    " \
    "    uniform vec3 u_green_ls;                                                                    " \
    "    uniform vec3 u_ka;                                                                            " \
    "    uniform vec3 u_kd;                                                                            " \
    "    uniform vec3 u_ks;                                                                            " \
    "    uniform vec4 u_blue_light_position;                                                            " \
    "    uniform vec4 u_red_light_position;                                                            " \
    "    uniform vec4 u_green_light_position;                                                        " \
    "    uniform float u_material_shininess;                                                            " \
    "                                                                                            \n    " \
    "    void main(void)                                                                                " \
    "    {                                                                                            " \
    "        if(u_l_key_is_pressed == 1)                                                                " \
    "        {                                                                                        " \
    "            vec4 eyeCoordinates = u_v_matrix * u_m_matrix * vPosition;                            " \
    "            vec3 tNorm            = normalize(mat3(u_v_matrix * u_m_matrix) * vNormal);            " \
    "            vec3 viewerVector   = normalize(vec3(-eyeCoordinates));                                " \
    "                                                                                            \n    " \
    "            vec3 blueLightDirection = normalize(vec3(u_blue_light_position - eyeCoordinates));    " \
    "            vec3 blueLightReflectionVector = reflect(-blueLightDirection, tNorm);                " \
    "            float bluetnDotld = max(dot(tNorm, blueLightDirection), 0.0);                        " \
    "            float bluervDotvv = max(dot(viewerVector, blueLightReflectionVector), 0.0);            " \
    "                                                                                            \n    " \
    "            vec3 redLightDirection  = normalize(vec3(u_red_light_position - eyeCoordinates));    " \
    "            vec3 redLightReflectionVector = reflect(-redLightDirection, tNorm);                    " \
    "            float redtnDotld = max(dot(tNorm, redLightDirection), 0.0);                            " \
    "            float redrvDotvv = max(dot(viewerVector, redLightReflectionVector), 0.0);            " \
    "                                                                                            \n    " \
    "            vec3 greenLightDirection = normalize(vec3(u_green_light_position - eyeCoordinates));" \
    "            vec3 greenLightReflectionVector = reflect(-blueLightDirection, tNorm);                " \
    "            float greentnDotld = max(dot(tNorm, greenLightDirection), 0.0);                        " \
    "            float greenrvDotvv = max(dot(viewerVector, greenLightReflectionVector), 0.0);        " \
    "                                                                                            \n    " \
    "            vec3 blueAmbient  = u_blue_la * u_ka;                                                " \
    "            vec3 blueDiffuse  = u_blue_ld * u_kd * bluetnDotld;                                    " \
    "            vec3 blueSpecular = u_blue_ls * u_ks * pow(bluervDotvv, u_material_shininess);        " \
    "                                                                                            \n    " \
    "            vec3 redAmbient  = u_red_la * u_ka;                                                    " \
    "            vec3 redDiffuse  = u_red_ld * u_kd * redtnDotld;                                    " \
    "            vec3 redSpecular = u_red_ls * u_ks * pow(redrvDotvv, u_material_shininess);            " \
    "                                                                                            \n    " \
    "            vec3 greenAmbient  = u_green_la * u_ka;                                                " \
    "            vec3 greenDiffuse  = u_green_ld * u_kd * greentnDotld;                                " \
    "            vec3 greenSpecular = u_green_ls * u_ks * pow(greenrvDotvv, u_material_shininess);    " \
    "                                                                                            \n    " \
    "            vec3 Ambient  = blueAmbient + redAmbient + greenAmbient;                            " \
    "            vec3 Diffuse  = blueDiffuse + redDiffuse + greenDiffuse;                            " \
    "            vec3 Specular = blueSpecular + redSpecular + greenSpecular;                            " \
    "            phong_ads_light = Ambient + Diffuse + Specular;                                        " \
    "        }                                                                                        " \
    "        else                                                                                    " \
    "        {                                                                                        " \
    "            phong_ads_light = vec3(1.0, 1.0, 1.0);                                                " \
    "        }                                                                                        " \
    "        gl_Position = u_v_matrix * u_p_matrix * u_m_matrix * vPosition;                            " \
    "    }                                                                                            ";

    glShaderSource(vertexShaderObject_v, 1, (const GLchar**)&vertexShaderSourceCode_v, NULL);

    // compile shader
    GLint iInfoLogLength =0;
    GLint iShaderCompileStatus = 0;
    char *szInfoLog = NULL;

    glCompileShader(vertexShaderObject_v);
	glGetShaderiv(vertexShaderObject_v, GL_COMPILE_STATUS, &iShaderCompileStatus);
	if (iShaderCompileStatus == GL_FALSE)
	{
		glGetShaderiv(vertexShaderObject_v, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (GLchar *)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(vertexShaderObject_v, iInfoLogLength, &written, szInfoLog);
				fprintf(gpFile_ps, "VERTEX SHADER COMPILATION LOG : %s\n", szInfoLog);
				free(szInfoLog);
                [self release];
                [NSApp terminate:self];
            }
		}
	}

    /*      ***** FRAGMENT SHADER *****     */
    //create shader
    fragmentShaderObject_v = glCreateShader(GL_FRAGMENT_SHADER);

    // source code for fragment shader
    const GLchar* fragmentShaderSourceCode_v =
    "	 #version 410 	    							" \
    "	 \n												" \
    "    in vec3 phong_ads_light;                       " \
    "                                             \n    " \
    "    out vec4 FragColor;                            " \
    "                                             \n    " \
    "    void main(void)                                " \
    "    {                                              " \
    "        FragColor = vec4(phong_ads_light,1.0);     " \
    "    }                                              ";

     glShaderSource(fragmentShaderObject_v, 1, (const GLchar**)&fragmentShaderSourceCode_v, NULL);

    // compile shader
    iInfoLogLength =0;
    iShaderCompileStatus = 0;
    szInfoLog = NULL;

    glCompileShader(fragmentShaderObject_v);
	glGetShaderiv(fragmentShaderObject_v, GL_COMPILE_STATUS, &iShaderCompileStatus);
	if (iShaderCompileStatus == GL_FALSE)
	{
		glGetShaderiv(fragmentShaderObject_v, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (GLchar *)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(fragmentShaderObject_v, iInfoLogLength, &written, szInfoLog);
				fprintf(gpFile_ps, "FRAGMENT SHADER COMPILATION LOG : %s\n", szInfoLog);
				free(szInfoLog);
                [self release];
                [NSApp terminate:self];
            }
		}
	}

    /*      ***** SHADER PROGRAM *****      */
    //create program object
    shaderProgramObject_v = glCreateProgram();

    //attach shaders to it
    glAttachShader(shaderProgramObject_v, vertexShaderObject_v);
    glAttachShader(shaderProgramObject_v, fragmentShaderObject_v);

    // pre-linking binding of shader program object with vertex shader position attribute
    //(not required when used layout qualifier with location in shader, writing here for legacy of assignments)
    glBindAttribLocation(shaderProgramObject_v, PS_ATTRIBUTE_POSITION, "vPosition");
    glBindAttribLocation(shaderProgramObject_v, PS_ATTRIBUTE_NORMAL, "vNormal");

    // link shader program object
	glLinkProgram(shaderProgramObject_v);

	//error checking
	GLint iProgramLinkStatus = 0;
	iInfoLogLength = 0;
	szInfoLog = NULL;

	glGetProgramiv(shaderProgramObject_v, GL_LINK_STATUS, &iProgramLinkStatus);
	if (iProgramLinkStatus == GL_FALSE)
	{
		glGetProgramiv(shaderProgramObject_v, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (GLchar *)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLint written;
				glGetProgramInfoLog(shaderProgramObject_v, iInfoLogLength, &written, szInfoLog);
				fprintf(gpFile_ps, "SHADER PROGRAM LINK LOG : %s\n", szInfoLog);
				free(szInfoLog);
				[self release];
                [NSApp terminate:self];
			}
		}
	}


    //postlinking retieving uniform location
    // model, view and projection matrices
    viewUniform_v = glGetUniformLocation(shaderProgramObject_v, "u_v_matrix");
    modelUniform_v = glGetUniformLocation(shaderProgramObject_v, "u_m_matrix");
    projectionUniform_v = glGetUniformLocation(shaderProgramObject_v, "u_p_matrix");
    // blue light uniforms
    blue_ldUniform_v = glGetUniformLocation(shaderProgramObject_v, "u_blue_ld");
    blue_laUniform_v = glGetUniformLocation(shaderProgramObject_v, "u_blue_la");
    blue_lsUniform_v = glGetUniformLocation(shaderProgramObject_v, "u_blue_ls");
    blue_lightPositionUniform_v = glGetUniformLocation(shaderProgramObject_v, "u_blue_light_position");
    // red light uniforms
    red_ldUniform_v = glGetUniformLocation(shaderProgramObject_v, "u_red_ld");
    red_laUniform_v = glGetUniformLocation(shaderProgramObject_v, "u_red_la");
    red_lsUniform_v = glGetUniformLocation(shaderProgramObject_v, "u_red_ls");
    red_lightPositionUniform_v = glGetUniformLocation(shaderProgramObject_v, "u_red_light_position");
    // green light uniforms
    green_ldUniform_v = glGetUniformLocation(shaderProgramObject_v, "u_green_ld");
    green_laUniform_v = glGetUniformLocation(shaderProgramObject_v, "u_green_la");
    green_lsUniform_v = glGetUniformLocation(shaderProgramObject_v, "u_green_ls");
    green_lightPositionUniform_v = glGetUniformLocation(shaderProgramObject_v, "u_green_light_position");
    // material uniform
    material_kdUniform_v = glGetUniformLocation(shaderProgramObject_v, "u_kd");
    material_kaUniform_v = glGetUniformLocation(shaderProgramObject_v, "u_ka");
    material_ksUniform_v = glGetUniformLocation(shaderProgramObject_v, "u_ks");
    material_shininessUniform_v = glGetUniformLocation(shaderProgramObject_v, "u_material_shininess");
    // key pressed uniform
    keyPressedUniform_v = glGetUniformLocation(shaderProgramObject_v, "u_l_key_is_pressed");
    
    
    // PER FRAGMENT LIGHTING
    //****** VERTEX SHADER *****
    //create shader
    vertexShaderObject_f = glCreateShader(GL_VERTEX_SHADER);
    
    // source code for vertex shader
    const GLchar *vertexShaderSourceCode_f =
    "    #version 410                                                                         " \
    "    \n                                                                                  " \
    "    in vec4 vPosition;                                                                            " \
    "    in vec3 vNormal;                                                                            " \
    "                                                                                            \n    " \
    "    out vec4 eyeCoordinates;                                                                    " \
    "    out vec3 tNorm;                                                                                " \
    "    out vec3 viewerVector;                                                                        " \
    "    out vec3 blueLightDirection;                                                                " \
    "    out vec3 redLightDirection;                                                                    " \
    "    out vec3 greenLightDirection;                                                                " \
    "                                                                                            \n    " \
    "    uniform mat4 u_m_matrix;                                                                    " \
    "    uniform mat4 u_v_matrix;                                                                    " \
    "    uniform mat4 u_p_matrix;                                                                    " \
    "    uniform int u_l_key_is_pressed;                                                                " \
    "    uniform vec4 u_blue_light_position;                                                            " \
    "    uniform vec4 u_red_light_position;                                                            " \
    "    uniform vec4 u_green_light_position;                                                        " \
    "                                                                                            \n    " \
    "    void main(void)                                                                                " \
    "    {                                                                                            " \
    "        if(u_l_key_is_pressed == 1)                                                                " \
    "        {                                                                                        " \
    "            eyeCoordinates     = u_v_matrix * u_m_matrix * vPosition;                                " \
    "            tNorm            = mat3(u_v_matrix * u_m_matrix) * vNormal;                            " \
    "            viewerVector       = vec3(-eyeCoordinates);                                            " \
    "                                                                                            \n    " \
    "            blueLightDirection = vec3(u_blue_light_position - eyeCoordinates);                    " \
    "            redLightDirection  = vec3(u_red_light_position - eyeCoordinates);                    " \
    "            greenLightDirection  = vec3(u_green_light_position - eyeCoordinates);                    " \
    "        }                                                                                        " \
    "        gl_Position = u_v_matrix * u_p_matrix * u_m_matrix * vPosition;                            " \
    "    }                                                                                            ";

    glShaderSource(vertexShaderObject_f, 1, (const GLchar**)&vertexShaderSourceCode_f, NULL);
    
    // compile shader
    iInfoLogLength =0;
    iShaderCompileStatus = 0;
    szInfoLog = NULL;
    
    glCompileShader(vertexShaderObject_f);
    glGetShaderiv(vertexShaderObject_f, GL_COMPILE_STATUS, &iShaderCompileStatus);
    if (iShaderCompileStatus == GL_FALSE)
    {
        glGetShaderiv(vertexShaderObject_f, GL_INFO_LOG_LENGTH, &iInfoLogLength);
        if (iInfoLogLength > 0)
        {
            szInfoLog = (GLchar *)malloc(iInfoLogLength);
            if (szInfoLog != NULL)
            {
                GLsizei written;
                glGetShaderInfoLog(vertexShaderObject_f, iInfoLogLength, &written, szInfoLog);
                fprintf(gpFile_ps, "VERTEX SHADER COMPILATION LOG : %s\n", szInfoLog);
                free(szInfoLog);
                [self release];
                [NSApp terminate:self];
            }
        }
    }
    
    /*      ***** FRAGMENT SHADER *****     */
    //create shader
    fragmentShaderObject_f = glCreateShader(GL_FRAGMENT_SHADER);
    
    // source code for fragment shader
    const GLchar* fragmentShaderSourceCode_f =
    "     #version 410                                                                             " \
    "     \n                                                                                        " \
    "    in vec3 tNorm;                                                                             " \
    "    in vec3 viewerVector;                                                                      " \
    "    in vec3 blueLightDirection;                                                                " \
    "    in vec3 redLightDirection;                                                                 " \
    "    in vec3 greenLightDirection;                                                               " \
    "                                                                                         \n    " \
    "    uniform vec3 u_blue_ld;                                                                    " \
    "    uniform vec3 u_blue_la;                                                                    " \
    "    uniform vec3 u_blue_ls;                                                                    " \
    "    uniform vec3 u_red_ld;                                                                                " \
    "    uniform vec3 u_red_la;                                                                                " \
    "    uniform vec3 u_red_ls;                                                                                " \
    "    uniform vec3 u_green_ld;                                                                            " \
    "    uniform vec3 u_green_la;                                                                            " \
    "    uniform vec3 u_green_ls;                                                                            " \
    "    uniform vec3 u_ka;                                                                                    " \
    "    uniform vec3 u_kd;                                                                                    " \
    "    uniform vec3 u_ks;                                                                                    " \
    "    uniform float u_material_shininess;                                                                    " \
    "    uniform int u_l_key_is_pressed;                                                                        " \
    "                                                                                                    \n    " \
    "    out vec4 FragColor;                                                                                    " \
    "                                                                                                    \n    " \
    "    void main(void)                                                                                        " \
    "    {                                                                                                    " \
    "        vec3 phong_ads_light;                                                                            " \
    "        if(u_l_key_is_pressed == 1)                                                                        " \
    "        {                                                                                                " \
    "            vec3 normalizedtNorm = normalize(tNorm);                                                    " \
    "            vec3 normalizedViewerVector          = normalize(viewerVector);                                " \
    "            vec3 normalizedBlueLightDirection = normalize(blueLightDirection);                            " \
    "            vec3 normalizedRedLightDirection  = normalize(redLightDirection);                            " \
    "            vec3 normalizedGreenLightDirection  = normalize(greenLightDirection);                        " \
    "                                                                                                    \n    " \
    "            vec3 blueLightReflectionVector = reflect(-normalizedBlueLightDirection, normalizedtNorm);    " \
    "            vec3 redLightReflectionVector  = reflect(-normalizedRedLightDirection, normalizedtNorm);    " \
    "            vec3 greenLightReflectionVector  = reflect(-normalizedGreenLightDirection, normalizedtNorm);" \
    "                                                                                                    \n    " \
    "            float bluetnDotld = max(dot(normalizedtNorm, normalizedBlueLightDirection), 0.0);            " \
    "            float bluervDotvv = max(dot(normalizedViewerVector, blueLightReflectionVector), 0.0);        " \
    "            float redtnDotld = max(dot(normalizedtNorm, normalizedRedLightDirection), 0.0);                " \
    "            float redrvDotvv = max(dot(normalizedViewerVector, redLightReflectionVector), 0.0);            " \
    "            float greentnDotld = max(dot(normalizedtNorm, normalizedGreenLightDirection), 0.0);            " \
    "            float greenrvDotvv = max(dot(normalizedViewerVector, greenLightReflectionVector), 0.0);        " \
    "                                                                                                    \n    " \
    "            vec3 blueAmbient  = u_blue_la * u_ka;                                                        " \
    "            vec3 blueDiffuse  = u_blue_ld * u_kd * bluetnDotld;                                            " \
    "            vec3 blueSpecular = u_blue_ls * u_ks * pow(bluervDotvv, u_material_shininess);                " \
    "                                                                                                    \n    " \
    "            vec3 redAmbient  = u_red_la * u_ka;                                                            " \
    "            vec3 redDiffuse  = u_red_ld * u_kd * redtnDotld;                                            " \
    "            vec3 redSpecular = u_red_ls * u_ks * pow(redrvDotvv, u_material_shininess);                    " \
    "                                                                                                    \n    " \
    "            vec3 greenAmbient  = u_green_la * u_ka;                                                        " \
    "            vec3 greenDiffuse  = u_green_ld * u_kd * greentnDotld;                                        " \
    "            vec3 greenSpecular = u_green_ls * u_ks * pow(greenrvDotvv, u_material_shininess);            " \
    "                                                                                                    \n    " \
    "            vec3 Ambient  = blueAmbient + redAmbient + greenAmbient;                                    " \
    "            vec3 Diffuse  = blueDiffuse + redDiffuse + greenDiffuse;                                    " \
    "            vec3 Specular = blueSpecular + redSpecular + greenSpecular;                                    " \
    "            phong_ads_light = Ambient + Diffuse + Specular;                                                " \
    "            FragColor = vec4(phong_ads_light,1.0);                                                        " \
    "        }                                                                                                " \
    "        else                                                                                            " \
    "        {                                                                                                " \
    "            phong_ads_light = vec3(1.0, 1.0, 1.0);                                                        " \
    "            FragColor = vec4(phong_ads_light,1.0);                                                        " \
    "        }                                                                                                " \
    "                                                                                                    \n    " \
    "    }                                                                                                    ";

    
    glShaderSource(fragmentShaderObject_f, 1, (const GLchar**)&fragmentShaderSourceCode_f, NULL);
    
    // compile shader
    iInfoLogLength =0;
    iShaderCompileStatus = 0;
    szInfoLog = NULL;
    
    glCompileShader(fragmentShaderObject_f);
    glGetShaderiv(fragmentShaderObject_f, GL_COMPILE_STATUS, &iShaderCompileStatus);
    if (iShaderCompileStatus == GL_FALSE)
    {
        glGetShaderiv(fragmentShaderObject_f, GL_INFO_LOG_LENGTH, &iInfoLogLength);
        if (iInfoLogLength > 0)
        {
            szInfoLog = (GLchar *)malloc(iInfoLogLength);
            if (szInfoLog != NULL)
            {
                GLsizei written;
                glGetShaderInfoLog(fragmentShaderObject_f, iInfoLogLength, &written, szInfoLog);
                fprintf(gpFile_ps, "FRAGMENT SHADER COMPILATION LOG : %s\n", szInfoLog);
                free(szInfoLog);
                [self release];
                [NSApp terminate:self];
            }
        }
    }
    
    /*      ***** SHADER PROGRAM *****      */
    //create program object
    shaderProgramObject_f = glCreateProgram();
    
    //attach shaders to it
    glAttachShader(shaderProgramObject_f, vertexShaderObject_f);
    glAttachShader(shaderProgramObject_f, fragmentShaderObject_f);
    
    // pre-linking binding of shader program object with vertex shader position attribute
    //(not required when used layout qualifier with location in shader, writing here for legacy of assignments)
    glBindAttribLocation(shaderProgramObject_f, PS_ATTRIBUTE_POSITION, "vPosition");
    glBindAttribLocation(shaderProgramObject_f, PS_ATTRIBUTE_NORMAL, "vNormal");
    
    // link shader program object
    glLinkProgram(shaderProgramObject_f);
    
    //error checking
    iProgramLinkStatus = 0;
    iInfoLogLength = 0;
    szInfoLog = NULL;
    
    glGetProgramiv(shaderProgramObject_f, GL_LINK_STATUS, &iProgramLinkStatus);
    if (iProgramLinkStatus == GL_FALSE)
    {
        glGetProgramiv(shaderProgramObject_f, GL_INFO_LOG_LENGTH, &iInfoLogLength);
        if (iInfoLogLength > 0)
        {
            szInfoLog = (GLchar *)malloc(iInfoLogLength);
            if (szInfoLog != NULL)
            {
                GLint written;
                glGetProgramInfoLog(shaderProgramObject_f, iInfoLogLength, &written, szInfoLog);
                fprintf(gpFile_ps, "SHADER PROGRAM LINK LOG : %s\n", szInfoLog);
                free(szInfoLog);
                [self release];
                [NSApp terminate:self];
            }
        }
    }
    
    //postlinking retieving uniform location
    // model, view and projection matrices
    viewUniform_f          = glGetUniformLocation(shaderProgramObject_f, "u_v_matrix");
    modelUniform_f         = glGetUniformLocation(shaderProgramObject_f, "u_m_matrix");
    projectionUniform_f = glGetUniformLocation(shaderProgramObject_f, "u_p_matrix");
    // blue light uniforms
    blue_ldUniform_f = glGetUniformLocation(shaderProgramObject_f, "u_blue_ld");
    blue_laUniform_f = glGetUniformLocation(shaderProgramObject_f, "u_blue_la");
    blue_lsUniform_f = glGetUniformLocation(shaderProgramObject_f, "u_blue_ls");
    blue_lightPositionUniform_f = glGetUniformLocation(shaderProgramObject_f, "u_blue_light_position");
    // red light uniforms
    red_ldUniform_f = glGetUniformLocation(shaderProgramObject_f, "u_red_ld");
    red_laUniform_f = glGetUniformLocation(shaderProgramObject_f, "u_red_la");
    red_lsUniform_f = glGetUniformLocation(shaderProgramObject_f, "u_red_ls");
    red_lightPositionUniform_f = glGetUniformLocation(shaderProgramObject_f, "u_red_light_position");
    // green light uniforms
    green_ldUniform_f = glGetUniformLocation(shaderProgramObject_f, "u_green_ld");
    green_laUniform_f = glGetUniformLocation(shaderProgramObject_f, "u_green_la");
    green_lsUniform_f = glGetUniformLocation(shaderProgramObject_f, "u_green_ls");
    green_lightPositionUniform_f = glGetUniformLocation(shaderProgramObject_f, "u_green_light_position");
    // material uniform
    material_kdUniform_f = glGetUniformLocation(shaderProgramObject_f, "u_kd");
    material_kaUniform_f = glGetUniformLocation(shaderProgramObject_f, "u_ka");
    material_ksUniform_f = glGetUniformLocation(shaderProgramObject_f, "u_ks");
    material_shininessUniform_f = glGetUniformLocation(shaderProgramObject_f, "u_material_shininess");
    // key pressed uniform
    keyPressedUniform_f = glGetUniformLocation(shaderProgramObject_f, "u_l_key_is_pressed");

    
    // sphere data
    mySphere = [[Sphere alloc]init];
    fprintf(gpFile_ps, "sphere alloc init done\n");
    
    [mySphere getSphereVertexDataWithPosition:sphere_vertices withNormals:sphere_normals withTexCoords:sphere_textures andElements:sphere_elements];
    fprintf(gpFile_ps, "sphere get data done\n");
    
    iNumVertices = [mySphere getNumberOfSphereVertices];
    iNumElements = [mySphere getNumberOfSphereElements];
    fprintf(gpFile_ps, "sphere get number of vertices and elements done\n");
    
    glGenVertexArrays(1,&vao_sphere);
    glBindVertexArray(vao_sphere);
    
    // position vbo
    glGenBuffers(1, &vbo_sphere_position);
    glBindBuffer(GL_ARRAY_BUFFER, vbo_sphere_position);
    glBufferData(GL_ARRAY_BUFFER, sizeof(sphere_vertices), sphere_vertices, GL_STATIC_DRAW);
    glVertexAttribPointer(PS_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
    glEnableVertexAttribArray(PS_ATTRIBUTE_POSITION);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    
    // normal vbo
    glGenBuffers(1, &vbo_sphere_normals);
    glBindBuffer(GL_ARRAY_BUFFER, vbo_sphere_normals);
    glBufferData(GL_ARRAY_BUFFER, sizeof(sphere_normals), sphere_normals, GL_STATIC_DRAW);
    glVertexAttribPointer(PS_ATTRIBUTE_NORMAL, 3, GL_FLOAT, GL_FALSE, 0, NULL);
    glEnableVertexAttribArray(PS_ATTRIBUTE_NORMAL);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    
    // elements vbo
    glGenBuffers(1, &vbo_sphere_elements);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_elements);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(sphere_elements), sphere_elements, GL_STATIC_DRAW);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
    
    glBindVertexArray(0);
    
    // light and material properties
    //light ambient
    lightAmbient_red[0] = 0.0f;
    lightAmbient_red[1] = 0.0f;
    lightAmbient_red[2] = 0.0f;
    lightAmbient_red[3] = 0.0f;
    
    // light diffuse
    lightDiffuse_red[0] = 1.0f;
    lightDiffuse_red[1] = 0.0f;
    lightDiffuse_red[2] = 0.0f;
    lightDiffuse_red[3] = 1.0f;
    
    //light specular
    lightSpecular_red[0] = 1.0f;
    lightSpecular_red[1] = 0.0f;
    lightSpecular_red[2] = 0.0f;
    lightSpecular_red[3] = 1.0f;
    
    // light position
    lightPosition_red[0] = 0.0f;
    lightPosition_red[1] = 0.0f;
    lightPosition_red[2] = 0.0f;
    lightPosition_red[3] = 1.0f;
    
    // GREEN LIGHT
    //light ambient
    lightAmbient_green[0] = 0.0f;
    lightAmbient_green[1] = 0.0f;
    lightAmbient_green[2] = 0.0f;
    lightAmbient_green[3] = 0.0f;
    
    // light diffuse
    lightDiffuse_green[0] = 0.0f;
    lightDiffuse_green[1] = 1.0f;
    lightDiffuse_green[2] = 0.0f;
    lightDiffuse_green[3] = 1.0f;
    
    //light specular
    lightSpecular_green[0] = 0.0f;
    lightSpecular_green[1] = 1.0f;
    lightSpecular_green[2] = 0.0f;
    lightSpecular_green[3] = 1.0f;
    
    // light position
    lightPosition_green[0] = 0.0f;
    lightPosition_green[1] = 0.0f;
    lightPosition_green[2] = 0.0f;
    lightPosition_green[3] = 1.0f;
    
    //BLUE LIGHT
    //light ambient
    lightAmbient_blue[0] = 0.0f;
    lightAmbient_blue[1] = 0.0f;
    lightAmbient_blue[2] = 0.0f;
    lightAmbient_blue[3] = 0.0f;
    
    // light diffuse
    lightDiffuse_blue[0] = 0.0f;
    lightDiffuse_blue[1] = 0.0f;
    lightDiffuse_blue[2] = 1.0f;
    lightDiffuse_blue[3] = 1.0f;
    
    //light specular
    lightSpecular_blue[0] = 0.0f;
    lightSpecular_blue[1] = 0.0f;
    lightSpecular_blue[2] = 1.0f;
    lightSpecular_blue[3] = 1.0f;
    
    // light position
    lightPosition_blue[0] = 0.0f;
    lightPosition_blue[1] = 0.0f;
    lightPosition_blue[2] = 0.0f;
    lightPosition_blue[3] = 1.0f;
    
    // material ambient
    materialAmbient[0] = 0.0f;
    materialAmbient[1] = 0.0f;
    materialAmbient[2] = 0.0f;
    materialAmbient[3] = 0.0f;
    
    // material diffuse
    materialDiffuse[0] = 1.0f;
    materialDiffuse[1] = 1.0f;
    materialDiffuse[2] = 1.0f;
    materialDiffuse[3] = 1.0f;
    
    // material specular
    materialSpecular[0] = 1.0f;
    materialSpecular[1] = 1.0f;
    materialSpecular[2] = 1.0f;
    materialSpecular[3] = 1.0f;
    
    // material shininess
    materialShininess = 128.0f;
    
    theta  = 0.0;
    
    startAnimation = NO;
    enableLighting = NO;
    toggleLightingType = YES;
    
    glClearDepth(1.0f);
    glEnable(GL_DEPTH_TEST);
    glDepthFunc(GL_LEQUAL);

    //glEnable(GL_CULL_FACE);

    // set background color
    glClearColor(0.0f, 0.0f, 0.0f, 1.0f);

    // set projection matrix to identity
    perspectiveProjectionMatrix = vmath::mat4::identity();

    CVDisplayLinkCreateWithActiveCGDisplays(&displayLink);
    CVDisplayLinkSetOutputCallback(displayLink, &MyDisplayLinkCallback, self);
    CGLContextObj cglContext = (CGLContextObj)[[self openGLContext]CGLContextObj];
    CGLPixelFormatObj cglPixelFormat = (CGLPixelFormatObj)[[self pixelFormat]CGLPixelFormatObj];
    CVDisplayLinkSetCurrentCGDisplayFromOpenGLContext(displayLink, cglContext, cglPixelFormat);
    CVDisplayLinkStart(displayLink);
    
    //[super prepareOpenGL];
}

-(void)reshape
{
    // code
    CGLLockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);

    NSRect rect = [self bounds];

    GLfloat width  = rect.size.width;
    GLfloat height = rect.size.height;

    if(height == 0)
        height =1;

    glViewport(0,0,(GLsizei)width, (GLsizei)height);

    // perspective projection matrix
    perspectiveProjectionMatrix = vmath::perspective(45.0f, width/height, 0.1f, 100.0f);

    CGLUnlockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);
    
    [super reshape];
}

-(void)drawRect:(NSRect)dirtyRect
{
    //code
    [self drawView];
}

-(void)drawView
{
    // code
    [[self openGLContext]makeCurrentContext];

    CGLLockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);

    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    vmath::mat4 modelMatrix = vmath::mat4::identity();
    vmath::mat4 viewMatrix = vmath::mat4::identity();
    vmath::mat4 projectionMatrix = vmath::mat4::identity();
    vmath::mat4 translationMatrix = vmath::mat4::identity();
	vmath::mat4 rotationMatrix = vmath::mat4::identity();
	vmath::mat4 scaleMatrix = vmath::mat4::identity();

    
    
    
    if(toggleLightingType == NO)
    {
        glUseProgram(shaderProgramObject_v);
        
        // Sphere
        translationMatrix =  vmath::translate(0.0f, 0.0f, -3.0f);
        rotationMatrix =  vmath::rotate(angle_sphere, angle_sphere, angle_sphere);
        modelMatrix = translationMatrix * rotationMatrix;
        projectionMatrix = perspectiveProjectionMatrix;
        
        if (enableLighting == YES)
        {
            // calculate position of rotating lights
            lightPosition_blue[0] = 5 * cosf(theta);
            lightPosition_blue[2] = 0.0f;
            lightPosition_blue[1] = 5 * sinf(theta);
            
            lightPosition_green[0] = 5 * cosf(theta);
            lightPosition_green[1] = 0.0f;
            lightPosition_green[2] = 5 * sinf(theta);
            
            lightPosition_red[0] = 0.0f;
            lightPosition_red[1] = 500 * cosf(theta);
            lightPosition_red[2] = 500 * sinf(theta);

            
            //send necessary matrices to shader in respective uniforms
            glUniform1i(keyPressedUniform_v, 1);
            glUniform3fv(blue_ldUniform_v, 1, lightDiffuse_blue);
            glUniform3fv(blue_laUniform_v, 1, lightAmbient_blue);
            glUniform3fv(blue_lsUniform_v, 1, lightSpecular_blue);
            
            glUniform3fv(red_ldUniform_v, 1, lightDiffuse_red);
            glUniform3fv(red_laUniform_v, 1, lightAmbient_red);
            glUniform3fv(red_lsUniform_v, 1, lightSpecular_red);
            
            glUniform3fv(green_ldUniform_v, 1, lightDiffuse_green);
            glUniform3fv(green_laUniform_v, 1, lightAmbient_green);
            glUniform3fv(green_lsUniform_v, 1, lightSpecular_green);
            
            glUniform3fv(material_kdUniform_v, 1, materialDiffuse);
            glUniform3fv(material_kaUniform_v, 1, materialAmbient);
            glUniform3fv(material_ksUniform_v, 1, materialSpecular);
            
            glUniform1f(material_shininessUniform_v, materialShininess);
            
            glUniform4fv(blue_lightPositionUniform_v, 1, lightPosition_blue);
            glUniform4fv(green_lightPositionUniform_v, 1, lightPosition_green);
            glUniform4fv(red_lightPositionUniform_v, 1, lightPosition_red);
        }
        else
        {
            glUniform1i(keyPressedUniform_v, 0);
        }
        //send necessary matrices to shader in respective uniforms
        glUniformMatrix4fv(modelUniform_v, 1, GL_FALSE, modelMatrix);
        glUniformMatrix4fv(viewUniform_v, 1, GL_FALSE, viewMatrix);
        glUniformMatrix4fv(projectionUniform_v, 1, GL_FALSE, projectionMatrix);

        // draw sphere
        glBindVertexArray(vao_sphere);
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_elements);
        glDrawElements(GL_TRIANGLES, iNumElements, GL_UNSIGNED_SHORT, 0);
        glBindVertexArray(0);
    
        glUseProgram(0);
    }
    else
    {
        glUseProgram(shaderProgramObject_f);
        
        // Sphere
        translationMatrix =  vmath::translate(0.0f, 0.0f, -3.0f);
        rotationMatrix =  vmath::rotate(angle_sphere, angle_sphere, angle_sphere);
        modelMatrix = translationMatrix * rotationMatrix;
        projectionMatrix = perspectiveProjectionMatrix;
        
        
        
        if (enableLighting == YES)
        {
            // calculate position of rotating lights
            lightPosition_blue[0] = 5 * cosf(theta);
            lightPosition_blue[2] = 0.0f;
            lightPosition_blue[1] = 5 * sinf(theta);
            
            lightPosition_green[0] = 5 * cosf(theta);
            lightPosition_green[1] = 0.0f;
            lightPosition_green[2] = 5 * sinf(theta);
            
            lightPosition_red[0] = 0.0f;
            lightPosition_red[1] = 500 * cosf(theta);
            lightPosition_red[2] = 500 * sinf(theta);

            //send necessary matrices to shader in respective uniforms
            glUniform1i(keyPressedUniform_f, 1);
            glUniform3fv(blue_ldUniform_f, 1, lightDiffuse_blue);
            glUniform3fv(blue_laUniform_f, 1, lightAmbient_blue);
            glUniform3fv(blue_lsUniform_f, 1, lightSpecular_blue);
            
            glUniform3fv(red_ldUniform_f, 1, lightDiffuse_red);
            glUniform3fv(red_laUniform_f, 1, lightAmbient_red);
            glUniform3fv(red_lsUniform_f, 1, lightSpecular_red);
            
            glUniform3fv(green_ldUniform_f, 1, lightDiffuse_green);
            glUniform3fv(green_laUniform_f, 1, lightAmbient_green);
            glUniform3fv(green_lsUniform_f, 1, lightSpecular_green);
            
            glUniform3fv(material_kdUniform_f, 1, materialDiffuse);
            glUniform3fv(material_kaUniform_f, 1, materialAmbient);
            glUniform3fv(material_ksUniform_f, 1, materialSpecular);
            
            glUniform1f(material_shininessUniform_f, materialShininess);
            
            glUniform4fv(blue_lightPositionUniform_f, 1, lightPosition_blue);
            glUniform4fv(red_lightPositionUniform_f, 1, lightPosition_red);
            glUniform4fv(green_lightPositionUniform_f, 1, lightPosition_green);
        }
        else
        {
            glUniform1i(keyPressedUniform_f, 0);
        }
        //send necessary matrices to shader in respective uniforms
        glUniformMatrix4fv(modelUniform_f, 1, GL_FALSE, modelMatrix);
        glUniformMatrix4fv(viewUniform_f, 1, GL_FALSE, viewMatrix);
        glUniformMatrix4fv(projectionUniform_f, 1, GL_FALSE, projectionMatrix);
        
        
        // draw sphere
        glBindVertexArray(vao_sphere);
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_elements);
        glDrawElements(GL_TRIANGLES, iNumElements, GL_UNSIGNED_SHORT, 0);
        glBindVertexArray(0);
        
        glUseProgram(0);
    }
    if (startAnimation == YES)
    {
        theta = theta + 0.03f;
        if (theta > 360)
        {
            theta = 0.0f;
        }
    }
    
    CGLFlushDrawable((CGLContextObj)[[self openGLContext]CGLContextObj]);
    CGLUnlockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);
    
   // [self update];
}

-(BOOL)acceptsFirstResponder
{
    //code
    [[self window]makeFirstResponder:self];
    return(YES);
}

-(void)keyDown:(NSEvent *)theEvent
{
    //code
    int key = (int)[[theEvent characters]characterAtIndex:0];
    switch(key)
    {
        case 27:
            [self release];
            [NSApp terminate:self];
            break;
        case 'F':
        case 'f':
            [[self window]toggleFullScreen:self];   //repainting occurs automatically
            break;
        
        case 'L':
        case 'l':
            if(enableLighting == NO)
                enableLighting = YES;
            else
                enableLighting = NO;
            fprintf(gpFile_ps, "l is pressed\n");
            break;
        case 'A':
        case 'a':
            fprintf(gpFile_ps, "a is pressed\n");
            if(startAnimation == NO)
                startAnimation = YES;
            else
                startAnimation = NO;
            break;
        case 't':
        case 'T':
            if(toggleLightingType == NO)
                toggleLightingType = YES;
            else
                toggleLightingType = NO;
            break;
        default:
            break;
    }
}

-(void)mouseDown:(NSEvent *)theEvent
{
    // code
}

-(void)mouseDragged:(NSEvent *)theEvent
{
    // code
}

-(void)rightMouseDown:(NSEvent *)theEvent
{
    // code
}

-(void)dealloc
{
    // code
    // destroy vao
   	if (vbo_sphere_elements)
	{
		glDeleteBuffers(1, &vbo_sphere_elements);
		vbo_sphere_elements = 0;
	}
	if (vbo_sphere_normals)
	{
		glDeleteBuffers(1, &vbo_sphere_normals);
		vbo_sphere_normals = 0;
	}
    if (vbo_sphere_position)
    {
        glDeleteBuffers(1, &vbo_sphere_position);
        vbo_sphere_position = 0;
    }
	if (vao_sphere)
	{
		glDeleteVertexArrays(1, &vao_sphere);
		vao_sphere = 0;
	}

    //detach shaders
    glDetachShader(shaderProgramObject_v, vertexShaderObject_v);
    glDetachShader(shaderProgramObject_v, fragmentShaderObject_v);

    //delete shader objects
    glDeleteShader(vertexShaderObject_v);
    vertexShaderObject_v=0;
    glDeleteShader(fragmentShaderObject_v);
    fragmentShaderObject_v = 0;

    // delete program object
    glDeleteProgram(shaderProgramObject_v);
    shaderProgramObject_v = 0;

    CVDisplayLinkStop(displayLink);
    CVDisplayLinkRelease(displayLink);

    [super dealloc];
}
@end

CVReturn MyDisplayLinkCallback(CVDisplayLinkRef displayLink, 
                                const CVTimeStamp *pNow,
                                const CVTimeStamp *pOutputTime,
                                CVOptionFlags flagsIn,
                                CVOptionFlags *pFlagsOut,
                                void *pDisplayLinkcontext)
{
    CVReturn result = [(GLView *)pDisplayLinkcontext getFrameForTime:pOutputTime];
    return(result);    
}
