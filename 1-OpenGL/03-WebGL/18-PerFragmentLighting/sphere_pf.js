//global variables
var canvas = null;
var gl = null;			// webgl context
var bFullscreen = false;
var canvas_original_width;
var canvas_original_height;

const WebGLMacros = //when whole 'WebGLMacros' is const all inside it are automaticaly const
{
	AMC_ATTRIBUTE_VERTEX:0,
	AMC_ATTRIBUTE_COLOR:1,
	AMC_ATTRIBUTE_NORMAL:2,
	AMC_ATTRIBUTE_TEXCOORD0:3,
}

var vertexShaderObject;
var fragmentShaderObject;
var shaderProgramObject;

var mUniform, vUniform, pUniform;
var ldUniform, kdUniform;
var laUniform, kaUniform;
var lsUniform, ksUniform;
var materialShininessUniform;
var lightPositionUniform;
var keyPressedUniform;

var lightAmbient	= new Float32Array([0.0, 0.0, 0.0]);
var lightDiffuse	= new Float32Array([1.0, 1.0, 1.0]);
var lightSpecular	= new Float32Array([1.0, 1.0, 1.0]);
var lightPosition 	= new Float32Array([10.0, 10.0, 10.0, 1.0]);

var materialAmbient  	= new Float32Array([0.0, 0.0, 0.0]);
var materialDiffuse  	= new Float32Array([1.0, 1.0, 1.0]);
var materialSpecular  	= new Float32Array([1.0, 1.0, 1.0]);
var materialShininess 	= 128.0;

var enableLighting = false;
var perspectiveProjectionMatrix;

var sphere = null;

// To start animation : To have requestAnimationFrame() to be called "cross-browser" compatible
var requestAnimationFrame = 
window.requestAnimationFrame ||
window.webkitRequestAnimationFrame ||
window.mozRequestAnimationFrame ||
window.oRequestAnimationFrame ||
window.msRequestAnimationFrame ||
null;

// To stop animation : To have cancelAnimationFrame() to be called "cross-browser" compatible
var cancelAnimationFrame = 
window.cancelAnimationFrame ||
window.webkitCancelAnimationFrame || window.webkitCancelRequestAnimationFrame ||
window.mozCancelAnimationFrame || window.mozCancelRequestAnimationFrame ||
window.oCancelAmimationFrame || window.oCancelRequestAnimationFrame ||
window.msCancelAnimationFrame || window.msCancelRequestAnimationFrame ||
null;

// onload function
function main()
{
	// get <canvas> element
	canvas = document.getElementById("AMC");
	if(!canvas)
		console.log("Obtaining Canvas Failed");
	else
		console.log("Obtaining Canvas Succeeded");

	canvas_original_width = canvas.width;
	canvas_original_height = canvas.height;
	
	// Register keyboard's keydown event handler
	window.addEventListener("keydown", keyDown, false);
	window.addEventListener("click", mouseDown, false);
	window.addEventListener("resize", resize, false);

	// initialize webgl
	init();

	//start drawing here as warming-up
	resize();
	draw();
}

function toggleFullScreen()
{
	//code
	var fullscreen_element = document.fullscreen_element ||
	document.webkitFullscreenElement ||
	document.mozFullScreenElement ||
	document.msFullscreenElement ||
	null;

	// if not fullscreen
	if(fullscreen_element == null)
	{
		if(canvas.requestFullscreen)
			canvas.requestFullscreen();
		else if(canvas.mozRequestFullScreen)
			canvas.mozRequestFullScreen();
		else if(canvas.webkitRequestFullscreen)
			canvas.webkitRequestFullscreen();
		else if(canvas.msRequestFullscreen)
			canvas.msRequestFullscreen();
		bFullscreen = true;
	}
	else 		// if already fullscreen
	{
		if(document.exitFullscreen)
			document.exitFullscreen();
		else if(document.mozCancelFullScreen)
			document.mozCancelFullScreen();
		else if(document.webkitExitFullscreen)
			document.webkitExitFullscreen()
		else if(document.webkitExitFullscreen)
			document.webkitExitFullscreen();
		bFullscreen = false;
	}
}

function init()
{
	//code
	// get webgl 2.0 context
	gl = canvas.getContext("webgl2");
	if(!gl)
	{
		console.log("Failed to get the rendering context for WebGL");
		return;
	}

	gl.viewportWidth = canvas.width;
	gl.viewportHeight = canvas.height;

	//vertex shader
	vertexShaderObject =gl.createShader(gl.VERTEX_SHADER);

	var vertexShaderSourceCode = 
	"	#version 300 es 																	" +
	"																	                \n  " +
    "	in vec4 vPosition;													                " +
    "   in vec3 vNormal;                                                                    " +
    "																	                \n  " +
    "   out vec3 tNorm;                                                                     " +
    "   out vec3 lightDirection;                                                            " +
    "   out vec3 viewerVector;                                                              " +
    "	uniform mat4 u_m_matrix;											                " +
    "	uniform mat4 u_v_matrix;											                " +
    "	uniform mat4 u_p_matrix;											                " +
    "   uniform vec4 u_light_position;                                                      " +
    "   uniform mediump int u_enable_lighting;                                              " +
    "																	                \n  " +
    "	void main(void)														                " +
    "	{																	                " +
    "       if(u_enable_lighting == 1)                                                      " +
    "       {                                                                               " +
    "           vec4 eyeCoordinates = u_v_matrix * u_m_matrix * vPosition;                  " +
    "           tNorm = mat3(u_v_matrix * u_m_matrix) * vNormal;                            " +
    "           lightDirection = vec3(u_light_position - eyeCoordinates);                   " +
    "           viewerVector = vec3(-eyeCoordinates);                                       " +
    "       }                                                                               " +
    "		gl_Position = u_p_matrix * u_v_matrix * u_m_matrix * vPosition;	                " +
    "	}																	                ";

	gl.shaderSource(vertexShaderObject, vertexShaderSourceCode);
	gl.compileShader(vertexShaderObject);
	if(gl.getShaderParameter(vertexShaderObject, gl.COMPILE_STATUS) == false)
	{
		var error = gl.getShaderInfoLog(vertexShaderObject);
		if(error.length > 0)
		{
			alert(error);
			uninitialize();
		}
	}

	//fragment shader
	fragmentShaderObject = gl.createShader(gl.FRAGMENT_SHADER);

	var fragmentShaderSourceCode = 
	"	#version 300 es 																					" +
	"																									\n	" +
	"	precision highp float;																			\n	" +
	"															                        				\n  " +
    "   in vec3 lightDirection;                                                         				    " +
    "   in vec3 viewerVector;                                                           				    " +
    "   in vec3 tNorm;                                                                  				    " +
    "															                        				\n  " +
    "	out vec4 FragColor;										                        				    " +
    "                                                                                   				\n  " +
    "   uniform vec3 u_la;                                                              				    " +
    "   uniform vec3 u_ld;                                                              				    " +
    "   uniform vec3 u_ls;                                                              				    " +
    "   uniform vec3 u_ka;                                                              				    " +
    "   uniform vec3 u_kd;                                                              				    " +
    "   uniform vec3 u_ks;                                                              				    " +
    "   uniform float u_shininess;                                                      				    " +
    "   uniform int u_enable_lighting;                                                          		    " +
    "															                                		\n  " +
    "	void main(void)											                                			" +
    "	{														                               			    " +
    "       vec3 phong_ads_lights;                                                              		    " +
    "                                                                                           		\n  " +
    "       if(u_enable_lighting == 1)                                                          		    " +
    "       {                                                                                   		    " +
    "           vec3 normalizedtNorm = normalize(tNorm);                                      				" +
    "           vec3 normalizedlightDirection = normalize(lightDirection);                    				" +
    "           vec3 normalizedviewerVector = normalize(viewerVector);                        				" +
    "                                                                                         			\n  " +
    "           vec3 reflectionVector = normalize(reflect(-normalizedlightDirection, normalizedtNorm));		" +
    "           float tnDotld = max(dot(normalizedlightDirection, normalizedtNorm), 0.0);   			    " +
    "           float rvDotvv = max(dot(reflectionVector, normalizedviewerVector), 0.0);    			    " +
    "                                                                                       			\n  " +
    "           vec3 ambient = u_la * u_ka;                                                 			    " +
    "           vec3 diffuse = u_ld * u_kd * tnDotld;                                       			    " +
    "           vec3 specular = u_ls * u_ks * pow(rvDotvv, u_shininess);            						" +
    "           phong_ads_lights = specular + diffuse + ambient;                                 			" +
    "       }                                                                                       		" +
    "       else                                                                                    		" +
    "       {                                                                                       		" +
    "           phong_ads_lights    = vec3(1.0, 1.0, 1.0);                                          		" +
    "       }                                                                                       		" +
    "       FragColor = vec4(phong_ads_lights, 1.0);                                            			" +
    "	}															                                		";

	gl.shaderSource(fragmentShaderObject, fragmentShaderSourceCode);
	gl.compileShader(fragmentShaderObject);
	if(gl.getShaderParameter(fragmentShaderObject, gl.COMPILE_STATUS) == false)
	{
		var error = gl.getShaderInfoLog(fragmentShaderObject);
		if(error.length > 0)
		{
			alert(error);
			uninitialize();
		}
	}

	//shader program object
	shaderProgramObject = gl.createProgram();
	gl.attachShader(shaderProgramObject, vertexShaderObject);
	gl.attachShader(shaderProgramObject, fragmentShaderObject);

	//pre linking binding to shader program object with shader attributes
	gl.bindAttribLocation(shaderProgramObject, WebGLMacros.AMC_ATTRIBUTE_VERTEX, "vPosition");
	gl.bindAttribLocation(shaderProgramObject, WebGLMacros.AMC_ATTRIBUTE_NORMAL, "vNormal");

	// linking
	gl.linkProgram(shaderProgramObject);
	if(!gl.getProgramParameter(shaderProgramObject, gl.LINK_STATUS))
	{
		var error = gl.getProgramInfoLog(shaderProgramObject);
		if(error.length > 0)
		{
			alert(error);
			uninitialize();
		}
	}

	// get mvp uniform location
	mUniform = gl.getUniformLocation(shaderProgramObject, "u_m_matrix");
	vUniform = gl.getUniformLocation(shaderProgramObject, "u_v_matrix");
	pUniform = gl.getUniformLocation(shaderProgramObject, "u_p_matrix");
	
	ldUniform = gl.getUniformLocation(shaderProgramObject, "u_ld");
	laUniform = gl.getUniformLocation(shaderProgramObject, "u_la");
	lsUniform = gl.getUniformLocation(shaderProgramObject, "u_ls");
    
    kdUniform = gl.getUniformLocation(shaderProgramObject, "u_kd");
    kaUniform = gl.getUniformLocation(shaderProgramObject, "u_ka");
    ksUniform = gl.getUniformLocation(shaderProgramObject, "u_ks");
    
    lightPositionUniform = gl.getUniformLocation(shaderProgramObject, "u_light_position");
  
    materialShininessUniform = gl.getUniformLocation(shaderProgramObject, "u_shininess");
    keyPressedUniform = gl.getUniformLocation(shaderProgramObject, "u_enable_lighting");

    // **** vertices, colors, shader attribs, vbo, vao initializations *****
    sphere = new Mesh();
    makeSphere(sphere, 2.0, 30, 30);

	// set clear color
	gl.clearColor(0.0, 0.0, 0.0, 1.0); // blue colored screen
	//enable depth
	gl.clearDepth(1.0);
	gl.enable(gl.DEPTH_TEST);
	gl.depthFunc(gl.LEQUAL);

	// initialize projection matrix
	perspectiveProjectionMatrix = mat4.create();
}

function resize()
{
	//code
	if(bFullscreen == true)
	{
		canvas.width = window.innerWidth;
		canvas.height = window.innerHeight;
	}
	else
	{
		canvas.width = canvas_original_width;
		canvas.height = canvas_original_height;
	}

	//set the viewport to match
	gl.viewport(0, 0, canvas.width, canvas.height);

	//perspective projection:
    mat4.perspective(perspectiveProjectionMatrix, 45.0, parseFloat(canvas.width)/parseFloat(canvas.height), 0.1, 100.0);
}


function degToRad(degrees)
{
	///code
	return(degrees * Math.PI / 100);
}

function draw()
{
	//code
	gl.clear(gl.COLOR_BUFFER_BIT|gl.DEPTH_BUFFER_BIT);

	var modelMatrix 		= mat4.create();
	var viewMatrix 			= mat4.create();
	var projectionMatrix 	= mat4.create();
	var translationMatrix 	= mat4.create();
	var scaleMatrix 		= mat4.create();

	mat4.translate(translationMatrix, translationMatrix, [0.0,0.0,-5.5]);
	mat4.multiply(modelMatrix, modelMatrix, translationMatrix);
	mat4.multiply(projectionMatrix, projectionMatrix, perspectiveProjectionMatrix);
	
	gl.useProgram(shaderProgramObject);

	if(enableLighting == true)
	{
	 	gl.uniform1i(keyPressedUniform, 1);

        gl.uniform3fv(laUniform, lightAmbient); 
        gl.uniform3fv(ldUniform, lightDiffuse); 
        gl.uniform3fv(lsUniform, lightSpecular);

       	gl.uniform1f(materialShininessUniform, materialShininess);

        gl.uniform3fv(kaUniform, materialAmbient);
        gl.uniform3fv(kdUniform, materialDiffuse);
        gl.uniform3fv(ksUniform, materialSpecular);
        
        gl.uniform4fv(lightPositionUniform, lightPosition);

	}
	else
	{
		gl.uniform1i(keyPressedUniform, 0);
	}

	// Send matrices to shaders in respective uniforms
    gl.uniformMatrix4fv(mUniform, false, modelMatrix);
    gl.uniformMatrix4fv(vUniform, false, viewMatrix);
    gl.uniformMatrix4fv(pUniform, false, projectionMatrix);

	// Draw sphere
	sphere.draw();

	gl.useProgram(null);

	//animation loop
	requestAnimationFrame(draw, canvas);
}

function uninitialize()
{
	//code
	if(sphere)
	{
		sphere.deallocate();
		sphere = null;
	}

	if(shaderProgramObject)
	{
		if(fragmentShaderObject)
		{
			gl.detachShader(shaderProgramObject, fragmentShaderObject);
			gl.deleteShader(fragmentShaderObject);
			fragmentShaderObject = null;
		}

		if(vertexShaderObject)
		{
			gl.detachShader(shaderProgramObject, vertexShaderObject);
			gl.deleteShader(vertexShaderObject);
			vertexShaderObject = null;
		}
		gl.deleteProgram(shaderProgramObject);
		shaderProgramObject = null;

	}
}

function keyDown(event)
{
	//code
	switch(event.keyCode)
	{
		case 70: 	// for f or F
			toggleFullScreen();

			break;
		case 108: 		// l
		case 76: 		// L
			if(enableLighting == false)
			{
				enableLighting = true;
			}
			else
				enableLighting = false;
			break;
		case 27:
			uninitialize();
			window.close();
			break;
	}
}

function mouseDown()
{
	//code
}
