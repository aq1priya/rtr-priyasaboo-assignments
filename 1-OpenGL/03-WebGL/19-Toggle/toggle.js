//global variables
var canvas = null;
var gl = null;			// webgl context
var bFullscreen = false;
var canvas_original_width;
var canvas_original_height;

const WebGLMacros = //when whole 'WebGLMacros' is const all inside it are automaticaly const
{
	AMC_ATTRIBUTE_VERTEX:0,
	AMC_ATTRIBUTE_COLOR:1,
	AMC_ATTRIBUTE_NORMAL:2,
	AMC_ATTRIBUTE_TEXCOORD0:3,
}

var vertexShaderObject_f;
var fragmentShaderObject_f;
var shaderProgramObject_f;
var vertexShaderObject_v;
var fragmentShaderObject_v;
var shaderProgramObject_v;

var mUniform_f, vUniform_f, pUniform_f;
var ldUniform_f, kdUniform_f;
var laUniform_f, kaUniform_f;
var lsUniform_f, ksUniform_f;
var materialShininessUniform_f;
var lightPositionUniform_f;
var keyPressedUniform_f;

var mUniform_v, vUniform_v, pUniform_v;
var ldUniform_v, kdUniform_v;
var laUniform_v, kaUniform_v;
var lsUniform_v, ksUniform_v;
var materialShininessUniform_v;
var lightPositionUniform_v;
var keyPressedUniform_v;

var lightAmbient	= new Float32Array([0.0, 0.0, 0.0]);
var lightDiffuse	= new Float32Array([1.0, 1.0, 1.0]);
var lightSpecular	= new Float32Array([1.0, 1.0, 1.0]);
var lightPosition 	= new Float32Array([10.0, 10.0, 10.0, 1.0]);

var materialAmbient  	= new Float32Array([0.0, 0.0, 0.0]);
var materialDiffuse  	= new Float32Array([1.0, 1.0, 1.0]);
var materialSpecular  	= new Float32Array([1.0, 1.0, 1.0]);
var materialShininess 	= 128.0;

var toggleLighting = false;
var enableLighting = false;
var perspectiveProjectionMatrix;

var sphere = null;

// To start animation : To have requestAnimationFrame() to be called "cross-browser" compatible
var requestAnimationFrame = 
window.requestAnimationFrame ||
window.webkitRequestAnimationFrame ||
window.mozRequestAnimationFrame ||
window.oRequestAnimationFrame ||
window.msRequestAnimationFrame ||
null;

// To stop animation : To have cancelAnimationFrame() to be called "cross-browser" compatible
var cancelAnimationFrame = 
window.cancelAnimationFrame ||
window.webkitCancelAnimationFrame || window.webkitCancelRequestAnimationFrame ||
window.mozCancelAnimationFrame || window.mozCancelRequestAnimationFrame ||
window.oCancelAmimationFrame || window.oCancelRequestAnimationFrame ||
window.msCancelAnimationFrame || window.msCancelRequestAnimationFrame ||
null;

// onload function
function main()
{
	// get <canvas> element
	canvas = document.getElementById("AMC");
	if(!canvas)
		console.log("Obtaining Canvas Failed");
	else
		console.log("Obtaining Canvas Succeeded");

	canvas_original_width = canvas.width;
	canvas_original_height = canvas.height;
	
	// Register keyboard's keydown event handler
	window.addEventListener("keydown", keyDown, false);
	window.addEventListener("click", mouseDown, false);
	window.addEventListener("resize", resize, false);

	// initialize webgl
	init();

	//start drawing here as warming-up
	resize();
	draw();
}

function toggleFullScreen()
{
	//code
	var fullscreen_element = document.fullscreen_element ||
	document.webkitFullscreenElement ||
	document.mozFullScreenElement ||
	document.msFullscreenElement ||
	null;

	// if not fullscreen
	if(fullscreen_element == null)
	{
		if(canvas.requestFullscreen)
			canvas.requestFullscreen();
		else if(canvas.mozRequestFullScreen)
			canvas.mozRequestFullScreen();
		else if(canvas.webkitRequestFullscreen)
			canvas.webkitRequestFullscreen();
		else if(canvas.msRequestFullscreen)
			canvas.msRequestFullscreen();
		bFullscreen = true;
	}
	else 		// if already fullscreen
	{
		if(document.exitFullscreen)
			document.exitFullscreen();
		else if(document.mozCancelFullScreen)
			document.mozCancelFullScreen();
		else if(document.webkitExitFullscreen)
			document.webkitExitFullscreen()
		else if(document.webkitExitFullscreen)
			document.webkitExitFullscreen();
		bFullscreen = false;
	}
}

function init()
{
	//code
	// get webgl 2.0 context
	gl = canvas.getContext("webgl2");
	if(!gl)
	{
		console.log("Failed to get the rendering context for WebGL");
		return;
	}

	gl.viewportWidth = canvas.width;
	gl.viewportHeight = canvas.height;

	//vertex shader
	vertexShaderObject_f =gl.createShader(gl.VERTEX_SHADER);

	var vertexShaderSourceCode_f = 
	"	#version 300 es 																	" +
	"																	                \n  " +
    "	in vec4 vPosition;													                " +
    "   in vec3 vNormal;                                                                    " +
    "																	                \n  " +
    "   out vec3 tNorm;                                                                     " +
    "   out vec3 lightDirection;                                                            " +
    "   out vec3 viewerVector;                                                              " +
    "	uniform mat4 u_m_matrix;											                " +
    "	uniform mat4 u_v_matrix;											                " +
    "	uniform mat4 u_p_matrix;											                " +
    "   uniform vec4 u_light_position;                                                      " +
    "   uniform mediump int u_enable_lighting;                                              " +
    "																	                \n  " +
    "	void main(void)														                " +
    "	{																	                " +
    "       if(u_enable_lighting == 1)                                                      " +
    "       {                                                                               " +
    "           vec4 eyeCoordinates = u_v_matrix * u_m_matrix * vPosition;                  " +
    "           tNorm = mat3(u_v_matrix * u_m_matrix) * vNormal;                            " +
    "           lightDirection = vec3(u_light_position - eyeCoordinates);                   " +
    "           viewerVector = vec3(-eyeCoordinates);                                       " +
    "       }                                                                               " +
    "		gl_Position = u_p_matrix * u_v_matrix * u_m_matrix * vPosition;	                " +
    "	}																	                ";

	gl.shaderSource(vertexShaderObject_f, vertexShaderSourceCode_f);
	gl.compileShader(vertexShaderObject_f);
	if(gl.getShaderParameter(vertexShaderObject_f, gl.COMPILE_STATUS) == false)
	{
		var error = gl.getShaderInfoLog(vertexShaderObject_f);
		if(error.length > 0)
		{
			alert(error);
			uninitialize();
		}
	}

	//fragment shader
	fragmentShaderObject_f = gl.createShader(gl.FRAGMENT_SHADER);

	var fragmentShaderSourceCode_f = 
	"	#version 300 es 																					" +
	"																									\n	" +
	"	precision highp float;																			\n	" +
	"															                        				\n  " +
    "   in vec3 lightDirection;                                                         				    " +
    "   in vec3 viewerVector;                                                           				    " +
    "   in vec3 tNorm;                                                                  				    " +
    "															                        				\n  " +
    "	out vec4 FragColor;										                        				    " +
    "                                                                                   				\n  " +
    "   uniform vec3 u_la;                                                              				    " +
    "   uniform vec3 u_ld;                                                              				    " +
    "   uniform vec3 u_ls;                                                              				    " +
    "   uniform vec3 u_ka;                                                              				    " +
    "   uniform vec3 u_kd;                                                              				    " +
    "   uniform vec3 u_ks;                                                              				    " +
    "   uniform float u_shininess;                                                      				    " +
    "   uniform int u_enable_lighting;                                                          		    " +
    "															                                		\n  " +
    "	void main(void)											                                			" +
    "	{														                               			    " +
    "       vec3 phong_ads_lights;                                                              		    " +
    "                                                                                           		\n  " +
    "       if(u_enable_lighting == 1)                                                          		    " +
    "       {                                                                                   		    " +
    "           vec3 normalizedtNorm = normalize(tNorm);                                      				" +
    "           vec3 normalizedlightDirection = normalize(lightDirection);                    				" +
    "           vec3 normalizedviewerVector = normalize(viewerVector);                        				" +
    "                                                                                         			\n  " +
    "           vec3 reflectionVector = normalize(reflect(-normalizedlightDirection, normalizedtNorm));		" +
    "           float tnDotld = max(dot(normalizedlightDirection, normalizedtNorm), 0.0);   			    " +
    "           float rvDotvv = max(dot(reflectionVector, normalizedviewerVector), 0.0);    			    " +
    "                                                                                       			\n  " +
    "           vec3 ambient = u_la * u_ka;                                                 			    " +
    "           vec3 diffuse = u_ld * u_kd * tnDotld;                                       			    " +
    "           vec3 specular = u_ls * u_ks * pow(rvDotvv, u_shininess);            						" +
    "           phong_ads_lights = specular + diffuse + ambient;                                 			" +
    "       }                                                                                       		" +
    "       else                                                                                    		" +
    "       {                                                                                       		" +
    "           phong_ads_lights    = vec3(1.0, 1.0, 1.0);                                          		" +
    "       }                                                                                       		" +
    "       FragColor = vec4(phong_ads_lights, 1.0);                                            			" +
    "	}															                                		";

	gl.shaderSource(fragmentShaderObject_f, fragmentShaderSourceCode_f);
	gl.compileShader(fragmentShaderObject_f);
	if(gl.getShaderParameter(fragmentShaderObject_f, gl.COMPILE_STATUS) == false)
	{
		var error = gl.getShaderInfoLog(fragmentShaderObject_f);
		if(error.length > 0)
		{
			alert(error);
			uninitialize();
		}
	}

	//shader program object
	shaderProgramObject_f = gl.createProgram();
	gl.attachShader(shaderProgramObject_f, vertexShaderObject_f);
	gl.attachShader(shaderProgramObject_f, fragmentShaderObject_f);

	//pre linking binding to shader program object with shader attributes
	gl.bindAttribLocation(shaderProgramObject_f, WebGLMacros.AMC_ATTRIBUTE_VERTEX, "vPosition");
	gl.bindAttribLocation(shaderProgramObject_f, WebGLMacros.AMC_ATTRIBUTE_NORMAL, "vNormal");

	// linking
	gl.linkProgram(shaderProgramObject_f);
	if(!gl.getProgramParameter(shaderProgramObject_f, gl.LINK_STATUS))
	{
		var error = gl.getProgramInfoLog(shaderProgramObject_f);
		if(error.length > 0)
		{
			alert(error);
			uninitialize();
		}
	}

	// get mvp uniform location
	mUniform_f = gl.getUniformLocation(shaderProgramObject_f, "u_m_matrix");
	vUniform_f = gl.getUniformLocation(shaderProgramObject_f, "u_v_matrix");
	pUniform_f = gl.getUniformLocation(shaderProgramObject_f, "u_p_matrix");
	
	ldUniform_f = gl.getUniformLocation(shaderProgramObject_f, "u_ld");
	laUniform_f = gl.getUniformLocation(shaderProgramObject_f, "u_la");
	lsUniform_f = gl.getUniformLocation(shaderProgramObject_f, "u_ls");
    
    kdUniform_f = gl.getUniformLocation(shaderProgramObject_f, "u_kd");
    kaUniform_f = gl.getUniformLocation(shaderProgramObject_f, "u_ka");
    ksUniform_f = gl.getUniformLocation(shaderProgramObject_f, "u_ks");
    
    lightPositionUniform_f = gl.getUniformLocation(shaderProgramObject_f, "u_light_position");
  
    materialShininessUniform_f = gl.getUniformLocation(shaderProgramObject_f, "u_shininess");
    keyPressedUniform_f = gl.getUniformLocation(shaderProgramObject_f, "u_enable_lighting");


    //--------------------------------------------------------------------------------------------------
    //vertex shader
	vertexShaderObject_v =gl.createShader(gl.VERTEX_SHADER);

	var vertexShaderSourceCode_v = 
	"	#version 300 es 																				" +
	"																								\n 	" +
    "	in vec4 vPosition;													         				    " +
    "   in vec3 vNormal;                                                             				    " +
    "																	             				\n  " +
    "	out vec3 phong_ads_lights;													 				    " +
    "																	             				\n  " +
    "	uniform mat4 u_m_matrix;											         				    " +
    "	uniform mat4 u_v_matrix;											         				    " +
    "	uniform mat4 u_p_matrix;											         				    " +
    "   uniform vec3 u_la;                                                               				" +
    "   uniform vec3 u_ld;                                                               				" +
    "   uniform vec3 u_ls;                                                               				" +
    "   uniform vec3 u_ka;                                                             				    " +
    "   uniform vec3 u_kd;                                                             				    " +
    "   uniform vec3 u_ks;                                                             				    " +
    "   uniform float u_shininess;                                                     				    " +
    "   uniform vec4 u_light_position;                      				 						    " +
    "   uniform int u_enable_lighting;                                                 				    " +
    "																	               				\n  " +
    "	void main(void)														           				    " +
    "	{																	           				    " +
    "       if(u_enable_lighting == 1)                                                 				    " +
    "       {                                                                          				    " +
    "           vec4 eyeCoordinates = u_v_matrix * u_m_matrix * vPosition;             				    " +
    "           mat3 normalMatrix   = mat3(u_v_matrix * u_m_matrix);                   				    " +
    "           vec3 tNorm          = normalize(normalMatrix * vNormal);               				    " +
    "																	               				\n  " +
    "           vec3 lightDirection   = normalize(vec3(u_light_position - eyeCoordinates)); 			" +
    "           float tnDotld         = max(dot(tNorm, lightDirection), 0.0);               			" +
    "           vec3 reflectionVector = reflect(-lightDirection, tNorm);                    			" +
    "           vec3 viewerVector     = normalize(vec3(-eyeCoordinates));   							" +
    "           float rvDotvv         = max(dot(reflectionVector, viewerVector), 0.0);   				" +
    "                                                                                   			\n  " +
    "           vec3 ambient   = u_la * u_ka;                                          					" +
    "           vec3 diffuse   = u_ld * u_kd * tnDotld;                                					" +
    "           vec3 specular  = u_ls * u_ks * pow(rvDotvv, u_shininess);              					" +
    "                                                                                   			\n  " +
    "           phong_ads_lights    = ambient + diffuse + specular;                     			    " +
    "       }                                                                           			    " +
    "       else                                                                        			    " +
    "       {                                                                           			    " +
    "           phong_ads_lights   = vec3(1.0, 1.0, 1.0);                               			    " +
    "       }                                                                           			    " +
    "                                                                                   			\n  " +
    "		gl_Position = u_p_matrix * u_v_matrix * u_m_matrix * vPosition;								" +
    "	}																								";

	gl.shaderSource(vertexShaderObject_v, vertexShaderSourceCode_v);
	gl.compileShader(vertexShaderObject_v);
	if(gl.getShaderParameter(vertexShaderObject_v, gl.COMPILE_STATUS) == false)
	{
		var error = gl.getShaderInfoLog(vertexShaderObject_v);
		if(error.length > 0)
		{
			alert(error);
			uninitialize();
		}
	}

	//fragment shader
	fragmentShaderObject_v = gl.createShader(gl.FRAGMENT_SHADER);

	var fragmentShaderSourceCode_v = 
	"	#version 300 es 												" +
	"															\n	" +
	"	precision highp float;									\n	" +
    "	in vec3 phong_ads_lights;									" +
    "															\n  " +
    "	out vec4 FragColor;											" +
    "															\n  " +
    "	void main(void)												" +
    "	{															" +
    "           FragColor = vec4(phong_ads_lights, 1.0);            " +
    "	}															";

	gl.shaderSource(fragmentShaderObject_v, fragmentShaderSourceCode_v);
	gl.compileShader(fragmentShaderObject_v);
	if(gl.getShaderParameter(fragmentShaderObject_v, gl.COMPILE_STATUS) == false)
	{
		var error = gl.getShaderInfoLog(fragmentShaderObject_v);
		if(error.length > 0)
		{
			alert(error);
			uninitialize();
		}
	}

	//shader program object
	shaderProgramObject_v = gl.createProgram();
	gl.attachShader(shaderProgramObject_v, vertexShaderObject_v);
	gl.attachShader(shaderProgramObject_v, fragmentShaderObject_v);

	//pre linking binding to shader program object with shader attributes
	gl.bindAttribLocation(shaderProgramObject_v, WebGLMacros.AMC_ATTRIBUTE_VERTEX, "vPosition");
	gl.bindAttribLocation(shaderProgramObject_v, WebGLMacros.AMC_ATTRIBUTE_NORMAL, "vNormal");

	// linking
	gl.linkProgram(shaderProgramObject_v);
	if(!gl.getProgramParameter(shaderProgramObject_v, gl.LINK_STATUS))
	{
		var error = gl.getProgramInfoLog(shaderProgramObject_v);
		if(error.length > 0)
		{
			alert(error);
			uninitialize();
		}
	}

	// get mvp uniform location
	mUniform_v = gl.getUniformLocation(shaderProgramObject_v, "u_m_matrix");
	vUniform_v = gl.getUniformLocation(shaderProgramObject_v, "u_v_matrix");
	pUniform_v = gl.getUniformLocation(shaderProgramObject_v, "u_p_matrix");
	
	ldUniform_v = gl.getUniformLocation(shaderProgramObject_v, "u_ld");
	laUniform_v = gl.getUniformLocation(shaderProgramObject_v, "u_la");
	lsUniform_v = gl.getUniformLocation(shaderProgramObject_v, "u_ls");
    
    kdUniform_v = gl.getUniformLocation(shaderProgramObject_v, "u_kd");
    kaUniform_v = gl.getUniformLocation(shaderProgramObject_v, "u_ka");
    ksUniform_v = gl.getUniformLocation(shaderProgramObject_v, "u_ks");
    
    lightPositionUniform_v = gl.getUniformLocation(shaderProgramObject_v, "u_light_position");
  
    materialShininessUniform_v = gl.getUniformLocation(shaderProgramObject_v, "u_shininess");
    keyPressedUniform_v = gl.getUniformLocation(shaderProgramObject_v, "u_enable_lighting");


    // **** vertices, colors, shader attribs, vbo, vao initializations *****
    sphere = new Mesh();
    makeSphere(sphere, 2.0, 30, 30);

	// set clear color
	gl.clearColor(0.0, 0.0, 0.0, 1.0); // blue colored screen
	//enable depth
	gl.clearDepth(1.0);
	gl.enable(gl.DEPTH_TEST);
	gl.depthFunc(gl.LEQUAL);

	// initialize projection matrix
	perspectiveProjectionMatrix = mat4.create();
}

function resize()
{
	//code
	if(bFullscreen == true)
	{
		canvas.width = window.innerWidth;
		canvas.height = window.innerHeight;
	}
	else
	{
		canvas.width = canvas_original_width;
		canvas.height = canvas_original_height;
	}

	//set the viewport to match
	gl.viewport(0, 0, canvas.width, canvas.height);

	//perspective projection:
    mat4.perspective(perspectiveProjectionMatrix, 45.0, parseFloat(canvas.width)/parseFloat(canvas.height), 0.1, 100.0);
}


function degToRad(degrees)
{
	///code
	return(degrees * Math.PI / 100);
}

function draw()
{
	//code
	gl.clear(gl.COLOR_BUFFER_BIT|gl.DEPTH_BUFFER_BIT);

	var modelMatrix 		= mat4.create();
	var viewMatrix 			= mat4.create();
	var projectionMatrix 	= mat4.create();
	var translationMatrix 	= mat4.create();
	var scaleMatrix 		= mat4.create();

	mat4.translate(translationMatrix, translationMatrix, [0.0,0.0,-5.5]);
	mat4.multiply(modelMatrix, modelMatrix, translationMatrix);
	mat4.multiply(projectionMatrix, projectionMatrix, perspectiveProjectionMatrix);
	
	if(toggleLighting == true)		// per Fragment Lighting
	{
		gl.useProgram(shaderProgramObject_f);

		if(enableLighting == true)
		{
		 	gl.uniform1i(keyPressedUniform_f, 1);

	        gl.uniform3fv(laUniform_f, lightAmbient); 
	        gl.uniform3fv(ldUniform_f, lightDiffuse); 
	        gl.uniform3fv(lsUniform_f, lightSpecular);

	       	gl.uniform1f(materialShininessUniform_f, materialShininess);

	        gl.uniform3fv(kaUniform_f, materialAmbient);
	        gl.uniform3fv(kdUniform_f, materialDiffuse);
	        gl.uniform3fv(ksUniform_f, materialSpecular);
	        
	        gl.uniform4fv(lightPositionUniform_f, lightPosition);

		}
		else
		{
			gl.uniform1i(keyPressedUniform_f, 0);
		}

		// Send matrices to shaders in respective uniforms
	    gl.uniformMatrix4fv(mUniform_f, false, modelMatrix);
	    gl.uniformMatrix4fv(vUniform_f, false, viewMatrix);
	    gl.uniformMatrix4fv(pUniform_f, false, projectionMatrix);

		// Draw sphere
		sphere.draw();

		gl.useProgram(null);
	}
	else
	{
		gl.useProgram(shaderProgramObject_v);

		if(enableLighting == true)
		{
		 	gl.uniform1i(keyPressedUniform_v, 1);

	        gl.uniform3fv(laUniform_v, lightAmbient); 
	        gl.uniform3fv(ldUniform_v, lightDiffuse); 
	        gl.uniform3fv(lsUniform_v, lightSpecular);

	       	gl.uniform1f(materialShininessUniform_v, materialShininess);

	        gl.uniform3fv(kaUniform_v, materialAmbient);
	        gl.uniform3fv(kdUniform_v, materialDiffuse);
	        gl.uniform3fv(ksUniform_v, materialSpecular);
	        
	        gl.uniform4fv(lightPositionUniform_v, lightPosition);

		}
		else
		{
			gl.uniform1i(keyPressedUniform_v, 0);
		}

		// Send matrices to shaders in respective uniforms
	    gl.uniformMatrix4fv(mUniform_v, false, modelMatrix);
	    gl.uniformMatrix4fv(vUniform_v, false, viewMatrix);
	    gl.uniformMatrix4fv(pUniform_v, false, projectionMatrix);

		// Draw sphere
		sphere.draw();

		gl.useProgram(null);
	}
	

	//animation loop
	requestAnimationFrame(draw, canvas);
}

function uninitialize()
{
	//code
	if(sphere)
	{
		sphere.deallocate();
		sphere = null;
	}

	if(shaderProgramObject_f)
	{
		if(fragmentShaderObject_f)
		{
			gl.detachShader(shaderProgramObject_f, fragmentShaderObject_f);
			gl.deleteShader(fragmentShaderObject_f);
			fragmentShaderObject_f = null;
		}

		if(vertexShaderObject_f)
		{
			gl.detachShader(shaderProgramObject_f, vertexShaderObject_f);
			gl.deleteShader(vertexShaderObject_f);
			vertexShaderObject_f = null;
		}
		gl.deleteProgram(shaderProgramObject_f);
		shaderProgramObject_f = null;

	}

	if(shaderProgramObject_v)
	{
		if(fragmentShaderObject_v)
		{
			gl.detachShader(shaderProgramObject_v, fragmentShaderObject_v);
			gl.deleteShader(fragmentShaderObject_v);
			fragmentShaderObject_v = null;
		}

		if(vertexShaderObject_v)
		{
			gl.detachShader(shaderProgramObject_v, vertexShaderObject_v);
			gl.deleteShader(vertexShaderObject_v);
			vertexShaderObject_v = null;
		}
		gl.deleteProgram(shaderProgramObject_v);
		shaderProgramObject_v = null;

	}
}

function keyDown(event)
{
	//code
	switch(event.keyCode)
	{
		case 70: 	// for f or F
			toggleFullScreen();

			break;
		case 108: 		// l
		case 76: 		// L
			if(enableLighting == false)
			{
				enableLighting = true;
			}
			else
				enableLighting = false;
			break;
		case 84:
		case 116:
			if(toggleLighting == false)
			{
				toggleLighting = true;
				enableLighting = false;
			}
			else
			{
				toggleLighting = false;
				enableLighting = false;
			}	
			break;
		case 27:
			uninitialize();
			window.close();
			break;
	}
}

function mouseDown()
{
	//code
}
