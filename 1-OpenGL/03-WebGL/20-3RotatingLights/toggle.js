//global variables
var canvas = null;
var gl = null;			// webgl context
var bFullscreen = false;
var canvas_original_width;
var canvas_original_height;

const WebGLMacros = //when whole 'WebGLMacros' is const all inside it are automaticaly const
{
	AMC_ATTRIBUTE_VERTEX:0,
	AMC_ATTRIBUTE_COLOR:1,
	AMC_ATTRIBUTE_NORMAL:2,
	AMC_ATTRIBUTE_TEXCOORD0:3,
}

var vertexShaderObject_f;
var fragmentShaderObject_f;
var shaderProgramObject_f;
var vertexShaderObject_v;
var fragmentShaderObject_v;
var shaderProgramObject_v;

var mUniform_f, vUniform_f, pUniform_f;
var red_ldUniform_f, green_ldUniform_f, blue_ldUniform_f, kdUniform_f;
var red_laUniform_f, green_laUniform_f, blue_laUniform_f, kaUniform_f;
var red_lsUniform_f, green_lsUniform_f, blue_lsUniform_f, ksUniform_f;
var materialShininessUniform_f;
var red_lightPositionUniform_f;
var green_lightPositionUniform_f;
var blue_lightPositionUniform_f;
var keyPressedUniform_f;

var mUniform_v, vUniform_v, pUniform_v;
var red_ldUniform_v, green_ldUniform_v, blue_ldUniform_v, kdUniform_v;
var red_laUniform_v, green_laUniform_v, blue_laUniform_v, kaUniform_v;
var red_lsUniform_v, green_lsUniform_v, blue_lsUniform_v, ksUniform_v;
var materialShininessUniform_v;
var red_lightPositionUniform_v;
var green_lightPositionUniform_v;
var blue_lightPositionUniform_v;
var keyPressedUniform_v;

// RED LIGHT PROPERTIES
var red_lightAmbient	= new Float32Array([0.0, 0.0, 0.0]);
var red_lightDiffuse	= new Float32Array([1.0, 0.0, 0.0]);
var red_lightSpecular	= new Float32Array([1.0, 0.0, 0.0]);
var red_lightPosition 	= new Float32Array([0.0, 0.0, 0.0, 1.0]);

// GREEN LIGHT PROPERTIES
var green_lightAmbient	= new Float32Array([0.0, 0.0, 0.0]);
var green_lightDiffuse	= new Float32Array([0.0, 1.0, 0.0]);
var green_lightSpecular	= new Float32Array([0.0, 1.0, 0.0]);
var green_lightPosition = new Float32Array([0.0, 0.0, 0.0, 1.0]);

// BLUE LIGHT PROPERTIES
var blue_lightAmbient	= new Float32Array([0.0, 0.0, 0.0]);
var blue_lightDiffuse	= new Float32Array([0.0, 0.0, 1.0]);
var blue_lightSpecular	= new Float32Array([0.0, 0.0, 1.0]);
var blue_lightPosition 	= new Float32Array([0.0, 0.0, 0.0, 1.0]);

// MATERIAL PROPERTIES
var materialAmbient  	= new Float32Array([0.0, 0.0, 0.0]);
var materialDiffuse  	= new Float32Array([1.0, 1.0, 1.0]);
var materialSpecular  	= new Float32Array([1.0, 1.0, 1.0]);
var materialShininess 	= 128.0;

var theta 		   = 0.0;
var toggleLighting = false;
var enableLighting = false;
var perspectiveProjectionMatrix;

var sphere = null;

// To start animation : To have requestAnimationFrame() to be called "cross-browser" compatible
var requestAnimationFrame = 
window.requestAnimationFrame ||
window.webkitRequestAnimationFrame ||
window.mozRequestAnimationFrame ||
window.oRequestAnimationFrame ||
window.msRequestAnimationFrame ||
null;

// To stop animation : To have cancelAnimationFrame() to be called "cross-browser" compatible
var cancelAnimationFrame = 
window.cancelAnimationFrame ||
window.webkitCancelAnimationFrame || window.webkitCancelRequestAnimationFrame ||
window.mozCancelAnimationFrame || window.mozCancelRequestAnimationFrame ||
window.oCancelAmimationFrame || window.oCancelRequestAnimationFrame ||
window.msCancelAnimationFrame || window.msCancelRequestAnimationFrame ||
null;

// onload function
function main()
{
	// get <canvas> element
	canvas = document.getElementById("AMC");
	if(!canvas)
		console.log("Obtaining Canvas Failed");
	else
		console.log("Obtaining Canvas Succeeded");

	canvas_original_width = canvas.width;
	canvas_original_height = canvas.height;
	
	// Register keyboard's keydown event handler
	window.addEventListener("keydown", keyDown, false);
	window.addEventListener("click", mouseDown, false);
	window.addEventListener("resize", resize, false);

	// initialize webgl
	init();

	//start drawing here as warming-up
	resize();
	draw();
}

function toggleFullScreen()
{
	//code
	var fullscreen_element = document.fullscreen_element ||
	document.webkitFullscreenElement ||
	document.mozFullScreenElement ||
	document.msFullscreenElement ||
	null;

	// if not fullscreen
	if(fullscreen_element == null)
	{
		if(canvas.requestFullscreen)
			canvas.requestFullscreen();
		else if(canvas.mozRequestFullScreen)
			canvas.mozRequestFullScreen();
		else if(canvas.webkitRequestFullscreen)
			canvas.webkitRequestFullscreen();
		else if(canvas.msRequestFullscreen)
			canvas.msRequestFullscreen();
		bFullscreen = true;
	}
	else 		// if already fullscreen
	{
		if(document.exitFullscreen)
			document.exitFullscreen();
		else if(document.mozCancelFullScreen)
			document.mozCancelFullScreen();
		else if(document.webkitExitFullscreen)
			document.webkitExitFullscreen()
		else if(document.webkitExitFullscreen)
			document.webkitExitFullscreen();
		bFullscreen = false;
	}
}

function init()
{
	//code
	// get webgl 2.0 context
	gl = canvas.getContext("webgl2");
	if(!gl)
	{
		console.log("Failed to get the rendering context for WebGL");
		return;
	}

	gl.viewportWidth = canvas.width;
	gl.viewportHeight = canvas.height;

	//vertex shader
	vertexShaderObject_f =gl.createShader(gl.VERTEX_SHADER);

	var vertexShaderSourceCode_f = 
	"	#version 300 es 																	" +
	"																	                \n  " +
    "	in vec4 vPosition;													                " +
    "   in vec3 vNormal;                                                                    " +
    "																	                \n  " +
    "   out vec3 tNorm;                                                                     " +
    "   out vec3 red_lightDirection;                                                            " +
    "   out vec3 green_lightDirection;                                                            " +
    "   out vec3 blue_lightDirection;                                                            " +
    "   out vec3 viewerVector;                                                              " +
    "																	                \n  " +
    "	uniform mat4 u_m_matrix;											                " +
    "	uniform mat4 u_v_matrix;											                " +
    "	uniform mat4 u_p_matrix;											                " +
    "   uniform vec4 u_red_light_position;                                                      " +
    "   uniform vec4 u_green_light_position;                                                      " +
    "   uniform vec4 u_blue_light_position;                                                      " +
    "   uniform mediump int u_enable_lighting;                                              " +
    "																	                \n  " +
    "	void main(void)														                " +
    "	{																	                " +
    "       if(u_enable_lighting == 1)                                                      " +
    "       {                                                                               " +
    "           vec4 eyeCoordinates = u_v_matrix * u_m_matrix * vPosition;                  " +
    "           tNorm = mat3(u_v_matrix * u_m_matrix) * vNormal;                            " +
    "           red_lightDirection = vec3(u_red_light_position - eyeCoordinates);                   " +
    "           green_lightDirection = vec3(u_green_light_position - eyeCoordinates);                   " +
    "           blue_lightDirection = vec3(u_blue_light_position - eyeCoordinates);                   " +
    "           viewerVector = vec3(-eyeCoordinates);                                       " +
    "       }                                                                               " +
    "		gl_Position = u_p_matrix * u_v_matrix * u_m_matrix * vPosition;	                " +
    "	}																	                ";

	gl.shaderSource(vertexShaderObject_f, vertexShaderSourceCode_f);
	gl.compileShader(vertexShaderObject_f);
	if(gl.getShaderParameter(vertexShaderObject_f, gl.COMPILE_STATUS) == false)
	{
		var error = gl.getShaderInfoLog(vertexShaderObject_f);
		if(error.length > 0)
		{
			alert(error);
			uninitialize();
		}
	}

	//fragment shader
	fragmentShaderObject_f = gl.createShader(gl.FRAGMENT_SHADER);

	var fragmentShaderSourceCode_f = 
	"	#version 300 es 																			" +
	"																							\n	" +
	"	precision highp float;																	\n	" +
	"															                        		\n  " +
    "	in vec3 red_lightDirection; 																" +
	"	in vec3 green_lightDirection; 																" +
	"	in vec3 blue_lightDirection; 																" +
    "   in vec3 viewerVector;                                                           		    " +
    "   in vec3 tNorm;                                                                  		    " +
    "															                        		\n  " +
    "	out vec4 FragColor;										                        		    " +
    "                                                                                   		\n  " +
    "	uniform vec3 u_red_ld;																		" +
	"	uniform vec3 u_red_la;																		" +
	"	uniform vec3 u_red_ls;																		" +
	"	uniform vec3 u_green_ld;																	" +
	"	uniform vec3 u_green_la;																	" +
	"	uniform vec3 u_green_ls;																	" +
	"	uniform vec3 u_blue_ld;																		" +
	"	uniform vec3 u_blue_la;																		" +
	"	uniform vec3 u_blue_ls;																		" +
	"   uniform vec3 u_ka;                                                              		    " +
    "   uniform vec3 u_kd;                                                              		    " +
    "   uniform vec3 u_ks;                                                              		    " +
    "   uniform float u_shininess;                                                      		    " +
    "   uniform int u_enable_lighting;                                                              " +
    "															                                \n  " +
    "	void main(void)											                                	" +
    "	{														                               	    " +
    "       vec3 phong_ads_lights;                                                                  " +
    "		vec3 norm_tNorm;																		" +
	"		vec3 norm_red_lightDirection;															" +
	"		vec3 norm_green_lightDirection;															" +
	"		vec3 norm_blue_lightDirection;															" +
	"		vec3 norm_viewerVector;																	" +
	"                                                                                           \n  " +
    "       if(u_enable_lighting == 1)                                                              " +
    "       {                                                                                       " +
    "			norm_tNorm = normalize(tNorm);														" +
	"			norm_red_lightDirection = normalize(red_lightDirection);							" +
	"			norm_green_lightDirection = normalize(green_lightDirection);						" +
	"			norm_blue_lightDirection = normalize(blue_lightDirection);							" +
	"			norm_viewerVector = normalize(viewerVector);										" +
	"																							\n	" +
	"			vec3 red_reflectionVector = reflect(-norm_red_lightDirection, norm_tNorm);			" +
	"			vec3 green_reflectionVector = reflect(-norm_green_lightDirection, norm_tNorm);		" +
	"			vec3 blue_reflectionVector = reflect(-norm_blue_lightDirection, norm_tNorm);		" +
	"																							\n	" +
	"			float red_tnDotld = max(dot(norm_tNorm, norm_red_lightDirection), 0.0);				" +
	"			float green_tnDotld = max(dot(norm_tNorm, norm_green_lightDirection), 0.0);			" +
	"			float blue_tnDotld = max(dot(norm_tNorm, norm_blue_lightDirection), 0.0);			" +
	"																							\n	" +
	"			float red_rvDotvv = max(dot(red_reflectionVector, norm_viewerVector), 0.0);			" +
	"			float green_rvDotvv = max(dot(green_reflectionVector, norm_viewerVector), 0.0);		" +
	"			float blue_rvDotvv = max(dot(blue_reflectionVector, norm_viewerVector), 0.0);		" +
	"																							\n	" +
	"			vec3 red_ambient = u_red_la * u_ka;													" +
	"			vec3 red_diffuse = u_red_ld * u_kd * red_tnDotld;									" +
	"			vec3 red_specular = u_red_ls * u_ks * pow(red_rvDotvv, u_shininess);				" +
	"																							\n	" +
	"			vec3 green_ambient = u_green_la * u_ka;												" +
	"			vec3 green_diffuse = u_green_ld * u_kd * green_tnDotld;								" +
	"			vec3 green_specular = u_green_ls * u_ks * pow(green_rvDotvv, u_shininess);			" +
	"																							\n	" +
	"			vec3 blue_ambient = u_blue_la * u_ka;												" +
	"			vec3 blue_diffuse = u_blue_ld * u_kd * blue_tnDotld;								" +
	"			vec3 blue_specular = u_blue_ls * u_ks * pow(blue_rvDotvv, u_shininess);				" +
	"																							\n	" +
	"			vec3 ambient = red_ambient + green_ambient  + blue_ambient ;						" +
	"			vec3 diffuse = red_diffuse + green_diffuse  + blue_diffuse  ;						" +
	"			vec3 specular =red_specular+ green_specular + blue_specular;						" +
	"																							\n	" +
	"			phong_ads_lights = ambient + diffuse + specular;									" +
	"																							\n	" +
	"			FragColor = vec4(phong_ads_lights, 1.0);											" +
	"		}																					\n	" +
	"       else                                                                                	" +
    "       {                                                                                   	" +
    "           phong_ads_lights    = vec3(1.0, 1.0, 1.0);                                      	" +
    "       	FragColor = vec4(phong_ads_lights, 1.0);                                           	" +
    "       }                                                                                   	" +
    "	}															                            	";

	gl.shaderSource(fragmentShaderObject_f, fragmentShaderSourceCode_f);
	gl.compileShader(fragmentShaderObject_f);
	if(gl.getShaderParameter(fragmentShaderObject_f, gl.COMPILE_STATUS) == false)
	{
		var error = gl.getShaderInfoLog(fragmentShaderObject_f);
		if(error.length > 0)
		{
			alert(error);
			uninitialize();
		}
	}

	//shader program object
	shaderProgramObject_f = gl.createProgram();
	gl.attachShader(shaderProgramObject_f, vertexShaderObject_f);
	gl.attachShader(shaderProgramObject_f, fragmentShaderObject_f);

	//pre linking binding to shader program object with shader attributes
	gl.bindAttribLocation(shaderProgramObject_f, WebGLMacros.AMC_ATTRIBUTE_VERTEX, "vPosition");
	gl.bindAttribLocation(shaderProgramObject_f, WebGLMacros.AMC_ATTRIBUTE_NORMAL, "vNormal");

	// linking
	gl.linkProgram(shaderProgramObject_f);
	if(!gl.getProgramParameter(shaderProgramObject_f, gl.LINK_STATUS))
	{
		var error = gl.getProgramInfoLog(shaderProgramObject_f);
		if(error.length > 0)
		{
			alert(error);
			uninitialize();
		}
	}

	// get mvp uniform location
	mUniform_f = gl.getUniformLocation(shaderProgramObject_f, "u_m_matrix");
	vUniform_f = gl.getUniformLocation(shaderProgramObject_f, "u_v_matrix");
	pUniform_f = gl.getUniformLocation(shaderProgramObject_f, "u_p_matrix");
	
	// red light uniform location
	red_ldUniform_f = gl.getUniformLocation(shaderProgramObject_f, "u_red_ld");
	red_laUniform_f = gl.getUniformLocation(shaderProgramObject_f, "u_red_la");
	red_lsUniform_f = gl.getUniformLocation(shaderProgramObject_f, "u_red_ls");
	red_lightPositionUniform_f = gl.getUniformLocation(shaderProgramObject_f, "u_red_light_position" );
	// green light uniform location
	green_ldUniform_f = gl.getUniformLocation(shaderProgramObject_f, "u_green_ld");
	green_laUniform_f = gl.getUniformLocation(shaderProgramObject_f, "u_green_la");
	green_lsUniform_f = gl.getUniformLocation(shaderProgramObject_f, "u_green_ls");
	green_lightPositionUniform_f = gl.getUniformLocation(shaderProgramObject_f, "u_green_light_position" );
	// blue light uniform location
	blue_ldUniform_f = gl.getUniformLocation(shaderProgramObject_f, "u_blue_ld");
	blue_laUniform_f = gl.getUniformLocation(shaderProgramObject_f, "u_blue_la");
	blue_lsUniform_f = gl.getUniformLocation(shaderProgramObject_f, "u_blue_ls");
	blue_lightPositionUniform_f = gl.getUniformLocation(shaderProgramObject_f, "u_blue_light_position" );
	
    kdUniform_f = gl.getUniformLocation(shaderProgramObject_f, "u_kd");
    kaUniform_f = gl.getUniformLocation(shaderProgramObject_f, "u_ka");
    ksUniform_f = gl.getUniformLocation(shaderProgramObject_f, "u_ks");
    
    lightPositionUniform_f = gl.getUniformLocation(shaderProgramObject_f, "u_light_position");
  
    materialShininessUniform_f = gl.getUniformLocation(shaderProgramObject_f, "u_shininess");
    keyPressedUniform_f = gl.getUniformLocation(shaderProgramObject_f, "u_enable_lighting");


    //--------------------------------------------------------------------------------------------------
    //vertex shader
	vertexShaderObject_v =gl.createShader(gl.VERTEX_SHADER);

	var vertexShaderSourceCode_v = 
	"	#version 300 es 																				" +
	"																								\n 	" +
    "	in vec4 vPosition;													         				    " +
    "   in vec3 vNormal;                                                             				    " +
    "																	             				\n  " +
    "	out vec3 phong_ads_lights;													 				    " +
    "																	             				\n  " +
    "	uniform mat4 u_m_matrix;											         				    " +
    "	uniform mat4 u_v_matrix;											         				    " +
    "	uniform mat4 u_p_matrix;											         				    " +
    "   uniform vec3 u_red_la;                                                               				" +
    "   uniform vec3 u_red_ld;                                                               				" +
    "   uniform vec3 u_red_ls;                                                               				" +
    "   uniform vec3 u_green_la;                                                               				" +
    "   uniform vec3 u_green_ld;                                                               				" +
    "   uniform vec3 u_green_ls;                                                               				" +
    "   uniform vec3 u_blue_la;                                                               				" +
    "   uniform vec3 u_blue_ld;                                                               				" +
    "   uniform vec3 u_blue_ls;                                                               				" +
    "   uniform vec3 u_ka;                                                             				    " +
    "   uniform vec3 u_kd;                                                             				    " +
    "   uniform vec3 u_ks;                                                             				    " +
    "   uniform float u_shininess;                                                     				    " +
    "   uniform vec4 u_red_light_position;                      				 						    " +
    "   uniform vec4 u_green_light_position;                      				 						    " +
    "   uniform vec4 u_blue_light_position;                      				 						    " +
    "   uniform mediump int u_enable_lighting;                                                 				    " +
    "																	               				\n  " +
    "	void main(void)														           				    " +
    "	{																	           				    " +
    "       if(u_enable_lighting == 1)                                                 				    " +
    "       {                                                                          				    " +
    "           vec4 eyeCoordinates = u_v_matrix * u_m_matrix * vPosition;             				    " +
    "           mat3 normalMatrix   = mat3(u_v_matrix * u_m_matrix);                   				    " +
    "           vec3 tNorm          = normalize(normalMatrix * vNormal);               				    " +
    "           vec3 viewerVector   = normalize(vec3(-eyeCoordinates));   								" +
    "																	               				\n  " +
    "           vec3 red_lightDirection   = normalize(vec3(u_red_light_position - eyeCoordinates)); 	" +
    "           float red_tnDotld         = max(dot(tNorm, red_lightDirection), 0.0);               	" +
    "           vec3 red_reflectionVector = reflect(-red_lightDirection, tNorm);                    	" +
    "           float red_rvDotvv         = max(dot(red_reflectionVector, viewerVector), 0.0);   		" +
    "                                                                                   			\n  " +
    "           vec3 green_lightDirection   = normalize(vec3(u_green_light_position - eyeCoordinates));	" +
    "           float green_tnDotld         = max(dot(tNorm, green_lightDirection), 0.0);            	" +
    "           vec3 green_reflectionVector = reflect(-green_lightDirection, tNorm);                 	" +
    "           float green_rvDotvv         = max(dot(green_reflectionVector, viewerVector), 0.0);   	" +
    "                                                                                   			\n  " +
    "           vec3 blue_lightDirection   = normalize(vec3(u_blue_light_position - eyeCoordinates));	" +
    "           float blue_tnDotld         = max(dot(tNorm, blue_lightDirection), 0.0);             	" +
    "           vec3 blue_reflectionVector = reflect(-blue_lightDirection, tNorm);                  	" +
    "           float blue_rvDotvv         = max(dot(blue_reflectionVector, viewerVector), 0.0);   		" +
    "                                                                                   			\n  " +
    "           vec3 red_ambient   = u_red_la * u_ka;                                          			" +
    "           vec3 red_diffuse   = u_red_ld * u_kd * red_tnDotld;                                		" +
    "           vec3 red_specular  = u_red_ls * u_ks * pow(red_rvDotvv, u_shininess);              		" +
    "                                                                                   			\n  " +
    "           vec3 green_ambient   = u_green_la * u_ka;                                          		" +
    "           vec3 green_diffuse   = u_green_ld * u_kd * green_tnDotld;                           	" +
    "           vec3 green_specular  = u_green_ls * u_ks * pow(green_rvDotvv, u_shininess);         	" +
    "                                                                                   			\n  " +
    "           vec3 blue_ambient   = u_blue_la * u_ka;                                          		" +
    "           vec3 blue_diffuse   = u_blue_ld * u_kd * blue_tnDotld;                              	" +
    "           vec3 blue_specular  = u_blue_ls * u_ks * pow(blue_rvDotvv, u_shininess);            	" +
    "                                                                                   			\n  " +
    "           vec3 ambient   = red_ambient + green_ambient + blue_ambient;           					" +
    "           vec3 diffuse   = red_diffuse + green_diffuse + blue_diffuse;           					" +
    "           vec3 specular  = red_specular + green_specular + blue_specular;         				" +
    "                                                                                   			\n  " +
    "           phong_ads_lights    =  ambient + diffuse + specular ;                     			    " +
    "       }                                                                           			    " +
    "       else                                                                        			    " +
    "       {                                                                           			    " +
    "           phong_ads_lights   = vec3(1.0, 1.0, 1.0);                               			    " +
    "       }                                                                           			    " +
    "                                                                                   			\n  " +
    "		gl_Position = u_p_matrix * u_v_matrix * u_m_matrix * vPosition;								" +
    "	}																								";

	gl.shaderSource(vertexShaderObject_v, vertexShaderSourceCode_v);
	gl.compileShader(vertexShaderObject_v);
	if(gl.getShaderParameter(vertexShaderObject_v, gl.COMPILE_STATUS) == false)
	{
		var error = gl.getShaderInfoLog(vertexShaderObject_v);
		if(error.length > 0)
		{
			alert(error);
			uninitialize();
		}
	}

	//fragment shader
	fragmentShaderObject_v = gl.createShader(gl.FRAGMENT_SHADER);

	var fragmentShaderSourceCode_v = 
	"	#version 300 es 												" +
	"															\n	" +
	"	precision highp float;									\n	" +
    "	in vec3 phong_ads_lights;									" +
    "															\n  " +
    "	out vec4 FragColor;											" +
    "															\n  " +
    "	void main(void)												" +
    "	{															" +
    "           FragColor = vec4(phong_ads_lights, 1.0);            " +
    "	}															";

	gl.shaderSource(fragmentShaderObject_v, fragmentShaderSourceCode_v);
	gl.compileShader(fragmentShaderObject_v);
	if(gl.getShaderParameter(fragmentShaderObject_v, gl.COMPILE_STATUS) == false)
	{
		var error = gl.getShaderInfoLog(fragmentShaderObject_v);
		if(error.length > 0)
		{
			alert(error);
			uninitialize();
		}
	}

	//shader program object
	shaderProgramObject_v = gl.createProgram();
	gl.attachShader(shaderProgramObject_v, vertexShaderObject_v);
	gl.attachShader(shaderProgramObject_v, fragmentShaderObject_v);

	//pre linking binding to shader program object with shader attributes
	gl.bindAttribLocation(shaderProgramObject_v, WebGLMacros.AMC_ATTRIBUTE_VERTEX, "vPosition");
	gl.bindAttribLocation(shaderProgramObject_v, WebGLMacros.AMC_ATTRIBUTE_NORMAL, "vNormal");

	// linking
	gl.linkProgram(shaderProgramObject_v);
	if(!gl.getProgramParameter(shaderProgramObject_v, gl.LINK_STATUS))
	{
		var error = gl.getProgramInfoLog(shaderProgramObject_v);
		if(error.length > 0)
		{
			alert(error);
			uninitialize();
		}
	}

	// get mvp uniform location
	mUniform_v = gl.getUniformLocation(shaderProgramObject_v, "u_m_matrix");
	vUniform_v = gl.getUniformLocation(shaderProgramObject_v, "u_v_matrix");
	pUniform_v = gl.getUniformLocation(shaderProgramObject_v, "u_p_matrix");
	
	red_ldUniform_v = gl.getUniformLocation(shaderProgramObject_v, "u_red_ld");
	red_laUniform_v = gl.getUniformLocation(shaderProgramObject_v, "u_red_la");
	red_lsUniform_v = gl.getUniformLocation(shaderProgramObject_v, "u_red_ls");
    
    green_ldUniform_v = gl.getUniformLocation(shaderProgramObject_v, "u_green_ld");
	green_laUniform_v = gl.getUniformLocation(shaderProgramObject_v, "u_green_la");
	green_lsUniform_v = gl.getUniformLocation(shaderProgramObject_v, "u_green_ls");
    
    blue_ldUniform_v = gl.getUniformLocation(shaderProgramObject_v, "u_blue_ld");
	blue_laUniform_v = gl.getUniformLocation(shaderProgramObject_v, "u_blue_la");
	blue_lsUniform_v = gl.getUniformLocation(shaderProgramObject_v, "u_blue_ls");
    
    kdUniform_v = gl.getUniformLocation(shaderProgramObject_v, "u_kd");
    kaUniform_v = gl.getUniformLocation(shaderProgramObject_v, "u_ka");
    ksUniform_v = gl.getUniformLocation(shaderProgramObject_v, "u_ks");
    
    red_lightPositionUniform_v = gl.getUniformLocation(shaderProgramObject_v, "u_red_light_position");
    green_lightPositionUniform_v = gl.getUniformLocation(shaderProgramObject_v, "u_green_light_position");
    blue_lightPositionUniform_v = gl.getUniformLocation(shaderProgramObject_v, "u_blue_light_position");
  
    materialShininessUniform_v = gl.getUniformLocation(shaderProgramObject_v, "u_shininess");
    keyPressedUniform_v = gl.getUniformLocation(shaderProgramObject_v, "u_enable_lighting");


    // **** vertices, colors, shader attribs, vbo, vao initializations *****
    sphere = new Mesh();
    makeSphere(sphere, 2.0, 30, 30);

	// set clear color
	gl.clearColor(0.0, 0.0, 0.0, 1.0); // blue colored screen
	//enable depth
	gl.clearDepth(1.0);
	gl.enable(gl.DEPTH_TEST);
	gl.depthFunc(gl.LEQUAL);

	// initialize projection matrix
	perspectiveProjectionMatrix = mat4.create();
}

function resize()
{
	//code
	if(bFullscreen == true)
	{
		canvas.width = window.innerWidth;
		canvas.height = window.innerHeight;
	}
	else
	{
		canvas.width = canvas_original_width;
		canvas.height = canvas_original_height;
	}

	//set the viewport to match
	gl.viewport(0, 0, canvas.width, canvas.height);

	//perspective projection:
    mat4.perspective(perspectiveProjectionMatrix, 45.0, parseFloat(canvas.width)/parseFloat(canvas.height), 0.1, 100.0);
}


function degToRad(degrees)
{
	///code
	return(degrees * Math.PI / 100);
}

function draw()
{
	//code
	gl.clear(gl.COLOR_BUFFER_BIT|gl.DEPTH_BUFFER_BIT);

	var modelMatrix 		= mat4.create();
	var viewMatrix 			= mat4.create();
	var projectionMatrix 	= mat4.create();
	var translationMatrix 	= mat4.create();
	var scaleMatrix 		= mat4.create();

	mat4.translate(translationMatrix, translationMatrix, [0.0,0.0,-5.5]);
	mat4.multiply(modelMatrix, modelMatrix, translationMatrix);
	mat4.multiply(projectionMatrix, projectionMatrix, perspectiveProjectionMatrix);
	
	
	if(toggleLighting == true)		// per Fragment Lighting
	{
		gl.useProgram(shaderProgramObject_f);

		if(enableLighting == true)
		{
			var cosvalue = 10.0 * Math.cos(degToRad(theta));
			var sinvalue = 10.0 * Math.sin(degToRad(theta));

			red_lightPosition[0] = 0.0;
			red_lightPosition[1] = cosvalue;
			red_lightPosition[2] = sinvalue;

			green_lightPosition[0] = cosvalue;
			green_lightPosition[1] = 0.0;
			green_lightPosition[2] = sinvalue;

			blue_lightPosition[0] = cosvalue;
			blue_lightPosition[1] = sinvalue;
			blue_lightPosition[2] = 0.0;

		 	gl.uniform1i(keyPressedUniform_f, 1);

	        gl.uniform3fv(red_laUniform_f, red_lightAmbient); 
	        gl.uniform3fv(red_ldUniform_f, red_lightDiffuse); 
	        gl.uniform3fv(red_lsUniform_f, red_lightSpecular);

	        gl.uniform3fv(green_laUniform_f, green_lightAmbient); 
	        gl.uniform3fv(green_ldUniform_f, green_lightDiffuse); 
	        gl.uniform3fv(green_lsUniform_f, green_lightSpecular);

	        gl.uniform3fv(blue_laUniform_f, blue_lightAmbient); 
	        gl.uniform3fv(blue_ldUniform_f, blue_lightDiffuse); 
	        gl.uniform3fv(blue_lsUniform_f, blue_lightSpecular);


	       	gl.uniform1f(materialShininessUniform_f, materialShininess);

	        gl.uniform3fv(kaUniform_f, materialAmbient);
	        gl.uniform3fv(kdUniform_f, materialDiffuse);
	        gl.uniform3fv(ksUniform_f, materialSpecular);
	        
	        gl.uniform4fv(green_lightPositionUniform_f, green_lightPosition);
	        gl.uniform4fv(red_lightPositionUniform_f, red_lightPosition);
	        gl.uniform4fv(blue_lightPositionUniform_f, blue_lightPosition);

		}
		else
		{
			gl.uniform1i(keyPressedUniform_f, 0);
		}

		// Send matrices to shaders in respective uniforms
	    gl.uniformMatrix4fv(mUniform_f, false, modelMatrix);
	    gl.uniformMatrix4fv(vUniform_f, false, viewMatrix);
	    gl.uniformMatrix4fv(pUniform_f, false, projectionMatrix);

		// Draw sphere
		sphere.draw();

		gl.useProgram(null);
	}
	else
	{
		gl.useProgram(shaderProgramObject_v);

		if(enableLighting == true)
		{
			var cosvalue = 10.0 * Math.cos(degToRad(theta));
			var sinvalue = 10.0 * Math.sin(degToRad(theta));

			red_lightPosition[0] = 0.0;
			red_lightPosition[1] = cosvalue;
			red_lightPosition[2] = sinvalue;

			green_lightPosition[0] = cosvalue;
			green_lightPosition[1] = 0.0;
			green_lightPosition[2] = sinvalue;

			blue_lightPosition[0] = cosvalue;
			blue_lightPosition[1] = sinvalue;
			blue_lightPosition[2] = 0.0;

		 	gl.uniform1i(keyPressedUniform_v, 1);

	        gl.uniform3fv(red_laUniform_v, red_lightAmbient); 
	        gl.uniform3fv(red_ldUniform_v, red_lightDiffuse); 
	        gl.uniform3fv(red_lsUniform_v, red_lightSpecular);

	        gl.uniform3fv(green_laUniform_v, green_lightAmbient); 
	        gl.uniform3fv(green_ldUniform_v, green_lightDiffuse); 
	        gl.uniform3fv(green_lsUniform_v, green_lightSpecular);

	        gl.uniform3fv(blue_laUniform_v, blue_lightAmbient); 
	        gl.uniform3fv(blue_ldUniform_v, blue_lightDiffuse); 
	        gl.uniform3fv(blue_lsUniform_v, blue_lightSpecular);

	       	gl.uniform1f(materialShininessUniform_v, materialShininess);

	        gl.uniform3fv(kaUniform_v, materialAmbient);
	        gl.uniform3fv(kdUniform_v, materialDiffuse);
	        gl.uniform3fv(ksUniform_v, materialSpecular);
	        
	        gl.uniform4fv(green_lightPositionUniform_v, green_lightPosition);
	        gl.uniform4fv(red_lightPositionUniform_v, red_lightPosition);
	        gl.uniform4fv(blue_lightPositionUniform_v, blue_lightPosition);

		}
		else
		{
			gl.uniform1i(keyPressedUniform_v, 0);
		}

		// Send matrices to shaders in respective uniforms
	    gl.uniformMatrix4fv(mUniform_v, false, modelMatrix);
	    gl.uniformMatrix4fv(vUniform_v, false, viewMatrix);
	    gl.uniformMatrix4fv(pUniform_v, false, projectionMatrix);

		// Draw sphere
		sphere.draw();

		gl.useProgram(null);
	}
	

	//animation loop
	requestAnimationFrame(draw, canvas);
	theta = theta + 0.5;
	//if(theta > 360.0)
	//	theta =0.0;
}

function uninitialize()
{
	//code
	if(sphere)
	{
		sphere.deallocate();
		sphere = null;
	}

	if(shaderProgramObject_f)
	{
		if(fragmentShaderObject_f)
		{
			gl.detachShader(shaderProgramObject_f, fragmentShaderObject_f);
			gl.deleteShader(fragmentShaderObject_f);
			fragmentShaderObject_f = null;
		}

		if(vertexShaderObject_f)
		{
			gl.detachShader(shaderProgramObject_f, vertexShaderObject_f);
			gl.deleteShader(vertexShaderObject_f);
			vertexShaderObject_f = null;
		}
		gl.deleteProgram(shaderProgramObject_f);
		shaderProgramObject_f = null;

	}

	if(shaderProgramObject_v)
	{
		if(fragmentShaderObject_v)
		{
			gl.detachShader(shaderProgramObject_v, fragmentShaderObject_v);
			gl.deleteShader(fragmentShaderObject_v);
			fragmentShaderObject_v = null;
		}

		if(vertexShaderObject_v)
		{
			gl.detachShader(shaderProgramObject_v, vertexShaderObject_v);
			gl.deleteShader(vertexShaderObject_v);
			vertexShaderObject_v = null;
		}
		gl.deleteProgram(shaderProgramObject_v);
		shaderProgramObject_v = null;

	}
}

function keyDown(event)
{
	//code
	switch(event.keyCode)
	{
		case 70: 	// for f or F
			toggleFullScreen();

			break;
		case 108: 		// l
		case 76: 		// L
			if(enableLighting == false)
			{
				enableLighting = true;
			}
			else
				enableLighting = false;
			break;
		case 84:
		case 116:
			if(toggleLighting == false)
			{
				toggleLighting = true;
				enableLighting = false;
			}
			else
			{
				toggleLighting = false;
				enableLighting = false;
			}	
			break;
		case 27:
			uninitialize();
			window.close();
			break;
	}
}

function mouseDown()
{
	//code
}
