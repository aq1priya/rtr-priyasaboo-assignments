//global variables
var canvas = null;
var gl = null;			// webgl context
var bFullscreen = false;
var canvas_original_width;
var canvas_original_height;

const WebGLMacros = //when whole 'WebGLMacros' is const all inside it are automaticaly const
{
	AMC_ATTRIBUTE_VERTEX:0,
	AMC_ATTRIBUTE_COLOR:1,
	AMC_ATTRIBUTE_NORMAL:2,
	AMC_ATTRIBUTE_TEXCOORD0:3,
}

var vertexShaderObject;
var fragmentShaderObject;
var shaderProgramObject;

var vao_cube;
var vbo_cube_data;

var samplerUniform;
var mUniform, vUniform, pUniform;	// ModelView Matrix Uniform and Projection Matrix Uniform
var LdUniform, KdUniform;			// Diffuse property of Light and diffuse property of material
var LaUniform, KaUniform;			// Ambient property of Light and ambient property of material
var LsUniform, KsUniform;			// Specular property of Light and specular property of material
var materialShininessUniform;		// material shininess
var lightPositionUniform;			// light position
var keyPressedUniform;				// is L key Pressed, to enable lighting effect

var textureMarble = 0;

var perspectiveProjectionMatrix;

var cubeAngle = 0.0;
var enableLighting =false;

var lightAmbient  = new Float32Array([0.0, 0.0, 0.0]);
var lightDiffuse  = new Float32Array([ 1.0, 1.0, 1.0, 1.0 ]);
var lightSpecular = new Float32Array([ 1.0, 1.0, 1.0, 1.0 ]);
var lightPosition = new Float32Array([ 10.0, 10.0, 10.0, 1.0 ]);

var materialAmbient  = new Float32Array([0.0, 0.0, 0.0]);
var materialDiffuse  = new Float32Array([ 1.0, 1.0, 1.0, 1.0 ]);
var materialSpecular = new Float32Array([ 1.0, 1.0, 1.0, 1.0 ]);
var materialShininess = 128.0;

// To start animation : To have requestAnimationFrame() to be called "cross-browser" compatible
var requestAnimationFrame = 
window.requestAnimationFrame ||
window.webkitRequestAnimationFrame ||
window.mozRequestAnimationFrame ||
window.oRequestAnimationFrame ||
window.msRequestAnimationFrame ||
null;

// To stop animation : To have cancelAnimationFrame() to be called "cross-browser" compatible
var cancelAnimationFrame = 
window.cancelAnimationFrame ||
window.webkitCancelAnimationFrame || window.webkitCancelRequestAnimationFrame ||
window.mozCancelAnimationFrame || window.mozCancelRequestAnimationFrame ||
window.oCancelAmimationFrame || window.oCancelRequestAnimationFrame ||
window.msCancelAnimationFrame || window.msCancelRequestAnimationFrame ||
null;

// onload function
function main()
{
	// get <canvas> element
	canvas = document.getElementById("AMC");
	if(!canvas)
		console.log("Obtaining Canvas Failed");
	else
		console.log("Obtaining Canvas Succeeded");

	canvas_original_width = canvas.width;
	canvas_original_height = canvas.height;
	
	// Register keyboard's keydown event handler
	window.addEventListener("keydown", keyDown, false);
	window.addEventListener("click", mouseDown, false);
	window.addEventListener("resize", resize, false);

	// initialize webgl
	init();

	//start drawing here as warming-up
	resize();
	draw();
}

function toggleFullScreen()
{
	//code
	var fullscreen_element = document.fullscreen_element ||
	document.webkitFullscreenElement ||
	document.mozFullScreenElement ||
	document.msFullscreenElement ||
	null;

	// if not fullscreen
	if(fullscreen_element == null)
	{
		if(canvas.requestFullscreen)
			canvas.requestFullscreen();
		else if(canvas.mozRequestFullScreen)
			canvas.mozRequestFullScreen();
		else if(canvas.webkitRequestFullscreen)
			canvas.webkitRequestFullscreen();
		else if(canvas.msRequestFullscreen)
			canvas.msRequestFullscreen();
		bFullscreen = true;
	}
	else 		// if already fullscreen
	{
		if(document.exitFullscreen)
			document.exitFullscreen();
		else if(document.mozCancelFullScreen)
			document.mozCancelFullScreen();
		else if(document.webkitExitFullscreen)
			document.webkitExitFullscreen()
		else if(document.webkitExitFullscreen)
			document.webkitExitFullscreen();
		bFullscreen = false;
	}
}

function init()
{
	//code
	// get webgl 2.0 context
	gl = canvas.getContext("webgl2");
	if(!gl)
	{
		console.log("Failed to get the rendering context for WebGL");
		return;
	}

	gl.viewportWidth = canvas.width;
	gl.viewportHeight = canvas.height;

	//vertex shader
	vertexShaderObject =gl.createShader(gl.VERTEX_SHADER);

	var vertexShaderSourceCode = 
	"#version 300 es 										" +
	"													\n 	" +
	"	in vec4 vPosition;																	" +
		"	in vec3 vNormal;																	" +
		"	in vec4 vColor;																	" +
		"	in vec2 vTexCoord;																	" +
		"																					  \n" +
		"	out vec4 out_color;																	" +
		"	out vec2 out_tex;																	" +
		"	out vec4 eyeCoordinates;															" +
		"	out vec3 tNorm;																		" +
		"	out vec3 lightDirection;															" +
		"	out vec3 viewerVector;																" +
		"																					  \n" +
		"	uniform mat4 u_m_matrix;															" +
		"	uniform mat4 u_v_matrix;															" +
		"	uniform mat4 u_p_matrix;															" +
		"	uniform mediump int   u_l_key_is_pressed;														" +
		"	uniform vec4 u_light_position;														" +
		"																					  \n" +
		"	void main(void)																		" +
		"	{																					" +
		"		if(u_l_key_is_pressed == 1)														" +
		"		{																				" +
		"			eyeCoordinates = u_v_matrix * u_m_matrix * vPosition;						" +
		"			tNorm = mat3(u_v_matrix * u_m_matrix) * vNormal;							" +
		"			lightDirection = vec3(u_light_position - eyeCoordinates);					" +
		"			viewerVector = vec3(-eyeCoordinates);										" +
		"		}																				" +
		"																					  \n" +
		"	gl_Position = u_v_matrix * u_p_matrix * u_m_matrix * vPosition;						" +
		"	out_tex = vTexCoord;															  \n" +
		"	out_color = vColor;																  \n" +
		"	}																					";


	gl.shaderSource(vertexShaderObject, vertexShaderSourceCode);
	gl.compileShader(vertexShaderObject);
	if(gl.getShaderParameter(vertexShaderObject, gl.COMPILE_STATUS) == false)
	{
		var error = gl.getShaderInfoLog(vertexShaderObject);
		if(error.length > 0)
		{
			alert(error);
			uninitialize();
		}
	}

	//fragment shader
	fragmentShaderObject = gl.createShader(gl.FRAGMENT_SHADER);

	var fragmentShaderSourceCode = 
	"#version 300 es 											" +
	"														\n	" +
	"precision highp float;										" +
	"	in vec3 tNorm;																			" +
		"	in vec3 lightDirection;																	" +
		"	in vec3 viewerVector;																	" +
		"	in vec2 out_tex;																	  \n" +
		"	in vec4 out_color;  																  \n" +
		"																						  \n" +
		"	out vec4 fragColor;																		" +
		"																						  \n" +
		"	uniform vec3 u_ld;																		" +
		"	uniform vec3 u_kd;																		" +
		"	uniform vec3 u_la;																		" +
		"	uniform vec3 u_ka;																		" +
		"	uniform vec3 u_ls;																		" +
		"	uniform vec3 u_ks;																		" +
		"	uniform float u_material_shininess;														" +
		"	uniform  mediump int u_l_key_is_pressed;															" +
		"	uniform sampler2D u_sampler;														  \n" +
		"																						  \n" +
		"	void main(void)																			" +
		"	{																						" +
		"		vec3 phong_ads_light;																" +
		"		vec4 texture_color;																  \n" +
		"																						  \n" +
		"		if(u_l_key_is_pressed == 1)															" +
		"		{																					" +
		"			vec3 normalizedtNorm = normalize(tNorm);										" +
		"			vec3 normalizedlightDirection = normalize(lightDirection);						" +
		"			vec3 normalizedviewerVector = normalize(viewerVector);							" +
		"																						  \n" +
		"			vec3 reflectionVector = normalize(reflect(-normalizedlightDirection, normalizedtNorm));" +
		"			float tnDotld = max(dot(normalizedlightDirection, normalizedtNorm), 0.0);		" +
		"			float rvDotvv = max(dot(reflectionVector, normalizedviewerVector), 0.0);		" +
		"																						  \n" +
		"			vec3 ambient = u_la * u_ka;														" +
		"			vec3 diffuse = u_ld * u_kd * tnDotld;											" +
		"			vec3 specular = u_ls * u_ks * pow(rvDotvv, u_material_shininess);				" +
		"			phong_ads_light = ambient + diffuse +	specular;								" +
		"																						  \n" +
		"		}																					" +
		"		else																				" +
		"		{																					" +
		"			phong_ads_light = vec3(1.0, 1.0, 1.0);											" +
		"		}																					" +
		"	texture_color = texture(u_sampler, out_tex);										  \n" +
		"																						  \n" +
		"	fragColor = texture_color * vec4(( vec3(out_color) *phong_ads_light), 1.0);			  \n" +
		"	}																						";

	gl.shaderSource(fragmentShaderObject, fragmentShaderSourceCode);
	gl.compileShader(fragmentShaderObject);
	if(gl.getShaderParameter(fragmentShaderObject, gl.COMPILE_STATUS) == false)
	{
		var error = gl.getShaderInfoLog(fragmentShaderObject);
		if(error.length > 0)
		{
			alert(error);
			uninitialize();
		}
	}

	//shader program object
	shaderProgramObject = gl.createProgram();
	gl.attachShader(shaderProgramObject, vertexShaderObject);
	gl.attachShader(shaderProgramObject, fragmentShaderObject);

	//pre linking binding to shader program object with shader attributes
	gl.bindAttribLocation(shaderProgramObject, WebGLMacros.AMC_ATTRIBUTE_VERTEX, "vPosition");
	gl.bindAttribLocation(shaderProgramObject, WebGLMacros.AMC_ATTRIBUTE_COLOR, "vColor");
	gl.bindAttribLocation(shaderProgramObject, WebGLMacros.AMC_ATTRIBUTE_NORMAL, "vNormal");
	gl.bindAttribLocation(shaderProgramObject, WebGLMacros.AMC_ATTRIBUTE_TEXCOORD0, "vTexcoord");

	// linking
	gl.linkProgram(shaderProgramObject);
	if(!gl.getProgramParameter(shaderProgramObject, gl.LINK_STATUS))
	{
		var error = gl.getProgramInfoLog(shaderProgramObject);
		if(error.length > 0)
		{
			alert(error);
			uninitialize();
		}
	}

	// get mvp uniform location
	vUniform = gl.getUniformLocation(shaderProgramObject, "u_v_matrix");
	mUniform = gl.getUniformLocation(shaderProgramObject, "u_m_matrix");
	pUniform = gl.getUniformLocation(shaderProgramObject, "u_p_matrix");
	LdUniform = gl.getUniformLocation(shaderProgramObject, "u_ld");
	KdUniform = gl.getUniformLocation(shaderProgramObject, "u_kd");
	LaUniform = gl.getUniformLocation(shaderProgramObject, "u_la");
	KaUniform = gl.getUniformLocation(shaderProgramObject, "u_ka");
	LsUniform = gl.getUniformLocation(shaderProgramObject, "u_ls");
	KsUniform = gl.getUniformLocation(shaderProgramObject, "u_ks");
	materialShininessUniform = gl.getUniformLocation(shaderProgramObject, "u_material_shininess");
	lightPositionUniform = gl.getUniformLocation(shaderProgramObject, "u_light_position");
	keyPressedUniform = gl.getUniformLocation(shaderProgramObject, "u_l_key_is_pressed");
	samplerUniform = gl.getUniformLocation(shaderProgramObject, "u_sampler");

	// load both the textures
	stoneTexture = gl.createTexture();
	stoneTexture.image = new Image();
	stoneTexture.image.src = "marble.bmp";
	stoneTexture.image.onload = function()
	{
		gl.bindTexture(gl.TEXTURE_2D, stoneTexture);
		gl.pixelStorei(gl.UNPACK_FLIP_Y_WEBGL, true);
		gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA, gl.RGBA, gl.UNSIGNED_BYTE, stoneTexture.image);
		gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.NEAREST);
		gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.NEAREST);
		gl.bindTexture(gl.TEXTURE_2D, null);
	}


	// data
	var cube_vcnt = new Float32Array([
		 1.0,  1.0,  1.0, 	1.0, 0.0, 0.0,	 0.0,  0.0,  1.0, 	1.0, 1.0, 
		-1.0,  1.0,  1.0, 	1.0, 0.0, 0.0,	 0.0,  0.0,  1.0,	0.0, 1.0,
		-1.0, -1.0,  1.0, 	1.0, 0.0, 0.0,	 0.0,  0.0,  1.0,	0.0, 0.0,
		 1.0, -1.0,  1.0, 	1.0, 0.0, 0.0,	 0.0,  0.0,  1.0,	1.0, 0.0,
		 1.0,  1.0, -1.0, 	0.0, 1.0, 0.0,	 1.0,  0.0,  0.0,	1.0, 0.0, 
		 1.0,  1.0,  1.0, 	0.0, 1.0, 0.0,	 1.0,  0.0,  0.0,	1.0, 1.0,
		 1.0, -1.0,  1.0, 	0.0, 1.0, 0.0,	 1.0,  0.0,  0.0,	0.0, 1.0,
		 1.0, -1.0, -1.0, 	0.0, 1.0, 0.0,	 1.0,  0.0,  0.0,	0.0, 0.0,
		 1.0,  1.0, -1.0, 	0.0, 0.0, 1.0,	 0.0,  0.0, -1.0, 	0.0, 0.0, 
		-1.0,  1.0, -1.0, 	0.0, 0.0, 1.0,	 0.0,  0.0, -1.0,	0.0, 1.0,
		-1.0, -1.0, -1.0, 	0.0, 0.0, 1.0,	 0.0,  0.0, -1.0,	1.0, 1.0,
		 1.0, -1.0, -1.0, 	0.0, 0.0, 1.0,	 0.0,  0.0, -1.0,	1.0, 0.0,
		-1.0,  1.0, -1.0, 	1.0, 0.0, 1.0,	-1.0,  0.0,  0.0,	1.0, 0.0, 
		-1.0,  1.0,  1.0, 	1.0, 0.0, 1.0,	-1.0,  0.0,  0.0,	0.0, 0.0,
		-1.0, -1.0,  1.0, 	1.0, 0.0, 1.0,	-1.0,  0.0,  0.0,	0.0, 1.0,
		-1.0, -1.0, -1.0, 	1.0, 0.0, 1.0,	-1.0,  0.0,  0.0,	1.0, 1.0,
		 1.0,  1.0, -1.0, 	1.0, 1.0, 0.0,	 0.0,  1.0,  0.0, 	0.0, 1.0, 
		-1.0,  1.0, -1.0, 	1.0, 1.0, 0.0,	 0.0,  1.0,  0.0,	0.0, 0.0,
		-1.0,  1.0,  1.0, 	1.0, 1.0, 0.0,	 0.0,  1.0,  0.0,	1.0, 0.0,
		 1.0,  1.0,  1.0, 	1.0, 1.0, 0.0,	 0.0,  1.0,  0.0,	1.0, 1.0,
		 1.0, -1.0, -1.0, 	0.0, 1.0, 1.0,	 0.0, -1.0,  0.0,	1.0, 0.0, 
		-1.0, -1.0, -1.0, 	0.0, 1.0, 1.0,	 0.0, -1.0,  0.0,	0.0, 0.0,
		-1.0, -1.0,  1.0, 	0.0, 1.0, 1.0,	 0.0, -1.0,  0.0,	0.0, 1.0,
		 1.0, -1.0,  1.0, 	0.0, 1.0, 1.0,	 0.0, -1.0,  0.0,	1.0, 1.0
	]);

	vao_cube = gl.createVertexArray();
	gl.bindVertexArray(vao_cube);

	vbo_cube_data = gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER, vbo_cube_data);
	gl.bufferData(gl.ARRAY_BUFFER, cube_vcnt, gl.STATIC_DRAW);
	
	gl.vertexAttribPointer(WebGLMacros.AMC_ATTRIBUTE_VERTEX,
		3,
		gl.FLOAT,
		false,
		11*4,
		0*4);
	gl.enableVertexAttribArray(WebGLMacros.AMC_ATTRIBUTE_VERTEX);
	
	gl.vertexAttribPointer(WebGLMacros.AMC_ATTRIBUTE_COLOR,
		3,
		gl.FLOAT,
		false,
		11*4,
		3*4);
	gl.enableVertexAttribArray(WebGLMacros.AMC_ATTRIBUTE_COLOR);
	
	gl.vertexAttribPointer(WebGLMacros.AMC_ATTRIBUTE_NORMAL,
		3,
		gl.FLOAT,
		false,
		11*4,
		6*4);
	gl.enableVertexAttribArray(WebGLMacros.AMC_ATTRIBUTE_NORMAL);
	
	gl.vertexAttribPointer(WebGLMacros.AMC_ATTRIBUTE_TEXCOORD0,
		2,
		gl.FLOAT,
		false,
		11*4,
		9*4);
	gl.enableVertexAttribArray(WebGLMacros.AMC_ATTRIBUTE_TEXCOORD0);
	
	
	gl.bindBuffer(gl.ARRAY_BUFFER, null);
	gl.bindVertexArray(null);

	// set clear color
	gl.clearColor(0.0, 0.0, 1.0, 1.0); // blue colored screen
	//enable depth
	gl.clearDepth(1.0);
	gl.enable(gl.DEPTH_TEST);
	gl.depthFunc(gl.LEQUAL);

	// initialize projection matrix
	perspectiveProjectionMatrix = mat4.create();
}

function resize()
{
	//code
	if(bFullscreen == true)
	{
		canvas.width = window.innerWidth;
		canvas.height = window.innerHeight;
	}
	else
	{
		canvas.width = canvas_original_width;
		canvas.height = canvas_original_height;
	}

	//set the viewport to match
	gl.viewport(0, 0, canvas.width, canvas.height);

	//perspective projection:
    mat4.perspective(perspectiveProjectionMatrix, 45.0, parseFloat(canvas.width)/parseFloat(canvas.height), 0.1, 100.0);
}

function spin()
{
	if(cubeAngle == 360.0)
		cubeAngle = 0.0;
	cubeAngle = cubeAngle + 0.3;
}

function degToRad(degrees)
{
	///code
	return(degrees * Math.PI / 100);
}

function draw()
{
	//code
	gl.clear(gl.COLOR_BUFFER_BIT|gl.DEPTH_BUFFER_BIT);

	gl.useProgram(shaderProgramObject);

	var modelMatrix = mat4.create();
	var viewMatrix = mat4.create();
	var projectionMatrix = mat4.create();
	var translationMatrix = mat4.create();
	var rotationMatrix = mat4.create();

	mat4.rotateX(rotationMatrix, rotationMatrix, degToRad(cubeAngle));
	mat4.rotateY(rotationMatrix, rotationMatrix, degToRad(cubeAngle));
	mat4.rotateZ(rotationMatrix, rotationMatrix, degToRad(cubeAngle));
	mat4.translate(translationMatrix, translationMatrix, [0.0,0.0,-5.5]);
	mat4.multiply(modelMatrix, modelMatrix, translationMatrix);
	mat4.multiply(modelMatrix, modelMatrix, rotationMatrix);
	mat4.multiply(projectionMatrix, projectionMatrix, perspectiveProjectionMatrix);

	if (enableLighting == true)
	{
		gl.uniform1i(keyPressedUniform, 1);
		gl.uniform3f(LdUniform,  1.0,1.0,1.0);
		gl.uniform3fv(LaUniform,  lightAmbient);
		gl.uniform3fv(LsUniform, lightSpecular);

		gl.uniform3f(KdUniform,  1.0,1.0,1.0);
		gl.uniform3fv(KaUniform,  materialAmbient);
		gl.uniform3fv(KsUniform,  materialSpecular);

		gl.uniform1f(materialShininessUniform, materialShininess);

		gl.uniform4fv(lightPositionUniform,  lightPosition);
	}
	else
	{
		gl.uniform1i(keyPressedUniform, 0);
	}

	//send necessary matrices to shader in respective uniforms
	gl.uniform1i(samplerUniform, 0);
	gl.uniformMatrix4fv(mUniform, false, modelMatrix);
	gl.uniformMatrix4fv(vUniform,  false, viewMatrix);
	gl.uniformMatrix4fv(pUniform,  false, projectionMatrix);


	// draw cube
	gl.bindTexture(gl.TEXTURE_2D, stoneTexture);

	gl.bindVertexArray(vao_cube);
	gl.drawArrays(gl.TRIANGLE_FAN, 0, 4);
	gl.drawArrays(gl.TRIANGLE_FAN, 4, 4);
	gl.drawArrays(gl.TRIANGLE_FAN, 8, 4);
	gl.drawArrays(gl.TRIANGLE_FAN, 12, 4);
	gl.drawArrays(gl.TRIANGLE_FAN, 16, 4);
	gl.drawArrays(gl.TRIANGLE_FAN, 20, 4);

	gl.bindVertexArray(null);

	gl.useProgram(null);

	spin();

	//animation loop
	requestAnimationFrame(draw, canvas);
}

function uninitialize()
{
	//code
	if(vao_cube)
	{
		gl.deleteVertexArray(vao_cube);
		vao_cube = null;
	}

	if(vbo_cube_vcnt)
	{
		gl.deleteBuffer(vbo_cube_vcnt);
		vbo_cube_vcnt = null;
	}

	if(shaderProgramObject)
	{
		if(fragmentShaderObject)
		{
			gl.detachShader(shaderProgramObject, fragmentShaderObject);
			gl.deleteShader(fragmentShaderObject);
			fragmentShaderObject = null;
		}

		if(vertexShaderObject)
		{
			gl.detachShader(shaderProgramObject, vertexShaderObject);
			gl.deleteShader(vertexShaderObject);
			vertexShaderObject = null;
		}
		gl.deleteProgram(shaderProgramObject);
		shaderProgramObject = null;

	}
}

function keyDown(event)
{
	//code
	switch(event.keyCode)
	{
		case 70: 	// for f or F
			toggleFullScreen();

			break;
		case 108: 		// l
		case 76: 		// L
			if(enableLighting == false)
			{
				enableLighting = true;
			}
			else
				enableLighting = false;
			break;
		case 27:
		uninitialize();
		window.close();
		break;
	}
}

function mouseDown()
{
	//code
}
