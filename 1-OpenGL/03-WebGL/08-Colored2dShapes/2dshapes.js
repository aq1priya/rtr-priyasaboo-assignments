//global variables
var canvas = null;
var gl = null;			// webgl context
var bFullscreen = false;
var canvas_original_width;
var canvas_original_height;

const WebGLMacros = //when whole 'WebGLMacros' is const all inside it are automaticaly const
{
	AMC_ATTRIBUTE_VERTEX:0,
	AMC_ATTRIBUTE_COLOR:1,
	AMC_ATTRIBUTE_NORMAL:2,
	AMC_ATTRIBUTE_TEXCOORD0:3,
}

var vertexShaderObject;
var fragmentShaderObject;
var shaderProgramObject;

var vao_triangle, vao_square;
var vbo_triangle_vertices, vbo_square_vertices;
var vbo_triangle_color, vbo_square_color;
var mvpUniform;

var perspectiveProjectionMatrix;

// To start animation : To have requestAnimationFrame() to be called "cross-browser" compatible
var requestAnimationFrame = 
window.requestAnimationFrame ||
window.webkitRequestAnimationFrame ||
window.mozRequestAnimationFrame ||
window.oRequestAnimationFrame ||
window.msRequestAnimationFrame ||
null;

// To stop animation : To have cancelAnimationFrame() to be called "cross-browser" compatible
var cancelAnimationFrame = 
window.cancelAnimationFrame ||
window.webkitCancelAnimationFrame || window.webkitCancelRequestAnimationFrame ||
window.mozCancelAnimationFrame || window.mozCancelRequestAnimationFrame ||
window.oCancelAmimationFrame || window.oCancelRequestAnimationFrame ||
window.msCancelAnimationFrame || window.msCancelRequestAnimationFrame ||
null;

// onload function
function main()
{
	// get <canvas> element
	canvas = document.getElementById("AMC");
	if(!canvas)
		console.log("Obtaining Canvas Failed");
	else
		console.log("Obtaining Canvas Succeeded");

	canvas_original_width = canvas.width;
	canvas_original_height = canvas.height;
	
	// Register keyboard's keydown event handler
	window.addEventListener("keydown", keyDown, false);
	window.addEventListener("click", mouseDown, false);
	window.addEventListener("resize", resize, false);

	// initialize webgl
	init();

	//start drawing here as warming-up
	resize();
	draw();
}

function toggleFullScreen()
{
	//code
	var fullscreen_element = document.fullscreen_element ||
	document.webkitFullscreenElement ||
	document.mozFullScreenElement ||
	document.msFullscreenElement ||
	null;

	// if not fullscreen
	if(fullscreen_element == null)
	{
		if(canvas.requestFullscreen)
			canvas.requestFullscreen();
		else if(canvas.mozRequestFullScreen)
			canvas.mozRequestFullScreen();
		else if(canvas.webkitRequestFullscreen)
			canvas.webkitRequestFullscreen();
		else if(canvas.msRequestFullscreen)
			canvas.msRequestFullscreen();
		bFullscreen = true;
	}
	else 		// if already fullscreen
	{
		if(document.exitFullscreen)
			document.exitFullscreen();
		else if(document.mozCancelFullScreen)
			document.mozCancelFullScreen();
		else if(document.webkitExitFullscreen)
			document.webkitExitFullscreen()
		else if(document.webkitExitFullscreen)
			document.webkitExitFullscreen();
		bFullscreen = false;
	}
}

function init()
{
	//code
	// get webgl 2.0 context
	gl = canvas.getContext("webgl2");
	if(!gl)
	{
		console.log("Failed to get the rendering context for WebGL");
		return;
	}

	gl.viewportWidth = canvas.width;
	gl.viewportHeight = canvas.height;

	//vertex shader
	vertexShaderObject =gl.createShader(gl.VERTEX_SHADER);

	var vertexShaderSourceCode = 
	"#version 300 es 										" +
	"													\n 	" +
	"in vec4 vPosition;										" +
	"in vec4 vColor;										" +
	"													\n 	" +
	"out vec4 out_color;									" +
	"													\n 	" +
	"uniform mat4 u_mvp_matrix;								" +
	"													\n 	" +
	"void main(void)										" +
	"{														" +
	"	gl_Position = u_mvp_matrix * vPosition;				" +
	"	out_color = vColor;									" +
	"}														";

	gl.shaderSource(vertexShaderObject, vertexShaderSourceCode);
	gl.compileShader(vertexShaderObject);
	if(gl.getShaderParameter(vertexShaderObject, gl.COMPILE_STATUS) == false)
	{
		var error = gl.getShaderInfoLog(vertexShaderObject);
		if(error.length > 0)
		{
			alert(error);
			uninitialize();
		}
	}

	//fragment shader
	fragmentShaderObject = gl.createShader(gl.FRAGMENT_SHADER);

	var fragmentShaderSourceCode = 
	"#version 300 es 										" +
	"													\n	" +
	"precision highp float;									" +
	"in vec4 out_color;										" +
	"													\n	" +
	"out vec4 FragColor;									" +
	"													\n	" +
	"void main(void)										" +
	"{														" +
	"	FragColor = out_color;								" +
	"}														" ;

	gl.shaderSource(fragmentShaderObject, fragmentShaderSourceCode);
	gl.compileShader(fragmentShaderObject);
	if(gl.getShaderParameter(fragmentShaderObject, gl.COMPILE_STATUS) == false)
	{
		var error = gl.getShaderInfoLog(fragmentShaderObject);
		if(error.length > 0)
		{
			alert(error);
			uninitialize();
		}
	}

	//shader program object
	shaderProgramObject = gl.createProgram();
	gl.attachShader(shaderProgramObject, vertexShaderObject);
	gl.attachShader(shaderProgramObject, fragmentShaderObject);

	//pre linking binding to shader program object with shader attributes
	gl.bindAttribLocation(shaderProgramObject, WebGLMacros.AMC_ATTRIBUTE_VERTEX, "vPosition");
	gl.bindAttribLocation(shaderProgramObject, WebGLMacros.AMC_ATTRIBUTE_COLOR, "vColor");

	// linking
	gl.linkProgram(shaderProgramObject);
	if(!gl.getProgramParameter(shaderProgramObject, gl.LINK_STATUS))
	{
		var error = gl.getProgramInfoLog(shaderProgramObject);
		if(error.length > 0)
		{
			alert(error);
			uninitialize();
		}
	}

	// get mvp uniform location
	mvpUniform = gl.getUniformLocation(shaderProgramObject, "u_mvp_matrix");

	// data
	var triangleVertices = new Float32Array([
		0.0, 1.0, 0.0,		//appex
		-1.0, -1.0, 0.0,	//left-bottom
		1.0, -1.0, 0.0	//right-bottom
		]);

	var triangleColor = new Float32Array([
		1.0, 0.0, 0.0,	//blue
		0.0, 1.0, 0.0,
		0.0, 0.0, 1.0
		]);

	vao_triangle = gl.createVertexArray();
	gl.bindVertexArray(vao_triangle);

	vbo_triangle_vertices = gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER, vbo_triangle_vertices);
	gl.bufferData(gl.ARRAY_BUFFER, triangleVertices, gl.STATIC_DRAW);
	gl.vertexAttribPointer(WebGLMacros.AMC_ATTRIBUTE_VERTEX,
		3,
		gl.FLOAT,
		false,
		0,
		0);
	gl.enableVertexAttribArray(WebGLMacros.AMC_ATTRIBUTE_VERTEX);
	gl.bindBuffer(gl.ARRAY_BUFFER, null);

	vbo_triangle_color = gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER, vbo_triangle_color);
	gl.bufferData(gl.ARRAY_BUFFER, triangleColor, gl.STATIC_DRAW);
	gl.vertexAttribPointer(WebGLMacros.AMC_ATTRIBUTE_COLOR,
		3, 
		gl.FLOAT,
		false,
		0,
		0);
	gl.enableVertexAttribArray(WebGLMacros.AMC_ATTRIBUTE_COLOR);
	gl.bindBuffer(gl.ARRAY_BUFFER, null);
	gl.bindVertexArray(null);

	// data for square
	var squareVertices = new Float32Array([
		1.0, 1.0, 0.0,	//right top
		-1.0, 1.0, 0.0,	//left top
		-1.0, -1.0, 0.0,//left bottom
		1.0, -1.0, 0.0	//right bottom
		]);

	var squareColor = new Float32Array([
		0.0, 0.0, 1.0,
		0.0, 0.0, 1.0,
		0.0, 0.0, 1.0,
		0.0, 0.0, 1.0
		]);

	vao_square = gl.createVertexArray();
	gl.bindVertexArray(vao_square);

	vbo_square_vertices = gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER, vbo_square_vertices);
	gl.bufferData(gl.ARRAY_BUFFER, squareVertices, gl.STATIC_DRAW);
	gl.vertexAttribPointer(WebGLMacros.AMC_ATTRIBUTE_VERTEX,
		3,
		gl.FLOAT,
		false,
		0,
		0);
	gl.enableVertexAttribArray(WebGLMacros.AMC_ATTRIBUTE_VERTEX);
	gl.bindBuffer(gl.ARRAY_BUFFER, null);

	vbo_square_color = gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER, vbo_square_color);
	gl.bufferData(gl.ARRAY_BUFFER, squareColor, gl.STATIC_DRAW);
	gl.vertexAttribPointer(WebGLMacros.AMC_ATTRIBUTE_COLOR,
		3, 
		gl.FLOAT,
		false,
		0,
		0);
	gl.enableVertexAttribArray(WebGLMacros.AMC_ATTRIBUTE_COLOR);
	gl.bindBuffer(gl.ARRAY_BUFFER, null);
	gl.bindVertexArray(null);

	// set clear color
	gl.clearColor(0.0, 0.0, 0.0, 1.0); // blue colored screen

	// initialize projection matrix
	perspectiveProjectionMatrix = mat4.create();
}

function resize()
{
	//code
	if(bFullscreen == true)
	{
		canvas.width = window.innerWidth;
		canvas.height = window.innerHeight;
	}
	else
	{
		canvas.width = canvas_original_width;
		canvas.height = canvas_original_height;
	}

	//set the viewport to match
	gl.viewport(0, 0, canvas.width, canvas.height);

	//perspective projection:
    mat4.perspective(perspectiveProjectionMatrix, 45.0, parseFloat(canvas.width)/parseFloat(canvas.height), 0.1, 100.0);
}


function draw()
{
	//code
	gl.clear(gl.COLOR_BUFFER_BIT);

	gl.useProgram(shaderProgramObject);

	var modelViewMatrix = mat4.create();
	var modelViewProjectionMatrix = mat4.create();
	var translationMatrix = mat4.create();

	mat4.translate(translationMatrix, translationMatrix, [-1.5,0.0,-4.0]);
	mat4.multiply(modelViewMatrix, modelViewMatrix, translationMatrix);
	mat4.multiply(modelViewProjectionMatrix, perspectiveProjectionMatrix, modelViewMatrix);
	
	gl.uniformMatrix4fv(mvpUniform, false, modelViewProjectionMatrix);

	// draw triangle
	gl.bindVertexArray(vao_triangle);
	gl.drawArrays(gl.TRIANGLES, 0, 3);
	gl.bindVertexArray(null);
	
	mat4.identity(modelViewMatrix);
	mat4.identity(modelViewProjectionMatrix);
	mat4.identity(translationMatrix);

	mat4.translate(translationMatrix, translationMatrix, [1.5,0.0,-4.0]);
	mat4.multiply(modelViewMatrix, modelViewMatrix, translationMatrix);
	mat4.multiply(modelViewProjectionMatrix, perspectiveProjectionMatrix, modelViewMatrix);
	
	gl.uniformMatrix4fv(mvpUniform, false, modelViewProjectionMatrix);

	// draw square
	gl.bindVertexArray(vao_square);
	gl.drawArrays(gl.TRIANGLE_FAN, 0, 4);
	gl.bindVertexArray(null);

	gl.useProgram(null);

	//animation loop
	requestAnimationFrame(draw, canvas);
}

function uninitialize()
{
	//code
	if(vao_triangle)
	{
		gl.deleteVertexArray(vao_triangle);
		vao_triangle = null;
	}

	if(vbo_triangle_vertices)
	{
		gl.deleteBuffer(vbo_triangle_vertices);
		vbo_triangle_vertices = null;
	}

	if(vao_square)
	{
		gl.deleteVertexArray(vao_square);
		vao_square = null;
	}

	if(vbo_square_vertices)
	{
		gl.deleteBuffer(vbo_square_vertices);
		vbo_square_vertices = null;
	}

	if(shaderProgramObject)
	{
		if(fragmentShaderObject)
		{
			gl.detachShader(shaderProgramObject, fragmentShaderObject);
			gl.deleteShader(fragmentShaderObject);
			fragmentShaderObject = null;
		}

		if(vertexShaderObject)
		{
			gl.detachShader(shaderProgramObject, vertexShaderObject);
			gl.deleteShader(vertexShaderObject);
			vertexShaderObject = null;
		}
		gl.deleteProgram(shaderProgramObject);
		shaderProgramObject = null;

	}
}

function keyDown(event)
{
	//code
	switch(event.keyCode)
	{
		case 70: 	// for f or F
			toggleFullScreen();

			break;
		case 27:
		uninitialize();
		window.close();
		break;
	}
}

function mouseDown()
{
	//code
}
