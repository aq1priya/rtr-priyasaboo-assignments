//global variables
var canvas = null;
var context = null;

// onload function
function main()
{
	// get <canvas> element
	canvas = document.getElementById("AMC");
	if(!canvas)
		console.log("Obtaining Canvas Failed");
	else
		console.log("Obtaining Canvas Succeeded");

	// print canvas width and height on console
	console.log("canvas Width: "+canvas.width+" And Canvas Height : "+canvas.height);

	// get 2D context
	context = canvas.getContext("2d");
	if(!context)
		console.log("Obtaining 2d Context Failed");
	else
		console.log("Obtaining 2d Contex Succeeded");

	// fill canvas with black Color
	context.fillStyle="black";		// "#000000"
	context.fillRect(0, 0, canvas.width, canvas.height);

	// draw text
	drawText("Hello World !!!");

	// Register keyboard's keydown event handler
	window.addEventListener("keydown", keyDown, false);
	window.addEventListener("click", mouseDown, false);
}

function drawText(text)
{
	//center the text
	context.textAlign = "center";	//center horizontally
	context.textBaseline = "middle";	//center vertically

	// text font
	context.font = "48px sans-serif";

	//text color
	context.fillStyle="white";		// "#FFFFFF"

	//display the text in center
	//context.fillText("     ", canvas.width/2, canvas.height/2);

	context.fillText(text, canvas.width/2, canvas.height/2);

}

function toggleFullScreen()
{
	//code
	var fullscreen_element = document.fullscreen_element ||
	document.webkitFullscreenElement ||
	document.mozFullScreenElement ||
	document.msFullscreenElement ||
	null;

	// if not fullscreen
	if(fullscreen_element == null)
	{
		if(canvas.requestFullscreen)
			canvas.requestFullscreen();
		else if(canvas.mozRequestFullScreen)
			canvas.mozRequestFullScreen();
		else if(canvas.webkitRequestFullscreen)
			canvas.webkitRequestFullscreen();
		else if(canvas.msRequestFullscreen)
			canvas.msRequestFullscreen();
	}
	else 		// if already fullscreen
	{
		if(document.exitFullscreen)
			document.exitFullscreen();
		else if(document.mozCancelFullScreen)
			document.mozCancelFullScreen();
		else if(document.webkitExitFullscreen)
			document.webkitExitFullscreen()
		else if(document.webkitExitFullscreen)
			document.webkitExitFullscreen();
	}
}

function keyDown(event)
{
	//code
	switch(event.keyCode)
	{
		case 70: 	// for f or F
			toggleFullScreen();

			//repaint
			// fill canvas with black Color
		//	context.fillStyle="black";							// "#000000"
		//	context.fillRect(0, 0, canvas.width, canvas.height);

			drawText("Hello World !!!");
			break;
	}
}

function mouseDown()
{
	//code
}
