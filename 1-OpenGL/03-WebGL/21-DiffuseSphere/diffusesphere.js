//global variables
var canvas = null;
var gl = null;			// webgl context
var bFullscreen = false;
var canvas_original_width;
var canvas_original_height;

const WebGLMacros = //when whole 'WebGLMacros' is const all inside it are automaticaly const
{
	AMC_ATTRIBUTE_VERTEX:0,
	AMC_ATTRIBUTE_COLOR:1,
	AMC_ATTRIBUTE_NORMAL:2,
	AMC_ATTRIBUTE_TEXCOORD0:3,
}

var vertexShaderObject;
var fragmentShaderObject;
var shaderProgramObject;

var sphere = null;

var mUniform, vUniform, pUniform;
var ldUniform;
var kdUniform;
var lightPositionUniform;
var keyPressedUniform;

var lightDiffuse	 = new Float32Array([1.0, 1.0, 1.0, 1.0]);
var materialDiffuse  = new Float32Array([1.0, 1.0, 1.0, 1.0]);
var lightPosition 	 = new Float32Array([10.0, 10.0, 12.0, 1.0]);

var startAnimation = false;
var enableLighting = false;

var perspectiveProjectionMatrix;

var cubeAngle = 0.0;

// To start animation : To have requestAnimationFrame() to be called "cross-browser" compatible
var requestAnimationFrame = 
window.requestAnimationFrame ||
window.webkitRequestAnimationFrame ||
window.mozRequestAnimationFrame ||
window.oRequestAnimationFrame ||
window.msRequestAnimationFrame ||
null;

// To stop animation : To have cancelAnimationFrame() to be called "cross-browser" compatible
var cancelAnimationFrame = 
window.cancelAnimationFrame ||
window.webkitCancelAnimationFrame || window.webkitCancelRequestAnimationFrame ||
window.mozCancelAnimationFrame || window.mozCancelRequestAnimationFrame ||
window.oCancelAmimationFrame || window.oCancelRequestAnimationFrame ||
window.msCancelAnimationFrame || window.msCancelRequestAnimationFrame ||
null;

// onload function
function main()
{
	// get <canvas> element
	canvas = document.getElementById("AMC");
	if(!canvas)
		console.log("Obtaining Canvas Failed");
	else
		console.log("Obtaining Canvas Succeeded");

	canvas_original_width = canvas.width;
	canvas_original_height = canvas.height;
	
	// Register keyboard's keydown event handler
	window.addEventListener("keydown", keyDown, false);
	window.addEventListener("click", mouseDown, false);
	window.addEventListener("resize", resize, false);

	// initialize webgl
	init();

	//start drawing here as warming-up
	resize();
	draw();
}

function toggleFullScreen()
{
	//code
	var fullscreen_element = document.fullscreen_element ||
	document.webkitFullscreenElement ||
	document.mozFullScreenElement ||
	document.msFullscreenElement ||
	null;

	// if not fullscreen
	if(fullscreen_element == null)
	{
		if(canvas.requestFullscreen)
			canvas.requestFullscreen();
		else if(canvas.mozRequestFullScreen)
			canvas.mozRequestFullScreen();
		else if(canvas.webkitRequestFullscreen)
			canvas.webkitRequestFullscreen();
		else if(canvas.msRequestFullscreen)
			canvas.msRequestFullscreen();
		bFullscreen = true;
	}
	else 		// if already fullscreen
	{
		if(document.exitFullscreen)
			document.exitFullscreen();
		else if(document.mozCancelFullScreen)
			document.mozCancelFullScreen();
		else if(document.webkitExitFullscreen)
			document.webkitExitFullscreen()
		else if(document.webkitExitFullscreen)
			document.webkitExitFullscreen();
		bFullscreen = false;
	}
}

function init()
{
	//code
	// get webgl 2.0 context
	gl = canvas.getContext("webgl2");
	if(!gl)
	{
		console.log("Failed to get the rendering context for WebGL");
		return;
	}

	gl.viewportWidth = canvas.width;
	gl.viewportHeight = canvas.height;

	//vertex shader
	vertexShaderObject =gl.createShader(gl.VERTEX_SHADER);

	var vertexShaderSourceCode = 
	"	#version 300 es 																	" +
	"																					\n 	" +
	"	in vec4 vPosition;													                " +
    "   in vec3 vNormal;                                                                    " +
    "																	                \n  " +
    "	out vec3 diffuse_color;													           	" +
    "																	                \n  " +
    "	uniform mat4 u_m_matrix;											                " +
    "	uniform mat4 u_v_matrix;											               	" +
    "	uniform mat4 u_p_matrix;															" +
    "   uniform vec4 u_ld;                                                      			" +
    "   uniform vec4 u_kd;                                                                  " +
    "   uniform vec4 u_light_position;                                                      " +
    "   uniform mediump int u_enable_lighting;                                              " +
    "																	                \n  " +
    "	void main(void)														                " +
    "	{																	                " +
    "       if(u_enable_lighting == 1)                                                      " +
    "       {                                                                               " +
    "           vec4 eyeCoordinates = u_v_matrix * u_m_matrix * vPosition;                  " +
    "           mat3 normalMatrix   = mat3(u_v_matrix * u_m_matrix);                        " +
    "           vec3 tNorm          = normalize(normalMatrix * vNormal);                    " +
    "           vec3 s              = normalize(vec3(u_light_position - eyeCoordinates));   " +
    "           diffuse_color       = u_ld.xyz * u_kd.xyz * max(dot(s, tNorm), 0.0);        " +
    "       }                                                                               " +
    "		gl_Position = u_p_matrix * u_v_matrix * u_m_matrix * vPosition;					" +
    "	}																					" ;

	gl.shaderSource(vertexShaderObject, vertexShaderSourceCode);
	gl.compileShader(vertexShaderObject);
	if(gl.getShaderParameter(vertexShaderObject, gl.COMPILE_STATUS) == false)
	{
		var error = gl.getShaderInfoLog(vertexShaderObject);
		if(error.length > 0)
		{
			alert(error);
			uninitialize();
		}
	}

	//fragment shader
	fragmentShaderObject = gl.createShader(gl.FRAGMENT_SHADER);

	var fragmentShaderSourceCode = 
	"#version 300 es 												" +
	"															\n	" +
	"	precision highp float;									\n	" +
    "	in vec3 diffuse_color;										" +
    "															\n  " +
    "   uniform int u_enable_lighting;                              " +
    "                                                           \n  " +
    "	out vec4 FragColor;											" +
    "															\n  " +
    "	void main(void)												" +
    "	{															" +
    "       if(u_enable_lighting == 1)                              " +
    "       {                                                       " +
    "           FragColor = vec4(diffuse_color, 1.0);               " +
    "       }                                                       " +
    "       else                                                    " +
    "       {                                                       " +
    "           FragColor = vec4(1.0, 1.0, 1.0, 1.0);               " +
    "       }                                                       " +
    "	}															";

	gl.shaderSource(fragmentShaderObject, fragmentShaderSourceCode);
	gl.compileShader(fragmentShaderObject);
	if(gl.getShaderParameter(fragmentShaderObject, gl.COMPILE_STATUS) == false)
	{
		var error = gl.getShaderInfoLog(fragmentShaderObject);
		if(error.length > 0)
		{
			alert(error);
			uninitialize();
		}
	}

	//shader program object
	shaderProgramObject = gl.createProgram();
	gl.attachShader(shaderProgramObject, vertexShaderObject);
	gl.attachShader(shaderProgramObject, fragmentShaderObject);

	//pre linking binding to shader program object with shader attributes
	gl.bindAttribLocation(shaderProgramObject, WebGLMacros.AMC_ATTRIBUTE_VERTEX, "vPosition");
	gl.bindAttribLocation(shaderProgramObject, WebGLMacros.AMC_ATTRIBUTE_NORMAL, "vNormal");

	// linking
	gl.linkProgram(shaderProgramObject);
	if(!gl.getProgramParameter(shaderProgramObject, gl.LINK_STATUS))
	{
		var error = gl.getProgramInfoLog(shaderProgramObject);
		if(error.length > 0)
		{
			alert(error);
			uninitialize();
		}
	}

	// get mvp uniform location
	mUniform = gl.getUniformLocation(shaderProgramObject, "u_m_matrix");
	vUniform = gl.getUniformLocation(shaderProgramObject, "u_v_matrix");
	pUniform = gl.getUniformLocation(shaderProgramObject, "u_p_matrix");
	ldUniform = gl.getUniformLocation(shaderProgramObject, "u_ld");
    kdUniform = gl.getUniformLocation(shaderProgramObject, "u_kd");
    lightPositionUniform = gl.getUniformLocation(shaderProgramObject, "u_light_position");
    keyPressedUniform = gl.getUniformLocation(shaderProgramObject, "u_enable_lighting");

    sphere = new Mesh();
    makeSphere(sphere, 2.0, 30, 30);

	// set clear color
	gl.clearColor(0.0, 0.0, 0.0, 1.0); // blue colored screen
	//enable depth
	gl.clearDepth(1.0);
	gl.enable(gl.DEPTH_TEST);
	gl.depthFunc(gl.LEQUAL);

	// initialize projection matrix
	perspectiveProjectionMatrix = mat4.create();
}

function resize()
{
	//code
	if(bFullscreen == true)
	{
		canvas.width = window.innerWidth;
		canvas.height = window.innerHeight;
	}
	else
	{
		canvas.width = canvas_original_width;
		canvas.height = canvas_original_height;
	}

	//set the viewport to match
	gl.viewport(0, 0, canvas.width, canvas.height);

	//perspective projection:
    mat4.perspective(perspectiveProjectionMatrix, 45.0, parseFloat(canvas.width)/parseFloat(canvas.height), 0.1, 100.0);
}

function spin()
{
}

function degToRad(degrees)
{
	///code
	return(degrees * Math.PI / 100);
}

function draw()
{
	//code
	gl.clear(gl.COLOR_BUFFER_BIT|gl.DEPTH_BUFFER_BIT);

	var modelMatrix 		= mat4.create();
	var viewMatrix 			= mat4.create();
	var projectionMatrix 	= mat4.create();
	var translationMatrix 	= mat4.create();
	var rotationMatrix 		= mat4.create();
	var scaleMatrix 		= mat4.create();

	mat4.rotateX(rotationMatrix, rotationMatrix, degToRad(cubeAngle));
	mat4.rotateY(rotationMatrix, rotationMatrix, degToRad(cubeAngle));
	mat4.rotateZ(rotationMatrix, rotationMatrix, degToRad(cubeAngle));
	mat4.translate(translationMatrix, translationMatrix, [0.0,0.0,-5.5]);
	mat4.multiply(modelMatrix, modelMatrix, translationMatrix);
	mat4.multiply(modelMatrix, modelMatrix, rotationMatrix);
	mat4.multiply(projectionMatrix, projectionMatrix, perspectiveProjectionMatrix);
	
	gl.useProgram(shaderProgramObject);

	if(enableLighting == true)
	{
	 	gl.uniform1i(keyPressedUniform, 1);
        gl.uniform4fv(ldUniform, lightDiffuse);
        gl.uniform4fv(kdUniform, materialDiffuse);
        gl.uniform4fv(lightPositionUniform, lightPosition);
	}
	else
	{
		gl.uniform1i(keyPressedUniform, 0);
	}

	// Send matrices to shaders in respective uniforms
    gl.uniformMatrix4fv(mUniform, gl.FALSE, modelMatrix);
    gl.uniformMatrix4fv(vUniform, gl.FALSE, viewMatrix);
    gl.uniformMatrix4fv(pUniform, gl.FALSE, projectionMatrix);

	// Draw sphere
	sphere.draw();

	gl.useProgram(null);

	if(startAnimation == true)
		spin();

	//animation loop
	requestAnimationFrame(draw, canvas);
}

function uninitialize()
{
	//code
	if(vao_cube)
	{
		gl.deleteVertexArray(vao_cube);
		vao_cube = null;
	}

	if(vbo_cube_vertices)
	{
		gl.deleteBuffer(vbo_cube_vertices);
		vbo_cube_vertices = null;
	}

	if(vbo_cube_normal)
	{
		gl.deleteBuffer(vbo_cube_normal);
		vbo_cube_normal = null;
	}

	if(shaderProgramObject)
	{
		if(fragmentShaderObject)
		{
			gl.detachShader(shaderProgramObject, fragmentShaderObject);
			gl.deleteShader(fragmentShaderObject);
			fragmentShaderObject = null;
		}

		if(vertexShaderObject)
		{
			gl.detachShader(shaderProgramObject, vertexShaderObject);
			gl.deleteShader(vertexShaderObject);
			vertexShaderObject = null;
		}
		gl.deleteProgram(shaderProgramObject);
		shaderProgramObject = null;

	}
}

function keyDown(event)
{
	//code
	switch(event.keyCode)
	{
		case 70: 	// for f or F
			toggleFullScreen();

			break;
		case 108: 		// l
		case 76: 		// L
			if(enableLighting == false)
			{
				enableLighting = true;
			}
			else
				enableLighting = false;
			break;
		case 97: 	 	// a
		case 65: 		// A
			if(startAnimation == false)
            {
                startAnimation= true;
            }
            else
            {
                startAnimation = false;
            }
            break;
		case 27:
		uninitialize();
		window.close();
		break;
	}
}

function mouseDown()
{
	//code
}
