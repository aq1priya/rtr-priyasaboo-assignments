//global variables
var canvas = null;
var gl = null;			// webgl context
var bFullscreen = false;
var canvas_original_width;
var canvas_original_height;

const WebGLMacros = //when whole 'WebGLMacros' is const all inside it are automaticaly const
{
	AMC_ATTRIBUTE_VERTEX:0,
	AMC_ATTRIBUTE_COLOR:1,
	AMC_ATTRIBUTE_NORMAL:2,
	AMC_ATTRIBUTE_TEXCOORD0:3,
}

var vertexShaderObject;
var fragmentShaderObject;
var shaderProgramObject;

var vao_pyramid;
var vbo_pyramid_position;
var vbo_pyramid_normal;

var mUniform, vUniform, pUniform;
var blue_ldUniform, red_ldUniform;
var blue_laUniform, red_laUniform;
var blue_lsUniform, red_lsUniform;
var kdUniform;
var kaUniform;
var ksUniform;
var materialShininessUniform;
var blue_lightPositionUniform;
var red_lightPositionUniform;
var keyPressedUniform;

var blue_lightDiffuse	= new Float32Array([0.0, 0.0, 1.0]);
var blue_lightAmbient	= new Float32Array([0.0, 0.0, 0.0]);
var blue_lightSpecular	= new Float32Array([0.0, 0.0, 1.0]);
var blue_lightPosition 	= new Float32Array([-2.0, 0.0, 0.0, 1.0]);

var red_lightDiffuse	= new Float32Array([1.0, 0.0, 0.0]);
var red_lightAmbient	= new Float32Array([0.0, 0.0, 0.0]);
var red_lightSpecular	= new Float32Array([1.0, 0.0, 0.0]);
var red_lightPosition 	= new Float32Array([2.0, 0.0, 0.0, 1.0]);

var materialDiffuse  	= new Float32Array([1.0, 1.0, 1.0]);
var materialAmbient  	= new Float32Array([0.0, 0.0, 0.0]);
var materialSpecular  	= new Float32Array([1.0, 1.0, 1.0]);

var materialShininess 	= 128.0;

var startAnimation = false;
var enableLighting = false;

var perspectiveProjectionMatrix;

var pyramidAngle = 0.0;

// To start animation : To have requestAnimationFrame() to be called "cross-browser" compatible
var requestAnimationFrame = 
window.requestAnimationFrame ||
window.webkitRequestAnimationFrame ||
window.mozRequestAnimationFrame ||
window.oRequestAnimationFrame ||
window.msRequestAnimationFrame ||
null;

// To stop animation : To have cancelAnimationFrame() to be called "cross-browser" compatible
var cancelAnimationFrame = 
window.cancelAnimationFrame ||
window.webkitCancelAnimationFrame || window.webkitCancelRequestAnimationFrame ||
window.mozCancelAnimationFrame || window.mozCancelRequestAnimationFrame ||
window.oCancelAmimationFrame || window.oCancelRequestAnimationFrame ||
window.msCancelAnimationFrame || window.msCancelRequestAnimationFrame ||
null;

// onload function
function main()
{
	// get <canvas> element
	canvas = document.getElementById("AMC");
	if(!canvas)
		console.log("Obtaining Canvas Failed");
	else
		console.log("Obtaining Canvas Succeeded");

	canvas_original_width = canvas.width;
	canvas_original_height = canvas.height;
	
	// Register keyboard's keydown event handler
	window.addEventListener("keydown", keyDown, false);
	window.addEventListener("click", mouseDown, false);
	window.addEventListener("resize", resize, false);

	// initialize webgl
	init();

	//start drawing here as warming-up
	resize();
	draw();
}

function toggleFullScreen()
{
	//code
	var fullscreen_element = document.fullscreen_element ||
	document.webkitFullscreenElement ||
	document.mozFullScreenElement ||
	document.msFullscreenElement ||
	null;

	// if not fullscreen
	if(fullscreen_element == null)
	{
		if(canvas.requestFullscreen)
			canvas.requestFullscreen();
		else if(canvas.mozRequestFullScreen)
			canvas.mozRequestFullScreen();
		else if(canvas.webkitRequestFullscreen)
			canvas.webkitRequestFullscreen();
		else if(canvas.msRequestFullscreen)
			canvas.msRequestFullscreen();
		bFullscreen = true;
	}
	else 		// if already fullscreen
	{
		if(document.exitFullscreen)
			document.exitFullscreen();
		else if(document.mozCancelFullScreen)
			document.mozCancelFullScreen();
		else if(document.webkitExitFullscreen)
			document.webkitExitFullscreen()
		else if(document.webkitExitFullscreen)
			document.webkitExitFullscreen();
		bFullscreen = false;
	}
}

function init()
{
	//code
	// get webgl 2.0 context
	gl = canvas.getContext("webgl2");
	if(!gl)
	{
		console.log("Failed to get the rendering context for WebGL");
		return;
	}

	gl.viewportWidth = canvas.width;
	gl.viewportHeight = canvas.height;

	//vertex shader
	vertexShaderObject =gl.createShader(gl.VERTEX_SHADER);

	var vertexShaderSourceCode = 
	"	#version 300 es 																				" +
	"																								\n 	" +
    "	in vec4 vPosition;													         				    " +
    "   in vec3 vNormal;                                                             				    " +
    "																	             				\n  " +
    "	out vec3 phong_ads_lights;													 				    " +
    "																	             				\n  " +
    "	uniform mat4 u_m_matrix;											         				    " +
    "	uniform mat4 u_v_matrix;											         				    " +
    "	uniform mat4 u_p_matrix;											         				    " +
    "   uniform vec3 u_la_blue, u_la_red;                                                               " +
    "   uniform vec3 u_ld_blue, u_ld_red;                                                               " +
    "   uniform vec3 u_ls_blue, u_ls_red;                                                               " +
    "   uniform vec3 u_ka;                                                             				    " +
    "   uniform vec3 u_kd;                                                             				    " +
    "   uniform vec3 u_ks;                                                             				    " +
    "   uniform float u_shininess;                                                     				    " +
    "   uniform vec4 u_light_position_blue, u_light_position_red;                      				    " +
    "   uniform int u_enable_lighting;                                                 				    " +
    "																	               				\n  " +
    "	void main(void)														           				    " +
    "	{																	           				    " +
    "       if(u_enable_lighting == 1)                                                 				    " +
    "       {                                                                          				    " +
    "           vec4 eyeCoordinates = u_v_matrix * u_m_matrix * vPosition;             				    " +
    "           mat3 normalMatrix   = mat3(u_v_matrix * u_m_matrix);                   				    " +
    "           vec3 tNorm          = normalize(normalMatrix * vNormal);               				    " +
    "																	               				\n  " +
    "           vec3 lightDirection_blue   = normalize(vec3(u_light_position_blue - eyeCoordinates));   " +
    "           vec3 lightDirection_red    = normalize(vec3(u_light_position_red - eyeCoordinates));   	" +
    "           float tnDotld_blue         = max(dot(tNorm, lightDirection_blue), 0.0);                 " +
    "           float tnDotld_red          = max(dot(tNorm, lightDirection_red), 0.0);                 	" +
    "           vec3 reflectionVector_blue = reflect(-lightDirection_blue, tNorm);                    	" +
    "           vec3 reflectionVector_red  = reflect(-lightDirection_red, tNorm);                    	" +
    "           vec3 viewerVector_blue     = normalize(vec3(u_light_position_blue - eyeCoordinates));   " +
    "           vec3 viewerVector_red      = normalize(vec3(u_light_position_red - eyeCoordinates));   	" +
    "           float rvDotvv_blue         = max(dot(reflectionVector_blue, viewerVector_blue), 0.0);   " +
    "           float rvDotvv_red          = max(dot(reflectionVector_red, viewerVector_red), 0.0);     " +
    "                                                                                   			\n  " +
    "           vec3 ambient_blue   = u_la_blue * u_ka;                                          		" +
    "           vec3 diffuse_blue   = u_ld_blue * u_kd * tnDotld_blue;                                	" +
    "           vec3 specular_blue  = u_ls_blue * u_ks * pow(rvDotvv_blue, u_shininess);              	" +
    "                                                                                   			\n  " +
    "           vec3 ambient_red 	= u_la_red * u_ka;                                          		" +
    "           vec3 diffuse_red    = u_ld_red * u_kd * tnDotld_red;                                	" +
    "           vec3 specular_red   = u_ls_red * u_ks * pow(rvDotvv_red, u_shininess);              	" +
    "                                                                                   			\n  " +
    "           vec3 ambient        = ambient_blue + ambient_red;             							" +
    "           vec3 diffuse        = diffuse_blue + diffuse_red;             						 	" +
    "           vec3 specular       = specular_blue + specular_red;           					   		" +
    "                                                                                   			\n  " +
    "           phong_ads_lights    = ambient + diffuse + specular;                     			    " +
    "       }                                                                           			    " +
    "       else                                                                        			    " +
    "       {                                                                           			    " +
    "           phong_ads_lights   = vec3(1.0, 1.0, 1.0);                               			    " +
    "       }                                                                           			    " +
    "                                                                                   			\n  " +
    "		gl_Position = u_p_matrix * u_v_matrix * u_m_matrix * vPosition;								" +
    "	}																								";

	gl.shaderSource(vertexShaderObject, vertexShaderSourceCode);
	gl.compileShader(vertexShaderObject);
	if(gl.getShaderParameter(vertexShaderObject, gl.COMPILE_STATUS) == false)
	{
		var error = gl.getShaderInfoLog(vertexShaderObject);
		if(error.length > 0)
		{
			alert(error);
			uninitialize();
		}
	}

	//fragment shader
	fragmentShaderObject = gl.createShader(gl.FRAGMENT_SHADER);

	var fragmentShaderSourceCode = 
	"	#version 300 es 												" +
	"															\n	" +
	"	precision highp float;									\n	" +
    "	in vec3 phong_ads_lights;									" +
    "															\n  " +
    "	out vec4 FragColor;											" +
    "															\n  " +
    "	void main(void)												" +
    "	{															" +
    "           FragColor = vec4(phong_ads_lights, 1.0);            " +
    "	}															";

	gl.shaderSource(fragmentShaderObject, fragmentShaderSourceCode);
	gl.compileShader(fragmentShaderObject);
	if(gl.getShaderParameter(fragmentShaderObject, gl.COMPILE_STATUS) == false)
	{
		var error = gl.getShaderInfoLog(fragmentShaderObject);
		if(error.length > 0)
		{
			alert(error);
			uninitialize();
		}
	}

	//shader program object
	shaderProgramObject = gl.createProgram();
	gl.attachShader(shaderProgramObject, vertexShaderObject);
	gl.attachShader(shaderProgramObject, fragmentShaderObject);

	//pre linking binding to shader program object with shader attributes
	gl.bindAttribLocation(shaderProgramObject, WebGLMacros.AMC_ATTRIBUTE_VERTEX, "vPosition");
	gl.bindAttribLocation(shaderProgramObject, WebGLMacros.AMC_ATTRIBUTE_NORMAL, "vNormal");

	// linking
	gl.linkProgram(shaderProgramObject);
	if(!gl.getProgramParameter(shaderProgramObject, gl.LINK_STATUS))
	{
		var error = gl.getProgramInfoLog(shaderProgramObject);
		if(error.length > 0)
		{
			alert(error);
			uninitialize();
		}
	}

	// get mvp uniform location
	mUniform = gl.getUniformLocation(shaderProgramObject, "u_m_matrix");
	vUniform = gl.getUniformLocation(shaderProgramObject, "u_v_matrix");
	pUniform = gl.getUniformLocation(shaderProgramObject, "u_p_matrix");
	
	blue_ldUniform = gl.getUniformLocation(shaderProgramObject, "u_ld_blue");
	blue_laUniform = gl.getUniformLocation(shaderProgramObject, "u_ld_blue");
	blue_lsUniform = gl.getUniformLocation(shaderProgramObject, "u_ld_blue");

	red_ldUniform = gl.getUniformLocation(shaderProgramObject, "u_ld_red");
	red_laUniform = gl.getUniformLocation(shaderProgramObject, "u_ld_red");
	red_lsUniform = gl.getUniformLocation(shaderProgramObject, "u_ld_red");
    
    kdUniform = gl.getUniformLocation(shaderProgramObject, "u_kd");
    kaUniform = gl.getUniformLocation(shaderProgramObject, "u_ka");
    ksUniform = gl.getUniformLocation(shaderProgramObject, "u_ks");
    
    blue_lightPositionUniform = gl.getUniformLocation(shaderProgramObject, "u_light_position_blue");
    red_lightPositionUniform  = gl.getUniformLocation(shaderProgramObject, "u_light_position_red");
  
    materialShininessUniform = gl.getUniformLocation(shaderProgramObject, "u_shininess");
    keyPressedUniform = gl.getUniformLocation(shaderProgramObject, "u_enable_lighting");


	// ***** PYRAMID DATA *****
    var pyramid_vertices = new Float32Array([
    	0.0, 1.0, 0.0, -1.0, -1.0,  1.0,  1.0, -1.0,  1.0,
        0.0, 1.0, 0.0,  1.0, -1.0,  1.0,  1.0, -1.0, -1.0,
        0.0, 1.0, 0.0,  1.0, -1.0, -1.0, -1.0, -1.0, -1.0,
        0.0, 1.0, 0.0, -1.0, -1.0, -1.0, -1.0, -1.0,  1.0
    ]);

    var pyramid_normals = new Float32Array([
		0.0, 0.447214, 0.894427, 0.0, 0.447214, 0.894427, 0.0, 0.447214, 0.894427,
		0.894427, 0.447214, 0.0, 0.894427, 0.447214, 0.0, 0.894427, 0.447214, 0.0,
		0.0, 0.447214, -0.894427, 0.0, 0.447214, -0.894427, 0.0, 0.447214, -0.894427,
		-0.894427, 0.447214, 0.0, -0.894427, 0.447214, 0.0, -0.894427, 0.447214, 0.0
    ]);

	vao_pyramid = gl.createVertexArray();
	gl.bindVertexArray(vao_pyramid);

	vbo_pyramid_position = gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER, vbo_pyramid_position);
	gl.bufferData(gl.ARRAY_BUFFER, pyramid_vertices, gl.STATIC_DRAW);
	gl.vertexAttribPointer(WebGLMacros.AMC_ATTRIBUTE_VERTEX,
		3,
		gl.FLOAT,
		false,
		0,
		0);
	gl.enableVertexAttribArray(WebGLMacros.AMC_ATTRIBUTE_VERTEX);
	gl.bindBuffer(gl.ARRAY_BUFFER, null);

	vbo_pyramid_normal = gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER, vbo_pyramid_normal);
	gl.bufferData(gl.ARRAY_BUFFER, pyramid_normals, gl.STATIC_DRAW);
	gl.vertexAttribPointer(WebGLMacros.AMC_ATTRIBUTE_NORMAL,
		3, 
		gl.FLOAT,
		false,
		0,
		0);
	gl.enableVertexAttribArray(WebGLMacros.AMC_ATTRIBUTE_NORMAL);
	gl.bindBuffer(gl.ARRAY_BUFFER, null);
	gl.bindVertexArray(null);

	// set clear color
	gl.clearColor(0.0, 0.0, 0.0, 1.0); // blue colored screen
	//enable depth
	gl.clearDepth(1.0);
	gl.enable(gl.DEPTH_TEST);
	gl.depthFunc(gl.LEQUAL);

	// initialize projection matrix
	perspectiveProjectionMatrix = mat4.create();
}

function resize()
{
	//code
	if(bFullscreen == true)
	{
		canvas.width = window.innerWidth;
		canvas.height = window.innerHeight;
	}
	else
	{
		canvas.width = canvas_original_width;
		canvas.height = canvas_original_height;
	}

	//set the viewport to match
	gl.viewport(0, 0, canvas.width, canvas.height);

	//perspective projection:
    mat4.perspective(perspectiveProjectionMatrix, 45.0, parseFloat(canvas.width)/parseFloat(canvas.height), 0.1, 100.0);
}

function spin()
{
	if(pyramidAngle == 360.0)
		pyramidAngle = 0.0;
	pyramidAngle = pyramidAngle + 0.3;
}

function degToRad(degrees)
{
	///code
	return(degrees * Math.PI / 100);
}

function draw()
{
	//code
	gl.clear(gl.COLOR_BUFFER_BIT|gl.DEPTH_BUFFER_BIT);

	var modelMatrix 		= mat4.create();
	var viewMatrix 			= mat4.create();
	var projectionMatrix 	= mat4.create();
	var translationMatrix 	= mat4.create();
	var rotationMatrix 		= mat4.create();
	var scaleMatrix 		= mat4.create();

	mat4.rotateY(rotationMatrix, rotationMatrix, degToRad(pyramidAngle));
	mat4.translate(translationMatrix, translationMatrix, [0.0,0.0,-5.5]);
	mat4.multiply(modelMatrix, modelMatrix, translationMatrix);
	mat4.multiply(modelMatrix, modelMatrix, rotationMatrix);
	mat4.multiply(projectionMatrix, projectionMatrix, perspectiveProjectionMatrix);
	
	gl.useProgram(shaderProgramObject);

	if(enableLighting == true)
	{
	 	gl.uniform1i(keyPressedUniform, 1);

        gl.uniform3fv(blue_laUniform, blue_lightAmbient); 
        gl.uniform3fv(blue_ldUniform, blue_lightDiffuse); 
        gl.uniform3fv(blue_lsUniform, blue_lightSpecular);

		gl.uniform3fv(red_laUniform, red_lightAmbient); 
        gl.uniform3fv(red_ldUniform, red_lightDiffuse); 
		gl.uniform3fv(red_lsUniform, red_lightSpecular);

       	gl.uniform1f(materialShininessUniform, materialShininess);

        gl.uniform3fv(kaUniform, materialAmbient);
        gl.uniform3fv(kdUniform, materialDiffuse);
        gl.uniform3fv(ksUniform, materialSpecular);
        
        gl.uniform4fv(blue_lightPositionUniform, blue_lightPosition);
        gl.uniform4fv(red_lightPositionUniform, red_lightPosition);

	}
	else
	{
		gl.uniform1i(keyPressedUniform, 0);
	}

	// Send matrices to shaders in respective uniforms
    gl.uniformMatrix4fv(mUniform, gl.FALSE, modelMatrix);
    gl.uniformMatrix4fv(vUniform, gl.FALSE, viewMatrix);
    gl.uniformMatrix4fv(pUniform, gl.FALSE, projectionMatrix);

	// ***** PYRAMID *****
    //-----------------------------------------------
    // Bind to vao for Pyramid
    gl.bindVertexArray(vao_pyramid);
    gl.drawArrays(gl.TRIANGLES, 0, 12);

    // Unbind to vao
	gl.bindVertexArray(null);
    //-----------------------------------------------

	gl.useProgram(null);

	if(startAnimation == true)
		spin();

	//animation loop
	requestAnimationFrame(draw, canvas);
}

function uninitialize()
{
	//code
	if(vao_pyramid)
	{
		gl.deleteVertexArray(vao_pyramid);
		vao_pyramid = null;
	}

	if(vbo_pyramid_position)
	{
		gl.deleteBuffer(vbo_pyramid_position);
		vbo_pyramid_position = null;
	}

	if(vbo_pyramid_normal)
	{
		gl.deleteBuffer(vbo_pyramid_normal);
		vbo_pyramid_normal = null;
	}

	if(shaderProgramObject)
	{
		if(fragmentShaderObject)
		{
			gl.detachShader(shaderProgramObject, fragmentShaderObject);
			gl.deleteShader(fragmentShaderObject);
			fragmentShaderObject = null;
		}

		if(vertexShaderObject)
		{
			gl.detachShader(shaderProgramObject, vertexShaderObject);
			gl.deleteShader(vertexShaderObject);
			vertexShaderObject = null;
		}
		gl.deleteProgram(shaderProgramObject);
		shaderProgramObject = null;

	}
}

function keyDown(event)
{
	//code
	switch(event.keyCode)
	{
		case 70: 	// for f or F
			toggleFullScreen();

			break;
		case 108: 		// l
		case 76: 		// L
			if(enableLighting == false)
			{
				enableLighting = true;
			}
			else
				enableLighting = false;
			break;
		case 97: 	 	// a
		case 65: 		// A
			if(startAnimation == false)
            {
                startAnimation= true;
            }
            else
            {
                startAnimation = false;
            }
            break;
		case 27:
		uninitialize();
		window.close();
		break;
	}
}

function mouseDown()
{
	//code
}
