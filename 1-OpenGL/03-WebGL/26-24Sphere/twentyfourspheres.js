//global variables
var canvas = null;
var gl = null;			// webgl context
var bFullscreen = false;
var canvas_original_width;
var canvas_original_height;

const WebGLMacros = //when whole 'WebGLMacros' is const all inside it are automaticaly const
{
	AMC_ATTRIBUTE_VERTEX:0,
	AMC_ATTRIBUTE_COLOR:1,
	AMC_ATTRIBUTE_NORMAL:2,
	AMC_ATTRIBUTE_TEXCOORD0:3,
}

var vertexShaderObject_f;
var fragmentShaderObject_f;
var shaderProgramObject_f;
var vertexShaderObject_v;
var fragmentShaderObject_v;
var shaderProgramObject_v;

var mUniform_f, vUniform_f, pUniform_f;
var ldUniform_f, kdUniform_f;
var laUniform_f, kaUniform_f;
var lsUniform_f, ksUniform_f;
var materialShininessUniform_f;
var lightPositionUniform_f;
var keyPressedUniform_f;

var mUniform_v, vUniform_v, pUniform_v;
var ldUniform_v, kdUniform_v;
var laUniform_v, kaUniform_v;
var lsUniform_v, ksUniform_v;
var materialShininessUniform_v;
var lightPositionUniform_v;
var keyPressedUniform_v;

var sphereViewport 	= new Float32Array(24*4);
var lightAmbient	= new Float32Array([0.0, 0.0, 0.0]);
var lightDiffuse	= new Float32Array([1.0, 1.0, 1.0]);
var lightSpecular	= new Float32Array([1.0, 1.0, 1.0]);
var lightPosition 	= new Float32Array([0.0, 0.0, 0.0, 1.0]);

var materialAmbient  	= new Float32Array([
		0.0215, 0.1745, 0.0215, 1.0,	//emrald
		0.135, 0.2225, 0.1575, 1.0,		//jade
		0.05375, 0.05, 0.06625, 1.0, 	//obsidian
		0.25, 0.20725, 0.20725, 1.0,	//pearl
		0.1745, 0.01175, 0.01175, 1.0, 	//ruby
		0.1, 0.18725, 0.1745, 1.0,		//turquoise

		0.329412, 0.223529, 0.027451, 1.0,		//brass
		0.2125, 0.1275, 0.054, 1.0,			//bronze
		0.25, 0.25, 0.25, 1.0,				//chrome
		0.19125, 0.0735, 0.0225, 1.0,		//copper
		0.24725, 0.1995, 0.0745, 1.0,		//gold
		0.19225, 0.19225, 0.19225, 1.0,		//silver

		0.0, 0.0, 0.0, 1.0,		//black plastic
		0.0, 0.1, 0.06, 1.0,	//cyan plastic
		0.0, 0.0, 0.0, 1.0,		//green plastic
		0.0, 0.0, 0.0, 1.0,		//red plastic
		0.0, 0.0, 0.0, 1.0,		//white plastic
		0.0, 0.0, 0.0, 1.0,		//yellow plastic

		0.02, 0.02, 0.02, 1.0, 	//black rubber
		0.0, 0.05, 0.05, 1.0,  	//cyan rubber
		0.0, 0.05, 0.0, 1.0,	//green rubber
		0.05, 0.0, 0.0, 1.0,	//red rubber
		0.05, 0.05, 0.05, 1.0,	//white rubber
		0.05, 0.05, 0.0, 1.0	//yellow rubber
]);

var materialDiffuse  	= new Float32Array([
		0.07568, 0.61424, 0.07568, 1.0,	//emrald
		0.54, 0.89, 0.63, 1.0,			//jade
		0.18275, 0.17, 0.22525, 1.0,	//obsidian
		1.0, 0.829, 0.829, 1.0,			//pearl
		0.61424, 0.04136, 0.04136, 1.0,	//ruby
		0.396, 0.74151, 0.69102, 1.0,	//turquoise

		0.780392, 0.568627, 0.113725, 1.0,	//brass
		0.714, 0.4284, 0.18144, 1.0,		//bronze
		0.4, 0.4, 0.4, 1.0,					//chrome
		0.7038, 0.27048, 0.0828, 1.0,		//copper
		0.24725, 0.1995, 0.0745, 1.0,		//gold
		0.50754, 0.50754, 0.50754, 1.0,		//silver

		0.01, 0.01, 0.01, 1.0,				//blackplastic
		0.0, 0.50980392, 0.50980392, 1.0,	//cyanplastic
		0.1, 0.35, 0.1, 1.0,				//green plastic
		0.5, 0.0, 0.0, 1.0,					//red plastic
		0.55, 0.55, 0.55, 1.0,				//white plastic
		0.5, 0.5, 0.0, 1.0,					//yellow plastic
		
		0.01, 0.01, 0.01, 1.0,				//black rubber
		0.40, 0.50, 0.50, 1.0,				//cyan rubber
		0.4, 0.5, 0.4, 1.0,					//green  rubber
		0.5, 0.4, 0.4, 1.0,					//red  rubber
		0.5, 0.5, 0.5, 1.0,					//white  rubber
		0.50, 0.50, 0.40, 1.0, 				//yellow  rubber
]);

var materialSpecular  	= new Float32Array([
		0.633, 0.727811, 0.633, 1.0,
		0.316228, 0.316228, 0.316228, 1.0,
		0.332741, 0.328634, 0.346435, 1.0,
		0.296648, 0.296648, 0.296648, 1.0,
		0.727811, 0.626959, 0.626959, 1.0,
		0.297254, 0.308290, 0.306678, 1.0,

		0.992157, 0.941176, 0.807843, 1.0,
		0.393548, 0.271906, 0.166721, 1.0,
		0.774597, 0.774597, 0.774597, 1.0,
		0.256777, 0.137622, 0.086014, 1.0,
		0.628281, 0.555802, 0.366065, 1.0,
		0.508273, 0.508273, 0.508273, 1.0,

		0.50, 0.50, 0.50, 1.0,
		0.50196078, 0.50196078, 0.50196078, 1.0,
		0.45, 0.55, 0.45, 1.0,
		0.7, 0.6, 0.6, 1.0,
		0.70, 0.70, 0.70, 1.0,
		0.60, 0.60, 0.50, 1.0,

		0.4, 0.40, 0.40, 1.0,	//black rubber	
		0.04, 0.7, 0.7, 1.0,	//cyan rubber
		0.04, 0.7, 0.04, 1.0,	//green  rubber
		0.7, 0.04, 0.04, 1.0,	//red  rubber
		0.70, 0.70, 0.70, 1.0,	//white  rubber
		0.70, 0.70, 0.04, 1.0	//yellow  rubber
	
]);
var materialShininess 	= new Float32Array([
	0.6 * 128.0,
	0.1 * 128.0,
	0.3 * 128.0,
	0.88 * 128.0,
	0.6 * 128.0,
	0.1 * 128.0,

	0.21794872 * 128.0,
	0.2 * 128.0,
	0.6 * 128.0,
	0.1 * 128.0,
	0.4 * 128.0,
	0.4 * 128.0,

	0.25 * 128.0,
	0.25 * 128.0,
	0.25 * 128.0,
	0.25 * 128.0,
	0.25 * 128.0,
	0.25 * 128.0,

	0.078125 * 128.0,
	0.078125 * 128.0,
	0.078125 * 128.0,
	0.078125 * 128.0,
	0.078125 * 128.0,
	0.078125 * 128.0
]);

var theta = 0.0;
var axis = 0;
var gWidth;
var gHeight;

var toggleLighting = false;
var enableLighting = false;
var perspectiveProjectionMatrix;

var sphere = null;

// To start animation : To have requestAnimationFrame() to be called "cross-browser" compatible
var requestAnimationFrame = 
window.requestAnimationFrame ||
window.webkitRequestAnimationFrame ||
window.mozRequestAnimationFrame ||
window.oRequestAnimationFrame ||
window.msRequestAnimationFrame ||
null;

// To stop animation : To have cancelAnimationFrame() to be called "cross-browser" compatible
var cancelAnimationFrame = 
window.cancelAnimationFrame ||
window.webkitCancelAnimationFrame || window.webkitCancelRequestAnimationFrame ||
window.mozCancelAnimationFrame || window.mozCancelRequestAnimationFrame ||
window.oCancelAmimationFrame || window.oCancelRequestAnimationFrame ||
window.msCancelAnimationFrame || window.msCancelRequestAnimationFrame ||
null;

// onload function
function main()
{
	// get <canvas> element
	canvas = document.getElementById("AMC");
	if(!canvas)
		console.log("Obtaining Canvas Failed");
	else
		console.log("Obtaining Canvas Succeeded");

	canvas_original_width = canvas.width;
	canvas_original_height = canvas.height;
	
	// Register keyboard's keydown event handler
	window.addEventListener("keydown", keyDown, false);
	window.addEventListener("click", mouseDown, false);
	window.addEventListener("resize", resize, false);

	// initialize webgl
	init();

	//start drawing here as warming-up
	resize();
	draw();
}

function toggleFullScreen()
{
	//code
	var fullscreen_element = document.fullscreen_element ||
	document.webkitFullscreenElement ||
	document.mozFullScreenElement ||
	document.msFullscreenElement ||
	null;

	// if not fullscreen
	if(fullscreen_element == null)
	{
		if(canvas.requestFullscreen)
			canvas.requestFullscreen();
		else if(canvas.mozRequestFullScreen)
			canvas.mozRequestFullScreen();
		else if(canvas.webkitRequestFullscreen)
			canvas.webkitRequestFullscreen();
		else if(canvas.msRequestFullscreen)
			canvas.msRequestFullscreen();
		bFullscreen = true;
	}
	else 		// if already fullscreen
	{
		if(document.exitFullscreen)
			document.exitFullscreen();
		else if(document.mozCancelFullScreen)
			document.mozCancelFullScreen();
		else if(document.webkitExitFullscreen)
			document.webkitExitFullscreen()
		else if(document.webkitExitFullscreen)
			document.webkitExitFullscreen();
		bFullscreen = false;
	}
}

function init()
{
	//code
	// get webgl 2.0 context
	gl = canvas.getContext("webgl2");
	if(!gl)
	{
		console.log("Failed to get the rendering context for WebGL");
		return;
	}

	gl.viewportWidth = canvas.width;
	gl.viewportHeight = canvas.height;

	//vertex shader
	vertexShaderObject_f =gl.createShader(gl.VERTEX_SHADER);

	var vertexShaderSourceCode_f = 
	"	#version 300 es 																	" +
	"																	                \n  " +
    "	in vec4 vPosition;													                " +
    "   in vec3 vNormal;                                                                    " +
    "																	                \n  " +
    "   out vec3 tNorm;                                                                     " +
    "   out vec3 lightDirection;                                                            " +
    "   out vec3 viewerVector;                                                              " +
    "	uniform mat4 u_m_matrix;											                " +
    "	uniform mat4 u_v_matrix;											                " +
    "	uniform mat4 u_p_matrix;											                " +
    "   uniform vec4 u_light_position;                                                      " +
    "   uniform mediump int u_enable_lighting;                                              " +
    "																	                \n  " +
    "	void main(void)														                " +
    "	{																	                " +
    "       if(u_enable_lighting == 1)                                                      " +
    "       {                                                                               " +
    "           vec4 eyeCoordinates = u_v_matrix * u_m_matrix * vPosition;                  " +
    "           tNorm = mat3(u_v_matrix * u_m_matrix) * vNormal;                            " +
    "           lightDirection = vec3(u_light_position - eyeCoordinates);                   " +
    "           viewerVector = vec3(-eyeCoordinates);                                       " +
    "       }                                                                               " +
    "		gl_Position = u_p_matrix * u_v_matrix * u_m_matrix * vPosition;	                " +
    "	}																	                ";

	gl.shaderSource(vertexShaderObject_f, vertexShaderSourceCode_f);
	gl.compileShader(vertexShaderObject_f);
	if(gl.getShaderParameter(vertexShaderObject_f, gl.COMPILE_STATUS) == false)
	{
		var error = gl.getShaderInfoLog(vertexShaderObject_f);
		if(error.length > 0)
		{
			alert(error);
			uninitialize();
		}
	}

	//fragment shader
	fragmentShaderObject_f = gl.createShader(gl.FRAGMENT_SHADER);

	var fragmentShaderSourceCode_f = 
	"	#version 300 es 																					" +
	"																									\n	" +
	"	precision highp float;																			\n	" +
	"															                        				\n  " +
    "   in vec3 lightDirection;                                                         				    " +
    "   in vec3 viewerVector;                                                           				    " +
    "   in vec3 tNorm;                                                                  				    " +
    "															                        				\n  " +
    "	out vec4 FragColor;										                        				    " +
    "                                                                                   				\n  " +
    "   uniform vec3 u_la;                                                              				    " +
    "   uniform vec3 u_ld;                                                              				    " +
    "   uniform vec3 u_ls;                                                              				    " +
    "   uniform vec3 u_ka;                                                              				    " +
    "   uniform vec3 u_kd;                                                              				    " +
    "   uniform vec3 u_ks;                                                              				    " +
    "   uniform float u_shininess;                                                      				    " +
    "   uniform int u_enable_lighting;                                                          		    " +
    "															                                		\n  " +
    "	void main(void)											                                			" +
    "	{														                               			    " +
    "       vec3 phong_ads_lights;                                                              		    " +
    "                                                                                           		\n  " +
    "       if(u_enable_lighting == 1)                                                          		    " +
    "       {                                                                                   		    " +
    "           vec3 normalizedtNorm = normalize(tNorm);                                      				" +
    "           vec3 normalizedlightDirection = normalize(lightDirection);                    				" +
    "           vec3 normalizedviewerVector = normalize(viewerVector);                        				" +
    "                                                                                         			\n  " +
    "           vec3 reflectionVector = normalize(reflect(-normalizedlightDirection, normalizedtNorm));		" +
    "           float tnDotld = max(dot(normalizedlightDirection, normalizedtNorm), 0.0);   			    " +
    "           float rvDotvv = max(dot(reflectionVector, normalizedviewerVector), 0.0);    			    " +
    "                                                                                       			\n  " +
    "           vec3 ambient = u_la * u_ka;                                                 			    " +
    "           vec3 diffuse = u_ld * u_kd * tnDotld;                                       			    " +
    "           vec3 specular = u_ls * u_ks * pow(rvDotvv, u_shininess);            						" +
    "           phong_ads_lights = specular + diffuse + ambient;                                 			" +
    "       }                                                                                       		" +
    "       else                                                                                    		" +
    "       {                                                                                       		" +
    "           phong_ads_lights    = vec3(1.0, 1.0, 1.0);                                          		" +
    "       }                                                                                       		" +
    "       FragColor = vec4(phong_ads_lights, 1.0);                                            			" +
    "	}															                                		";

	gl.shaderSource(fragmentShaderObject_f, fragmentShaderSourceCode_f);
	gl.compileShader(fragmentShaderObject_f);
	if(gl.getShaderParameter(fragmentShaderObject_f, gl.COMPILE_STATUS) == false)
	{
		var error = gl.getShaderInfoLog(fragmentShaderObject_f);
		if(error.length > 0)
		{
			alert(error);
			uninitialize();
		}
	}

	//shader program object
	shaderProgramObject_f = gl.createProgram();
	gl.attachShader(shaderProgramObject_f, vertexShaderObject_f);
	gl.attachShader(shaderProgramObject_f, fragmentShaderObject_f);

	//pre linking binding to shader program object with shader attributes
	gl.bindAttribLocation(shaderProgramObject_f, WebGLMacros.AMC_ATTRIBUTE_VERTEX, "vPosition");
	gl.bindAttribLocation(shaderProgramObject_f, WebGLMacros.AMC_ATTRIBUTE_NORMAL, "vNormal");

	// linking
	gl.linkProgram(shaderProgramObject_f);
	if(!gl.getProgramParameter(shaderProgramObject_f, gl.LINK_STATUS))
	{
		var error = gl.getProgramInfoLog(shaderProgramObject_f);
		if(error.length > 0)
		{
			alert(error);
			uninitialize();
		}
	}

	// get mvp uniform location
	mUniform_f = gl.getUniformLocation(shaderProgramObject_f, "u_m_matrix");
	vUniform_f = gl.getUniformLocation(shaderProgramObject_f, "u_v_matrix");
	pUniform_f = gl.getUniformLocation(shaderProgramObject_f, "u_p_matrix");
	
	ldUniform_f = gl.getUniformLocation(shaderProgramObject_f, "u_ld");
	laUniform_f = gl.getUniformLocation(shaderProgramObject_f, "u_la");
	lsUniform_f = gl.getUniformLocation(shaderProgramObject_f, "u_ls");
    
    kdUniform_f = gl.getUniformLocation(shaderProgramObject_f, "u_kd");
    kaUniform_f = gl.getUniformLocation(shaderProgramObject_f, "u_ka");
    ksUniform_f = gl.getUniformLocation(shaderProgramObject_f, "u_ks");
    
    lightPositionUniform_f = gl.getUniformLocation(shaderProgramObject_f, "u_light_position");
  
    materialShininessUniform_f = gl.getUniformLocation(shaderProgramObject_f, "u_shininess");
    keyPressedUniform_f = gl.getUniformLocation(shaderProgramObject_f, "u_enable_lighting");


    //--------------------------------------------------------------------------------------------------
    //vertex shader
	vertexShaderObject_v =gl.createShader(gl.VERTEX_SHADER);

	var vertexShaderSourceCode_v = 
	"	#version 300 es 																				" +
	"																								\n 	" +
    "	in vec4 vPosition;													         				    " +
    "   in vec3 vNormal;                                                             				    " +
    "																	             				\n  " +
    "	out vec3 phong_ads_lights;													 				    " +
    "																	             				\n  " +
    "	uniform mat4 u_m_matrix;											         				    " +
    "	uniform mat4 u_v_matrix;											         				    " +
    "	uniform mat4 u_p_matrix;											         				    " +
    "   uniform vec3 u_la;                                                               				" +
    "   uniform vec3 u_ld;                                                               				" +
    "   uniform vec3 u_ls;                                                               				" +
    "   uniform vec3 u_ka;                                                             				    " +
    "   uniform vec3 u_kd;                                                             				    " +
    "   uniform vec3 u_ks;                                                             				    " +
    "   uniform float u_shininess;                                                     				    " +
    "   uniform vec4 u_light_position;                      				 						    " +
    "   uniform int u_enable_lighting;                                                 				    " +
    "																	               				\n  " +
    "	void main(void)														           				    " +
    "	{																	           				    " +
    "       if(u_enable_lighting == 1)                                                 				    " +
    "       {                                                                          				    " +
    "           vec4 eyeCoordinates = u_v_matrix * u_m_matrix * vPosition;             				    " +
    "           mat3 normalMatrix   = mat3(u_v_matrix * u_m_matrix);                   				    " +
    "           vec3 tNorm          = normalize(normalMatrix * vNormal);               				    " +
    "																	               				\n  " +
    "           vec3 lightDirection   = normalize(vec3(u_light_position - eyeCoordinates)); 			" +
    "           float tnDotld         = max(dot(tNorm, lightDirection), 0.0);               			" +
    "           vec3 reflectionVector = reflect(-lightDirection, tNorm);                    			" +
    "           vec3 viewerVector     = normalize(vec3(-eyeCoordinates));   							" +
    "           float rvDotvv         = max(dot(reflectionVector, viewerVector), 0.0);   				" +
    "                                                                                   			\n  " +
    "           vec3 ambient   = u_la * u_ka;                                          					" +
    "           vec3 diffuse   = u_ld * u_kd * tnDotld;                                					" +
    "           vec3 specular  = u_ls * u_ks * pow(rvDotvv, u_shininess);              					" +
    "                                                                                   			\n  " +
    "           phong_ads_lights    = ambient + diffuse + specular;                     			    " +
    "       }                                                                           			    " +
    "       else                                                                        			    " +
    "       {                                                                           			    " +
    "           phong_ads_lights   = vec3(1.0, 1.0, 1.0);                               			    " +
    "       }                                                                           			    " +
    "                                                                                   			\n  " +
    "		gl_Position = u_p_matrix * u_v_matrix * u_m_matrix * vPosition;								" +
    "	}																								";

	gl.shaderSource(vertexShaderObject_v, vertexShaderSourceCode_v);
	gl.compileShader(vertexShaderObject_v);
	if(gl.getShaderParameter(vertexShaderObject_v, gl.COMPILE_STATUS) == false)
	{
		var error = gl.getShaderInfoLog(vertexShaderObject_v);
		if(error.length > 0)
		{
			alert(error);
			uninitialize();
		}
	}

	//fragment shader
	fragmentShaderObject_v = gl.createShader(gl.FRAGMENT_SHADER);

	var fragmentShaderSourceCode_v = 
	"	#version 300 es 												" +
	"															\n	" +
	"	precision highp float;									\n	" +
    "	in vec3 phong_ads_lights;									" +
    "															\n  " +
    "	out vec4 FragColor;											" +
    "															\n  " +
    "	void main(void)												" +
    "	{															" +
    "           FragColor = vec4(phong_ads_lights, 1.0);            " +
    "	}															";

	gl.shaderSource(fragmentShaderObject_v, fragmentShaderSourceCode_v);
	gl.compileShader(fragmentShaderObject_v);
	if(gl.getShaderParameter(fragmentShaderObject_v, gl.COMPILE_STATUS) == false)
	{
		var error = gl.getShaderInfoLog(fragmentShaderObject_v);
		if(error.length > 0)
		{
			alert(error);
			uninitialize();
		}
	}

	//shader program object
	shaderProgramObject_v = gl.createProgram();
	gl.attachShader(shaderProgramObject_v, vertexShaderObject_v);
	gl.attachShader(shaderProgramObject_v, fragmentShaderObject_v);

	//pre linking binding to shader program object with shader attributes
	gl.bindAttribLocation(shaderProgramObject_v, WebGLMacros.AMC_ATTRIBUTE_VERTEX, "vPosition");
	gl.bindAttribLocation(shaderProgramObject_v, WebGLMacros.AMC_ATTRIBUTE_NORMAL, "vNormal");

	// linking
	gl.linkProgram(shaderProgramObject_v);
	if(!gl.getProgramParameter(shaderProgramObject_v, gl.LINK_STATUS))
	{
		var error = gl.getProgramInfoLog(shaderProgramObject_v);
		if(error.length > 0)
		{
			alert(error);
			uninitialize();
		}
	}

	// get mvp uniform location
	mUniform_v = gl.getUniformLocation(shaderProgramObject_v, "u_m_matrix");
	vUniform_v = gl.getUniformLocation(shaderProgramObject_v, "u_v_matrix");
	pUniform_v = gl.getUniformLocation(shaderProgramObject_v, "u_p_matrix");
	
	ldUniform_v = gl.getUniformLocation(shaderProgramObject_v, "u_ld");
	laUniform_v = gl.getUniformLocation(shaderProgramObject_v, "u_la");
	lsUniform_v = gl.getUniformLocation(shaderProgramObject_v, "u_ls");
    
    kdUniform_v = gl.getUniformLocation(shaderProgramObject_v, "u_kd");
    kaUniform_v = gl.getUniformLocation(shaderProgramObject_v, "u_ka");
    ksUniform_v = gl.getUniformLocation(shaderProgramObject_v, "u_ks");
    
    lightPositionUniform_v = gl.getUniformLocation(shaderProgramObject_v, "u_light_position");
  
    materialShininessUniform_v = gl.getUniformLocation(shaderProgramObject_v, "u_shininess");
    keyPressedUniform_v = gl.getUniformLocation(shaderProgramObject_v, "u_enable_lighting");

	axis = 0;
	theta = 0.0;

    // **** vertices, colors, shader attribs, vbo, vao initializations *****
    sphere = new Mesh();
    makeSphere(sphere, 2.0, 30, 30);

	// set clear color
	gl.clearColor(0.0, 0.0, 0.0, 1.0); // blue colored screen
	//enable depth
	gl.clearDepth(1.0);
	gl.enable(gl.DEPTH_TEST);
	gl.depthFunc(gl.LEQUAL);

	// initialize projection matrix
	perspectiveProjectionMatrix = mat4.create();
}

function resize()
{
	//code
	if(bFullscreen == true)
	{
		canvas.width = window.innerWidth;
		canvas.height = window.innerHeight;
	}
	else
	{
		canvas.width = canvas_original_width;
		canvas.height = canvas_original_height;
	}

	gWidth = canvas.width;
	gHeight = canvas.height;

	setViewports();

	//perspective projection:
    mat4.perspective(perspectiveProjectionMatrix, 45.0, parseFloat(canvas.width)/parseFloat(canvas.height), 0.1, 100.0);
}


function degToRad(degrees)
{
	///code
	return(degrees * Math.PI / 100);
}

function draw()
{
	
	//code
	gl.clear(gl.COLOR_BUFFER_BIT|gl.DEPTH_BUFFER_BIT);

	var modelMatrix 		= mat4.create();
	var viewMatrix 			= mat4.create();
	var projectionMatrix 	= mat4.create();
	var translationMatrix 	= mat4.create();

	mat4.translate(translationMatrix, translationMatrix, [0.0,0.0,-12.5]);
	mat4.multiply(modelMatrix, modelMatrix, translationMatrix);
	mat4.multiply(projectionMatrix, projectionMatrix, perspectiveProjectionMatrix);
	
	var offset = 0;
	for (var i = 0; i < 24; i++)
	{
		
		offset = i*4;

		if(toggleLighting == true)		// per Fragment Lighting
		{
			gl.useProgram(shaderProgramObject_f);

			if(enableLighting == true)
			{
				if (axis == 3)	// z
					{
						lightPosition[0] = 15 * Math.cos(degToRad(theta));
						lightPosition[1] = 15 * Math.sin(degToRad(theta));
						lightPosition[2] = 0.0;
					}
					if (axis == 2)	// y
					{
						lightPosition[0] = 15 *  Math.cos(degToRad(theta));
						lightPosition[1] = 0.0;
						lightPosition[2] = 15 * Math.sin(degToRad(theta));
					}
					if (axis == 1)	// x
					{
						lightPosition[0] = 0.0;
						lightPosition[1] = 15 * Math.cos(degToRad(theta));
						lightPosition[2] = 15 * Math.sin(degToRad(theta));
					}

				gl.uniform1i(keyPressedUniform_f, 1);

	        	gl.uniform3fv(laUniform_f, lightAmbient); 
	        	gl.uniform3fv(ldUniform_f, lightDiffuse); 
	        	gl.uniform3fv(lsUniform_f, lightSpecular);

	       		gl.uniform1f(materialShininessUniform_f, materialShininess[i]);

	        	gl.uniform3f(kaUniform_f, materialAmbient[offset + 0],materialAmbient[offset + 1],materialAmbient[offset + 2]);
	        	gl.uniform3f(kdUniform_f, materialDiffuse[offset + 0],materialDiffuse[offset + 1], materialDiffuse[offset + 2]);
	        	gl.uniform3f(ksUniform_f, materialSpecular[offset + 0],materialSpecular[offset + 1],materialSpecular[offset + 2]);
	        
	        	gl.uniform4fv(lightPositionUniform_f, lightPosition);
			}
			else
			{
				gl.uniform1i(keyPressedUniform_f, 0);
			}
			// Send matrices to shaders in respective uniforms
			gl.uniformMatrix4fv(mUniform_f, false, modelMatrix);
			gl.uniformMatrix4fv(vUniform_f, false, viewMatrix);
			gl.uniformMatrix4fv(pUniform_f, false, projectionMatrix);
	
			gl.viewport(sphereViewport[offset + 0],sphereViewport[offset + 1],sphereViewport[offset + 2], sphereViewport[offset + 3]);

			// Draw sphere
			sphere.draw();
	
			gl.useProgram(null);
		}
		else							// per Vertex
		{
			gl.useProgram(shaderProgramObject_v);

			if(enableLighting == true)
			{
				if (axis == 3)	// z
					{
						lightPosition[0] = 15 * Math.cos(degToRad(theta));
						lightPosition[1] = 15 * Math.sin(degToRad(theta));
						lightPosition[2] = 0.0;
					}
					if (axis == 2)	// y
					{
						lightPosition[0] = 15 *  Math.cos(degToRad(theta));
						lightPosition[1] = 0.0;
						lightPosition[2] = 15 * Math.sin(degToRad(theta));
					}
					if (axis == 1)	// x
					{
						lightPosition[0] = 0.0;
						lightPosition[1] = 15 * Math.cos(degToRad(theta));
						lightPosition[2] = 15 * Math.sin(degToRad(theta));
					}

				gl.uniform1i(keyPressedUniform_v, 1);

	       	 	gl.uniform3fv(laUniform_v, lightAmbient); 
	        	gl.uniform3fv(ldUniform_v, lightDiffuse); 
	        	gl.uniform3fv(lsUniform_v, lightSpecular);

	       		gl.uniform1f(materialShininessUniform_v, materialShininess[i]);

	        	gl.uniform3f(kaUniform_v, materialAmbient[offset+0], materialAmbient[offset+1], materialAmbient[offset+2]);
	        	gl.uniform3f(kdUniform_v, materialDiffuse[offset+0], materialDiffuse[offset+1], materialDiffuse[offset+2]);
	        	gl.uniform3f(ksUniform_v, materialSpecular[offset+0], materialSpecular[offset+1], materialSpecular[offset+2]);
	        
				gl.uniform4fv(lightPositionUniform_v, lightPosition);
			}
			else
			{
				gl.uniform1i(keyPressedUniform_v, 0);
			}
			// Send matrices to shaders in respective uniforms
			gl.uniformMatrix4fv(mUniform_v, false, modelMatrix);
			gl.uniformMatrix4fv(vUniform_v, false, viewMatrix);
			gl.uniformMatrix4fv(pUniform_v, false, projectionMatrix);
	
			gl.viewport(sphereViewport[offset + 0],sphereViewport[offset + 1],sphereViewport[offset + 2], sphereViewport[offset + 3]);

			// Draw sphere
			sphere.draw();
	
			gl.useProgram(null);
		}
	}
	theta = theta + 5.0;
	if (theta > 360)
	{
		theta = 0.0;
	}
	//animation loop
	requestAnimationFrame(draw, canvas);
}

function setViewports()
{
	//sphere 1
	sphereViewport[0] = 0;
	sphereViewport[1] = 4 * gHeight / 6;
	sphereViewport[2] = gWidth/2;
	sphereViewport[3] = gHeight/2;
	//sphere 2
	sphereViewport[4] = 0;
	sphereViewport[5] = 3* gHeight / 6;
	sphereViewport[6] = gWidth/2;
	sphereViewport[7] = gHeight/2;
	//sphere 3
	sphereViewport[8] = 0;
	sphereViewport[9] = 2* gHeight / 6;
	sphereViewport[10] = gWidth/2;
	sphereViewport[11] = gHeight/2;
	//sphere 4
	sphereViewport[12] = 0;
	sphereViewport[13] = 1* gHeight / 6;
	sphereViewport[14] = gWidth/2;
	sphereViewport[15] = gHeight/2;
	//sphere 5
	sphereViewport[16] = 0;
	sphereViewport[17] = 0;
	sphereViewport[18] = gWidth/2;
	sphereViewport[19] = gHeight/2;
	//sphere 6
	sphereViewport[20] = 0;
	sphereViewport[21] = -1 * gHeight / 6;
	sphereViewport[22] = gWidth/2;
	sphereViewport[23] = gHeight/2;

	//sphere 7
	sphereViewport[24] = gWidth/6;
	sphereViewport[25] = 4 * gHeight / 6;
	sphereViewport[26] = gWidth/2;
	sphereViewport[27] = gHeight/2;
	//sphere 8
	sphereViewport[28] = gWidth/6;
	sphereViewport[29] = 3 * gHeight / 6;
	sphereViewport[30] = gWidth/2;
	sphereViewport[31] = gHeight/2;
	//sphere 9
	sphereViewport[32] = gWidth/6;
	sphereViewport[33] = 2 * gHeight / 6;
	sphereViewport[34] = gWidth/2;
	sphereViewport[35] = gHeight/2;
	//sphere 10
	sphereViewport[36] = gWidth/6;
	sphereViewport[37] = 1 * gHeight / 6;
	sphereViewport[38] = gWidth/2;
	sphereViewport[39] = gHeight/2;
	//sphere 11
	sphereViewport[40] = gWidth/6;
	sphereViewport[41] = 0;
	sphereViewport[42] = gWidth/2;
	sphereViewport[43] = gHeight/2;
	//sphere 12
	sphereViewport[44] = gWidth/6;
	sphereViewport[45] = -1 * gHeight / 6;
	sphereViewport[46] = gWidth/2;
	sphereViewport[47] = gHeight/2;
	
	//sphere 13
	sphereViewport[48] = gWidth / 3;
	sphereViewport[49] = 4 * gHeight / 6;
	sphereViewport[50] = gWidth/2;
	sphereViewport[51] = gHeight/2;
	//sphere 14
	sphereViewport[52] = gWidth / 3;
	sphereViewport[53] = 3 * gHeight / 6;
	sphereViewport[54] = gWidth/2;
	sphereViewport[55] = gHeight/2;
	//sphere 15
	sphereViewport[56] = gWidth / 3;
	sphereViewport[57] = 2 * gHeight / 6;
	sphereViewport[58] = gWidth/2;
	sphereViewport[59] = gHeight/2;
	//sphere 16
	sphereViewport[60] = gWidth / 3;
	sphereViewport[61] = 1 * gHeight / 6;
	sphereViewport[62] = gWidth/2;
	sphereViewport[63] = gHeight/2;
	//sphere 17
	sphereViewport[64] = gWidth / 3;
	sphereViewport[65] = 0;
	sphereViewport[66] = gWidth/2;
	sphereViewport[67] = gHeight/2;
	//sphere 18
	sphereViewport[68] = gWidth / 3;
	sphereViewport[69] = -1 * gHeight / 6;
	sphereViewport[70] = gWidth/2;
	sphereViewport[71] = gHeight/2;

	//sphere 19
	sphereViewport[72] = gWidth/2;
	sphereViewport[73] = 4*gHeight/6;
	sphereViewport[74] = gWidth/2;
	sphereViewport[75] = gHeight/2;
	//sphere 20
	sphereViewport[76] = gWidth/2;
	sphereViewport[77] = 3*gHeight/6;
	sphereViewport[78] = gWidth/2;
	sphereViewport[79] = gHeight/2;
	//sphere 21
	sphereViewport[80] = gWidth/2;
	sphereViewport[81] = 2*gHeight/6;
	sphereViewport[82] = gWidth/2;
	sphereViewport[83] = gHeight/2;
	//sphere 22
	sphereViewport[84] = gWidth/2;
	sphereViewport[85] = 1*gHeight/6;
	sphereViewport[86] = gWidth/2;
	sphereViewport[87] = gHeight/2;
	//sphere 23
	sphereViewport[88] = gWidth/2;
	sphereViewport[89] = 0;
	sphereViewport[90] = gWidth/2;
	sphereViewport[91] = gHeight/2;
	//sphere 24
	sphereViewport[92] = gWidth/2;
	sphereViewport[93] = -gHeight/6;
	sphereViewport[94] = gWidth/2;
	sphereViewport[95] = gHeight/2;
}

function uninitialize()
{
	//code
	if(sphere)
	{
		sphere.deallocate();
		sphere = null;
	}

	if(shaderProgramObject_f)
	{
		if(fragmentShaderObject_f)
		{
			gl.detachShader(shaderProgramObject_f, fragmentShaderObject_f);
			gl.deleteShader(fragmentShaderObject_f);
			fragmentShaderObject_f = null;
		}

		if(vertexShaderObject_f)
		{
			gl.detachShader(shaderProgramObject_f, vertexShaderObject_f);
			gl.deleteShader(vertexShaderObject_f);
			vertexShaderObject_f = null;
		}
		gl.deleteProgram(shaderProgramObject_f);
		shaderProgramObject_f = null;

	}

	if(shaderProgramObject_v)
	{
		if(fragmentShaderObject_v)
		{
			gl.detachShader(shaderProgramObject_v, fragmentShaderObject_v);
			gl.deleteShader(fragmentShaderObject_v);
			fragmentShaderObject_v = null;
		}

		if(vertexShaderObject_v)
		{
			gl.detachShader(shaderProgramObject_v, vertexShaderObject_v);
			gl.deleteShader(vertexShaderObject_v);
			vertexShaderObject_v = null;
		}
		gl.deleteProgram(shaderProgramObject_v);
		shaderProgramObject_v = null;

	}
}

function keyDown(event)
{
	//code
	switch(event.keyCode)
	{
		case 70: 	// for f or F
			toggleFullScreen();

			break;
		case 108: 		// l
		case 76: 		// L
			if(enableLighting == false)
			{
				enableLighting = true;
			}
			else
				enableLighting = false;
			break;
		case 120:
		case 88:
			axis =1;
			theta = 0.0;
			lightPosition[0] = 0.0;
			lightPosition[1] = 0.0;
			lightPosition[2] = 0.0;

			break;

		case 121:
		case 89:
			axis = 2;
			theta = 0.0;
			lightPosition[0] = 0.0;
			lightPosition[1] = 0.0;
			lightPosition[2] = 0.0;
			break;

		case 122:
		case 90:
			axis = 3;
			theta = 0.0;
			lightPosition[0] = 0.0;
			lightPosition[1] = 0.0;
			lightPosition[2] = 0.0;
			break;
		case 84:
		case 116:
			if(toggleLighting == false)
			{
				toggleLighting = true;
				enableLighting = false;
			}
			else
			{
				toggleLighting = false;
				enableLighting = false;
			}	
			break;
		case 27:
			uninitialize();
			window.close();
			break;
	}
}

function mouseDown()
{
	//code
}
