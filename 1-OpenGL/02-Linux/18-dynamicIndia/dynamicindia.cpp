#include<stdio.h>
#include<iostream>
#include<stdlib.h>
#include<memory.h>
#include<math.h>

#include<X11/Xlib.h>
#include<X11/Xutil.h>
#include<X11/XKBlib.h>
#include<X11/keysym.h>

#include<GL/gl.h>
#include<GL/glx.h>
#include<GL/glu.h>

//namespaces
using namespace std;

//global variables declarations
bool bFullScreen = false;
Display *gpDisplay = NULL;
XVisualInfo *gpXVisualInfo =NULL;
Colormap gColormap;
Window gWindow;
int giWindowWidth = 800;
int giWindowHeight = 600;
GLXContext gGLXContext;

//global variables
GLfloat Translation_of_I = -1.2f;
GLfloat Translation_of_A = 1.2f;
GLfloat Translation_of_N = 1.5f;
GLfloat Translation_of_I2 = -1.5f;
GLfloat Translation_Of_MiddlePlane = -3.0f;
GLfloat Color_Brightness = 0.0f;
GLfloat Para_x = -1.5f, Para_y = 0.0f;
GLfloat angleOfPlaneRotation = -60.0f;
GLfloat angleOfPlaneRotation1 = -300.0f;
GLfloat AlphaValue = 1.0f;
GLfloat Color_Brightness1 =1.0f;
bool bDrawI1 = true;
bool bDrawN = false;
bool bDrawD = false;
bool bDrawI2 = false;
bool bDrawA = false;
bool bDrawPlane = false;
bool bConverge = true;
bool bStraight = false;
bool bDiverge = false;
bool bStartFading = false;

//entry point function
int main(void)
{
    //function prototype
    void createWindow(void);
    void toggleFullScreen(void);
    void initialize(void);
    void resize(int, int);
    void update(void);
    void display(void);
    void uninitialize(void);

    //variable declarations
    int winWidth = giWindowWidth;
    int winHeight = giWindowHeight;
    bool bDone = false;
    char keys[26];

    //code
    createWindow();

    //initialize
    initialize();

    //message Loop
    XEvent event;
    KeySym keysym;

    while(bDone == false)
    {
        while(XPending(gpDisplay))
        {
            XNextEvent(gpDisplay, &event);
            switch(event.type)
            {
                case MapNotify:
                    break;
                case KeyPress:
                    keysym = XkbKeycodeToKeysym(gpDisplay, event.xkey.keycode, 0, 0);
                    switch(keysym)
                    {
                        case XK_Escape:
                            bDone = true;
                            break;
                        default:
                            break;
                    }
                    XLookupString(&event.xkey, keys, sizeof(keys), NULL, NULL);
                    switch(keys[0])
                    {
                        case 'f':
                        case 'F':
                            if(bFullScreen == false)
                            {
                                toggleFullScreen();
                                bFullScreen = true;
                            }
                            else
                            {
                                toggleFullScreen();
                                bFullScreen = false;
                            }
                            break;
                        default:
                            break;
                    }
                    break;
                case ButtonPress:
                    switch(event.xbutton.button)
                    {
                        case 1:
                            break;
                        case 2:
                            break;
                        case 3:
                            break;
                        default:
                            break;
                    }
                    break;
                case MotionNotify:
                    break;
                case ConfigureNotify:
                    winWidth = event.xconfigure.width;
                    winHeight =event.xconfigure.height;
                    resize(winWidth, winHeight);
                    break;
                case Expose:
                    break;
                case DestroyNotify:
                    break;
                case 33:
                    bDone = true;
                    break;
                default:
                    break;
            }
        }
            update();
            display();
    }
    uninitialize();
    return(0);
}

void createWindow(void)
{
    //function prototype
    void uninitialize(void);

    //variable declarations
    XSetWindowAttributes winAttribs;
    int defaultScreen;
    int styleMask;
    static int frameBufferAttributes[] = {
        GLX_DOUBLEBUFFER, True,
        GLX_RGBA,
        GLX_RED_SIZE, 8,
        GLX_GREEN_SIZE, 8,
        GLX_BLUE_SIZE, 8,
        GLX_ALPHA_SIZE, 8,
        None };

    //code
    gpDisplay = XOpenDisplay(NULL);
    if(!gpDisplay)
    {
        printf("ERROR: Unable To Open XDisplay.\nExitting Now ..\n");
        uninitialize();
        exit(1);
    }

    defaultScreen = XDefaultScreen(gpDisplay);

    gpXVisualInfo = glXChooseVisual(gpDisplay, defaultScreen, frameBufferAttributes);
    if(!gpXVisualInfo)
    {
        printf("ERROR: Unable To Get A Visual.\nExitting Now ..\n");
        uninitialize();
        exit(1);
    }

    winAttribs.border_pixel = 0;
    winAttribs.background_pixmap =0;
    winAttribs.colormap = XCreateColormap(gpDisplay,
                                          RootWindow(gpDisplay, gpXVisualInfo->screen),
                                          gpXVisualInfo->visual,
                                          AllocNone);

    gColormap = winAttribs.colormap;

    winAttribs.background_pixel = BlackPixel(gpDisplay, defaultScreen);
    winAttribs.event_mask = ExposureMask | VisibilityChangeMask | ButtonPressMask | KeyPressMask | PointerMotionMask | StructureNotifyMask;
    styleMask = CWBorderPixel | CWBackPixel | CWEventMask | CWColormap;

    gWindow = XCreateWindow(gpDisplay,
                            RootWindow(gpDisplay, gpXVisualInfo->screen),
                            0,
                            0,
                            giWindowWidth,
                            giWindowHeight,
                            0,
                            gpXVisualInfo->depth,
                            InputOutput,
                            gpXVisualInfo->visual,
                            styleMask,
                            &winAttribs);
    if(!gWindow)
    {
        printf("ERROR: Failed To Create Main Window.\nExitting Now ..\n");
        uninitialize();
        exit(1);
    }

    XStoreName(gpDisplay, gWindow, "Dynamic India - Priya Saboo");

    Atom WindowManagerDelete = XInternAtom(gpDisplay, "WM_DELETE_WINDOW", True);
    XSetWMProtocols(gpDisplay, gWindow, &WindowManagerDelete, 1);
    XMapWindow(gpDisplay, gWindow);
}

void toggleFullScreen(void)
{
  //variable declarations
    Atom  wm_state;
    Atom fullscreen;
    XEvent xev = {0};

    //code
    wm_state = XInternAtom(gpDisplay, "_NET_WM_STATE", False);
    memset(&xev,0,sizeof(xev));

    xev.type = ClientMessage;
    xev.xclient.window = gWindow;
    xev.xclient.message_type = wm_state;
    xev.xclient.format = 32;
    xev.xclient.data.l[0] = bFullScreen ? 0 : 1;

    fullscreen = XInternAtom(gpDisplay, "_NET_WM_STATE_FULLSCREEN", False);
    xev.xclient.data.l[1] = fullscreen;
                       
    fullscreen = XInternAtom(gpDisplay, "_NET_WM_STATE_FULLSCREEN", False);
    xev.xclient.data.l[1] = fullscreen;
    
    XSendEvent(gpDisplay,
               RootWindow(gpDisplay, gpXVisualInfo->screen),
               False,
               StructureNotifyMask,
               &xev);
}
  
//initialize() function
void initialize(void)
{
    gGLXContext = glXCreateContext(gpDisplay, gpXVisualInfo,NULL, GL_TRUE);

    glXMakeCurrent(gpDisplay,gWindow,gGLXContext);
    
    glClearColor(0.0f,0.0f,0.0f,1.0f);
}

//resize() function
void resize(int width, int height)
{
    if(height == 0)
        height =1;
    glViewport(0,0,(GLsizei)width, (GLsizei)height);
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    gluPerspective(45.0f, (GLfloat)width/(GLfloat)height, 0.1f,100.0f);
}

//uninitialize function
void uninitialize(void)
{
    GLXContext current;
    current =glXGetCurrentContext();
    if(current !=NULL && current ==gGLXContext)
    {
        glXMakeCurrent(gpDisplay,0,0);
    }
    if(gGLXContext)
    {
        glXDestroyContext(gpDisplay, gGLXContext);
    }
    if(gWindow)
    {
        XDestroyWindow(gpDisplay,gWindow);
    }
    if(gColormap)
    {
        XFreeColormap(gpDisplay, gColormap);
    }
    if(gpXVisualInfo)
    {
        free(gpXVisualInfo);
        gpXVisualInfo = NULL;
    }
    if(gpDisplay)
    {
        XCloseDisplay(gpDisplay);
        gpDisplay = NULL;
    }
}

//update function
void update(void)
{
    if(bDrawI1)
    {
        if(Translation_of_I <= 0.0f)
            Translation_of_I += 0.0002f;
        else
            bDrawA = true;
    }
    if(bDrawA)
    {
        if(Translation_of_A >= 0.0f)
            Translation_of_A -= 0.0002f;
        else
            bDrawN = true;
    }
    if(bDrawN)
    {
        if(Translation_of_N >= 0.0f)
            Translation_of_N -= 0.0002f;
        else
            bDrawI2 = true;
    }
    if(bDrawI2)
    {
        if(Translation_of_I2 <= 0.0f)
            Translation_of_I2 += 0.0002f;
        else
            bDrawD = true;
    }
    if(bDrawD)
    {
        if(Color_Brightness < 1.0f)
            Color_Brightness += 0.0002f;
        else
            bDrawPlane = true;
    }
    if(bDrawPlane)
    {
        if(bConverge && (Para_x < 0.0f))
        {
            Para_y = 1.2f * (GLfloat)pow(Para_x, 2);
            Para_x = Para_x + 0.0002;
        }
        if( bStraight)
        {
            Para_y = 0.000000f;
            Para_x = Para_x + 0.0002f;
        }
        if(bDiverge)
        {
            Para_y = 1.2f * (GLfloat)pow(Para_x, 2);
            Para_x = Para_x + 0.0002f;
        }
        if(Translation_Of_MiddlePlane < 3.0f)
        {
            Translation_Of_MiddlePlane = Translation_Of_MiddlePlane +0.0002f;
        }
        if((bConverge == true) && (angleOfPlaneRotation < 0.0f) && (angleOfPlaneRotation1 > -360.0f))
        {
            angleOfPlaneRotation = angleOfPlaneRotation + 0.01f;
            angleOfPlaneRotation1 = angleOfPlaneRotation1 - 0.01f;
            if(angleOfPlaneRotation > 0.0f)
                angleOfPlaneRotation = 0.0f;
            if(angleOfPlaneRotation1 <= -360.0f)
                angleOfPlaneRotation1 = 0.0f;
        }
        if(bDiverge)
        {
            angleOfPlaneRotation = angleOfPlaneRotation - 0.015f;
            angleOfPlaneRotation1 = angleOfPlaneRotation1 + 0.015f;
        }
    }

}

//display function
void display(void)
{
    //function declarations
    void drawI(void);
    void drawN(void);
    void drawD(GLfloat);
    void drawI2(void);
    void drawA(void);
    void drawPlane(void);
    void drawTriColor(void);
    void pathOfMiddlePlane(GLfloat, GLfloat, GLfloat);
    void pathOfConvergingPlane(GLfloat, GLfloat, GLfloat);
    void pathOfDivergingPlane(GLfloat, GLfloat, GLfloat);
    
    //variable declarations
    GLfloat temp = 0.0f;

    glClear(GL_COLOR_BUFFER_BIT );
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

    if(bDrawI1)
    {
        glLoadIdentity();
        glTranslatef(Translation_of_I, 0.0f,-4.0f);
        glLineWidth(10.0f);
        drawI();
    }
    if(bDrawA)
    {
        glLoadIdentity();
        glTranslatef(Translation_of_A, 0.0f,-4.0f);
        glLineWidth(10.0f);
        drawA();
    }
    if(bDrawN)
    {
        glLoadIdentity();
        glTranslatef(0.0f,Translation_of_N,-4.0f);
        glLineWidth(10.0f);
        drawN();
    }
    if(bDrawI2)
    {
        glLoadIdentity();
        glTranslatef(0.0f,Translation_of_I2,-4.0f);
        glLineWidth(10.0f);
        drawI2();
    }
    if(bDrawD);
    {
        glLoadIdentity();
        glTranslatef(0.0f,0.0f,-4.0f);
        glLineWidth(10.0f);
        drawD(Color_Brightness);
    }
    
    if(bDrawPlane)
    {
        glLoadIdentity();
        glTranslatef(0.0f,0.0f,-4.0f);
        if(bConverge && (Para_x < 0.0f))
            pathOfConvergingPlane(Para_x, 1.0f,1.0f);
        else if(bConverge && (Para_x > 0.000000f))
        {
            bConverge = false;
            bDiverge = false;
            bStraight = true;
            temp = Para_x;
        }

        if(bStraight)
        {
            pathOfConvergingPlane(0.0f,1.0f,1.0f);
//            pathOfMiddlePlane();
            if(Para_x > 3.0f)
            {
                bDiverge = true;
                bConverge = false;
                bStraight = false;
                Para_x = temp;
            }
        }

        if(bDiverge)
        {
            pathOfDivergingPlane(Para_x, 1.0f, 1.0f);
            pathOfConvergingPlane(0.0f,1.0f,1.0f);
            if(Para_x > 1.5f)
            {
                bDiverge = false;
                bConverge = false;
                bStraight = false;
            }
        }

        //Upper Plane
        glLoadIdentity();
        if(bConverge == true)
        {
            glTranslatef(-1.5f, 0.0f, -3.5f);
            glTranslatef(Para_x, Para_y, 0.0f);
            glRotatef(angleOfPlaneRotation, 0.0f, 0.0f,1.0f);
            drawPlane();
        }
        if(bStraight)
        {
            glTranslatef(-1.5f, 0.0f,-3.5f);
            glTranslatef(Para_x, Para_y, 0.0f);
            glRotatef(angleOfPlaneRotation1, 0.0f,0.0f,1.0f);
            drawPlane();
        }
        if(bDiverge)
        {
            glTranslatef(1.5f, 0.0f,-3.5f);
            glTranslatef(Para_x, Para_y, 0.0f);
            glRotatef(angleOfPlaneRotation1, 0.0f,0.0f, 1.0f);
            drawPlane();
        }

        //middle Plane
        if(Translation_Of_MiddlePlane < 3.0f)
        {
            glLoadIdentity();
            glTranslatef(0.0f,0.0f,-3.5f);
            pathOfMiddlePlane(Translation_Of_MiddlePlane, 1.0f,1.0f);
            glTranslatef(Translation_Of_MiddlePlane, 0.0f,0.0f);
            drawPlane();
            if(Translation_Of_MiddlePlane >= 2.99f)
                bStartFading = true;
        }

        //Lower Plane
        glLoadIdentity();
        if(bConverge)
        {
            glTranslatef(-1.5f, 0.0f, -3.50f);
            glTranslated(Para_x, -Para_y, 0.0f);
            glRotatef(angleOfPlaneRotation1, 0.0f, 0.0f, 1.0f);
            drawPlane();
        }
        if(bStraight)
        {
            glTranslatef(-1.5f,0.0f,0.0f);
            glTranslatef(Para_x, Para_y, -3.50f);
            drawPlane();
        }
        if(bDiverge)
        {
            glTranslatef(1.5f, 0.0f,0.0f);
            glTranslatef(Para_x, -Para_y, -3.50f);
            glRotatef(angleOfPlaneRotation, 0.0f,0.0f, 1.0f);
            drawPlane();
        }
        
    }

    if(Translation_Of_MiddlePlane > 3.0f && bStartFading)
    {
        drawTriColor();
        glLoadIdentity();
        glTranslatef(0.0f,0.0f,-3.5f);
        pathOfConvergingPlane(0.0f, Color_Brightness1, AlphaValue);
        pathOfDivergingPlane(1.5f, Color_Brightness1, AlphaValue);
        pathOfMiddlePlane(Translation_Of_MiddlePlane, Color_Brightness1, AlphaValue);
        AlphaValue = AlphaValue - 0.0002f;
        if(AlphaValue <= 0.0f)
            bStartFading = false;
    }
    if(Translation_Of_MiddlePlane > 3.0f && bStartFading == false)
        drawTriColor();


    glXSwapBuffers(gpDisplay, gWindow);
    
}

//drawI function
void drawI(void)							//for I
{
	glBegin(GL_LINES);						
	glColor3f(1.0f, 0.6f, 0.2f);			//saffron
	glVertex3f(-1.20f, 0.5f, 0.0f);
	glVertex3f(-0.88f, 0.5f, 0.0f);

	glColor3f(0.070f, 0.533f, 0.027f);		//green
	glVertex3f(-1.20f, -0.5f, 0.0f);
	glVertex3f(-0.88f, -0.5f, 0.0f);

	glColor3f(1.0f, 0.6f, 0.2f);
	glVertex3f(-1.04f, 0.5f, 0.0f);
	glColor3f(0.070f, 0.533f, 0.027f);
	glVertex3f(-1.04f, -0.5f, 0.0f);
	glEnd();
}

//drawN function
void drawN(void)							// N
{
	glBegin(GL_LINES);	
	glColor3f(1.0f, 0.6f, 0.2f);
	glVertex3f(-0.68f, 0.52f, 0.0f);
	glColor3f(0.070f, 0.533f, 0.027f);
	glVertex3f(-0.68f, -0.52f, 0.0f);

	glColor3f(1.0f, 0.6f, 0.2f);
	glVertex3f(-0.68f, 0.52f, 0.0f);
	glColor3f(0.070f, 0.533f, 0.027f);
	glVertex3f(-0.36f, -0.51f, 0.0f);

	glColor3f(1.0f, 0.6f, 0.2f);
	glVertex3f(-0.36f, 0.52f, 0.0f);
	glColor3f(0.070f, 0.533f, 0.027f);
	glVertex3f(-0.36f, -0.52f, 0.0f);
	glEnd();
}

//drawD function
void drawD(GLfloat b)						// D
{
	glBegin(GL_LINES);	
	glColor3f(1.0f * b, 0.6f* b, 0.2f*b);
	glVertex3f(-0.10f, 0.5f, 0.0f);
	glColor3f(0.070f * b, 0.533f * b, 0.027f * b);
	glVertex3f(-0.10f, -0.5f, 0.0f);

	glColor3f(1.0f * b, 0.6f * b, 0.2f * b);
	glVertex3f(-0.17f, 0.5f, 0.0f);
	glColor3f(1.0f * b, 0.6f * b, 0.2f * b);
	glVertex3f(0.17f, 0.5f, 0.0f);

	glColor3f(1.0f * b, 0.6f * b, 0.2f * b);
	glVertex3f(0.17f, 0.5f, 0.0f);
	glColor3f(0.070f * b, 0.533f * b, 0.027f * b);
	glVertex3f(0.17f, -0.5f, 0.0f);

	glColor3f(0.070f * b, 0.533f * b, 0.027f * b);
	glVertex3f(-0.17f, -0.5f, 0.0f);
	glColor3f(0.070f * b, 0.533f * b, 0.027f * b);
	glVertex3f(0.17f, -0.5f, 0.0f);
	glEnd();
}

//drawI2 function
void drawI2(void)						// I2
{
	glBegin(GL_LINES);	 
	glColor3f(1.0f, 0.6f, 0.2f);//saffron
	glVertex3f(0.36f, 0.5f, 0.0f);
	glVertex3f(0.68f, 0.5f, 0.0f);

	glColor3f(0.070f, 0.533f, 0.027f);//green
	glVertex3f(0.36f, -0.5f, 0.0f);
	glVertex3f(0.68f, -0.5f, 0.0f);

	glColor3f(1.0f, 0.6f, 0.2f);
	glVertex3f(0.52f, 0.5f, 0.0f);
	glColor3f(0.070f, 0.533f, 0.027f);
	glVertex3f(0.52f, -0.5f, 0.0f);
	glEnd();
}

//drawA function
void drawA(void)						 // A
{
	glBegin(GL_LINES);	
	glColor3f(0.070f, 0.533f, 0.027f);
	glVertex3f(0.83f, -0.52f, 0.0f);
	glColor3f(1.0f, 0.6f, 0.2f);
	glVertex3f(1.04f, 0.52f, 0.0f);
	glColor3f(1.0f, 0.6f, 0.2f);
	glVertex3f(1.04f, 0.52f, 0.0f);
	glColor3f(0.070f, 0.533f, 0.027f);
	glVertex3f(1.25f, -0.52f, 0.0f);
	glEnd();
}

//draw flag
void drawTriColor(void)
{
	glLoadIdentity();
	glTranslatef(0.0f, 0.0f, -4.0f);
	glBegin(GL_QUADS);					//flag tricolor
	glColor3f(1.0f, 0.6f, 0.2f);		//saffron
	glVertex3f(0.951f, 0.015f, 0.0f);
	glVertex3f(1.126f, 0.015f, 0.0f);
	glVertex3f(1.120f, 0.045f, 0.0f);
	glVertex3f(0.956f, 0.045f, 0.0f);

	glColor3f(1.0f, 1.0f, 1.0f);		//white
	glVertex3f(0.944f, -0.015f, 0.0f);
	glVertex3f(1.132f, -0.015f, 0.0f);
	glVertex3f(1.125f, 0.015f, 0.0f);
	glVertex3f(0.950f, 0.015f, 0.0f);

	glColor3f(0.068f, 0.533f, 0.027f);	//green
	glVertex3f(1.133f, -0.015f, 0.0f);
	glVertex3f(0.945f, -0.015f, 0.0f);
	glVertex3f(0.938f, -0.045f, 0.0f);
	glVertex3f(1.138f, -0.045f, 0.0f);
	glEnd();
}

//plane function
void drawPlane(void)
{
	glLineWidth(1.0f);
	//at origin
	glBegin(GL_POLYGON);
	glColor3f(0.72f, 0.886f, 0.933f);
	glVertex3f(0.0f, -0.04f, 0.0f);
	glVertex3f(0.3f, -0.04f, 0.0f);
	glVertex3f(0.4f, 0.0f, 0.0f);//center point
	glVertex3f(0.3f, 0.04f, 0.0f);
	glVertex3f(0.0f, 0.04f, 0.0f);
	glVertex3f(-0.08f, 0.09f, 0.0f);
	glVertex3f(-0.08f, -0.09f, 0.0f);
	glEnd();

	glBegin(GL_QUADS);
	glColor3f(0.72f, 0.886f, 0.933f);
	glVertex3f(0.2f, 0.03f, 0.0f);
	glVertex3f(0.10f, 0.2f, 0.0f);
	glVertex3f(0.0f, 0.2f, 0.0f);
	glVertex3f(0.05f, 0.03f, 0.0f);

	glVertex3f(0.2f, -0.03f, 0.0f);
	glVertex3f(0.10f, -0.2f, 0.0f);
	glVertex3f(0.0f, -0.2f, 0.0f);
	glVertex3f(0.05f, -0.03f, 0.0f);
	glEnd();

	glLineWidth(2.0f);
	glBegin(GL_LINES);
	glColor3f(1.0f, 1.0f, 1.0f);
	//I
	glVertex3f(0.03f, 0.02f, 0.0f);
	glVertex3f(0.08f, 0.02f, 0.0f);
	glVertex3f(0.03f, -0.02f, 0.0f);
	glVertex3f(0.08f, -0.02f, 0.0f);
	glVertex3f(0.055f, 0.02f, 0.0f);
	glVertex3f(0.055f, -0.02f, 0.0f);
	//A
	glVertex3f(0.10f, 0.02f, 0.0f);
	glVertex3f(0.10f, -0.025f, 0.0f);
	glVertex3f(0.14f, 0.02f, 0.0f);
	glVertex3f(0.14f, -0.025f, 0.0f);
	glVertex3f(0.10f, 0.02f, 0.0f);
	glVertex3f(0.14f, 0.02f, 0.0f);
	glVertex3f(0.10f, 0.0f, 0.0f);
	glVertex3f(0.14f, 0.0f, 0.0f);
	//F
	glVertex3f(0.16f, 0.02f, 0.0f);
	glVertex3f(0.16f, -0.025f, 0.0f);
	glVertex3f(0.16f, 0.02f, 0.0f);
	glVertex3f(0.20f, 0.02f, 0.0f);
	glVertex3f(0.16f, 0.0f, 0.0f);
	glVertex3f(0.19f, 0.0f, 0.0f);
	glEnd();
}

// pathOfMiddlePlane function
void pathOfMiddlePlane(GLfloat trail_end, GLfloat b, GLfloat alpha)
{
	
	glLoadIdentity();
	glTranslatef(0.0f, 0.0f, -3.5f);
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	for (GLfloat start = -3.001f; start < trail_end - 0.1f; start = start + 0.008)
	{
		glPointSize(10.0f);
		glBegin(GL_POINTS);
		glColor4f(1.0f * b, 1.0f * b, 1.0f * b, alpha);//white
		glVertex3f(start, 0.0f, 0.0f);
		glColor4f(1.0f * b, 0.6f * b, 0.2f * b, alpha);//saffron
		glVertex3f(start, 0.035f, 0.0f);
		glColor4f(0.070f * b, 0.533f * b, 0.027f*b, alpha);//green
		glVertex3f(start, -0.035f, 0.0f);
		glEnd();
	}
	glDisable(GL_BLEND);
}

// pathOfConvergingPlane function
void pathOfConvergingPlane(GLfloat trail_end, GLfloat b, GLfloat alpha)
{
	//variable declaration
	GLfloat ParabolicY = 0.0f;
	
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	for (GLfloat StartParabola = -1.505f; StartParabola < trail_end; StartParabola = StartParabola + 0.005f)
	{
		glLoadIdentity();
		glTranslatef(0.0f, 0.0f, -3.5f);
		glPointSize(9.0f);
		glBegin(GL_POINTS);
		glColor4f(1.0f * b, 0.6f * b, 0.2f * b, alpha);//saffron
		ParabolicY = 1.2f * (GLfloat)pow(StartParabola, 2);
		glVertex3f(StartParabola - 1.5f, ParabolicY, 0.0f);
		glVertex3f(StartParabola - 0.06f - 1.5f, -ParabolicY, 0.0f);
		
		glColor4f(1.0f * b, 1.0f * b, 1.0f * b, alpha);//white
		glVertex3f(StartParabola - 0.03f - 1.5f, ParabolicY, 0.0f);
		glVertex3f(StartParabola - 0.03f - 1.5f, -ParabolicY, 0.0f);
		
		glColor4f(0.070f * b, 0.533f * b, 0.027f * b, alpha);//green
		glVertex3f(StartParabola - 0.06f - 1.5f, ParabolicY, 0.0f);
		glVertex3f(StartParabola - 1.5f, -ParabolicY, 0.0f);
		glEnd();
	}
	glDisable(GL_BLEND);
}

//pathOfDivergingPlane() function
void pathOfDivergingPlane(GLfloat trail_end, GLfloat b, GLfloat alpha)
{
	//variable declaration
	GLfloat ParabolicY = 0.0f;

	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	for (GLfloat StartParabola = 0.0f; StartParabola < trail_end; StartParabola = StartParabola + 0.004f)
	{
		glLoadIdentity();
		glTranslatef(1.52f, 0.0f, -3.5f);
		glPointSize(9.0f);
		glBegin(GL_POINTS);
		ParabolicY = 1.2f * (GLfloat)pow(StartParabola, 2);

		glColor4f(1.0f * b, 0.6f * b, 0.2f * b, alpha);//saffron
		glVertex3f(StartParabola - 0.06f, ParabolicY, 0.0f);
		glVertex3f(StartParabola, -ParabolicY, 0.0f);

		glColor4f(1.0f * b, 1.0f * b, 1.0f * b, alpha);//white
		glVertex3f(StartParabola - 0.03f, ParabolicY, 0.0f);
		glVertex3f(StartParabola - 0.03f, -ParabolicY, 0.0f);

		glColor4f(0.070f * b, 0.533f * b, 0.027f * b, alpha);//green
		glVertex3f(StartParabola, ParabolicY, 0.0f);
		glVertex3f(StartParabola - 0.06f, -ParabolicY, 0.0f);
		glEnd();
	}
	glDisable(GL_BLEND);
}


