#include<iostream>
#include<stdio.h>
#include<stdlib.h>
#include<memory.h>
#define _USE_MATH_DEFINES 1
#include<math.h>

#include<X11/Xlib.h>
#include<X11/Xutil.h>
#include<X11/XKBlib.h>
#include<X11/keysym.h>

#include<GL/gl.h>
#include<GL/glu.h>
#include<GL/glx.h>

#define NUM_POINTS 1000

//namespaces
using namespace std;

//global variable declarations
bool bFullScreen = false;
Display *gpDisplay = NULL;
XVisualInfo *gpXVisualInfo =NULL;
Colormap gColormap;
Window gWindow;
int giWindowWidth = 800;
int giWindowHeight = 600;
GLXContext gGLXContext;
GLfloat angleTriangle=0.0f, angleRect =0.0f;

bool gbDrawTriangle = true;
bool gbDrawCircle = false;
bool gbDrawLineSegment = false;
GLfloat angle = 0.0f;
GLfloat tx = 2.0f, ty = -2.0f;
int count, draw =0;

//entry point function
int main(void)
{
    //function prototype
    void createWindow(void);
    void toggleFullScreen(void);
    void uninitialize(void);
    void initialize(void);
    void resize(int, int);
    void display(void);
    void update(void);

    //variable declarations
    int winWidth = giWindowWidth;
    int winHeight = giWindowHeight;
    bool bDone = false;
    char keys[26];

    //code
    createWindow();

    //initalize
    initialize();

    //Message Loop
    XEvent event;
    KeySym keysym;

    while(bDone == false)
    {
        while(XPending(gpDisplay))
            {
                XNextEvent(gpDisplay, &event);
                switch(event.type)
                {
                    case MapNotify:
                        break;
                    case KeyPress:
                        keysym =XkbKeycodeToKeysym(gpDisplay, event.xkey.keycode, 0, 0);
                        switch(keysym)
                        {
                            case XK_Escape:
                                bDone = true;
                                break;
                            default:
                                break;
                        }
                        XLookupString(&event.xkey, keys, sizeof(keys),NULL,NULL);
                        switch(keys[0])
                        {
                            case 'F':
                            case 'f':
                                if(bFullScreen == false)
                                {
                                    toggleFullScreen();
                                    bFullScreen = true;
                                }
                                else
                                {
                                    toggleFullScreen();
                                    bFullScreen = false;
                                }
                                break;
                            default:
                                break;
                         }
                         break;
                     case ButtonPress:
                         switch(event.xbutton.button)
                         {
                             case 1:
                                 break;
                             case 2:
                                 break;
                             case 3:
                                 break;
                             default:
                                 break;
                         }
                         break;
                     case MotionNotify:
                         break;
                     case ConfigureNotify:
                         winWidth = event.xconfigure.width;
                         winHeight = event.xconfigure.height;
                         resize(winWidth, winHeight);
                         break;
                     case Expose:
                         break;
                     case DestroyNotify:
                         break;
                     case 33:
                         bDone = true;
                         break;
                     default:
                         break;
                }
            }
            update();
            display();
    }
    uninitialize();
    return(0);
}

void createWindow(void)
{
    //function prototype
    void uninitialize(void);

    //variable declarations
    XSetWindowAttributes winAttribs;
    int defaultScreen;
    int defaultDepth;
    int styleMask;
    static int frameBufferAttributes[] = {
        GLX_DOUBLEBUFFER, True,
        GLX_RGBA,
        GLX_RED_SIZE, 8,
        GLX_GREEN_SIZE , 8,
        GLX_BLUE_SIZE, 8,
        GLX_ALPHA_SIZE, 8,
        None};
    
    //code
    gpDisplay = XOpenDisplay(NULL);
    if(!gpDisplay)
    {
        printf("ERROR : Unable To Open X Display.\nExitting Now ...\n");
        uninitialize();
        exit(1);
    }

    defaultScreen = XDefaultScreen(gpDisplay);

    //defaultDepth = DefaultDepth(gpDisplay, defaultScreen);

    gpXVisualInfo = glXChooseVisual(gpDisplay, defaultScreen, frameBufferAttributes);
    if(!gpXVisualInfo)
    {
        printf("ERROR : Unable to create gpXVisualInfo.\nExitting Now ...\n");
        uninitialize();
        exit(1);
    }

    winAttribs.border_pixel = 0;
    winAttribs.background_pixmap = 0;
    winAttribs.colormap = XCreateColormap(gpDisplay, 
                                          RootWindow(gpDisplay, gpXVisualInfo->screen),
                                          gpXVisualInfo->visual,
                                          AllocNone);
    gColormap = winAttribs.colormap;

    winAttribs.background_pixel = BlackPixel(gpDisplay,defaultScreen);

    winAttribs.event_mask = ExposureMask | VisibilityChangeMask | ButtonPressMask |
                           KeyPressMask | PointerMotionMask | StructureNotifyMask;
   
    styleMask = CWBorderPixel | CWBackPixel | CWEventMask | CWColormap;

    gWindow = XCreateWindow(gpDisplay,
                            RootWindow(gpDisplay,gpXVisualInfo->screen),
                            0,
                            0,
                            giWindowWidth,
                            giWindowHeight,
                            0,                          // border width
                            gpXVisualInfo->depth,       // color depth
                            InputOutput,
                            gpXVisualInfo->visual,
                            styleMask,
                            &winAttribs);
    if(!gWindow)
    {
        printf("ERROR : Failed To Create Main Window.\nExitting Now ...\n");
        uninitialize();
        exit(1);
    }

    XStoreName(gpDisplay, gWindow, "Deathly Hallows - Priya Saboo");

    Atom windowManagerDelete = XInternAtom(gpDisplay, "WM_DELETE_WINDOW", True);
    XSetWMProtocols(gpDisplay, gWindow, &windowManagerDelete, 1);

    XMapWindow(gpDisplay, gWindow);
}

 void toggleFullScreen(void)
{
    //variable declarations
    Atom  wm_state;
    Atom fullscreen;
    XEvent xev = {0};

    //code
    wm_state = XInternAtom(gpDisplay, "_NET_WM_STATE", False);
    memset(&xev,0,sizeof(xev));

    xev.type = ClientMessage;
    xev.xclient.window = gWindow;
    xev.xclient.message_type = wm_state;
    xev.xclient.format = 32;
    xev.xclient.data.l[0] = bFullScreen ? 0 : 1;

    fullscreen = XInternAtom(gpDisplay, "_NET_WM_STATE_FULLSCREEN", False);
    xev.xclient.data.l[1] = fullscreen;
                       
    fullscreen = XInternAtom(gpDisplay, "_NET_WM_STATE_FULLSCREEN", False);
    xev.xclient.data.l[1] = fullscreen;
    
    XSendEvent(gpDisplay,
               RootWindow(gpDisplay, gpXVisualInfo->screen),
               False,
               StructureNotifyMask,
               &xev);
}
                   
void uninitialize(void)
{
    GLXContext current;
    current =glXGetCurrentContext();
    if(current !=NULL && current ==gGLXContext)
    {
        glXMakeCurrent(gpDisplay,0,0);
    }
    if(gGLXContext)
    {
        glXDestroyContext(gpDisplay, gGLXContext);
    }
    if(gWindow)
    {
        XDestroyWindow(gpDisplay,gWindow);
    }
    if(gColormap)
    {
        XFreeColormap(gpDisplay, gColormap);
    }
    if(gpXVisualInfo)
    {
        free(gpXVisualInfo);
        gpXVisualInfo = NULL;
    }
    if(gpDisplay)
    {
        XCloseDisplay(gpDisplay);
        gpDisplay = NULL;
    }
}

//initialize() function
void initialize(void)
{
    gGLXContext = glXCreateContext(gpDisplay, gpXVisualInfo,NULL, GL_TRUE);

    glXMakeCurrent(gpDisplay,gWindow,gGLXContext);

    glClearColor(0.0f,0.0f,0.0f,1.0f);
}

//resize() function
void resize(int width, int height)
{
    if(height == 0)
        height =1;
    glViewport(0,0,(GLsizei)width, (GLsizei)height);
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    gluPerspective(45.0f, (GLfloat)width/(GLfloat)height, 0.1f,100.0f);
}

//update function
void update(void)
{
    angle =angle+0.02f;

    if (angle == 4*M_PI)
	{
		angle = 0.0f;
	}
	if (tx != 0.0f)
		tx = tx- 0.0003f;
	if (ty != 0.0f)
		ty = ty + 0.0003f;
	if ((tx <= 0.0f) && (ty >= 0.0f))
	{
		count++;
		tx = 2.0f;
		ty = -2.0f;
	}
}

//display function
void display(void)
{
    //variable declaration
    GLfloat A[3] = { 0.0f, 1.0f, 0.0f };
    GLfloat B[3] = { -1.0f, -1.0f, 0.0f };
    GLfloat C[3] = { 1.0f, -1.0f, 0.0f };
    GLfloat perimeter, semiperimeter;
    GLfloat InCenter[3], Ta[3];
    GLfloat lengthA, lengthB, lengthC, inradius, theta = 0.0f;

    //code
    glClear(GL_COLOR_BUFFER_BIT);
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    
    //calculate sides of triangle
    lengthA = sqrtf(pow((C[0]-B[0]), 2.0f) + pow((C[1]-B[1]), 2.0f));
    lengthB = sqrtf(pow((C[0]-A[0]), 2.0f) + pow((C[1]-A[1]), 2.0f));
    lengthC = sqrtf(pow((B[0]-A[0]), 2.0f) + pow((B[1]-A[1]), 2.0f));

    //perimeter and semiperimeter
    perimeter =lengthA + lengthB + lengthC;
    semiperimeter = perimeter / 2.0f;

    //incenter
    InCenter[0] = ((lengthA * A[0]) + (lengthB * B[0]) + (lengthC * C[0])) / perimeter;
    InCenter[1] = ((lengthA * A[1]) + (lengthB * B[1]) + (lengthC * C[1])) / perimeter;
    
    //radius of incircle
    inradius = sqrt(semiperimeter * (semiperimeter - lengthA) * (semiperimeter - lengthB) * (semiperimeter - lengthC))/semiperimeter;
   
    //calculation of midpoint
    Ta[0] = 0.0f;
    Ta[1] = -1.0f;
    Ta[2] = 0.0f;


    //plot triangle
    if(gbDrawTriangle == true)
    {
        glLineWidth(3.0f);
        if(count == 0)
        {
            glTranslatef(tx, ty, -5.0f);
            glRotatef(angle, 0.0f,1.0f,0.0f);
        }
        else
        {
            glTranslatef(0.0f,0.0f,-5.0f);
            glRotatef(angle, 0.0f,1.0f,0.0f);
            gbDrawCircle = true;
        }
         glBegin(GL_LINE_LOOP);
        glVertex3f(A[0], A[1], A[2]);
        glVertex3f(B[0], B[1], B[2]);
        glVertex3f(C[0], C[1], C[2]);
        glEnd();
    }

    if(gbDrawCircle == true)
    {
        glLoadIdentity();
        if(count == 1)
        {
            glTranslatef(-tx + InCenter[0], ty + InCenter[1], -5.0f);
            glRotatef(angle, 0.0f,1.0f,0.0f);
        }
        else
        {
            glTranslatef(InCenter[0], InCenter[1], -5.0f);
            glRotatef(angle, 0.0f,1.0f,0.0f);
            gbDrawLineSegment = true;
            draw = 2;
        }

        //plot incircle
        glBegin(GL_LINE_LOOP);
        for(int i = 0; i <= NUM_POINTS; i++)
        {
            theta = 2 * M_PI * i /NUM_POINTS;
            glVertex3f(inradius * (GLfloat)cos(theta), inradius * (GLfloat)sin(theta), 0.0f);
        }
        glEnd();
    }

    if(gbDrawLineSegment ==true)
    {
        glLoadIdentity();
        if(count == 2)
            glTranslatef(0.0f, -ty ,-5.0f);
        else
            glTranslatef(0.0f,0.0f,-5.0f);
    
        //plot vertical line
        glBegin(GL_LINES);
        glVertex3f(A[0], A[1], A[2]);
        glVertex3f(Ta[0], Ta[1], Ta[2]);
        glEnd();
    }

    glXSwapBuffers(gpDisplay, gWindow);
    
}


