#include<iostream>
#include<stdio.h>
#include<stdlib.h>
#include<memory.h>

#include<X11/Xlib.h>
#include<X11/Xutil.h>
#include<X11/XKBlib.h>
#include<X11/keysym.h>

//namespaces
using namespace std;

//global variable declarations
bool bFullScreen = false;
Display *gpDisplay = NULL;
XVisualInfo *gpXVisualInfo = NULL;
Colormap gColormap;
Window gWindow;
int giWindowWidth = 800;
int giWindowHeight = 600;

//entry-point function
int main(void)
{
    //function prototype
    void createWindow(void);
    void toggleFullScreen(void);
    void uninitialize(void);

    //variable declarations
    static int winWidth = giWindowWidth;
    static int winHeight = giWindowHeight;
    static XFontStruct *pXFontStruct = NULL;       
    static GC gc;                                   //similar to HDC
    XGCValues gcValues;                             //PAINTSTRUCT
    XColor textColor;
    char str[] = "Hello World!!!";
    int strLength;
    int strWidth;
    int fontHeight;
    char keys[26];

    //code
    createWindow();

    //messageLoop
    XEvent event;
    KeySym keysym;

    while(1)
    {
        XNextEvent(gpDisplay, &event);
        switch(event.type)
        {
            case MapNotify:
                pXFontStruct = XLoadQueryFont(gpDisplay, "fixed");
                break;
            case KeyPress:
                keysym = XkbKeycodeToKeysym(gpDisplay, event.xkey.keycode, 0, 0);
                switch(keysym)
                {
                    case XK_Escape:
                        XFreeGC(gpDisplay, gc);
                        XUnloadFont(gpDisplay, pXFontStruct->fid);
                        uninitialize();
                        exit(0);
                        break;
                }
                XLookupString(&event.xkey, keys, sizeof(keys), NULL, NULL);
                switch(keys[0])
                {
                    case 'F':
                    case 'f':
                        if(bFullScreen == false)
                        {
                            toggleFullScreen();
                            bFullScreen = true;
                        }
                        else
                        {
                            toggleFullScreen();
                            bFullScreen = false;
                        }
                        break;
                    default:
                        break;
                }
                break;
            case ButtonPress:
                switch(event.xbutton.button)
                {
                    case 1:                         //left button
                        break;
                    case 2:                         //middle button
                        break;
                    case 3:                         //right button
                        break;
                    default:
                        break;
                }
                break;
            case MotionNotify:
                break;
            case ConfigureNotify:
                winWidth    = event.xconfigure.width;
                winHeight   = event.xconfigure.height;
                break;
            case Expose:
                gc          = XCreateGC(gpDisplay, gWindow, 0, &gcValues);
                XSetFont(gpDisplay, gc, pXFontStruct->fid);
                XAllocNamedColor(gpDisplay, gColormap, "green", &textColor, &textColor);
                XSetForeground(gpDisplay, gc, textColor.pixel);
                strLength   = strlen(str);
                strWidth    = XTextWidth(pXFontStruct, str, strLength);
                fontHeight  = pXFontStruct->ascent + pXFontStruct->descent;
                XDrawString(gpDisplay,
                        gWindow,
                        gc,
                        (winWidth/2 - strWidth/2),
                        (winHeight/2 - fontHeight/2),
                        str,
                        strLength);
                break;
            case DestroyNotify:
                break;
            case 33:
                XFree(gpDisplay);
                XUnloadFont(gpDisplay, pXFontStruct->fid);
                uninitialize();
                exit(0);
                break;
            default:
                break;
        }
    }

    uninitialize();
    return(0);
}

void createWindow(void)
{
    //function prototype
    void uninitialize(void);

    //variables declarations
    XSetWindowAttributes winAttribs;
    int defaultScreen;
    int defaultDepth;
    int styleMask;

    //code
    gpDisplay = XOpenDisplay(NULL);
    if(!gpDisplay)
    {
        printf("ERROR : Unable To Open XDisplay.\nExitting Now ...\n");
        uninitialize();
        exit(1);
    }

    defaultScreen = XDefaultScreen(gpDisplay);

    defaultDepth = DefaultDepth(gpDisplay, defaultScreen);

    gpXVisualInfo = (XVisualInfo *)malloc(sizeof(XVisualInfo));
    if(!gpXVisualInfo)
    {
        printf("ERROR : Unable to Allocate Memory For Visual Info.\n Exitting Now ...\n");
        uninitialize();
        exit(1);
    }

    XMatchVisualInfo(gpDisplay, defaultScreen, defaultDepth, TrueColor, gpXVisualInfo);
    if(!gpXVisualInfo)
    {
        printf("ERROR : Unable To Get A Visual.\nExitting Now ...\n");
        uninitialize();
        exit(1);
    }

    winAttribs.border_pixel = 0;
    winAttribs.background_pixmap = 0;
    winAttribs.colormap = XCreateColormap(gpDisplay,
            RootWindow(gpDisplay,gpXVisualInfo->screen),
            gpXVisualInfo->visual,
            AllocNone);
    gColormap = winAttribs.colormap;

    winAttribs.background_pixel = BlackPixel(gpDisplay, defaultScreen);
    winAttribs.event_mask = ExposureMask | VisibilityChangeMask | ButtonPressMask |
            KeyPressMask | PointerMotionMask | StructureNotifyMask;
    
    styleMask = CWBorderPixel | CWBackPixel | CWEventMask | CWColormap;

    gWindow = XCreateWindow(gpDisplay,
                            RootWindow(gpDisplay, gpXVisualInfo->screen),
                            0,
                            0,
                            giWindowWidth,
                            giWindowHeight,
                            0,
                            gpXVisualInfo->depth,
                            InputOutput,
                            gpXVisualInfo->visual,
                            styleMask,
                            &winAttribs);
    if(!gWindow)
    {
        printf("ERROR : Failed To Create Main Window");
        uninitialize();
        exit(1);
    }

    XStoreName(gpDisplay, gWindow, "Hello_World");

    Atom windowManagerDelete = XInternAtom(gpDisplay, "WM_DELETE_WINDOW", True);
    XSetWMProtocols(gpDisplay, gWindow, &windowManagerDelete, 1);

    XMapWindow(gpDisplay, gWindow);
}

void toggleFullScreen(void)
{
    //variable declarations
    Atom wm_state;
    Atom fullscreen;
    XEvent xev = {0};

    //code
    wm_state = XInternAtom(gpDisplay, "_NET_WM_STATE", False);
    memset(&xev, 0, sizeof(xev));

    xev.type = ClientMessage;
    xev.xclient.window = gWindow;
    xev.xclient.message_type = wm_state;
    xev.xclient.data.l[0] = bFullScreen ? 0 : 1;

    fullscreen = XInternAtom(gpDisplay, "_NET_WM_STATE_FULLSCREEN", False);
    xev.xclient.data.l[1] =  fullscreen;

    XSendEvent(gpDisplay,
            RootWindow(gpDisplay, gpXVisualInfo->screen),
            False,
            StructureNotifyMask,
            &xev);
}

void uninitialize(void)
{
    if(gWindow)
    {
        XDestroyWindow(gpDisplay, gWindow);
    }

    if(gColormap)
    {
        XFreeColormap(gpDisplay, gColormap);
    }

    if(gpXVisualInfo)
    {
        free(gpXVisualInfo);
        gpXVisualInfo=NULL;
    }

    if(gpDisplay)
    {
        XCloseDisplay(gpDisplay);
        gpDisplay=NULL;
    }
}

