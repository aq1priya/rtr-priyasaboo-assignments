#include<stdio.h>
#include<iostream>
#include<stdlib.h>
#include<memory.h>

#include<X11/Xlib.h>
#include<X11/Xutil.h>
#include<X11/XKBlib.h>
#include<X11/keysym.h>

#include<GL/gl.h>
#include<GL/glx.h>
#include<GL/glu.h>

//namespaces
using namespace std;

//global variables declarations
bool bFullScreen = false;
Display *gpDisplay = NULL;
XVisualInfo *gpXVisualInfo =NULL;
Colormap gColormap;
Window gWindow;
int giWindowWidth = 800;
int giWindowHeight = 600;
GLXContext gGLXContext;
GLfloat angleTriangle=0.0f, angleRect =0.0f;

//entry point function
int main(void)
{
    //function prototype
    void createWindow(void);
    void toggleFullScreen(void);
    void initialize(void);
    void resize(int, int);
    void display(void);
    void uninitialize(void);

    //variable declarations
    int winWidth = giWindowWidth;
    int winHeight = giWindowHeight;
    bool bDone = false;
    char keys[26];

    //code
    createWindow();

    //initialize
    initialize();

    //message Loop
    XEvent event;
    KeySym keysym;

    while(bDone == false)
    {
        while(XPending(gpDisplay))
        {
            XNextEvent(gpDisplay, &event);
            switch(event.type)
            {
                case MapNotify:
                    break;
                case KeyPress:
                    keysym = XkbKeycodeToKeysym(gpDisplay, event.xkey.keycode, 0, 0);
                    switch(keysym)
                    {
                        case XK_Escape:
                            bDone = true;
                            break;
                        default:
                            break;
                    }
                    XLookupString(&event.xkey, keys, sizeof(keys), NULL, NULL);
                    switch(keys[0])
                    {
                        case 'f':
                        case 'F':
                            if(bFullScreen == false)
                            {
                                toggleFullScreen();
                                bFullScreen = true;
                            }
                            else
                            {
                                toggleFullScreen();
                                bFullScreen = false;
                            }
                            break;
                        default:
                            break;
                    }
                    break;
                case ButtonPress:
                    switch(event.xbutton.button)
                    {
                        case 1:
                            break;
                        case 2:
                            break;
                        case 3:
                            break;
                        default:
                            break;
                    }
                    break;
                case MotionNotify:
                    break;
                case ConfigureNotify:
                    winWidth = event.xconfigure.width;
                    winHeight =event.xconfigure.height;
                    resize(winWidth, winHeight);
                    break;
                case Expose:
                    break;
                case DestroyNotify:
                    break;
                case 33:
                    bDone = true;
                    break;
                default:
                    break;
            }
            display();
        }
    }
    uninitialize();
    return(0);
}

void createWindow(void)
{
    //function prototype
    void uninitialize(void);

    //variable declarations
    XSetWindowAttributes winAttribs;
    int defaultScreen;
    int styleMask;
    static int frameBufferAttributes[] = {
        GLX_DOUBLEBUFFER, True,
        GLX_RGBA,
        GLX_RED_SIZE, 8,
        GLX_GREEN_SIZE, 8,
        GLX_BLUE_SIZE, 8,
        None };

    //code
    gpDisplay = XOpenDisplay(NULL);
    if(!gpDisplay)
    {
        printf("ERROR: Unable To Open XDisplay.\nExitting Now ..\n");
        uninitialize();
        exit(1);
    }

    defaultScreen = XDefaultScreen(gpDisplay);

    gpXVisualInfo = glXChooseVisual(gpDisplay, defaultScreen, frameBufferAttributes);
    if(!gpXVisualInfo)
    {
        printf("ERROR: Unable To Get A Visual.\nExitting Now ..\n");
        uninitialize();
        exit(1);
    }

    winAttribs.border_pixel = 0;
    winAttribs.background_pixmap =0;
    winAttribs.colormap = XCreateColormap(gpDisplay,
                                          RootWindow(gpDisplay, gpXVisualInfo->screen),
                                          gpXVisualInfo->visual,
                                          AllocNone);

    gColormap = winAttribs.colormap;

    winAttribs.background_pixel = BlackPixel(gpDisplay, defaultScreen);
    winAttribs.event_mask = ExposureMask | VisibilityChangeMask | ButtonPressMask | KeyPressMask | PointerMotionMask | StructureNotifyMask;
    styleMask = CWBorderPixel | CWBackPixel | CWEventMask | CWColormap;

    gWindow = XCreateWindow(gpDisplay,
                            RootWindow(gpDisplay, gpXVisualInfo->screen),
                            0,
                            0,
                            giWindowWidth,
                            giWindowHeight,
                            0,
                            gpXVisualInfo->depth,
                            InputOutput,
                            gpXVisualInfo->visual,
                            styleMask,
                            &winAttribs);
    if(!gWindow)
    {
        printf("ERROR: Failed To Create Main Window.\nExitting Now ..\n");
        uninitialize();
        exit(1);
    }

    XStoreName(gpDisplay, gWindow, "Static India");

    Atom WindowManagerDelete = XInternAtom(gpDisplay, "WM_DELETE_WINDOW", True);
    XSetWMProtocols(gpDisplay, gWindow, &WindowManagerDelete, 1);
    XMapWindow(gpDisplay, gWindow);
}

void toggleFullScreen(void)
{
  //variable declarations
    Atom  wm_state;
    Atom fullscreen;
    XEvent xev = {0};

    //code
    wm_state = XInternAtom(gpDisplay, "_NET_WM_STATE", False);
    memset(&xev,0,sizeof(xev));

    xev.type = ClientMessage;
    xev.xclient.window = gWindow;
    xev.xclient.message_type = wm_state;
    xev.xclient.format = 32;
    xev.xclient.data.l[0] = bFullScreen ? 0 : 1;

    fullscreen = XInternAtom(gpDisplay, "_NET_WM_STATE_FULLSCREEN", False);
    xev.xclient.data.l[1] = fullscreen;
                       
    fullscreen = XInternAtom(gpDisplay, "_NET_WM_STATE_FULLSCREEN", False);
    xev.xclient.data.l[1] = fullscreen;
    
    XSendEvent(gpDisplay,
               RootWindow(gpDisplay, gpXVisualInfo->screen),
               False,
               StructureNotifyMask,
               &xev);
}
  
//initialize() function
void initialize(void)
{
    gGLXContext = glXCreateContext(gpDisplay, gpXVisualInfo,NULL, GL_TRUE);

    glXMakeCurrent(gpDisplay,gWindow,gGLXContext);

    glClearColor(0.0f,0.0f,0.0f,1.0f);
}

//resize() function
void resize(int width, int height)
{
    if(height == 0)
        height =1;
    glViewport(0,0,(GLsizei)width, (GLsizei)height);
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    gluPerspective(45.0f, (GLfloat)width/(GLfloat)height, 0.1f,100.0f);
}

//uninitialize function
void uninitialize(void)
{
    GLXContext current;
    current =glXGetCurrentContext();
    if(current !=NULL && current ==gGLXContext)
    {
        glXMakeCurrent(gpDisplay,0,0);
    }
    if(gGLXContext)
    {
        glXDestroyContext(gpDisplay, gGLXContext);
    }
    if(gWindow)
    {
        XDestroyWindow(gpDisplay,gWindow);
    }
    if(gColormap)
    {
        XFreeColormap(gpDisplay, gColormap);
    }
    if(gpXVisualInfo)
    {
        free(gpXVisualInfo);
        gpXVisualInfo = NULL;
    }
    if(gpDisplay)
    {
        XCloseDisplay(gpDisplay);
        gpDisplay = NULL;
    }
}


//display function
void display(void)
{
    glClear(GL_COLOR_BUFFER_BIT);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(0.0f, 0.0f, -3.0f);
    
    glLineWidth(10.0f);
	glBegin(GL_LINES);	                //draw I
	glColor3f(1.0f, 0.6f, 0.2f);        //saffron
	glVertex3f(-1.20f, 0.5f, 0.0f);     
	glVertex3f(-0.88f, 0.5f, 0.0f);

	glColor3f(0.070f, 0.533f, 0.027f);  //green
	glVertex3f(-1.20f, -0.5f, 0.0f);    
	glVertex3f(-0.88f, -0.5f, 0.0f);

	glColor3f(1.0f, 0.6f, 0.2f);
	glVertex3f(-1.04f, 0.5f, 0.0f);     
	glColor3f(0.070f, 0.533f, 0.027f);
	glVertex3f(-1.04f, -0.5f, 0.0f);
	glEnd();

    glBegin(GL_LINES);	                // drwa N
	glColor3f(1.0f, 0.6f, 0.2f);    
	glVertex3f(-0.68f, 0.52f, 0.0f);   
	glColor3f(0.070f, 0.533f, 0.027f);
	glVertex3f(-0.68f, -0.52f, 0.0f);

	glColor3f(1.0f, 0.6f, 0.2f);
	glVertex3f(-0.68f, 0.52f, 0.0f);    
	glColor3f(0.070f, 0.533f, 0.027f);
	glVertex3f(-0.36f, -0.51f, 0.0f);

	glColor3f(1.0f, 0.6f, 0.2f);
	glVertex3f(-0.36f, 0.52f, 0.0f);
	glColor3f(0.070f, 0.533f, 0.027f);
	glVertex3f(-0.36f, -0.52f, 0.0f);
	glEnd();

 	glBegin(GL_LINES);	                // draw D
	glColor3f(1.0f, 0.6f, 0.2f);
	glVertex3f(-0.10f, 0.5f, 0.0f);
	glColor3f(0.070f, 0.533f, 0.027f);
	glVertex3f(-0.10f, -0.5f, 0.0f);

	glColor3f(1.0f, 0.6f, 0.2f);
	glVertex3f(-0.17f, 0.5f, 0.0f);
	glColor3f(1.0f, 0.6f, 0.2f);
	glVertex3f(0.17f, 0.5f, 0.0f);

	glColor3f(1.0f, 0.6f, 0.2f);
	glVertex3f(0.17f, 0.5f, 0.0f);
	glColor3f(0.070f, 0.533f, 0.027f);
	glVertex3f(0.17f, -0.5f, 0.0f);

	glColor3f(0.070f, 0.533f, 0.027f);
	glVertex3f(-0.17f, -0.5f, 0.0f);
	glColor3f(0.070f, 0.533f, 0.027f);
	glVertex3f(0.17f, -0.5f, 0.0f);
	glEnd();
   
    glBegin(GL_LINES);	                // draw I
	glColor3f(1.0f, 0.6f, 0.2f);        //saffron
	glVertex3f(0.36f, 0.5f, 0.0f);
	glVertex3f(0.68f, 0.5f, 0.0f);

	glColor3f(0.070f, 0.533f, 0.027f);  //green
	glVertex3f(0.36f, -0.5f, 0.0f);
	glVertex3f(0.68f, -0.5f, 0.0f);

	glColor3f(1.0f, 0.6f, 0.2f);
	glVertex3f(0.52f, 0.5f, 0.0f);
	glColor3f(0.070f, 0.533f, 0.027f);
	glVertex3f(0.52f, -0.5f, 0.0f);
	glEnd();

	glBegin(GL_LINES);	                // draw A
	glColor3f(0.070f, 0.533f, 0.027f);
	glVertex3f(0.83f, -0.52f, 0.0f);    //1
	glColor3f(1.0f, 0.6f, 0.2f);
	glVertex3f(1.04f, 0.52f, 0.0f);     //2
	glColor3f(1.0f, 0.6f, 0.2f);
	glVertex3f(1.04f, 0.52f, 0.0f);     //2
	glColor3f(0.070f, 0.533f, 0.027f);
	glVertex3f(1.25f, -0.52f, 0.0f);    //4
	glEnd();

	glBegin(GL_QUADS);	                //flag tricolor
	glColor3f(1.0f, 0.6f, 0.2f);	    //saffron
	glVertex3f(0.953f, 0.015f, 0.0f);
	glVertex3f(1.128f, 0.015f, 0.0f);
	glVertex3f(1.122f, 0.045f, 0.0f);
	glVertex3f(0.958f, 0.045f, 0.0f);
	
	glColor3f(1.0f, 1.0f, 1.0f);        //white
	glVertex3f(0.947f, -0.015f, 0.0f);
	glVertex3f(1.135f, -0.015f, 0.0f);
	glVertex3f(1.128f, 0.015f, 0.0f);
	glVertex3f(0.953f, 0.015f, 0.0f);

	glColor3f(0.070f, 0.533f, 0.027f);  //green
	glVertex3f(1.135f, -0.015f, 0.0f);
	glVertex3f(0.947f, -0.015f, 0.0f);
	glVertex3f(0.94f, -0.045f, 0.0f);
	glVertex3f(1.14f, -0.045f, 0.0f);
	glEnd();
    
    glXSwapBuffers(gpDisplay, gWindow);
}
