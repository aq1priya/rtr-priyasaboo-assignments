#include<stdio.h>
#include<iostream>
#include<stdlib.h>
#include<memory.h>

#include<X11/Xlib.h>
#include<X11/Xutil.h>
#include<X11/XKBlib.h>
#include<X11/keysym.h>

#include<GL/gl.h>
#include<GL/glx.h>
#include<GL/glu.h>
#include<SOIL/SOIL.h>

//namespaces
using namespace std;

//global variables declarations
bool bFullScreen = false;
bool bLight = false;
Display *gpDisplay = NULL;
XVisualInfo *gpXVisualInfo =NULL;
Colormap gColormap;
Window gWindow;
int giWindowWidth = 800;
int giWindowHeight = 600;
GLXContext gGLXContext;

//variables for light properties
GLfloat lightAmbientZero[]      = {0.0f, 0.0f, 0.0f, 1.0f};
GLfloat lightDiffuseZero[]      = {1.0f, 0.0f, 0.0f, 1.0f};
GLfloat lightPositionZero[]     = {-2.0f, 0.0f, 0.0f, 1.0f};
GLfloat lightSpecularZero[]     = {1.0f, 0.0f, 0.0f, 1.0f};

GLfloat LightAmbientOne[] = { 0.0f,0.0f,0.0f,1.0f };
GLfloat LightDiffuseOne[] = { 0.0f,0.0f,1.0f,1.0f };	//Blue
GLfloat LightSpecularOne[] = { 0.0f,0.0f,1.0f,1.0f };	//Blue
GLfloat LightPositionOne[] = { 2.0f,00.0f,0.0f,1.0f };

//variables for material properties
GLfloat MaterialAmbient[] = { 0.5f,0.5f,0.5f,1.0f };
GLfloat MaterialDiffuse[] = { 1.0f,1.0f,1.0f,1.0f };
GLfloat MaterialSpecular[] = { 1.0f,1.0f,1.0f,1.0f };
GLfloat MaterialShininess[] = { 50.0f };//128.0f

GLfloat anglePyramid =0.0f;

//entry point function
int main(void)
{
    //function prototype
    void createWindow(void);
    void toggleFullScreen(void);
    void initialize(void);
    void resize(int, int);
    void display(void);
    void uninitialize(void);
    void update(void);

    //variable declarations
    int winWidth = giWindowWidth;
    int winHeight = giWindowHeight;
    bool bDone = false;
    char keys[26];

    //code
    createWindow();

    //initialize
    initialize();

    //message Loop
    XEvent event;
    KeySym keysym;

    while(bDone == false)
    {
        while(XPending(gpDisplay))
        {
            XNextEvent(gpDisplay, &event);
            switch(event.type)
            {
                case MapNotify:
                    break;
                case KeyPress:
                    keysym = XkbKeycodeToKeysym(gpDisplay, event.xkey.keycode, 0, 0);
                    switch(keysym)
                    {
                        case XK_Escape:
                            bDone = true;
                            break;
                        default:
                            break;
                    }
                    XLookupString(&event.xkey, keys, sizeof(keys), NULL, NULL);
                    switch(keys[0])
                    {
                        case 'f':
                        case 'F':
                            if(bFullScreen == false)
                            {
                                toggleFullScreen();
                                bFullScreen = true;
                            }
                            else
                            {
                                toggleFullScreen();
                                bFullScreen = false;
                            }
                            break;
                        case 'l':
                        case 'L':
                            if(!bLight)
                            {
                                bLight = true;
                                glEnable(GL_LIGHTING);
                            }
                            else
                            {
                                bLight = false;
                                glDisable(GL_LIGHTING);
                            }
                            break;
                        default:
                            break;
                    }
                    break;
                case ButtonPress:
                    switch(event.xbutton.button)
                    {
                        case 1:
                            break;
                        case 2:
                            break;
                        case 3:
                            break;
                        default:
                            break;
                    }
                    break;
                case MotionNotify:
                    break;
                case ConfigureNotify:
                    winWidth = event.xconfigure.width;
                    winHeight =event.xconfigure.height;
                    resize(winWidth, winHeight);
                    break;
                case Expose:
                    break;
                case DestroyNotify:
                    break;
                case 33:
                    bDone = true;
                    break;
                default:
                    break;
            }
        }
            update();
            display();
    }
    uninitialize();
    return(0);
}

void createWindow(void)
{
    //function prototype
    void uninitialize(void);

    //variable declarations
    XSetWindowAttributes winAttribs;
    int defaultScreen;
    int styleMask;
    static int frameBufferAttributes[] = {
        GLX_DOUBLEBUFFER, True,
        GLX_RGBA,
        GLX_RED_SIZE, 8,
        GLX_GREEN_SIZE, 8,
        GLX_BLUE_SIZE, 8,
        GLX_ALPHA_SIZE, 8,
        GLX_DEPTH_SIZE, 24,
        None };

    //code
    gpDisplay = XOpenDisplay(NULL);
    if(!gpDisplay)
    {
        printf("ERROR: Unable To Open XDisplay.\nExitting Now ..\n");
        uninitialize();
        exit(1);
    }

    defaultScreen = XDefaultScreen(gpDisplay);

    gpXVisualInfo = glXChooseVisual(gpDisplay, defaultScreen, frameBufferAttributes);
    if(!gpXVisualInfo)
    {
        printf("ERROR: Unable To Get A Visual.\nExitting Now ..\n");
        uninitialize();
        exit(1);
    }

    winAttribs.border_pixel = 0;
    winAttribs.background_pixmap =0;
    winAttribs.colormap = XCreateColormap(gpDisplay,
                                          RootWindow(gpDisplay, gpXVisualInfo->screen),
                                          gpXVisualInfo->visual,
                                          AllocNone);

    gColormap = winAttribs.colormap;

    winAttribs.background_pixel = BlackPixel(gpDisplay, defaultScreen);
    winAttribs.event_mask = ExposureMask | VisibilityChangeMask | ButtonPressMask | KeyPressMask | PointerMotionMask | StructureNotifyMask;
    styleMask = CWBorderPixel | CWBackPixel | CWEventMask | CWColormap;

    gWindow = XCreateWindow(gpDisplay,
                            RootWindow(gpDisplay, gpXVisualInfo->screen),
                            0,
                            0,
                            giWindowWidth,
                            giWindowHeight,
                            0,
                            gpXVisualInfo->depth,
                            InputOutput,
                            gpXVisualInfo->visual,
                            styleMask,
                            &winAttribs);
    if(!gWindow)
    {
        printf("ERROR: Failed To Create Main Window.\nExitting Now ..\n");
        uninitialize();
        exit(1);
    }

    XStoreName(gpDisplay, gWindow, "Pyramid with Two Colored Rotating Light - Priya Saboo");

    Atom WindowManagerDelete = XInternAtom(gpDisplay, "WM_DELETE_WINDOW", True);
    XSetWMProtocols(gpDisplay, gWindow, &WindowManagerDelete, 1);
    XMapWindow(gpDisplay, gWindow);
}

void toggleFullScreen(void)
{
  //variable declarations
    Atom  wm_state;
    Atom fullscreen;
    XEvent xev = {0};

    //code
    wm_state = XInternAtom(gpDisplay, "_NET_WM_STATE", False);
    memset(&xev,0,sizeof(xev));

    xev.type = ClientMessage;
    xev.xclient.window = gWindow;
    xev.xclient.message_type = wm_state;
    xev.xclient.format = 32;
    xev.xclient.data.l[0] = bFullScreen ? 0 : 1;

    fullscreen = XInternAtom(gpDisplay, "_NET_WM_STATE_FULLSCREEN", False);
    xev.xclient.data.l[1] = fullscreen;
                       
    fullscreen = XInternAtom(gpDisplay, "_NET_WM_STATE_FULLSCREEN", False);
    xev.xclient.data.l[1] = fullscreen;
    
    XSendEvent(gpDisplay,
               RootWindow(gpDisplay, gpXVisualInfo->screen),
               False,
               StructureNotifyMask,
               &xev);
}
  
//initialize() function
void initialize(void)
{
    //code
    gGLXContext = glXCreateContext(gpDisplay, gpXVisualInfo,NULL, GL_TRUE);

    glXMakeCurrent(gpDisplay,gWindow,gGLXContext);
    
    glShadeModel(GL_SMOOTH);
    
    glClearDepth(1.0f);
    glEnable(GL_DEPTH_TEST);
    glDepthFunc(GL_LEQUAL);
    glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);
    
    glClearColor(0.0f,0.0f,0.0f,1.0f);
    
    //setting light properties(LIGHT0)
    glLightfv(GL_LIGHT0, GL_AMBIENT, lightAmbientZero);
    glLightfv(GL_LIGHT0, GL_DIFFUSE, lightDiffuseZero);
    glLightfv(GL_LIGHT0, GL_POSITION, lightPositionZero);
    glLightfv(GL_LIGHT0, GL_SPECULAR, lightSpecularZero);
    
    glEnable(GL_LIGHT0);

    //seting light position and light properties of LIGHT1
	glLightfv(GL_LIGHT1, GL_AMBIENT, LightAmbientOne);
	glLightfv(GL_LIGHT1, GL_DIFFUSE, LightDiffuseOne);
	glLightfv(GL_LIGHT1, GL_POSITION, LightPositionOne);
	glLightfv(GL_LIGHT1, GL_SPECULAR, LightSpecularOne);

    glEnable(GL_LIGHT1);

   	//setting material properties
	glMaterialfv(GL_FRONT, GL_AMBIENT, MaterialAmbient);
	glMaterialfv(GL_FRONT, GL_DIFFUSE, MaterialDiffuse);
	glMaterialfv(GL_FRONT, GL_SHININESS, MaterialShininess);
	glMaterialfv(GL_FRONT, GL_SPECULAR, MaterialSpecular);

}

//update function
void update(void)
{
    if(anglePyramid == 360)
    {
        anglePyramid = 0.0f;
    }
    anglePyramid =anglePyramid + 0.03f;
}

//resize() function
void resize(int width, int height)
{
    if(height == 0)
        height =1;
    glViewport(0,0,(GLsizei)width, (GLsizei)height);
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    gluPerspective(45.0f, (GLfloat)width/(GLfloat)height, 0.1f,100.0f);
}

//uninitialize function
void uninitialize(void)
{
    GLXContext current;
    current =glXGetCurrentContext();
    if(current !=NULL && current ==gGLXContext)
    {
        glXMakeCurrent(gpDisplay,0,0);
    }
    if(gGLXContext)
    {
        glXDestroyContext(gpDisplay, gGLXContext);
    }
    if(gWindow)
    {
        XDestroyWindow(gpDisplay,gWindow);
    }
    if(gColormap)
    {
        XFreeColormap(gpDisplay, gColormap);
    }
    if(gpXVisualInfo)
    {
        free(gpXVisualInfo);
        gpXVisualInfo = NULL;
    }
    if(gpDisplay)
    {
        XCloseDisplay(gpDisplay);
        gpDisplay = NULL;
    }
}

//display function
void display(void)
{
    //function declaration
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
    glTranslatef(0.0f,0.0f,-7.0f);
    glRotatef(anglePyramid, 0.0f,1.0f,0.0f);
    
    glBegin(GL_TRIANGLES);
	//front face
    glNormal3f(0.0f, 0.447214f, 0.894427f);
	glVertex3f(0.0f, 1.0f, 0.0f);
	glVertex3f(-1.0f, -1.0f, 1.0f);
	glVertex3f(1.0f, -1.0f, 1.0f);

	//right face
    glNormal3f(0.894427f, 0.447214f, 0.0f);
	glVertex3f(0.0f, 1.0f, 0.0f);
	glVertex3f(1.0f, -1.0f, 1.0f);
	glVertex3f(1.0f, -1.0f, -1.0f);

	//back face
    glNormal3f(0.0f, 0.447214f, -0.894427f);
	glVertex3f(0.0f, 1.0f, 0.0f);
	glVertex3f(1.0f, -1.0f, -1.0f);
	glVertex3f(-1.0f, -1.0f, -1.0f);

	//left face
    glNormal3f(-0.89447214f, 0.447214f, 0.0f);
	glVertex3f(0.0f, 1.0f, 0.0f);
	glVertex3f(-1.0f, -1.0f, -1.0f);
	glVertex3f(-1.0f, -1.0f, 1.0f);
	glEnd();

    
    glXSwapBuffers(gpDisplay, gWindow);
}


