#include<stdio.h>
#include<iostream>
#include<stdlib.h>
#include<memory.h>

#include<X11/Xlib.h>
#include<X11/Xutil.h>
#include<X11/XKBlib.h>
#include<X11/keysym.h>

#include<GL/gl.h>
#include<GL/glx.h>
#include<GL/glu.h>
#include<SOIL/SOIL.h>

//namespaces
using namespace std;

//global variables declarations
bool bFullScreen = false;
bool bLight = false;
Display *gpDisplay = NULL;
XVisualInfo *gpXVisualInfo =NULL;
Colormap gColormap;
Window gWindow;
int giWindowWidth = 800;
int giWindowHeight = 600;
GLXContext gGLXContext;

//variables for light properties
GLfloat lightAmbient[]      = {0.0f, 0.0f, 0.0f, 1.0f};
GLfloat lightSpecular[]     = {1.0f, 1.0f, 1.0f, 1.0f, 1.0f};
GLfloat lightDiffuse[]      = {1.0f, 1.0f, 1.0f, 1.0f};
GLfloat lightPosition[]     = {0.0f, 0.0f, 0.0f, 1.0f};

GLfloat lightModelAmbient[] = {0.2f,0.2f,0.2f, 1.0f};
GLfloat lightModelLocalViewer[] = {0.0f};

//variables for material properties
GLfloat materialAmbient[]   = {0.5f, 0.5f, 0.5f, 1.0f};
GLfloat materialDiffuse[]   = {1.0f, 1.0f, 1.0f, 1.0f};
GLfloat materialSpecular[]  = {1.0f, .0f, 1.0f, 1.0f};
GLfloat materialShininess[] = {50.0f};

GLUquadric *spheres[24];
GLint keyPress = 0;
GLfloat angleOfXRotation =0.0f;
GLfloat angleOfYRotation =0.0f;
GLfloat angleOfZRotation = 0.0f;

//entry point function
int main(void)
{
    //function prototype
    void createWindow(void);
    void toggleFullScreen(void);
    void initialize(void);
    void resize(int, int);
    void display(void);
    void update(void);
    void uninitialize(void);

    //variable declarations
    int winWidth = giWindowWidth;
    int winHeight = giWindowHeight;
    bool bDone = false;
    char keys[26];

    //code
    createWindow();

    //initialize
    initialize();

    //message Loop
    XEvent event;
    KeySym keysym;

    while(bDone == false)
    {
        while(XPending(gpDisplay))
        {
            XNextEvent(gpDisplay, &event);
            switch(event.type)
            {
                case MapNotify:
                    break;
                case KeyPress:
                    keysym = XkbKeycodeToKeysym(gpDisplay, event.xkey.keycode, 0, 0);
                    switch(keysym)
                    {
                        case XK_Escape:
                            bDone = true;
                            break;
                        default:
                            break;
                    }
                    XLookupString(&event.xkey, keys, sizeof(keys), NULL, NULL);
                    switch(keys[0])
                    {
                        case 'f':
                        case 'F':
                            if(bFullScreen == false)
                            {
                                toggleFullScreen();
                                bFullScreen = true;
                            }
                            else
                            {
                                toggleFullScreen();
                                bFullScreen = false;
                            }
                            break;
                        case 'l':
                        case 'L':
                            if(!bLight)
                            {
                                bLight = true;
                                glEnable(GL_LIGHTING);
                            }
                            else
                            {
                                bLight = false;
                                glDisable(GL_LIGHTING);
                            }
                            break;
                        case 'x':
                        case 'X':
                            keyPress = 1;
                            angleOfXRotation = 0.0f;
                            break;
                        case 'y':
                        case 'Y':
                            keyPress = 2;
                            angleOfYRotation = 0.0f;
                            break;
                        case 'z':
                        case 'Z':
                            keyPress =3;
                            angleOfZRotation = 0.0f;
                            break;
                        default:
                            break;
                    }
                    break;
                case ButtonPress:
                    switch(event.xbutton.button)
                    {
                        case 1:
                            break;
                        case 2:
                            break;
                        case 3:
                            break;
                        default:
                            break;
                    }
                    break;
                case MotionNotify:
                    break;
                case ConfigureNotify:
                    winWidth = event.xconfigure.width;
                    winHeight =event.xconfigure.height;
                    resize(winWidth, winHeight);
                    break;
                case Expose:
                    break;
                case DestroyNotify:
                    break;
                case 33:
                    bDone = true;
                    break;
                default:
                    break;
            }
        }
        update();
        display();
    }
    uninitialize();
    return(0);
}

void createWindow(void)
{
    //function prototype
    void uninitialize(void);

    //variable declarations
    XSetWindowAttributes winAttribs;
    int defaultScreen;
    int styleMask;
    static int frameBufferAttributes[] = {
        GLX_DOUBLEBUFFER, True,
        GLX_RGBA,
        GLX_RED_SIZE, 8,
        GLX_GREEN_SIZE, 8,
        GLX_BLUE_SIZE, 8,
        GLX_ALPHA_SIZE, 8,
        GLX_DEPTH_SIZE, 24,
        None };

    //code
    gpDisplay = XOpenDisplay(NULL);
    if(!gpDisplay)
    {
        printf("ERROR: Unable To Open XDisplay.\nExitting Now ..\n");
        uninitialize();
        exit(1);
    }

    defaultScreen = XDefaultScreen(gpDisplay);

    gpXVisualInfo = glXChooseVisual(gpDisplay, defaultScreen, frameBufferAttributes);
    if(!gpXVisualInfo)
    {
        printf("ERROR: Unable To Get A Visual.\nExitting Now ..\n");
        uninitialize();
        exit(1);
    }

    winAttribs.border_pixel = 0;
    winAttribs.background_pixmap =0;
    winAttribs.colormap = XCreateColormap(gpDisplay,
                                          RootWindow(gpDisplay, gpXVisualInfo->screen),
                                          gpXVisualInfo->visual,
                                          AllocNone);

    gColormap = winAttribs.colormap;

    winAttribs.background_pixel = BlackPixel(gpDisplay, defaultScreen);
    winAttribs.event_mask = ExposureMask | VisibilityChangeMask | ButtonPressMask | KeyPressMask | PointerMotionMask | StructureNotifyMask;
    styleMask = CWBorderPixel | CWBackPixel | CWEventMask | CWColormap;

    gWindow = XCreateWindow(gpDisplay,
                            RootWindow(gpDisplay, gpXVisualInfo->screen),
                            0,
                            0,
                            giWindowWidth,
                            giWindowHeight,
                            0,
                            gpXVisualInfo->depth,
                            InputOutput,
                            gpXVisualInfo->visual,
                            styleMask,
                            &winAttribs);
    if(!gWindow)
    {
        printf("ERROR: Failed To Create Main Window.\nExitting Now ..\n");
        uninitialize();
        exit(1);
    }

    XStoreName(gpDisplay, gWindow, "24 Spheres - Priya Saboo");

    Atom WindowManagerDelete = XInternAtom(gpDisplay, "WM_DELETE_WINDOW", True);
    XSetWMProtocols(gpDisplay, gWindow, &WindowManagerDelete, 1);
    XMapWindow(gpDisplay, gWindow);
}

void toggleFullScreen(void)
{
  //variable declarations
    Atom  wm_state;
    Atom fullscreen;
    XEvent xev = {0};

    //code
    wm_state = XInternAtom(gpDisplay, "_NET_WM_STATE", False);
    memset(&xev,0,sizeof(xev));

    xev.type = ClientMessage;
    xev.xclient.window = gWindow;
    xev.xclient.message_type = wm_state;
    xev.xclient.format = 32;
    xev.xclient.data.l[0] = bFullScreen ? 0 : 1;

    fullscreen = XInternAtom(gpDisplay, "_NET_WM_STATE_FULLSCREEN", False);
    xev.xclient.data.l[1] = fullscreen;
                       
    fullscreen = XInternAtom(gpDisplay, "_NET_WM_STATE_FULLSCREEN", False);
    xev.xclient.data.l[1] = fullscreen;
    
    XSendEvent(gpDisplay,
               RootWindow(gpDisplay, gpXVisualInfo->screen),
               False,
               StructureNotifyMask,
               &xev);
}
  
//initialize() function
void initialize(void)
{
    //code
    gGLXContext = glXCreateContext(gpDisplay, gpXVisualInfo,NULL, GL_TRUE);

    glXMakeCurrent(gpDisplay,gWindow,gGLXContext);
    
    glShadeModel(GL_SMOOTH);
    
    glClearDepth(1.0f);
    glEnable(GL_DEPTH_TEST);
    glDepthFunc(GL_LEQUAL);
    glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);
    
    glClearColor(0.0f,0.0f,0.0f,1.0f);
    
    glEnable(GL_AUTO_NORMAL);
    glEnable(GL_NORMALIZE);

    glLightfv(GL_LIGHT0, GL_AMBIENT, lightAmbient);
    glLightfv(GL_LIGHT0, GL_DIFFUSE, lightDiffuse);
    glLightfv(GL_LIGHT0, GL_SPECULAR, lightSpecular);

    glLightModelfv(GL_LIGHT_MODEL_AMBIENT, lightModelAmbient);
    glLightModelfv(GL_LIGHT_MODEL_LOCAL_VIEWER, lightModelLocalViewer);

    glEnable(GL_LIGHT0);

    for(int i =0; i<24 ; i++)
    {
        spheres[i] = gluNewQuadric();
    }
}

//resize() function
void resize(int width, int height)
{
    if(height == 0)
        height =1;
    glViewport(0,0,(GLsizei)width, (GLsizei)height);
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    if (width <= height)
	{
		glOrtho(0.0f, 15.5f, 0.0f, 15.5f*((GLfloat)height / (GLfloat)width), -10.0f, 10.0f);
	}
	else
	{
		glOrtho(0.0f, 15.5f*((GLfloat)width / (GLfloat)height), 0.0f, 15.5f, -10.0f, 10.0f);
	}

}

//uninitialize function
void uninitialize(void)
{
    GLXContext current;
    current =glXGetCurrentContext();
    if(current !=NULL && current ==gGLXContext)
    {
        glXMakeCurrent(gpDisplay,0,0);
    }
    if(gGLXContext)
    {
        glXDestroyContext(gpDisplay, gGLXContext);
    }
    if(gWindow)
    {
        XDestroyWindow(gpDisplay,gWindow);
    }
    if(gColormap)
    {
        XFreeColormap(gpDisplay, gColormap);
    }
    if(gpXVisualInfo)
    {
        free(gpXVisualInfo);
        gpXVisualInfo = NULL;
    }
    if(gpDisplay)
    {
        XCloseDisplay(gpDisplay);
        gpDisplay = NULL;
    }
}

//update  function
void update(void)
{
  	if (angleOfXRotation == 360)
    	angleOfXRotation = 0.0f;
	angleOfXRotation = angleOfXRotation + 0.04f;

	if (angleOfYRotation == 360)
		angleOfYRotation = 0.0f;
	angleOfYRotation = angleOfYRotation + 0.04f;

	if (angleOfZRotation == 360)
		angleOfZRotation = 0.0f;
	angleOfZRotation = angleOfZRotation + 0.04f;
}

//display function
void display(void)
{
    //function declaration
    void draw24Spheres(void);
    
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
    
    if (keyPress == 1)
	{
		glRotatef(angleOfXRotation, 1.0f, 0.0f, 0.0f);
		lightPosition[1] = angleOfXRotation;
		lightPosition[0] = 0.0f;
		lightPosition[2] = 0.0f;
	}
	else if (keyPress == 2)
	{
		glRotatef(angleOfYRotation, 0.0f, 1.0f, 0.0f);
		lightPosition[2] = angleOfYRotation;
		lightPosition[0] = 0.0f;
		lightPosition[1] = 0.0f;
	}
	else if (keyPress == 3)
	{
		glRotatef(angleOfZRotation, 0.0f, 0.0f, 1.0f);
		lightPosition[0] = angleOfZRotation;
		lightPosition[1] = 0.0f;
		lightPosition[2] = 0.0f;
	}
	glLightfv(GL_LIGHT0, GL_POSITION, lightPosition);

	draw24Spheres();

    glXSwapBuffers(gpDisplay, gWindow);
}

//draw24Spheres() function
void draw24Spheres(void)
{
	//variables
	GLfloat materialAmbient[4];
	GLfloat materialDiffuse[4];
	GLfloat materialSpecular[4];
	GLfloat materialShininess[1];

	//code
	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);

	//***** 1st Column : Gems *****
	//	1st Sphere : emarald
	//material ambient
	materialAmbient[0] = 0.0215f;
	materialAmbient[1] = 0.1745f;
	materialAmbient[2] = 0.0215f;
	materialAmbient[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_AMBIENT, materialAmbient);
	//diffuse material
	materialDiffuse[0] = 0.07568f;
	materialDiffuse[1] = 0.61424f;
	materialDiffuse[2] = 0.07568f;
	materialDiffuse[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_DIFFUSE, materialDiffuse);
	//specular material
	materialSpecular[0] = 0.633f;
	materialSpecular[1] = 0.727811f;
	materialSpecular[2] = 0.633f;
	materialSpecular[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_SPECULAR, materialSpecular);
	//shininess material
	materialShininess[0] = 0.6f*128.0f;
	glMaterialfv(GL_FRONT, GL_SHININESS, materialShininess);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(9.5f, 14.0f, 0.0f);
	gluSphere(spheres[0], 1.0f, 30, 30);

	//2nd Sphere : Jade
	//material ambient
	materialAmbient[0] = 0.135f;
	materialAmbient[1] = 0.2225f;
	materialAmbient[2] = 0.1575f;
	materialAmbient[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_AMBIENT, materialAmbient);
	//diffuse material
	materialDiffuse[0] = 0.54f;
	materialDiffuse[1] = 0.89f;
	materialDiffuse[2] = 0.63f;
	materialDiffuse[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_DIFFUSE, materialDiffuse);
	//specular material
	materialSpecular[0] = 0.316228f;
	materialSpecular[1] = 0.316228f;
	materialSpecular[2] = 0.316228f;
	materialSpecular[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_SPECULAR, materialSpecular);
	//shininess material
	materialShininess[0] = 0.1f*128.0f;
	glMaterialfv(GL_FRONT, GL_SHININESS, materialShininess);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(9.5f, 11.5f, 0.0f);
	gluSphere(spheres[1], 1.0f, 30, 30);

	// 3rd Sphere : Obsidian
	//ambient material
	materialAmbient[0] = 0.05375f;
	materialAmbient[1] = 0.05f;
	materialAmbient[2] = 0.06625f;
	materialAmbient[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_AMBIENT, materialAmbient);
	//diffuse material
	materialDiffuse[0] = 0.18275f;
	materialDiffuse[1] = 0.17f;
	materialDiffuse[2] = 0.22525f;
	materialDiffuse[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_DIFFUSE, materialDiffuse);
	//specular material
	materialSpecular[0] = 0.332741f;
	materialSpecular[1] = 0.328634f;
	materialSpecular[2] = 0.346435f;
	materialSpecular[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_SPECULAR, materialSpecular);
	//shininess
	materialShininess[0] = 0.3f*128.0f;
	glMaterialfv(GL_FRONT, GL_SHININESS, materialShininess);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(9.5f, 9.0f, 0.0f);
	gluSphere(spheres[2], 1.0f, 30, 30);

	//4th Sphere : Pearl
	//ambient material
	materialAmbient[0] = 0.25f;
	materialAmbient[1] = 0.20725f;
	materialAmbient[2] = 0.20725f;
	materialAmbient[3] = 1.0f;
	//material diffuse
	materialDiffuse[0] = 1.0f;
	materialDiffuse[1] = 0.829f;
	materialDiffuse[2] = 0.829f;
	materialDiffuse[3] = 1.0f;
	//material specular
	materialSpecular[0] = 0.296648f;
	materialSpecular[0] = 0.296648f;
	materialSpecular[0] = 0.296648f;
	materialSpecular[0] = 1.0f;
	//shininess
	materialShininess[0] = 0.088f*128.0f;
	glMaterialfv(GL_FRONT, GL_AMBIENT, materialAmbient);
	glMaterialfv(GL_FRONT, GL_DIFFUSE, materialDiffuse);
	glMaterialfv(GL_FRONT, GL_SPECULAR, materialSpecular);
	glMaterialfv(GL_FRONT, GL_SHININESS, materialShininess);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(9.5f, 6.5f, 0.0f);
	gluSphere(spheres[3], 1.0f, 30, 30);

	//5th Sphere : Ruby
	//ambient material
	materialAmbient[0] = 0.1745f;
	materialAmbient[1] = 0.01175f;
	materialAmbient[2] = 0.01175f;
	materialAmbient[3] = 1.0f;
	//material diffuse
	materialDiffuse[0] = 0.61424f;
	materialDiffuse[1] = 0.04136f;
	materialDiffuse[2] = 0.04136f;
	materialDiffuse[3] = 1.0f;
	//material specular
	materialSpecular[0] = 0.727811f;
	materialSpecular[0] = 0.626959f;
	materialSpecular[0] = 0.626959f;
	materialSpecular[0] = 1.0f;
	//shininess
	materialShininess[0] = 0.6f*128.0f;
	glMaterialfv(GL_FRONT, GL_AMBIENT, materialAmbient);
	glMaterialfv(GL_FRONT, GL_DIFFUSE, materialDiffuse);
	glMaterialfv(GL_FRONT, GL_SPECULAR, materialSpecular);
	glMaterialfv(GL_FRONT, GL_SHININESS, materialShininess);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(9.5f, 4.0f, 0.0f);
	gluSphere(spheres[4], 1.0f, 30, 30);

	//6th Sphere : turquoise
	//ambient material
	materialAmbient[0] = 0.1f;
	materialAmbient[1] = 0.18725f;
	materialAmbient[2] = 0.1745f;
	materialAmbient[3] = 1.0f;
	//material diffuse
	materialDiffuse[0] = 0.396f;
	materialDiffuse[1] = 0.74151f;
	materialDiffuse[2] = 0.69102f;
	materialDiffuse[3] = 1.0f;
	//material specular
	materialSpecular[0] = 0.297254f;
	materialSpecular[0] = 0.30829f;
	materialSpecular[0] = 0.306678f;
	materialSpecular[0] = 1.0f;
	//shininess
	materialShininess[0] = 0.1f*128.0f;
	glMaterialfv(GL_FRONT, GL_AMBIENT, materialAmbient);
	glMaterialfv(GL_FRONT, GL_DIFFUSE, materialDiffuse);
	glMaterialfv(GL_FRONT, GL_SPECULAR, materialSpecular);
	glMaterialfv(GL_FRONT, GL_SHININESS, materialShininess);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(9.5f, 1.5f, 0.0f);
	gluSphere(spheres[5], 1.0f, 30, 30);

	// ***** 2nd column : Metal *****
	//	1st Sphere : brass
	materialAmbient[0] = 0.329412f;
	materialAmbient[1] = 0.223529f;
	materialAmbient[2] = 0.027451f;
	materialAmbient[3] = 1.0f;
	//material diffuse
	materialDiffuse[0] = 0.780392f;
	materialDiffuse[1] = 0.568627f;
	materialDiffuse[2] = 0.113725f;
	materialDiffuse[3] = 1.0f;
	//material specular
	materialSpecular[0] = 0.992157f;
	materialSpecular[0] = 0.941176f;
	materialSpecular[0] = 0.807843f;
	materialSpecular[0] = 1.0f;
	//shininess
	materialShininess[0] = 0.21794872f*128.0f;
	glMaterialfv(GL_FRONT, GL_AMBIENT, materialAmbient);
	glMaterialfv(GL_FRONT, GL_DIFFUSE, materialDiffuse);
	glMaterialfv(GL_FRONT, GL_SPECULAR, materialSpecular);
	glMaterialfv(GL_FRONT, GL_SHININESS, materialShininess);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(12.5f, 14.0f, 0.0f);
	gluSphere(spheres[6], 1.0f, 30, 30);

	//	2nd Sphere : Bronze
	//ambient material
	materialAmbient[0] = 0.2125f;
	materialAmbient[1] = 0.1275f;
	materialAmbient[2] = 0.054f;
	materialAmbient[3] = 1.0f;
	//material diffuse
	materialDiffuse[0] = 0.714f;
	materialDiffuse[1] = 0.4284f;
	materialDiffuse[2] = 0.18144f;
	materialDiffuse[3] = 1.0f;
	//material specular
	materialSpecular[0] = 0.393548f;
	materialSpecular[0] = 0.271906f;
	materialSpecular[0] = 0.166721f;
	materialSpecular[0] = 1.0f;
	//shininess
	materialShininess[0] = 0.2f*128.0f;
	glMaterialfv(GL_FRONT, GL_AMBIENT, materialAmbient);
	glMaterialfv(GL_FRONT, GL_DIFFUSE, materialDiffuse);
	glMaterialfv(GL_FRONT, GL_SPECULAR, materialSpecular);
	glMaterialfv(GL_FRONT, GL_SHININESS, materialShininess);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(12.5f, 11.5f, 0.0f);
	gluSphere(spheres[7], 1.0f, 30, 30);

	//	3rd Sphere : Chrome
	//ambient material
	materialAmbient[0] = 0.25f;
	materialAmbient[1] = 0.25f;
	materialAmbient[2] = 0.25f;
	materialAmbient[3] = 1.0f;
	//material diffuse
	materialDiffuse[0] = 0.4f;
	materialDiffuse[1] = 0.4f;
	materialDiffuse[2] = 0.4f;
	materialDiffuse[3] = 1.0f;
	//material specular
	materialSpecular[0] = 0.774597f;
	materialSpecular[0] = 0.774597f;
	materialSpecular[0] = 0.774597f;
	materialSpecular[0] = 1.0f;
	//shininess
	materialShininess[0] = 0.6f*128.0f;
	glMaterialfv(GL_FRONT, GL_AMBIENT, materialAmbient);
	glMaterialfv(GL_FRONT, GL_DIFFUSE, materialDiffuse);
	glMaterialfv(GL_FRONT, GL_SPECULAR, materialSpecular);
	glMaterialfv(GL_FRONT, GL_SHININESS, materialShininess);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(12.5f, 9.0f, 0.0f);
	gluSphere(spheres[8], 1.0f, 30, 30);

	//	4th Sphere : Copper
	//ambient material
	materialAmbient[0] = 0.19125f;
	materialAmbient[1] = 0.0735f;
	materialAmbient[2] = 0.0225f;
	materialAmbient[3] = 1.0f;
	//material diffuse
	materialDiffuse[0] = 0.7038f;
	materialDiffuse[1] = 0.27048f;
	materialDiffuse[2] = 0.0828f;
	materialDiffuse[3] = 1.0f;
	//material specular
	materialSpecular[0] = 0.256777f;
	materialSpecular[0] = 0.137622f;
	materialSpecular[0] = 0.086014f;
	materialSpecular[0] = 1.0f;
	//shininess
	materialShininess[0] = 0.1f*128.0f;
	glMaterialfv(GL_FRONT, GL_AMBIENT, materialAmbient);
	glMaterialfv(GL_FRONT, GL_DIFFUSE, materialDiffuse);
	glMaterialfv(GL_FRONT, GL_SPECULAR, materialSpecular);
	glMaterialfv(GL_FRONT, GL_SHININESS, materialShininess);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(12.5f, 6.5f, 0.0f);
	gluSphere(spheres[9], 1.0f, 30, 30);

	// 5th Sphere : Gold
	//ambient material
	materialAmbient[0] = 0.24725f;
	materialAmbient[1] = 0.1995f;
	materialAmbient[2] = 0.0745f;
	materialAmbient[3] = 1.0f;
	//material diffuse
	materialDiffuse[0] = 0.75164f;
	materialDiffuse[1] = 0.60648f;
	materialDiffuse[2] = 0.22648f;
	materialDiffuse[3] = 1.0f;
	//material specular
	materialSpecular[0] = 0.628281f;
	materialSpecular[0] = 0.555802f;
	materialSpecular[0] = 0.366065f;
	materialSpecular[0] = 1.0f;
	//shininess
	materialShininess[0] = 0.4f*128.0f;
	glMaterialfv(GL_FRONT, GL_AMBIENT, materialAmbient);
	glMaterialfv(GL_FRONT, GL_DIFFUSE, materialDiffuse);
	glMaterialfv(GL_FRONT, GL_SPECULAR, materialSpecular);
	glMaterialfv(GL_FRONT, GL_SHININESS, materialShininess);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(12.5f, 4.0f, 0.0f);
	gluSphere(spheres[10], 1.0f, 30, 30);

	//	6th Sphere : Silver
	//ambient material
	materialAmbient[0] = 0.19225f;
	materialAmbient[1] = 0.19225f;
	materialAmbient[2] = 0.19225f;
	materialAmbient[3] = 1.0f;
	//material diffuse
	materialDiffuse[0] = 0.50754f;
	materialDiffuse[1] = 0.50754f;
	materialDiffuse[2] = 0.50754f;
	materialDiffuse[3] = 1.0f;
	//material specular
	materialSpecular[0] = 0.508273f;
	materialSpecular[0] = 0.508273f;
	materialSpecular[0] = 0.508273f;
	materialSpecular[0] = 1.0f;
	//shininess
	materialShininess[0] = 0.4f*128.0f;
	glMaterialfv(GL_FRONT, GL_AMBIENT, materialAmbient);
	glMaterialfv(GL_FRONT, GL_DIFFUSE, materialDiffuse);
	glMaterialfv(GL_FRONT, GL_SPECULAR, materialSpecular);
	glMaterialfv(GL_FRONT, GL_SHININESS, materialShininess);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(12.5f, 1.5f, 0.0f);
	gluSphere(spheres[11], 1.0f, 30, 30);

	// ***** 3rd column : Plastic *****
	//	1st Sphere : Black Plastic
	materialAmbient[0] = 0.0f;
	materialAmbient[1] = 0.0f;
	materialAmbient[2] = 0.0f;
	materialAmbient[3] = 1.0f;
	//material diffuse
	materialDiffuse[0] = 0.01f;
	materialDiffuse[1] = 0.01f;
	materialDiffuse[2] = 0.01f;
	materialDiffuse[3] = 1.0f;
	//material specular
	materialSpecular[0] = 0.50f;
	materialSpecular[0] = 0.50f;
	materialSpecular[0] = 0.50f;
	materialSpecular[0] = 1.0f;
	//shininess
	materialShininess[0] = 0.25f*128.0f;
	glMaterialfv(GL_FRONT, GL_AMBIENT, materialAmbient);
	glMaterialfv(GL_FRONT, GL_DIFFUSE, materialDiffuse);
	glMaterialfv(GL_FRONT, GL_SPECULAR, materialSpecular);
	glMaterialfv(GL_FRONT, GL_SHININESS, materialShininess);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(15.5f, 14.0f, 0.0f);
	gluSphere(spheres[12], 1.0f, 30, 30);

	//	2nd Sphere : Cyan Plastic
	//ambient material
	materialAmbient[0] = 0.0f;
	materialAmbient[1] = 0.1f;
	materialAmbient[2] = 0.06f;
	materialAmbient[3] = 1.0f;
	//material diffuse
	materialDiffuse[0] = 0.0f;
	materialDiffuse[1] = 0.50980392f;
	materialDiffuse[2] = 0.50980392f;
	materialDiffuse[3] = 1.0f;
	//material specular
	materialSpecular[0] = 0.50196078f;
	materialSpecular[0] = 0.50196078f;
	materialSpecular[0] = 0.50196078f;
	materialSpecular[0] = 1.0f;
	//shininess
	materialShininess[0] = 0.25f*128.0f;
	glMaterialfv(GL_FRONT, GL_AMBIENT, materialAmbient);
	glMaterialfv(GL_FRONT, GL_DIFFUSE, materialDiffuse);
	glMaterialfv(GL_FRONT, GL_SPECULAR, materialSpecular);
	glMaterialfv(GL_FRONT, GL_SHININESS, materialShininess);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(15.5f, 11.5f, 0.0f);
	gluSphere(spheres[13], 1.0f, 30, 30);

	//	3rd Sphere : Green Plastic
	//ambient material
	materialAmbient[0] = 0.0f;
	materialAmbient[1] = 0.0f;
	materialAmbient[2] = 0.0f;
	materialAmbient[3] = 1.0f;
	//material diffuse
	materialDiffuse[0] = 0.1f;
	materialDiffuse[1] = 0.35f;
	materialDiffuse[2] = 0.1f;
	materialDiffuse[3] = 1.0f;
	//material specular
	materialSpecular[0] = 0.45f;
	materialSpecular[0] = 0.55f;
	materialSpecular[0] = 0.45f;
	materialSpecular[0] = 1.0f;
	//shininess
	materialShininess[0] = 0.25f*128.0f;
	glMaterialfv(GL_FRONT, GL_AMBIENT, materialAmbient);
	glMaterialfv(GL_FRONT, GL_DIFFUSE, materialDiffuse);
	glMaterialfv(GL_FRONT, GL_SPECULAR, materialSpecular);
	glMaterialfv(GL_FRONT, GL_SHININESS, materialShininess);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(15.5f, 9.0f, 0.0f);
	gluSphere(spheres[14], 1.0f, 30, 30);

	//	4th Sphere : Red Plastic
	//ambient material
	materialAmbient[0] = 0.0f;
	materialAmbient[1] = 0.0f;
	materialAmbient[2] = 0.0f;
	materialAmbient[3] = 1.0f;
	//material diffuse
	materialDiffuse[0] = 0.5f;
	materialDiffuse[1] = 0.0f;
	materialDiffuse[2] = 0.0f;
	materialDiffuse[3] = 1.0f;
	//material specular
	materialSpecular[0] = 0.7f;
	materialSpecular[0] = 0.6f;
	materialSpecular[0] = 0.6f;
	materialSpecular[0] = 1.0f;
	//shininess
	materialShininess[0] = 0.25f*128.0f;
	glMaterialfv(GL_FRONT, GL_AMBIENT, materialAmbient);
	glMaterialfv(GL_FRONT, GL_DIFFUSE, materialDiffuse);
	glMaterialfv(GL_FRONT, GL_SPECULAR, materialSpecular);
	glMaterialfv(GL_FRONT, GL_SHININESS, materialShininess);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(15.5f, 6.5f, 0.0f);
	gluSphere(spheres[15], 1.0f, 30, 30);

	// 5th Sphere : White Plastic
	//ambient material
	materialAmbient[0] = 0.0f;
	materialAmbient[1] = 0.0f;
	materialAmbient[2] = 0.0f;
	materialAmbient[3] = 1.0f;
	//material diffuse
	materialDiffuse[0] = 0.55f;
	materialDiffuse[1] = 0.55f;
	materialDiffuse[2] = 0.55f;
	materialDiffuse[3] = 1.0f;
	//material specular
	materialSpecular[0] = 0.70f;
	materialSpecular[0] = 0.70f;
	materialSpecular[0] = 0.70f;
	materialSpecular[0] = 1.0f;
	//shininess
	materialShininess[0] = 0.25f*128.0f;
	glMaterialfv(GL_FRONT, GL_AMBIENT, materialAmbient);
	glMaterialfv(GL_FRONT, GL_DIFFUSE, materialDiffuse);
	glMaterialfv(GL_FRONT, GL_SPECULAR, materialSpecular);
	glMaterialfv(GL_FRONT, GL_SHININESS, materialShininess);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(15.5f, 4.0f, 0.0f);
	gluSphere(spheres[16], 1.0f, 30, 30);

	//	6th Sphere : Yellow Plastic
	//ambient material
	materialAmbient[0] = 0.0f;
	materialAmbient[1] = 0.0f;
	materialAmbient[2] = 0.0f;
	materialAmbient[3] = 1.0f;
	//material diffuse
	materialDiffuse[0] = 0.5f;
	materialDiffuse[1] = 0.5f;
	materialDiffuse[2] = 0.0f;
	materialDiffuse[3] = 1.0f;
	//material specular
	materialSpecular[0] = 0.60f;
	materialSpecular[0] = 0.60f;
	materialSpecular[0] = 0.50f;
	materialSpecular[0] = 1.0f;
	//shininess
	materialShininess[0] = 0.25f*128.0f;
	glMaterialfv(GL_FRONT, GL_AMBIENT, materialAmbient);
	glMaterialfv(GL_FRONT, GL_DIFFUSE, materialDiffuse);
	glMaterialfv(GL_FRONT, GL_SPECULAR, materialSpecular);
	glMaterialfv(GL_FRONT, GL_SHININESS, materialShininess);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(15.5f, 1.5f, 0.0f);
	gluSphere(spheres[17], 1.0f, 30, 30);

	// ***** 4th column : Rubber *****
	//	1st Sphere : Black Rubber
	materialAmbient[0] = 0.02f;
	materialAmbient[1] = 0.02f;
	materialAmbient[2] = 0.02f;
	materialAmbient[3] = 1.0f;
	//material diffuse
	materialDiffuse[0] = 0.01f;
	materialDiffuse[1] = 0.01f;
	materialDiffuse[2] = 0.01f;
	materialDiffuse[3] = 1.0f;
	//material specular
	materialSpecular[0] = 0.4f;
	materialSpecular[0] = 0.4f;
	materialSpecular[0] = 0.4f;
	materialSpecular[0] = 1.0f;
	//shininess
	materialShininess[0] = 0.078125f*128.0f;
	glMaterialfv(GL_FRONT, GL_AMBIENT, materialAmbient);
	glMaterialfv(GL_FRONT, GL_DIFFUSE, materialDiffuse);
	glMaterialfv(GL_FRONT, GL_SPECULAR, materialSpecular);
	glMaterialfv(GL_FRONT, GL_SHININESS, materialShininess);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(18.5f, 14.0f, 0.0f);
	gluSphere(spheres[18], 1.0f, 30, 30);

	//	2nd Sphere : Cyan Rubber
	//ambient material
	materialAmbient[0] = 0.0f;
	materialAmbient[1] = 0.05f;
	materialAmbient[2] = 0.05f;
	materialAmbient[3] = 1.0f;
	//material diffuse
	materialDiffuse[0] = 0.4f;
	materialDiffuse[1] = 0.5f;
	materialDiffuse[2] = 0.5f;
	materialDiffuse[3] = 1.0f;
	//material specular
	materialSpecular[0] = 0.04f;
	materialSpecular[0] = 0.7f;
	materialSpecular[0] = 0.7f;
	materialSpecular[0] = 1.0f;
	//shininess
	materialShininess[0] = 0.078125f*128.0f;
	glMaterialfv(GL_FRONT, GL_AMBIENT, materialAmbient);
	glMaterialfv(GL_FRONT, GL_DIFFUSE, materialDiffuse);
	glMaterialfv(GL_FRONT, GL_SPECULAR, materialSpecular);
	glMaterialfv(GL_FRONT, GL_SHININESS, materialShininess);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(18.5f, 11.5f, 0.0f);
	gluSphere(spheres[19], 1.0f, 30, 30);

	//	3rd Sphere : Green Rubber
	//ambient material
	materialAmbient[0] = 0.0f;
	materialAmbient[1] = 0.05f;
	materialAmbient[2] = 0.0f;
	materialAmbient[3] = 1.0f;
	//material diffuse
	materialDiffuse[0] = 0.4f;
	materialDiffuse[1] = 0.5f;
	materialDiffuse[2] = 0.4f;
	materialDiffuse[3] = 1.0f;
	//material specular
	materialSpecular[0] = 0.04f;
	materialSpecular[0] = 0.7f;
	materialSpecular[0] = 0.04f;
	materialSpecular[0] = 1.0f;
	//shininess
	materialShininess[0] = 0.078125f*128.0f;
	glMaterialfv(GL_FRONT, GL_AMBIENT, materialAmbient);
	glMaterialfv(GL_FRONT, GL_DIFFUSE, materialDiffuse);
	glMaterialfv(GL_FRONT, GL_SPECULAR, materialSpecular);
	glMaterialfv(GL_FRONT, GL_SHININESS, materialShininess);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(18.5f, 9.0f, 0.0f);
	gluSphere(spheres[20], 1.0f, 30, 30);

	//	4th Sphere : Red Rubber
	//ambient material
	materialAmbient[0] = 0.05f;
	materialAmbient[1] = 0.0f;
	materialAmbient[2] = 0.0f;
	materialAmbient[3] = 1.0f;
	//material diffuse
	materialDiffuse[0] = 0.5f;
	materialDiffuse[1] = 0.4f;
	materialDiffuse[2] = 0.4f;
	materialDiffuse[3] = 1.0f;
	//material specular
	materialSpecular[0] = 0.7f;
	materialSpecular[0] = 0.04f;
	materialSpecular[0] = 0.04f;
	materialSpecular[0] = 1.0f;
	//shininess
	materialShininess[0] = 0.078125f*128.0f;
	glMaterialfv(GL_FRONT, GL_AMBIENT, materialAmbient);
	glMaterialfv(GL_FRONT, GL_DIFFUSE, materialDiffuse);
	glMaterialfv(GL_FRONT, GL_SPECULAR, materialSpecular);
	glMaterialfv(GL_FRONT, GL_SHININESS, materialShininess);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(18.5f, 6.5f, 0.0f);
	gluSphere(spheres[21], 1.0f, 30, 30);

	// 5th Sphere : White Rubber
	//ambient material
	materialAmbient[0] = 0.05f;
	materialAmbient[1] = 0.05f;
	materialAmbient[2] = 0.05f;
	materialAmbient[3] = 1.0f;
	//material diffuse
	materialDiffuse[0] = 0.5f;
	materialDiffuse[1] = 0.5f;
	materialDiffuse[2] = 0.5f;
	materialDiffuse[3] = 1.0f;
	//material specular
	materialSpecular[0] = 0.70f;
	materialSpecular[0] = 0.70f;
	materialSpecular[0] = 0.70f;
	materialSpecular[0] = 1.0f;
	//shininess
	materialShininess[0] = 0.078125f*128.0f;
	glMaterialfv(GL_FRONT, GL_AMBIENT, materialAmbient);
	glMaterialfv(GL_FRONT, GL_DIFFUSE, materialDiffuse);
	glMaterialfv(GL_FRONT, GL_SPECULAR, materialSpecular);
	glMaterialfv(GL_FRONT, GL_SHININESS, materialShininess);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(18.5f, 4.0f, 0.0f);
	gluSphere(spheres[22], 1.0f, 30, 30);

	//	6th Sphere : Yellow Rubber
	//ambient material
	materialAmbient[0] = 0.05f;
	materialAmbient[1] = 0.05f;
	materialAmbient[2] = 0.0f;
	materialAmbient[3] = 1.0f;
	//material diffuse
	materialDiffuse[0] = 0.5f;
	materialDiffuse[1] = 0.5f;
	materialDiffuse[2] = 0.4f;
	materialDiffuse[3] = 1.0f;
	//material specular
	materialSpecular[0] = 0.70f;
	materialSpecular[0] = 0.70f;
	materialSpecular[0] = 0.04f;
	materialSpecular[0] = 1.0f;
	//shininess
	materialShininess[0] = 0.078125f*128.0f;
	glMaterialfv(GL_FRONT, GL_AMBIENT, materialAmbient);
	glMaterialfv(GL_FRONT, GL_DIFFUSE, materialDiffuse);
	glMaterialfv(GL_FRONT, GL_SPECULAR, materialSpecular);
	glMaterialfv(GL_FRONT, GL_SHININESS, materialShininess);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(18.5f, 1.5f, 0.0f);
	gluSphere(spheres[23], 1.0f, 30, 30);
}
