#import <OpenGLES/ES3/gl.h>
#import <OpenGLES/ES3/glext.h>

#import "vmath.h"
#import "ps_sphere.h"
#import "GLESView.h"

enum
{
    AMC_ATTRIBUTE_POSITION = 0,
    AMC_ATTRIBUTE_COLOR,
    AMC_ATTRIBUTE_NORMAL,
    AMC_ATTRIBUTE_TEXTURE0
};

@implementation GLESView
{
    EAGLContext *eaglContext;
    Sphere *mySphere;
    
    GLuint defaultFramebuffer;
    GLuint colorRenderbuffer;
    GLuint depthRenderbuffer;
    
    id displaylink;
    NSInteger animationFrameInterval;
    BOOL isAnimating;
    
    GLuint vertexShaderObject_v;
    GLuint fragmentShaderObject_v;
    GLuint shaderProgramObject_v;
    
    GLuint vertexShaderObject_f;
    GLuint fragmentShaderObject_f;
    GLuint shaderProgramObject_f;

    GLuint vao_sphere;
    GLuint vbo_sphere_elements;
    GLuint vbo_sphere_position;
    GLuint vbo_sphere_normals;
    
    float sphere_vertices[1146];
    float sphere_normals[1146];
    float sphere_textures[764];
    unsigned short sphere_elements[2280];
    
    int iNumVertices;
    int iNumElements;
    
    GLfloat angle_sphere;
    
    GLuint mUniform_v, vUniform_v, pUniform_v;    // Model Matrix Uniform, view Matrix Uniform and Projection Matrix Uniform
    GLuint LdUniform_v, KdUniform_v;            // Diffuse property of Light and diffuse property of material
    GLuint LaUniform_v, KaUniform_v;            // Ambient property of light and material
    GLuint LsUniform_v, KsUniform_v;            // Specular property of light and material
    GLuint lightPositionUniform_v;            // light position
    GLuint keyPressedUniform_v;                // is L key Pressed, to enable lighting effect
    GLuint materialShininessUniform_v;
    
    GLuint mUniform_f, vUniform_f, pUniform_f;    // Model Matrix Uniform, view Matrix Uniform and Projection Matrix Uniform
    GLuint LdUniform_f, KdUniform_f;            // Diffuse property of Light and diffuse property of material
    GLuint LaUniform_f, KaUniform_f;            // Ambient property of light and material
    GLuint LsUniform_f, KsUniform_f;            // Specular property of light and material
    GLuint lightPositionUniform_f;            // light position
    GLuint keyPressedUniform_f;                // is L key Pressed, to enable lighting effect
    GLuint materialShininessUniform_f;
    
    GLfloat lightDiffuse[4];
    GLfloat lightAmbient[4];
    GLfloat lightSpecular[4];
    GLfloat lightPosition[4];
    
    float materialAmbient[4];
    float materialDiffuse[4];
    float materialSpecular[4];
    float materialShininess;
    
    BOOL startAnimation;
    BOOL enableLighting;
    BOOL toggleLightingType;
    
    vmath::mat4 perspectiveProjectionMatrix;
}

-(id) initWithFrame:(CGRect)frameRect
{
    //code
    self = [super initWithFrame:frameRect];
    
    if (self)
    {
        CAEAGLLayer *eaglLayer = (CAEAGLLayer *)super.layer;
        
        eaglLayer.opaque = YES;
        eaglLayer.drawableProperties = [NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithBool:FALSE],
                                        kEAGLDrawablePropertyRetainedBacking, kEAGLColorFormatRGBA8, kEAGLDrawablePropertyColorFormat, nil];
        
        eaglContext=[[EAGLContext alloc]initWithAPI:kEAGLRenderingAPIOpenGLES3];
        
        if(eaglContext==nil)
        {
            [self release];
            return(nil);
        }
        
        [EAGLContext setCurrentContext:eaglContext];
        
        glGenFramebuffers(1, &defaultFramebuffer);
        glGenRenderbuffers(1, &colorRenderbuffer);
        glBindFramebuffer(GL_FRAMEBUFFER, defaultFramebuffer);
        glBindRenderbuffer(GL_RENDERBUFFER, colorRenderbuffer);
        
        [eaglContext renderbufferStorage:GL_RENDERBUFFER fromDrawable:eaglLayer];
        
        glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_RENDERBUFFER, colorRenderbuffer);
        
        GLint backingWidth;
        GLint backingHeight;
        
        glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_WIDTH, &backingWidth);
        glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_WIDTH, &backingHeight);
        
        glGenRenderbuffers(1, &depthRenderbuffer);
        glBindRenderbuffer(GL_RENDERBUFFER, depthRenderbuffer);
        glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT16, backingWidth, backingHeight);
        glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, depthRenderbuffer);
        
        if(glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
        {
            printf("Failed to Create Complete ramebuffer Object %x \n", glCheckFramebufferStatus(GL_FRAMEBUFFER));
            glDeleteFramebuffers(1, &defaultFramebuffer);
            glDeleteRenderbuffers(1, &colorRenderbuffer);
            glDeleteRenderbuffers(1, &depthRenderbuffer);
            
            return(nil);
        }
        
        printf("Renderer : %s | GL Version : %s | GLSL Version : %s\n",
               glGetString(GL_RENDERER), glGetString(GL_VERSION), glGetString(GL_SHADING_LANGUAGE_VERSION));
        
        // hardcore initializations
        isAnimating = NO;
        animationFrameInterval = 60;    //default since iOS 8.2
        
        //PER VERTEX LIGHTING - vertex shader
        //create shader
        vertexShaderObject_v = glCreateShader(GL_VERTEX_SHADER);
        
        const GLchar *vertexShaderSourceCode_v =
        "   #version 300 es                                " \
        "                                           \n  " \
        "    in vec4 vPosition;                                                                  " \
        "    in vec3 vNormal;                                                                    " \
        "                                                                                      \n" \
        "    uniform mat4 u_m_matrix;                                                            " \
        "    uniform mat4 u_v_matrix;                                                            " \
        "    uniform mat4 u_p_matrix;                                                            " \
        "    uniform mediump int u_l_key_is_pressed;                                                        " \
        "    uniform vec3 u_ld;                                                                    " \
        "    uniform vec3 u_kd;                                                                    " \
        "    uniform vec3 u_la;                                                                    " \
        "    uniform vec3 u_ka;                                                                    " \
        "    uniform vec3 u_ls;                                                                    " \
        "    uniform vec3 u_ks;                                                                    " \
        "    uniform vec4 u_light_position;                                                        " \
        "    uniform float u_material_shininess;                                                    " \
        "                                                                                      \n" \
        "    out vec3 phong_ads_light;                                                            " \
        "                                                                                      \n" \
        "    void main(void)                                                                        " \
        "    {                                                                                    " \
        "        if(u_l_key_is_pressed == 1)                                                        " \
        "        {                                                                                " \
        "            vec4 eyeCoordinates = u_v_matrix * u_m_matrix * vPosition;                    " \
        "            vec3 tNorm = normalize(mat3(u_v_matrix * u_m_matrix) * vNormal);            " \
        "            vec3 lightDirection = normalize(vec3(u_light_position - eyeCoordinates));    " \
        "            float tnDotld = max(dot(lightDirection, tNorm), 0.0);                        " \
        "            vec3 reflectionVector = reflect(-lightDirection, tNorm);                    " \
        "            vec3 viewerVector = normalize(vec3(-eyeCoordinates));                        " \
        "            float rvDotvv = max(dot(reflectionVector, viewerVector), 0.0);                " \
        "                                                                                      \n" \
        "            vec3 ambient = u_la * u_ka;                                                    " \
        "            vec3 diffuse = u_ld * u_kd * tnDotld;                                        " \
        "            vec3 specular = u_ls * u_ks * pow(rvDotvv, u_material_shininess);            " \
        "                                                                                      \n" \
        "            phong_ads_light = ambient + diffuse + specular;                                " \
        "        }                                                                                " \
        "        else                                                                            " \
        "        {                                                                                " \
        "            phong_ads_light = vec3(1.0, 1.0, 1.0);                                        " \
        "        }                                                                                " \
        "                                                                                      \n" \
        "    gl_Position = u_v_matrix * u_p_matrix * u_m_matrix * vPosition;                        " \
        "    }                                                                                    ";
        
        glShaderSource(vertexShaderObject_v, 1, (const GLchar**)&vertexShaderSourceCode_v, NULL);
        
        // compile shader
        GLint iInfoLogLength =0;
        GLint iShaderCompileStatus = 0;
        char *szInfoLog = NULL;
        
        glCompileShader(vertexShaderObject_v);
        glGetShaderiv(vertexShaderObject_v, GL_COMPILE_STATUS, &iShaderCompileStatus);
        if (iShaderCompileStatus == GL_FALSE)
        {
            glGetShaderiv(vertexShaderObject_v, GL_INFO_LOG_LENGTH, &iInfoLogLength);
            if (iInfoLogLength > 0)
            {
                szInfoLog = (GLchar *)malloc(iInfoLogLength);
                if (szInfoLog != NULL)
                {
                    GLsizei written;
                    glGetShaderInfoLog(vertexShaderObject_v, iInfoLogLength, &written, szInfoLog);
                    printf("VERTEX SHADER COMPILATION LOG : %s\n", szInfoLog);
                    free(szInfoLog);
                    [self release];
                }
            }
        }
        
        //PER VERTEX LIGHTING - fragment shader
        fragmentShaderObject_v = glCreateShader(GL_FRAGMENT_SHADER);
        
        // source code for fragment shader
        const GLchar* fragmentShaderSourceCode_v =
        "     #version 300 es                                                                             " \
        "     \n                                                                                        " \
        "   precision highp float;  " \
        "    in vec3 phong_ads_light;                                                                " \
        "    out vec4 fragColor;                                                                        " \
        "                                                                                        \n    " \
        "    void main(void)                                                                            " \
        "    {                                                                                        " \
        "        fragColor = vec4(phong_ads_light, 1.0);                                                " \
        "    }                                                                                        ";
        
        glShaderSource(fragmentShaderObject_v, 1, (const GLchar**)&fragmentShaderSourceCode_v, NULL);
        
        // compile shader
        iInfoLogLength =0;
        iShaderCompileStatus = 0;
        szInfoLog = NULL;
        
        glCompileShader(fragmentShaderObject_v);
        glGetShaderiv(fragmentShaderObject_v, GL_COMPILE_STATUS, &iShaderCompileStatus);
        if (iShaderCompileStatus == GL_FALSE)
        {
            glGetShaderiv(fragmentShaderObject_v, GL_INFO_LOG_LENGTH, &iInfoLogLength);
            if (iInfoLogLength > 0)
            {
                szInfoLog = (GLchar *)malloc(iInfoLogLength);
                if (szInfoLog != NULL)
                {
                    GLsizei written;
                    glGetShaderInfoLog(fragmentShaderObject_v, iInfoLogLength, &written, szInfoLog);
                    printf("FRAGMENT SHADER COMPILATION LOG : %s\n", szInfoLog);
                    free(szInfoLog);
                    [self release];
                }
            }
        }
        //PER VERTEX LIGHTING - shader program
        //create program object
        shaderProgramObject_v = glCreateProgram();
        
        //attach shaders to it
        glAttachShader(shaderProgramObject_v, vertexShaderObject_v);
        glAttachShader(shaderProgramObject_v, fragmentShaderObject_v);
        
        // pre-linking binding of shader program object with vertex shader position attribute
        //(not required when used layout qualifier with location in shader, writing here for legacy of assignments)
        glBindAttribLocation(shaderProgramObject_v, AMC_ATTRIBUTE_POSITION, "vPosition");
        glBindAttribLocation(shaderProgramObject_v, AMC_ATTRIBUTE_NORMAL, "vNormal");
        
        // link shader program object
        glLinkProgram(shaderProgramObject_v);
        
        //error checking
        GLint iProgramLinkStatus = 0;
        iInfoLogLength = 0;
        szInfoLog = NULL;
        
        glGetProgramiv(shaderProgramObject_v, GL_LINK_STATUS, &iProgramLinkStatus);
        if (iProgramLinkStatus == GL_FALSE)
        {
            glGetProgramiv(shaderProgramObject_v, GL_INFO_LOG_LENGTH, &iInfoLogLength);
            if (iInfoLogLength > 0)
            {
                szInfoLog = (GLchar *)malloc(iInfoLogLength);
                if (szInfoLog != NULL)
                {
                    GLint written;
                    glGetProgramInfoLog(shaderProgramObject_v, iInfoLogLength, &written, szInfoLog);
                    printf("SHADER PROGRAM LINK LOG : %s\n", szInfoLog);
                    free(szInfoLog);
                    [self release];
                }
            }
        }
        
        // get MVP uniform location
        //postlinking retieving uniform location
        vUniform_v = glGetUniformLocation(shaderProgramObject_v, "u_v_matrix");
        mUniform_v = glGetUniformLocation(shaderProgramObject_v, "u_m_matrix");
        pUniform_v = glGetUniformLocation(shaderProgramObject_v, "u_p_matrix");
        LdUniform_v = glGetUniformLocation(shaderProgramObject_v, "u_ld");
        KdUniform_v = glGetUniformLocation(shaderProgramObject_v, "u_kd");
        LaUniform_v = glGetUniformLocation(shaderProgramObject_v, "u_la");
        KaUniform_v = glGetUniformLocation(shaderProgramObject_v, "u_ka");
        LsUniform_v = glGetUniformLocation(shaderProgramObject_v, "u_ls");
        KsUniform_v = glGetUniformLocation(shaderProgramObject_v, "u_ks");
        materialShininessUniform_v = glGetUniformLocation(shaderProgramObject_v, "u_material_shininess");
        lightPositionUniform_v = glGetUniformLocation(shaderProgramObject_v, "u_light_position");
        keyPressedUniform_v = glGetUniformLocation(shaderProgramObject_v, "u_l_key_is_pressed");
        
        // *********************** VERTEX SHADER ***************************
        vertexShaderObject_f = glCreateShader(GL_VERTEX_SHADER);
        
        const GLchar* vertexShaderSourceCode_f =
        "   #version 300 es                                " \
        "                                           \n  " \
        "    in vec4 vPosition;                                                                  " \
        "    in vec3 vNormal;                                                                    " \
        "                                                                                      \n" \
        "    out vec3 tNorm;                                                                        " \
        "    out vec3 lightDirection;                                                            " \
        "    out vec3 viewerVector;                                                                " \
        "                                                                                      \n" \
        "    uniform mat4 u_m_matrix;                                                            " \
        "    uniform mat4 u_v_matrix;                                                            " \
        "    uniform mat4 u_p_matrix;                                                            " \
        "    uniform mediump int u_l_key_is_pressed;                                                        " \
        "    uniform vec4 u_light_position;                                                        " \
        "                                                                                      \n" \
        "    void main(void)                                                                        " \
        "    {                                                                                    " \
        "        if(u_l_key_is_pressed == 1)                                                        " \
        "        {                                                                                " \
        "            vec4 eyeCoordinates = u_v_matrix * u_m_matrix * vPosition;                          " \
        "            tNorm = mat3(u_v_matrix * u_m_matrix) * vNormal;                               " \
        "            lightDirection = vec3(u_light_position - eyeCoordinates);                      " \
        "            viewerVector = vec3(-eyeCoordinates);                                     " \
        "        }                                                                                " \
        "                                                                                      \n" \
        "    gl_Position = u_v_matrix * u_p_matrix * u_m_matrix * vPosition;                        " \
        "    }                                                                                    ";
        
        glShaderSource(vertexShaderObject_f, 1, (const GLchar**) &vertexShaderSourceCode_f, NULL);
        
        //compile shader
        glCompileShader(vertexShaderObject_f);
        
        iInfoLogLength = 0;
        iShaderCompileStatus = 0;
        szInfoLog = NULL;
        
        glGetShaderiv(vertexShaderObject_f, GL_COMPILE_STATUS, &iShaderCompileStatus);
        
        if(iShaderCompileStatus == GL_FALSE)
        {
            glGetShaderiv(vertexShaderObject_f, GL_INFO_LOG_LENGTH, &iInfoLogLength);
            if(iInfoLogLength > 0)
            {
                szInfoLog = (char *)malloc(iInfoLogLength);
                
                if(szInfoLog != NULL)
                {
                    GLsizei written;
                    glGetShaderInfoLog(vertexShaderObject_f, iInfoLogLength, &written, szInfoLog);
                    printf("Vertex Shader Compilation Log : %s \n", szInfoLog);
                    free(szInfoLog);
                    [self release];
                }
            }
        }
        
        //****************************** FRAGMENT SHADER *********************************
        fragmentShaderObject_f = glCreateShader(GL_FRAGMENT_SHADER);
        
        const GLchar* fragmentShaderSourceCode_f =
        "#version 300 es                " \
        "                           \n  " \
        "   precision highp float;     \n  " \
        "    in vec3 tNorm;                                                                            " \
        "    in vec3 lightDirection;                                                                    " \
        "    in vec3 viewerVector;                                                                    " \
        "                                                                                          \n" \
        "    out vec4 fragColor;                                                                        " \
        "                                                                                          \n" \
        "    uniform vec3 u_ld;                                                                        " \
        "    uniform vec3 u_kd;                                                                        " \
        "    uniform vec3 u_la;                                                                        " \
        "    uniform vec3 u_ka;                                                                        " \
        "    uniform vec3 u_ls;                                                                        " \
        "    uniform vec3 u_ks;                                                                        " \
        "    uniform float u_material_shininess;                                                        " \
        "    uniform int u_l_key_is_pressed;                                                            " \
        "                                                                                          \n" \
        "    void main(void)                                                                            " \
        "    {                                                                                        " \
        "        vec3 phong_ads_light;                                                                " \
        "        if(u_l_key_is_pressed == 1)                                                            " \
        "        {                                                                                    " \
        "            vec3 normalizedtNorm = normalize(tNorm);                                        " \
        "            vec3 normalizedlightDirection = normalize(lightDirection);                        " \
        "            vec3 normalizedviewerVector = normalize(viewerVector);                            " \
        "                                                                                          \n" \
        "            vec3 reflectionVector = normalize(reflect(-normalizedlightDirection, normalizedtNorm));" \
        "            float tnDotld = max(dot(normalizedlightDirection, normalizedtNorm), 0.0);        " \
        "            float rvDotvv = max(dot(reflectionVector, normalizedviewerVector), 0.0);        " \
        "                                                                                          \n" \
        "            vec3 ambient = u_la * u_ka;                                                        " \
        "            vec3 diffuse = u_ld * u_kd * tnDotld;                                            " \
        "            vec3 specular = u_ls * u_ks * pow(rvDotvv, u_material_shininess);                " \
        "            phong_ads_light = ambient + diffuse + specular;                                    " \
        "                                                                                          \n" \
        "            fragColor = vec4(phong_ads_light, 1.0);                                            " \
        "        }                                                                                    " \
        "        else                                                                                " \
        "        {                                                                                    " \
        "            phong_ads_light = vec3(1.0, 1.0, 1.0);                                            " \
        "            fragColor = vec4(phong_ads_light, 1.0);                                            " \
        "        }                                                                                    " \
        "    }                                                                                        ";
        
        glShaderSource(fragmentShaderObject_f, 1, (const GLchar**)&fragmentShaderSourceCode_f, NULL);
        
        //compile shader
        glCompileShader(fragmentShaderObject_f);
        
        iInfoLogLength = 0;
        iShaderCompileStatus = 0;
        szInfoLog = NULL;
        
        glGetShaderiv(fragmentShaderObject_f, GL_COMPILE_STATUS, &iShaderCompileStatus);
        
        if(iShaderCompileStatus == GL_FALSE)
        {
            glGetShaderiv(fragmentShaderObject_f, GL_INFO_LOG_LENGTH, &iInfoLogLength);
            if(iInfoLogLength > 0)
            {
                szInfoLog = (char *)malloc(iInfoLogLength);
                
                if(szInfoLog != NULL)
                {
                    GLsizei written;
                    glGetShaderInfoLog(fragmentShaderObject_f, iInfoLogLength, &written, szInfoLog);
                    printf("Fragment Shader Compilation Log : %s \n", szInfoLog);
                    free(szInfoLog);
                    [self release];
                }
            }
        }
        
        //************************************ SHADER PROGRAM ***********************************
        //create
        shaderProgramObject_f = glCreateProgram();
        
        // attach shaders
        glAttachShader(shaderProgramObject_f, vertexShaderObject_f);
        glAttachShader(shaderProgramObject_f, fragmentShaderObject_f);
        
        glBindAttribLocation(shaderProgramObject_f, AMC_ATTRIBUTE_POSITION, "vPosition");
        glBindAttribLocation(shaderProgramObject_f, AMC_ATTRIBUTE_NORMAL, "vNormal");
        //link
        glLinkProgram(shaderProgramObject_f);
        
        GLint iShaderProgramLinkStatus = 0;
        
        glGetProgramiv(shaderProgramObject_f, GL_LINK_STATUS, &iShaderProgramLinkStatus);
        
        if(iShaderProgramLinkStatus == GL_FALSE)
        {
            glGetProgramiv(shaderProgramObject_f, GL_INFO_LOG_LENGTH, &iInfoLogLength);
            
            if(iInfoLogLength > 0)
            {
                szInfoLog = (char *)malloc(iInfoLogLength);
                if(szInfoLog != NULL)
                {
                    GLsizei written;
                    glGetProgramInfoLog(shaderProgramObject_f, iInfoLogLength, &written, szInfoLog);
                    printf("Shader Program Link Log : %s\n", szInfoLog);
                    free(szInfoLog);
                    [self release];
                }
            }
        }
        
        //postlinking retieving uniform location
        vUniform_f = glGetUniformLocation(shaderProgramObject_f, "u_v_matrix");
        mUniform_f = glGetUniformLocation(shaderProgramObject_f, "u_m_matrix");
        pUniform_f = glGetUniformLocation(shaderProgramObject_f, "u_p_matrix");
        LdUniform_f = glGetUniformLocation(shaderProgramObject_f, "u_ld");
        KdUniform_f = glGetUniformLocation(shaderProgramObject_f, "u_kd");
        LaUniform_f = glGetUniformLocation(shaderProgramObject_f, "u_la");
        KaUniform_f = glGetUniformLocation(shaderProgramObject_f, "u_ka");
        LsUniform_f = glGetUniformLocation(shaderProgramObject_f, "u_ls");
        KsUniform_f = glGetUniformLocation(shaderProgramObject_f, "u_ks");
        materialShininessUniform_f = glGetUniformLocation(shaderProgramObject_f, "u_material_shininess");
        lightPositionUniform_f = glGetUniformLocation(shaderProgramObject_f, "u_light_position");
        keyPressedUniform_f = glGetUniformLocation(shaderProgramObject_f, "u_l_key_is_pressed");
        
        // sphere data
        mySphere = [[Sphere alloc]init];
        
        [mySphere getSphereVertexDataWithPosition:sphere_vertices withNormals:sphere_normals withTexCoords:sphere_textures andElements:sphere_elements];
        
        iNumVertices = [mySphere getNumberOfSphereVertices];
        iNumElements = [mySphere getNumberOfSphereElements];
        
        glGenVertexArrays(1,&vao_sphere);
        glBindVertexArray(vao_sphere);
        
        // position vbo
        glGenBuffers(1, &vbo_sphere_position);
        glBindBuffer(GL_ARRAY_BUFFER, vbo_sphere_position);
        glBufferData(GL_ARRAY_BUFFER, sizeof(sphere_vertices), sphere_vertices, GL_STATIC_DRAW);
        glVertexAttribPointer(AMC_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
        glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);
        glBindBuffer(GL_ARRAY_BUFFER, 0);
        
        // normal vbo
        glGenBuffers(1, &vbo_sphere_normals);
        glBindBuffer(GL_ARRAY_BUFFER, vbo_sphere_normals);
        glBufferData(GL_ARRAY_BUFFER, sizeof(sphere_normals), sphere_normals, GL_STATIC_DRAW);
        glVertexAttribPointer(AMC_ATTRIBUTE_NORMAL, 3, GL_FLOAT, GL_FALSE, 0, NULL);
        glEnableVertexAttribArray(AMC_ATTRIBUTE_NORMAL);
        glBindBuffer(GL_ARRAY_BUFFER, 0);
        
        // elements vbo
        glGenBuffers(1, &vbo_sphere_elements);
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_elements);
        glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(sphere_elements), sphere_elements, GL_STATIC_DRAW);
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
        
        glBindVertexArray(0);
        
        // light and material properties
        //light ambient
        lightAmbient[0] = 0.0f;
        lightAmbient[1] = 0.0f;
        lightAmbient[2] = 0.0f;
        lightAmbient[3] = 0.0f;
        
        // light diffuse
        lightDiffuse[0] = 1.0f;
        lightDiffuse[1] = 1.0f;
        lightDiffuse[2] = 1.0f;
        lightDiffuse[3] = 1.0f;
        
        //light specular
        lightSpecular[0] = 1.0f;
        lightSpecular[1] = 1.0f;
        lightSpecular[2] = 1.0f;
        lightSpecular[3] = 1.0f;
        
        // light position
        lightPosition[0] = 100.0f;
        lightPosition[1] = 100.0f;
        lightPosition[2] = 100.0f;
        lightPosition[3] = 1.0f;
        
        // material ambient
        materialAmbient[0] = 0.0f;
        materialAmbient[1] = 0.0f;
        materialAmbient[2] = 0.0f;
        materialAmbient[3] = 0.0f;
        
        // material diffuse
        materialDiffuse[0] = 1.0f;
        materialDiffuse[1] = 1.0f;
        materialDiffuse[2] = 1.0f;
        materialDiffuse[3] = 1.0f;
        
        // material specular
        materialSpecular[0] = 1.0f;
        materialSpecular[1] = 1.0f;
        materialSpecular[2] = 1.0f;
        materialSpecular[3] = 1.0f;
        
        // material shininess
        materialShininess = 128.0f;
        
        startAnimation = NO;
        enableLighting = NO;
        
        glEnable(GL_DEPTH_TEST);
        glDepthFunc(GL_LEQUAL);
        
        // clear color
        glClearColor(0.0f, 0.0f, 0.0f, 1.0f);       //blue
        
        perspectiveProjectionMatrix = vmath::mat4::identity();
        
        //Gesture Recognition Code
        //Tap gesture code
        UITapGestureRecognizer *singleTapGestureRecognizer = [ [ UITapGestureRecognizer alloc ] initWithTarget:self action:@selector(onSingleTap:)] ;
        [singleTapGestureRecognizer setNumberOfTapsRequired:1]; // 1 touch for 1 fingure
        [singleTapGestureRecognizer setDelegate:self];
        [self addGestureRecognizer:singleTapGestureRecognizer];
        
        // Double Tap Gesture Code
        UITapGestureRecognizer *doubleTapGestureRecognizer = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(onDoubleTap:)];
        [doubleTapGestureRecognizer setNumberOfTapsRequired:2];
        [doubleTapGestureRecognizer setNumberOfTouchesRequired:1]; // 1 touch of fingure
        [doubleTapGestureRecognizer setDelegate:self];
        [self addGestureRecognizer:doubleTapGestureRecognizer];
        
        [singleTapGestureRecognizer requireGestureRecognizerToFail:doubleTapGestureRecognizer];
        
        //swipe gesture
        UISwipeGestureRecognizer *swipGestureRecognizer = [ [UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(onSwipe:) ] ;
        [self addGestureRecognizer:swipGestureRecognizer];
        
        // long Pres Gesture
        UILongPressGestureRecognizer *longPressGestureRecognizer = [ [UILongPressGestureRecognizer alloc]initWithTarget:self action:@selector(onLongPress:) ];
        
        [self addGestureRecognizer:longPressGestureRecognizer];
        
    }
    return(self);
}

/*
 // Only override drawRect: if you perform custom drawing.
 // An empty implementation adversely affects performance during animation.
 - (void)drawRect:(CGRect)rect
 {
 // Drawing code
 }
 */

//static method
+(Class)layerClass
{
    //code
    return([CAEAGLLayer class]);
}

-(void)drawView:(id)sender
{
    //code
    [EAGLContext setCurrentContext:eaglContext];
    
    glBindFramebuffer(GL_FRAMEBUFFER, defaultFramebuffer);
    
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);
    
    vmath::mat4 modelMatrix = vmath::mat4::identity();
    vmath::mat4 viewMatrix = vmath::mat4::identity();
    vmath::mat4 projectionMatrix = vmath::mat4::identity();
    vmath::mat4 translationMatrix = vmath::mat4::identity();
    vmath::mat4 rotationMatrix = vmath::mat4::identity();
    
    if(toggleLightingType == NO)
    {
        glUseProgram(shaderProgramObject_v);
        
        // Sphere
        translationMatrix =  vmath::translate(0.0f, 0.0f, -3.0f);
        rotationMatrix =  vmath::rotate(angle_sphere, angle_sphere, angle_sphere);
        modelMatrix = translationMatrix * rotationMatrix;
        projectionMatrix = perspectiveProjectionMatrix;
        
        if (enableLighting == YES)
        {
            glUniform1i(keyPressedUniform_v, 1);
            glUniform3fv(LaUniform_v, 1, lightAmbient);
            glUniform3fv(LdUniform_v, 1, lightDiffuse);
            glUniform3fv(LsUniform_v, 1, lightSpecular);
            
            glUniform3fv(KaUniform_v, 1, materialAmbient);
            glUniform3fv(KdUniform_v, 1, materialDiffuse);
            glUniform3fv(KsUniform_v, 1, materialSpecular);
            
            glUniform1f(materialShininessUniform_v, materialShininess);
            
            glUniform4fv(lightPositionUniform_v, 1, lightPosition);
        }
        else
        {
            glUniform1i(keyPressedUniform_v, 0);
        }
        //send necessary matrices to shader in respective uniforms
        glUniformMatrix4fv(mUniform_v, 1, GL_FALSE, modelMatrix);
        glUniformMatrix4fv(vUniform_v, 1, GL_FALSE, viewMatrix);
        glUniformMatrix4fv(pUniform_v, 1, GL_FALSE, projectionMatrix);
        
        // draw sphere
        glBindVertexArray(vao_sphere);
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_elements);
        glDrawElements(GL_TRIANGLES, iNumElements, GL_UNSIGNED_SHORT, 0);
        glBindVertexArray(0);
        
        glUseProgram(0);
    }
    else
    {
        glUseProgram(shaderProgramObject_f);
        
        // Sphere
        translationMatrix =  vmath::translate(0.0f, 0.0f, -3.0f);
        rotationMatrix =  vmath::rotate(angle_sphere, angle_sphere, angle_sphere);
        modelMatrix = translationMatrix * rotationMatrix;
        projectionMatrix = perspectiveProjectionMatrix;
        
        if (enableLighting == YES)
        {
            glUniform1i(keyPressedUniform_f, 1);
            glUniform3fv(LaUniform_f, 1, lightAmbient);
            glUniform3fv(LdUniform_f, 1, lightDiffuse);
            glUniform3fv(LsUniform_f, 1, lightSpecular);
            
            glUniform3fv(KaUniform_f, 1, materialAmbient);
            glUniform3fv(KdUniform_f, 1, materialDiffuse);
            glUniform3fv(KsUniform_f, 1, materialSpecular);
            
            glUniform1f(materialShininessUniform_f, materialShininess);
            
            glUniform4fv(lightPositionUniform_f, 1, lightPosition);
        }
        else
        {
            glUniform1i(keyPressedUniform_f, 0);
        }
        //send necessary matrices to shader in respective uniforms
        glUniformMatrix4fv(mUniform_f, 1, GL_FALSE, modelMatrix);
        glUniformMatrix4fv(vUniform_f, 1, GL_FALSE, viewMatrix);
        glUniformMatrix4fv(pUniform_f, 1, GL_FALSE, projectionMatrix);
        
        // draw sphere
        glBindVertexArray(vao_sphere);
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_elements);
        glDrawElements(GL_TRIANGLES, iNumElements, GL_UNSIGNED_SHORT, 0);
        glBindVertexArray(0);
        
        glUseProgram(0);
    }
    
    glBindRenderbuffer(GL_RENDERBUFFER, colorRenderbuffer);
    [eaglContext presentRenderbuffer:GL_RENDERBUFFER];          // swapbuffers
    
    if (startAnimation == YES)
    {
        if (angle_sphere < 360.0f)
            angle_sphere = angle_sphere + 0.5f;
        else
            angle_sphere = 0.0f;
    }
    else
    {
        angle_sphere = 0.0f;
    }
}

-(void)layoutSubviews   //reshape
{
    //code
    GLint width;
    GLint height;
    
    glBindRenderbuffer(GL_RENDERBUFFER, colorRenderbuffer);
    [eaglContext renderbufferStorage:GL_RENDERBUFFER fromDrawable:(CAEAGLLayer*)self.layer];
    glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_WIDTH, &width);
    glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_HEIGHT, &height);
    glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, depthRenderbuffer);
    
    glViewport(0, 0, width, height);
    
    //ortho(l-r-b-t-n-f)
    GLfloat fwidth = (GLfloat)width;
    GLfloat fheight = (GLfloat)height;
    
    perspectiveProjectionMatrix = vmath::perspective(45.0f, fwidth/fheight, 0.1f, 100.0f);
    
    if(glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
    {
        printf("Failed to Create Complete ramebuffer Object %x \n", glCheckFramebufferStatus(GL_FRAMEBUFFER));
    }
    [self drawView:nil];//repaint like warmup style
}

-(void)startAnimation
{
    if(!isAnimating)
    {
        displaylink = [NSClassFromString(@"CADisplayLink") displayLinkWithTarget:self selector:@selector(drawView:)];
        [displaylink setPreferredFramesPerSecond:animationFrameInterval];
        [displaylink addToRunLoop:[NSRunLoop currentRunLoop] forMode:NSDefaultRunLoopMode];
        
        isAnimating = YES;
    }
}

-(void)stopAnimation
{
    if(isAnimating)
    {
        [displaylink invalidate];
        displaylink=nil;
        
        isAnimating = NO;
    }
}

-(BOOL) acceptsFirstResponder
{
    return YES;
}

-(void)touchesBegane :(NSSet *)touches withEvent:(UIEvent *)UIEvent
{
    
}


-(void) onLongPress: (UIGestureRecognizer *)gesture
{
    
}

-(void) onSwipe: (UIGestureRecognizer *)gesture
{
    [self release];
    
    exit(0);
}

-(void) onDoubleTap: (UITapGestureRecognizer *)gesture
{
    if(toggleLightingType == NO)
        toggleLightingType = YES;
    else
        toggleLightingType = NO;
}

-(void) onSingleTap: (UITapGestureRecognizer *)gesture
{
    if(enableLighting == NO)
    {
        startAnimation = YES;
        enableLighting = YES;
    }
    else
    {
        startAnimation = NO;
        enableLighting = NO;
    }
}

- (void)dealloc
{
    if(vao_sphere)
    {
        glDeleteVertexArrays(1, &vao_sphere);
        vao_sphere = 0;
    }
    
    if (vbo_sphere_position)
    {
        glDeleteBuffers(1, &vbo_sphere_position);
        vbo_sphere_position = 0;
    }
    
    if (vbo_sphere_normals)
    {
        glDeleteBuffers(1, &vbo_sphere_normals);
        vbo_sphere_normals = 0;
    }
    
    if (vbo_sphere_elements)
    {
        glDeleteBuffers(1, &vbo_sphere_elements);
        vbo_sphere_elements = 0;
    }
    
    // per vertex
    glDetachShader(shaderProgramObject_v, vertexShaderObject_v);
    glDetachShader(shaderProgramObject_v, fragmentShaderObject_v);
    
    glDeleteShader(vertexShaderObject_v);
    vertexShaderObject_v = 0;
    
    glDeleteShader(fragmentShaderObject_v);
    fragmentShaderObject_v = 0;
    
    glDeleteProgram(shaderProgramObject_v);
    shaderProgramObject_v = 0;
    
    //per fragment
    glDetachShader(shaderProgramObject_f, vertexShaderObject_f);
    glDetachShader(shaderProgramObject_f, fragmentShaderObject_f);
    
    glDeleteShader(vertexShaderObject_f);
    vertexShaderObject_f = 0;
    
    glDeleteShader(fragmentShaderObject_f);
    fragmentShaderObject_f = 0;
    
    glDeleteProgram(shaderProgramObject_f);
    shaderProgramObject_f = 0;
    
    if(depthRenderbuffer)
    {
        glDeleteRenderbuffers(1, &depthRenderbuffer);
        depthRenderbuffer=0;
    }
    
    if(colorRenderbuffer)
    {
        glDeleteRenderbuffers(1, &colorRenderbuffer);
        colorRenderbuffer=0;
    }
    
    if(defaultFramebuffer)
    {
        glDeleteFramebuffers(1, &defaultFramebuffer);
        defaultFramebuffer=0;
    }
    
    if([EAGLContext currentContext] == eaglContext)
    {
        [EAGLContext setCurrentContext:nil];
    }
    [eaglContext release];
    eaglContext =nil;
    
    [super dealloc];
}

@end
