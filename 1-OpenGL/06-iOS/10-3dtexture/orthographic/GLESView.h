//
//  GLESView.h
//  orthographic
//
//  Created by pratik saboo on 23/02/20.
//

#import <UIKit/UIKit.h>

@interface GLESView : UIView<UIGestureRecognizerDelegate>
-(void)startAnimation;
-(void)stopAnimation;
@end

