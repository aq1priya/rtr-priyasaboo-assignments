#import <OpenGLES/ES3/gl.h>
#import <OpenGLES/ES3/glext.h>

#import "vmath.h"

#import "GLESView.h"

enum
{
    AMC_ATTRIBUTE_POSITION = 0,
    AMC_ATTRIBUTE_COLOR,
    AMC_ATTRIBUTE_NORMAL,
    AMC_ATTRIBUTE_TEXTURE0
};

@implementation GLESView
{
    EAGLContext *eaglContext;
    
    GLuint defaultFramebuffer;
    GLuint colorRenderbuffer;
    GLuint depthRenderbuffer;
    
    id displaylink;
    NSInteger animationFrameInterval;
    BOOL isAnimating;
    
    GLuint vertexShaderObject;
    GLuint fragmentShaderObject;
    GLuint shaderProgramObject;
    
    GLuint vao_cube;
    GLuint vbo_data_cube;
    
    GLfloat angle_cube;
    
    GLuint textureMarble;
    
    GLuint samplerUniform;
    GLuint mUniform, vUniform, pUniform;                // ModelView Matrix Uniform and Projection Matrix Uniform
    GLuint LdUniform, KdUniform;            // Diffuse property of Light and diffuse property of material
    GLuint LaUniform, KaUniform;            // Ambient property of light and material
    GLuint LsUniform, KsUniform;            // Specular property of light and material
    GLuint lightPositionUniform;            // light position
    GLuint materialShininessUniform;
    GLuint keyPressedUniform;                // is L key Pressed, to enable lighting effect
    
    float lightAmbient[4];
    float lightDiffuse[4];
    float lightSpecular[4];
    float lightPosition[4];
    
    float materialAmbient[4];
    float materialDiffuse[4];
    float materialSpecular[4];
    float materialShininess;
    
    BOOL startAnimation;
    BOOL enableLighting;
    
    vmath::mat4 perspectiveProjectionMatrix;
}

-(id) initWithFrame:(CGRect)frameRect
{
    //code
    self = [super initWithFrame:frameRect];
    
    if (self)
    {
        CAEAGLLayer *eaglLayer = (CAEAGLLayer *)super.layer;
        
        eaglLayer.opaque = YES;
        eaglLayer.drawableProperties = [NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithBool:FALSE],
                                        kEAGLDrawablePropertyRetainedBacking, kEAGLColorFormatRGBA8, kEAGLDrawablePropertyColorFormat, nil];
        
        eaglContext=[[EAGLContext alloc]initWithAPI:kEAGLRenderingAPIOpenGLES3];
        
        if(eaglContext==nil)
        {
            [self release];
            return(nil);
        }
        
        [EAGLContext setCurrentContext:eaglContext];
        
        glGenFramebuffers(1, &defaultFramebuffer);
        glGenRenderbuffers(1, &colorRenderbuffer);
        glBindFramebuffer(GL_FRAMEBUFFER, defaultFramebuffer);
        glBindRenderbuffer(GL_RENDERBUFFER, colorRenderbuffer);
        
        [eaglContext renderbufferStorage:GL_RENDERBUFFER fromDrawable:eaglLayer];
        
        glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_RENDERBUFFER, colorRenderbuffer);
        
        GLint backingWidth;
        GLint backingHeight;
        
        glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_WIDTH, &backingWidth);
        glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_WIDTH, &backingHeight);
        
        glGenRenderbuffers(1, &depthRenderbuffer);
        glBindRenderbuffer(GL_RENDERBUFFER, depthRenderbuffer);
        glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT16, backingWidth, backingHeight);
        glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, depthRenderbuffer);
        
        if(glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
        {
            printf("Failed to Create Complete ramebuffer Object %x \n", glCheckFramebufferStatus(GL_FRAMEBUFFER));
            glDeleteFramebuffers(1, &defaultFramebuffer);
            glDeleteRenderbuffers(1, &colorRenderbuffer);
            glDeleteRenderbuffers(1, &depthRenderbuffer);
            
            return(nil);
        }
        
        printf("Renderer : %s | GL Version : %s | GLSL Version : %s\n",
               glGetString(GL_RENDERER), glGetString(GL_VERSION), glGetString(GL_SHADING_LANGUAGE_VERSION));
        
        // hardcore initializations
        isAnimating = NO;
        animationFrameInterval = 60;    //default since iOS 8.2
        
        // *********************** VERTEX SHADER ***************************
        vertexShaderObject = glCreateShader(GL_VERTEX_SHADER);
        
        const GLchar* vertexShadreSourceCode =
        "   #version 300 es                                " \
        "                                           \n  " \
        "    in vec4 vPosition;                                                                    " \
        "    in vec3 vNormal;                                                                    " \
        "   in vec4 vColor;                                                                     " \
        "   in vec2 vTexCoord;                                                                  " \
        "                                                                                      \n" \
        "    uniform mat4 u_m_matrix;                                                            " \
        "   uniform mat4 u_v_matrix;                                                            " \
        "    uniform mat4 u_p_matrix;                                                            " \
        "    uniform mediump int u_l_key_is_pressed;                                                        " \
        "    uniform vec4 u_light_position;                                                        " \
        "                                                                                      \n" \
        "    out vec4 out_color;                                                                      " \
        "   out vec2 out_tex;                                                                 \n" \
        "   out vec4 eyeCoordinates;                                                           \n" \
        "   out vec3 tNorm;                                                                     " \
        "   out vec3 lightDirection;                                                            " \
        "   out vec3 viewerVector;                                                              " \
        "                                                                                     \n" \
        "    void main(void)                                                                        " \
        "    {                                                                                    " \
        "       if(u_l_key_is_pressed == 1)                                                     " \
        "       {                                                                               " \
        "            eyeCoordinates = u_v_matrix * u_m_matrix * vPosition;                        " \
        "            tNorm = mat3(u_v_matrix * u_m_matrix) * vNormal;                            " \
        "            lightDirection = vec3(u_light_position - eyeCoordinates);                   " \
        "           viewerVector = vec3(-eyeCoordinates);                                       " \
        "       }                                                                               " \
        "    gl_Position = u_p_matrix * u_v_matrix * u_m_matrix * vPosition;                        " \
        "   out_tex = vTexCoord;                                                                " \
        "   out_color = vColor;                                                                 " \
        "    }                                                                                    ";
        
        glShaderSource(vertexShaderObject, 1, (const GLchar**) &vertexShadreSourceCode, NULL);
        
        //compile shader
        glCompileShader(vertexShaderObject);
        
        GLint iInfoLogLength = 0;
        GLint iShaderCompileStatus = 0;
        char *szLogInfo = NULL;
        
        glGetShaderiv(vertexShaderObject, GL_COMPILE_STATUS, &iShaderCompileStatus);
        
        if(iShaderCompileStatus == GL_FALSE)
        {
            glGetShaderiv(vertexShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
            if(iInfoLogLength > 0)
            {
                szLogInfo = (char *)malloc(iInfoLogLength);
                
                if(szLogInfo != NULL)
                {
                    GLsizei written;
                    glGetShaderInfoLog(vertexShaderObject, iInfoLogLength, &written, szLogInfo);
                    printf("Vertex Shader Compilation Log : %s \n", szLogInfo);
                    free(szLogInfo);
                    [self release];
                }
            }
        }
        
        //****************************** FRAGMENT SHADER *********************************
        fragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);
        
        const GLchar* fragmentShaderSourceCode =
        "#version 300 es                " \
        "                           \n  " \
        "precision highp float;     \n  " \
        "    in vec3 tNorm;                                                                         " \
        "    in vec3 lightDirection;                                                                " \
        "    in vec3 viewerVector;                                                                  " \
        "    in vec2 out_tex;                                                                      \n" \
        "    in vec4 out_color;                                                                    \n" \
        "                                                                                          \n" \
        "    out vec4 fragColor;                                                                     " \
        "                                                                                          \n" \
        "    uniform vec3 u_ld;                                                                      " \
        "    uniform vec3 u_kd;                                                                      " \
        "    uniform vec3 u_la;                                                                      " \
        "    uniform vec3 u_ka;                                                                      " \
        "    uniform vec3 u_ls;                                                                      " \
        "    uniform vec3 u_ks;                                                                      " \
        "    uniform float u_material_shininess;                                                     " \
        "    uniform mediump int u_l_key_is_pressed;                                                         " \
        "    uniform sampler2D u_sampler;                                                          \n" \
        "                                                                                          \n" \
        "    void main(void)                                                                             " \
        "    {                                                                                       " \
        "        vec3 phong_ads_light;                                                               " \
        "        vec4 texture_color;                                                               \n" \
        "                                                                                          \n" \
        "        if(u_l_key_is_pressed == 1)                                                         " \
        "        {                                                                                   " \
        "            vec3 normalizedtNorm = normalize(tNorm);                                        " \
        "            vec3 normalizedlightDirection = normalize(lightDirection);                      " \
        "            vec3 normalizedviewerVector = normalize(viewerVector);                          " \
        "                                                                                          \n" \
        "            vec3 reflectionVector = normalize(reflect(-normalizedlightDirection, normalizedtNorm));" \
        "            float tnDotld = max(dot(normalizedlightDirection, normalizedtNorm), 0.0);       " \
        "            float rvDotvv = max(dot(reflectionVector, normalizedviewerVector), 0.0);        " \
        "                                                                                          \n" \
        "            vec3 ambient = u_la * u_ka;                                                     " \
        "            vec3 diffuse = u_ld * u_kd * tnDotld;                                           " \
        "            vec3 specular = u_ls * u_ks * pow(rvDotvv, u_material_shininess);               " \
        "            phong_ads_light = diffuse + ambient + specular;                                 " \
        "                                                                                          \n" \
        "        }                                                                                   " \
        "        else                                                                                " \
        "        {                                                                                   " \
        "            phong_ads_light = vec3(1.0, 0.0, 1.0);                                          " \
        "        }                                                                                   " \
        "    texture_color = texture(u_sampler, out_tex);                                          \n" \
        "                                                                                          \n" \
        "    fragColor =  texture_color * vec4(( vec3(out_color) *phong_ads_light), 1.0);          \n" \
        "    }                                                                                       ";
        
        glShaderSource(fragmentShaderObject, 1, (const GLchar**)&fragmentShaderSourceCode, NULL);
        
        //compile shader
        glCompileShader(fragmentShaderObject);
        
        iInfoLogLength = 0;
        iShaderCompileStatus = 0;
        szLogInfo = NULL;
        
        glGetShaderiv(fragmentShaderObject, GL_COMPILE_STATUS, &iShaderCompileStatus);
        
        if(iShaderCompileStatus == GL_FALSE)
        {
            glGetShaderiv(fragmentShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
            if(iInfoLogLength > 0)
            {
                szLogInfo = (char *)malloc(iInfoLogLength);
                
                if(szLogInfo != NULL)
                {
                    GLsizei written;
                    glGetShaderInfoLog(fragmentShaderObject, iInfoLogLength, &written, szLogInfo);
                    printf("Fragment Shader Compilation Log : %s \n", szLogInfo);
                    free(szLogInfo);
                    [self release];
                }
            }
        }
        
        //************************************ SHADER PROGRAM ***********************************
        //create
        shaderProgramObject = glCreateProgram();
        
        // attach shaders
        glAttachShader(shaderProgramObject, vertexShaderObject);
        glAttachShader(shaderProgramObject, fragmentShaderObject);
        
        glBindAttribLocation(shaderProgramObject, AMC_ATTRIBUTE_POSITION, "vPosition");
        glBindAttribLocation(shaderProgramObject, AMC_ATTRIBUTE_COLOR, "vColor");
        glBindAttribLocation(shaderProgramObject, AMC_ATTRIBUTE_NORMAL, "vNormal");
        glBindAttribLocation(shaderProgramObject, AMC_ATTRIBUTE_TEXTURE0, "vTexCoord");
        
        //link
        glLinkProgram(shaderProgramObject);
        
        GLint iShaderProgramLinkStatus = 0;
        
        glGetProgramiv(shaderProgramObject, GL_LINK_STATUS, &iShaderProgramLinkStatus);
        
        if(iShaderProgramLinkStatus == GL_FALSE)
        {
            glGetProgramiv(shaderProgramObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
            
            if(iInfoLogLength > 0)
            {
                szLogInfo = (char *)malloc(iInfoLogLength);
                if(szLogInfo != NULL)
                {
                    GLsizei written;
                    glGetProgramInfoLog(shaderProgramObject, iInfoLogLength, &written, szLogInfo);
                    printf("Shader Program Link Log : %s\n", szLogInfo);
                    free(szLogInfo);
                    [self release];
                }
            }
        }
        
        // get MVP uniform location
        vUniform = glGetUniformLocation(shaderProgramObject, "u_v_matrix");
        mUniform = glGetUniformLocation(shaderProgramObject, "u_m_matrix");
        pUniform = glGetUniformLocation(shaderProgramObject, "u_p_matrix");
        LdUniform = glGetUniformLocation(shaderProgramObject, "u_ld");
        KdUniform = glGetUniformLocation(shaderProgramObject, "u_kd");
        LaUniform = glGetUniformLocation(shaderProgramObject, "u_la");
        KaUniform = glGetUniformLocation(shaderProgramObject, "u_ka");
        LsUniform = glGetUniformLocation(shaderProgramObject, "u_ls");
        KsUniform = glGetUniformLocation(shaderProgramObject, "u_ks");
        materialShininessUniform = glGetUniformLocation(shaderProgramObject, "u_material_shininess");
        lightPositionUniform = glGetUniformLocation(shaderProgramObject, "u_light_position");
        keyPressedUniform = glGetUniformLocation(shaderProgramObject, "u_l_key_is_pressed");
        samplerUniform = glGetUniformLocation(shaderProgramObject, "u_sampler");
        
        const GLfloat cube_vcnt[] = {
            1.0f,  1.0f,  1.0f,     1.0f, 0.0f, 0.0f,     0.0f,  0.0f,  1.0f,     1.0f, 1.0f,
            -1.0f,  1.0f,  1.0f,     1.0f, 0.0f, 0.0f,     0.0f,  0.0f,  1.0f,    0.0f, 1.0f,
            -1.0f, -1.0f,  1.0f,     1.0f, 0.0f, 0.0f,     0.0f,  0.0f,  1.0f,    0.0f, 0.0f,
            1.0f, -1.0f,  1.0f,     1.0f, 0.0f, 0.0f,     0.0f,  0.0f,  1.0f,    1.0f, 0.0f,
            1.0f,  1.0f, -1.0f,     0.0f, 1.0f, 0.0f,     1.0f,  0.0f,  0.0f,    1.0f, 0.0f,
            1.0f,  1.0f,  1.0f,     0.0f, 1.0f, 0.0f,     1.0f,  0.0f,  0.0f,    1.0f, 1.0f,
            1.0f, -1.0f,  1.0f,     0.0f, 1.0f, 0.0f,     1.0f,  0.0f,  0.0f,    0.0f, 1.0f,
            1.0f, -1.0f, -1.0f,     0.0f, 1.0f, 0.0f,     1.0f,  0.0f,  0.0f,    0.0f, 0.0f,
            1.0f,  1.0f, -1.0f,     0.0f, 0.0f, 1.0f,     0.0f,  0.0f, -1.0f,     0.0f, 0.0f,
            -1.0f,  1.0f, -1.0f,     0.0f, 0.0f, 1.0f,     0.0f,  0.0f, -1.0f,    0.0f, 1.0f,
            -1.0f, -1.0f, -1.0f,     0.0f, 0.0f, 1.0f,     0.0f,  0.0f, -1.0f,    1.0f, 1.0f,
            1.0f, -1.0f, -1.0f,     0.0f, 0.0f, 1.0f,     0.0f,  0.0f, -1.0f,    1.0f, 0.0f,
            -1.0f,  1.0f, -1.0f,     1.0f, 0.0f, 1.0f,    -1.0f,  0.0f,  0.0f,    1.0f, 0.0f,
            -1.0f,  1.0f,  1.0f,     1.0f, 0.0f, 1.0f,    -1.0f,  0.0f,  0.0f,    0.0f, 0.0f,
            -1.0f, -1.0f,  1.0f,     1.0f, 0.0f, 1.0f,    -1.0f,  0.0f,  0.0f,    0.0f, 1.0f,
            -1.0f, -1.0f, -1.0f,     1.0f, 0.0f, 1.0f,    -1.0f,  0.0f,  0.0f,    1.0f, 1.0f,
            1.0f,  1.0f, -1.0f,     1.0f, 1.0f, 0.0f,     0.0f,  1.0f,  0.0f,     0.0f, 1.0f,
            -1.0f,  1.0f, -1.0f,     1.0f, 1.0f, 0.0f,     0.0f,  1.0f,  0.0f,    0.0f, 0.0f,
            -1.0f,  1.0f,  1.0f,     1.0f, 1.0f, 0.0f,     0.0f,  1.0f,  0.0f,    1.0f, 0.0f,
            1.0f,  1.0f,  1.0f,     1.0f, 1.0f, 0.0f,     0.0f,  1.0f,  0.0f,    1.0f, 1.0f,
            1.0f, -1.0f, -1.0f,     0.0f, 1.0f, 1.0f,     0.0f, -1.0f,  0.0f,    1.0f, 0.0f,
            -1.0f, -1.0f, -1.0f,     0.0f, 1.0f, 1.0f,     0.0f, -1.0f,  0.0f,    0.0f, 0.0f,
            -1.0f, -1.0f,  1.0f,     0.0f, 1.0f, 1.0f,     0.0f, -1.0f,  0.0f,    0.0f, 1.0f,
            1.0f, -1.0f,  1.0f,     0.0f, 1.0f, 1.0f,     0.0f, -1.0f,  0.0f,    1.0f, 1.0f
        };//front:4-right:4-back:4-left:4-top:4-bottom:4
        
        
        
        //    *** CUBE ***
        glGenVertexArrays(1, &vao_cube);
        glBindVertexArray(vao_cube);
        //buffer for all  data
        glGenBuffers(1, &vbo_data_cube);
        glBindBuffer(GL_ARRAY_BUFFER, vbo_data_cube);
        glBufferData(GL_ARRAY_BUFFER, sizeof(cube_vcnt), cube_vcnt, GL_STATIC_DRAW);
        
        glVertexAttribPointer(AMC_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 11 * sizeof(GLfloat), (void *)(0 * sizeof(GLfloat)));
        glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);
        
        glVertexAttribPointer(AMC_ATTRIBUTE_COLOR, 3, GL_FLOAT, GL_FALSE, 11 * sizeof(GLfloat), (void *)(3 * sizeof(GLfloat)));
        glEnableVertexAttribArray(AMC_ATTRIBUTE_COLOR);
        
        glVertexAttribPointer(AMC_ATTRIBUTE_NORMAL, 3, GL_FLOAT, GL_FALSE, 11 * sizeof(GLfloat), (void *)(6 * sizeof(GLfloat)));
        glEnableVertexAttribArray(AMC_ATTRIBUTE_NORMAL);
        
        glVertexAttribPointer(AMC_ATTRIBUTE_TEXTURE0, 2, GL_FLOAT, GL_FALSE, 11 * sizeof(GLfloat), (void *)(9 * sizeof(GLfloat)));
        glEnableVertexAttribArray(AMC_ATTRIBUTE_TEXTURE0);
        
        glBindBuffer(GL_ARRAY_BUFFER, 0);
        glBindVertexArray(0);
        
        // load texture
        textureMarble = [self loadTextureFromBMPFile:@"Marble" :@"bmp"];
        
        lightAmbient[0] = 0.25f;
        lightAmbient[1] = 0.25f;
        lightAmbient[2] = 0.25f;
        lightAmbient[3] = 0.25f;
        
        lightDiffuse[0] =  1.0f;
        lightDiffuse[1] =  1.0f;
        lightDiffuse[2] =  1.0f;
        lightDiffuse[3] =  1.0f;
        
        lightSpecular[0] = 1.0f;
        lightSpecular[1] = 1.0f;
        lightSpecular[2] = 1.0f;
        lightSpecular[3] = 1.0f;
        
        lightPosition[0] = 2.0f;
        lightPosition[1] = 2.0f;
        lightPosition[2] = 2.0f;
        lightPosition[3] = 1.0f;
        
        materialAmbient[0] = 0.25f;
        materialAmbient[1] = 0.25f;
        materialAmbient[2] = 0.25f;
        materialAmbient[3] = 0.0f;
        
        materialDiffuse[0] = 1.0f;
        materialDiffuse[1] = 1.0f;
        materialDiffuse[2] = 1.0f;
        materialDiffuse[3] = 1.0f;
        
        materialSpecular[0] = 1.0f;
        materialSpecular[1] = 1.0f;
        materialSpecular[2] = 1.0f;
        materialSpecular[3] = 1.0f;
        
        materialShininess = 128.0f;
        
        startAnimation = NO;
        enableLighting = NO;
        
        glEnable(GL_DEPTH_TEST);
        glDepthFunc(GL_LEQUAL);
        
        glDisable(GL_CULL_FACE);
        
        // clear color
        glClearColor(0.0f, 0.0f, 0.0f, 1.0f);       //blue
        
        perspectiveProjectionMatrix = vmath::mat4::identity();
        
        //Gesture Recognition Code
        //Tap gesture code
        UITapGestureRecognizer *singleTapGestureRecognizer = [ [ UITapGestureRecognizer alloc ] initWithTarget:self action:@selector(onSingleTap:)] ;
        [singleTapGestureRecognizer setNumberOfTapsRequired:1]; // 1 touch for 1 fingure
        [singleTapGestureRecognizer setDelegate:self];
        [self addGestureRecognizer:singleTapGestureRecognizer];
        
        // Double Tap Gesture Code
        UITapGestureRecognizer *doubleTapGestureRecognizer = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(onDoubleTap:)];
        [doubleTapGestureRecognizer setNumberOfTapsRequired:2];
        [doubleTapGestureRecognizer setNumberOfTouchesRequired:1]; // 1 touch of fingure
        [doubleTapGestureRecognizer setDelegate:self];
        [self addGestureRecognizer:doubleTapGestureRecognizer];
        
        [singleTapGestureRecognizer requireGestureRecognizerToFail:doubleTapGestureRecognizer];
        
        //swipe gesture
        UISwipeGestureRecognizer *swipGestureRecognizer = [ [UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(onSwipe:) ] ;
        [self addGestureRecognizer:swipGestureRecognizer];
        
        // long Pres Gesture
        UILongPressGestureRecognizer *longPressGestureRecognizer = [ [UILongPressGestureRecognizer alloc]initWithTarget:self action:@selector(onLongPress:) ];
        
        [self addGestureRecognizer:longPressGestureRecognizer];
        
    }
    return(self);
}

/*
 // Only override drawRect: if you perform custom drawing.
 // An empty implementation adversely affects performance during animation.
 - (void)drawRect:(CGRect)rect
 {
 // Drawing code
 }
 */

//static method
+(Class)layerClass
{
    //code
    return([CAEAGLLayer class]);
}

-(GLuint)loadTextureFromBMPFile:(NSString *)texFileName :(NSString *)extension
{
    NSString *textureFileNameWithPath = [[NSBundle mainBundle]pathForResource:texFileName ofType:extension];
    
    UIImage *bmpImage = [[UIImage alloc]initWithContentsOfFile:textureFileNameWithPath];
    if(!bmpImage)
    {
        NSLog(@"can't find %@", textureFileNameWithPath);
        return(0);
    }
    
    CGImageRef cgImage = bmpImage.CGImage;
    
    int w = (int)CGImageGetWidth(cgImage);
    int h = (int)CGImageGetHeight(cgImage);
    
    CFDataRef imageData = CGDataProviderCopyData(CGImageGetDataProvider(cgImage));
    void *pixels = (void *)CFDataGetBytePtr(imageData);
    
    GLuint bmpTexture;
    
    glGenTextures(1, &bmpTexture);
    
    glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
    glBindTexture(GL_TEXTURE_2D, bmpTexture);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
    
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, w, h, 0, GL_RGBA, GL_UNSIGNED_BYTE, pixels);
    
    //create mipmaps for better performance and texture image quality
    glGenerateMipmap(GL_TEXTURE_2D);
    
    CFRelease(imageData);
    return(bmpTexture);
}

-(void)drawView:(id)sender
{
    //code
    [EAGLContext setCurrentContext:eaglContext];
    
    glBindFramebuffer(GL_FRAMEBUFFER, defaultFramebuffer);
    
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);
    
    glUseProgram(shaderProgramObject);
    
    //drawing
    vmath::mat4 modelMatrix = vmath::mat4::identity();
    vmath::mat4 viewMatrix = vmath::mat4::identity();
    vmath::mat4 projectionMatrix = vmath::mat4::identity();
    vmath::mat4 translationMatrix = vmath::mat4::identity();
    vmath::mat4 rotationMatrix = vmath::mat4::identity();
    
    translationMatrix = vmath::translate(0.0f, 0.0f, -5.0f);
    rotationMatrix =  vmath::rotate(angle_cube, angle_cube, angle_cube);
    modelMatrix = translationMatrix * rotationMatrix;
    projectionMatrix = perspectiveProjectionMatrix;
    
    if (enableLighting == YES)
    {
        glUniform1i(keyPressedUniform, 1);
        glUniform3fv(LdUniform, 1, lightDiffuse);
        glUniform3fv(LaUniform, 1, lightAmbient);
        glUniform3fv(LsUniform, 1, lightSpecular);
        
        glUniform3fv(KdUniform, 1, materialDiffuse);
        glUniform3fv(KaUniform, 1, materialAmbient);
        glUniform3fv(KsUniform, 1, materialSpecular);
        
        glUniform1f(materialShininessUniform, materialShininess);
        
        glUniform4f(lightPositionUniform, 0.0f, 0.0f, 2.0f, 1.0f);
    }
    else
    {
        glUniform1i(keyPressedUniform, 0);
    }
    //send necessary matrices to shader in respective uniforms
    glUniform1i(samplerUniform, 0);
    glUniformMatrix4fv(mUniform, 1, GL_FALSE, modelMatrix);
    glUniformMatrix4fv(vUniform, 1, GL_FALSE, viewMatrix);
    glUniformMatrix4fv(pUniform, 1, GL_FALSE, projectionMatrix);
    
    // bind vao
    glBindVertexArray(vao_cube);
    
    // draw
    glDrawArrays(GL_TRIANGLE_FAN, 0, 4);
    glDrawArrays(GL_TRIANGLE_FAN, 4, 4);
    glDrawArrays(GL_TRIANGLE_FAN, 8, 4);
    glDrawArrays(GL_TRIANGLE_FAN, 12, 4);
    glDrawArrays(GL_TRIANGLE_FAN, 16, 4);
    glDrawArrays(GL_TRIANGLE_FAN, 20, 4);
    
    glBindVertexArray(0);
    
    glUseProgram(0);
    
    glBindRenderbuffer(GL_RENDERBUFFER, colorRenderbuffer);
    [eaglContext presentRenderbuffer:GL_RENDERBUFFER];          // swapbuffers
    
    if (startAnimation == YES)
    {
        if (angle_cube < 360.0f)
            angle_cube = angle_cube + 0.5f;
        else
            angle_cube = 0.0f;
    }
}

-(void)layoutSubviews   //reshape
{
    //code
    GLint width;
    GLint height;
    
    glBindRenderbuffer(GL_RENDERBUFFER, colorRenderbuffer);
    [eaglContext renderbufferStorage:GL_RENDERBUFFER fromDrawable:(CAEAGLLayer*)self.layer];
    glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_WIDTH, &width);
    glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_HEIGHT, &height);
    glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, depthRenderbuffer);
    
    glViewport(0, 0, width, height);
    
    //ortho(l-r-b-t-n-f)
    GLfloat fwidth = (GLfloat)width;
    GLfloat fheight = (GLfloat)height;
    
    perspectiveProjectionMatrix = vmath::perspective(45.0f, fwidth/fheight, 0.1f, 100.0f);
    
    if(glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
    {
        printf("Failed to Create Complete ramebuffer Object %x \n", glCheckFramebufferStatus(GL_FRAMEBUFFER));
    }
    [self drawView:nil];//repaint like warmup style
}

-(void)startAnimation
{
    if(!isAnimating)
    {
        displaylink = [NSClassFromString(@"CADisplayLink") displayLinkWithTarget:self selector:@selector(drawView:)];
        [displaylink setPreferredFramesPerSecond:animationFrameInterval];
        [displaylink addToRunLoop:[NSRunLoop currentRunLoop] forMode:NSDefaultRunLoopMode];
        
        isAnimating = YES;
    }
}

-(void)stopAnimation
{
    if(isAnimating)
    {
        [displaylink invalidate];
        displaylink=nil;
        
        isAnimating = NO;
    }
}

-(BOOL) acceptsFirstResponder
{
    return YES;
}

-(void)touchesBegane :(NSSet *)touches withEvent:(UIEvent *)UIEvent
{
    
}


-(void) onLongPress: (UIGestureRecognizer *)gesture
{
    
}

-(void) onSwipe: (UIGestureRecognizer *)gesture
{
    [self release];
    
    exit(0);
}

-(void) onDoubleTap: (UITapGestureRecognizer *)gesture
{
    if(startAnimation == NO)
        startAnimation = YES;
    else
        startAnimation = NO;
}

-(void) onSingleTap: (UITapGestureRecognizer *)gesture
{
    if(enableLighting == NO)
        enableLighting = YES;
    else
        enableLighting = NO;
}

- (void)dealloc
{
    if (vbo_data_cube)
    {
        glDeleteBuffers(1, &vbo_data_cube);
        vbo_data_cube = 0;
    }

    if (vao_cube)
    {
        glDeleteVertexArrays(1, &vao_cube);
        vao_cube = 0;
    }
    
    glDetachShader(shaderProgramObject, vertexShaderObject);
    glDetachShader(shaderProgramObject, fragmentShaderObject);
    
    glDeleteShader(vertexShaderObject);
    vertexShaderObject = 0;
    
    glDeleteShader(fragmentShaderObject);
    fragmentShaderObject = 0;
    
    glDeleteProgram(shaderProgramObject);
    shaderProgramObject = 0;
    
    if(depthRenderbuffer)
    {
        glDeleteRenderbuffers(1, &depthRenderbuffer);
        depthRenderbuffer=0;
    }
    
    if(colorRenderbuffer)
    {
        glDeleteRenderbuffers(1, &colorRenderbuffer);
        colorRenderbuffer=0;
    }
    
    if(defaultFramebuffer)
    {
        glDeleteFramebuffers(1, &defaultFramebuffer);
        defaultFramebuffer=0;
    }
    
    if([EAGLContext currentContext] == eaglContext)
    {
        [EAGLContext setCurrentContext:nil];
    }
    [eaglContext release];
    eaglContext =nil;
    
    [super dealloc];
}

@end
