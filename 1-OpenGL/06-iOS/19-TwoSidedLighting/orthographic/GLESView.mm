#import <OpenGLES/ES3/gl.h>
#import <OpenGLES/ES3/glext.h>

#import "vmath.h"

#import "GLESView.h"

enum
{
    AMC_ATTRIBUTE_POSITION = 0,
    AMC_ATTRIBUTE_COLOR,
    AMC_ATTRIBUTE_NORMAL,
    AMC_ATTRIBUTE_TEXTURE0
};

@implementation GLESView
{
    EAGLContext *eaglContext;
    
    GLuint defaultFramebuffer;
    GLuint colorRenderbuffer;
    GLuint depthRenderbuffer;
    
    id displaylink;
    NSInteger animationFrameInterval;
    BOOL isAnimating;
    
    GLuint vertexShaderObject;
    GLuint fragmentShaderObject;
    GLuint shaderProgramObject;
    
    GLuint vao_pyramid;
    GLuint vbo_normal_pyramid;
    GLuint vbo_position_pyramid;
    
    // light and material properties values
    float lightDiffuse_blue[4];
    float lightAmbient_blue[4];
    float lightSpecular_blue[4];
    float lightPosition_blue[4];
    
    float lightDiffuse_red[4];
    float lightAmbient_red[4];
    float lightSpecular_red[4];
    float lightPosition_red[4];
    
    float MaterialDiffuse[4];
    float MaterialAmbient[4];
    float MaterialSpecular[4];
    float materialShininess;

    GLuint modelUniform_v, viewUniform_v, projectionUniform_v;
    GLuint blue_ldUniform_v;
    GLuint blue_laUniform_v;
    GLuint blue_lsUniform_v;
    GLuint blue_lightPositionUniform_v;
    
    GLuint red_ldUniform_v;
    GLuint red_laUniform_v;
    GLuint red_lsUniform_v;
    GLuint red_lightPositionUniform_v;
    
    GLuint material_kdUniform_v;
    GLuint material_kaUniform_v;
    GLuint material_ksUniform_v;
    GLuint material_shininessUniform_v;
    GLuint keyPressedUniform_v;
    
    float anglePyramid;
    
    BOOL startAnimation;
    BOOL enableLighting;
    
    vmath::mat4 perspectiveProjectionMatrix;
}

-(id) initWithFrame:(CGRect)frameRect
{
    //code
    self = [super initWithFrame:frameRect];
    
    if (self)
    {
        CAEAGLLayer *eaglLayer = (CAEAGLLayer *)super.layer;
        
        eaglLayer.opaque = YES;
        eaglLayer.drawableProperties = [NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithBool:FALSE],
                                        kEAGLDrawablePropertyRetainedBacking, kEAGLColorFormatRGBA8, kEAGLDrawablePropertyColorFormat, nil];
        
        eaglContext=[[EAGLContext alloc]initWithAPI:kEAGLRenderingAPIOpenGLES3];
        
        if(eaglContext==nil)
        {
            [self release];
            return(nil);
        }
        
        [EAGLContext setCurrentContext:eaglContext];
        
        glGenFramebuffers(1, &defaultFramebuffer);
        glGenRenderbuffers(1, &colorRenderbuffer);
        glBindFramebuffer(GL_FRAMEBUFFER, defaultFramebuffer);
        glBindRenderbuffer(GL_RENDERBUFFER, colorRenderbuffer);
        
        [eaglContext renderbufferStorage:GL_RENDERBUFFER fromDrawable:eaglLayer];
        
        glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_RENDERBUFFER, colorRenderbuffer);
        
        GLint backingWidth;
        GLint backingHeight;
        
        glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_WIDTH, &backingWidth);
        glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_WIDTH, &backingHeight);
        
        glGenRenderbuffers(1, &depthRenderbuffer);
        glBindRenderbuffer(GL_RENDERBUFFER, depthRenderbuffer);
        glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT16, backingWidth, backingHeight);
        glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, depthRenderbuffer);
        
        if(glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
        {
            printf("Failed to Create Complete ramebuffer Object %x \n", glCheckFramebufferStatus(GL_FRAMEBUFFER));
            glDeleteFramebuffers(1, &defaultFramebuffer);
            glDeleteRenderbuffers(1, &colorRenderbuffer);
            glDeleteRenderbuffers(1, &depthRenderbuffer);
            
            return(nil);
        }
        
        printf("Renderer : %s | GL Version : %s | GLSL Version : %s\n",
               glGetString(GL_RENDERER), glGetString(GL_VERSION), glGetString(GL_SHADING_LANGUAGE_VERSION));
        
        // hardcore initializations
        isAnimating = NO;
        animationFrameInterval = 60;    //default since iOS 8.2
        
        // *********************** VERTEX SHADER ***************************
        vertexShaderObject = glCreateShader(GL_VERTEX_SHADER);
        
        const GLchar* vertexShadreSourceCode =
        "#version 300 es                                " \
        "                                           \n  " \
        "    in vec4 vPosition;                                                                         " \
        "    in vec3 vNormal;                                                                           " \
        "                                                                                            \n " \
        "    out vec3 phong_ads_light;                                                                  " \
        "                                                                                            \n " \
        "    uniform mat4 u_m_matrix;                                                                   " \
        "    uniform mat4 u_v_matrix;                                                                   " \
        "    uniform mat4 u_p_matrix;                                                                   " \
        "    uniform mediump int u_l_key_is_pressed;                                                            " \
        "    uniform vec3 u_blue_ld;                                                                    " \
        "    uniform vec3 u_blue_la;                                                                    " \
        "    uniform vec3 u_blue_ls;                                                                    " \
        "    uniform vec3 u_red_ld;                                                                     " \
        "    uniform vec3 u_red_la;                                                                     " \
        "    uniform vec3 u_red_ls;                                                                     " \
        "    uniform vec3 u_ka;                                                                         " \
        "    uniform vec3 u_kd;                                                                         " \
        "    uniform vec3 u_ks;                                                                         " \
        "    uniform vec4 u_blue_light_position;                                                        " \
        "    uniform vec4 u_red_light_position;                                                         " \
        "    uniform float u_material_shininess;                                                        " \
        "                                                                                            \n " \
        "    void main(void)                                                                            " \
        "    {                                                                                          " \
        "        if(u_l_key_is_pressed == 1)                                                            " \
        "        {                                                                                      " \
        "            vec4 eyeCoordinates = u_v_matrix * u_m_matrix * vPosition;                         " \
        "            vec3 tNorm            = normalize(mat3(u_v_matrix * u_m_matrix) * vNormal);        " \
        "            vec3 viewerVector   = normalize(vec3(-eyeCoordinates));                            " \
        "                                                                                            \n " \
        "            vec3 blueLightDirection = normalize(vec3(vec4(-3.0, 0.0, 0.0, 1.0) - eyeCoordinates)); " \
        "            vec3 blueLightReflectionVector = reflect(-blueLightDirection, tNorm);              " \
        "            float bluetnDotld = max(dot(tNorm, blueLightDirection), 0.0);                      " \
        "            float bluervDotvv = max(dot(viewerVector, blueLightReflectionVector), 0.0);        " \
        "                                                                                            \n " \
        "            vec3 redLightDirection  = normalize(vec3(u_red_light_position - eyeCoordinates));  " \
        "            vec3 redLightReflectionVector = reflect(-redLightDirection, tNorm);                " \
        "            float redtnDotld = max(dot(tNorm, redLightDirection), 0.0);                        " \
        "            float redrvDotvv = max(dot(viewerVector, redLightReflectionVector), 0.0);          " \
        "                                                                                            \n " \
        "            vec3 blueAmbient  = u_blue_la * u_ka;                                              " \
        "            vec3 blueDiffuse  = u_blue_ld * u_kd * bluetnDotld;                                " \
        "            vec3 blueSpecular = u_blue_ls * u_ks * pow(bluervDotvv, u_material_shininess);     " \
        "                                                                                            \n " \
        "            vec3 redAmbient  = u_red_la * u_ka;                                                " \
        "            vec3 redDiffuse  = u_red_ld * u_kd * redtnDotld;                                   " \
        "            vec3 redSpecular = u_red_ls * u_ks * pow(redrvDotvv, u_material_shininess);        " \
        "                                                                                            \n " \
        "            vec3 Ambient  = blueAmbient + redAmbient;                                          " \
        "            vec3 Diffuse  = blueDiffuse + redDiffuse;                                          " \
        "            vec3 Specular = blueSpecular + redSpecular;                                        " \
        "            phong_ads_light = Ambient + Diffuse + Specular;                                    " \
        "        }                                                                                      " \
        "        else                                                                                   " \
        "        {                                                                                      " \
        "            phong_ads_light = vec3(1.0, 1.0, 1.0);                                             " \
        "        }                                                                                      " \
        "        gl_Position = u_v_matrix * u_p_matrix * u_m_matrix * vPosition;                        " \
        "    }                                                                                          ";
        
        glShaderSource(vertexShaderObject, 1, (const GLchar**) &vertexShadreSourceCode, NULL);
        
        //compile shader
        glCompileShader(vertexShaderObject);
        
        GLint iInfoLogLength = 0;
        GLint iShaderCompileStatus = 0;
        char *szLogInfo = NULL;
        
        glGetShaderiv(vertexShaderObject, GL_COMPILE_STATUS, &iShaderCompileStatus);
        
        if(iShaderCompileStatus == GL_FALSE)
        {
            glGetShaderiv(vertexShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
            if(iInfoLogLength > 0)
            {
                szLogInfo = (char *)malloc(iInfoLogLength);
                
                if(szLogInfo != NULL)
                {
                    GLsizei written;
                    glGetShaderInfoLog(vertexShaderObject, iInfoLogLength, &written, szLogInfo);
                    printf("Vertex Shader Compilation Log : %s \n", szLogInfo);
                    free(szLogInfo);
                    [self release];
                }
            }
        }
        
        //****************************** FRAGMENT SHADER *********************************
        fragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);
        
        const GLchar* fragmentShaderSourceCode =
        "#version 300 es                " \
        "                           \n  " \
        "precision highp float;     \n  " \
        "                           \n  " \
        "    in vec3 phong_ads_light;                               " \
        "                                                       \n  " \
        "    out vec4 FragColor;                                    " \
        "                                                       \n  " \
        "    void main(void)                                        " \
        "    {                                                      " \
        "        FragColor = vec4(phong_ads_light,1.0);             " \
        "    }                                                      ";
        
        glShaderSource(fragmentShaderObject, 1, (const GLchar**)&fragmentShaderSourceCode, NULL);
        
        //compile shader
        glCompileShader(fragmentShaderObject);
        
        iInfoLogLength = 0;
        iShaderCompileStatus = 0;
        szLogInfo = NULL;
        
        glGetShaderiv(fragmentShaderObject, GL_COMPILE_STATUS, &iShaderCompileStatus);
        
        if(iShaderCompileStatus == GL_FALSE)
        {
            glGetShaderiv(fragmentShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
            if(iInfoLogLength > 0)
            {
                szLogInfo = (char *)malloc(iInfoLogLength);
                
                if(szLogInfo != NULL)
                {
                    GLsizei written;
                    glGetShaderInfoLog(fragmentShaderObject, iInfoLogLength, &written, szLogInfo);
                    printf("Fragment Shader Compilation Log : %s \n", szLogInfo);
                    free(szLogInfo);
                    [self release];
                }
            }
        }
        
        //************************************ SHADER PROGRAM ***********************************
        //create
        shaderProgramObject = glCreateProgram();
        
        // attach shaders
        glAttachShader(shaderProgramObject, vertexShaderObject);
        glAttachShader(shaderProgramObject, fragmentShaderObject);
        
        glBindAttribLocation(shaderProgramObject, AMC_ATTRIBUTE_POSITION, "vPosition");
        glBindAttribLocation(shaderProgramObject, AMC_ATTRIBUTE_NORMAL, "vNormal");
        
        //link
        glLinkProgram(shaderProgramObject);
        
        GLint iShaderProgramLinkStatus = 0;
        
        glGetProgramiv(shaderProgramObject, GL_LINK_STATUS, &iShaderProgramLinkStatus);
        
        if(iShaderProgramLinkStatus == GL_FALSE)
        {
            glGetProgramiv(shaderProgramObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
            
            if(iInfoLogLength > 0)
            {
                szLogInfo = (char *)malloc(iInfoLogLength);
                if(szLogInfo != NULL)
                {
                    GLsizei written;
                    glGetProgramInfoLog(shaderProgramObject, iInfoLogLength, &written, szLogInfo);
                    printf("Shader Program Link Log : %s\n", szLogInfo);
                    free(szLogInfo);
                    [self release];
                }
            }
        }
        
        // get uniform location
        // model, view and projection matrices
        viewUniform_v = glGetUniformLocation(shaderProgramObject, "u_v_matrix");
        modelUniform_v = glGetUniformLocation(shaderProgramObject, "u_m_matrix");
        projectionUniform_v = glGetUniformLocation(shaderProgramObject, "u_p_matrix");
        // blue light uniforms
        blue_ldUniform_v = glGetUniformLocation(shaderProgramObject, "u_blue_ld");
        blue_laUniform_v = glGetUniformLocation(shaderProgramObject, "u_blue_la");
        blue_lsUniform_v = glGetUniformLocation(shaderProgramObject, "u_blue_ls");
        blue_lightPositionUniform_v = glGetUniformLocation(shaderProgramObject, "u_blue_light_position");
        // red light uniforms
        red_ldUniform_v = glGetUniformLocation(shaderProgramObject, "u_red_ld");
        red_laUniform_v = glGetUniformLocation(shaderProgramObject, "u_red_la");
        red_lsUniform_v = glGetUniformLocation(shaderProgramObject, "u_red_ls");
        red_lightPositionUniform_v = glGetUniformLocation(shaderProgramObject, "u_red_light_position");
        // material uniform
        material_kdUniform_v = glGetUniformLocation(shaderProgramObject, "u_kd");
        material_kaUniform_v = glGetUniformLocation(shaderProgramObject, "u_ka");
        material_ksUniform_v = glGetUniformLocation(shaderProgramObject, "u_ks");
        material_shininessUniform_v = glGetUniformLocation(shaderProgramObject, "u_material_shininess");
        // key pressed uniform
        keyPressedUniform_v = glGetUniformLocation(shaderProgramObject, "u_l_key_is_pressed");
        
        /// TRIANGLE data, vao and vbo
        GLfloat pyramidVertices[] = {
            0.0f, 1.0f, 0.0f, -1.0f, -1.0f, 1.0f, 1.0f, -1.0f, 1.0f,
            0.0f, 1.0f, 0.0f, 1.0f, -1.0f, 1.0f, 1.0f, -1.0f, -1.0f,
            0.0f, 1.0f, 0.0f, 1.0f, -1.0f, -1.0f, -1.0f, -1.0f, -1.0f,
            0.0f, 1.0f, 0.0f, -1.0f, -1.0f, -1.0f, -1.0f, -1.0f, 1.0f
        };//front-right-back-left
        
        const GLfloat pyramidNormal[] = {
            0.0f, 0.447214f, 0.894427f, 0.0f, 0.447214f, 0.894427f, 0.0f, 0.447214f, 0.894427f,
            0.894427f, 0.447214f, 0.0f, 0.894427f, 0.447214f, 0.0f, 0.894427f, 0.447214f, 0.0f,
            0.0f, 0.447214f, -0.894427f, 0.0f, 0.447214f, -0.894427f, 0.0f, 0.447214f, -0.894427f,
            -0.894427f, 0.447214f, 0.0f, -0.894427f, 0.447214f, 0.0f, -0.894427f, 0.447214f, 0.0f
        };
        
        //    *** PYRAMID ***
        glGenVertexArrays(1, &vao_pyramid);
        glBindVertexArray(vao_pyramid);
        //buffer for position
        glGenBuffers(1, &vbo_position_pyramid);
        glBindBuffer(GL_ARRAY_BUFFER, vbo_position_pyramid);
        glBufferData(GL_ARRAY_BUFFER, sizeof(pyramidVertices), pyramidVertices, GL_STATIC_DRAW);
        glVertexAttribPointer(AMC_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
        glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);
        glBindBuffer(GL_ARRAY_BUFFER, 0);
        
        //buffer for color
        glGenBuffers(1, &vbo_normal_pyramid);
        glBindBuffer(GL_ARRAY_BUFFER, vbo_normal_pyramid);
        glBufferData(GL_ARRAY_BUFFER, sizeof(pyramidNormal), pyramidNormal, GL_STATIC_DRAW);
        glVertexAttribPointer(AMC_ATTRIBUTE_NORMAL, 3, GL_FLOAT, GL_FALSE, 0, NULL);
        glEnableVertexAttribArray(AMC_ATTRIBUTE_NORMAL);
        glBindBuffer(GL_ARRAY_BUFFER, 0);
        
        glBindVertexArray(0); // unbinding with trianle vao
        
        lightDiffuse_blue[0]    = 0.0f;
        lightDiffuse_blue[1]    = 0.0f;
        lightDiffuse_blue[2]    = 1.0f;
        lightDiffuse_blue[3]    = 1.0f;
        
        lightAmbient_blue[0]    = 0.0f;
        lightAmbient_blue[1]    = 0.0f;
        lightAmbient_blue[2]    = 0.0f;
        lightAmbient_blue[3]    = 0.0f;
        
        lightSpecular_blue[0]    = 1.0f;
        lightSpecular_blue[1]    = 1.0f;
        lightSpecular_blue[2]    = 1.0f;
        lightSpecular_blue[3]    = 1.0f;
    
        lightPosition_blue[0] = -3.0f;
        lightPosition_blue[0] = 0.0f;
        lightPosition_blue[0] = 0.0f;
        lightPosition_blue[0] = 1.0f;
        
        lightDiffuse_red[0]    = 1.0f;
        lightDiffuse_red[1]    = 0.0f;
        lightDiffuse_red[2]    = 0.0f;
        lightDiffuse_red[3]    = 1.0f;
        
        lightAmbient_red[0]    = 0.0f;
        lightAmbient_red[1]    = 0.0f;
        lightAmbient_red[2]    = 0.0f;
        lightAmbient_red[3]    = 0.0f;
        
        lightSpecular_red[0]     = 1.0f;
        lightSpecular_red[1]     = 1.0f;
        lightSpecular_red[2]     = 1.0f;
        lightSpecular_red[3]     = 1.0f;
        
        
        lightPosition_red[0]     = 3.0f;
        lightPosition_red[1]     = 0.0f;
        lightPosition_red[2]     = 0.0f;
        lightPosition_red[3]     = 1.0f;
        
        MaterialDiffuse[0]     = 1.0f;
        MaterialDiffuse[1]     = 1.0f;
        MaterialDiffuse[2]     = 1.0f;
        MaterialDiffuse[3]     = 1.0f;
        
        MaterialAmbient[0]    = 0.5f;
        MaterialAmbient[1]    = 0.5f;
        MaterialAmbient[2]    = 0.5f;
        MaterialAmbient[3]    = 0.0f;
        
        MaterialSpecular[0]    = 1.0f;
        MaterialSpecular[1]    = 1.0f;
        MaterialSpecular[2]    = 1.0f;
        MaterialSpecular[3]    = 1.0f;
        
        materialShininess = 128.0f;
        
       startAnimation = NO;
       enableLighting = NO;

        
        glEnable(GL_DEPTH_TEST);
        glDepthFunc(GL_LEQUAL);
        glDisable(GL_CULL_FACE);
        // clear color
        glClearColor(0.0f, 0.0f, 0.0f, 1.0f);       //blue
        
        perspectiveProjectionMatrix = vmath::mat4::identity();
        
        //Gesture Recognition Code
        //Tap gesture code
        UITapGestureRecognizer *singleTapGestureRecognizer = [ [ UITapGestureRecognizer alloc ] initWithTarget:self action:@selector(onSingleTap:)] ;
        [singleTapGestureRecognizer setNumberOfTapsRequired:1]; // 1 touch for 1 fingure
        [singleTapGestureRecognizer setDelegate:self];
        [self addGestureRecognizer:singleTapGestureRecognizer];
        
        // Double Tap Gesture Code
        UITapGestureRecognizer *doubleTapGestureRecognizer = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(onDoubleTap:)];
        [doubleTapGestureRecognizer setNumberOfTapsRequired:2];
        [doubleTapGestureRecognizer setNumberOfTouchesRequired:1]; // 1 touch of fingure
        [doubleTapGestureRecognizer setDelegate:self];
        [self addGestureRecognizer:doubleTapGestureRecognizer];
        
        [singleTapGestureRecognizer requireGestureRecognizerToFail:doubleTapGestureRecognizer];
        
        //swipe gesture
        UISwipeGestureRecognizer *swipGestureRecognizer = [ [UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(onSwipe:) ] ;
        [self addGestureRecognizer:swipGestureRecognizer];
        
        // long Pres Gesture
        UILongPressGestureRecognizer *longPressGestureRecognizer = [ [UILongPressGestureRecognizer alloc]initWithTarget:self action:@selector(onLongPress:) ];
        
        [self addGestureRecognizer:longPressGestureRecognizer];
        
    }
    return(self);
}

/*
 // Only override drawRect: if you perform custom drawing.
 // An empty implementation adversely affects performance during animation.
 - (void)drawRect:(CGRect)rect
 {
 // Drawing code
 }
 */

//static method
+(Class)layerClass
{
    //code
    return([CAEAGLLayer class]);
}

-(void)drawView:(id)sender
{
    //code
    [EAGLContext setCurrentContext:eaglContext];
    
    glBindFramebuffer(GL_FRAMEBUFFER, defaultFramebuffer);
    
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);
    
    glUseProgram(shaderProgramObject);
    
    vmath::mat4 modelMatrix = vmath::mat4::identity();
    vmath::mat4 viewMatrix = vmath::mat4::identity();
    vmath::mat4 projectionMatrix = vmath::mat4::identity();
    vmath::mat4 translationMatrix = vmath::mat4::identity();
    vmath::mat4 rotationMatrix = vmath::mat4::identity();
    
    glUseProgram(shaderProgramObject);
    
    // **** TRIANGLE ****
    translationMatrix = vmath::translate(0.0f, 0.0f, -4.0f);
    rotationMatrix = vmath::rotate(anglePyramid, 0.0f, 1.0f, 0.0f);
    modelMatrix = translationMatrix * rotationMatrix;
    projectionMatrix = perspectiveProjectionMatrix;
    
    if(enableLighting == YES)
    {
        //send necessary matrices to shader in respective uniforms
        glUniform1i(keyPressedUniform_v, 1);
        glUniform3fv(blue_ldUniform_v, 1, lightDiffuse_blue);
        glUniform3fv(blue_laUniform_v, 1, lightAmbient_blue);
        glUniform3fv(blue_lsUniform_v, 1, lightSpecular_blue);
        glUniform3fv(red_ldUniform_v, 1, lightDiffuse_red);
        glUniform3fv(red_laUniform_v, 1, lightAmbient_red);
        glUniform3fv(red_lsUniform_v, 1, lightSpecular_red);
        glUniform3fv(material_kdUniform_v, 1, MaterialDiffuse);
        glUniform3fv(material_kaUniform_v, 1, MaterialAmbient);
        glUniform3fv(material_ksUniform_v, 1, MaterialSpecular);
        glUniform1f(material_shininessUniform_v, materialShininess);
        glUniform4fv(blue_lightPositionUniform_v, 1, lightPosition_blue);
        glUniform4fv(red_lightPositionUniform_v, 1, lightPosition_red);
    }
    else
    {
        glUniform1i(keyPressedUniform_v, 0);
    }
    glUniformMatrix4fv(modelUniform_v, 1, GL_FALSE, modelMatrix);
    glUniformMatrix4fv(viewUniform_v, 1, GL_FALSE, viewMatrix);
    glUniformMatrix4fv(projectionUniform_v, 1, GL_FALSE, projectionMatrix);
    
    glBindVertexArray(vao_pyramid);
    glDrawArrays(GL_TRIANGLES, 0, 12);
    glBindVertexArray(0);
    
    glUseProgram(0);
    
    glBindRenderbuffer(GL_RENDERBUFFER, colorRenderbuffer);
    [eaglContext presentRenderbuffer:GL_RENDERBUFFER];          // swapbuffers
    
    if(startAnimation == YES)
    {
        anglePyramid = anglePyramid + 0.5f;
        if (anglePyramid > 360.0f)
        {
            anglePyramid = 0.0;
        }
    }
    
}

-(void)layoutSubviews   //reshape
{
    //code
    GLint width;
    GLint height;
    
    glBindRenderbuffer(GL_RENDERBUFFER, colorRenderbuffer);
    [eaglContext renderbufferStorage:GL_RENDERBUFFER fromDrawable:(CAEAGLLayer*)self.layer];
    glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_WIDTH, &width);
    glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_HEIGHT, &height);
    glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, depthRenderbuffer);
    
    glViewport(0, 0, width, height);
    
    //ortho(l-r-b-t-n-f)
    GLfloat fwidth = (GLfloat)width;
    GLfloat fheight = (GLfloat)height;
    
    perspectiveProjectionMatrix = vmath::perspective(45.0f, fwidth/fheight, 0.1f, 100.0f);
    
    if(glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
    {
        printf("Failed to Create Complete ramebuffer Object %x \n", glCheckFramebufferStatus(GL_FRAMEBUFFER));
    }
    [self drawView:nil];//repaint like warmup style
}

-(void)startAnimation
{
    if(!isAnimating)
    {
        displaylink = [NSClassFromString(@"CADisplayLink") displayLinkWithTarget:self selector:@selector(drawView:)];
        [displaylink setPreferredFramesPerSecond:animationFrameInterval];
        [displaylink addToRunLoop:[NSRunLoop currentRunLoop] forMode:NSDefaultRunLoopMode];
        
        isAnimating = YES;
    }
}

-(void)stopAnimation
{
    if(isAnimating)
    {
        [displaylink invalidate];
        displaylink=nil;
        
        isAnimating = NO;
    }
}

-(BOOL) acceptsFirstResponder
{
    return YES;
}

-(void)touchesBegane :(NSSet *)touches withEvent:(UIEvent *)UIEvent
{
    
}


-(void) onLongPress: (UIGestureRecognizer *)gesture
{
    
}

-(void) onSwipe: (UIGestureRecognizer *)gesture
{
    [self release];
    
    exit(0);
}

-(void) onDoubleTap: (UITapGestureRecognizer *)gesture
{
    if(startAnimation == NO)
        startAnimation = YES;
    else
        startAnimation = NO;
}

-(void) onSingleTap: (UITapGestureRecognizer *)gesture
{
    if(enableLighting == NO)
        enableLighting = YES;
    else
        enableLighting = NO;
}

- (void)dealloc
{
    
    if (vbo_normal_pyramid)
    {
        glDeleteBuffers(1, &vbo_normal_pyramid);
        vbo_normal_pyramid = 0;
    }
    if (vbo_position_pyramid)
    {
        glDeleteBuffers(1, &vbo_position_pyramid);
        vbo_position_pyramid = 0;
    }
    if (vao_pyramid)
    {
        glDeleteVertexArrays(1, &vao_pyramid);
        vao_pyramid = 0;
    }
    
    glDetachShader(shaderProgramObject, vertexShaderObject);
    glDetachShader(shaderProgramObject, fragmentShaderObject);
    
    glDeleteShader(vertexShaderObject);
    vertexShaderObject = 0;
    
    glDeleteShader(fragmentShaderObject);
    fragmentShaderObject = 0;
    
    glDeleteProgram(shaderProgramObject);
    shaderProgramObject = 0;
    
    if(depthRenderbuffer)
    {
        glDeleteRenderbuffers(1, &depthRenderbuffer);
        depthRenderbuffer=0;
    }
    
    if(colorRenderbuffer)
    {
        glDeleteRenderbuffers(1, &colorRenderbuffer);
        colorRenderbuffer=0;
    }
    
    if(defaultFramebuffer)
    {
        glDeleteFramebuffers(1, &defaultFramebuffer);
        defaultFramebuffer=0;
    }
    
    if([EAGLContext currentContext] == eaglContext)
    {
        [EAGLContext setCurrentContext:nil];
    }
    [eaglContext release];
    eaglContext =nil;
    
    [super dealloc];
}

@end
