#import <OpenGLES/ES3/gl.h>
#import <OpenGLES/ES3/glext.h>

#import "vmath.h"

#import "GLESView.h"

enum
{
    AMC_ATTRIBUTE_POSITION = 0,
    AMC_ATTRIBUTE_COLOR,
    AMC_ATTRIBUTE_NORMAL,
    AMC_ATTRIBUTE_TEXTURE0
};

@implementation GLESView
{
    EAGLContext *eaglContext;
    
    GLuint defaultFramebuffer;
    GLuint colorRenderbuffer;
    GLuint depthRenderbuffer;
    
    id displaylink;
    NSInteger animationFrameInterval;
    BOOL isAnimating;
    
    GLuint vertexShaderObject;
    GLuint fragmentShaderObject;
    GLuint shaderProgramObject;
    
    GLuint vao_cube;
    GLuint vbo_normal_cube;
    GLuint vbo_position_cube;
    
    GLfloat angle_cube;
    
    GLuint mvUniform, pUniform;                // ModelView Matrix Uniform and Projection Matrix Uniform
    GLuint LdUniform, KdUniform;            // Diffuse property of Light and diffuse property of material
    GLuint lightPositionUniform;            // light position
    GLuint keyPressedUniform;                // is L key Pressed, to enable lighting effect
    
    BOOL startAnimation;
    BOOL enableLighting;
    
    vmath::mat4 perspectiveProjectionMatrix;
}

-(id) initWithFrame:(CGRect)frameRect
{
    //code
    self = [super initWithFrame:frameRect];
    
    if (self)
    {
        CAEAGLLayer *eaglLayer = (CAEAGLLayer *)super.layer;
        
        eaglLayer.opaque = YES;
        eaglLayer.drawableProperties = [NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithBool:FALSE],
                                        kEAGLDrawablePropertyRetainedBacking, kEAGLColorFormatRGBA8, kEAGLDrawablePropertyColorFormat, nil];
        
        eaglContext=[[EAGLContext alloc]initWithAPI:kEAGLRenderingAPIOpenGLES3];
        
        if(eaglContext==nil)
        {
            [self release];
            return(nil);
        }
        
        [EAGLContext setCurrentContext:eaglContext];
        
        glGenFramebuffers(1, &defaultFramebuffer);
        glGenRenderbuffers(1, &colorRenderbuffer);
        glBindFramebuffer(GL_FRAMEBUFFER, defaultFramebuffer);
        glBindRenderbuffer(GL_RENDERBUFFER, colorRenderbuffer);
        
        [eaglContext renderbufferStorage:GL_RENDERBUFFER fromDrawable:eaglLayer];
        
        glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_RENDERBUFFER, colorRenderbuffer);
        
        GLint backingWidth;
        GLint backingHeight;
        
        glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_WIDTH, &backingWidth);
        glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_WIDTH, &backingHeight);
        
        glGenRenderbuffers(1, &depthRenderbuffer);
        glBindRenderbuffer(GL_RENDERBUFFER, depthRenderbuffer);
        glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT16, backingWidth, backingHeight);
        glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, depthRenderbuffer);
        
        if(glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
        {
            printf("Failed to Create Complete ramebuffer Object %x \n", glCheckFramebufferStatus(GL_FRAMEBUFFER));
            glDeleteFramebuffers(1, &defaultFramebuffer);
            glDeleteRenderbuffers(1, &colorRenderbuffer);
            glDeleteRenderbuffers(1, &depthRenderbuffer);
            
            return(nil);
        }
        
        printf("Renderer : %s | GL Version : %s | GLSL Version : %s\n",
               glGetString(GL_RENDERER), glGetString(GL_VERSION), glGetString(GL_SHADING_LANGUAGE_VERSION));
        
        // hardcore initializations
        isAnimating = NO;
        animationFrameInterval = 60;    //default since iOS 8.2
        
        // *********************** VERTEX SHADER ***************************
        vertexShaderObject = glCreateShader(GL_VERTEX_SHADER);
        
        const GLchar* vertexShadreSourceCode =
        "   #version 300 es                                " \
        "                                           \n  " \
        "    in vec4 vPosition;                                                                    " \
        "    in vec3 vNormal;                                                                    " \
        "                                                                                      \n" \
        "    uniform mat4 u_mv_matrix;                                                            " \
        "    uniform mat4 u_p_matrix;                                                            " \
        "    uniform mediump int u_l_key_is_pressed;                                                        " \
        "    uniform vec3 u_ld;                                                                    " \
        "    uniform vec3 u_kd;                                                                    " \
        "    uniform vec4 u_light_position;                                                        " \
        "                                                                                      \n" \
        "    out vec3 diffuseColor;                                                                " \
        "                                                                                      \n" \
        "    void main(void)                                                                        " \
        "    {                                                                                    " \
        "        if(u_l_key_is_pressed == 1)                                                        " \
        "        {                                                                                " \
        "            vec4 eyeCoordinates = u_mv_matrix * vPosition;                                " \
        "            mat3 normalMatrix = mat3(transpose(inverse(u_mv_matrix)));                    " \
        "            vec3 tNorm = normalize(normalMatrix * vNormal);                                " \
        "            vec3 s = normalize(vec3(u_light_position - eyeCoordinates));                " \
        "            diffuseColor =u_ld * u_kd * max(dot(s, tNorm), 0.0f);;                        " \
        "        }                                                                                " \
        "    gl_Position = u_p_matrix * u_mv_matrix * vPosition;                                    " \
        "    }                                                                                    ";
        
        glShaderSource(vertexShaderObject, 1, (const GLchar**) &vertexShadreSourceCode, NULL);
        
        //compile shader
        glCompileShader(vertexShaderObject);
        
        GLint iInfoLogLength = 0;
        GLint iShaderCompileStatus = 0;
        char *szLogInfo = NULL;
        
        glGetShaderiv(vertexShaderObject, GL_COMPILE_STATUS, &iShaderCompileStatus);
        
        if(iShaderCompileStatus == GL_FALSE)
        {
            glGetShaderiv(vertexShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
            if(iInfoLogLength > 0)
            {
                szLogInfo = (char *)malloc(iInfoLogLength);
                
                if(szLogInfo != NULL)
                {
                    GLsizei written;
                    glGetShaderInfoLog(vertexShaderObject, iInfoLogLength, &written, szLogInfo);
                    printf("Vertex Shader Compilation Log : %s \n", szLogInfo);
                    free(szLogInfo);
                    [self release];
                }
            }
        }
        
        //****************************** FRAGMENT SHADER *********************************
        fragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);
        
        const GLchar* fragmentShaderSourceCode =
        "#version 300 es                " \
        "                           \n  " \
        "precision highp float;     \n  " \
        "    in vec3 diffuseColor;                                                                    " \
        "    out vec4 fragColor;                                                                        " \
        "    uniform int u_l_key_is_pressed;                                                            " \
        "                                                                                          \n" \
        "    void main(void)                                                                            " \
        "    {                                                                                        " \
        "        if(u_l_key_is_pressed == 1)                                                            " \
        "        {                                                                                    " \
        "            fragColor = vec4(diffuseColor, 1.0);                                            " \
        "        }                                                                                    " \
        "        else                                                                                " \
        "        {                                                                                    " \
        "            fragColor = vec4(1.0, 1.0, 1.0, 1.0);                                            " \
        "        }                                                                                    " \
        "    }                                                                                        ";
        
        glShaderSource(fragmentShaderObject, 1, (const GLchar**)&fragmentShaderSourceCode, NULL);
        
        //compile shader
        glCompileShader(fragmentShaderObject);
        
        iInfoLogLength = 0;
        iShaderCompileStatus = 0;
        szLogInfo = NULL;
        
        glGetShaderiv(fragmentShaderObject, GL_COMPILE_STATUS, &iShaderCompileStatus);
        
        if(iShaderCompileStatus == GL_FALSE)
        {
            glGetShaderiv(fragmentShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
            if(iInfoLogLength > 0)
            {
                szLogInfo = (char *)malloc(iInfoLogLength);
                
                if(szLogInfo != NULL)
                {
                    GLsizei written;
                    glGetShaderInfoLog(fragmentShaderObject, iInfoLogLength, &written, szLogInfo);
                    printf("Fragment Shader Compilation Log : %s \n", szLogInfo);
                    free(szLogInfo);
                    [self release];
                }
            }
        }
        
        //************************************ SHADER PROGRAM ***********************************
        //create
        shaderProgramObject = glCreateProgram();
        
        // attach shaders
        glAttachShader(shaderProgramObject, vertexShaderObject);
        glAttachShader(shaderProgramObject, fragmentShaderObject);
        
        glBindAttribLocation(shaderProgramObject, AMC_ATTRIBUTE_POSITION, "vPosition");
        glBindAttribLocation(shaderProgramObject, AMC_ATTRIBUTE_NORMAL, "vNormal");
        //link
        glLinkProgram(shaderProgramObject);
        
        GLint iShaderProgramLinkStatus = 0;
        
        glGetProgramiv(shaderProgramObject, GL_LINK_STATUS, &iShaderProgramLinkStatus);
        
        if(iShaderProgramLinkStatus == GL_FALSE)
        {
            glGetProgramiv(shaderProgramObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
            
            if(iInfoLogLength > 0)
            {
                szLogInfo = (char *)malloc(iInfoLogLength);
                if(szLogInfo != NULL)
                {
                    GLsizei written;
                    glGetProgramInfoLog(shaderProgramObject, iInfoLogLength, &written, szLogInfo);
                    printf("Shader Program Link Log : %s\n", szLogInfo);
                    free(szLogInfo);
                    [self release];
                }
            }
        }
        
        mvUniform = glGetUniformLocation(shaderProgramObject, "u_mv_matrix");
        pUniform = glGetUniformLocation(shaderProgramObject, "u_p_matrix");
        LdUniform = glGetUniformLocation(shaderProgramObject, "u_ld");
        KdUniform = glGetUniformLocation(shaderProgramObject, "u_kd");
        lightPositionUniform = glGetUniformLocation(shaderProgramObject, "u_light_position");
        keyPressedUniform = glGetUniformLocation(shaderProgramObject, "u_l_key_is_pressed");
        
        GLfloat cubeVertices[] = {
            1.0f, 1.0f, 1.0f, -1.0f, 1.0f, 1.0f, -1.0f, -1.0f, 1.0f, 1.0f, -1.0f, 1.0f,
            1.0f, 1.0f, -1.0f, 1.0f, 1.0f, 1.0f, 1.0f, -1.0f, 1.0f, 1.0f, -1.0f, -1.0f,
            1.0f, 1.0f, -1.0f, -1.0f, 1.0f, -1.0f, -1.0f, -1.0f, -1.0f, 1.0f, -1.0f, -1.0f,
            -1.0f, 1.0f, -1.0f,    -1.0f, 1.0f, 1.0f, -1.0f, -1.0f, 1.0f, -1.0f, -1.0f, -1.0f,
            1.0f, 1.0f, -1.0f, -1.0f, 1.0f, -1.0f, -1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f,
            1.0f, -1.0f, -1.0f, -1.0f, -1.0f, -1.0f, -1.0f, -1.0f, 1.0f, 1.0f, -1.0f, 1.0f
        };//front-right-back-left-top-bottom
        
        const GLfloat cubeNormal[] = {
            0.0f, 0.0f, 1.0f,0.0f, 0.0f, 1.0f,0.0f, 0.0f, 1.0f,0.0f, 0.0f, 1.0f,
            1.0f, 0.0f, 0.0f,1.0f, 0.0f, 0.0f,1.0f, 0.0f, 0.0f,1.0f, 0.0f, 0.0f,
            0.0f, 0.0f, -1.0f,0.0f, 0.0f, -1.0f,0.0f, 0.0f, -1.0f,0.0f, 0.0f, -1.0f,
            -1.0f, 0.0f, 0.0f,-1.0f, 0.0f, 0.0f,-1.0f, 0.0f, 0.0f,-1.0f, 0.0f, 0.0f,
            0.0f, 1.0f, 0.0f,0.0f, 1.0f, 0.0f,0.0f, 1.0f, 0.0f,0.0f, 1.0f, 0.0f,
            0.0f, -1.0f, 0.0f,0.0f, -1.0f, 0.0f,0.0f, -1.0f, 0.0f,0.0f, -1.0f, 0.0f,
        };
        
        //    *** CUBE ***
        glGenVertexArrays(1, &vao_cube);
        glBindVertexArray(vao_cube);
        //buffer for position
        glGenBuffers(1, &vbo_position_cube);
        glBindBuffer(GL_ARRAY_BUFFER, vbo_position_cube);
        glBufferData(GL_ARRAY_BUFFER, sizeof(cubeVertices), cubeVertices, GL_STATIC_DRAW);
        glVertexAttribPointer(AMC_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
        glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);
        glBindBuffer(GL_ARRAY_BUFFER, 0);
        
        //buffer for normal
        glGenBuffers(1, &vbo_normal_cube);
        glBindBuffer(GL_ARRAY_BUFFER, vbo_normal_cube);
        glBufferData(GL_ARRAY_BUFFER, sizeof(cubeNormal), cubeNormal, GL_STATIC_DRAW);
        glVertexAttribPointer(AMC_ATTRIBUTE_NORMAL, 3, GL_FLOAT, GL_FALSE, 0, NULL);
        glEnableVertexAttribArray(AMC_ATTRIBUTE_NORMAL);
        glBindBuffer(GL_ARRAY_BUFFER, 0);
        glBindVertexArray(0);
        
        startAnimation = NO;
        enableLighting = NO;
        
        glEnable(GL_DEPTH_TEST);
        glDepthFunc(GL_LEQUAL);
        
        // clear color
        glClearColor(0.0f, 0.0f, 0.0f, 1.0f);       //blue
        
        perspectiveProjectionMatrix = vmath::mat4::identity();
        
        //Gesture Recognition Code
        //Tap gesture code
        UITapGestureRecognizer *singleTapGestureRecognizer = [ [ UITapGestureRecognizer alloc ] initWithTarget:self action:@selector(onSingleTap:)] ;
        [singleTapGestureRecognizer setNumberOfTapsRequired:1]; // 1 touch for 1 fingure
        [singleTapGestureRecognizer setDelegate:self];
        [self addGestureRecognizer:singleTapGestureRecognizer];
        
        // Double Tap Gesture Code
        UITapGestureRecognizer *doubleTapGestureRecognizer = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(onDoubleTap:)];
        [doubleTapGestureRecognizer setNumberOfTapsRequired:2];
        [doubleTapGestureRecognizer setNumberOfTouchesRequired:1]; // 1 touch of fingure
        [doubleTapGestureRecognizer setDelegate:self];
        [self addGestureRecognizer:doubleTapGestureRecognizer];
        
        [singleTapGestureRecognizer requireGestureRecognizerToFail:doubleTapGestureRecognizer];
        
        //swipe gesture
        UISwipeGestureRecognizer *swipGestureRecognizer = [ [UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(onSwipe:) ] ;
        [self addGestureRecognizer:swipGestureRecognizer];
        
        // long Pres Gesture
        UILongPressGestureRecognizer *longPressGestureRecognizer = [ [UILongPressGestureRecognizer alloc]initWithTarget:self action:@selector(onLongPress:) ];
        
        [self addGestureRecognizer:longPressGestureRecognizer];
        
    }
    return(self);
}

/*
 // Only override drawRect: if you perform custom drawing.
 // An empty implementation adversely affects performance during animation.
 - (void)drawRect:(CGRect)rect
 {
 // Drawing code
 }
 */

//static method
+(Class)layerClass
{
    //code
    return([CAEAGLLayer class]);
}

-(void)drawView:(id)sender
{
    //code
    [EAGLContext setCurrentContext:eaglContext];
    
    glBindFramebuffer(GL_FRAMEBUFFER, defaultFramebuffer);
    
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);
    
    glUseProgram(shaderProgramObject);
    
    //drawing
    vmath::mat4 modelViewMatrix = vmath::mat4::identity();
    vmath::mat4 projectionMatrix = vmath::mat4::identity();
    vmath::mat4 translationMatrix = vmath::mat4::identity();
    vmath::mat4 rotationMatrix = vmath::mat4::identity();
    vmath::mat4 scaleMatrix = vmath::mat4::identity();
    
    translationMatrix = vmath::translate(0.0f, 0.0f, -6.0f);
    rotationMatrix =  vmath::rotate(angle_cube, angle_cube, angle_cube);
    scaleMatrix = vmath::scale(0.8f, 0.8f, 0.8f);
    modelViewMatrix = translationMatrix * rotationMatrix;
    projectionMatrix = perspectiveProjectionMatrix;
    
    if (enableLighting == YES)
    {
        glUniform1i(keyPressedUniform, 1);
        glUniform3f(LdUniform, 1.0, 1.0, 1.0);
        glUniform3f(KdUniform, 0.5, 0.5, 0.5);
        glUniform4f(lightPositionUniform, 10.0f, 10.0f, 10.0f, 1.0f);
    }
    else
    {
        glUniform1i(keyPressedUniform, 0);
    }
    //send necessary matrices to shader in respective uniforms
    glUniformMatrix4fv(mvUniform, 1, GL_FALSE, modelViewMatrix);
    glUniformMatrix4fv(pUniform, 1, GL_FALSE, projectionMatrix);
    
    // bind vao
    glBindVertexArray(vao_cube);
    
    // draw
    glDrawArrays(GL_TRIANGLE_FAN, 0, 4);
    glDrawArrays(GL_TRIANGLE_FAN, 4, 4);
    glDrawArrays(GL_TRIANGLE_FAN, 8, 4);
    glDrawArrays(GL_TRIANGLE_FAN, 12, 4);
    glDrawArrays(GL_TRIANGLE_FAN, 16, 4);
    glDrawArrays(GL_TRIANGLE_FAN, 20, 4);
    
    glBindVertexArray(0);
    
    glUseProgram(0);
    
    glBindRenderbuffer(GL_RENDERBUFFER, colorRenderbuffer);
    [eaglContext presentRenderbuffer:GL_RENDERBUFFER];          // swapbuffers
    
    if (startAnimation == YES)
    {
        if (angle_cube < 360.0f)
            angle_cube = angle_cube + 0.5f;
        else
            angle_cube = 0.0f;
    }
    else
    {
        angle_cube = 0.0f;
    }
}

-(void)layoutSubviews   //reshape
{
    //code
    GLint width;
    GLint height;
    
    glBindRenderbuffer(GL_RENDERBUFFER, colorRenderbuffer);
    [eaglContext renderbufferStorage:GL_RENDERBUFFER fromDrawable:(CAEAGLLayer*)self.layer];
    glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_WIDTH, &width);
    glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_HEIGHT, &height);
    glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, depthRenderbuffer);
    
    glViewport(0, 0, width, height);
    
    //ortho(l-r-b-t-n-f)
    GLfloat fwidth = (GLfloat)width;
    GLfloat fheight = (GLfloat)height;
    
    perspectiveProjectionMatrix = vmath::perspective(45.0f, fwidth/fheight, 0.1f, 100.0f);
    
    if(glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
    {
        printf("Failed to Create Complete ramebuffer Object %x \n", glCheckFramebufferStatus(GL_FRAMEBUFFER));
    }
    [self drawView:nil];//repaint like warmup style
}

-(void)startAnimation
{
    if(!isAnimating)
    {
        displaylink = [NSClassFromString(@"CADisplayLink") displayLinkWithTarget:self selector:@selector(drawView:)];
        [displaylink setPreferredFramesPerSecond:animationFrameInterval];
        [displaylink addToRunLoop:[NSRunLoop currentRunLoop] forMode:NSDefaultRunLoopMode];
        
        isAnimating = YES;
    }
}

-(void)stopAnimation
{
    if(isAnimating)
    {
        [displaylink invalidate];
        displaylink=nil;
        
        isAnimating = NO;
    }
}

-(BOOL) acceptsFirstResponder
{
    return YES;
}

-(void)touchesBegane :(NSSet *)touches withEvent:(UIEvent *)UIEvent
{
    
}


-(void) onLongPress: (UIGestureRecognizer *)gesture
{
    
}

-(void) onSwipe: (UIGestureRecognizer *)gesture
{
    [self release];
    
    exit(0);
}

-(void) onDoubleTap: (UITapGestureRecognizer *)gesture
{
    if(startAnimation == NO)
        startAnimation = YES;
    else
        startAnimation = NO;
}

-(void) onSingleTap: (UITapGestureRecognizer *)gesture
{
    if(enableLighting == NO)
        enableLighting = YES;
    else
        enableLighting = NO;
}

- (void)dealloc
{
    if (vbo_normal_cube)
    {
        glDeleteBuffers(1, &vbo_normal_cube);
        vbo_normal_cube = 0;
    }
    if (vbo_position_cube)
    {
        glDeleteBuffers(1, &vbo_position_cube);
        vbo_position_cube = 0;
    }
    if (vao_cube)
    {
        glDeleteVertexArrays(1, &vao_cube);
        vao_cube = 0;
    }
    
    glDetachShader(shaderProgramObject, vertexShaderObject);
    glDetachShader(shaderProgramObject, fragmentShaderObject);
    
    glDeleteShader(vertexShaderObject);
    vertexShaderObject = 0;
    
    glDeleteShader(fragmentShaderObject);
    fragmentShaderObject = 0;
    
    glDeleteProgram(shaderProgramObject);
    shaderProgramObject = 0;
    
    if(depthRenderbuffer)
    {
        glDeleteRenderbuffers(1, &depthRenderbuffer);
        depthRenderbuffer=0;
    }
    
    if(colorRenderbuffer)
    {
        glDeleteRenderbuffers(1, &colorRenderbuffer);
        colorRenderbuffer=0;
    }
    
    if(defaultFramebuffer)
    {
        glDeleteFramebuffers(1, &defaultFramebuffer);
        defaultFramebuffer=0;
    }
    
    if([EAGLContext currentContext] == eaglContext)
    {
        [EAGLContext setCurrentContext:nil];
    }
    [eaglContext release];
    eaglContext =nil;
    
    [super dealloc];
}

@end
