#import <OpenGLES/ES3/gl.h>
#import <OpenGLES/ES3/glext.h>

#import "vmath.h"
#import "ps_sphere.h"
#import "GLESView.h"

enum
{
    AMC_ATTRIBUTE_POSITION = 0,
    AMC_ATTRIBUTE_COLOR,
    AMC_ATTRIBUTE_NORMAL,
    AMC_ATTRIBUTE_TEXTURE0
};

@implementation GLESView
{
    EAGLContext *eaglContext;
    Sphere *mySphere;
    
    GLuint defaultFramebuffer;
    GLuint colorRenderbuffer;
    GLuint depthRenderbuffer;
    
    id displaylink;
    NSInteger animationFrameInterval;
    BOOL isAnimating;
    
    GLuint vertexShaderObject_v;
    GLuint fragmentShaderObject_v;
    GLuint shaderProgramObject_v;
    
    GLuint vertexShaderObject_f;
    GLuint fragmentShaderObject_f;
    GLuint shaderProgramObject_f;

    GLuint vao_sphere;
    GLuint vbo_sphere_elements;
    GLuint vbo_sphere_position;
    GLuint vbo_sphere_normals;
    
    float sphere_vertices[1146];
    float sphere_normals[1146];
    float sphere_textures[764];
    unsigned short sphere_elements[2280];
    
    int iNumVertices;
    int iNumElements;
    
    GLfloat angle_sphere;
    
    GLuint mUniform_v, vUniform_v, pUniform_v;    // Model Matrix Uniform, view Matrix Uniform and Projection Matrix Uniform
    GLuint LdUniform_v, KdUniform_v;            // Diffuse property of Light and diffuse property of material
    GLuint LaUniform_v, KaUniform_v;            // Ambient property of light and material
    GLuint LsUniform_v, KsUniform_v;            // Specular property of light and material
    GLuint lightPositionUniform_v;            // light position
    GLuint keyPressedUniform_v;                // is L key Pressed, to enable lighting effect
    GLuint materialShininessUniform_v;
    
    GLuint mUniform_f, vUniform_f, pUniform_f;    // Model Matrix Uniform, view Matrix Uniform and Projection Matrix Uniform
    GLuint LdUniform_f, KdUniform_f;            // Diffuse property of Light and diffuse property of material
    GLuint LaUniform_f, KaUniform_f;            // Ambient property of light and material
    GLuint LsUniform_f, KsUniform_f;            // Specular property of light and material
    GLuint lightPositionUniform_f;            // light position
    GLuint keyPressedUniform_f;                // is L key Pressed, to enable lighting effect
    GLuint materialShininessUniform_f;
    
    GLfloat lightDiffuse[4];
    GLfloat lightAmbient[4];
    GLfloat lightSpecular[4];
    GLfloat lightPosition[4];
    
    int axis;
    float theta;
    float gWidth;
    float gHeight;
    
    struct SphereProperty
    {
        vmath::vec3 spherePosition;
        vmath::vec4 MDiffuse;
        vmath::vec4 MAmbient;
        vmath::vec4 MSpecular;
        int SViewport[4];
        float sphereShininess;
    }Spheres[24];
    
    
    BOOL startAnimation;
    BOOL enableLighting;
    BOOL toggleLightingType;
    
    vmath::mat4 perspectiveProjectionMatrix;
}

-(id) initWithFrame:(CGRect)frameRect
{
    //code
    self = [super initWithFrame:frameRect];
    
    if (self)
    {
        CAEAGLLayer *eaglLayer = (CAEAGLLayer *)super.layer;
        
        eaglLayer.opaque = YES;
        eaglLayer.drawableProperties = [NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithBool:FALSE],
                                        kEAGLDrawablePropertyRetainedBacking, kEAGLColorFormatRGBA8, kEAGLDrawablePropertyColorFormat, nil];
        
        eaglContext=[[EAGLContext alloc]initWithAPI:kEAGLRenderingAPIOpenGLES3];
        
        if(eaglContext==nil)
        {
            [self release];
            return(nil);
        }
        
        [EAGLContext setCurrentContext:eaglContext];
        
        glGenFramebuffers(1, &defaultFramebuffer);
        glGenRenderbuffers(1, &colorRenderbuffer);
        glBindFramebuffer(GL_FRAMEBUFFER, defaultFramebuffer);
        glBindRenderbuffer(GL_RENDERBUFFER, colorRenderbuffer);
        
        [eaglContext renderbufferStorage:GL_RENDERBUFFER fromDrawable:eaglLayer];
        
        glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_RENDERBUFFER, colorRenderbuffer);
        
        GLint backingWidth;
        GLint backingHeight;
        
        glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_WIDTH, &backingWidth);
        glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_WIDTH, &backingHeight);
        
        glGenRenderbuffers(1, &depthRenderbuffer);
        glBindRenderbuffer(GL_RENDERBUFFER, depthRenderbuffer);
        glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT16, backingWidth, backingHeight);
        glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, depthRenderbuffer);
        
        if(glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
        {
            printf("Failed to Create Complete ramebuffer Object %x \n", glCheckFramebufferStatus(GL_FRAMEBUFFER));
            glDeleteFramebuffers(1, &defaultFramebuffer);
            glDeleteRenderbuffers(1, &colorRenderbuffer);
            glDeleteRenderbuffers(1, &depthRenderbuffer);
            
            return(nil);
        }
        
        printf("Renderer : %s | GL Version : %s | GLSL Version : %s\n",
               glGetString(GL_RENDERER), glGetString(GL_VERSION), glGetString(GL_SHADING_LANGUAGE_VERSION));
        
        // hardcore initializations
        isAnimating = NO;
        animationFrameInterval = 60;    //default since iOS 8.2
        
        //PER VERTEX LIGHTING - vertex shader
        //create shader
        vertexShaderObject_v = glCreateShader(GL_VERTEX_SHADER);
        
        const GLchar *vertexShaderSourceCode_v =
        "   #version 300 es                                " \
        "                                           \n  " \
        "    in vec4 vPosition;                                                                  " \
        "    in vec3 vNormal;                                                                    " \
        "                                                                                      \n" \
        "    uniform mat4 u_m_matrix;                                                            " \
        "    uniform mat4 u_v_matrix;                                                            " \
        "    uniform mat4 u_p_matrix;                                                            " \
        "    uniform mediump int u_l_key_is_pressed;                                                        " \
        "    uniform vec3 u_ld;                                                                    " \
        "    uniform vec3 u_kd;                                                                    " \
        "    uniform vec3 u_la;                                                                    " \
        "    uniform vec3 u_ka;                                                                    " \
        "    uniform vec3 u_ls;                                                                    " \
        "    uniform vec3 u_ks;                                                                    " \
        "    uniform vec4 u_light_position;                                                        " \
        "    uniform float u_material_shininess;                                                    " \
        "                                                                                      \n" \
        "    out vec3 phong_ads_light;                                                            " \
        "                                                                                      \n" \
        "    void main(void)                                                                        " \
        "    {                                                                                    " \
        "        if(u_l_key_is_pressed == 1)                                                        " \
        "        {                                                                                " \
        "            vec4 eyeCoordinates = u_v_matrix * u_m_matrix * vPosition;                    " \
        "            vec3 tNorm = normalize(mat3(u_v_matrix * u_m_matrix) * vNormal);            " \
        "            vec3 lightDirection = normalize(vec3(u_light_position - eyeCoordinates));    " \
        "            float tnDotld = max(dot(lightDirection, tNorm), 0.0);                        " \
        "            vec3 reflectionVector = reflect(-lightDirection, tNorm);                    " \
        "            vec3 viewerVector = normalize(vec3(-eyeCoordinates));                        " \
        "            float rvDotvv = max(dot(reflectionVector, viewerVector), 0.0);                " \
        "                                                                                      \n" \
        "            vec3 ambient = u_la * u_ka;                                                    " \
        "            vec3 diffuse = u_ld * u_kd * tnDotld;                                        " \
        "            vec3 specular = u_ls * u_ks * pow(rvDotvv, u_material_shininess);            " \
        "                                                                                      \n" \
        "            phong_ads_light = ambient + diffuse + specular;                                " \
        "        }                                                                                " \
        "        else                                                                            " \
        "        {                                                                                " \
        "            phong_ads_light = vec3(1.0, 1.0, 1.0);                                        " \
        "        }                                                                                " \
        "                                                                                      \n" \
        "    gl_Position = u_v_matrix * u_p_matrix * u_m_matrix * vPosition;                        " \
        "    }                                                                                    ";
        
        glShaderSource(vertexShaderObject_v, 1, (const GLchar**)&vertexShaderSourceCode_v, NULL);
        
        // compile shader
        GLint iInfoLogLength =0;
        GLint iShaderCompileStatus = 0;
        char *szInfoLog = NULL;
        
        glCompileShader(vertexShaderObject_v);
        glGetShaderiv(vertexShaderObject_v, GL_COMPILE_STATUS, &iShaderCompileStatus);
        if (iShaderCompileStatus == GL_FALSE)
        {
            glGetShaderiv(vertexShaderObject_v, GL_INFO_LOG_LENGTH, &iInfoLogLength);
            if (iInfoLogLength > 0)
            {
                szInfoLog = (GLchar *)malloc(iInfoLogLength);
                if (szInfoLog != NULL)
                {
                    GLsizei written;
                    glGetShaderInfoLog(vertexShaderObject_v, iInfoLogLength, &written, szInfoLog);
                    printf("VERTEX SHADER COMPILATION LOG : %s\n", szInfoLog);
                    free(szInfoLog);
                    [self release];
                }
            }
        }
        
        //PER VERTEX LIGHTING - fragment shader
        fragmentShaderObject_v = glCreateShader(GL_FRAGMENT_SHADER);
        
        // source code for fragment shader
        const GLchar* fragmentShaderSourceCode_v =
        "     #version 300 es                                                                             " \
        "     \n                                                                                        " \
        "   precision highp float;  " \
        "    in vec3 phong_ads_light;                                                                " \
        "    out vec4 fragColor;                                                                        " \
        "                                                                                        \n    " \
        "    void main(void)                                                                            " \
        "    {                                                                                        " \
        "        fragColor = vec4(phong_ads_light, 1.0);                                                " \
        "    }                                                                                        ";
        
        glShaderSource(fragmentShaderObject_v, 1, (const GLchar**)&fragmentShaderSourceCode_v, NULL);
        
        // compile shader
        iInfoLogLength =0;
        iShaderCompileStatus = 0;
        szInfoLog = NULL;
        
        glCompileShader(fragmentShaderObject_v);
        glGetShaderiv(fragmentShaderObject_v, GL_COMPILE_STATUS, &iShaderCompileStatus);
        if (iShaderCompileStatus == GL_FALSE)
        {
            glGetShaderiv(fragmentShaderObject_v, GL_INFO_LOG_LENGTH, &iInfoLogLength);
            if (iInfoLogLength > 0)
            {
                szInfoLog = (GLchar *)malloc(iInfoLogLength);
                if (szInfoLog != NULL)
                {
                    GLsizei written;
                    glGetShaderInfoLog(fragmentShaderObject_v, iInfoLogLength, &written, szInfoLog);
                    printf("FRAGMENT SHADER COMPILATION LOG : %s\n", szInfoLog);
                    free(szInfoLog);
                    [self release];
                }
            }
        }
        //PER VERTEX LIGHTING - shader program
        //create program object
        shaderProgramObject_v = glCreateProgram();
        
        //attach shaders to it
        glAttachShader(shaderProgramObject_v, vertexShaderObject_v);
        glAttachShader(shaderProgramObject_v, fragmentShaderObject_v);
        
        // pre-linking binding of shader program object with vertex shader position attribute
        //(not required when used layout qualifier with location in shader, writing here for legacy of assignments)
        glBindAttribLocation(shaderProgramObject_v, AMC_ATTRIBUTE_POSITION, "vPosition");
        glBindAttribLocation(shaderProgramObject_v, AMC_ATTRIBUTE_NORMAL, "vNormal");
        
        // link shader program object
        glLinkProgram(shaderProgramObject_v);
        
        //error checking
        GLint iProgramLinkStatus = 0;
        iInfoLogLength = 0;
        szInfoLog = NULL;
        
        glGetProgramiv(shaderProgramObject_v, GL_LINK_STATUS, &iProgramLinkStatus);
        if (iProgramLinkStatus == GL_FALSE)
        {
            glGetProgramiv(shaderProgramObject_v, GL_INFO_LOG_LENGTH, &iInfoLogLength);
            if (iInfoLogLength > 0)
            {
                szInfoLog = (GLchar *)malloc(iInfoLogLength);
                if (szInfoLog != NULL)
                {
                    GLint written;
                    glGetProgramInfoLog(shaderProgramObject_v, iInfoLogLength, &written, szInfoLog);
                    printf("SHADER PROGRAM LINK LOG : %s\n", szInfoLog);
                    free(szInfoLog);
                    [self release];
                }
            }
        }
        
        // get MVP uniform location
        //postlinking retieving uniform location
        vUniform_v = glGetUniformLocation(shaderProgramObject_v, "u_v_matrix");
        mUniform_v = glGetUniformLocation(shaderProgramObject_v, "u_m_matrix");
        pUniform_v = glGetUniformLocation(shaderProgramObject_v, "u_p_matrix");
        LdUniform_v = glGetUniformLocation(shaderProgramObject_v, "u_ld");
        KdUniform_v = glGetUniformLocation(shaderProgramObject_v, "u_kd");
        LaUniform_v = glGetUniformLocation(shaderProgramObject_v, "u_la");
        KaUniform_v = glGetUniformLocation(shaderProgramObject_v, "u_ka");
        LsUniform_v = glGetUniformLocation(shaderProgramObject_v, "u_ls");
        KsUniform_v = glGetUniformLocation(shaderProgramObject_v, "u_ks");
        materialShininessUniform_v = glGetUniformLocation(shaderProgramObject_v, "u_material_shininess");
        lightPositionUniform_v = glGetUniformLocation(shaderProgramObject_v, "u_light_position");
        keyPressedUniform_v = glGetUniformLocation(shaderProgramObject_v, "u_l_key_is_pressed");
        
        // *********************** VERTEX SHADER ***************************
        vertexShaderObject_f = glCreateShader(GL_VERTEX_SHADER);
        
        const GLchar* vertexShaderSourceCode_f =
        "   #version 300 es                                " \
        "                                           \n  " \
        "    in vec4 vPosition;                                                                  " \
        "    in vec3 vNormal;                                                                    " \
        "                                                                                      \n" \
        "    out vec3 tNorm;                                                                        " \
        "    out vec3 lightDirection;                                                            " \
        "    out vec3 viewerVector;                                                                " \
        "                                                                                      \n" \
        "    uniform mat4 u_m_matrix;                                                            " \
        "    uniform mat4 u_v_matrix;                                                            " \
        "    uniform mat4 u_p_matrix;                                                            " \
        "    uniform mediump int u_l_key_is_pressed;                                                        " \
        "    uniform vec4 u_light_position;                                                        " \
        "                                                                                      \n" \
        "    void main(void)                                                                        " \
        "    {                                                                                    " \
        "        if(u_l_key_is_pressed == 1)                                                        " \
        "        {                                                                                " \
        "            vec4 eyeCoordinates = u_v_matrix * u_m_matrix * vPosition;                          " \
        "            tNorm = mat3(u_v_matrix * u_m_matrix) * vNormal;                               " \
        "            lightDirection = vec3(u_light_position - eyeCoordinates);                      " \
        "            viewerVector = vec3(-eyeCoordinates);                                     " \
        "        }                                                                                " \
        "                                                                                      \n" \
        "    gl_Position = u_v_matrix * u_p_matrix * u_m_matrix * vPosition;                        " \
        "    }                                                                                    ";
        
        glShaderSource(vertexShaderObject_f, 1, (const GLchar**) &vertexShaderSourceCode_f, NULL);
        
        //compile shader
        glCompileShader(vertexShaderObject_f);
        
        iInfoLogLength = 0;
        iShaderCompileStatus = 0;
        szInfoLog = NULL;
        
        glGetShaderiv(vertexShaderObject_f, GL_COMPILE_STATUS, &iShaderCompileStatus);
        
        if(iShaderCompileStatus == GL_FALSE)
        {
            glGetShaderiv(vertexShaderObject_f, GL_INFO_LOG_LENGTH, &iInfoLogLength);
            if(iInfoLogLength > 0)
            {
                szInfoLog = (char *)malloc(iInfoLogLength);
                
                if(szInfoLog != NULL)
                {
                    GLsizei written;
                    glGetShaderInfoLog(vertexShaderObject_f, iInfoLogLength, &written, szInfoLog);
                    printf("Vertex Shader Compilation Log : %s \n", szInfoLog);
                    free(szInfoLog);
                    [self release];
                }
            }
        }
        
        //****************************** FRAGMENT SHADER *********************************
        fragmentShaderObject_f = glCreateShader(GL_FRAGMENT_SHADER);
        
        const GLchar* fragmentShaderSourceCode_f =
        "#version 300 es                " \
        "                           \n  " \
        "   precision highp float;     \n  " \
        "    in vec3 tNorm;                                                                            " \
        "    in vec3 lightDirection;                                                                    " \
        "    in vec3 viewerVector;                                                                    " \
        "                                                                                          \n" \
        "    out vec4 fragColor;                                                                        " \
        "                                                                                          \n" \
        "    uniform vec3 u_ld;                                                                        " \
        "    uniform vec3 u_kd;                                                                        " \
        "    uniform vec3 u_la;                                                                        " \
        "    uniform vec3 u_ka;                                                                        " \
        "    uniform vec3 u_ls;                                                                        " \
        "    uniform vec3 u_ks;                                                                        " \
        "    uniform float u_material_shininess;                                                        " \
        "    uniform int u_l_key_is_pressed;                                                            " \
        "                                                                                          \n" \
        "    void main(void)                                                                            " \
        "    {                                                                                        " \
        "        vec3 phong_ads_light;                                                                " \
        "        if(u_l_key_is_pressed == 1)                                                            " \
        "        {                                                                                    " \
        "            vec3 normalizedtNorm = normalize(tNorm);                                        " \
        "            vec3 normalizedlightDirection = normalize(lightDirection);                        " \
        "            vec3 normalizedviewerVector = normalize(viewerVector);                            " \
        "                                                                                          \n" \
        "            vec3 reflectionVector = normalize(reflect(-normalizedlightDirection, normalizedtNorm));" \
        "            float tnDotld = max(dot(normalizedlightDirection, normalizedtNorm), 0.0);        " \
        "            float rvDotvv = max(dot(reflectionVector, normalizedviewerVector), 0.0);        " \
        "                                                                                          \n" \
        "            vec3 ambient = u_la * u_ka;                                                        " \
        "            vec3 diffuse = u_ld * u_kd * tnDotld;                                            " \
        "            vec3 specular = u_ls * u_ks * pow(rvDotvv, u_material_shininess);                " \
        "            phong_ads_light = ambient + diffuse + specular;                                    " \
        "                                                                                          \n" \
        "            fragColor = vec4(phong_ads_light, 1.0);                                            " \
        "        }                                                                                    " \
        "        else                                                                                " \
        "        {                                                                                    " \
        "            phong_ads_light = vec3(1.0, 1.0, 1.0);                                            " \
        "            fragColor = vec4(phong_ads_light, 1.0);                                            " \
        "        }                                                                                    " \
        "    }                                                                                        ";
        
        glShaderSource(fragmentShaderObject_f, 1, (const GLchar**)&fragmentShaderSourceCode_f, NULL);
        
        //compile shader
        glCompileShader(fragmentShaderObject_f);
        
        iInfoLogLength = 0;
        iShaderCompileStatus = 0;
        szInfoLog = NULL;
        
        glGetShaderiv(fragmentShaderObject_f, GL_COMPILE_STATUS, &iShaderCompileStatus);
        
        if(iShaderCompileStatus == GL_FALSE)
        {
            glGetShaderiv(fragmentShaderObject_f, GL_INFO_LOG_LENGTH, &iInfoLogLength);
            if(iInfoLogLength > 0)
            {
                szInfoLog = (char *)malloc(iInfoLogLength);
                
                if(szInfoLog != NULL)
                {
                    GLsizei written;
                    glGetShaderInfoLog(fragmentShaderObject_f, iInfoLogLength, &written, szInfoLog);
                    printf("Fragment Shader Compilation Log : %s \n", szInfoLog);
                    free(szInfoLog);
                    [self release];
                }
            }
        }
        
        //************************************ SHADER PROGRAM ***********************************
        //create
        shaderProgramObject_f = glCreateProgram();
        
        // attach shaders
        glAttachShader(shaderProgramObject_f, vertexShaderObject_f);
        glAttachShader(shaderProgramObject_f, fragmentShaderObject_f);
        
        glBindAttribLocation(shaderProgramObject_f, AMC_ATTRIBUTE_POSITION, "vPosition");
        glBindAttribLocation(shaderProgramObject_f, AMC_ATTRIBUTE_NORMAL, "vNormal");
        //link
        glLinkProgram(shaderProgramObject_f);
        
        GLint iShaderProgramLinkStatus = 0;
        
        glGetProgramiv(shaderProgramObject_f, GL_LINK_STATUS, &iShaderProgramLinkStatus);
        
        if(iShaderProgramLinkStatus == GL_FALSE)
        {
            glGetProgramiv(shaderProgramObject_f, GL_INFO_LOG_LENGTH, &iInfoLogLength);
            
            if(iInfoLogLength > 0)
            {
                szInfoLog = (char *)malloc(iInfoLogLength);
                if(szInfoLog != NULL)
                {
                    GLsizei written;
                    glGetProgramInfoLog(shaderProgramObject_f, iInfoLogLength, &written, szInfoLog);
                    printf("Shader Program Link Log : %s\n", szInfoLog);
                    free(szInfoLog);
                    [self release];
                }
            }
        }
        
        //postlinking retieving uniform location
        vUniform_f = glGetUniformLocation(shaderProgramObject_f, "u_v_matrix");
        mUniform_f = glGetUniformLocation(shaderProgramObject_f, "u_m_matrix");
        pUniform_f = glGetUniformLocation(shaderProgramObject_f, "u_p_matrix");
        LdUniform_f = glGetUniformLocation(shaderProgramObject_f, "u_ld");
        KdUniform_f = glGetUniformLocation(shaderProgramObject_f, "u_kd");
        LaUniform_f = glGetUniformLocation(shaderProgramObject_f, "u_la");
        KaUniform_f = glGetUniformLocation(shaderProgramObject_f, "u_ka");
        LsUniform_f = glGetUniformLocation(shaderProgramObject_f, "u_ls");
        KsUniform_f = glGetUniformLocation(shaderProgramObject_f, "u_ks");
        materialShininessUniform_f = glGetUniformLocation(shaderProgramObject_f, "u_material_shininess");
        lightPositionUniform_f = glGetUniformLocation(shaderProgramObject_f, "u_light_position");
        keyPressedUniform_f = glGetUniformLocation(shaderProgramObject_f, "u_l_key_is_pressed");
        
        // sphere data
        mySphere = [[Sphere alloc]init];
        
        [mySphere getSphereVertexDataWithPosition:sphere_vertices withNormals:sphere_normals withTexCoords:sphere_textures andElements:sphere_elements];
        
        iNumVertices = [mySphere getNumberOfSphereVertices];
        iNumElements = [mySphere getNumberOfSphereElements];
        
        glGenVertexArrays(1,&vao_sphere);
        glBindVertexArray(vao_sphere);
        
        // position vbo
        glGenBuffers(1, &vbo_sphere_position);
        glBindBuffer(GL_ARRAY_BUFFER, vbo_sphere_position);
        glBufferData(GL_ARRAY_BUFFER, sizeof(sphere_vertices), sphere_vertices, GL_STATIC_DRAW);
        glVertexAttribPointer(AMC_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
        glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);
        glBindBuffer(GL_ARRAY_BUFFER, 0);
        
        // normal vbo
        glGenBuffers(1, &vbo_sphere_normals);
        glBindBuffer(GL_ARRAY_BUFFER, vbo_sphere_normals);
        glBufferData(GL_ARRAY_BUFFER, sizeof(sphere_normals), sphere_normals, GL_STATIC_DRAW);
        glVertexAttribPointer(AMC_ATTRIBUTE_NORMAL, 3, GL_FLOAT, GL_FALSE, 0, NULL);
        glEnableVertexAttribArray(AMC_ATTRIBUTE_NORMAL);
        glBindBuffer(GL_ARRAY_BUFFER, 0);
        
        // elements vbo
        glGenBuffers(1, &vbo_sphere_elements);
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_elements);
        glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(sphere_elements), sphere_elements, GL_STATIC_DRAW);
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
        
        glBindVertexArray(0);
        
        // light and material properties
        //light ambient
        lightAmbient[0] = 0.0f;
        lightAmbient[1] = 0.0f;
        lightAmbient[2] = 0.0f;
        lightAmbient[3] = 0.0f;
        
        // light diffuse
        lightDiffuse[0] = 1.0f;
        lightDiffuse[1] = 1.0f;
        lightDiffuse[2] = 1.0f;
        lightDiffuse[3] = 1.0f;
        
        //light specular
        lightSpecular[0] = 1.0f;
        lightSpecular[1] = 1.0f;
        lightSpecular[2] = 1.0f;
        lightSpecular[3] = 1.0f;
        
        // light position
        lightPosition[0] = 100.0f;
        lightPosition[1] = 100.0f;
        lightPosition[2] = 100.0f;
        lightPosition[3] = 1.0f;
        
        axis = 0;
        theta = 0.0f;
        
        startAnimation = NO;
        enableLighting = NO;
        
        glEnable(GL_DEPTH_TEST);
        glDepthFunc(GL_LEQUAL);
        
        // clear color
        glClearColor(0.0f, 0.0f, 0.0f, 1.0f);       //blue
        
        perspectiveProjectionMatrix = vmath::mat4::identity();
        
        //Gesture Recognition Code
        //Tap gesture code
        UITapGestureRecognizer *singleTapGestureRecognizer = [ [ UITapGestureRecognizer alloc ] initWithTarget:self action:@selector(onSingleTap:)] ;
        [singleTapGestureRecognizer setNumberOfTapsRequired:1]; // 1 touch for 1 fingure
        [singleTapGestureRecognizer setDelegate:self];
        [self addGestureRecognizer:singleTapGestureRecognizer];
        
        // Double Tap Gesture Code
        UITapGestureRecognizer *doubleTapGestureRecognizer = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(onDoubleTap:)];
        [doubleTapGestureRecognizer setNumberOfTapsRequired:2];
        [doubleTapGestureRecognizer setNumberOfTouchesRequired:1]; // 1 touch of fingure
        [doubleTapGestureRecognizer setDelegate:self];
        [self addGestureRecognizer:doubleTapGestureRecognizer];
        
        [singleTapGestureRecognizer requireGestureRecognizerToFail:doubleTapGestureRecognizer];
        
        //swipe gesture
        UISwipeGestureRecognizer *swipGestureRecognizer = [ [UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(onSwipe:) ] ;
        [self addGestureRecognizer:swipGestureRecognizer];
        
        // long Pres Gesture
        UILongPressGestureRecognizer *longPressGestureRecognizer = [ [UILongPressGestureRecognizer alloc]initWithTarget:self action:@selector(onLongPress:) ];
        
        [self addGestureRecognizer:longPressGestureRecognizer];
        
    }
    return(self);
}

/*
 // Only override drawRect: if you perform custom drawing.
 // An empty implementation adversely affects performance during animation.
 - (void)drawRect:(CGRect)rect
 {
 // Drawing code
 }
 */

//static method
+(Class)layerClass
{
    //code
    return([CAEAGLLayer class]);
}

-(void)drawView:(id)sender
{
    //code
    [EAGLContext setCurrentContext:eaglContext];
    
    glBindFramebuffer(GL_FRAMEBUFFER, defaultFramebuffer);
    
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);
    
    for(int i = 0; i< 24; i++)
    {
        vmath::mat4 modelMatrix = vmath::mat4::identity();
        vmath::mat4 viewMatrix = vmath::mat4::identity();
        vmath::mat4 projectionMatrix = vmath::mat4::identity();
        vmath::mat4 translationMatrix = vmath::mat4::identity();
        vmath::mat4 rotationMatrix = vmath::mat4::identity();
        vmath::mat4 scaleMatrix = vmath::mat4::identity();
        
        scaleMatrix = vmath::scale(0.5f,0.5f,0.5f);
        translationMatrix =  vmath::translate(Spheres[i].spherePosition);
        modelMatrix = translationMatrix * scaleMatrix * rotationMatrix;
        projectionMatrix = projectionMatrix * perspectiveProjectionMatrix;
        
        if(toggleLightingType == NO)
        {
            glUseProgram(shaderProgramObject_v);
            
            if (enableLighting == YES)
            {
                if (axis == 3)
                {
                    lightPosition[0] = 15 *cosf(theta);
                    lightPosition[1] = 15 * sinf(theta);
                    lightPosition[2] = 0.0f;
                }
                if (axis == 2)
                {
                    lightPosition[0] = 15 * cosf(theta);
                    lightPosition[1] = 0.0f;
                    lightPosition[2] = 15 * sinf(theta);
                }
                if (axis == 1)
                {
                    lightPosition[0] = 0.0f;
                    lightPosition[1] = 15 * cosf(theta);
                    lightPosition[2] = 15 * sinf(theta);
                }
                
                glUniform1i(keyPressedUniform_v, 1);
                glUniform3fv(LaUniform_v, 1, lightAmbient);
                glUniform3fv(LdUniform_v, 1, lightDiffuse);
                glUniform3fv(LsUniform_v, 1, lightSpecular);
                
                glUniform3fv(KaUniform_v, 1, Spheres[i].MAmbient);
                glUniform3fv(KdUniform_v, 1, Spheres[i].MDiffuse);
                glUniform3fv(KsUniform_v, 1, Spheres[i].MSpecular);
                
                glUniform1f(materialShininessUniform_v, Spheres[i].sphereShininess);
                
                glUniform4fv(lightPositionUniform_v, 1, lightPosition);
            }
            else
            {
                glUniform1i(keyPressedUniform_v, 0);
            }
            //send necessary matrices to shader in respective uniforms
            glUniformMatrix4fv(mUniform_v, 1, GL_FALSE, modelMatrix);
            glUniformMatrix4fv(vUniform_v, 1, GL_FALSE, viewMatrix);
            glUniformMatrix4fv(pUniform_v, 1, GL_FALSE, projectionMatrix);
            
            //set viewport
            glViewport((GLint)Spheres[i].SViewport[0], (GLint)Spheres[i].SViewport[1], (GLsizei)Spheres[i].SViewport[2], (GLsizei)Spheres[i].SViewport[3]);
            
            // draw sphere
            glBindVertexArray(vao_sphere);
            glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_elements);
            glDrawElements(GL_TRIANGLES, iNumElements, GL_UNSIGNED_SHORT, 0);
            glBindVertexArray(0);
            
            glUseProgram(0);
        }
        else
        {
            glUseProgram(shaderProgramObject_f);
            
            if (enableLighting == YES)
            {
                // calculate position of rotating lights
                if (axis == 3)    // z
                {
                    lightPosition[0] = 15 * cosf(theta);
                    lightPosition[1] = 15 * sinf(theta);
                    lightPosition[2] = 0.0f;
                }
                if (axis == 2)    // y
                {
                    lightPosition[0] = 15 * cosf(theta);
                    lightPosition[1] = 0.0f;
                    lightPosition[2] = 15 * sinf(theta);
                }
                if (axis == 1)    // x
                {
                    lightPosition[0] = 0.0f;
                    lightPosition[1] = 15 * cosf(theta);
                    lightPosition[2] = 15 * sinf(theta);
                }
                
                glUniform1i(keyPressedUniform_f, 1);
                glUniform3fv(LaUniform_f, 1, lightAmbient);
                glUniform3fv(LdUniform_f, 1, lightDiffuse);
                glUniform3fv(LsUniform_f, 1, lightSpecular);
                
                glUniform3fv(KaUniform_f, 1, Spheres[i].MAmbient);
                glUniform3fv(KdUniform_f, 1, Spheres[i].MDiffuse);
                glUniform3fv(KsUniform_f, 1, Spheres[i].MSpecular);
                
                glUniform1f(materialShininessUniform_f, Spheres[i].sphereShininess);
                
                glUniform4fv(lightPositionUniform_f, 1, lightPosition);
            }
            else
            {
                glUniform1i(keyPressedUniform_f, 0);
            }
            //send necessary matrices to shader in respective uniforms
            glUniformMatrix4fv(mUniform_f, 1, GL_FALSE, modelMatrix);
            glUniformMatrix4fv(vUniform_f, 1, GL_FALSE, viewMatrix);
            glUniformMatrix4fv(pUniform_f, 1, GL_FALSE, projectionMatrix);
            
            //set viewport
            glViewport((GLint)Spheres[i].SViewport[0], (GLint)Spheres[i].SViewport[1], (GLsizei)Spheres[i].SViewport[2], (GLsizei)Spheres[i].SViewport[3]);
            
            // draw sphere
            glBindVertexArray(vao_sphere);
            glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_elements);
            glDrawElements(GL_TRIANGLES, iNumElements, GL_UNSIGNED_SHORT, 0);
            glBindVertexArray(0);
            
            glUseProgram(0);
        }
    }
    
    glBindRenderbuffer(GL_RENDERBUFFER, colorRenderbuffer);
    [eaglContext presentRenderbuffer:GL_RENDERBUFFER];          // swapbuffers
    
    theta = theta + 0.05;
    if(theta > 360.0f)
        theta = 0.0f;
}

-(void)layoutSubviews   //reshape
{
    //code
    GLint width;
    GLint height;
    
    glBindRenderbuffer(GL_RENDERBUFFER, colorRenderbuffer);
    [eaglContext renderbufferStorage:GL_RENDERBUFFER fromDrawable:(CAEAGLLayer*)self.layer];
    glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_WIDTH, &width);
    glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_HEIGHT, &height);
    glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, depthRenderbuffer);
    
    //ortho(l-r-b-t-n-f)
    GLfloat fwidth = (GLfloat)width;
    GLfloat fheight = (GLfloat)height;
    
    gWidth = width;
    gHeight = height;

    //****************** 1st column GEMS***************
    // EMARALD
    Spheres[0].MAmbient = vmath::vec4( 0.0215f, 0.1745f, 0.0215f, 1.0f );
    Spheres[0].MDiffuse = vmath::vec4( 0.07568f, 0.61424f, 0.07568f, 1.0f );
    Spheres[0].MSpecular = vmath::vec4( 0.633f, 0.727811f, 0.633f, 1.0f );
    Spheres[0].spherePosition = vmath::vec3( 0.0f, 0.0f, -3.0f );
    Spheres[0].SViewport[0] = -gWidth/6;
    Spheres[0].SViewport[1] = -gHeight/8;
    Spheres[0].SViewport[2] = gWidth / 2;
    Spheres[0].SViewport[3] = gHeight / 2;
    Spheres[0].sphereShininess = 0.6f * 128.0f;
    // JADE
    Spheres[1].MAmbient = vmath::vec4(  0.135f, 0.2225f, 0.1575f, 1.0f );
    Spheres[1].MDiffuse = vmath::vec4(  0.54f, 0.89f, 0.63f, 1.0f );
    Spheres[1].MSpecular = vmath::vec4( 0.316228f, 0.316228f, 0.316228f, 1.0f );
    Spheres[1].spherePosition = vmath::vec3( 0.0f, 0.0f, -3.0f );
    Spheres[1].SViewport[0] = 0;
    Spheres[1].SViewport[1] = -gHeight/8;
    Spheres[1].SViewport[2] = gWidth / 2;
    Spheres[1].SViewport[3] = gHeight / 2;
    Spheres[1].sphereShininess = 0.1f * 128.0f;
    // OBSIDIAN
    Spheres[2].MAmbient = vmath::vec4( 0.05375f, 0.05f, 0.06625f, 1.0f );
    Spheres[2].MDiffuse = vmath::vec4( 0.18275f, 0.17f, 0.22525f, 1.0f );
    Spheres[2].MSpecular = vmath::vec4( 0.332741f, 0.328634f, 0.346435f, 1.0f );
    Spheres[2].spherePosition = vmath::vec3( 0.0f, 0.0f, -3.0f );
    Spheres[2].SViewport[0] = gWidth/6;
    Spheres[2].SViewport[1] = -gHeight/8;
    Spheres[2].SViewport[2] = gWidth / 2;
    Spheres[2].SViewport[3] = gHeight / 2;
    Spheres[2].sphereShininess = 0.3f * 128.0f;
    // PEARL
    Spheres[3].MAmbient = vmath::vec4( 0.25f, 0.20725f, 0.20725f, 1.0f );
    Spheres[3].MDiffuse = vmath::vec4( 1.0f, 0.829f, 0.829f, 1.0f );
    Spheres[3].MSpecular = vmath::vec4( 0.296648f, 0.296648f, 0.296648f, 1.0f );
    Spheres[3].spherePosition = vmath::vec3( 0.0f, 0.0f, -3.0f );
    Spheres[3].SViewport[0] = 2*gWidth/6;
    Spheres[3].SViewport[1] = -gHeight/8;
    Spheres[3].SViewport[2] = gWidth / 2;
    Spheres[3].SViewport[3] = gHeight / 2;
    Spheres[3].sphereShininess = 0.88f * 128.0f;
    // RUBY
    Spheres[4].MAmbient = vmath::vec4( 0.1745f, 0.01175f, 0.01175f, 1.0f );
    Spheres[4].MDiffuse = vmath::vec4( 0.61424f, 0.04136f, 0.04136f, 1.0f );
    Spheres[4].MSpecular = vmath::vec4( 0.727811f, 0.626959f, 0.626959f, 1.0f );
    Spheres[4].spherePosition = vmath::vec3( 0.0f, 0.0f, -3.0f );
    Spheres[4].SViewport[0] = 3*gWidth/6;
    Spheres[4].SViewport[1] = -gHeight/8;
    Spheres[4].SViewport[2] = gWidth / 2;
    Spheres[4].SViewport[3] = gHeight / 2;
    Spheres[4].sphereShininess = 0.6f * 128.0f;
    // TURQUOISE
    Spheres[5].MAmbient = vmath::vec4( 0.1f, 0.18725f, 0.1745f, 1.0f );
    Spheres[5].MDiffuse = vmath::vec4( 0.396f, 0.74151f, 0.69102f, 1.0f );
    Spheres[5].MSpecular = vmath::vec4( 0.297254f, 0.30829f, 0.306678f, 1.0f );
    Spheres[5].spherePosition = vmath::vec3( 0.0f, 0.0f, -3.0f );
    Spheres[5].SViewport[0] = 4*gWidth/6;
    Spheres[5].SViewport[1] = -gHeight/8;
    Spheres[5].SViewport[2] = gWidth / 2;
    Spheres[5].SViewport[3] = gHeight / 2;
    Spheres[5].sphereShininess = 0.1f * 128.0f;
    
    // **************** 2nd Column : Metal **************************
    // BRASS
    Spheres[6].MAmbient = vmath::vec4(  0.329412f, 0.223529f, 0.027451f, 1.0f );
    Spheres[6].MDiffuse = vmath::vec4( 0.780392f, 0.568627f, 0.113725f, 1.0f );
    Spheres[6].MSpecular = vmath::vec4(  0.992157f, 0.941176f, 0.807843f, 1.0f );
    Spheres[6].spherePosition  = vmath::vec3(  0.0f, 0.0f, -3.0f );
    Spheres[6].SViewport[0] = -gWidth / 6;
    Spheres[6].SViewport[1] = 1*gHeight/8;
    Spheres[6].SViewport[2] = gWidth / 2;
    Spheres[6].SViewport[3] = gHeight / 2;
    Spheres[6].sphereShininess = 0.21794872f * 128.0f;
    // BRONZE
    Spheres[7].MAmbient = vmath::vec4( 0.2125f, 0.1275f, 0.054f, 1.0f );
    Spheres[7].MDiffuse = vmath::vec4( 0.714f, 0.4284f, 0.18144f, 1.0f );
    Spheres[7].MSpecular = vmath::vec4( 0.393548f, 0.271906f, 0.166721f, 1.0f );
    Spheres[7].spherePosition = vmath::vec3( 0.0f, 0.0f, -3.0f );
    Spheres[7].SViewport[0] = 0;
    Spheres[7].SViewport[1] = 1*gHeight/8;
    Spheres[7].SViewport[2] = gWidth / 2;
    Spheres[7].SViewport[3] = gHeight / 2;
    Spheres[7].sphereShininess = 0.2f * 128.0f;
    // CHROME
    Spheres[8].MAmbient = vmath::vec4( 0.25f, 0.25f, 0.25f, 1.0f );
    Spheres[8].MDiffuse = vmath::vec4( 0.4f, 0.4f, 0.4f, 1.0f );
    Spheres[8].MSpecular = vmath::vec4( 0.774597f, 0.774597f, 0.774597f, 1.0f );
    Spheres[8].spherePosition = vmath::vec3( 0.0f, 0.0f, -3.0f );
    Spheres[8].SViewport[0] = gWidth / 6;
    Spheres[8].SViewport[1] = 1*gHeight/8;
    Spheres[8].SViewport[2] = gWidth / 2;
    Spheres[8].SViewport[3] = gHeight / 2;
    Spheres[8].sphereShininess = 0.6f * 128.0f;
    // COPPER
    Spheres[9].MAmbient = vmath::vec4( 0.19125f, 0.0735f, 0.0225f, 1.0f );
    Spheres[9].MDiffuse = vmath::vec4( 0.7038f, 0.27048f, 0.0828f, 1.0f );
    Spheres[9].MSpecular = vmath::vec4( 0.256777f, 0.137622f, 0.086014f, 1.0f );
    Spheres[9].spherePosition = vmath::vec3( 0.0f, 0.0f, -3.0f );
    Spheres[9].SViewport[0] = 2*gWidth/6;
    Spheres[9].SViewport[1] = 1*gHeight/8;
    Spheres[9].SViewport[2] = gWidth / 2;
    Spheres[9].SViewport[3] = gHeight / 2;
    Spheres[9].sphereShininess = 0.1f * 128.0f;
    // GOLD
    Spheres[10].MAmbient = vmath::vec4( 0.24725f, 0.1995f, 0.0745f, 1.0f );
    Spheres[10].MDiffuse = vmath::vec4(  0.75164f, 0.555802f, 0.366065f, 1.0f );
    Spheres[10].MSpecular = vmath::vec4( 0.628281f, 0.555802f, 0.366065f, 1.0f );
    Spheres[10].spherePosition = vmath::vec3( 0.0f, 0.0f, -3.0f );
    Spheres[10].SViewport[0] = 3*gWidth/6;
    Spheres[10].SViewport[1] = 1*gHeight/8;
    Spheres[10].SViewport[2] = gWidth / 2;
    Spheres[10].SViewport[3] = gHeight / 2;
    Spheres[10].sphereShininess = 0.4f * 128.0f;
    // SILVER
    Spheres[11].MAmbient = vmath::vec4(  0.19225f, 0.19225f, 0.19225f, 1.0f );
    Spheres[11].MDiffuse = vmath::vec4(  0.50754f, 0.50754f, 0.50754f, 1.0f );
    Spheres[11].MSpecular = vmath::vec4( 0.508273f, 0.508273f, 0.508273f, 1.0f );
    Spheres[11].spherePosition = vmath::vec3( 0.0f, 0.0f, -3.0f );
    Spheres[11].SViewport[0] =4*gWidth/6;
    Spheres[11].SViewport[1] = 1*gHeight/8;
    Spheres[11].SViewport[2] = gWidth / 2;
    Spheres[11].SViewport[3] = gHeight / 2;
    Spheres[11].sphereShininess = 0.4f * 128.0f;
    
    // **************** 3rd Column : Plastic **************************
    // BLACK PLASTIC
    Spheres[12].MAmbient = vmath::vec4( 0.0f, 0.0f, 0.0f, 1.0f );
    Spheres[12].MDiffuse = vmath::vec4( 0.01f, 0.01f, 0.01f, 1.0f );
    Spheres[12].MSpecular = vmath::vec4( 0.50f, 0.50f, 0.50f, 1.0f );
    Spheres[12].spherePosition = vmath::vec3( 0.0f, 0.0f, -3.0f );
    Spheres[12].SViewport[0] = -gWidth/6;
    Spheres[12].SViewport[1] = 3*gHeight/8;
    Spheres[12].SViewport[2] = gWidth / 2;
    Spheres[12].SViewport[3] = gHeight / 2;
    Spheres[12].sphereShininess = 0.25f * 128.0f;
    // CYAN PLASTIC
    Spheres[13].MAmbient = vmath::vec4( 0.0f, 0.1f, 0.06f, 1.0f );
    Spheres[13].MDiffuse = vmath::vec4( 0.0f, 0.50980392f, 0.50980392f, 1.0f );
    Spheres[13].MSpecular = vmath::vec4( 0.50196078f, 0.50196078f, 0.50196078f, 1.0f );
    Spheres[13].spherePosition = vmath::vec3( 0.0f, 0.0f, -3.0f );
    Spheres[13].SViewport[0] = 0;
    Spheres[13].SViewport[1] = 3 * gHeight / 8;
    Spheres[13].SViewport[2] = gWidth / 2;
    Spheres[13].SViewport[3] = gHeight / 2;
    Spheres[13].sphereShininess = 0.25f * 128.0f;
    // GREEN PLASTIC
    Spheres[14].MAmbient = vmath::vec4( 0.0f, 0.0f, 0.0f, 1.0f );
    Spheres[14].MDiffuse = vmath::vec4( 0.1f, 0.35f, 0.1f, 1.0f );
    Spheres[14].MSpecular = vmath::vec4( 0.45f, 0.55f, 0.45f, 1.0f );
    Spheres[14].spherePosition  = vmath::vec3( 0.0f, 0.0f, -3.0f );
    Spheres[14].SViewport[0] = gWidth/6;
    Spheres[14].SViewport[1] = 3*gHeight/8;
    Spheres[14].SViewport[2] = gWidth / 2;
    Spheres[14].SViewport[3] = gHeight / 2;
    Spheres[14].sphereShininess = 0.25f * 128.0f;
    // RED PLASTIC
    Spheres[15].MAmbient = vmath::vec4( 0.0f, 0.0f, 0.0f, 1.0f );
    Spheres[15].MDiffuse = vmath::vec4( 0.5f, 0.0f, 0.0f, 1.0f );
    Spheres[15].MSpecular = vmath::vec4( 0.7f, 0.6f, 0.6f, 1.0f );
    Spheres[15].spherePosition = vmath::vec3( 0.0f, 0.0f, -3.0f );
    Spheres[15].SViewport[0] = 2 * gWidth/6;
    Spheres[15].SViewport[1] = 3*gHeight/8;
    Spheres[15].SViewport[2] = gWidth / 2;
    Spheres[15].SViewport[3] = gHeight / 2;
    Spheres[15].sphereShininess = 0.25f * 128.0f;
    // WHITE PLASTIC
    Spheres[16].MAmbient = vmath::vec4( 0.0f, 0.0f, 0.0f, 1.0f );
    Spheres[16].MDiffuse = vmath::vec4( 0.55f, 0.55f, 0.55f, 1.0f );
    Spheres[16].MSpecular = vmath::vec4( 0.70f, 0.70f, 0.70f, 1.0f );
    Spheres[16].spherePosition = vmath::vec3( 0.0f, 0.0f, -3.0f);
    Spheres[16].SViewport[0] = 3 * gWidth/6;
    Spheres[16].SViewport[1] = 3*gHeight/8;
    Spheres[16].SViewport[2] = gWidth / 2;
    Spheres[16].SViewport[3] = gHeight / 2;
    Spheres[16].sphereShininess = 0.25f * 128.0f;
    // YELLOW PLASTIC
    Spheres[17].MAmbient = vmath::vec4( 0.0f, 0.0f, 0.0f, 1.0f );
    Spheres[17].MDiffuse = vmath::vec4( 0.5f, 0.5f, 0.0f, 1.0f );
    Spheres[17].MSpecular = vmath::vec4( 0.60f, 0.60f, 0.50f, 1.0f );
    Spheres[17].spherePosition = vmath::vec3(0.0f, 0.0f, -3.0f );
    Spheres[17].SViewport[0] = 4 * gWidth/6;
    Spheres[17].SViewport[1] = 3*gHeight/8;
    Spheres[17].SViewport[2] = gWidth / 2;
    Spheres[17].SViewport[3] = gHeight / 2;
    Spheres[17].sphereShininess = 0.25f * 128.0f;
    
    // **************** 4th Column : Rubber **************************
    // BLACK RUBBER
    Spheres[18].MAmbient = vmath::vec4( 0.02f, 0.02f, 0.02f, 1.0f );
    Spheres[18].MDiffuse = vmath::vec4( 0.01f, 0.01f, 0.01f, 1.0f );
    Spheres[18].MSpecular = vmath::vec4( 0.4f, 0.40f, 0.40f, 1.0f );
    Spheres[18].spherePosition = vmath::vec3( 0.0f, 0.0f, -3.0f );
    Spheres[18].SViewport[0] = -gWidth/6;
    Spheres[18].SViewport[1] = 5*gHeight/8;
    Spheres[18].SViewport[2] = gWidth / 2;
    Spheres[18].SViewport[3] = gHeight / 2;
    Spheres[18].sphereShininess = 0.078125f * 128.0f;
    // CYAN RUBBER
    Spheres[19].MAmbient = vmath::vec4( 0.0f, 0.05f, 0.05f, 1.0f );
    Spheres[19].MDiffuse = vmath::vec4( 0.40f, 0.50f, 0.50f, 1.0f );
    Spheres[19].MSpecular = vmath::vec4( 0.04f, 0.7f, 0.7f, 1.0f );
    Spheres[19].spherePosition = vmath::vec3( 0.0f, 0.0f, -3.0f );
    Spheres[19].SViewport[0] = 0;
    Spheres[19].SViewport[1] = 5*gHeight/8;
    Spheres[19].SViewport[2] = gWidth / 2;
    Spheres[19].SViewport[3] = gHeight / 2;
    Spheres[19].sphereShininess = 0.078125f * 128.0f;
    // GREEN RUBBER
    Spheres[20].MAmbient = vmath::vec4( 0.0f, 0.05f, 0.0f, 1.0f );
    Spheres[20].MDiffuse = vmath::vec4( 0.4f, 0.5f, 0.4f, 1.0f );
    Spheres[20].MSpecular = vmath::vec4( 0.04f, 0.7f, 0.04f, 1.0f );
    Spheres[20].spherePosition = vmath::vec3( 0.0f, 0.0f, -3.0f );
    Spheres[20].SViewport[0] = gWidth/6;
    Spheres[20].SViewport[1] = 5*gHeight/8;
    Spheres[20].SViewport[2] = gWidth / 2;
    Spheres[20].SViewport[3] = gHeight / 2;
    Spheres[20].sphereShininess = 0.078125f * 128.0f;
    // RED RUBBER
    Spheres[21].MAmbient = vmath::vec4( 0.05f, 0.0f, 0.0f, 1.0f );
    Spheres[21].MDiffuse = vmath::vec4( 0.5f, 0.4f, 0.4f, 1.0f );
    Spheres[21].MSpecular = vmath::vec4( 0.7f, 0.04f, 0.04f, 1.0f );
    Spheres[21].spherePosition = vmath::vec3( 0.0f, 0.0f, -3.0f );
    Spheres[21].SViewport[0] = 2 * gWidth/6;
    Spheres[21].SViewport[1] = 5*gHeight/8;
    Spheres[21].SViewport[2] = gWidth / 2;
    Spheres[21].SViewport[3] = gHeight / 2;
    Spheres[21].sphereShininess = 0.078125f * 128.0f;
    // WHITE RUBBER
    Spheres[22].MAmbient = vmath::vec4( 0.05f, 0.05f, 0.05f, 1.0f );
    Spheres[22].MDiffuse = vmath::vec4( 0.5f, 0.5f, 0.5f, 1.0f );
    Spheres[22].MSpecular = vmath::vec4( 0.70f, 0.70f, 0.70f, 1.0f );
    Spheres[22].spherePosition = vmath::vec3( 0.0f, 0.0f, -3.0f );
    Spheres[22].SViewport[0] = 3 * gWidth/6;
    Spheres[22].SViewport[1] = 5*gHeight/8;
    Spheres[22].SViewport[2] = gWidth / 2;
    Spheres[22].SViewport[3] = gHeight / 2;
    Spheres[22].sphereShininess = 0.078125f * 128.0f;
    // YELLOW RUBBER
    Spheres[23].MAmbient = vmath::vec4( 0.05f, 0.05f, 0.0f, 1.0f );
    Spheres[23].MDiffuse = vmath::vec4( 0.50f, 0.50f, 0.40f, 1.0f );
    Spheres[23].MSpecular = vmath::vec4( 0.70f, 0.70f, 0.04f, 1.0f );
    Spheres[23].spherePosition = vmath::vec3( 0.0f, 0.0f, -3.0f );
    Spheres[23].SViewport[0] = 4 * gWidth/6;
    Spheres[23].SViewport[1] = 5*gHeight/8;
    Spheres[23].SViewport[2] = gWidth / 2;
    Spheres[23].SViewport[3] = gHeight / 2;
    Spheres[23].sphereShininess = 0.078125f * 128.0f;
    
    
    
    
    perspectiveProjectionMatrix = vmath::perspective(45.0f, fwidth/fheight, 0.1f, 100.0f);
    
    if(glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
    {
        printf("Failed to Create Complete ramebuffer Object %x \n", glCheckFramebufferStatus(GL_FRAMEBUFFER));
    }
    [self drawView:nil];//repaint like warmup style
}

-(void)startAnimation
{
    if(!isAnimating)
    {
        displaylink = [NSClassFromString(@"CADisplayLink") displayLinkWithTarget:self selector:@selector(drawView:)];
        [displaylink setPreferredFramesPerSecond:animationFrameInterval];
        [displaylink addToRunLoop:[NSRunLoop currentRunLoop] forMode:NSDefaultRunLoopMode];
        
        isAnimating = YES;
    }
}

-(void)stopAnimation
{
    if(isAnimating)
    {
        [displaylink invalidate];
        displaylink=nil;
        
        isAnimating = NO;
    }
}

-(BOOL) acceptsFirstResponder
{
    return YES;
}

-(void)touchesBegane :(NSSet *)touches withEvent:(UIEvent *)UIEvent
{
    
}


-(void) onLongPress: (UIGestureRecognizer *)gesture
{
    
}

-(void) onSwipe: (UIGestureRecognizer *)gesture
{
    [self release];
    
    exit(0);
}

-(void) onDoubleTap: (UITapGestureRecognizer *)gesture
{
    if(toggleLightingType == NO)
        toggleLightingType = YES;
    else
        toggleLightingType = NO;

        startAnimation = YES;
    
}

-(void) onSingleTap: (UITapGestureRecognizer *)gesture
{
    if(axis > 3)
    {
        axis = 1;
    }
    else
    {
        enableLighting = YES;
        axis = axis + 1;
        theta = 0.0f;
        lightPosition[0] = 0.0f;
        lightPosition[1] = 0.0f;
        lightPosition[2] = 0.0f;
    }
}

- (void)dealloc
{
    if(vao_sphere)
    {
        glDeleteVertexArrays(1, &vao_sphere);
        vao_sphere = 0;
    }
    
    if (vbo_sphere_position)
    {
        glDeleteBuffers(1, &vbo_sphere_position);
        vbo_sphere_position = 0;
    }
    
    if (vbo_sphere_normals)
    {
        glDeleteBuffers(1, &vbo_sphere_normals);
        vbo_sphere_normals = 0;
    }
    
    if (vbo_sphere_elements)
    {
        glDeleteBuffers(1, &vbo_sphere_elements);
        vbo_sphere_elements = 0;
    }
    
    // per vertex
    glDetachShader(shaderProgramObject_v, vertexShaderObject_v);
    glDetachShader(shaderProgramObject_v, fragmentShaderObject_v);
    
    glDeleteShader(vertexShaderObject_v);
    vertexShaderObject_v = 0;
    
    glDeleteShader(fragmentShaderObject_v);
    fragmentShaderObject_v = 0;
    
    glDeleteProgram(shaderProgramObject_v);
    shaderProgramObject_v = 0;
    
    //per fragment
    glDetachShader(shaderProgramObject_f, vertexShaderObject_f);
    glDetachShader(shaderProgramObject_f, fragmentShaderObject_f);
    
    glDeleteShader(vertexShaderObject_f);
    vertexShaderObject_f = 0;
    
    glDeleteShader(fragmentShaderObject_f);
    fragmentShaderObject_f = 0;
    
    glDeleteProgram(shaderProgramObject_f);
    shaderProgramObject_f = 0;
    
    if(depthRenderbuffer)
    {
        glDeleteRenderbuffers(1, &depthRenderbuffer);
        depthRenderbuffer=0;
    }
    
    if(colorRenderbuffer)
    {
        glDeleteRenderbuffers(1, &colorRenderbuffer);
        colorRenderbuffer=0;
    }
    
    if(defaultFramebuffer)
    {
        glDeleteFramebuffers(1, &defaultFramebuffer);
        defaultFramebuffer=0;
    }
    
    if([EAGLContext currentContext] == eaglContext)
    {
        [EAGLContext setCurrentContext:nil];
    }
    [eaglContext release];
    eaglContext =nil;
    
    [super dealloc];
}

@end
