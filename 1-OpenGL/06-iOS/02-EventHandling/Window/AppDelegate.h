//
//  AppDelegate.h
//  Window
//
//  Created by pratik saboo on 16/02/20.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

