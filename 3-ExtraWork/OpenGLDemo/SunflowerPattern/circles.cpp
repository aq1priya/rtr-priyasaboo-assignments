//Headers
#include<windows.h>
#include<gl/gl.h>
#include<gl/glu.h>

#define _USE_MATH_DEFINES 1
#include<math.h>
#include<stdio.h>

#pragma comment (lib, "glu32.lib")
#pragma comment (lib, "opengl32.lib")

//Macros
#define WIN_WIDTH 800
#define WIN_HEIGHT 600
#define NUM_POINTS 1000
#define NBR_CIRCLES 200

//global function declarations
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

//global variables
HWND ghwnd = NULL;
HDC ghdc = NULL;
HGLRC ghrc = NULL;
bool gbActiveWindow = false;
bool gbFullScreen = false;
DWORD dwStyle;
WINDOWPLACEMENT wpPrev = { sizeof(WINDOWPLACEMENT) };
FILE *gpfile = NULL;
int Sel_Pattern;
GLfloat increment = 0;
GLfloat Radius = 0.0f;
//winmain
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{
	//function declarations
	int initialize(void);
	void display(void);

	//variables
	WNDCLASSEX wndclass;
	HWND hwnd;
	MSG msg;
	TCHAR szAppName[] = TEXT("OGLSkeleton");
	bool bDone = false;
	int iRet = 0;

	//code
	if (fopen_s(&gpfile, "log.txt", "w") != 0)
	{
		MessageBox(NULL, TEXT("Log file cannot be created"), TEXT("Error"), MB_OK);
		exit(0);
	}
	//initialization of WNDCLASS structure
	wndclass.cbSize = sizeof(WNDCLASSEX);
	wndclass.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
	wndclass.cbClsExtra = 0;
	wndclass.cbWndExtra = 0;
	wndclass.lpfnWndProc = WndProc;
	wndclass.hInstance = hInstance;
	wndclass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	wndclass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndclass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndclass.lpszClassName = szAppName;
	wndclass.lpszMenuName = NULL;
	wndclass.hIconSm = LoadIcon(hInstance, IDI_APPLICATION);

	//register above class
	RegisterClassEx(&wndclass);

	//create window
	hwnd = CreateWindowEx(WS_EX_APPWINDOW,
		szAppName,
		TEXT("Circle On Graph- Priya Saboo"),
		WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN | WS_CLIPSIBLINGS | WS_VISIBLE,
		100,
		100,
		WIN_WIDTH,
		WIN_HEIGHT,
		NULL,
		NULL,
		hInstance,
		NULL);

	ghwnd = hwnd;
	iRet = initialize();
	if (iRet == -1)
	{
		fprintf(gpfile, "ChoosePixelFormat() Failed\n");
		DestroyWindow(hwnd);
	}
	else if (iRet == -2)
	{
		fprintf(gpfile, "SetPixelFormat() Failed\n");
		DestroyWindow(hwnd);
	}
	else if (iRet == -3)
	{
		fprintf(gpfile, "wglCreateContext() Failed\n");
		DestroyWindow(hwnd);
	}
	else if (iRet == -4)
	{
		fprintf(gpfile, "wglMakeCurrent() Failed\n");
		DestroyWindow(hwnd);
	}
	else
		fprintf(gpfile, "Initialize() Succeded\n");
	ShowWindow(hwnd, iCmdShow);
	SetForegroundWindow(hwnd);
	SetFocus(hwnd);

	//gameLoop
	while (bDone == false)
	{
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			if (msg.message == WM_QUIT)
			{
				bDone = true;
			}
			else
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}
		else
		{
			if (gbActiveWindow == true)
			{
				//here you should call update
			}
			display();
		}
	}
	return((int)msg.wParam);
}

LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	//function declarations
	void resize(int, int);
	void ToggleFullScreen(void);
	void uninitialize(void);

	//code
	switch (iMsg)
	{
	case WM_SETFOCUS:
		gbActiveWindow = true;
		break;

	case WM_KILLFOCUS:
		gbActiveWindow = false;
		break;

	case WM_SIZE:
		resize(LOWORD(lParam), HIWORD(lParam));
		break;

	case WM_ERASEBKGND:
		return(0);

	case WM_CLOSE:
		DestroyWindow(hwnd);
		break;

	case WM_KEYDOWN:
		switch (LOWORD(wParam))
		{
		case VK_ESCAPE:
			DestroyWindow(hwnd);
			break;

		case 0x46:
			ToggleFullScreen();
			break;

		case 0x31:
			Sel_Pattern = 1;
			Radius = 0.0f;
			increment = 0.0f;
			break;

		case 0x32:
			Sel_Pattern = 2;
			increment = 0.0f;
			break;

		case 0x33:
			Sel_Pattern = 3;
			break;

		case 0x34:
			Sel_Pattern = 4;
			Radius = 0.0f;
			increment = 0.0f;
			break;
		}
		break;

	case WM_DESTROY:
		uninitialize();
		PostQuitMessage(0);
		break;
	}
	return(DefWindowProc(hwnd, iMsg, wParam, lParam));
}

//toggle fullscreen function
void ToggleFullScreen(void)
{
	MONITORINFO mi;
	if (gbFullScreen == false)
	{
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);
		if (dwStyle & WS_OVERLAPPEDWINDOW)
		{
			mi = { sizeof(MONITORINFO) };
			if (GetWindowPlacement(ghwnd, &wpPrev) && GetMonitorInfo(MonitorFromWindow(ghwnd, MONITORINFOF_PRIMARY), &mi))
			{
				SetWindowLong(ghwnd, GWL_STYLE, dwStyle & ~WS_OVERLAPPEDWINDOW);
				SetWindowPos(ghwnd,
					HWND_TOP,
					mi.rcMonitor.left,
					mi.rcMonitor.top,
					mi.rcMonitor.right - mi.rcMonitor.left,
					mi.rcMonitor.bottom - mi.rcMonitor.top,
					SWP_NOZORDER | SWP_FRAMECHANGED);
			}
		}
		ShowCursor(FALSE);
		gbFullScreen = true;
	}
	else
	{
		fprintf(gpfile, "Fullscreen to normal\n");
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPED);
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd,
			HWND_TOP,
			0,
			0,
			0,
			0,
			SWP_NOZORDER | SWP_FRAMECHANGED | SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER);
		ShowCursor(TRUE);
		gbFullScreen = false;
	}
}

//initialize() function
int initialize(void)
{
	//function declarations
	void resize(int, int);

	//variable declarations
	PIXELFORMATDESCRIPTOR pfd;
	int iPixelFormatIndex;

	//code
	//initializa pfd structure
	ZeroMemory(&pfd, sizeof(PIXELFORMATDESCRIPTOR));
	pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
	pfd.nVersion = 1;
	pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
	pfd.iPixelType = PFD_TYPE_RGBA;
	pfd.cColorBits = 32;
	pfd.cRedBits = 8;
	pfd.cGreenBits = 8;
	pfd.cBlueBits = 8;
	pfd.cAlphaBits = 8;

	ghdc = GetDC(ghwnd);
	iPixelFormatIndex = ChoosePixelFormat(ghdc, &pfd);
	if (iPixelFormatIndex == 0)
	{
		return(-1);
	}
	if (SetPixelFormat(ghdc, iPixelFormatIndex, &pfd) == false)
	{
		return(-2);
	}
	ghrc = wglCreateContext(ghdc);
	if (ghrc == NULL)
	{
		return(-3);
	}
	if (wglMakeCurrent(ghdc, ghrc) == false)
	{
		return(-4);
	}

	//clear the screen by opengl color
	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);	//r,g,b,a

	//warmup call to resize
	resize(WIN_WIDTH, WIN_HEIGHT);
	return(0);
}

//display() function
void display(void)
{
	//function declaration
	void DrawCircle(GLfloat, GLfloat, GLfloat, GLfloat);

	//variable declarations
	GLfloat Cx, Cy;
	
	static GLfloat Rangle = 0.01f;
	

	glClear(GL_COLOR_BUFFER_BIT);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	switch (Sel_Pattern)
	{
	case 1:	// Drawing circles  on circular path
		for (GLfloat angle = 0.0f; angle < 2.0f * M_PI; angle = angle + 0.1f)
		{
			Cx = (GLfloat)cos(angle);
			Cy = (GLfloat)sin(angle);
			DrawCircle(Cx, Cy, Radius, 0.0f);
		}
		break;

	/*	//	Transforming circle to get spiral effect by increasing the radius and angle for every next point
			1.	In case 2 we are incrementing the angle continuously, here we get two types of pattern.
				a.	Starfish pattern (spikes of straight line which gets decreased on incrementing angle)
				b.	Rose pattern
	*/
	case 2: 
	{	
		GLfloat angle = 0.0f;
		GLfloat angle_increment = 0.0f;

		angle_increment = increment * (GLfloat)(M_PI / 180.0f);

		for (int i = 0; i <= NBR_CIRCLES; i++)
		{
			angle = i * angle_increment;
			Cx = (GLfloat)cos(angle)* i / NBR_CIRCLES;
			Cy = (GLfloat)sin(angle)* i / NBR_CIRCLES;
			DrawCircle(Cx, Cy, 0.01f, 0.0f);
		}
	}
	break;

	// sunflower arrangement with all circles of same radii
	/*
		1.	From case two if we use golden angle to draw the same formation we will get a sunflower arrangement.
							
		2. theta = (sqrt(5) -1 )/2
			golden angle = 2*M_PI*theta (in radians)
			golden angle = 360*theta (in degrees)
	*/
	case 3:	
	{
		Radius = 0.0f;
		GLfloat Spiral_Radius = 0.0f;
		GLfloat angle = 0.0f;
		GLfloat phi = ((GLfloat)sqrt(5.0f) - 1.0f) / 2.0f;
		GLfloat GoldenAngle = 2 * M_PI*phi;
		GLfloat lg_Area, sm_Area, lg_Radius, sm_Radius;

		//In our case large circle is of unit radius , so lg_Area = M_PI * lg_Radius * lg_Radius;
		lg_Radius = 1.0f;
		lg_Area = M_PI * lg_Radius * lg_Radius;
		sm_Area = lg_Area / NBR_CIRCLES;
		sm_Radius = (GLfloat)sqrt(sm_Area / M_PI);

		for (int i = 0; i <= NBR_CIRCLES; i++)
		{
			angle = i * GoldenAngle;
			Spiral_Radius = (GLfloat)sqrt((i*sm_Area) / M_PI);	//Spiral radius is incremented with respect to smaller circle area	
			Cx = (GLfloat)cos(angle)*Spiral_Radius;
			Cy = (GLfloat)sin(angle)*Spiral_Radius;
			DrawCircle(Cx, Cy, sm_Radius*0.80f, 0.0f);			//sm_Radius is multiplied by a factor to avoid overlaping of the circles	
		}
	}
	break;

	// SunFlower Arrangement
	/*
		1.	In fourth case, we will follow the logic of 2nd case but the radius of outer circles
			will increment respective to the radius of the inner circle shrinks, so that area of circle remains same.
	*/
	case 4:
	{
		GLfloat Spiral_Radius = 0.0f;
		GLfloat angle = 0.0f;

		GLfloat cumulative_area;
		GLfloat lg_Area, sm_Area, lg_Radius, sm_Radius, avg_area, min_area, max_area;

		GLfloat phi = ((GLfloat)sqrt(5.0f) - 1.0f) / 2.0f;
		GLfloat GoldenAngle = 2 * M_PI*phi;

		GLfloat deviation = 5.0f/6.0f;

		//In our case large circle is of unit radius , so lg_Area = M_PI * lg_Radius * lg_Radius;
		lg_Radius = 1.0f;
		lg_Area = M_PI * lg_Radius * lg_Radius;
		fprintf(gpfile, "NBR_CIRCLES * = %d\n", NBR_CIRCLES);
		avg_area= lg_Area / 200.0f;	//average area of small circles

		min_area = avg_area * (1.0f - deviation);	// smallest size circle can have(half of average area)
		max_area = avg_area * (1.0f + deviation);	// largest size circle can have(1and half times of average area) //max_area- min_area =1.0f

		cumulative_area = 0.0f;

		for (int i = 0; i <= 200; i++)
		{
			GLfloat ratio = i / 200.0f;
			angle = i * GoldenAngle;

			sm_Area = min_area + (ratio* (max_area-min_area));
			sm_Radius = (GLfloat)sqrt(sm_Area / M_PI);
		
			cumulative_area = cumulative_area + sm_Area;
			Spiral_Radius = (GLfloat)sqrt(cumulative_area / M_PI);	//Spiral radius is incremented with respect to smaller circle area	
			
			Cx = (GLfloat)cos(angle)*Spiral_Radius;
			Cy = (GLfloat)sin(angle)*Spiral_Radius;
			DrawCircle(Cx, Cy, sm_Radius*0.80f, 0.0f);			//sm_Radius is multiplied by a factor to avoid overlaping of the circles	
		}
	}
	break;

	}	//switch
	SwapBuffers(ghdc);
	if (Sel_Pattern == 1)
	{
		Radius = Radius + 0.001f;

		Rangle = Rangle + 0.01f;
		if (Rangle >= 2.0f * M_PI)
			Rangle = 0.01f;
	}
	
	if (increment < 250)
			increment = increment +0.01f;
}

void DrawCircle(GLfloat Cx, GLfloat Cy, GLfloat Radius, GLfloat Rangle)
{
	GLfloat theta = 0.0f;

	glLoadIdentity();
	glTranslatef(0.0f, 0.0f, -3.0f);
	glRotatef(Rangle, 0.0f, 0.0f, 1.0f);
	glLineWidth(2.0f);
	glBegin(GL_LINE_LOOP);
	for (theta = 0.0f; theta <= 2 * M_PI; theta = theta + 0.1f)
	{
		if (Sel_Pattern == 1)
			glColor3f(Cx * 5, Cy * 5, 0.5f);
		else if (Sel_Pattern == 2)
			glColor3f(1.0f, Cy * 10, 0.4f);
		else
			glColor3f(1.0f, 1.0f, 0.0f);
		glVertex3f(Cx + (GLfloat)cos(theta)*Radius, Cy + (GLfloat)sin(theta)*Radius, 0.0f);
	}
	glEnd();
}
//resize() function
void resize(int width, int height)
{
	if (height == 0)
	{
		height = 1;
	}
	glViewport(0, 0, (GLsizei)width, (GLsizei)height);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluPerspective(45.0f, (GLfloat)width / (GLfloat)height, 0.1f, 100.0f);

}

//uninitialize() function
void uninitialize(void)
{
	if (gbFullScreen == true)
	{
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPED);
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd,
			HWND_TOP,
			0,
			0,
			0,
			0,
			SWP_NOZORDER | SWP_FRAMECHANGED | SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER);
		ShowCursor(TRUE);
	}

	//break the current context
	if (wglGetCurrentContext() == ghrc)
	{
		wglMakeCurrent(NULL, NULL);
	}

	if (ghrc)
	{
		wglDeleteContext(ghrc);
		ghrc = NULL;
	}

	if (ghdc)
	{
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	if (gpfile)
	{
		fprintf(gpfile, "log file is closed successfully\n");
		fclose(gpfile);
		gpfile = NULL;
	}
}