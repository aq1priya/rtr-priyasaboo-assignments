#include<stdio.h>

typedef struct Node
{
	int Data;
	struct Node *next;
}Node;

//function declaration
Node *CreateNewNode(void);
void ShowList(Node*);
void InsertAtBeginning(Node*);
void InsertAtEnd(Node*);
int InsertInBetween(Node*);
void DeleteAtEnd(Node*);
void DeleteAtBegining(Node*);
void DeleteInBetween(Node*);

int main()
{
	int i = 0;
	int Count = 0;
	Node *head;
	Node *Current;
	Node *NewNode;
	head = (Node*)malloc(sizeof(Node));
	head->next = NULL;
	printf("Number of nodes you want to create: ");
	scanf("%d", &Count);
	Current = head;
	for (i = 0; i < Count; i++)
	{
		NewNode = CreateNewNode();
		NewNode->next = NULL;
		Current->next = NewNode;
		Current = NewNode;
	}
	ShowList(head);
	InsertAtBeginning(head);
	ShowList(head);
	InsertInBetween(head);
	ShowList(head);
	InsertAtEnd(head);
	ShowList(head);
	printf("Deleting Node At Beginning\n\n");
	DeleteAtBegining(head);
	ShowList(head);
	printf("Deleting Node At End\n\n");
	DeleteAtEnd(head);
	ShowList(head);
	printf("Deleting Node In Between\n\n");
	DeleteInBetween(head);
	ShowList(head);
}

Node* CreateNewNode(void)
{
	int Value;
	Node *NewNode;
	NewNode = (Node*)malloc(sizeof(Node));
	printf("\nEnter the data for node: ");
	scanf("%d", &Value);
	NewNode->Data = Value;
	return (NewNode);
}

void InsertAtBeginning(Node* head)
{	
	Node *NewNode;
	NewNode = CreateNewNode();
	NewNode->next = head->next;
	head->next = NewNode;
}

void InsertAtEnd(Node* head)
{
	Node *CurrentNode;
	Node *NewNode;
	NewNode = CreateNewNode();
	
	CurrentNode = head;
	while (CurrentNode->next != NULL)
	{
		CurrentNode = CurrentNode->next;
	}
	
	NewNode->next = NULL;
	CurrentNode->next = NewNode;
}

int InsertInBetween(Node* head)
{
	int Position;

	printf("\nEnter the position at which you want to insert the node: ");
	scanf("%d", &Position);
	Node *CurrentNode;
	Node *NewNode = CreateNewNode();
	CurrentNode = head;
	while (Position != 1)
	{
		if (CurrentNode->next == NULL)
		{
			printf("Position does not exists in this list\n");
			return(1);
		}
			
		CurrentNode = CurrentNode->next;
		Position--;
	}
	NewNode->next = CurrentNode->next;
	CurrentNode->next = NewNode;
	return(0);
}

void DeleteAtBegining(Node *head)
{
	Node *Current;

	Current = head->next;
	head->next = head->next->next;
	free(Current);
	Current = NULL;
}

void DeleteAtEnd(Node *head)
{
	Node *Current;
	Node *Temp;

	Current = head;
	Temp = head->next;
	while (Temp->next != NULL)
	{
		Temp = Temp->next;
		Current = Current->next;
	}
	Current->next = NULL;
	free(Temp);
	Temp = NULL;
}

void DeleteInBetween(Node *head)
{
	Node *temp;
	Node *Current;
	int Position;

	printf("\nEnter the position of node you want to delete: ");
	scanf("%d", &Position);
	Current = head;
	while (Position != 1)
	{
		Current = Current->next;
		Position--;
	}
	temp = Current->next;
	Current->next = temp->next;
	free(temp);
	temp = NULL;
}

void ShowList(Node* head)
{
	Node* Current;
	Current = head->next;

	while (Current != NULL)
	{
		printf("%d-->", Current->Data);
		Current = Current->next;
	}

	printf("NULL\n");
}
