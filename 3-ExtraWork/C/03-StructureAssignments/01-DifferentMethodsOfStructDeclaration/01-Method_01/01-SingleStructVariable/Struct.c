#include <stdio.h>

//	DEFINING STRUCT
struct MyData
{
	int i;
	float f;
	double d;
} data;		//declaring a single struct variable of type 'struct MyData' globally

int main(void)
{
	//variable declarations
	int i_size;
	int f_size;
	int d_size;
	int struct_MyData_size;

	//code
	//	Assigning data values to the data members of struct MyData
	data.i = 30;
	data.f = 11.45f;
	data.d = 1.29995;

	//	Displaying values of the data members of 'struct MyData'
	printf("\nDATA MEMBERS OF 'struct MyData' ARE :\n\n");
	printf("i = %d\n", data.i);
	printf("f = %f\n", data.f);
	printf("d = %lf\n", data.d);

	//	calculating sizes (in Bytes) of the data members of 'struct MyData'
	i_size = sizeof(data.i);
	f_size = sizeof(data.f);
	d_size = sizeof(data.d);

	//	Displaying sizes of data members
	printf("\n\n");
	printf("SIZES OF DATA MEMBERS OF 'struct MyData'\n\n");
	printf("size of 'i' = %d bytes\n", i_size);
	printf("size of 'f' = %d bytes\n", f_size);
	printf("size of 'd' = %d bytes\n", d_size);

	//	Calculating size of the entire structure 'MyData'
	struct_MyData_size = sizeof(struct MyData);

	//	Displaying size of the entire structure 'MyData'
	printf("\nSize of 'struct MyData'  : %d bytes\n\n", struct_MyData_size);

	return(0);

}
