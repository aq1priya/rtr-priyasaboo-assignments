#include<stdio.h>

//defining two structure
struct MyPoint
{
	int x;
	int y;
}point;

struct MyPointProperties
{
	int quadrant;
	char axis_location[10];
}point_properties;

int main(void)
{
	//code
	//user input for the data members of 'struct MyPoint' variables 'Point_A'
	printf("\n\n");
	printf("Enter X-cordinate for a point : ");
	scanf("%d", &point.x);
	printf("Enter Y-cordinate for a point : ");
	scanf("%d", &point.y);
	printf("\n\n");
	printf("Point co-ordinates (x,y) are: (%d, %d) !!!\n", point.x, point.y);

	if (point.x == 0 && point.y == 0)
	{
		printf("The point is origin (%d, %d)!!! \n", point.x, point.y);
	}
	else
	{
		if (point.x == 0)
		{
			if (point.y < 0)//if y is negative
				strcpy(point_properties.axis_location, "Negative Y");
			if (point.y > 0)//if y is positive
				strcpy(point_properties.axis_location, "Positive Y");

			point_properties.quadrant = 0;
			printf("The point lies on the %s axis.\n", point_properties.axis_location);
		}
		else if (point.y == 0)
		{
			if (point.x < 0)//if x is negative
				strcpy(point_properties.axis_location, "Negative X");
			if (point.x > 0)//if x is positive
				strcpy(point_properties.axis_location, "Positive X");

			point_properties.quadrant = 0;
			printf("The point lies on the %s axis.\n", point_properties.axis_location);
		}
		else//both xand y are non zero
		{
			point_properties.axis_location[0] = '\0'; //A point lying in any quadrant cannot be lying on the co-ordinates axes
			if (point.x > 0 && point.y > 0)
				point_properties.quadrant = 1;
			else if (point.x < 0 && point.y > 0)
				point_properties.quadrant = 2;
			else if (point.x < 0 && point.y < 0)
				point_properties.quadrant = 3;
			else
				point_properties.quadrant = 4;
			printf("the point lies in %d Quadrant\n", point_properties.quadrant);
		}
	}
	return(0);
}
