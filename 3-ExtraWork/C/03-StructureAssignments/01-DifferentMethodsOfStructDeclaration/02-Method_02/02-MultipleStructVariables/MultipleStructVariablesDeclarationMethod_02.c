#include<stdio.h>

//DEFINING STRUCTURE
struct MyPoint
{
	int x;
	int y;
};

struct MyPoint point_A, point_B, point_C, point_D, point_E; //declaring 5 struct variables of type 'struct MyPoint' globally...

int main(void)
{
	//code
	//assigning data values to the data members of 'struct MyPoint' 
	point_A.x = 3;
	point_A.y = 0;

	point_B.x = 1;
	point_B.y = 2;

	point_C.x = 9;
	point_C.y = 6;

	point_D.x = 11;
	point_D.y = 3;

	point_E.x = 5;
	point_E.y = 8;

	//Displaying values of the data members of 'struct MyPoints' (All Variables)
	printf("\n\n");
	printf("co-ordinates (x,y) of point A are: (%d, %d)\n", point_A.x, point_A.y);
	printf("co-ordinates (x,y) of point B are: (%d, %d)\n", point_B.x, point_B.y);
	printf("co-ordinates (x,y) of point C are: (%d, %d)\n", point_C.x, point_C.y);
	printf("co-ordinates (x,y) of point D are: (%d, %d)\n", point_D.x, point_D.y);
	printf("co-ordinates (x,y) of point E are: (%d, %d)\n", point_E.x, point_E.y);

	return(0);
}