#include<stdio.h>

//defining struct
struct MyData
{
	int i;
	float f;
	double d;
};

struct MyData data;

int main(void)
{
	//variable declarations
	int i_size;
	int f_size;
	int d_size;
	int struct_MyData_size;

	//code
	//assigning data values to the data members of 'struct MyData'
	data.i = 30;
	data.f = 11.45f;
	data.d = 1.4555;

	//displaying values of the data members of 'struct MyData'
	printf("\n\n");
	printf("DATA MEMBERS OF 'struct MyData' ARE: \n\n");
	printf("i = %d\n", data.i);
	printf("f = %f\n", data.f);
	printf("d = %lf\n", data.d);

	//display size(in Bytes) of the Data Members of 'struct MyData'
	i_size = sizeof(data.i);
	f_size = sizeof(data.f);
	d_size = sizeof(data.d);

	printf("\n\n");
	printf("SIZE OF DATA MEMBERS OF 'struct MyData' ARE :\n\n");
	printf("size of i = %d bytes\n", i_size);
	printf("size of f = %d bytes\n", f_size);
	printf("size of d = %d bytes\n", d_size);

	//Calculating size (in bytes) of the entire 'struct MyData'
	struct_MyData_size = sizeof(struct MyData);
	printf("\n\n");
	printf("size of 'struct MyData' : %d bytes\n\n", struct_MyData_size);

	return(0);
}
