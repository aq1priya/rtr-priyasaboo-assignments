#include<stdio.h>

#define NAME_LENGTH 100
#define MARITAL_STATUS 10

struct Employee
{
	char name[NAME_LENGTH];
	int age;
	float salary;
	char sex;
	char mariatal_status[MARITAL_STATUS];
};

int main(void)
{
	//variable declarations
	struct Employee EmployeeRecord[5];	//array of 5 structure 'struct Employee'
	char employee_rajesh[] = "Rajesh";
	char employee_sameer[] = "Sameer";
	char employee_kalyani[] = "Kalyani";
	char employee_sonali[] = "Sonali";
	char employee_shantanu[] = "Shantanu";
	
	int i;

	//code 
	//HARDCODE INITIALIZATION OF ARRAY OF 'struct Employee'
	//***** EMPLOYEE 1 ******
	strcpy(EmployeeRecord[0].name, employee_rajesh);
	EmployeeRecord[0].age = 30;
	EmployeeRecord[0].sex = 'M';
	EmployeeRecord[0].salary = 50000.0f;
	strcpy(EmployeeRecord[0].mariatal_status, "Unmarried");

	//***** EMPLOYEE 2 ******
	strcpy(EmployeeRecord[1].name, employee_sameer);
	EmployeeRecord[1].age = 32;
	EmployeeRecord[1].sex = 'M';
	EmployeeRecord[1].salary = 60000.0f;
	strcpy(EmployeeRecord[1].mariatal_status, "Married");

	//***** EMPLOYEE 3 ******
	strcpy(EmployeeRecord[2].name, employee_kalyani);
	EmployeeRecord[2].age = 29;
	EmployeeRecord[2].sex = 'F';
	EmployeeRecord[2].salary = 62000.0f;
	strcpy(EmployeeRecord[2].mariatal_status, "Unmarried");

	//***** EMPLOYEE 4 ******
	strcpy(EmployeeRecord[3].name, employee_sonali);
	EmployeeRecord[3].age = 33;
	EmployeeRecord[3].sex = 'F';
	EmployeeRecord[3].salary = 40000.0f;
	strcpy(EmployeeRecord[3].mariatal_status, "Married");

	//***** EMPLOYEE 5 ******
	strcpy(EmployeeRecord[4].name, employee_shantanu);
	EmployeeRecord[4].age = 35;
	EmployeeRecord[4].sex = 'M';
	EmployeeRecord[4].salary = 45000.0f;
	strcpy(EmployeeRecord[4].mariatal_status, "Married");

	// ***** DISPLAY ******
	printf("\n\n");
	printf("* * * * * DISPLAYING EMPLOYEE RECORDS * * * * * \n\n");

	for (i = 0; i < 5; i++)
	{
		printf("EMPLOYEE NUMBER %d\n\n", i);
		printf("%-15s : %s\n","Name", EmployeeRecord[i].name);
		printf("%-15s : %d years\n","Age", EmployeeRecord[i].age);
		if (EmployeeRecord[i].sex == 'M' || EmployeeRecord[i].sex == 'm')
			printf("%-15s : %s\n", "Sex", "Male");
		else
			printf("%-15s : %s\n", "Sex", "Female");
		printf("%-15s : Rs. %.2f\n","Salary", EmployeeRecord[i].salary);
		printf("%-15s : %s\n\n", "Marital Status", EmployeeRecord[i].mariatal_status);
	}
	return(0);
}