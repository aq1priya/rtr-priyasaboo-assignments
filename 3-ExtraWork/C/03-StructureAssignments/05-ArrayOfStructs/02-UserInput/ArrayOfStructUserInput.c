#include<stdio.h>
#include<ctype.h>

#define NUM_EMPLOYEES 5
#define NAME_LENGTH 100

struct Employee
{
	char name[NAME_LENGTH];
	int age;
	float salary;
	char sex;
	char mariatal_status;
};

int main(void)
{
	//function prototype
	void MyGetString(char[], int);

	//variable declarations
	struct Employee EmployeeRecord[NUM_EMPLOYEES];
	int i;

	//code
	//USER INPUT INITIALIZATION OF ARRAY OF 'struct Employee'
	for (i = 0; i < NUM_EMPLOYEES; i++)
	{
		printf("\n\n\n");
		printf("*****DATA ENTRY FOR EMPLOYEE NUMBER %d*****\n", i);

		printf("\n");
		printf("Enter The Employee Name : ");
		MyGetString(EmployeeRecord[i].name, NAME_LENGTH);

		printf("\n");
		printf("Enter Employee's Age (in years) : ");
		scanf("%d", &EmployeeRecord[i].age);

		printf("Enter Employee's Sex (M/m for male, F/f for female) : ");
		EmployeeRecord[i].sex = getch();
		printf("%c", EmployeeRecord[i].sex);
		EmployeeRecord[i].sex = toupper(EmployeeRecord[i].sex);

		printf("\n");
		printf("Enter Employee's Salary (in Indian Rupees) : ");
		scanf("%f", &EmployeeRecord[i].salary);

		printf("Enter Employee's Marital Status (Y/y if married, N/n if unmarried) : ");
		EmployeeRecord[i].mariatal_status = getch();
		printf("%c", EmployeeRecord[i].mariatal_status);
		EmployeeRecord[i].mariatal_status = toupper(EmployeeRecord[i].mariatal_status);
	}

	//***** DISPLAY *****
	printf("\n\n\n");
	printf(" * * * * * DISPLAYING EMPLOYEE RECORDS * * * * * \n\n");
	for (i = 0; i < NUM_EMPLOYEES; i++)
	{
		printf("EMPLOYEE NUMBER %d\n\n", i);
		printf("%-15s : %s\n", "Name", EmployeeRecord[i].name);
		printf("%-15s : %d years\n", "Age", EmployeeRecord[i].age);

		if (EmployeeRecord[i].sex == 'M')
			printf("%-15s : %s\n", "Sex", "Male");
		else if (EmployeeRecord[i].sex == 'F')
			printf("%-15s : %s\n", "Sex", "Female");
		else
			printf("%-15s : %s\n", "Sex", "Invalid Data Entered");

		printf("%-15s : Rs. %.2f\n", "Salary", EmployeeRecord[i].salary);

		if (EmployeeRecord[i].mariatal_status == 'Y')
			printf("%-15s : %s\n\n", "Marital Status", "Married");
		else if (EmployeeRecord[i].mariatal_status == 'N')
			printf("%-15s : %s\n\n", "Marital Status", "Unmarried");
		else
			printf("%-15s : %s\n", "Mariatal Status", "Invalid Data Entered");

		printf("\n\n");
	}
	return(0);
}

void MyGetString(char str[], int str_size)
{
	//variable declarations
	int i;
	char ch = '\0';

	//code
	i = 0;
	do
	{
		ch = getch();
		str[i] = ch;
		printf("%c", str[i]);
		i++;
	} while ((ch != '\r') && (i < str_size));

	if (i == str_size)
		str[i - 1] = '\0';
	else
		str[i] = '\0';
}