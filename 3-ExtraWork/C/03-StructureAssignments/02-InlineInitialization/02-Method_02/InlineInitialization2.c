#include<stdio.h>

//defining structure
struct MyData
{
	int i;
	float f;
	double d;
	char c;
};
struct MyData data = { 10,15.67, 31.65793, 'p' };
int main(void)
{
	//code
	printf("\n\n");
	printf("Data members of 'struct MyData' are : \n\n");
	printf("i = %d\n", data.i);
	printf("f = %f\n", data.f);
	printf("d = %lf\n", data.d);
	printf("c = %c\n", data.c);

	return(0);
}