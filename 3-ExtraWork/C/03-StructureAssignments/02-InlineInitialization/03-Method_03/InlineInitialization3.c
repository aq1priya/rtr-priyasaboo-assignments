#include<stdio.h>

int main(void)
{
	//defining structure
	struct MyData
	{
		int i;
		float f;
		double d;
		char c;
	}data = { 23,11.55,97.9999,'s' };

	//code
	printf("\n\n");
	printf("Data members of 'struct MyData' are : \n\n");
	printf("i = %d\n", data.i);
	printf("f = %f\n", data.f);
	printf("d = %lf\n", data.d);
	printf("c = %c\n", data.c);

	return(0);
}