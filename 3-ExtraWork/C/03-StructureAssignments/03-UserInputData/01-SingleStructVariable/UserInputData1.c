#include<stdio.h>
#include<conio.h>
struct MyData
{
	int i;
	float f;
	double d;
	char ch;
};

int main(void)
{
	//variable declarations
	struct MyData data;

	//code
	printf("\n\n");

	printf("Enter the integer value: \n");
	scanf("%d", &data.i);

	printf("Enter the floating point value: \n");
	scanf("%f", &data.f);

	printf("Enter the double value: \n");
	scanf("%lf", &data.d);

	printf("Enter the character value: \n");
	data.ch = getch();

	//Display values of data members of 'struct MyData'
	printf("\n\n");
	printf("DATA MEMBERS OF 'struct MyData' ARE:\n");
	printf("i = %d\n", data.i);
	printf("f = %f\n", data.f);
	printf("d = %lf\n", data.d);
	printf("ch = %c\n", data.ch);

	return(0);
}
