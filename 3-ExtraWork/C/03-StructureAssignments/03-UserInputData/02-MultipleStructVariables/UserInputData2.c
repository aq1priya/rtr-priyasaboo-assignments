#include<stdio.h>

struct MyPoint
{
	int x;
	int y;
};

int main(void)
{
	//variable declarations
	struct MyPoint pointA, pointB, pointC, pointD, pointE;

	//code
	//User input for variable pointA of 'struct MyPoint'
	printf("\n\n");
	printf("Enter the X-coordinate of point A : ");
	scanf("%d", &pointA.x);
	printf("Enter the Y-coordinate of point A : ");
	scanf("%d", &pointA.y);

	//User input for variable pointB of 'struct MyPoint'
	printf("\n\n");
	printf("Enter the X-coordinate of point B : ");
	scanf("%d", &pointB.x);
	printf("Enter the Y-coordinate of point B : ");
	scanf("%d", &pointB.y);

	//User input for variable pointC of 'struct MyPoint'
	printf("\n\n");
	printf("Enter the X-coordinate of point C : ");
	scanf("%d", &pointC.x);
	printf("Enter the Y-coordinate of point C : ");
	scanf("%d", &pointC.y);

	//User input for variable pointD of 'struct MyPoint'
	printf("\n\n");
	printf("Enter the X-coordinate of point D : ");
	scanf("%d", &pointD.x);
	printf("Enter the Y-coordinate of point D : ");
	scanf("%d", &pointD.y);
	
	//User input for variable pointE of 'struct MyPoint'
	printf("\n\n");
	printf("Enter the X-coordinate of point E : ");
	scanf("%d", &pointE.x);
	printf("Enter the Y-coordinate of point E : ");
	scanf("%d", &pointE.y);

	//Displaying the values position of all points (all variables of 'struct MyPoint')
	printf("\n\n");
	printf("Co-ordinates of Point 'A(x, y)' are : A(%d, %d)\n", pointA.x, pointA.y);
	printf("Co-ordinates of Point 'B(x, y)' are : B(%d, %d)\n", pointB.x, pointB.y);
	printf("Co-ordinates of Point 'C(x, y)' are : C(%d, %d)\n", pointC.x, pointC.y);
	printf("Co-ordinates of Point 'D(x, y)' are : D(%d, %d)\n", pointD.x, pointD.y);
	printf("Co-ordinates of Point 'E(x, y)' are : E(%d, %d)\n", pointE.x, pointE.y);

	return(0);
}
