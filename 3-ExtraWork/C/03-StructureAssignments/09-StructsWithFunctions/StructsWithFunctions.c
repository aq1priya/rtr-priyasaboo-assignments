#include <stdio.h>

//DEFINING STRUCT 
struct MyData
{
	int i;
	float f;
	double d;
	char c;
};

int main(void)
{
	//function prototye
	struct MyData AddStructMembers(struct MyData, struct MyData, struct MyData);

	//variable declarations
	struct MyData data1, data2, data3, answer_data;

	//code
	// data 1
	printf("\n\n----------DATA 1-----------\n\n");
	printf("Enter the integer value for 'i' of struct MyData data1 : ");
	scanf("%d", &data1.i);

	printf("\n\nEnter the floating point value for 'f' of struct MyData data1 : ");
	scanf("%f", &data1.f);

	printf("\n\nEnter the double value for 'd' of struct MyData data1 : ");
	scanf("%lf", &data1.d);

	printf("\n\nEnter the character value for 'c' of struct MyData data1 : ");
	data1.c = getch();
	printf("%c", data1.c);

	// data 1
	printf("\n\n----------DATA 2-----------\n\n");
	printf("Enter the integer value for 'i' of struct MyData data2 : ");
	scanf("%d", &data2.i);

	printf("\n\nEnter the floating point value for 'f' of struct MyData data2 : ");
	scanf("%f", &data2.f);

	printf("\n\nEnter the double value for 'd' of struct MyData data2 : ");
	scanf("%lf", &data2.d);

	printf("\n\nEnter the character value for 'c' of struct MyData data2 : ");
	data1.c = getch();
	printf("%c", data2.c);

	// data 1
	printf("\n\n----------DATA 3-----------\n\n");
	printf("Enter the integer value for 'i' of struct MyData data3 : ");
	scanf("%d", &data3.i);

	printf("\n\nEnter the floating point value for 'f' of struct MyData data3 : ");
	scanf("%f", &data3.f);

	printf("\n\nEnter the double value for 'd' of struct MyData data3 : ");
	scanf("%lf", &data3.d);

	printf("\n\nEnter the character value for 'c' of struct MyData data3 : ");
	data1.c = getch();
	printf("%c", data3.c);

	//calling function AddStructMembers() acceptsthree variable of type 'struct MyData' as parameter
	//add the respective member and returns the answer in another variable 
	//of same type that is structure variable of type 'struct MyData'
	answer_data = AddStructMembers(data1, data2, data3);

	printf("\n\n----------ANSWER----------\n\n");
	printf("answer.data.i = %d\n", answer_data.i);
	printf("answer.data.f = %f\n", answer_data.f);
	printf("answer.data.d = %lf\n\n", answer_data.d);

	answer_data.c = data1.c;
	printf("answer_data.c (from data1) = %c\n", answer_data.c);
	answer_data.c = data2.c;
	printf("answer_data.c (from data2) = %c\n", answer_data.c);
	answer_data.c = data3.c;
	printf("answer_data.c (from data3) = %c\n", answer_data.c);

	return(0);
}

struct MyData AddStructMembers(struct MyData md_one, struct MyData md_two, struct MyData md_three)
{
	//variable declarations
	struct MyData answer;

	//code
	answer.i = md_one.i + md_two.i + md_three.i;
	answer.f = md_one.f + md_two.f + md_three.f;
	answer.f = md_one.d + md_two.d + md_three.d;
	return(answer);
}

