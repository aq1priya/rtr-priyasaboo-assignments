#include<stdio.h>

struct MyData
{
	int i;
	float f;
	double d;
	char c;
};

int main(void)
{
	//variable declarations
	struct MyData data;

	//code
	data.i = 30;
	data.f = 11.45f;
	data.d = 1.29945;
	data.c = 'A';

	//displaying values
	printf("\n\n");
	printf("DATA MEMBERS OF 'struct MyData' ARE : \n\n");
	printf("i = %d\n", data.i);
	printf("f = %f\n", data.f);
	printf("d = %lf\n", data.d);
	printf("c = %c\n\n", data.c);

	printf("\n");
	printf("ADDRESSES OF DATA MEMBERS OF 'structure MyData' ARE: \n\n");
	printf("'i' Occupies Addresses from %p\n", &data.i);
	printf("'f' Occupies Addresses from %p\n", &data.f);
	printf("'d' Occupies Addresses from %p\n", &data.d);
	printf("'c' Occupies Address %p\n", &data.c);

	printf("Starting address of 'struct MyData' variable 'data' = %p\n\n", &data);

	return(0);
}
