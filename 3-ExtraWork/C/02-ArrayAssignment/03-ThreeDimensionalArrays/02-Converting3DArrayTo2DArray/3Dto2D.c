#include<stdio.h>

#define NUM_ROWS 5
#define NUM_COLUMNS 3
#define DEPTH 2

int main(void)
{
	//variable declarations 
	// IN-LINE INITIALIZATION 
	int iArray[NUM_ROWS][NUM_COLUMNS][DEPTH] = { { {1, 2}, {3, 4}, {5, 6} },
							{ {10, 20}, {30, 40}, {50, 60} },
							{ {11, 12}, {13, 14}, {15, 16} },
							{ {21, 22}, {23, 24}, {25, 26} },
							{ {5, 10}, {15, 20}, {25, 30} } };

	int i, j, k;
	int iArray_2D[NUM_ROWS][NUM_COLUMNS * DEPTH];  

	//code
	//-------------DISPLAY 3D ARRAY --------
	for (i = 0; i < NUM_ROWS; i++)
	{
		printf("\n------------ROW %d-------------\n", (i + 1));
		for (j = 0; j < NUM_COLUMNS; j++)
		{
			printf("***** Column %d *****\n", (j + 1));
			for (k = 0; k < DEPTH; k++)
			{
				printf("iArray[%d][%d][%d]  =  %d\n", i, j, k, iArray[i][j][k]);
			}
			printf("\n");
		}
		printf("\n");
	}

	//converting 3d to 2d
	for (i = 0; i < NUM_ROWS; i++)
	{
		for (j = 0; j < NUM_COLUMNS; j++)
		{
			for (k = 0; k < DEPTH; k++)
			{
				iArray_2D[i][(j*DEPTH) + k] = iArray[i][j][k];
			}
		}
	}

	//display 2d array
	printf("\n\n");
	printf("Elements in the 2d array :\n\n");
	for (i = 0; i < NUM_ROWS; i++)
	{
		printf("------------------ROW %d--------------------\n",i);
		for (j = 0; j < (NUM_COLUMNS*DEPTH); j++)
		{
			printf("iArray_2D[%d][%d]  =  %d\n", i, j, iArray_2D[i][j]);
		}
		printf("\n");
	}
			
	return(0);
}