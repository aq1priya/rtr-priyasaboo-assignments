#include<stdio.h>

int main(void)
{
	//variable declarations
	int iArray[5][3][2] = { { {1, 2}, {3, 4}, {5, 6} },
							{ {10, 20}, {30, 40}, {50, 60} },
							{ {11, 12}, {13, 14}, {15, 16} },
							{ {21, 22}, {23, 24}, {25, 26} },
							{ {5, 10}, {15, 20}, {25, 30} } };
	int int_size;
	int iArray_size;
	int iArray_num_elements, iArray_width, iArray_heights, iArray_depth;

	//code
	printf("\n\n");

	int_size = sizeof(int);
	iArray_size = sizeof(iArray);
	printf("size of three Dimentional (3D) Integer Array Is = %d\n", iArray_size);

	iArray_width = iArray_size / sizeof(iArray[0]);
	printf("Number of rows (width) in the three dimentional integer array is = %d\n", iArray_width);

	iArray_heights = sizeof(iArray[0]) / sizeof(iArray[0][0]);
	printf("Number of columns (height) in the three dimentional integer array is = %d\n", iArray_heights);

	iArray_depth = sizeof(iArray[0][0]) / int_size;
	printf("Depth in the three dimentional integer array is = %d\n", iArray_depth);

	iArray_num_elements = iArray_width * iArray_heights * iArray_depth;
	printf("Number of elements in the three dimentional integer array is = %d\n", iArray_num_elements);

	printf("\n\n");
	printf("Elements in the 3D ineger array: \n");


	//PIECE-MEAL DISPLAY
	//***ROW 1***
	printf("\n\n------------------ROW 1--------------------\n");
	printf("************Column 1**************\n");
	printf("iArray[0][0][0] = %d\n", iArray[0][0][0]);
	printf("iArray[0][0][1] = %d\n", iArray[0][0][1]);
	
	printf("\n");
	printf("************Column 2**************\n");
	printf("iArray[0][1][0] = %d\n", iArray[0][1][0]);
	printf("iArray[0][1][1] = %d\n", iArray[0][1][1]);

	printf("\n");
	printf("************Column 3**************\n\n");
	printf("iArray[0][2][0] = %d\n", iArray[0][2][0]);
	printf("iArray[0][2][1] = %d\n", iArray[0][2][1]);

	//***ROW 2***
	printf("\n\n------------------ROW 2--------------------\n");
	printf("************Column 1**************\n");
	printf("iArray[1][0][0] = %d\n", iArray[1][0][0]);
	printf("iArray[1][0][1] = %d\n", iArray[1][0][1]);

	printf("\n");
	printf("************Column 2**************\n");
	printf("iArray[1][1][0] = %d\n", iArray[1][1][0]);
	printf("iArray[1][1][1] = %d\n", iArray[1][1][1]);

	printf("\n");
	printf("************Column 3**************\n\n");
	printf("iArray[1][2][0] = %d\n", iArray[1][2][0]);
	printf("iArray[1][2][1] = %d\n", iArray[1][2][1]);

	//***ROW 3***
	printf("\n\n-------------------ROW 3-------------------\n");
	printf("************Column 1**************\n");
	printf("iArray[2][0][0] = %d\n", iArray[2][0][0]);
	printf("iArray[2][0][1] = %d\n", iArray[2][0][1]);

	printf("\n");
	printf("************Column 2**************\n");
	printf("iArray[2][1][0] = %d\n", iArray[2][1][0]);
	printf("iArray[2][1][1] = %d\n", iArray[2][1][1]);

	printf("\n");
	printf("************Column 3**************\n\n");
	printf("iArray[2][2][0] = %d\n", iArray[2][2][0]);
	printf("iArray[2][2][1] = %d\n", iArray[2][2][1]);

	//***ROW 4***
	printf("\n\n-------------------ROW 4-------------------\n");
	printf("************Column 1**************\n");
	printf("iArray[3][0][0] = %d\n", iArray[3][0][0]);
	printf("iArray[3][0][1] = %d\n", iArray[3][0][1]);

	printf("\n");
	printf("************Column 2**************\n");
	printf("iArray[3][1][0] = %d\n", iArray[3][1][0]);
	printf("iArray[3][1][1] = %d\n", iArray[3][1][1]);

	printf("\n");
	printf("************Column 3**************\n\n");
	printf("iArray[3][2][0] = %d\n", iArray[3][2][0]);
	printf("iArray[3][2][1] = %d\n", iArray[3][2][1]);

	//***ROW 5***
	printf("\n\n------------------ROW 5--------------------\n");
	printf("************Column 1**************\n");
	printf("iArray[4][0][0] = %d\n", iArray[4][0][0]);
	printf("iArray[4][0][1] = %d\n", iArray[4][0][1]);

	printf("\n");
	printf("************Column 2**************\n");
	printf("iArray[4][1][0] = %d\n", iArray[4][1][0]);
	printf("iArray[4][1][1] = %d\n", iArray[4][1][1]);

	printf("\n");
	printf("************Column 3**************\n\n");
	printf("iArray[4][2][0] = %d\n", iArray[4][2][0]);
	printf("iArray[4][2][1] = %d\n", iArray[4][2][1]);

	return(0);
}