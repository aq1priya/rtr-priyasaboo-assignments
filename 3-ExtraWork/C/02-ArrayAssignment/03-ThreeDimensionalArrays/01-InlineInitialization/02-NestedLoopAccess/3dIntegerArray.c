#include<stdio.h>

int main(void)
{
	//variable declarations
	int iArray[5][3][2] = { { {1, 2}, {3, 4}, {5, 6} },
							{ {10, 20}, {30, 40}, {50, 60} },
							{ {11, 12}, {13, 14}, {15, 16} },
							{ {21, 22}, {23, 24}, {25, 26} },
							{ {5, 10}, {15, 20}, {25, 30} } };
	int int_size;
	int iArray_size;
	int iArray_num_elements, iArray_width, iArray_heights, iArray_depth;
	int i, j, k;

	//code
	printf("\n\n");

	int_size = sizeof(int);
	iArray_size = sizeof(iArray);
	printf("size of three Dimentional (3D) Integer Array Is = %d\n", iArray_size);

	iArray_width = iArray_size / sizeof(iArray[0]);
	printf("Number of rows (width) in the three dimentional integer array is = %d\n", iArray_width);

	iArray_heights = sizeof(iArray[0]) / sizeof(iArray[0][0]);
	printf("Number of columns (height) in the three dimentional integer array is = %d\n", iArray_heights);

	iArray_depth = sizeof(iArray[0][0]) / int_size;
	printf("Depth in the three dimentional integer array is = %d\n", iArray_depth);

	iArray_num_elements = iArray_width * iArray_heights * iArray_depth;
	printf("Number of elements in the three dimentional integer array is = %d\n", iArray_num_elements);

	printf("\n\n");
	printf("Elements in the 3D ineger array: \n");

	for (i = 0; i < iArray_width; i++)
	{
		printf("\n------------ROW %d-------------\n", (i + 1));
		for (j = 0; j < iArray_heights; j++)
		{
			printf("***** Column %d *****\n", (j + 1));
			for (k = 0; k < iArray_depth; k++)
			{
				printf("iArray[%d][%d][%d]  =  %d\n", i, j, k, iArray[i][j][k]);
			}
			printf("\n");
		}
		printf("\n");
	}

	return(0);
}
