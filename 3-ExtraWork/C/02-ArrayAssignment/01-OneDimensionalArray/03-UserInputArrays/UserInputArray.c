#include<stdio.h>

#define INT_ARRAY_NUM_ELEMENT 5
#define FLOAT_ARRAY_NUM_ELEMENT 3
#define CHAR_ARRAY_NUM_ELEMENT 5

int main(void)
{
	//variable declarations
	int iArray[INT_ARRAY_NUM_ELEMENT];
	float fArray[FLOAT_ARRAY_NUM_ELEMENT];
	char cArray[CHAR_ARRAY_NUM_ELEMENT];
	int i, num;

	//code
	//**********Array elements input************

	printf("\n\n");
	printf("Enter the elements for 'Integer' Array: \n");
	for (i = 0; i < INT_ARRAY_NUM_ELEMENT; i++)
	{
		scanf("%d", &iArray[i]);
	}

	printf("Enter the elements for 'Floating-point' Array: \n");
	for (i = 0; i < FLOAT_ARRAY_NUM_ELEMENT; i++)
	{
		scanf("%f", &fArray[i]);
	}

	printf("Enter the elements for 'character' Array: \n");
	for (i = 0; i < CHAR_ARRAY_NUM_ELEMENT; i++)
	{
		cArray[i] = getch();
		printf("%c\n", cArray[i]);
	}

	//************Array elements output*****************
	printf("\n\n");
	printf("Elements for 'Integer' Array: \n");
	for (i = 0; i < INT_ARRAY_NUM_ELEMENT; i++)
	{
		printf("%d\t", iArray[i]);
	}

	printf("\nElements for 'Floating-point' Array: \n");
	for (i = 0; i < FLOAT_ARRAY_NUM_ELEMENT; i++)
	{
		printf("%f\t", fArray[i]);
	}

	printf("\nElements for 'character' Array: \n");
	for (i = 0; i < CHAR_ARRAY_NUM_ELEMENT; i++)
	{
		printf("%c\t", cArray[i]);
	}
	return(0);
}