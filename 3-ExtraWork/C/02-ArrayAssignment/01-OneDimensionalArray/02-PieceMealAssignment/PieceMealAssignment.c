#include<stdio.h>

int main(void)
{
	//variable declarations
	int iArrayOne[10];
	int iArrayTwo[10];

	//code

	//*********ArrayOne[]**********
	iArrayOne[0] = 3;
	iArrayOne[1] = 6;
	iArrayOne[2] = 9;
	iArrayOne[3] = 12;
	iArrayOne[4] = 15;
	iArrayOne[5] = 18;
	iArrayOne[6] = 21;
	iArrayOne[7] = 24;
	iArrayOne[8] = 27;
	iArrayOne[9] = 30;
	
	printf("\n\n");
	printf("Piece Meal (Hardcoded) Assignment and display of Elements in array 'iArrayOne[]' : \n\n");
	printf("1st element of array 'iArrayOne' or element at 0th index of array 'iArrayOne[]' = %d\n", iArrayOne[0]);
	printf("2nd element of array 'iArrayOne' or element at 1st index of array 'iArrayOne[]' = %d\n", iArrayOne[1]);
	printf("3rd element of array 'iArrayOne' or element at 2nd index of array 'iArrayOne[]' = %d\n", iArrayOne[2]);
	printf("4th element of array 'iArrayOne' or element at 3rd index of array 'iArrayOne[]' = %d\n", iArrayOne[3]);
	printf("5th element of array 'iArrayOne' or element at 4th index of array 'iArrayOne[]' = %d\n", iArrayOne[4]);
	printf("6th element of array 'iArrayOne' or element at 5th index of array 'iArrayOne[]' = %d\n", iArrayOne[5]);
	printf("7th element of array 'iArrayOne' or element at 6th index of array 'iArrayOne[]' = %d\n", iArrayOne[6]);
	printf("8th element of array 'iArrayOne' or element at 7th index of array 'iArrayOne[]' = %d\n", iArrayOne[7]);
	printf("9th element of array 'iArrayOne' or element at 8th index of array 'iArrayOne[]' = %d\n", iArrayOne[8]);
	printf("10th element of array 'iArrayOne' or element at 9th index of array 'iArrayOne[]' = %d\n", iArrayOne[9]);

	//*********iArrayTwo[]********
	printf("\n\n");
	printf("Enter the 1st element of 'iArrayTwo[]' : ");
	scanf("%d", &iArrayTwo[0]);
	printf("\nEnter the 2nd element of 'iArrayTwo[]' : ");
	scanf("%d", &iArrayTwo[1]);
	printf("\nEnter the 3rd element of 'iArrayTwo[]' : ");
	scanf("%d", &iArrayTwo[2]);
	printf("\nEnter the 4th element of 'iArrayTwo[]' : ");
	scanf("%d", &iArrayTwo[3]);
	printf("\nEnter the 5th element of 'iArrayTwo[]' : ");
	scanf("%d", &iArrayTwo[4]);
	printf("\nEnter the 5th element of 'iArrayTwo[]' : ");
	scanf("%d", &iArrayTwo[4]);
	printf("\nEnter the 6th element of 'iArrayTwo[]' : ");
	scanf("%d", &iArrayTwo[5]);
	printf("\nEnter the 7th element of 'iArrayTwo[]' : ");
	scanf("%d", &iArrayTwo[6]);
	printf("\nEnter the 8th element of 'iArrayTwo[]' : ");
	scanf("%d", &iArrayTwo[7]);
	printf("\nEnter the 9th element of 'iArrayTwo[]' : ");
	scanf("%d", &iArrayTwo[8]);
	printf("\nEnter the 10th element of 'iArrayTwo[]' : ");
	scanf("%d", &iArrayTwo[9]);

	printf("\n\n");
	printf("Piece Meal (user input) Assignment and display of Elements in array 'iArrayOne[]' : \n\n");
	printf("1st element of array 'iArrayTwo' or element at 0th index of array 'iArrayTwo[]' = %d\n", iArrayTwo[0]);
	printf("2st element of array 'iArrayTwo' or element at 1st index of array 'iArrayTwo[]' = %d\n", iArrayTwo[1]);
	printf("3rd element of array 'iArrayTwo' or element at 2nd index of array 'iArrayTwo[]' = %d\n", iArrayTwo[2]);
	printf("4th element of array 'iArrayTwo' or element at 3rd index of array 'iArrayTwo[]' = %d\n", iArrayTwo[3]);
	printf("5th element of array 'iArrayTwo' or element at 4th index of array 'iArrayTwo[]' = %d\n", iArrayTwo[4]);
	printf("6th element of array 'iArrayTwo' or element at 5th index of array 'iArrayTwo[]' = %d\n", iArrayTwo[5]);
	printf("7th element of array 'iArrayTwo' or element at 6th index of array 'iArrayTwo[]' = %d\n", iArrayTwo[6]);
	printf("8th element of array 'iArrayTwo' or element at 7th index of array 'iArrayTwo[]' = %d\n", iArrayTwo[7]);
	printf("9th element of array 'iArrayTwo' or element at 8th index of array 'iArrayTwo[]' = %d\n", iArrayTwo[8]);
	printf("10th element of array 'iArrayTwo' or element at 9th index of array 'iArrayTwo[]' = %d\n", iArrayTwo[9]);

	return(0);
}
