#include<stdio.h>

#define NUM_ELEMENTS 10

int main(void)
{
	//variable declarations
	int iArray[NUM_ELEMENTS];
	int i, sum = 0, num;

	//code
	printf("\n\n");
	printf("Enter Integer Elements for Array:\n\n");
	for (i = 0; i < NUM_ELEMENTS; i++)
	{
		scanf("%d", &num);
		iArray[i] = num;
	}

	for (i = 0; i < NUM_ELEMENTS; i++)
	{
		sum = sum + iArray[i];
	}

	printf("\nSum of All Elements of Array = %d\n", sum);

	return(0);
}