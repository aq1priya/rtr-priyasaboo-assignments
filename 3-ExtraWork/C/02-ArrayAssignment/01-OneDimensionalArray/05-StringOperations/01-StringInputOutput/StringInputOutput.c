#include<stdio.h>

#define MAX_STRING_LENGTH 512

int main(void)
{
	//variable declarations
	char chArray[MAX_STRING_LENGTH];	//A Charater Array

	//code 
	printf("\n");
	printf("Entr the String: \n\n");
	gets_s(chArray, MAX_STRING_LENGTH);

	printf("\n");
	printf("String Entered by You is: \n");
	printf("%s\n", chArray);
	return(0);
}