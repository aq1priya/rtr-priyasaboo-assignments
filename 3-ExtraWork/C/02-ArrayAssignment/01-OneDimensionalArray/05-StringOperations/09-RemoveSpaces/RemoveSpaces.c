#include <stdio.h>

#define MAX_STRING_LENGTH 512

int main(void)
{
	//function prototype
	int MyStrlen(char[]);

	//variable declarations
	char chArray[MAX_STRING_LENGTH], chArray_SpacesRemoved[MAX_STRING_LENGTH];
	int iStringLength = 0;
	int i, j;

	//code
	//String input
	printf("\n\n");
	printf("Enter a string: \n");
	gets_s(chArray, MAX_STRING_LENGTH);

	iStringLength = MyStrlen(chArray);
	j = 0;
	for (i = 0; i < iStringLength; i++)
	{
		if (chArray[i] == ' ')
		{
			continue;
		}
		else
		{
			chArray_SpacesRemoved[j] = chArray[i];
			j++;
		}
	}

	chArray_SpacesRemoved[j] = '\0';

	//string output
	printf("\n\n");
	printf("String entered by you is: \n\n");
	printf("%s\n", chArray);

	printf("\n\n");
	printf("String after removal of spaces is: \n\n");
	printf("%s\n", chArray_SpacesRemoved);

	return(0);
}

int MyStrlen(char str[])
{
	//variable declarations
	int j;
	int string_length = 0;

	//code
	// ** DETERMINING EXACT LENGTH OF THE STRING, BY DETECTING THE FIRST OCCURANCE OF
	//NULL-TERMINATING CHARACTER (\0) ***

	for (j = 0; j < MAX_STRING_LENGTH; j++)
	{
		if (str[j] == '\0')
			break;
		else
			string_length++;
	}
	return(string_length);
}