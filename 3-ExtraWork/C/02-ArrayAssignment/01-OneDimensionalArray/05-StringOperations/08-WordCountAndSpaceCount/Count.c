#include<stdio.h>

#define MAX_STRING_LENGTH 512

int main(void)
{
	//function prototype
	int MyStrlen(char[]);

	//variable declarations
	char chArray[MAX_STRING_LENGTH];
	int iStringLength = 0;
	int i;
	int word_count = 0, space_count = 0;

	//code
	//string input
	printf("\n\n");
	printf("Enter a string: \n");
	gets_s(chArray, MAX_STRING_LENGTH);

	iStringLength = MyStrlen(chArray);

	for (i = 0; i < iStringLength; i++)
	{
		switch (chArray[i])
		{
		case 32:	//' '
			space_count++;
			break;
		default:
			break;
		}
	}

	word_count = space_count + 1;

	//string output
	printf("\n\n");
	printf("String entered by you is: \n\n");
	printf("%s\n", chArray);

	printf("\n\n");
	printf("Number of spaces in the input string is = %d \n\n", space_count);
	printf("Number of words in the input string is = %d \n\n", word_count);
	return(0);
}

int MyStrlen(char str[])
{
	//variable declarations
	int j;
	int string_length = 0;

	//code
	// ** DETERMINING EXACT LENGTH OF THE STRING, BY DETECTING THE FIRST OCCURANCE OF
	//NULL-TERMINATING CHARACTER (\0) ***

	for (j = 0; j < MAX_STRING_LENGTH; j++)
	{
		if (str[j] == '\0')
			break;
		else
			string_length++;
	}
	return(string_length);
}