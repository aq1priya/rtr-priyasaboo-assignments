#include <stdio.h>

int main(void)
{
	//variable declarations
	char chArray_01[] = { 'A', 'S','T','R','O','M','E','D','I','C','O','M','P','\0' };
	char chArray_02[9] = { 'W', 'E','L','C','O','M','E','S','\0' };
	char chArray_03[9] = { 'Y', 'O','U','\0' };
	char chArray_04[] ="To";
	char chArray_05[] ="Real Time Rendering Batch of 2018-19-20";

	char chArray_withoutNullTerminator[] = { 'H', 'E','L','L','O' };

	//code
	printf("\n\n");

	printf("Size of chArray_01 : %d\n", sizeof(chArray_01));
	printf("Size of chArray_02 : %d\n", sizeof(chArray_02));
	printf("Size of chArray_03 : %d\n", sizeof(chArray_03));
	printf("Size of chArray_04 : %d\n", sizeof(chArray_04));
	printf("Size of chArray_05 : %d\n", sizeof(chArray_05));
	
	printf("\n\n");

	printf("the strings are :\n\n");
	printf("chArray_01 : %s\n", chArray_01);
	printf("chArray_02 : %s\n", chArray_02);
	printf("chArray_03 : %s\n", chArray_03);
	printf("chArray_04 : %s\n", chArray_04);
	printf("chArray_05 : %s\n", chArray_05);

	printf("\n\n");
	printf("Size of chArray_withoutNullTerminator : %d\n", sizeof(chArray_withoutNullTerminator));
	printf("chArray_withoutNullTerminator : %s\n", chArray_withoutNullTerminator);

	return(0);

}