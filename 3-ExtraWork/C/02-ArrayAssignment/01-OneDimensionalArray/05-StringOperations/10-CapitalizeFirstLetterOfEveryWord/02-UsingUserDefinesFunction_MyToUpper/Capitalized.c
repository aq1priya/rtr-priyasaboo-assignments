#include <stdio.h>

#define MAX_STRING_LENGTH 512
#define SPACE ' '
#define FULLSTOP '.'
#define EXCLAMATION '!'
#define QUESTION_MARK '?'
#define COMMA ','

int main(void)
{
	//function prototype
	int MyStrlen(char[]);
	char MyToUpper(char);

	//variable declarations
	char chArray[MAX_STRING_LENGTH], chArray_Capitalized[MAX_STRING_LENGTH];
	int iStringLength = 0;
	int i, j;
	
	//code
	//String input
	printf("\n\n");
	printf("Enter a string: \n");
	gets_s(chArray, MAX_STRING_LENGTH);

	iStringLength = MyStrlen(chArray);
	j = 0;
	for (i = 0; i < iStringLength; i++)
	{
		if (i == 0)//First letter of any stencence must be capital letter
			chArray_Capitalized[j] = MyToUpper(chArray[i]);

		else if (chArray[i] == SPACE)//First letter of every word in the
			//sentence must be capital letter. words are separated by spaces
		{
			chArray_Capitalized[i] = chArray[i];
			chArray_Capitalized[j + 1] = MyToUpper(chArray[i + 1]);
			//SINCE, ALREADY TWO CHARACTERS (AT INDICES 'i' AND 'i+1' HAVE BEEN
			//CONSIDERED IN THIS ELSE-IF BLOCK, WE ARE EXTRA INCREMENTING 'i' AND 'j' BY 1
			j++;
			i++;
		}

		else if ((chArray[i] == FULLSTOP || chArray[i] == COMMA || chArray[i] == EXCLAMATION
			|| chArray[i] == QUESTION_MARK) && (chArray[i] != SPACE))
			//First letter of every word after punctuation mark,
			//sentence must be capital letter. words are separated by punctuations.
		{
			chArray_Capitalized[i] = chArray[i];
			chArray_Capitalized[j + 1] = SPACE;
			chArray_Capitalized[j + 2] = MyToUpper(chArray[i + 1]);
			//SINCE, ALREADY TWO CHARACTERS (AT INDICES 'i' AND 'i+1' HAVE BEEN CONSIDERED 
			//IN THIS ELSE-IF BLOCK, WE ARE EXTRA INCREMENTING 'i' AND 'j' BY 1
			//SINCE, ALREADY THREE CHARACTERS (AT INDICES 'j' AND 'j+1' AND 'j+2' HAVE
			//BEEN CONSIDERED IN THIS ELSE-IF BLOCK, WE ARE EXTRA INCREMENTING 'j' BY 2
			j = j + 2;
			i++;
		}
		else
		{
			chArray_Capitalized[j] = chArray[i];
		}
		j++;
	}
	chArray_Capitalized[j] = '\0';

	//string output
	printf("\n\n");
	printf("String entered by you is: \n\n");
	printf("%s\n", chArray);

	printf("\n\n");
	printf("String after capitalizing first letter of every word is: \n\n");
	printf("%s\n", chArray_Capitalized);

	return(0);
}

int MyStrlen(char str[])
{
	//variable declarations
	int j;
	int string_length = 0;

	//code
	// ** DETERMINING EXACT LENGTH OF THE STRING, BY DETECTING THE FIRST OCCURANCE OF
	//NULL-TERMINATING CHARACTER (\0) ***

	for (j = 0; j < MAX_STRING_LENGTH; j++)
	{
		if (str[j] == '\0')
			break;
		else
			string_length++;
	}
	return(string_length);
}

char MyToUpper(char ch)
{
	//variable declaration 
	int num;
	int c;

	//code
	//ASCII VALUE OF 'a' (97) - ASCII VALUE OF 'A' (65)  = 32
	//THIS SUBSTRACTION WILL GIVE EXACT DIFFERENCE BETWEEN THE  UPPER AND LOWER CASE
	//COUNTERPARTS OF EACH LETTER OF THE ALPHABET
	//IF THIS DIFFERENCE IS SUBSTRACTED FROM THE ASCII VALUE OF ALOWER CASE LETTER,
	//THE RESULTANT ASCII VALUE WILLBE THAT OF ITS UPPER CASE LETTER
	//HELPING US TO FIND ITS UPPER CASE LETTER !!!
	//ASCII VALUES OF 'a' TO 'z' = 97 TO 122
	//ASCII VALUES OF 'A' TO 'Z' = 65 TO 90

	num = 'a' - 'A';

	if ((int)ch >= 97 && (int)ch <= 122)
	{
		c = (int)ch - num;
		return((char)c);
	}
	else
		return(ch);
}