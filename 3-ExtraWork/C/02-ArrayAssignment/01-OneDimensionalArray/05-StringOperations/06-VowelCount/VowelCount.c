#include<stdio.h>

#define MAX_STRING_LENGTH 512

int main(void)
{
	//function prototype
	int MyStrlen(char[]);

	//variable declarations
	char chArray[MAX_STRING_LENGTH];
	int iStringLength;
	int count_A = 0, count_E = 0, count_I = 0, count_O = 0, count_U = 0;
	int i;

	//sring input
	printf("\n\n");
	printf("Enter A String: \n");
	gets_s(chArray, MAX_STRING_LENGTH);

	// *** STRING OUTPUT ***
	printf("\n\n");
	printf("String entered by you is: \n\n");
	printf("%s\n", chArray);

	iStringLength = MyStrlen(chArray);

	for (i = 0; i < iStringLength; i++)
	{
		switch (chArray[i])
		{
		case 'A':
		case 'a':
			count_A++;
			break;

		case 'E':
		case 'e':
			count_E++;
			break;

		case 'I':
		case 'i':
			count_I++;
			break;

		case 'O':
		case 'o':
			count_O++;
			break;

		case 'U':
		case 'u':
			count_U++;
			break;

		default:
			break;
		}
	}

	printf("\n\n");
	printf("In the string entered by you, the vowels and the number of their occurances are follows : \n\n");
	printf("'A' has occoured- %d times!!!\n", count_A);
	printf("'E' has occoured- %d times!!!\n", count_E);
	printf("'I' has occoured- %d times!!!\n", count_I);
	printf("'O' has occoured- %d times!!!\n", count_O);
	printf("'U' has occoured- %d times!!!\n", count_U);

	return(0);
}

int MyStrlen(char str[])
{
	//variable declarations
	int j;
	int string_length = 0;

	//code
	// ** DETERMINING EXACT LENGTH OF THE STRING, BY DETECTING THE FIRST OCCURANCE OF
	//NULL-TERMINATING CHARACTER (\0) ***

	for (j = 0; j < MAX_STRING_LENGTH; j++)
	{
		if (str[j] == '\0')
			break;
		else
			string_length++;
	}
	return(string_length);
}