#include<stdio.h>

#define MAX_STRING_LENGTH 512

int main(void)
{
	//function prototype
	void MyStrrev(char[], char[]);

	//variable declarations
	char chArray_One[MAX_STRING_LENGTH], chArray_Two[MAX_STRING_LENGTH];

	//code
	//String Input
	printf("\n\n");
	printf("Enter First String: \n");
	gets_s(chArray_One, MAX_STRING_LENGTH);

	printf("\n\n");
	printf("Enter Second String: \n");
	gets_s(chArray_Two, MAX_STRING_LENGTH);

	//***STRING CONCATENATE
	printf("\n\n");
	printf("\n**************Before Concatenation*****************\n");
	printf("The Original First String Entered By You is :\n");
	printf("%s\n", chArray_One);

	printf("\n\n");
	printf("The Original Second String Entered By You is :\n");
	printf("%s\n", chArray_Two);

	strcat(chArray_One, chArray_Two);

	printf("\n\n");
	printf("\n**************After Concatenation*****************\n");
	printf("'chArray_One[] is: \n");
	printf("%s\n", chArray_One);

	printf("'chArray_One[] is: \n");
	printf("%s\n", chArray_Two);

	return(0);
}