#include<stdio.h>

#define MAX_STRING_LENGTH 512

int main(void)
{
	//function prototype
	int MyStrlen(char[]);

	//variable declarations
	char chArray[MAX_STRING_LENGTH];
	int iStringLength = 0;

	//code
	// *** STRING INPUT ***
	printf("\n\n");
	printf("Enter a string: \n");
	gets_s(chArray, MAX_STRING_LENGTH);

	// *** STRING OUTPUT ***
	printf("\n\n");
	printf("String entered by you is: \n");
	printf("%s\n", chArray);

	// *** STRING LENGTH ***
	printf("\n\n");
	iStringLength = MyStrlen(chArray);
	printf("Length of string is = %d Characters!!!\n", iStringLength);

	return(0);
}

int MyStrlen(char str[])
{
	//variable declarations
	int j;
	int string_length = 0;

	//code
	// ** DETERMINING EXACT LENGTH OF THE STRING, BY DETECTING THE FIRST OCCURANCE OF
	//NULL-TERMINATING CHARACTER (\0) ***

	for (j = 0; j < MAX_STRING_LENGTH; j++)
	{
		if (str[j] == '\0')
			break;
		else
			string_length++;
	}
	return(string_length);
}
