#include <stdio.h>

#define MAX_STRING_LENGTH 512

int main(void)
{
	//variable declarations
	char chArray[MAX_STRING_LENGTH]; //a character array is a string
	int iStringLength = 0;

	//code

	// *** STRING INPUT ***
	printf("\n\n");
	printf("Enter A String: \n");
	gets_s(chArray, MAX_STRING_LENGTH);

	// *** STRING OUTPUT ***
	printf("\n\n");
	printf("String entered by you is: \n\n");
	printf("%s\n", chArray);

	// *** STRING LENGTH ***
	printf("\n\n");
	iStringLength = strlen(chArray);
	printf("Length Of String Is = %d Characters !!!\n\n", iStringLength);

	return(0);
}