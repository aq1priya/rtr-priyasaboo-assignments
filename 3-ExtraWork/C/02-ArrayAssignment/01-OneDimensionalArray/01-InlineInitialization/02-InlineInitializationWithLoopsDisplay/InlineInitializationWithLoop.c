#include<stdio.h>

int main(void)
{
	//variable declarations
	int iArray[] = { 9, 10, 11, 21, 13, 14, 15, 16, 17, 18 };
	int int_size;
	int iArray_size;
	int iArray_num_elements;

	float fArray[] = { 1.2f, 2.3f, 3.4f, 4.5f, 5.6f };
	int float_size;
	int fArray_size;
	int fArray_num_elements;

	char cArray[] = { 'p','r','i','y','a' };
	int char_size;
	int cArray_size;
	int cArray_num_elements;

	int i;

	//code
	//***********iArray[]****************//
	printf("\niArray[]\n");
	printf("In-Line initialization and Loop for display of elements of array iArray[]\n");

	int_size = sizeof(int);
	iArray_size = sizeof(iArray);
	iArray_num_elements = iArray_size / int_size;

	for (i = 0; i < iArray_num_elements; i++)
	{
		printf("iArray[%d] (Element %d) = %d\n", i, (i + 1), iArray[i]);
	}
	
	printf("\n\n");
	printf("Size of Data type 'int' = %d bytes\n", int_size);
	printf("Number of Elements in 'int' Array 'iArray' = %d elements\n", iArray_num_elements);
	printf("Size of Array 'iArray[]' (%d elements * %d Bytes) = %d bytes\n\n", iArray_num_elements, int_size, iArray_size);

	//************fArray[]************
	printf("\n\n");
	printf("In-Line initialization and Loop for display of elements of array 'fArray[]'\n");
	
	float_size = sizeof(float);
	fArray_size = sizeof(fArray);
	fArray_num_elements = fArray_size / float_size;

	i = 0;
	while(i < fArray_num_elements)
	{
		printf("fArray[%d] (Element %d) = %d\n", i, (i + 1), iArray[i]);
		i++;
	}
	
	printf("Size of Data type 'float' = %d bytes\n", float_size);
	printf("Number of Elements in 'float' Array 'fArray' = %d elements\n", fArray_num_elements);
	printf("Size of Array 'fArray[]' (%d elements * %d Bytes) = %d bytes\n\n", fArray_num_elements, float_size, fArray_size);

	//*************cArray**************
	printf("\n\n");
	printf("In-Line initialization and piece meal display of elements of array 'cArray[]'\n");

	char_size = sizeof(char);
	cArray_size = sizeof(cArray);
	cArray_num_elements = cArray_size / char_size;
	
	i = 0;
	do
	{
		printf("cArray[%d] (Element %d) = %c\n", i, (i + 1), cArray[i]);
		i++;
	} while (i < fArray_num_elements);
	
	printf("Size of Data type 'chart' = %d bytes\n", char_size);
	printf("Number of Elements in 'char' Array 'fArray' = %d elements\n", cArray_num_elements);
	printf("Size of Array 'cArray[]' (%d elements * %d Bytes) = %d bytes\n\n", cArray_num_elements, char_size, cArray_size);

	return(0);
}