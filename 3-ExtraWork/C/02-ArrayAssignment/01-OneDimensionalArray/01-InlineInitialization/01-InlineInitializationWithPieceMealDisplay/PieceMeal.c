#include<stdio.h>

int main(void)
{
	//variable declarations
	int iArray[] = { 9, 10, 11, 21, 13, 14, 15, 16, 17, 18 };
	int int_size;
	int iArray_size;
	int iArray_num_elements;

	float fArray[] = { 1.2f, 2.3f, 3.4f, 4.5f, 5.6f };
	int float_size;
	int fArray_size;
	int fArray_num_elements;

	char cArray[] = { 'p','r','i','y','a' };
	int char_size;
	int cArray_size;
	int cArray_num_elements;

	//code
	//***********iArray[]****************//
	printf("\niArray[]\n");
	printf("In-Line initialization and piece meal display of elementsof array iArray[]\n");
	printf("iArray[0] (1st Element) = %d\n", iArray[0]);
	printf("iArray[1] (2nd Element) = %d\n", iArray[1]);
	printf("iArray[2] (3rd Element) = %d\n", iArray[2]);
	printf("iArray[3] (4th Element) = %d\n", iArray[3]);
	printf("iArray[4] (5th Element) = %d\n", iArray[4]);
	printf("iArray[5] (6th Element) = %d\n", iArray[5]);
	printf("iArray[6] (7th Element) = %d\n", iArray[6]);
	printf("iArray[7] (8th Element) = %d\n", iArray[7]);
	printf("iArray[8] (9th Element) = %d\n", iArray[8]);
	printf("iArray[9] (10th Element) = %d\n", iArray[9]);

	int_size = sizeof(int);
	iArray_size = sizeof(iArray);
	iArray_num_elements = iArray_size / int_size;
	printf("Size of Data type 'int' = %d bytes\n", int_size);
	printf("Number of Elements in 'int' Array 'iArray' = %d elements\n", iArray_num_elements);
	printf("Size of Array 'iArray[]' (%d elements * %d Bytes) = %d bytes\n\n",iArray_num_elements, int_size, iArray_size);

	//************fArray[]************
	printf("\n\n");
	printf("In-Line initialization and piece meal display of elements of array 'fArray[]'\n");
	printf("fArray[0] (1st Element) = %f\n", fArray[0]);
	printf("fArray[1] (2nd Element) = %f\n", fArray[1]);
	printf("fArray[2] (3rd Element) = %f\n", fArray[2]);
	printf("fArray[3] (4th Element) = %f\n", fArray[3]);
	printf("fArray[4] (5th Element) = %f\n", fArray[4]);

	float_size = sizeof(float);
	fArray_size = sizeof(fArray);
	fArray_num_elements = fArray_size / float_size;
	printf("Size of Data type 'float' = %d bytes\n", float_size);
	printf("Number of Elements in 'float' Array 'fArray' = %d elements\n", fArray_num_elements);
	printf("Size of Array 'fArray[]' (%d elements * %d Bytes) = %d bytes\n\n", fArray_num_elements, float_size, fArray_size);

	//*************cArray**************
	printf("\n\n");
	printf("In-Line initialization and piece meal display of elements of array 'cArray[]'\n");
	printf("fArray[0] (1st Element) = %f\n", fArray[0]);
	printf("fArray[1] (2nd Element) = %f\n", fArray[1]);
	printf("fArray[2] (3rd Element) = %f\n", fArray[2]);
	printf("fArray[3] (4th Element) = %f\n", fArray[3]);
	printf("fArray[4] (5th Element) = %f\n", fArray[4]);

	char_size = sizeof(char);
	cArray_size = sizeof(cArray);
	cArray_num_elements = cArray_size / char_size;
	printf("Size of Data type 'chart' = %d bytes\n", char_size);
	printf("Number of Elements in 'char' Array 'fArray' = %d elements\n", cArray_num_elements);
	printf("Size of Array 'cArray[]' (%d elements * %d Bytes) = %d bytes\n\n", cArray_num_elements, char_size, cArray_size);

	return(0);

}