#include<stdio.h>

int main(void)
{
	//variable declarations
	int iArray[5][3] = { {1, 2, 3}, {2, 4, 6}, {3, 6, 9}, {4, 8, 12}, {5, 10, 15} };
	//IN_LINE INITIALIZATION
	int int_size;
	int iArray_size;
	int iArray_num_elements, iArray_num_rows, iArray_num_columns;

	//code
	printf("\n\n");

	int_size = sizeof(int);
	iArray_size = sizeof(iArray);
	printf("size of two Dimentional (2D) Integer Array Is = %d\n", iArray_size);

	iArray_num_rows = iArray_size / sizeof(iArray[0]);
	printf("Number of rows in the two dimentional integer array is = %d\n", iArray_num_rows);

	iArray_num_columns = sizeof(iArray[0]) / int_size;
	printf("Number of columns in the two dimentional integer array is = %d\n", iArray_num_columns);

	iArray_num_elements = iArray_num_rows * iArray_num_columns;
	printf("Number of elements in the two dimentional integer array is = %d\n", iArray_num_elements);

	printf("\n\n");
	printf("Number of elements in the array: \n");

	//ARRAY INDICS STARTS FROM 0, HENCE 1ST ROW IS ACTUALLY 0TH ROW AND FIRST COLUMN
	//IS ACTUALLY 0TH COLUMN

	//***ROW 1***
	printf("\n************Row 1**************\n");
	printf("iArray[0][0] = %d\n", iArray[0][0]);
	printf("iArray[0][1] = %d\n", iArray[0][1]);
	printf("iArray[0][2] = %d\n", iArray[0][2]);

	//***ROW 2***
	printf("\n************Row 2**************\n");
	printf("iArray[1][0] = %d\n", iArray[1][0]);
	printf("iArray[1][1] = %d\n", iArray[1][1]);
	printf("iArray[1][2] = %d\n", iArray[1][2]);

	//***ROW 3***
	printf("\n************Row 3**************\n");
	printf("iArray[2][0] = %d\n", iArray[2][0]);
	printf("iArray[2][1] = %d\n", iArray[2][1]);
	printf("iArray[2][2] = %d\n", iArray[2][2]);

	//***ROW 4***
	printf("\n************Row 4**************\n");
	printf("iArray[3][0] = %d\n", iArray[3][0]);
	printf("iArray[3][1] = %d\n", iArray[3][1]);
	printf("iArray[3][2] = %d\n", iArray[3][2]);

	//***ROW 5***
	printf("\n************Row 5**************\n");
	printf("iArray[4][0] = %d\n", iArray[4][0]);
	printf("iArray[4][1] = %d\n", iArray[4][1]);
	printf("iArray[4][2] = %d\n", iArray[4][2]);

	return(0);
}