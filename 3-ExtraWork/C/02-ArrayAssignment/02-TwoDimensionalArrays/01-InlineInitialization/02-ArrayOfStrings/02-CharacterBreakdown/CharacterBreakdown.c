#include <stdio.h>

#define MAX_STRING_LENGTH 512

int main(void)
{
	//function prototype
	int MyStrlen(char[]);

	//variable declarations

	/*
		A STRING IS AN ARRAY OF CHARACTERS, SO char[] IS A char ARRAY AND HENCE,
		char[] IS A STRING.
		AN ARRAY OF char ARRAY IS AN ARRAY OF STRINGS !!!
		HENCE, char[] IS ONE char ARRAY AND HENCE, IS ONE STRING.
		HENCE, char[][] IS AN ARRAY OF char ARRAYS AND HENCE, IS AN ARRAY OF STRING.
	*/

	/*
		Here, the string array can allow a maximum number of 10 strings(10 rows)
		and each of these 10 strings can haveonly upto 15 characters maximum (15 columns)
	*/

	char strArray[10][15] = { "Hello!", "Welcome", "To", "Real", "Time", "Rendering",
	"Batch", "(2018-19-20)", "Of", "AstroMediComp" }; //in-line initialization
	/*
		1d array - stores length of those strings at corresponding indices in strArray[]
		e.g. iStrLengths[0] will be the length of string at strArray[0]..and so on.
		10 strings, 10 lengths
	*/
	int iStrLengths[10];
	int strArray_size, strArray_num_rows;
	int i, j;

	//code
	strArray_size = sizeof(strArray);
	strArray_num_rows = strArray_size / sizeof(strArray[0]);

	//storing in lengths of all the strings...
	for (i = 0; i < strArray_num_rows; i++)
	{
		iStrLengths[i] = MyStrlen(strArray[i]);
	}
	printf("\n\n");
	printf("The entire string array: \n\n");
	for (i = 0; i < strArray_num_rows; i++)
	{
		printf("%s", strArray[i]);
	}

	printf("\n\n");
	printf("strings in the 2d array:\n");

	for (i = 0; i < strArray_num_rows; i++)
	{
		printf("String number %d => %s\n\n", (i + 1), strArray[i]);
		for (j = 0; j < iStrLengths[i]; j++)
		{
			printf("Character %d = %c\n", (j + 1), strArray[i][j]);
		}
		printf("\n\n");
	}
	return(0);
}

int MyStrlen(char str[])
{
	//variable declarations
	int j;
	int string_length = 0;

	//code
	// ** DETERMINING EXACT LENGTH OF THE STRING, BY DETECTING THE FIRST OCCURANCE OF
	//NULL-TERMINATING CHARACTER (\0) ***

	for (j = 0; j < MAX_STRING_LENGTH; j++)
	{
		if (str[j] == '\0')
			break;
		else
			string_length++;
	}
	return(string_length);
}