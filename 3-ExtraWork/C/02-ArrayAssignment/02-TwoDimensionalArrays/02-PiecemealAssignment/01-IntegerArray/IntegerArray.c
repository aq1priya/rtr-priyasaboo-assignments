#include <stdio.h>

int main(void)
{
	//variable declarations
	int iArray[3][5];
	int int_size;
	int iArray_size;
	int iArray_num_elements, iArray_num_rows, iArray_num_columns;
	int i, j;

	//code
	printf("\n\n");

	int_size = sizeof(int);
	iArray_size = sizeof(iArray);
	printf("size of two Dimentional (2D) Integer Array Is = %d\n", iArray_size);

	iArray_num_rows = iArray_size / sizeof(iArray[0]);
	printf("Number of rows in the two dimentional integer array is = %d\n", iArray_num_rows);

	iArray_num_columns = sizeof(iArray[0]) / int_size;
	printf("Number of columns in the two dimentional integer array is = %d\n", iArray_num_columns);

	iArray_num_elements = iArray_num_rows * iArray_num_columns;
	printf("Number of elements in the two dimentional integer array is = %d\n", iArray_num_elements);

	printf("\n\n");
	printf("Number of elements in the array: \n");

	// ***** piecemeal assignment ******
	// ***** ROW 1 ******
	iArray[0][0] = 21;
	iArray[0][1] = 42;
	iArray[0][2] = 63;
	iArray[0][3] = 84;
	iArray[0][4] = 105;

	// ***** ROW 2 *****
	iArray[1][0] = 21;
	iArray[1][1] = 42;
	iArray[1][2] = 63;
	iArray[1][3] = 84;
	iArray[1][4] = 105;

	// ***** ROW 3 *****
	iArray[2][0] = 21;
	iArray[2][1] = 42;
	iArray[2][2] = 63;
	iArray[2][3] = 84;
	iArray[2][4] = 105;

	// *** Display ***
	for (i = 0; i < iArray_num_rows; i++)
	{
		printf("\n****** ROW %d ******\n", (i + 1));
		for (j = 0; j < iArray_num_columns; j++)
		{
			printf("iArray[%d][%d] = %d\n", i, j, iArray[i][j]);
		}
		printf("\n");
	}
	return(0);

}