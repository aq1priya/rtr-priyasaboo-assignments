#include <stdio.h>

#define MAX_STRING_LENGTH 512

int main(void)
{
	//function prototype
	void MyStrcpy(char[], char[]);

	//variable declarations
	/*
		A STRING IS AN ARRAY OF CHARACTERS, SO char[] IS A char ARRAY AND HENCE,
		char[] IS A STRING.
		AN ARRAY OF char ARRAY IS AN ARRAY OF STRINGS !!!
		HENCE, char[] IS ONE char ARRAY AND HENCE, IS ONE STRING.
		HENCE, char[][] IS AN ARRAY OF char ARRAYS AND HENCE, IS AN ARRAY OF STRING.
	*/

	/*
		Here, the string array can allow a maximum number of 10 strings(10 rows)
		and each of these 10 strings can haveonly upto 15 characters maximum (15 columns)
	*/

	char strArray[5][10]; 
	int char_size;
	int strArray_size, strArray_num_elements, strArray_num_rows, strArray_num_columns;
	int i;

	//code
	printf("\n\n");

	char_size = sizeof(char);

	strArray_size = sizeof(strArray);
	printf("size of two Dimentional (2D) character Array Is = %d\n", strArray_size);

	strArray_num_rows = strArray_size / sizeof(strArray[0]);
	printf("Number of rows in the two dimentional character array is = %d\n", strArray_num_rows);

	strArray_num_columns = sizeof(strArray[0]) / char_size;
	printf("Number of columns in the two dimentional character array is = %d\n", strArray_num_columns);

	strArray_num_elements = strArray_num_rows * strArray_num_columns;
	printf("Number of elements in the two dimentional character array is = %d\n", strArray_num_elements);

	// *** Piece-meal assignment ***
	MyStrcpy(strArray[0], "My");
	MyStrcpy(strArray[1], "Name");
	MyStrcpy(strArray[2], "Is");
	MyStrcpy(strArray[3], "Priya");
	MyStrcpy(strArray[4], "Saboo");

	printf("\n\n");
	printf("Strings in the 2d character array are :\n\n");

	for (i = 0; i < strArray_num_rows; i++)
	{
		printf("%s ", strArray[i]);
	}
	printf("\n");
	return(0);
}

void MyStrcpy(char str_destination[], char str_source[])
{
	//function prototype
	int MyStrlen(char str[]);
	int j;
	//variable declarations
	int iStringLength = MyStrlen(str_source);
	for ( j = 0; j < iStringLength; j++)
	{
		str_destination[j] = str_source[j];
	}
	str_destination[j] = '\0';
}

int MyStrlen(char str[])
{
	//variable declarations
	int j;
	int string_length = 0;

	//code
	// ** DETERMINING EXACT LENGTH OF THE STRING, BY DETECTING THE FIRST OCCURANCE OF
	//NULL-TERMINATING CHARACTER (\0) ***

	for (j = 0; j < MAX_STRING_LENGTH; j++)
	{
		if (str[j] == '\0')
			break;
		else
			string_length++;
	}
	return(string_length);
}
