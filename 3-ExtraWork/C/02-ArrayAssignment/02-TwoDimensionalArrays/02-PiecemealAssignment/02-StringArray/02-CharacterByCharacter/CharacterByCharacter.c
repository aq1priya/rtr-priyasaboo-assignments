#include <stdio.h>

#define MAX_STRING_LENGTH 512

int main(void)
{
	//variable declarations

	/*
		A STRING IS AN ARRAY OF CHARACTERS, SO char[] IS A char ARRAY AND HENCE,
		char[] IS A STRING.
		AN ARRAY OF char ARRAY IS AN ARRAY OF STRINGS !!!
		HENCE, char[] IS ONE char ARRAY AND HENCE, IS ONE STRING.
		HENCE, char[][] IS AN ARRAY OF char ARRAYS AND HENCE, IS AN ARRAY OF STRING.
	*/

	/*
		Here, the string array can allow a maximum number of 10 strings(10 rows)
		and each of these 10 strings can haveonly upto 15 characters maximum (15 columns)
	*/
	char strArray[5][10]; //5 rows (0, 1, 2, 3, 4) -> 5strings (each can have maximum 10 characters
	int char_size;
	int strArray_size;
	int strArray_num_elements, strArray_num_rows, strArray_num_columns;
	int i;

	//code
	printf("\n\n");

	char_size = sizeof(char);

	strArray_size = sizeof(strArray);
	printf("size of two Dimentional (2D) character Array Is = %d\n", strArray_size);

	strArray_num_rows = strArray_size / sizeof(strArray[0]);
	printf("Number of rows in the two dimentional character array is = %d\n", strArray_num_rows);

	strArray_num_columns = sizeof(strArray[0]) / char_size;
	printf("Number of columns in the two dimentional character array is = %d\n", strArray_num_columns);

	strArray_num_elements = strArray_num_rows * strArray_num_columns;
	printf("Number of elements in the two dimentional character array is = %d\n", strArray_num_elements);
	
	// *** Piece-meal assignment ***
	// ***** ROW 1 / STRING 1 *****
	strArray[0][0] = 'M';
	strArray[0][1] = 'y';
	strArray[0][2] = '\0';	// NULL-TERMINATING CHARACTER

	// ***** ROW 2 / STRING 2 *****
	strArray[1][0] = 'N';
	strArray[1][1] = 'a';
	strArray[1][2] = 'm';	
	strArray[1][3] = 'e';	
	strArray[1][4] = '\0';	// NULL-TERMINATING CHARACTER

	// ***** ROW 3 / STRING 3 *****
	strArray[2][0] = 'I';
	strArray[2][1] = 's';
	strArray[2][2] = '\0';	// NULL-TERMINATING CHARACTER

	// ***** ROW 4 / STRING 4 *****
	strArray[3][0] = 'P';
	strArray[3][1] = 'r';
	strArray[3][2] = 'i';
	strArray[3][3] = 'y';
	strArray[3][4] = 'a';
	strArray[3][5] = '\0';	// NULL-TERMINATING CHARACTER

	// ***** ROW 5 / STRING 5 *****
	strArray[4][0] = 'S';
	strArray[4][1] = 'a';
	strArray[4][2] = 'b';
	strArray[4][3] = 'o';
	strArray[4][4] = 'o';
	strArray[4][5] = '\0';	// NULL-TERMINATING CHARACTER

	printf("\n\n");
	printf("Strings in the 2d character array are :\n\n");

	for (i = 0; i < strArray_num_rows; i++)
	{
		printf("%s ", strArray[i]);
	}
	printf("\n");
	return(0);
}