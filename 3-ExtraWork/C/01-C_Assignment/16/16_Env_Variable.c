/*
16. Print All Environment Variables.
	Note:Make 3 separate files for using loops "while","do-while" and "for" loop.
*/
//------------------------------------------------------------------------------------------------------
//code
//------------------------------------------------------------------------------------------------------
#include<stdio.h>

int main(int argc, char *argv[], char *envp[])
{
	int i;
	printf("\n----------------------------\n\tUsing For Loop\n--------------------------\n");
	for (i = 0; envp[i] != NULL; i++)
	{
		printf("\n%d:  %s", i, envp[i]);
	}
	printf("\n----------------------------\n\tUsing While Loop\n--------------------------\n");
	i = 0;
	while (envp[i] != NULL)
	{
		printf("\n%d: %s", i, envp[i]);
		i++;
	}
	printf("\n----------------------------\n\tUsing do-while Loop\n--------------------------\n");
	i = 0;
	do
	{
		printf("\n%d: %s", i, envp[i]);
		i++;
	} while (envp[i] != NULL);
	return(0);
}