//10. 	Write a program to accept a point in cartesian co - ordinate nad determine the quadrant of the point.

//-----------------------------------------------------------------------------------------------------------
//code
//-----------------------------------------------------------------------------------------------------------
#include <stdio.h>

int main(void)
{
	int x, y;
	printf("Enter the cordinate\n");
	scanf("%d %d", &x, &y);
	if ((x == 0) && (y == 0))
		printf("Point is at origin\n");
	else if ((x > 0) && (y == 0))
		printf("point lies on +ve x-axis\n");
	else if ((x < 0) && (y == 0))
		printf("Point lies on -ve x-axis\n");
	else if ((x == 0) && (y > 0))
		printf("Point lies on +ve y-axis\n");
	else if ((x == 0) && (y < 0))
		printf("Point lies on -ve y-axis\n");
	else if ((x > 0) && (y > 0))
		printf("Point lies in 1st quadrant\n");
	else if ((x < 0 ) && (y > 0))
		printf("Point lies in 2nd quadrant\n");
	else if ((x < 0) && (y < 0))
		printf("Point lies in 3rd quadrant\n");
	else if ((x > 0) && (y < 0))
		printf("Point lies in 4th quadrant\n");
}
