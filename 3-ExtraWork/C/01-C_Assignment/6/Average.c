#include<stdio.h>

int main(int argc, char *argv[])
{
	float avg = 0;
	float num, sum = 0;
	int n = argc - 1;
	int i = 1;

	if (argc < 3)
	{
		printf("usage:\n");
		printf("give numbers to find average as command line arguments\n");
		printf("please give upto 100 arguments\n");
		return(0);
	}
	
	while (argv[i] != NULL)
	{
		printf("%s\n", argv[i]);
		num = atof(argv[i]);
		printf(" %f ", num);
		sum = sum + num;
		i++;
	}
	avg = sum / n;
	printf("Average is %.2f\n", avg);
	return(0);
}