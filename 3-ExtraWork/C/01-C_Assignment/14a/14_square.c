/*
14. Print the powers of 2. Make New files for both parts
	part a)
			i. include "math.h" header file in your program
		   ii. use pow() function
		   Output:
			2
			4
			8
			16
			32
			64
			128
			256
			512
			1024
*/
//---------------------------------------------------------------------------------------
//code
//---------------------------------------------------------------------------------------
#include <stdio.h>
#include <math.h>

int main(void)
{
	int i;
	
	for (i = 1; i <= 10; i++)
	{
		printf("\t%d\n", (int)pow(2, i));
	}
}