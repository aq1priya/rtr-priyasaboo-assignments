/*
// Global Variables
done 1. Write a basic program (as mentioned in class) and make following changes
	a. Take a global Integer variable. (Don't assign it to anything)
	b. Print the global variable. (Inside main() )
	c. Write two functions as "IncrementByOne" and "IncrementByTwo".
	d. Inside "IncrementByOne" function increment the global variable by 1 and print the result.
	e. Inside "IncrementByTwo" function increment the global variable by 2 and print the result.
	f. Call "IncrementByOne" and "IncrementByTwo" atleast twice inside main().
	Observe it's results.
*/
//----------------------------------------------------------------------------------------------------
//code
//----------------------------------------------------------------------------------------------------
#include<stdio.h>

int GlobalInteger;
int IncrementByOne(void);
int IncrementByTwo(void);
int main(void)
{
	printf("Value of Global variable inside main : %d\n", GlobalInteger);
	GlobalInteger = IncrementByOne();
	printf("1. After calling funtion IncrementByOne : %d\n", GlobalInteger);
	GlobalInteger = IncrementByTwo();
	printf("2. After calling funtion IncrementByTwo : %d\n", GlobalInteger);
	GlobalInteger = IncrementByOne();
	printf("3. After calling funtion IncrementByOne : %d\n", GlobalInteger);
	GlobalInteger = IncrementByTwo();
	printf("4. After calling funtion IncrementByTwo : %d\n`", GlobalInteger);
}

int IncrementByOne(void)
{
	GlobalInteger = GlobalInteger + 1;
	return(GlobalInteger);
}

int IncrementByTwo(void)
{
	GlobalInteger = GlobalInteger + 2;
	return(GlobalInteger);
}
