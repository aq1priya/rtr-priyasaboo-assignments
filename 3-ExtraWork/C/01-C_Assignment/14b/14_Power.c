/*
14 part b)
			i. Use bitwise operator "<<" Left Shift Operator
		   ii. Take an integer say "n" and assign it to 1
		  iii. Use it as n<< (the power -1)
			Output:
			0002
			0004
			0008
			0016
			0032
			0064
			0128
			0256
			0512
			1024
		Note: while printing the data try using %4d or %04d as Format Specifier.
*/
//--------------------------------------------------------------------------------------
//code
//--------------------------------------------------------------------------------------
#include<stdio.h>

int main(void)
{
	int n = 1, i;
	for (i = 1; i <= 10; i++)
	{
		n = (n << 1);
		printf("\t%04d\n", n);
	}
}