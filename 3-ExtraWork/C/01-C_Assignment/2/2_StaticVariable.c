/*
2. 	Write a basic program (as mentioned in class) and make following changes
	a. Write a function as "GlobalCount".
	b. In this function declare a static integer (Don't assign it to anything).
	c. Then increment the integer and print it.
	e. Call this function inside main() atlest twice and observe the results.
*/
//---------------------------------------------------------------------------------------
//code
//---------------------------------------------------------------------------------------
#include <stdio.h>

int GlobalCount(void);

int main(void)
{
	int MyVariable;
	MyVariable = GlobalCount();
	printf("1. Value of static integer variable : %d\n", MyVariable);
	MyVariable = GlobalCount();
	printf("2. Value of tatic integer variable : %d\n", MyVariable);
}

int GlobalCount(void)
{
	static int MyVariable;
	MyVariable++;
	return(MyVariable);
}
